package de.crysxd.octoapp.signin.discover

import android.os.Bundle
import android.transition.AutoTransition
import android.transition.TransitionInflater
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.ViewPropertyAnimator
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.core.view.children
import androidx.core.view.forEach
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.common.NetworkStateProvider
import de.crysxd.baseui.common.NetworkStateViewModel
import de.crysxd.baseui.di.BaseUiInjector
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.requestFocusAndOpenSoftKeyboard
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.NetworkService
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.utils.AnimationTestUtils
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import de.crysxd.octoapp.sharedcommon.url.isSharedOctoEverywhereUrl
import de.crysxd.octoapp.signin.R
import de.crysxd.octoapp.signin.databinding.BaseSigninFragmentBinding
import de.crysxd.octoapp.signin.databinding.DiscoverFragmentContentManualBinding
import de.crysxd.octoapp.signin.databinding.DiscoverFragmentContentOptionsBinding
import de.crysxd.octoapp.signin.di.injectViewModel
import de.crysxd.octoapp.signin.ext.setUpAsHelpButton
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import kotlin.math.roundToLong


class DiscoverFragment : BaseFragment() {
    private val tag = "DiscoverFragment"
    override val viewModel by injectViewModel<DiscoverViewModel>()
    private val wifiViewModel by injectViewModel<NetworkStateViewModel>(BaseUiInjector.get().viewModelFactory())
    private lateinit var binding: BaseSigninFragmentBinding
    private var optionsBinding: DiscoverFragmentContentOptionsBinding? = null
    private var manualBinding: DiscoverFragmentContentManualBinding? = null
    private var loadingAnimationJob: Job? = null
    private var backgroundAlpha = 1f
    private var loadingAnimations: ViewPropertyAnimator? = null
    private val moveBackToOptionsBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            viewModel.moveToOptionsState()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.sign_in_shard_element)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(R.transition.sign_in_shard_element)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        BaseSigninFragmentBinding.inflate(inflater, container, false).also {
            binding = it
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backgroundAlpha = binding.octoBackground.alpha

        playLoadingAnimation()
        optionsBinding = null
        manualBinding = null
        viewModel.uiState.observe(viewLifecycleOwner) {
            when (it) {
                DiscoverViewModel.UiState.Loading -> playLoadingAnimation()

                is DiscoverViewModel.UiState.Options -> {
                    moveToOptionsLayout()
                    createDiscoveredOptions(it.discoveredOptions)
                    createPreviouslyConnectedOptions(it.previouslyConnectedOptions, it.supportsQuickSwitch)
                }

                is DiscoverViewModel.UiState.Manual -> moveToManualLayout(
                    webUrl = (it as? DiscoverViewModel.UiState.ManualSuccess)?.webUrl ?: "",
                    openSoftKeyboard = it !is DiscoverViewModel.UiState.ManualSuccess
                )
            }

            if (it is DiscoverViewModel.UiState.ManualError && !it.handled) {
                it.handled = true
                showManualError(it)
            }

            if (it is DiscoverViewModel.UiState.ManualSuccess && !it.handled) {
                it.handled = true
                continueWithManualConnect(it.webUrl)
            }
        }

        wifiViewModel.networkState.observe(viewLifecycleOwner) {
            Napier.i(tag = tag, message = "Wifi state: $it")
            binding.wifiWarning.isVisible = it is NetworkStateProvider.NetworkState.WifiNotConnected
        }

        // Disable back button, we can't go back here
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() = requireActivity().finish()
        })
    }

    private fun showManualError(it: DiscoverViewModel.UiState.ManualError) {
        if (!it.message.isNullOrBlank()) {
            requireOctoActivity().showDialog(
                OctoActivity.Message.DialogMessage(
                    text = { it.message },
                    neutralButton = { getString(R.string.show_details) },
                    neutralAction = { requireOctoActivity().showErrorDetailsDialog(it.exception, offerSupport = it.errorCount > 1) }
                )
            )
        } else {
            requireOctoActivity().showErrorDetailsDialog(it.exception, offerSupport = it.errorCount > 1)
        }
    }

    override fun onStart() {
        super.onStart()
        binding.scrollView.setupWithToolbar(requireOctoActivity())
    }

    override fun onStop() {
        super.onStop()
        manualBinding?.input?.hideSoftKeyboard()
    }

    private fun beginDelayedTransition() = TransitionManager.beginDelayedTransition(binding.root, AutoTransition().also {
        it.interpolator = DecelerateInterpolator()
        manualBinding?.input?.let { v -> it.excludeChildren(v, true) }
    })

    private fun playLoadingAnimation() {
        loadingAnimationJob = viewLifecycleOwner.lifecycleScope.launchWhenCreatedFixed {
            val duration = 600L
            binding.loading.progress.isVisible = true
            binding.octoBackground.alpha = 0f
            binding.loading.title.setText(R.string.sign_in___discovery___welcome_title)
            binding.loading.subtitle.setText(R.string.sign_in___discovery___welcome_subtitle_searching)

            // Cancel here if we don't want animations
            if (AnimationTestUtils.animationsDisabled) return@launchWhenCreatedFixed

            binding.loading.title.alpha = 0f
            binding.loading.subtitle.alpha = 0f
            binding.loading.progress.alpha = 0f

            delay(duration)
            binding.octoBackground.animate().alpha(backgroundAlpha).setDuration(duration).withEndAction {
                binding.octoView.swim()
            }.also {
                loadingAnimations = it
            }.start()
            delay(150)
            binding.loading.title.animate().alpha(1f).setDuration(duration).start()
            delay(150)
            binding.loading.subtitle.animate().alpha(1f).setDuration(duration).start()
            delay(150)
            binding.loading.progress.animate().alpha(1f).setDuration(duration).start()

            val steps = 200
            binding.loading.progress.max = steps
            val delay = (viewModel.getLoadingDelay() / steps.toFloat()).roundToLong()
            repeat(steps) {
                delay(delay)
                binding.loading.progress.progress = it
            }
            binding.loading.progress.isIndeterminate = true
        }
    }

    private fun createDiscoveredOptions(options: List<NetworkService>) = optionsBinding?.let { binding ->
        // Delete all that are not longer shown
        binding.discoveredOptions.children.toList().forEach {
            if (it !is DiscoverOptionView) binding.discoveredOptions.removeView(it)
            else if (!options.any { o -> it.isShowing(o) }) binding.discoveredOptions.removeView(it)
        }

        // Add all missing
        options.map {
            binding.discoveredOptions.children.firstOrNull { view ->
                view is DiscoverOptionView && view.isShowing(it)
            } ?: DiscoverOptionView(requireContext()).apply {
                show(it)
                setOnClickListener { _ -> continueWithDiscovered(it) }
            }
        }.filter {
            it.parent == null
        }.forEach {
            binding.discoveredOptions.addView(it, ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT))
        }

        // Update title visibility
        binding.discoveredOptionsTitle.isVisible = binding.discoveredOptions.childCount > 0
    }

    private fun createPreviouslyConnectedOptions(options: List<PrinterConfigurationV3>, quickSwitchEnabled: Boolean) = optionsBinding?.let { binding ->
        // Delete all that are not longer shown
        binding.previousOptions.children.toList().forEach {
            if (it == binding.quickSwitchOption) return@forEach
            else if (it !is DiscoverOptionView) binding.previousOptions.removeView(it)
            else if (!options.any { o -> it.isShowing(o) }) binding.previousOptions.removeView(it)
        }

        // Add all missing
        options.map {
            binding.previousOptions.children.firstOrNull { view ->
                view is DiscoverOptionView && view.isShowing(it)
            } ?: DiscoverOptionView(requireContext()).apply {
                show(it, quickSwitchEnabled)
                setOnClickListener { _ ->
                    if (quickSwitchEnabled) {
                        continueWithPreviouslyConnected(it)
                    } else {
                        continueWithManualConnect(webUrl = it.webUrl.toString(), confirmed = false)
                    }
                }
                onDelete = { deleteAfterConfirmation(it) }
            }
        }.filter {
            it.parent == null
        }.forEach {
            binding.previousOptions.addView(it, ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT))
        }

        // Update title visibility
        binding.previousOptionsTitle.isVisible = binding.previousOptions.childCount > 1
        binding.quickSwitchOption.isVisible = !quickSwitchEnabled && binding.previousOptionsTitle.isVisible
        binding.buttonShowDelete.isVisible = binding.buttonShowDelete.isVisible && binding.previousOptionsTitle.isVisible
        binding.quickSwitchOption.setOnClickListener { continueWithPurchase() }
    }

    private fun continueWithPurchase() {
        UriLibrary.getPurchaseUri().open(requireOctoActivity())
    }

    private fun continueWithDiscovered(octoPrint: NetworkService) {
        OctoAnalytics.logEvent(
            when (octoPrint.origin) {
                NetworkService.Origin.DnsSd -> OctoAnalytics.Event.DnsServiceSelected
                NetworkService.Origin.Upnp -> OctoAnalytics.Event.UpnpServiceSelected
            },
            mapOf(
                "options_count" to (optionsBinding?.discoveredOptions?.childCount ?: -1)
            )
        )

        val extras = FragmentNavigatorExtras(binding.octoView to "octoView", binding.octoBackground to "octoBackground")
        val directions = DiscoverFragmentDirections.requestAccess(
            webUrl = UriLibrary.secureEncode(octoPrint.webUrl),
            type = octoPrint.type
        )
        findNavController().navigate(directions, extras)
    }

    private fun continueWithPreviouslyConnected(octoPrint: PrinterConfigurationV3) {
        viewModel.activatePreviouslyConnected(octoPrint)
    }

    private fun continueWithManualConnect(webUrl: String, confirmed: Boolean = false) {
        val httpUrl = webUrl.toHttpUrlOrNull()?.toString()?.toUrl()
        when {
            // Skip subsequent checks, user confirmed
            confirmed -> Unit

            httpUrl?.isSharedOctoEverywhereUrl() == true -> return requireOctoActivity().showDialog(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.sign_in___discovery___octoeverywhere_error_message) },
                    neutralButton = { getString(R.string.sign_in___discovery___octoeverywhere_error_comply) },
                    neutralAction = {},
                    positiveAction = { continueWithManualConnect(webUrl, true) },
                    positiveButton = { getString(R.string.sign_in___discovery___octoeverywhere_error_ignore) },
                )
            )

            httpUrl?.isOctoEverywhereUrl() == true -> return requireOctoActivity().showDialog(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.sign_in___discovery___octoeverywhere_error_message_addition_shared_connection) },
                    positiveAction = { },
                    positiveButton = { getString(android.R.string.ok) },
                )
            )

            httpUrl?.isNgrokUrl() == true -> return requireOctoActivity().showDialog(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.sign_in___discovery___ngrok_error_message) },
                    positiveAction = { },
                    positiveButton = { getString(android.R.string.ok) },
                )
            )
        }

        manualBinding?.input?.hideSoftKeyboard()
        viewLifecycleOwner.lifecycleScope.launchWhenCreatedFixed {
            delay(200)

            // Start the "fix" flow, it will test the connection to the given URL
            // We do not allow the API key to be reused to prevent the user from bypassing quick switch.
            // If the user has BillingManager.FEATURE_QUICK_SWITCH, the fix flow will always allow API key reuse
            val extras = FragmentNavigatorExtras(binding.octoView to "octoView", binding.octoBackground to "octoBackground")
            val directions = DiscoverFragmentDirections.probeConnection(baseUrl = UriLibrary.secureEncode(webUrl), allowApiKeyReuse = false.toString())
            findNavController().navigate(directions, extras)
        }
    }

    private fun continueWithEnableQuickSwitch() = UriLibrary.getPurchaseUri().open(requireOctoActivity())

    private fun deleteAfterConfirmation(option: PrinterConfigurationV3) {
        requireOctoActivity().showDialog(
            OctoActivity.Message.DialogMessage(
                text = { getString(R.string.sign_in___discovery___delete_printer_message, option.label) },
                positiveButton = { getString(R.string.sign_in___discovery___delete_printer_confirmation) },
                positiveAction = { viewModel.deleteInstance(option.id) },
                neutralAction = {},
                neutralButton = { getString(R.string.cancel) },
            )
        )
    }

    private fun moveToOptionsLayout() {
        beginDelayedTransition()

        // Cancel loading animations
        loadingAnimationJob?.cancel()
        loadingAnimations?.cancel()

        binding.octoView.idle()
        moveBackToOptionsBackPressedCallback.isEnabled = false
        binding.octoView.isVisible = true

        binding.content.removeAllViews()
        val localOptionsBinding = optionsBinding ?: let {
            DiscoverFragmentContentOptionsBinding.inflate(LayoutInflater.from(requireContext()), binding.content, false)
        }
        binding.content.removeAllViews()
        (localOptionsBinding.root.parent as? ViewGroup)?.removeView(localOptionsBinding.root)
        binding.content.addView(localOptionsBinding.root)
        binding.contentWrapper.updateLayoutParams<FrameLayout.LayoutParams> { gravity = Gravity.CENTER_VERTICAL }
        optionsBinding = localOptionsBinding

        setUpAsHelpButton(localOptionsBinding.help)
        localOptionsBinding.manualConnectOptionOctoPrint.showManualConnect(octoPrint = true)
        localOptionsBinding.manualConnectOptionOctoPrint.setOnClickListener {
            OctoAnalytics.logEvent(
                OctoAnalytics.Event.ManualUrlSelected,
                mapOf(
                    "options_count" to (optionsBinding?.discoveredOptions?.childCount ?: -1)
                )
            )
            viewModel.moveToManualState()
        }
        localOptionsBinding.manualConnectOptionKlipper.showManualConnect(octoPrint = false)
        localOptionsBinding.manualConnectOptionKlipper.setOnClickListener {
            OctoAnalytics.logEvent(
                OctoAnalytics.Event.ManualUrlSelected,
                mapOf(
                    "options_count" to (optionsBinding?.discoveredOptions?.childCount ?: -1)
                )
            )
            viewModel.moveToManualState()
        }
        localOptionsBinding.quickSwitchOption.showQuickSwitchOption()
        localOptionsBinding.quickSwitchOption.setOnClickListener {
            continueWithEnableQuickSwitch()
        }
        localOptionsBinding.buttonShowDelete.setOnClickListener {
            beginDelayedTransition()
            localOptionsBinding.buttonShowDelete.isVisible = false
            localOptionsBinding.previousOptions.forEach {
                if (it != localOptionsBinding.quickSwitchOption) {
                    (it as? DiscoverOptionView)?.showDelete()
                }
            }
        }
    }

    private fun moveToManualLayout(webUrl: String, openSoftKeyboard: Boolean) {
        loadingAnimationJob?.cancel()
        loadingAnimations?.cancel()
        binding.octoBackground.clearAnimation()
        binding.octoBackground.alpha = backgroundAlpha
        beginDelayedTransition()
        moveBackToOptionsBackPressedCallback.isEnabled = true
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, moveBackToOptionsBackPressedCallback)
        binding.octoView.idle()
        binding.octoView.isVisible = false

        val localManualBinding = manualBinding ?: let {
            DiscoverFragmentContentManualBinding.inflate(LayoutInflater.from(requireContext()), binding.content, false)
        }
        binding.content.removeAllViews()
        (localManualBinding.root.parent as? ViewGroup)?.removeView(localManualBinding.root)
        binding.content.addView(localManualBinding.root)
        binding.contentWrapper.updateLayoutParams<FrameLayout.LayoutParams> { gravity = Gravity.TOP }
        manualBinding = localManualBinding
        manualBinding?.input?.hintActive = listOf(
            getString(R.string.sign_in___discover___web_url_hint, "OctoPrint/Klipper"),
            getString(R.string.sign_in___discover___web_url_hint_active)
        ).joinToString(" - ")

        // If we do not have a webURL
        if (openSoftKeyboard) {
            viewLifecycleOwner.lifecycleScope.launchWhenCreatedFixed {
                delay(300)
                manualBinding?.input?.editText?.requestFocusAndOpenSoftKeyboard()
            }
        }

        setUpAsHelpButton(localManualBinding.help)
        if (webUrl.isNotBlank()) {
            manualBinding?.input?.editText?.setText(webUrl)
            manualBinding?.input?.editText?.setSelection(webUrl.length)
        }
        manualBinding?.input?.editText?.setOnEditorActionListener { _, _, _ ->
            manualBinding?.buttonContinue?.performClick()
            true
        }
        localManualBinding.buttonContinue.setOnClickListener {
            val input = localManualBinding.input.editText.text
            Napier.i(tag = tag, message = "Continue with input '$input'")
            viewModel.testWebUrl(input?.toString() ?: "")
        }
    }
}
