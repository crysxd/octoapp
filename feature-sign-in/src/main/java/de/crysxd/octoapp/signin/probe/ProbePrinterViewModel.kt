package de.crysxd.octoapp.signin.probe

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.ktor.http.Url
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ProbePrinterViewModel(
    private val useCase: TestFullNetworkStackUseCase,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : BaseViewModel() {

    companion object {
        private const val MIN_PROBE_DURATION = 2000L
    }

    var lastWebUrl: Url? = null
        private set
    var probeIsActive: Boolean = false
    private val mutableUiState = MutableLiveData<UiState>(UiState.Loading)
    val uiState = mutableUiState.map { it }

    fun probe(webUrl: String, apiKey: String? = null) = viewModelScope.launch(coroutineExceptionHandler) {
        // Don't allow consecutive probes
        if (probeIsActive) return@launch

        try {
            probeIsActive = true
            val start = System.currentTimeMillis()
            mutableUiState.postValue(UiState.Loading)
            val apiKeyToUse = apiKey ?: printerConfigurationRepository.findInstances(webUrl.toUrlOrNull()).firstOrNull()?.first?.apiKey ?: ""
            val target = TestFullNetworkStackUseCase.Target.Printer(webUrl = webUrl, apiKey = apiKeyToUse)
            val finding = useCase.execute(target)
            (MIN_PROBE_DURATION - (System.currentTimeMillis() - start)).takeIf { it > 0 }?.let { delay(it) }
            lastWebUrl = finding.webUrl
            mutableUiState.postValue(UiState.FindingsReady(finding))
        } finally {
            probeIsActive = false
        }
    }

    sealed class UiState {
        data object Loading : UiState()
        data class FindingsReady(val finding: TestFullNetworkStackUseCase.Finding, var handled: Boolean = false) : UiState()
    }
}