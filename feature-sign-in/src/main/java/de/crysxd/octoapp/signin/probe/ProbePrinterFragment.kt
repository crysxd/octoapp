package de.crysxd.octoapp.signin.probe

import android.os.Bundle
import android.transition.TransitionInflater
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.common.NetworkStateProvider
import de.crysxd.baseui.common.NetworkStateViewModel
import de.crysxd.baseui.common.feedback.SendFeedbackDialog
import de.crysxd.baseui.di.BaseUiInjector
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.utils.ThemePlugin
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.BackendType
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.base.network.DetectBrokenSetupInterceptor
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.utils.texts.PrinterFindingDescriptionLibrary
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.signin.R
import de.crysxd.octoapp.signin.databinding.BaseSigninFragmentBinding
import de.crysxd.octoapp.signin.databinding.ProbeFragmentFindingBinding
import de.crysxd.octoapp.signin.databinding.ProbeFragmentInitialBinding
import de.crysxd.octoapp.signin.di.injectViewModel
import de.crysxd.octoapp.signin.ext.goBackToDiscover
import de.crysxd.octoapp.signin.ext.setUpAsHelpButton
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import io.noties.markwon.Markwon
import kotlinx.coroutines.runBlocking
import java.util.UUID

class ProbePrinterFragment : BaseFragment() {

    private val tag = "ProbeOctoPrintFragment"
    override val viewModel by injectViewModel<ProbePrinterViewModel>()
    private lateinit var binding: BaseSigninFragmentBinding
    private val wifiViewModel by injectViewModel<NetworkStateViewModel>(BaseUiInjector.get().viewModelFactory())
    private var loadingBinding: ProbeFragmentInitialBinding? = null
    private var findingBinding: ProbeFragmentFindingBinding? = null
    private val findingDescriptionLibrary by lazy { PrinterFindingDescriptionLibrary() }
    private val initialWebUrl by lazy {
        UriLibrary.secureDecode(navArgs<ProbePrinterFragmentArgs>().value.baseUrl)
    }
    private val instanceId by lazy {
        navArgs<ProbePrinterFragmentArgs>().value.instanceId
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.sign_in_shard_element)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(R.transition.sign_in_shard_element)
        viewModel.probe(initialWebUrl)

        // While probing the connection we disable the global broken setup interceptor to prevent
        // error messages to pop up while probing
        DetectBrokenSetupInterceptor.enabled = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        BaseSigninFragmentBinding.inflate(layoutInflater, container, false).also {
            binding = it
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loading.subtitle.isVisible = false
        binding.loading.title.text = getString(R.string.sign_in___probe___probing_active_title)

        if (SharedCommonInjector.get().platform.betaBuild) {
            binding.contentWrapper.setOnClickListener {
                SendFeedbackDialog.create(isForBugReport = true).show(childFragmentManager, "feedback")
            }
        }

        wifiViewModel.networkState.observe(viewLifecycleOwner) {
            Napier.i(tag = tag, message = "Wifi state: $it")
            binding.wifiWarning.isVisible = it is NetworkStateProvider.NetworkState.WifiNotConnected
        }

        viewModel.uiState.observe(viewLifecycleOwner) {
            when (it) {
                ProbePrinterViewModel.UiState.Loading -> showLoading()
                is ProbePrinterViewModel.UiState.FindingsReady -> {
                    val isReady = it.finding is TestFullNetworkStackUseCase.Finding.OctoPrintReady || it.finding is TestFullNetworkStackUseCase.Finding.MoonrakerReady
                    val isApiKeyMissingOctoPrint = it.finding is TestFullNetworkStackUseCase.Finding.InvalidApiKey && it.finding.type == BackendType.OctoPrint

                    when {
                        isReady || isApiKeyMissingOctoPrint -> {
                            // We might get navigated back to this fragment in case there are issues later down the road
                            // This checks makes sure we are never reusing a old result to navigate away and instead restarting the probe if the screen
                            // gets shown again
                            if (it.handled) {
                                viewModel.probe(viewModel.lastWebUrl?.toString() ?: initialWebUrl)
                                return@observe
                            }

                            it.handled = true
                            when (it.finding) {
                                is TestFullNetworkStackUseCase.Finding.OctoPrintReady -> continueWithPresentApiKey(
                                    finding = it.finding
                                )

                                is TestFullNetworkStackUseCase.Finding.MoonrakerReady -> continueToSuccess(
                                    webUrl = it.finding.webUrl,
                                    type = BackendType.Moonraker,
                                    apiKey = it.finding.apiKey ?: ""
                                )

                                is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> continueToRequestApiKey(
                                    webUrl = it.finding.webUrl,
                                    type = it.finding.type
                                )

                                else -> Unit
                            }
                        }

                        else -> showFinding(it.finding)
                    }
                }
            }
        }
    }

    private fun beginDelayedTransition() = TransitionManager.beginDelayedTransition(binding.root)

    private fun continueToRequestApiKey(webUrl: Url, type: BackendType) = binding.octoView.doAfterAnimation(lifecycle) {
        // Octo is now in a neutral position, we can animate states
        val extras = FragmentNavigatorExtras(binding.octoView to "octoView", binding.octoBackground to "octoBackground")
        val directions = ProbePrinterFragmentDirections.requestAccess(UriLibrary.secureEncode(webUrl.toString()), type = type)
        findNavController().navigate(directions, extras)
    }

    private fun continueToSuccess(webUrl: Url, type: BackendType, apiKey: String) = binding.octoView.doAfterAnimation(lifecycle) {
        // Octo is now in a neutral position, we can animate states
        val extras = FragmentNavigatorExtras(binding.octoView to "octoView", binding.octoBackground to "octoBackground")
        val directions = ProbePrinterFragmentDirections.actionSuccess(UriLibrary.secureEncode(webUrl.toString()), type = type, apiKey = apiKey)
        Napier.i(tag = tag, message = "Continue to request access")
        findNavController().navigate(directions, extras)
    }

    private fun continueWithPresentApiKey(finding: TestFullNetworkStackUseCase.Finding.OctoPrintReady) {
        // If the "thing" which started this screen gave us a instance id of an existing instance OR the user has the quick switch feature, we can continue with the existing
        // API key. Otherwise, the user is forced to reconnect OctoPrint. Reusing might be explicitly allowed, when this fragment is started to troubleshoot the connection.
        if (instanceId != null && BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
            val repo = BaseInjector.get().octorPrintRepository()
            val oldInstance = instanceId?.let { repo.get(it) }
            val instance = oldInstance?.copy(
                webUrl = finding.webUrl,
                apiKey = finding.apiKey,
            ) ?: PrinterConfigurationV3(
                id = UUID.randomUUID().toString(),
                webUrl = finding.webUrl,
                apiKey = finding.apiKey,
            )

            // Clearing and setting the instance will ensure we reset the navigation
            Napier.i(tag = tag, message = "Activating with current API key")
            BaseInjector.get().octorPrintRepository().clearActive()
            requireOctoActivity().enforceAllowAutomaticNavigationFromCurrentDestination()
            BaseInjector.get().octorPrintRepository().setActive(instance, trigger = "Probe")
        } else {
            Napier.i(tag = tag, message = "No instance or quick switch, continue to request access")
            continueToSuccess(webUrl = finding.webUrl, type = BackendType.OctoPrint, apiKey = finding.apiKey)
        }
    }

    private fun showLoading() {
        beginDelayedTransition()
        binding.octoView.scheduleAnimation(600) { swim() }
        val b = loadingBinding ?: ProbeFragmentInitialBinding.inflate(LayoutInflater.from(requireContext()), binding.content, false)
        loadingBinding = b
        binding.content.removeAllViews()
        (b.root.parent as? ViewGroup)?.removeView(b.root)
        binding.content.addView(b.root)
    }

    private fun showFinding(finding: TestFullNetworkStackUseCase.Finding) {
        beginDelayedTransition()
        binding.octoView.idle()
        val b = findingBinding ?: ProbeFragmentFindingBinding.inflate(LayoutInflater.from(requireContext()), binding.content, false)
        findingBinding = b
        binding.content.removeAllViews()
        (b.root.parent as? ViewGroup)?.removeView(b.root)
        binding.content.addView(b.root)
        setUpAsHelpButton(b.help)

        val markwon = Markwon.builder(requireContext())
            .usePlugin(ThemePlugin(requireContext()))
            .build()

        markwon.setMarkdown(b.content, findingDescriptionLibrary.getExplainerForFinding(finding))
        markwon.setMarkdown(b.title, findingDescriptionLibrary.getTitleForFinding(finding))

        b.buttonEdit.text = getEditButtonText()
        b.buttonEdit.setOnClickListener { getEditButtonAction() }
        b.buttonContinue.setOnClickListener { performPrimaryAction(finding) }
        b.buttonContinue.text = getPrimaryActionText(finding)
        b.passwordInput.isVisible = finding is TestFullNetworkStackUseCase.Finding.BasicAuthRequired
        b.usernameInput.isVisible = b.passwordInput.isVisible
        b.apiKeyInput.isVisible = finding is TestFullNetworkStackUseCase.Finding.InvalidApiKey
        b.passwordInput.editText.setOnEditorActionListener { _, _, _ ->
            performPrimaryAction(finding)
            true
        }

        val inputTint = ContextCompat.getColor(requireContext(), de.crysxd.baseui.R.color.input_background_alternative)
        b.passwordInput.backgroundTint = inputTint
        b.usernameInput.backgroundTint = inputTint
        b.apiKeyInput.backgroundTint = inputTint
    }

    private fun getEditButtonAction() {
        if (isInTestOnlyMode()) {
            // Case A: We got here because a API key was invalid, there is an active instance
            goBackToDiscover()
        } else {
            // Case B: User is signing in, but nothing is active yet
            findNavController().popBackStack()
        }
    }

    private fun getEditButtonText() = if (isInTestOnlyMode()) {
        // Case A: We got here because a API key was invalid, there is an active instance
        getString(R.string.sign_in___connect_to_other_octoprint)
    } else {
        // Case B: User is signing in, but nothing is active yet
        getString(R.string.sign_in___probe___edit_information)
    }

    private fun isInTestOnlyMode() = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot() != null

    private fun getPrimaryActionText(finding: TestFullNetworkStackUseCase.Finding) = when (finding) {
        is TestFullNetworkStackUseCase.Finding.HttpsNotTrusted -> if (finding.certificate != null) {
            getString(R.string.sing_in___probe___trust_and_continue)
        } else {
            getString(R.string.sign_in___try_again)
        }

        is TestFullNetworkStackUseCase.Finding.BasicAuthRequired -> getString(R.string.sign_in___continue)
        is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> getString(R.string.sign_in___continue)
        else -> getString(R.string.sign_in___try_again)
    }

    private fun performPrimaryAction(finding: TestFullNetworkStackUseCase.Finding): Any = when (finding) {
        is TestFullNetworkStackUseCase.Finding.HttpsNotTrusted -> {
            BaseInjector.get().sslKeyStoreHandler().also { keyHandler ->
                finding.certificate?.let { keyHandler.storeCertificate(it.certificate) }
                if (finding.weakHostnameVerificationRequired) {
                    keyHandler.enforceWeakVerificationForHost(finding.webUrl)
                }
            }

            // Start again
            viewModel.probe(finding.webUrl.toString())
        }

        is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> viewModel.probe(
            webUrl = finding.webUrl.toString(),
            apiKey = findingBinding?.apiKeyInput?.editText?.text?.toString()
        )

        is TestFullNetworkStackUseCase.Finding.BasicAuthRequired -> {
            val webUrl = finding.webUrl.withBasicAuth(
                user = findingBinding?.usernameInput?.editText?.text?.toString(),
                password = findingBinding?.passwordInput?.editText?.text?.toString()
            )
            runBlocking {
                SensitiveDataMask.registerWebUrl(webUrl)
            }
            viewModel.probe(webUrl.toString())
        }

        else -> viewModel.probe(finding.webUrl.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        DetectBrokenSetupInterceptor.enabled = true
    }
}