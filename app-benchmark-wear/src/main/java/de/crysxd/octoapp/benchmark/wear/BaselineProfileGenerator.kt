package de.crysxd.octoapp.benchmark.wear

import androidx.benchmark.macro.junit4.BaselineProfileRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.crysxd.octoapp.tests.rules.InitBenchmarkAppRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BaselineProfileGenerator {

    private val baselineProfileRule = BaselineProfileRule()

    @get:Rule
    val chain = RuleChain.outerRule(baselineProfileRule)
        .around(InitBenchmarkAppRule())
        .around(TestDocumentationRule(showNotification = false))

    @Test
    fun startup() = baselineProfileRule.collect(
        packageName = "de.crysxd.octoapp",
        stableIterations = 2,
        maxIterations = 5,
    ) {
        launchApp()
        Thread.sleep(1000)
    }

    @Test
    fun journey() = baselineProfileRule.collect(packageName = "de.crysxd.octoapp", includeInStartupProfile = true) {
        launchApp()
        runBenchmark()
    }
}