package de.crysxd.octoapp.help.troubleshoot

import androidx.lifecycle.asLiveData
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow

@OptIn(ExperimentalCoroutinesApi::class)
class WebcamTroubleShootingViewModel(
    printerConfigurationRepository: PrinterConfigurationRepository,
    printerEngineProvider: PrinterEngineProvider,
    testFullNetworkStackUseCase: TestFullNetworkStackUseCase,
) : BaseViewModel() {

    companion object {
        private const val TAG = "WebcamTroubleShootingViewModel"
        private const val MIN_LOADING_TIME = 2000
    }

    private val retrySignalChannel = MutableStateFlow(0)
    val uiState = printerEngineProvider.printerFlow()
        .combine(retrySignalChannel) { _, _ ->
            // No data returned, we only need a trigger :)
        }.flatMapLatest {
            val instance = requireNotNull(printerConfigurationRepository.getActiveInstanceSnapshot()) { "No OctoPrint active" }
            val activeIndex = instance.appSettings?.activeWebcamIndex ?: 0
            Napier.i(tag = TAG, message = "Troubleshooting webcam $activeIndex @ $instance'")
            flow {
                emit(UiState.Loading)
                val start = System.currentTimeMillis()
                val target = TestFullNetworkStackUseCase.Target.Webcam(
                    instanceId = instance.id,
                    webcamIndex = activeIndex
                )
                val finding = testFullNetworkStackUseCase.execute(target)
                val end = System.currentTimeMillis()
                val delay = MIN_LOADING_TIME - (end - start)
                if (delay > 0) delay(delay)
                emit(UiState.Finding(finding))
            }
        }.catch {
            Napier.e(tag = TAG, message = "Failed", throwable = it)
            if (it is TestFullNetworkStackUseCase.WebcamUnsupportedException) {
                emit(UiState.UnsupportedWebcam)
            } else {
                emit(UiState.Finding(TestFullNetworkStackUseCase.Finding.UnexpectedIssue(null, it)))
            }
        }.asLiveData()

    fun retry() = retrySignalChannel.value++

    sealed class UiState {
        data object Loading : UiState()
        data class Finding(val finding: TestFullNetworkStackUseCase.Finding) : UiState()
        data object UnsupportedWebcam : UiState()
    }
}