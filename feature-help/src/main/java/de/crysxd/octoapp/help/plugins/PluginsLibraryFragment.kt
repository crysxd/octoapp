package de.crysxd.octoapp.help.plugins

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.baseui.compose.screens.ComposeScreenFragment
import de.crysxd.baseui.compose.screens.pluginlibrary.PluginLibraryScreen

class PluginsLibraryFragment : ComposeScreenFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = null, insets = composeInsets) {
        PluginLibraryScreen(initialCategory = arguments?.getString("category"))
    }
}
