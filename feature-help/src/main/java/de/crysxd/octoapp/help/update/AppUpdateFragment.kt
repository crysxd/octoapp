package de.crysxd.octoapp.help.update

import android.os.Bundle
import android.os.RemoteException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallException
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallErrorCode
import com.google.android.play.core.install.model.UpdateAvailability
import de.crysxd.baseui.compose.screens.help.AppUpdater
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.help.BuildConfig
import io.github.aakira.napier.Napier

class AppUpdateFragment : Fragment(), AppUpdater {

    private val tag = "AppUpdateFragment"
    private val appUpdateManager by lazy { AppUpdateManagerFactory.create(requireContext()) }
    private val minUpdatePriority = 2
    private val appUpdateType = AppUpdateType.IMMEDIATE
    private val appUpdateRequestCode = 135
    private var availableUpdate: AppUpdateInfo? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = View(inflater.context)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Skip ob debug builds
        if (BuildConfig.DEBUG) return

        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = try {
            appUpdateManager.appUpdateInfo
        } catch (e: Exception) {
            Napier.w(tag = tag, message = "Unable to connect to PlayStore")
            null
        }

        // Checks that the platform will allow the specified type of update.
        Napier.i(tag = tag, message = "Requesting app update info")
        appUpdateInfoTask?.addOnCompleteListener { result ->
            Napier.i(tag = tag, message = "App update info: $result")

            if (result.exception != null) {
                if ((result.exception as? InstallException)?.errorCode != InstallErrorCode.ERROR_APP_NOT_OWNED && result.exception !is RemoteException) {
                    Napier.w(tag = tag, message = "Failed", throwable = result.exception)
                }
            } else {
                val appUpdateInfo = result.result

                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.AppUpdateAvailable)
                    Napier.i(tag = tag, message = "App update info: $appUpdateInfo")

                    if (appUpdateInfo.updatePriority() >= minUpdatePriority) activity?.let {
                        Napier.i(tag = tag, message = "Requesting app update")
                        OctoAnalytics.logEvent(OctoAnalytics.Event.AppUpdatePresented)

                        // Request the update
                        triggerUpdateIfAvailable()
                    }
                } else {
                    availableUpdate = null
                }
            }
        }
    }

    override fun triggerUpdateIfAvailable(): Boolean {
        Napier.i(tag = tag, message = "Triggering update if available: ${availableUpdate?.availableVersionCode()}")
        val update = availableUpdate ?: return false
        val activity = activity ?: return false

        return if (update.isUpdateTypeAllowed(appUpdateType)) {
            availableUpdate = null
            appUpdateManager.startUpdateFlowForResult(update, appUpdateType, activity, appUpdateRequestCode)
            true
        } else {
            false
        }
    }
}