package de.crysxd.octoapp.help.faq

import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.compose.screens.help.HelpLauncherViewModel
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.utils.ThemePlugin
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.models.Faq
import de.crysxd.octoapp.base.models.KnownBug
import de.crysxd.octoapp.help.R
import de.crysxd.octoapp.help.databinding.HelpDetailFragmentBinding
import io.github.aakira.napier.Napier
import io.noties.markwon.Markwon
import io.noties.markwon.image.ImagesPlugin
import io.noties.markwon.linkify.LinkifyPlugin
import kotlinx.coroutines.launch

class HelpDetailFragment : Fragment(), InsetAwareScreen {

    private val args by navArgs<HelpDetailFragmentArgs>()
    private lateinit var binding: HelpDetailFragmentBinding
    private val viewModel: HelpLauncherViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        HelpDetailFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            val markwon = Markwon.builder(view.context)
                .usePlugin(ImagesPlugin.create())
                .usePlugin(LinkifyPlugin.create())
                .usePlugin(ThemePlugin(view.context))
                .build()


            Napier.i(tag = "HelpDetailFragment", message = "Showing details for: ${args.faqId}")
            var bound = false
            args.faqId?.let { faqId ->
                listOf(
                    faqId,
                    UriLibrary.secureDecode(faqId)
                ).firstNotNullOfOrNull {
                    getFaq(it)?.let { faq ->
                        bindFaq(faq, markwon)
                        bound = true
                    } ?: getBug(it)?.let { bug ->
                        bindBug(bug, markwon)
                        bound = true
                    }
                } ?: showNotAvailable()
            }

            if (bound) {
                TransitionManager.beginDelayedTransition(binding.root)
                binding.progress.isVisible = false
                binding.scrollView.isVisible = true
                binding.content.post {
                    binding.content.setTextIsSelectable(true)
                    binding.content.movementMethod = LinkClickMovementMethod(LinkClickMovementMethod.OpenWithIntentLinkClickedListener(requireActivity()))
                }
            }
        }
    }

    private fun getFaq(faqId: String) = viewModel.faq.firstOrNull { it.id == faqId }

    private fun getBug(bugId: String) = viewModel.knownBugs.firstOrNull { it.id == bugId }

    private fun showNotAvailable() {
        requireOctoActivity().showDialog(
            OctoActivity.Message.DialogMessage(
                text = { getString(R.string.help___content_not_available) },
                positiveAction = {
                    if (isAdded) {
                        findNavController().popBackStack()
                    }
                }
            )
        )
    }

    private fun bindBug(bug: KnownBug, markwon: Markwon) {
        binding.title.text = bug.title
        binding.status.text = getString(R.string.help___status_x, bug.status)
        markwon.setMarkdown(binding.content, bug.content ?: "")
        binding.videoThumbnailContainer.isVisible = false
    }

    private fun bindFaq(faq: Faq, markwon: Markwon) {
        binding.title.text = faq.title
        binding.status.isVisible = false
        markwon.setMarkdown(binding.content, faq.content ?: "")
        if (!faq.youtubeThumbnailUrl.isNullOrBlank() && !faq.youtubeUrl.isNullOrBlank()) {
            Picasso.get().load(faq.youtubeThumbnailUrl).into(binding.videoThumbnail)
            binding.videoThumbnailContainer.setOnClickListener {
                Uri.parse(faq.youtubeUrl).open(requireOctoActivity())
            }
        } else {
            binding.videoThumbnailContainer.isVisible = false
        }
    }

    override fun handleInsets(insets: Rect) {
        binding.statusBarScrim.updateLayoutParams {
            height = insets.top
        }

        if (!binding.videoThumbnailContainer.isVisible) {
            binding.root.updatePadding(top = insets.top)
        }

        binding.root.updatePadding(bottom = insets.bottom)
    }
}