package de.crysxd.octoapp.help.faq

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import de.crysxd.baseui.compose.framework.helpers.ComposeContentWithoutOctoPrint
import de.crysxd.baseui.compose.screens.ComposeScreenFragment
import de.crysxd.baseui.compose.screens.help.HelpLauncherView

class HelpFragment : ComposeScreenFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        ComposeContentWithoutOctoPrint(insets = composeInsets) {
            HelpLauncherView()
        }
}