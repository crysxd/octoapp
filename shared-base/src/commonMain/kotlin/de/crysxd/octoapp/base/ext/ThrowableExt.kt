package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.sharedcommon.exceptions.ProxyException
import de.crysxd.octoapp.sharedcommon.utils.parseHtml


expect fun Throwable.composeErrorMessage(): CharSequence

expect fun Throwable.composeTechnicalErrorMessage(): CharSequence

fun Throwable.composeMessageStack(): CharSequence {
    val messageBuilder = StringBuilder()
    messageBuilder.append("<b>Error ${this::class.simpleName}</b>: ")
    messageBuilder.append(composeTechnicalErrorMessage().htmlify())

    var cause = cause
    while (cause != null) {
        if (cause is ProxyException) {
            cause = cause.original
        }

        messageBuilder.append("<br/><br/>")
        messageBuilder.append("<b>Caused by ${cause::class.simpleName}</b>: ")
        messageBuilder.append(cause.composeTechnicalErrorMessage().htmlify())
        cause = cause.cause
    }

    return messageBuilder.toString().parseHtml()
}

private fun CharSequence.htmlify() = this.replace(Regex("\n"), "<br/>")

internal expect fun Throwable.getStackAddresses(): List<Long>