package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider

class ResetZOffsetUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<ResetZOffsetUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) {
        val engine = printerEngineProvider.printer(instanceId = param.instanceId)
        engine.printerApi.resetZOffset()
    }

    data class Param(
        val instanceId: String,
    )
}