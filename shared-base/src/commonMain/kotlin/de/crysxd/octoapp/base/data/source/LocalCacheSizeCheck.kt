package de.crysxd.octoapp.base.data.source

import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.io.FileManager
import io.github.aakira.napier.Napier

class LocalCacheSizeCheck(
    tag: String,
    val maxSize: () -> Long,
    val files: FileManager.Delegate,
    val index: () -> List<String>,
    val ageForKey: (String) -> Long?,
    val validFile: (String) -> Boolean,
    val removeFromCache: suspend (String) -> Unit,
) {

    private val tag = "$tag/Check"
    private val Long.humanReadable get() = formatAsFileSize()

    suspend fun checkSize() = try {
        Napier.i(tag = tag, message = "Cleaning up cache")
        val initialSize = files.totalSize()

        if (initialSize > maxSize()) {
            Napier.i(tag = tag, message = "Total size exceeds maximum: ${initialSize.humanReadable} / ${maxSize().humanReadable}")
        }

        // Delete files that are incomplete
        files.list().forEach { name ->
            if (!validFile(name)) {
                Napier.i(tag = tag, message = "Deleting incomplete cache: $name")
                removeFromCache(name)
            }
        }

        // Delete files that are not supposed to be there
        // Filter keys that are not only digits
        val keys = index()
        Napier.i(tag = tag, message = "Files: ${files.list()}")
        files.list().forEach { name ->
            val key = name.split(".").firstOrNull()
            if (!keys.contains(key)) {
                Napier.i(tag = tag, message = "Deleting dangling file: $name")
                files.deleteCacheFile(name)
            }
        }

        // Delete files until cache size is below max
        val fileCount = index().size
        for (i in 0..fileCount) {
            if (maxSize() > files.totalSize()) {
                break
            }

            val cacheEntries = keys.mapNotNull { cacheKey ->
                val entry = ageForKey(cacheKey) ?: return@mapNotNull null
                Pair(cacheKey, entry)
            }.sortedBy { (_, age) ->
                age
            }.toMutableList()

            if (cacheEntries.size == 1) {
                Napier.i(tag = tag, message = "Only one cache entry left occupying ${files.totalSize().humanReadable}")
                break
            } else if (cacheEntries.size == 0) {
                Napier.i(tag = tag, message = "Cache is empty")
                break
            } else {
                val oldest = cacheEntries.removeAt(0)
                Napier.i(tag = tag, message = "Removing ${oldest.first}, total=${files.totalSize().humanReadable}, max=${maxSize().humanReadable}")
                removeFromCache(oldest.first)
            }
        }

        Napier.i(tag = tag, message = "Cache cleaned, occupying ${files.totalSize().humanReadable} and ${index().size} files")
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to check cache size", throwable = e)
    }
}