package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.models.printer.PrinterProfile

class JogPrintHeadUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val activateToolUseCase: ActivateToolUseCase,
) : UseCase2<JogPrintHeadUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) {
        val profile = requireNotNull(printerConfigurationRepository.get(param.instanceId)?.activeProfile) { "Missing instance ${param.instanceId}" }
        val axes = profile.axes
        val singleNozzle = profile.extruders.size == 1

        // Activate the tool select by the user in app settings unless there is only a single nozzle,
        // which is the case for a MMU or ERCF. In this case we don't want to activate the tool to job
        // because it will unload and load filament
        if (singleNozzle) {
            logger.i("Skipping tool activation, single nozzle")
        } else {
            activateToolUseCase.execute(ActivateToolUseCase.Params(instanceId = param.instanceId))
        }

        printerEngineProvider.printer(instanceId = param.instanceId).printerApi.executePrintHeadCommand(
            PrintHeadCommand.JogPrintHeadCommand(
                x = param.xDistance * axes.x.multiplier,
                y = param.yDistance * axes.y.multiplier,
                z = param.zDistance * axes.z.multiplier,
                speed = param.speedMmMin
            )
        )
    }

    data class Param(
        val xDistance: Float = 0f,
        val yDistance: Float = 0f,
        val zDistance: Float = 0f,
        val speedMmMin: Int = 4000,
        val instanceId: String
    )

    private val PrinterProfile.Axis.multiplier get() = if (inverted) -1f else 1f
}