package de.crysxd.octoapp.base.models


@kotlinx.serialization.Serializable
data class GcodeLayerInfo(
    val moveCount: Int,
    val zHeight: Float,
    val positionInFile: Int,
    val lengthInFile: Int,
    val positionInCacheFile: Long = 0,
    val lengthInCacheFile: Int = 0,
)