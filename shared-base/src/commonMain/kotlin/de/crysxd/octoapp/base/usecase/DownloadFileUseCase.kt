package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.MediaFileRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.files.FileObject

class DownloadFileUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val mediaFileRepository: MediaFileRepository,
) : UseCase2<DownloadFileUseCase.Params, DownloadFileUseCase.Result>() {

    override suspend fun doExecute(param: Params, logger: Logger): Result {
        val cacheKey = "${param.file.path}@${param.file.date}"

        if (!mediaFileRepository.isCached(cacheKey)) {
            download(param = param, cacheKey = cacheKey)
        }

        return Result(
            localFilePath = mediaFileRepository.prepareFileForPublicSharing(
                key = cacheKey,
                name = param.file.name
            ).toString()
        )
    }

    private suspend fun download(param: Params, cacheKey: String) = printerEngineProvider.printer(instanceId = param.instanceId).filesApi.downloadFile(
        file = param.file,
        progressUpdate = param.progressUpdate,
        consume = { byteReadChannel ->
            mediaFileRepository.addFile(
                key = cacheKey,
                byteReadChannel = byteReadChannel
            )
        }
    )

    data class Params(
        val file: FileObject.File,
        val instanceId: String?,
        val progressUpdate: (Float) -> Unit
    )

    data class Result(
        val localFilePath: String
    )
}