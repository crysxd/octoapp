package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.engine.models.settings.Settings
import kotlinx.serialization.Serializable

@Serializable
data class AppSettings(
    val webcamScaleType: Int? = null,
    val webcamFullscreenScaleType: Int? = null,
    val webcamLastAspectRatio: String? = null,
    val activeWebcamIndex: Int = 0,
    val hiddenTemperatureComponents: Set<String> = emptySet(),
    val defaultPowerDevices: Map<String, String>? = null,
    val selectedTerminalFilters: List<Settings.TerminalFilter>? = null,
    val isStyledTerminal: Boolean? = null,
    val moveZFeedRate: Int? = null,
    val moveXFeedRate: Int? = null,
    val moveYFeedRate: Int? = null,
    val extrudeFeedRate: Int? = 400,
    val activeToolIndex: Int? = 0,
) {
    companion object {
        const val DEFAULT_POWER_DEVICE_PSU = "printerpsu"
        const val DEFAULT_POWER_DEVICE_LIGHT = "light"
        const val DEFAULT_POWER_DEVICE_UNSPECIFIED = "unspecified"
        const val DEFAULT_POWER_DEVICE_VALUE_NONE = "none"
    }
}