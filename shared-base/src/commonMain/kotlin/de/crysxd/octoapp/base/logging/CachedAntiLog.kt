package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.Napier
import io.ktor.utils.io.charsets.Charset
import io.ktor.utils.io.core.toByteArray
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.offsetAt
import okio.Buffer
import okio.FileHandle
import okio.Sink
import okio.buffer
import okio.use
import kotlin.math.roundToLong

object CachedAntiLog : BaseAntiLog() {

    override var minPriority = LogLevel.DEBUG
    override val writeTime = true
    override val writeExceptionAsText = true
    override val name = "CachedAntiLog"
    override val lineLength = Int.MAX_VALUE
    override val timeZone = TimeZone.UTC

    private const val maxLength = 1024 * 1024L
    private var file: FileHandle? = null
    private var sink: Sink? = null
    private val fileMutex = Mutex()
    private val buffer = Buffer()
    private val newLine = "\n".toByteArray()
    private val charset = Charset.forName("UTF-8")

    init {
        buffer.write(createHeader("App started"))

    }

    fun setUpDiskCache() = try {
        runBlocking {
            fileMutex.withLock {
                val ns = SharedCommonInjector.get().fileManager.forNameSpace("logs")
                val name = "app.log"

                file = file ?: ns.writeCacheFile(name)
                sink = sink ?: file?.appendingSink()?.buffer()
                sink?.write(buffer, buffer.size)
                buffer.clear()

                Napier.i(tag = "CachedAntiLog", message = "Writing logs to ${ns.getPath(name)}")
            }
        }
    } catch (e: Exception) {
        Napier.e(tag = name, message = "Failed to set up disk cache", throwable = e)
    }

    override suspend fun writeLine(line: String) = fileMutex.withLock {
        buffer.write(line.toByteArray(charset))
        buffer.write(newLine)
        flushBuffer()
    }

    private fun flushBuffer(forced: Boolean = false) {
        sink.takeIf { buffer.size > 8192 || forced }?.run {
            write(buffer, buffer.size)
            buffer.clear()
            flush()
            ensureSizeLimit()
        }
    }

    override suspend fun writeException(t: Throwable) {
        // Nothing to do
    }

    private fun ensureSizeLimit() {
        // Exceeding limit? Drop first 33%
        file?.takeIf { it.size() > maxLength }?.run {
            val initialSize = size()
            val dropLength = (size() * 0.33).roundToLong()
            Napier.d(tag = "CachedAntiLog", message = "Dropping first ${dropLength.formatAsFileSize()} of log")

            // Create sink for start of file, source for where we want to drop
            val copySink = sink(0).buffer()
            val copySource = source(dropLength).buffer()

            // We drop the first line, it's partial
            val firstLine = copySource.readUtf8Line()?.plus("\n") ?: ""

            // Write head and copy
            val head = createHeader("Dropping first ${dropLength.formatAsFileSize()}")
            val tail = "------- Repositioning file -------\n".toByteArray(charset)
            copySink.write(head)
            copySink.writeAll(copySource)
            copySink.write(tail)
            copySink.flush()
            flush()

            // Resize
            val newSize = initialSize - dropLength - firstLine.length + head.size + tail.size
            sink?.let {
                reposition(it, newSize)
            }
            resize(newSize)
        }
    }

    private fun writeMarker() {
        val tz = TimeZone.currentSystemDefault()
        buffer.write(
            """
            ------
            Exporting ${file?.size()?.formatAsFileSize()} of logs
            Logging in ${timeZone}, system time zone is $tz (${tz.offsetAt(Clock.System.now())} from logs)
            ------
            
            """.trimIndent().toByteArray(charset)
        )

        flushBuffer(forced = true)
    }

    suspend fun writeCache(out: Sink) = fileMutex.withLock {
        writeMarker()

        Napier.w(tag = "CachedAntiLog", message = "Copying cache: ${file?.size()?.formatAsFileSize()}")

        out.buffer().let { buffer ->
            file?.source()?.buffer()?.use { source ->
                buffer.writeAll(source)
            }
            buffer.flush()
        }
    }

    suspend fun getCache(): ByteArray = fileMutex.withLock {
        writeMarker()

        val size = file?.size()?.toInt() ?: 0
        val out = ByteArray(size)

        Napier.i(tag = "CachedAntiLog", message = "Reading cache: ${size.formatAsFileSize()}")

        file?.source()?.buffer()?.use {
            it.readFully(out)
        }
        return out
    }

    fun clear() {
        buffer.clear()
        file?.resize(0)
        sink?.let { file?.reposition(it, 0) }
    }

    private fun createHeader(text: String): ByteArray {
        val mask = "==============="
        val padded = " $text "
        val replace = padded.map { "=" }.joinToString("")

        return """
            
            
            $mask$replace$mask
            $mask$padded$mask
            $mask$replace$mask
            
            Current date: ${Clock.System.now()}
            Timezone: ${TimeZone.currentSystemDefault()}
            
            
        
        """.trimIndent().toByteArray(charset)
    }
}