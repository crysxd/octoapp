package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.models.settings.Settings

class SelectMmu2FilamentUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<SelectMmu2FilamentUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) = with(printerEngineProvider.printer(instanceId = param.instanceId).asOctoPrint()) {
        when (param.plugin) {
            Settings.Mmu.Plugin.Mmu2FilamentSelect -> mmu2FilamentSelectApi.selectChoice(param.choice)
            Settings.Mmu.Plugin.PrusaMmu -> prusaMmuApi.selectChoice(param.choice)
        }
    }

    data class Params internal constructor(
        val choice: Int,
        val toolIndexLabel: Int,
        val plugin: Settings.Mmu.Plugin,
        val instanceId: String,
    ) {
        companion object {
            @Suppress("FunctionName")
            fun Cancel(
                plugin: Settings.Mmu.Plugin,
                instanceId: String,
            ) = Params(
                choice = 5,
                toolIndexLabel = -1,
                plugin = plugin,
                instanceId = instanceId,
            )

            @Suppress("FunctionName")
            fun ToolIndex(
                toolIndex: Int,
                plugin: Settings.Mmu.Plugin,
                instanceId: String,
            ) = when (toolIndex) {
                in 0..4 -> Params(choice = toolIndex, toolIndexLabel = toolIndex + 1, plugin = plugin, instanceId = instanceId)
                5 -> Params(choice = 5, toolIndexLabel = -1, plugin = plugin, instanceId = instanceId) // Cancel
                else -> throw IllegalArgumentException("Unsupported tool index $toolIndex")
            }
        }
    }
}