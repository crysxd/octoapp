package de.crysxd.octoapp.base.data.models

enum class ScaleType {
    @Deprecated("Do not use. Only here for compatibility of ordinal values with ImageView.ScaleType on Android.")
    MATRIX,

    @Deprecated("Do not use. Only here for compatibility of ordinal values with ImageView.ScaleType on Android.")
    FIT_XY,

    @Deprecated("Do not use. Only here for compatibility of ordinal values with ImageView.ScaleType on Android.")
    FIT_START,

    @Deprecated("Do not use. Only here for compatibility of ordinal values with ImageView.ScaleType on Android.")
    FIT_CENTER,

    @Deprecated("Do not use. Only here for compatibility of ordinal values with ImageView.ScaleType on Android.")
    FIT_END,

    @Deprecated("Do not use. Only here for compatibility of ordinal values with ImageView.ScaleType on Android.")
    CENTER,

    CENTER_CROP,
    CENTER_INSIDE,
}