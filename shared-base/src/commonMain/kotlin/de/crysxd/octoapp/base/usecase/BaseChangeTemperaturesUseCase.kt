package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.api.PrinterApi
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize

abstract class BaseChangeTemperaturesUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val getCurrentPrinterProfileUseCase: GetCurrentPrinterProfileUseCase,
) : UseCase2<BaseChangeTemperaturesUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        applyTemperatures(
            printerApi = printerEngineProvider.printer(param.instanceId).printerApi,
            temperatures = param.temps.associate { it.component to (it.temperature ?: 0f) }
        )
    }

    protected abstract suspend fun applyTemperatures(printerApi: PrinterApi, temperatures: Map<String, Float>)

    data class Params(
        val temps: List<Temperature>,
        val instanceId: String? = null,
    ) {
        constructor(temp: Temperature, instanceId: String? = null) : this(listOf(temp), instanceId)
    }

    @CommonParcelize
    data class Temperature(
        val temperature: Float?,
        val component: String
    ) : CommonParcelable
}