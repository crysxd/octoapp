package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.INSTANCE_ID_PATH_PREFIX
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.ktor.http.Url
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

class HandleObicoAppPortalSuccessUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val testNewUrlUseCase: TestNewUrlUseCase,
) : UseCase2<Url, Unit>() {

    companion object {
        private val busyCount = MutableStateFlow(0)
        val loadingFlow = busyCount.map { it > 0 }
    }

    override suspend fun doExecute(param: Url, logger: Logger) = try {
        busyCount.update { it + 1 }

        try {
            //region Extract data from param
            logger.i("Handling connection result for Obico")
            val tunnelUrl = requireNotNull(param.parameters["tunnel_endpoint"]?.toUrlOrNull()) { "Failed to get tunnelUrl" }
            val instanceId = param.pathSegments.firstOrNull { it.startsWith(INSTANCE_ID_PATH_PREFIX) }?.removePrefix(INSTANCE_ID_PATH_PREFIX)
                ?: throw IllegalStateException("No instance id found in URL")
            //endregion
            //region Ensure remote connection is valid
            testNewUrlUseCase.execute(
                param = TestNewUrlUseCase.Params(
                    instanceId = instanceId,
                    newUrl = tunnelUrl,
                    fullTest = false,
                )
            )
            //endregion
            //region Update
            printerConfigurationRepository.update(id = instanceId) {
                it.copy(
                    alternativeWebUrl = tunnelUrl,
                    octoEverywhereConnection = null,
                    remoteConnectionFailure = null,
                    alternativeWebUrlPlugin = OctoPlugins.Obico,
                )
            }
            //endregion
        } catch (e: Exception) {
            OctoAnalytics.logEvent(
                event = OctoAnalytics.Event.ObicoConnectFailed,
                params = mapOf("reason" to e::class.simpleName)
            )
            throw e
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.ObicoConnected)
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.ObicoUser, "true")
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, "spaghetti_detective")
        logger.i("Stored connection info for Obico")
    } finally {
        busyCount.update { it - 1 }
    }
}