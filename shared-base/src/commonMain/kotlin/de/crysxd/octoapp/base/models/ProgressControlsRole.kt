package de.crysxd.octoapp.base.models

enum class ProgressControlsRole {
    ForNormal, ForWebcamFullscreen
}
