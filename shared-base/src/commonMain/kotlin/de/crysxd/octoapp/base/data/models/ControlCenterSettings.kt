package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.Serializable

@Serializable
data class ControlCenterSettings(
    val sortBy: SortBy = SortBy.Color,
) {
    @Serializable
    enum class SortBy {
        Color, Label;
    }
}