package de.crysxd.octoapp.base.models

data class BillingState(
    val products: List<BillingProduct>,
    val purchases: List<BillingPurchase>,
    val isBillingInitialized: Boolean,
    val isBillingAvailable: Boolean = true,
) {
    fun toSimpleString() = "purchases=[${purchases.joinToString()}] purchaseCount=${purchases.size} products=[${products.joinToString { it.productId }}]"
}