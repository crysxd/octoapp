package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.ext.collectSaveWithRetry
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileList
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import okio.FileNotFoundException

class FileListRepository(
    private val printerRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
) {

    private val tag = "FileListRepository"
    private val fileCache = mutableMapOf<FileCacheKey, MutableStateFlow<FlowState<FileObject>>>()
    private val treeCache = mutableMapOf<TreeCacheKey, MutableStateFlow<FlowState<FileList>>>()
    private val collectionJobs = mutableMapOf<String?, Job>()

    init {
        observeAllInstancesForInvalidation()
    }

    fun observeTree(
        instanceId: String,
        fileOrigin: FileOrigin,
    ) = TreeCacheKey(
        instanceId = instanceId,
        fileOrigin = fileOrigin,
    ).let { treeCacheKey ->
        getTreeFlow(
            cacheKey = treeCacheKey
        ).onStart {
            ensureTreeLoaded(cacheKey = treeCacheKey)
        }.onEach { state ->
            if ((state as? FlowState.Error)?.throwable is FileTreeInvalidatedException) {
                Napier.d(tag = tag, message = "File tree on active flow invalidated")
                ensureTreeLoaded(cacheKey = treeCacheKey)
            }
        }.filter {
            // Filter invalidation out, we already triggered reload above
            !it.isInvalidationSignal
        }.onStart {
            emit(FlowState.Loading())
        }.flowOn(Dispatchers.SharedIO)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    fun observePath(
        instanceId: String,
        fileOrigin: FileOrigin,
        path: String,
    ) = FileCacheKey(
        instanceId = instanceId,
        fileOrigin = fileOrigin,
        path = path
    ).let { fileCacheKey ->
        observeTree(
            instanceId = instanceId,
            fileOrigin = fileOrigin,
        ).flatMapLatest { treeState ->
            when (treeState) {
                is FlowState.Error -> flowOf(FlowState.Error(treeState.throwable))
                is FlowState.Loading -> flowOf(FlowState.Loading())
                is FlowState.Ready -> {
                    val parent = path.split("/").dropLast(1).joinToString("/").ifEmpty { "/" }
                    // Prefer from parent in case we already have that loaded
                    // No hit from parent? Use tree
                    // No hit in tree? It's a empty folder in Moonraker...
                    val parenFolder = ((fileCache[fileCacheKey.copy(path = parent)]?.value as? FlowState.Ready)?.data as? FileObject.Folder)
                    val item = parenFolder?.children?.firstOrNull { it.path == path }?.takeIf { ref ->
                        // If the ref is a folder and has children...take them. Empty children might also mean they were not loaded
                        ref !is FileObject.Folder || !ref.children.isNullOrEmpty()
                    } ?: treeState.data.getItemOrNull(path) ?: SimpleFolderReference(fileCacheKey)

                    when (item) {
                        // The tree contains full objects already, no need to load separately (OctoPrint usually)
                        is FileObject.File -> flowOf(FlowState.Ready(item))

                        // The tree contains full objects already, no need to load separately (OctoPrint usually)
                        is FileObject.Folder -> flowOf(FlowState.Ready(item))

                        // The item is a node, so we can create a FileObject.Folder from it (Moonraker usually)
                        is FileReference.Folder -> observeAsFile(fileCacheKey, isFolder = true).onEach { state ->
                            if (state.isInvalidationSignal) {
                                Napier.d(tag = tag, message = "File tree on active flow invalidated")
                                ensureFileLoaded(cacheKey = fileCacheKey, isFolder = true)
                            }
                        }

                        // The item is a leaf, we need to load it from the API (Moonraker usually)
                        is FileReference.File -> observeAsFile(fileCacheKey, isFolder = false).onEach { state ->
                            if (state.isInvalidationSignal) {
                                Napier.d(tag = tag, message = "File tree on active flow invalidated")
                                ensureFileLoaded(cacheKey = fileCacheKey, isFolder = true)
                            }
                        }

                        else -> throw IllegalStateException("${item::class} is not an expected case")
                    }
                }
            }
        }.onStart {
            Napier.i(tag = tag, message = "Started to observe path: $fileCacheKey")
            emit(FlowState.Loading())
        }.onCompletion {
            Napier.i(tag = tag, message = "Stopped to observe path: $fileCacheKey")
        }.filter {
            // Filter invalidation out, we already triggered reload above
            !it.isInvalidationSignal
        }.catch {
            Napier.e(tag = tag, message = "File flow failed", throwable = it)
            emit(FlowState.Error(it))
        }
    }.flowOn(Dispatchers.SharedIO)

    fun invalidate(
        instanceId: String,
        fileOrigin: FileOrigin,
    ) {
        fileCache.keys.filter { key ->
            key.instanceId == instanceId && key.fileOrigin == fileOrigin
        }.forEach { key ->
            fileCache[key]?.value = flowStateFileNotLoadedYet()
        }

        treeCache.keys.filter { key ->
            key.instanceId == instanceId && key.fileOrigin == fileOrigin
        }.forEach { key ->
            treeCache[key]?.value = flowStateTreeNotLoadedYet()
        }
    }

    private fun getFileFlow(cacheKey: FileCacheKey) = fileCache.getOrPut(cacheKey) {
        MutableStateFlow(flowStateFileNotLoadedYet())
    }

    private fun getTreeFlow(cacheKey: TreeCacheKey) = treeCache.getOrPut(cacheKey) {
        MutableStateFlow(flowStateTreeNotLoadedYet())
    }

    private fun observeAsFile(fileCacheKey: FileCacheKey, isFolder: Boolean) = getFileFlow(
        cacheKey = fileCacheKey
    ).onStart {
        Napier.v(tag = tag, message = "Started to observe as file: $fileCacheKey")
        ensureFileLoaded(cacheKey = fileCacheKey, isFolder = isFolder)
    }.onCompletion {
        Napier.v(tag = tag, message = "Stopped to observe as file: $fileCacheKey")
    }.onEach { state ->
        if ((state as? FlowState.Error)?.throwable is FileTreeInvalidatedException) {
            Napier.d(tag = tag, message = "File tree on active flow invalidated")
            ensureFileLoaded(cacheKey = fileCacheKey, isFolder = isFolder)
        }
    }

    private suspend fun ensureTreeLoaded(cacheKey: TreeCacheKey) {
        Napier.v(tag = tag, message = "Ensuring loaded: $cacheKey")
        when (getTreeFlow(cacheKey).value) {
            is FlowState.Error -> loadFileTree(cacheKey)
            is FlowState.Loading -> Unit
            is FlowState.Ready -> Unit
        }
    }

    private suspend fun ensureFileLoaded(cacheKey: FileCacheKey, isFolder: Boolean) {
        Napier.v(tag = tag, message = "Ensuring file loaded: $cacheKey")
        when (getFileFlow(cacheKey).value) {
            is FlowState.Error -> loadFile(cacheKey, isFolder = isFolder)
            is FlowState.Loading -> Unit
            is FlowState.Ready -> Unit
        }
    }

    private suspend fun loadFileTree(
        cacheKey: TreeCacheKey,
    ): FileList? = getTreeFlow(cacheKey).let { flow ->
        try {
            flow.value = FlowState.Loading()
            Napier.v(tag = tag, message = "Loading tree for $cacheKey")
            val printer = printerEngineProvider.printer(instanceId = cacheKey.instanceId)
            val tree = printer.filesApi.getAllFiles(origin = cacheKey.fileOrigin)
            flow.value = FlowState.Ready(tree)
            Napier.d(tag = tag, message = "Loading tree for $cacheKey done")
            return tree
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Loading tree for $cacheKey failed", throwable = e)
            flow.value = FlowState.Error(e)
            return null
        }
    }

    private suspend fun loadFile(
        cacheKey: FileCacheKey,
        isFolder: Boolean
    ) = getFileFlow(cacheKey).let { flow ->
        try {
            flow.value = FlowState.Loading()
            Napier.v(tag = tag, message = "Loading file for $cacheKey (isFolder=$isFolder)")
            val printer = printerEngineProvider.printer(instanceId = cacheKey.instanceId)
            val file = printer.filesApi.getFile(origin = cacheKey.fileOrigin, path = cacheKey.path, isDirectory = isFolder)
            flow.value = FlowState.Ready(file)
            Napier.d(tag = tag, message = "Loading file for $cacheKey done")
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Loading file for $cacheKey failed", throwable = e)
            flow.value = FlowState.Error(e)
        }
    }

    private fun observeAllInstancesForInvalidation() {
        AppScope.launch(Dispatchers.Default) {
            // Keep a tap on the instances and make sure to always passively collect all of them
            printerRepository.allInstanceInformationFlow().collectSaveWithRetry { all ->
                // Cancel outdated ones if a instance is deleted
                collectionJobs.filter { !all.containsKey(it.key) }.forEach {
                    it.value.cancel()
                    collectionJobs.remove(it.key)
                }
                // Start any new jobs
                all.filter { !collectionJobs.containsKey(it.key) }.forEach { collectionJobs[it.key] = collectEventsForInstance(it.value.id) }
            }
        }
    }

    private fun collectEventsForInstance(instanceId: String) = AppScope.launch(Dispatchers.Default) {
        Napier.i(tag = tag, message = "Observing $instanceId for file changes change")
        listOf(
            launch {
                printerEngineProvider.passiveEventFlow(instanceId = instanceId)
                    .mapNotNull { it as? Event.MessageReceived }
                    .filter { it.message is Message.Event.UpdatedFiles }
                    .collectSaveWithRetry { _ ->
                        Napier.i(tag = tag, message = "Invalidating all files for $instanceId")
                        invalidate(
                            instanceId = instanceId,
                            fileOrigin = FileOrigin.Gcode
                        )
                    }
            },
            launch {
                printerEngineProvider.passiveEventFlow(instanceId = instanceId)
                    .mapNotNull { ((it as? Event.MessageReceived)?.message as? Message.Current)?.job?.file?.path }
                    .distinctUntilChanged()
                    .collectSaveWithRetry { path ->
                        ensureFileLoaded(
                            cacheKey = FileCacheKey(
                                instanceId = instanceId,
                                fileOrigin = FileOrigin.Gcode,
                                path = path,
                            ),
                            isFolder = false,
                        )
                    }
            }
        ).joinAll()
    }.also { job ->
        job.invokeOnCompletion {
            Napier.i(tag = tag, message = "Stop observing $instanceId for file changes change ($it)")
        }
    }

    private fun flowStateFileNotLoadedYet() = FlowState.Error<FileObject>(FileTreeInvalidatedException())

    private fun flowStateTreeNotLoadedYet() = FlowState.Error<FileList>(FileTreeInvalidatedException())

    private fun FileList.getItem(path: String) = if (path == "/") {
        files
    } else {
        val pathSegments = path.removePrefix("/").split("/").toMutableList()
        val name = pathSegments.last()
        var currentNode: FileReference.Folder = files

        pathSegments.dropLast(1).forEach { segment ->
            val item = currentNode[segment]
                ?: throw FileNotFoundException("Requested $path, but node ${currentNode.path} has no child named $segment")
            val childNode = (item as? FileReference.Folder)
                ?: throw FileNotFoundException("Requested $path, but node ${item.path} is node a node")
            currentNode = childNode
        }

        currentNode[name]
            ?: throw FileNotFoundException("Requested $path, but node ${currentNode.path} has no child named $name")
    }

    private fun FileList.getItemOrNull(path: String): FileReference? = try {
        getItem(path)
    } catch (e: FileNotFoundException) {
        Napier.w(tag = tag, message = e.message ?: "Unable to find $path")
        null
    }

    private class FileTreeInvalidatedException : SuppressedIllegalStateException("Files were invalidated")

    private val FlowState<*>.isInvalidationSignal get() = this is FlowState.Error && throwable is FileTreeInvalidatedException

    data class FileCacheKey(
        val instanceId: String,
        val fileOrigin: FileOrigin,
        val path: String,
    )

    data class TreeCacheKey(
        val instanceId: String,
        val fileOrigin: FileOrigin,
    )

    @CommonParcelize
    private class SimpleFolderReference(
        override val path: String,
        override val origin: FileOrigin,
        override val id: String,
    ) : FileReference.Folder, CommonParcelable {
        constructor(fileCacheKey: FileCacheKey) : this(
            path = fileCacheKey.path,
            origin = fileCacheKey.fileOrigin,
            id = fileCacheKey.path,
        )

        override val display: String get() = name
        override val children get() = null
    }
}