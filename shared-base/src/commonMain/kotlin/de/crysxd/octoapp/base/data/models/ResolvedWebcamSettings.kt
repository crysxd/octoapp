package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import io.ktor.http.Url

sealed class ResolvedWebcamSettings(val description: String) {
    abstract val webcam: WebcamSettings.Webcam

    data class MjpegSettings(
        val url: Url,
        override val webcam: WebcamSettings.Webcam,
        val extraHeaders: Map<String, String>
    ) : ResolvedWebcamSettings(url.toString())

    data class HlsSettings(
        val url: Url,
        override val webcam: WebcamSettings.Webcam,
        val basicAuth: String?
    ) : ResolvedWebcamSettings(url.toString())

    data class WebPageWebcamSettings(
        val url: Url,
        override val webcam: WebcamSettings.Webcam,
        val extraHeaders: Map<String, String>,
    ) : ResolvedWebcamSettings(url.toString())

    data class RtspSettings(
        val url: String,
        override val webcam: WebcamSettings.Webcam,
        val basicAuth: String?
    ) : ResolvedWebcamSettings(url)

    data class ObicoSettings(
        val webcamIndex: Int,
        override val webcam: WebcamSettings.Webcam
    ) : ResolvedWebcamSettings(
        description = "Obico Camera${" #${webcamIndex + 1}".takeIf { webcamIndex > 0 } ?: ""}"
    )

    data class Unsupported(
        val displayName: String,
        override val webcam: WebcamSettings.Webcam,
    ) : ResolvedWebcamSettings(
        description = "Unsupported webcam: $displayName"
    )
}
