package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.dto.plugins.companion.AppRegistration
import de.crysxd.octoapp.sharedcommon.Platform
import kotlin.time.Duration.Companion.days

class RegisterWithCompanionPluginUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
    private val octoPreferences: OctoPreferences,
    private val getAppLanguageUseCase: GetAppLanguageUseCase,
    private val platform: Platform,
) : UseCase2<RegisterWithCompanionPluginUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        val config = requireNotNull(printerConfigurationRepository.get(param.instanceId)) { "Did not find instance ${param.instanceId}" }
        val octoPrint = printerEngineProvider.printer(instanceId = param.instanceId)

        when {
            !config.settings.isCompanionInstalled() -> logger.i("Companion is not installed")
            octoPreferences.suppressRemoteMessageInitialization -> logger.i("Remote notifications suppressed")
            param.pushToken == null -> logger.i("No push token available")
            else -> {
                logger.i("Companion is installed, registering with OctoPrint...")
                octoPrint.octoAppCompanionApi.registerApp(
                    AppRegistration(
                        fcmToken = param.pushToken,
                        fcmTokenFallback = param.pushTokenFallback,
                        displayName = param.deviceName ?: platform.deviceName,
                        displayDescription = param.deviceDescription,
                        model = platform.deviceModel,
                        instanceId = param.instanceId,
                        appVersion = platform.appVersion,
                        appLanguage = getAppLanguageUseCase.execute().appLanguage,
                        appBuild = platform.appBuild.toLong(),
                        expireInSecs = param.expireInSecs,
                        excludeNotifications = createLayerNotificationsExclude()
                    )
                )
            }
        }
    }

    private fun createLayerNotificationsExclude() = (setOf(1, 3) - octoPreferences.notifyForLayers.toSet()).toList().map {
        "layer_$it"
    } + listOfNotNull(
        "beep".takeUnless { octoPreferences.notifyPrinterBeep },
        "interaction".takeUnless { octoPreferences.notifyForUserInteraction },
    )

    private fun Settings?.isCompanionInstalled() = this?.plugins?.octoAppCompanion != null

    data class Params(
        val instanceId: String,
        val pushToken: String?,
        val pushTokenFallback: String? = null,
        val deviceName: String? = null,
        val deviceDescription: String? = null,
        val expireInSecs: Long? = 14.days.inWholeSeconds,
    )
}