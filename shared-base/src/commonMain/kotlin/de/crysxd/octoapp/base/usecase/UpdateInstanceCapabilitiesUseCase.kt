package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.OctoPushMessaging
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.exceptions.MissingPermissionException
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.url.isBasedOn
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock

class UpdateInstanceCapabilitiesUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val octoPreferences: OctoPreferences,
    private val getAppLanguageUseCase: GetAppLanguageUseCase,
    private val registerWithCompanionPluginUseCase: RegisterWithCompanionPluginUseCase,
) : UseCase2<UpdateInstanceCapabilitiesUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) = withContext(Dispatchers.SharedIO) {
        val instanceToUpdate = printerConfigurationRepository.get(param.instanceId) ?: return@withContext
        logger.i("Updating ${instanceToUpdate.id}")
        val octoPrint = try {
            printerEngineProvider.printer(instanceId = instanceToUpdate.id)
        } catch (e: IllegalStateException) {
            logger.w("Cancelling update, no OctoPrint available")
            return@withContext
        }

        val basedOnPrimary = octoPrint.baseUrl.value.isBasedOn(instanceToUpdate.webUrl)
        val basedOnAlternative = octoPrint.baseUrl.value.isBasedOn(instanceToUpdate.alternativeWebUrl)
        if (!basedOnPrimary && !basedOnAlternative) {
            logger.e("OctoPrint does not match active instance!")
            return@withContext
        }

        // Gather all info in parallel
        val version = async {
            if (param.updateVersion || param.updatePlugins) {
                octoPrint.versionApi.getVersion()
            } else {
                null
            }
        }
        val settings = async {
            if (param.updateSettings) {
                octoPrint.settingsApi.getSettings()
            } else {
                null
            }
        }
        val macros = async {
            if (param.updateSettings) {
                val macros = (octoPrint as? MoonrakerEngine)?.macroApi?.getMacros()
                    ?: settings.await()?.plugins?.octoKlipper?.macros

                // Support case 1362
                if (macros?.any { it.id == "OCTOAPP_DEBUG_DISABLE_MACRO" } == true) {
                    emptyList()
                } else {
                    macros
                }
            } else {
                null
            }
        }
        val commands = async {
            try {
                if (param.updateCommands) {
                    octoPrint.systemApi.getSystemCommands()
                } else {
                    null
                }
            } catch (e: MissingPermissionException) {
                logger.w("Missing SYSTEM permission")
                null
            } catch (e: Exception) {
                logger.e(e)
                null
            }
        }
        val profile = async {
            try {
                if (param.updateProfile) {
                    val profiles = octoPrint.printerProfileApi.getPrinterProfiles().values
                    profiles.firstOrNull { it.current } ?: profiles.firstOrNull { it.default }
                } else {
                    null
                }
            } catch (e: Exception) {
                logger.e(e)
                null
            }
        }
        val systemInfo = async {
            try {
                if (param.updateSystemInfo) {
                    octoPrint.systemApi.getSystemInfo()
                } else {
                    null
                }
            } catch (e: MissingPermissionException) {
                logger.w("Missing SYSTEM permission")
                null
            } catch (e: Exception) {
                logger.e(e)
                null
            }
        }
        val plugins = async {
            try {
                if (param.updatePlugins && octoPrint is OctoPrintEngine) {
                    // Check if we have at least OctoPrint 1.8 or running maintenance
                    val serverVersion = version.await()?.severVersion ?: "1.0.0"
                    if (serverVersion.asVersion() > "1.8.0".asVersion() || serverVersion == "0+unknown") {
                        octoPrint.pluginManagerApi.getSimplePluginInfo()
                    } else {
                        octoPrint.asOctoPrint().pluginManagerApi.getFullPluginInfo().plugins
                            .filter { it.enabled }
                            .associate { it.key to it.version }
                    }
                } else {
                    null
                }
            } catch (e: MissingPermissionException) {
                logger.w("Missing PLUGIN permission, OctoPrint too old for simple check")
                null
            } catch (e: Exception) {
                logger.e(e)
                null
            }?.toMap()
        }

        val settingsResult = settings.await()

        val m115Response = if (settingsResult?.isCompanionInstalled() == true && octoPrint is OctoPrintEngine) {
            octoPrint.asOctoPrint().octoAppCompanionApi.getFirmwareInfo()
        } else {
            null
        }

        val commandsResult = commands.await()?.all
        val profileResult = profile.await()
        val systemInfoResult = systemInfo.await()
        val versionResult = version.await()
        val pluginsResult = plugins.await()
        val macrosResult = macros.await()

        // Only start update after all network requests are done to prevent race conditions
        if (param.updatePlugins || param.updateProfile || param.updateCommands || param.updateSettings || param.updateVersion || param.updateSystemInfo) {
            printerConfigurationRepository.update(instanceToUpdate.id) { current ->
                val updated = current.copy(
                    version = versionResult ?: current.version,
                    m115Response = m115Response ?: current.m115Response,
                    settings = settingsResult ?: current.settings,
                    macros = macrosResult ?: current.macros,
                    systemInfo = systemInfoResult ?: current.systemInfo,
                    activeProfile = profileResult ?: current.activeProfile,
                    systemCommands = commandsResult ?: current.systemCommands,
                    availablePlugins = pluginsResult ?: current.availablePlugins,
                    capabilitiesFetchedAt = Clock.System.now().toEpochMilliseconds(),
                )
                val standardPlugins = OctoConfig.get(OctoConfigField.DefaultPlugins).split(",").map { it.trim() }
                pluginsResult?.filter { !standardPlugins.contains(it.key) }?.forEach {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.PluginDetected(it.key))
                }

                logger.i("Updated capabilities: $updated")
                updated
            }
        }

        // Register with companion
        (settingsResult ?: instanceToUpdate.settings)?.let {
            registerWithCompanionPlugin(logger, instanceToUpdate.id)
        }

        Unit
    }

    private fun Settings.isCompanionInstalled() = plugins.octoAppCompanion != null

    private suspend fun registerWithCompanionPlugin(logger: Logger, instanceId: String) {
        try {
            registerWithCompanionPluginUseCase.execute(
                param = RegisterWithCompanionPluginUseCase.Params(
                    instanceId = instanceId,
                    pushToken = OctoPushMessaging.getPushToken(),
                )
            )
        } catch (e: Exception) {
            logger.e(e)
        }
    }

    data class Params(
        val updateVersion: Boolean = true,
        val updateSettings: Boolean = true,
        val updateCommands: Boolean = true,
        val updateProfile: Boolean = true,
        val updateSystemInfo: Boolean = true,
        val updatePlugins: Boolean = true,
        val instanceId: String? = null
    )
}