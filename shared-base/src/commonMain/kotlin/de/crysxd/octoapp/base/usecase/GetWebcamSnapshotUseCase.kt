package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.OctoPrint
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.forLogging
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image
import de.crysxd.octoapp.sharedexternalapis.mjpeg.JpegCoder
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import io.ktor.client.call.body
import io.ktor.client.plugins.expectSuccess
import io.ktor.client.request.get
import io.ktor.client.request.head
import io.ktor.http.Url
import io.ktor.http.contentType
import io.ktor.util.toByteArray
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.isActive
import okio.IOException
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class GetWebcamSnapshotUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
    private val getActiveHttpUrlUseCase: GetActiveHttpUrlUseCase,
    private val getWebcamSettingsUseCase: GetWebcamSettingsUseCase,
    private val applyWebcamTransformationsUseCase: ApplyWebcamTransformationsUseCase,
    private val handleAutomaticLightEventUseCase: HandleAutomaticLightEventUseCase,
    private val httpSettings: HttpClientSettings,
) : UseCase2<GetWebcamSnapshotUseCase.Params, Flow<GetWebcamSnapshotUseCase.Snapshot>>() {

    override suspend fun doExecute(param: Params, logger: Logger): Flow<Snapshot> {
        var illuminated = false
        return printerConfigurationRepository.instanceInformationFlow(param.instanceId)
            .map { requireNotNull(it) { "OctoPrint not found" } }
            .flatMapLatest { instance -> getActiveHttpUrlUseCase.execute(instance).map { it to instance } }
            .distinctUntilChangedBy { (baseUrl, instanceInfo) -> baseUrl.hashCode() + instanceInfo.settings.hashCode() }
            .flatMapLatest { (baseUrl, instanceInfo) -> getWebcamSettingsUseCase.execute(instanceInfo).map { settings -> Triple(baseUrl, instanceInfo, settings) } }
            .flatMapLatest { (baseUrl, instanceInfo, webcams) ->
                require(webcams.isNotEmpty()) { throw SuppressedIllegalStateException("Webcam not configured, zero settings") }
                logger.d("Creating snapshot stream for input: baseUrl=$baseUrl webcams=$webcams")

                val limitedWebcamIndex = param.webcamIndex.coerceAtMost(webcams.size - 1)

                // Bit weird structure, but allows us to only get it once and only get it if we need it
                // composeOctoPrintSnapshotUrl triggers a network request, so we need to be cautious
                var octoPrintSnapshotUrl: Url? = null
                suspend fun getOctoPrintSnapshotUrl() = octoPrintSnapshotUrl
                    ?: composeOctoPrintSnapshotUrl(logger, baseUrl, limitedWebcamIndex, instanceInfo).also { octoPrintSnapshotUrl = it }

                val companionVersion = instanceInfo.settings?.plugins?.octoAppCompanion?.version
                val mjpegSettings = webcams[limitedWebcamIndex] as? ResolvedWebcamSettings.MjpegSettings
                val octoPrintSnapshotSettings = mjpegSettings?.webcam ?: webcams.firstOrNull()?.webcam

                when {
                    // We prefer to use our plugin
                    companionVersion != null && companionVersion.asVersion() >= "1.0.11".asVersion() && instanceInfo.systemInfo?.interfaceType == OctoPrint -> createCompanionSnapshotFlow(
                        instanceId = instanceInfo.id,
                        octoPrintSnapshotUrl = ::getOctoPrintSnapshotUrl,
                        webcamIndex = limitedWebcamIndex,
                        octoPrintSnapshotSettings = octoPrintSnapshotSettings,
                        mjpegSettings = mjpegSettings,
                        logger = logger,
                        interval = param.interval,
                        maxSize = param.maxSize,
                    )

                    // Second best choice is the snapshot URL
                    getOctoPrintSnapshotUrl() != null -> createOctoPrintSnapshotFlow(
                        octoPrintSnapshotUrl = octoPrintSnapshotUrl!!,
                        octoPrintSnapshotSettings = octoPrintSnapshotSettings,
                        mjpegSettings = mjpegSettings,
                        logger = logger,
                        interval = param.interval,
                        maxSize = param.maxSize,
                    )

                    // Last resort is using MJPEG
                    mjpegSettings != null -> createMjpegOctoPrintSnapshotFlow(
                        mjpegSettings = mjpegSettings,
                        logger = logger,
                        maxSize = param.maxSize,
                        interval = param.interval,
                    )

                    // No snapshot available :(
                    else -> throw WebcamNotSupportedException()
                }
            }.onStart {
                try {
                    if (param.illuminateIfPossible) {
                        illuminated = handleAutomaticLightEventUseCase.executeBlocking(
                            HandleAutomaticLightEventUseCase.Event.WebcamVisible(source = "webcam-snapshot-uc", instanceId = param.instanceId)
                        )
                        // Slight delay so a single snapshot is nicely lit
                        delay(500)
                    }
                } catch (e: CancellationException) {
                    throw e
                } catch (e: Exception) {
                    logger.e("Error while handling automatic", e)
                }
            }.onCompletion {
                try {
                    if (illuminated) {
                        // Execute blocking as a normal execute switches threads causing the task never to be done as the current scope
                        // is about to be terminated
                        handleAutomaticLightEventUseCase.executeBlocking(
                            HandleAutomaticLightEventUseCase.Event.WebcamGone(source = "webcam-snapshot-uc", instanceId = param.instanceId, delayAction = true)
                        )
                    }
                } catch (e: CancellationException) {
                    throw e
                } catch (e: Exception) {
                    logger.e("Error while handling automatic", e)
                }
            }.flowOn(Dispatchers.SharedIO)
    }

    private suspend fun composeOctoPrintSnapshotUrl(
        logger: Logger,
        activeUrl: Url,
        webcamIndex: Int,
        instance: PrinterConfigurationV3
    ): Url? {
        val rawSnapshotUrl = instance.settings?.webcam?.webcams?.get(webcamIndex)?.snapshotUrl ?: run {
            logger.w("No native snapshot URL available for ${instance.id}")
            return null
        }

        // OctoPi default value, reconstruct to a known working value as this one won't do
        val snapshotHttpUrl = if (rawSnapshotUrl == "http://127.0.0.1:8080/?action=snapshot") {
            "http://127.0.0.1/webcam/?action=snapshot".toUrl()
        } else {
            requireNotNull(rawSnapshotUrl.toUrlOrNull() ?: instance.webUrl.resolve(rawSnapshotUrl)) {
                SuppressedIllegalStateException("Unable to build snapshot URL from parts: webUrl='${instance.webUrl.forLogging()}' snapshotUrl='$rawSnapshotUrl'")
            }
        }

        // Replace hosts we can't resolve with something we can
        // Also make sure to carry over the basic auth
        val resolvedHost = when (snapshotHttpUrl.host) {
            "localhost",
            "127.0.0.1" -> Triple(activeUrl.host, activeUrl.user, activeUrl.password)

            instance.webUrl.host -> Triple(activeUrl.host, activeUrl.user, activeUrl.password)
            else -> Triple(snapshotHttpUrl.host, snapshotHttpUrl.user, snapshotHttpUrl.password)
        }

        // Compose URL
        val url = snapshotHttpUrl.withHost(resolvedHost.first).withBasicAuth(user = resolvedHost.second, password = resolvedHost.third)

        // Let's test it!
        logger.i("Composed URL, now testing: $url")
        val success = try {
            createHttpClient(logger)
                .head(url) { expectSuccess = false }
                .status.value
            true
        } catch (e: CancellationException) {
            throw e
        } catch (e: Throwable) {
            if (e is PrinterApiException || e.cause is PrinterApiException) {
                // Some HTTP response is still OK
                true
            } else {
                // Not sure what went wrong
                logger.e("Composed URL failed with exception ${e::class.simpleName}: ${e.message}")
                false
            }
        }

        return if (success) {
            logger.i("Composed URL is fine: $url")
            url
        } else {
            null
        }
    }

    private fun createCompanionSnapshotFlow(
        instanceId: String,
        webcamIndex: Int,
        octoPrintSnapshotUrl: suspend () -> Url?,
        octoPrintSnapshotSettings: WebcamSettings.Webcam?,
        mjpegSettings: ResolvedWebcamSettings.MjpegSettings?,
        logger: Logger,
        interval: Long?,
        maxSize: Int,
    ) = flow {
        val octoPrint = printerEngineProvider.printer(instanceId = instanceId)
        val decoder = JpegCoder(usePool = interval != null, logTag = "JpegDecoder", maxImageSize = maxSize)
        logger.d("Using companion snapshot stream")
        suspend fun emitSnapshot(): Boolean = try {
            logger.v("Loading snapshot")
            val bytes = octoPrint.asOctoPrint().octoAppCompanionApi.getWebcamSnapshot(maxSize = maxSize, webcamIndex = webcamIndex).toByteArray()
            val image = decoder.decode(bytes, bytes.size)

            emit(image)
            true
        } catch (e: PrinterApiException) {
            if (e.responseCode == 406) {
                throw CompanionHasNoSnapshotException()
            } else {
                throw e
            }
        }

        interval?.let {
            while (currentCoroutineContext().isActive) {
                if (emitSnapshot()) {
                    // Success
                    delay(interval)
                } else {
                    // Image was not available, use shortened delay and try again
                    delay(2.seconds)
                }
            }
        } ?: emitSnapshot()
    }.onStart {
        logger.i("Starting companion snapshot flow")
    }.map { image ->
        Snapshot(
            bitmap = image,
            source = SnapshotSource.CompanionPlugin
        )
    }.onCompletion {
        logger.i("Stopping companion snapshot flow")
    }.catch {
        if (it !is CompanionHasNoSnapshotException) {
            logger.e("Failure in companion snapshot flow. Trying to fall back on OctoPrint snapshot flow: ${it::class.simpleName}: ${it.message}")
        } else {
            logger.w("Companion has no snapshots, trying fallbacks...")
        }

        // Can we fallback on the OctoPrint snapshot flow?
        val url = octoPrintSnapshotUrl()

        when {
            url != null && octoPrintSnapshotSettings != null -> {
                logger.i("Can fall back on OctoPrint snapshots, executing fallback")
                emitAll(
                    createOctoPrintSnapshotFlow(
                        logger = logger,
                        octoPrintSnapshotSettings = octoPrintSnapshotSettings,
                        octoPrintSnapshotUrl = url,
                        mjpegSettings = mjpegSettings,
                        interval = interval,
                        maxSize = maxSize,
                    )
                )
            }

            mjpegSettings != null -> {
                logger.e("Can fall back on MJPEG snapshots, executing fallback")
                emitAll(
                    createMjpegOctoPrintSnapshotFlow(
                        logger = logger,
                        mjpegSettings = mjpegSettings,
                        interval = interval,
                        maxSize = maxSize,
                    )
                )
            }

            else -> {
                logger.e("Unable to fall back")
                throw it
            }
        }
    }

    private fun createOctoPrintSnapshotFlow(
        logger: Logger,
        octoPrintSnapshotSettings: WebcamSettings.Webcam?,
        mjpegSettings: ResolvedWebcamSettings.MjpegSettings?,
        octoPrintSnapshotUrl: Url,
        interval: Long?,
        maxSize: Int,
    ) = flow {
        logger.d("Using OctoPrint snapshot stream with: $mjpegSettings")
        val client = createHttpClient(logger)
        val decoder = JpegCoder(usePool = interval != null, logTag = "JpegDecoder", maxImageSize = maxSize)
        suspend fun emitSnapshot() {
            val response = client.get(octoPrintSnapshotUrl)

            if (response.status.value !in 200..299) {
                throw IOException("Received bad status ${response.status}")
            }

            if (response.contentType()?.contentType != "image") {
                throw IOException("Received bad content type ${response.contentType()}")
            }

            val bytes = response.body<ByteArray>()
            if (bytes.isEmpty()) throw IOException("Received image with empty body")
            emit(decoder.decode(bytes, bytes.size))
        }
        interval?.let {
            while (currentCoroutineContext().isActive) {
                emitSnapshot()
                delay(interval)
            }
        } ?: emitSnapshot()
    }.map {
        octoPrintSnapshotSettings?.let { s ->
            applyWebcamTransformationsUseCase.execute(
                ApplyWebcamTransformationsUseCase.Params(
                    frame = it,
                    settings = s
                )
            )
        } ?: it
    }.map { image ->
        Snapshot(
            bitmap = image,
            source = SnapshotSource.OctoPrintSnapshot(url = octoPrintSnapshotUrl)
        )
    }.onStart {
        logger.i("Starting OctoPrint snapshot flow")
    }.onCompletion {
        logger.i("Stopping OctoPrint snapshot flow")
    }.catch {
        logger.e("Error while loading snapshot", it)
        // Can we fallback on the MJPEG flow?
        mjpegSettings?.let { settings ->
            logger.e("Failure in OctoPrint snapshot flow, falling back on MJPEG snapshot flow: ${it::class.simpleName}: ${it.message}")
            emitAll(
                createMjpegOctoPrintSnapshotFlow(
                    logger = logger,
                    mjpegSettings = settings,
                    interval = interval,
                    maxSize = maxSize,
                )
            )
        } ?: throw it
    }

    private fun createMjpegOctoPrintSnapshotFlow(
        logger: Logger,
        mjpegSettings: ResolvedWebcamSettings.MjpegSettings,
        interval: Long?,
        maxSize: Int,
    ) = flow {
        logger.d("Using MJPEG snapshot stream with: $mjpegSettings")
        suspend fun emitSnapshot() {
            val connection = MjpegConnection3(
                streamUrl = mjpegSettings.url,
                name = "snap",
                throwExceptions = true,
                maxSize = maxSize,
                httpSettings = httpSettings.copy(
                    extraHeaders = httpSettings.extraHeaders + mjpegSettings.extraHeaders
                )
            )
            val frame = connection.load().mapNotNull { it as? MjpegConnection3.MjpegSnapshot.Frame }.first()
            emit(frame)
        }

        interval?.let {
            while (currentCoroutineContext().isActive) {
                emitSnapshot()
                delay(interval)
            }
        } ?: emitSnapshot()
    }.map {
        applyWebcamTransformationsUseCase.execute(
            ApplyWebcamTransformationsUseCase.Params(
                frame = it.frame,
                settings = mjpegSettings.webcam
            )
        )
    }.map { image ->
        Snapshot(
            bitmap = image,
            source = SnapshotSource.MjpegSnapshot(url = mjpegSettings.url)
        )
    }.onStart {
        logger.i("Starting MJPEG snapshot flow")
    }.onCompletion {
        logger.i("Stopping MJPEG snapshot flow")
    }

    private fun createHttpClient(logger: Logger) = DefaultHttpClient(
        settings = httpSettings.copy(
            cache = null,
            logTag = logger.httpTag
        ),
    )

    private class CompanionHasNoSnapshotException : SuppressedIllegalStateException("Plugin has no snapshots")

    data class Snapshot(
        val bitmap: Image,
        val source: SnapshotSource,
    )

    class WebcamNotSupportedException : SuppressedIllegalStateException(), UserMessageException {
        override val userMessage = getString("webcam___error_not_supported_for_data_saver")
    }

    sealed class SnapshotSource {
        data object CompanionPlugin : SnapshotSource()
        data class OctoPrintSnapshot(val url: Url) : SnapshotSource()
        data class MjpegSnapshot(val url: Url) : SnapshotSource()
    }

    data class Params(
        val instanceId: String?,
        val webcamIndex: Int = 0,
        val illuminateIfPossible: Boolean = true,
        val interval: Long?,
        val maxSize: Int,
    )
}