package de.crysxd.octoapp.base.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull

// Can't use <Nothing> because of Swift
sealed class FlowState<T> {
    data class Error<T>(val throwable: Throwable) : FlowState<T>()
    data class Ready<T>(val data: T) : FlowState<T>()
    class Loading<T> : FlowState<T>() {
        override fun equals(other: Any?) = other is Loading<*>
        override fun hashCode() = "loading".hashCode()
    }
}

inline fun <reified T, reified U> Flow<FlowState<out T>>.mapData(crossinline mapper: suspend (T) -> U) = map { state ->
    when (state) {
        is FlowState.Error -> FlowState.Error(state.throwable)
        is FlowState.Loading -> FlowState.Loading()
        is FlowState.Ready -> FlowState.Ready(mapper(state.data))
    }
}

inline fun <reified T, reified U> combine(vararg flows: Flow<FlowState<out T>>, crossinline mapper: suspend (List<T>) -> U) = kotlinx.coroutines.flow.combine(
    *flows
) { states ->
    when {
        states.any { it is FlowState.Loading } -> FlowState.Loading()
        states.any { it is FlowState.Error } -> FlowState.Error(states.firstNotNullOf { (it as? FlowState.Error)?.throwable })

        // Implies all Ready
        else -> FlowState.Ready(mapper(states.map { (it as FlowState.Ready).data }))
    }
}

suspend inline fun <reified T> Flow<FlowState<out T>>.awaitItem(): T = mapNotNull { state ->
    when (state) {
        is FlowState.Error -> throw state.throwable
        is FlowState.Loading -> null
        is FlowState.Ready -> state.data
    }
}.first()