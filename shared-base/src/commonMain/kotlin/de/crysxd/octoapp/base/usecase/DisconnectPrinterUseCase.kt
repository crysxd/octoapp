package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.models.commands.ConnectionCommand

class DisconnectPrinterUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<DisconnectPrinterUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) = printerEngineProvider.printer(instanceId = param.instanceId)
        .asOctoPrint().connectionApi.executeConnectionCommand(ConnectionCommand.Disconnect)

    data class Params(
        val instanceId: String,
    )
}