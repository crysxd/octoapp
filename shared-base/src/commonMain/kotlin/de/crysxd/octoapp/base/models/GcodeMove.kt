package de.crysxd.octoapp.base.models

import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

@kotlinx.serialization.Serializable
sealed class GcodeMove {
    abstract val positionInFile: Int

    @kotlinx.serialization.Serializable
    data class Linear(
        val positionInArray: Int,
        override val positionInFile: Int,
    ) : GcodeMove()

    @kotlinx.serialization.Serializable
    data class Arc(
        override val positionInFile: Int,
        val leftX: Float,
        val topY: Float,
        val radius: Float,
        val startAngle: Float,
        val sweepAngle: Float,
    ) : GcodeMove() {

        val endPosition: GcodePoint
            get() {
                val cx = leftX + radius
                val cy = topY + radius
                val angleRad = (startAngle + sweepAngle) * PI / 180f
                val endX = cx + radius * cos(angleRad)
                val endY = cy + radius * sin(angleRad)
                return GcodePoint(endX.toFloat(), endY.toFloat())
            }

    }

    enum class Type {
        Travel, Extrude, Unsupported
    }
}