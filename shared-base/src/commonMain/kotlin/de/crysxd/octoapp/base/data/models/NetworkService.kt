package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.ktor.http.hostWithPort

data class NetworkService(
    val label: String,
    val detailLabel: String,
    val webUrl: String,
    val originQuality: Int,
    val origin: Origin,
    val type: BackendType,
) {
    val id = webUrl.toUrl().hostWithPort.hashCode()

    enum class Origin {
        Upnp,
        DnsSd,
    }
}