package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.JobCommand
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first

class CancelPrintJobUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val setTargetTemperaturesUseCase: SetTargetTemperaturesUseCase,
) : UseCase2<CancelPrintJobUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        OctoAnalytics.logEvent(OctoAnalytics.Event.PrintCancelledByApp)

        // Collect temps
        val current = if (param.restoreTemperatures) {
            logger.i("Capturing active temperature")
            printerEngineProvider.passiveCurrentMessageFlow(instanceId = param.instanceId, tag = "cancel_print_use_case_1").filter { it.temps.isNotEmpty() }.first()
        } else {
            null
        }

        // Issue cancel
        printerEngineProvider.printer(param.instanceId).jobApi.executeJobCommand(JobCommand.CancelJobCommand)

        val temps = current?.temps?.firstOrNull()
        if (param.restoreTemperatures) {
            // Wait for print to be cancelled
            logger.i("Waiting for cancellation")
            printerEngineProvider.passiveCurrentMessageFlow(instanceId = param.instanceId, tag = "cancel_print_use_case_2").filter {
                !it.state.flags.printing
            }.first()

            // Restore temps
            val targets = BaseChangeTemperaturesUseCase.Params(
                instanceId = param.instanceId,
                temps = listOf("tool0", "tool1", "tool2", "tool3", "bed", "chamber").map {
                    BaseChangeTemperaturesUseCase.Temperature(component = it, temperature = temps?.components?.get(it)?.target)
                }
            )
            setTargetTemperaturesUseCase.execute(targets)
        }
    }

    data class Params(
        val restoreTemperatures: Boolean,
        val instanceId: String? = null,
    )
}