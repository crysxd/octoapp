package de.crysxd.octoapp.base.billing

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.get
import de.crysxd.octoapp.base.models.BillingEvent
import de.crysxd.octoapp.base.models.BillingPurchase
import de.crysxd.octoapp.base.models.BillingState
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.io.getSerializable
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

object BillingManager {

    private const val tag = "BillingManager"
    private val core = BillingManagerCore()
    private val internalState by lazy { MutableStateFlow(loadBillingState()) }
    private val internalEvent by lazy { MutableSharedFlow<BillingEvent>(replay = 0) }
    private val settings by lazy { SharedCommonInjector.get().settings.forNameSpace("billing") }
    val state: StateFlow<BillingState> by lazy { internalState.asStateFlow() }
    val events: SharedFlow<BillingEvent> by lazy { internalEvent }
    val purchases: List<String> get() = state.value.purchases.map { it.productId }
    private var resumeJob: Job? = null
    lateinit var adapter: BillingAdapter

    fun isFeatureEnabled(feature: String) = core.isFeatureAvailable(
        purchases = internalState.value.purchases,
        feature = feature,
    )

    fun isFeatureEnabledFlow(feature: String) = internalState.map { state ->
        core.isFeatureAvailable(
            purchases = state.purchases,
            feature = feature,
        )
    }.distinctUntilChanged()

    fun shouldAdvertisePremium(): Boolean = core.shouldAdvertisePremium(
        state = internalState.value
    )


    fun shouldAdvertisePremiumFlow(): Flow<Boolean> = internalState.map { state ->
        core.shouldAdvertisePremium(state = state)
    }

    fun onResume(): Job {
        resumeJob?.cancel("Resuming now")
        return AppScope.launch(Dispatchers.Default) {
            try {
                Napier.i(tag = tag, message = "Resuming billing, checking if available")
                if (adapter.isAvailable()) {
                    Napier.i(tag = tag, message = "Resuming billing, initializing external billing...")
                    adapter.initBilling(purchasesUpdated = ::handlePurchaseUpdate, notifyPurchaseCompleted = ::notifyPurchaseCompleted)
                    Napier.i(tag = tag, message = "Fetching current state...")
                    fetchState()
                    Napier.i(tag = tag, message = "Updating config and fetching state again...")
                    fetchConfig()
                    fetchState()
                    Napier.i(tag = tag, message = "Billing resumed")
                } else {
                    Napier.w(tag = tag, message = "Resuming billing, but not available")
                    internalState.update {
                        it.copy(
                            isBillingAvailable = false,
                            isBillingInitialized = true
                        )
                    }
                }
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to init billing and fetch state, restoring cached state", throwable = e)
                internalState.value = loadBillingState()
            }
        }.also {
            resumeJob = it
        }
    }

    fun onPause() {
        AppScope.launch(Dispatchers.Default) {
            try {
                Napier.i(tag = tag, message = "Pausing billing, destroying external billing...")
                resumeJob?.cancel("Pausing now")
                adapter.destroyBilling()
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to destroy billing", throwable = e)
            }
        }
    }

    private fun notifyPurchaseCompleted() {
        AppScope.launch {
            internalEvent.emit(BillingEvent.PurchaseCompleted)
        }
    }

    private fun handlePurchaseUpdate(purchases: List<BillingPurchase>) = internalState.update { state ->
        Napier.i(tag = tag, message = "Handling purchase update: ${state.purchases.joinToString(",")} -> ${purchases.joinToString(",")}")
        val new = state.copy(purchases = purchases)
        core.ensureGcodePreviewNotHidden(state)
        new
    }

    private suspend fun fetchConfig() {
        Napier.i(tag = tag, message = "Fetching latest config...")
        if (OctoConfig.fetchAndActivate()) {
            Napier.i(tag = tag, message = "Fetched latest config successfully. (premiumFeatures=${OctoConfig.get(OctoConfigField.PremiumFeatures)})")
        } else {
            Napier.w(tag = tag, message = "Failed to fetch latest config. (premiumFeatures=${OctoConfig.get(OctoConfigField.PremiumFeatures)})")
        }
    }

    private suspend fun fetchState() = withContext(Dispatchers.Default) {
        val purchaseOffers = OctoConfig.get(OctoConfigField.PurchaseOffers)
        val specialProductIds = listOf("preview_support_infinite", "preview_support_infinite_sale", "preview_support_1_year", "preview_support_1_month", "tip")
        val transferProductIds = listOf("support_infinitive_transfer", "support_infinite_transfer_2")
        val productIds = listOfNotNull(
            purchaseOffers.subscriptionSku,
            purchaseOffers.purchaseSku,
            transferProductIds,
            specialProductIds,
        ).flatten()
        Napier.i(tag = tag, message = "Fetching state for products: $productIds")
        val products = async { adapter.loadProducts(productIds = productIds) }
        val purchases = async { adapter.loadPurchases() }
        val state = BillingState(
            products = products.await().sortedBy { it.priceOrder },
            purchases = purchases.await(),
            isBillingInitialized = true,
        )

        val hasPremium = core.isFeatureAvailable(feature = FEATURE_QUICK_SWITCH, purchases = state.purchases)
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.PremiumUser, hasPremium.toString())
        Napier.i(tag = tag, message = "State updated: ${state.toSimpleString()} -> hasPremium=$hasPremium")
        Napier.i(tag = tag, message = "Features unlocked: $hasPremium")
        cacheBillingState(state)
        this@BillingManager.internalState.value = state
    }

    private fun cacheBillingState(state: BillingState) {
        Napier.i(tag = tag, message = "Caching billing state: ${state.toSimpleString()}")
        settings.putSerializable("purchases", state.purchases)
    }

    private fun loadBillingState() = BillingState(
        products = emptyList(),
        purchases = settings.getSerializable("purchases", emptyList()),
        isBillingInitialized = false,
    ).also { state ->
        Napier.d(tag = tag, message = "Loading billing state: ${state.toSimpleString()}")
        Napier.i(tag = tag, message = "Billing state with ${state.purchases.size} purchases restored")
    }
}

const val FEATURE_AUTOMATIC_LIGHTS = "auto_lights"
const val FEATURE_QUICK_SWITCH = "quick_switch"
const val FEATURE_GCODE_PREVIEW = "gcode_preview"
const val FEATURE_HLS_WEBCAM = "hls_webcam"
const val FEATURE_INFINITE_WIDGETS = "infinite_app_widgets"
const val FEATURE_FULL_WEBCAM_RESOLUTION = "full_webcam_resolution"
const val FEATURE_FILE_MANAGEMENT = "file_management"
const val FEATURE_WEAR_OS_APP = "wear_os_app"
const val FEATURE_LIVE_ACTIVITY = "live_activity"