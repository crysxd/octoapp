package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.base.exceptions.UnknownHostException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException
import io.ktor.client.call.NoTransformationFoundException
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.channels.ClosedReceiveChannelException

class ExceptionFilter {

    private var last: Throwable? = null

    fun filter(t: Throwable?): Result = when {
        t == null -> Result.Drop
        t == last -> Result.Drop
        t.isAlwaysLogged() -> Result.Log
        t.isDropped() -> Result.Drop
        t.isSuppressed() -> Result.Suppress
        else -> Result.Log
    }.also {
        last = t
    }

    private fun Throwable.isAlwaysLogged(): Boolean =
        this is AlwaysLoggedIllegalStateException

    private fun Throwable.isDropped(): Boolean =
        cause?.isDropped() == true ||
                isPlatformExceptionDropped() ||
                this is SuppressedException ||
                this is ClosedReceiveChannelException ||
                this is CancellationException ||
                this is io.ktor.utils.io.errors.IOException ||
                this is okio.IOException

    private fun Throwable.isSuppressed() = this is NetworkException ||
            this is UnknownHostException ||
            this is SuppressedException ||
            this is PrinterApiException ||
            this is NoTransformationFoundException ||
            this.message?.contains("NSURLErrorDomain") == true ||
            this.message?.contains("x-correlation-id: aaaaaaaa") == true ||
            this.cause?.message?.contains("[{\"error\":{\"type\":1,\"address\":\"/\",\"description\":\"unauthorized user\"}}]") == true ||
            this::class.qualifiedName == "com.google.android.play.core.install.InstallException"

    enum class Result {
        Log, Suppress, Drop
    }
}

class AlwaysLoggedIllegalStateException(message: String, cause: Throwable) : IllegalStateException(message, cause)

expect fun Throwable.isPlatformExceptionDropped(): Boolean