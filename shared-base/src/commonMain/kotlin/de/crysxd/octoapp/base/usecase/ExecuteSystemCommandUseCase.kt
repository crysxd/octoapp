package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.system.SystemCommand

class ExecuteSystemCommandUseCase(private val printerEngineProvider: PrinterEngineProvider) : UseCase2<ExecuteSystemCommandUseCase.Params, Unit>() {
    override suspend fun doExecute(param: Params, logger: Logger) {
        printerEngineProvider.printer(param.instanceId).systemApi.executeSystemCommand(param.systemCommand)
    }

    data class Params(
        val systemCommand: SystemCommand,
        val instanceId: String? = null,
    )
}