package de.crysxd.octoapp.base.usecase

import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant


abstract class UseCase2<Param, Res> {

    companion object {
        private var executionCounter = 0
        private val executionCounterMutex = Mutex()
        private val NoopLogger = object : Logger {
            override val tag = "Noop"
            override fun v(message: String) = Unit
            override fun d(message: String) = Unit
            override fun i(message: String) = Unit
            override fun w(e: Throwable) = Unit
            override fun w(message: String) = Unit
            override fun e(message: String) = Unit
            override fun e(message: String, e: Throwable) = Unit
            override fun e(e: Throwable) = Unit
        }
    }

    protected var suppressLogging = false

    @Throws(Throwable::class)
    suspend fun execute(param: Param): Res = withContext(Dispatchers.Default) {
        internalExecute(param)
    }

    @Throws(Throwable::class)
    fun executeBlocking(param: Param): Res = runBlocking {
        internalExecute(param)
    }

    private suspend fun internalExecute(param: Param): Res {
        val executionId = executionCounterMutex.withLock { executionCounter++ }
        val start = Clock.System.now()
        val name = this@UseCase2::class.simpleName?.take(20)
        val tag = "UC/${name}/$executionId"
        val logger = if (suppressLogging) NoopLogger else TaggedLogger(tag)

        try {
            logger.i("😶️ Executing with param=$param")
            val res = doExecute(param, logger)
            logger.i("🥳 Finished time=${start.millisSince}ms res=$res")
            return res
        } catch (e: CancellationException) {
            logger.w("🥲 Cancelled time=${start.millisSince}ms exception=${e::class.simpleName}")
            throw e
        } catch (e: Exception) {
            logger.e(message = "🤬 Failed time=${start.millisSince}ms exception=${e::class.simpleName}", e = e)
            throw e
        }
    }

    private val Instant.millisSince get() = (Clock.System.now() - this).inWholeMilliseconds

    protected abstract suspend fun doExecute(param: Param, logger: Logger): Res

    interface Logger {
        val tag: String
        fun v(message: String)
        fun d(message: String)
        fun i(message: String)
        fun w(message: String)
        fun w(e: Throwable)
        fun e(message: String)
        fun e(message: String, e: Throwable)
        fun e(e: Throwable)
    }

    protected val Logger.httpTag get() = "HTTP/$tag"

    class TaggedLogger(override val tag: String) : Logger {
        override fun v(message: String) = Napier.v(tag = tag, message = message)
        override fun d(message: String) = Napier.d(tag = tag, message = message)
        override fun i(message: String) = Napier.i(tag = tag, message = message)
        override fun w(message: String) = Napier.w(tag = tag, message = message)
        override fun w(e: Throwable) = Napier.w(tag = tag, message = e.message ?: "", throwable = e)
        override fun e(message: String) = Napier.e(tag = tag, message = message)
        override fun e(message: String, e: Throwable) = Napier.e(tag = tag, message = message, throwable = e)
        override fun e(e: Throwable) = Napier.e(tag = tag, message = e.message ?: "", throwable = e)

    }
}


suspend fun <T> UseCase2<Unit, T>.execute(): T = execute(Unit)