package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.engine.models.power.PowerDevice
import kotlinx.coroutines.delay

class CyclePsuUseCase : UseCase2<PowerDevice, Unit>() {

    override suspend fun doExecute(param: PowerDevice, logger: Logger) {
        param.turnOff()
        delay(2000)
        param.turnOn()
    }
}