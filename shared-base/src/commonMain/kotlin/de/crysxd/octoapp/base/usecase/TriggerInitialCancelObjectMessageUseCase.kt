package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.exceptions.MissingPluginException
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.OctoPlugins


class TriggerInitialCancelObjectMessageUseCase(
    val printerEngineProvider: PrinterEngineProvider,
    val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<TriggerInitialCancelObjectMessageUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        if (printerConfigurationRepository.get(param.instanceId)?.hasPlugin(OctoPlugins.CancelObject) != true) {
            throw MissingPluginException(OctoPlugins.CancelObject)
        }

        printerEngineProvider.printer(param.instanceId).asOctoPrint().cancelObjectApi.triggerInitialMessage()
    }

    data class Params(
        val instanceId: String?
    )
}