package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.Serializable

@Serializable
data class ProgressWidgetSettings(
    val showUsedTime: Boolean = true,
    val showLeftTime: Boolean = true,
    val showThumbnail: Boolean = false,
    val showPrinterMessage: Boolean = false,
    val showLayer: Boolean = false,
    val showSpeed: Boolean = true,
    val showMaxSpeed: Boolean = false,
    val showFlow: Boolean = true,
    val showMaxFlow: Boolean = false,
    val showFilamentUsed: Boolean = true,
    val showZHeight: Boolean = false,
    val etaStyle: EtaStyle = EtaStyle.Compact,
    val printNameStyle: PrintNameStyle = PrintNameStyle.Compact,
    val fontSize: FontSize = FontSize.Normal,
) {
    @Serializable
    enum class FontSize {
        Small, Normal, Large
    }

    @Serializable
    enum class PrintNameStyle {
        None, Compact, Full
    }

    @Serializable
    enum class EtaStyle {
        None, Compact, Full
    }
}