package de.crysxd.octoapp.base.utils

import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.http.framework.withoutQuery

const val PreConfigurationHost = "link.octoapp.eu"

fun urlFromStringInput(input: String): String? {
    val trimmed = input.trim()
    if (trimmed.isBlank()) {
        return null
    }

    // If the URL is pointing to a pre-configuration we do not touch it
    if (trimmed.toUrlOrNull()?.host == PreConfigurationHost) {
        return input
    }

    val upgradedWebUrl = if (!trimmed.startsWith("http://") && !trimmed.startsWith("https://")) {
        "http://${trimmed}"
    } else {
        trimmed
    }
    val loginMarker = "/login/?redirect="
    val withoutLogin = if (upgradedWebUrl.contains(loginMarker)) {
        upgradedWebUrl.take(upgradedWebUrl.indexOf(loginMarker))
    } else {
        upgradedWebUrl
    }
    val result = withoutLogin.toUrlOrNull()?.withoutQuery()?.toString()

    return if (result == "http://localhost") {
        null
    } else {
        result
    }
}