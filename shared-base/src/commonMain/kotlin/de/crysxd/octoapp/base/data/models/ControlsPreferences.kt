package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
data class ControlsPreferences(
    val listId: String,
    val items: List<ControlType> = emptyList(),
    val hidden: List<ControlType> = emptyList(),
) : CommonParcelable {

    constructor(listId: String, list: List<Pair<ControlType, Boolean>>) : this(
        listId,
        list.map { it.first },
        list.mapNotNull { it.first.takeIf { _ -> it.second } },
    )

    fun prepare(list: List<ControlType>) = list.sortedBy { item ->
        items.indexOf(item).takeIf { it >= 0 } ?: Int.MAX_VALUE
    }.associateWith {
        hidden.contains(it)
    }
}
