package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.source.GcodeFileDataSource
import de.crysxd.octoapp.base.data.source.LocalGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteGcodeFileDataSource
import de.crysxd.octoapp.engine.models.files.FileReference
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.retry

class GcodeFileRepository(
    private val localDataSource: LocalGcodeFileDataSource,
    private val remoteDataSource: RemoteGcodeFileDataSource,
) {

    private val tag = "GcodeFileRepository"

    @OptIn(ExperimentalCoroutinesApi::class)
    fun loadFile(file: FileReference.File, allowLargeFileDownloads: Boolean) = flow {
        emit(flowOf(GcodeFileDataSource.LoadState.Loading(0f)))
        if (!localDataSource.canLoadFile(file)) {
            Napier.i(tag = tag, message = "Loading ${file.path} from remote")
            emit(remoteDataSource.loadFile(file, allowLargeFileDownloads))
        } else {
            Napier.i(tag = tag, message = "Loading ${file.path} from local")
            emit(localDataSource.loadFile(file))
        }
    }.flatMapLatest {
        it
    }.retry(1) { e ->
        Napier.w(tag = tag, message = "Failed to load ${file.path}, removing from cache and trying again", throwable = e)
        localDataSource.removeFromCache(file)
        true
    }.catch { e ->
        Napier.e(tag = tag, message = "Failed to load ${file.path} after clearing cache", throwable = e)
        localDataSource.removeFromCache(file)
    }
}