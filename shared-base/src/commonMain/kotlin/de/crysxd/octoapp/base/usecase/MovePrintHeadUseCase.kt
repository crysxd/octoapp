package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.models.printer.PrinterProfile

class MovePrintHeadUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val activateToolUseCase: ActivateToolUseCase,
) : UseCase2<MovePrintHeadUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) {
        val axes = printerConfigurationRepository.get(param.instanceId)?.activeProfile?.axes

        // Activate the tool select by the user in app settings
        activateToolUseCase.execute(ActivateToolUseCase.Params(instanceId = param.instanceId))

        printerEngineProvider.printer(instanceId = param.instanceId).printerApi.executePrintHeadCommand(
            PrintHeadCommand.MovePrintHeadCommand(
                x = param.xPosition * axes?.x.multiplier,
                y = param.yPosition * axes?.y.multiplier,
                z = param.zPosition * axes?.z.multiplier,
                speed = param.speedMmMin
            )
        )
    }

    data class Param(
        val xPosition: Float = 0f,
        val yPosition: Float = 0f,
        val zPosition: Float = 0f,
        val speedMmMin: Int = 4000,
        val instanceId: String
    )

    private val PrinterProfile.Axis?.multiplier get() = if (this?.inverted == true) -1f else 1f
}