package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
data class GcodeHistoryItem(
    val command: String,
    val lastUsed: Long = 0,
    val isFavorite: Boolean = false,
    val usageCount: Int = 0,
    val label: String? = null
) : CommonParcelable {

    val oneLineCommand get() = command.replace("\n", " | ")
    val name get() = label ?: oneLineCommand

}