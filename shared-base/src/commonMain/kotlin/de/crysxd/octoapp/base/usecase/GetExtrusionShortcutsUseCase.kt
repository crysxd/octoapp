package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.ExtrusionHistoryItem
import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class GetExtrusionShortcutsUseCase(
    private val extrusionHistoryRepository: ExtrusionHistoryRepository
) : UseCase2<Unit, Flow<List<ExtrusionHistoryItem>>>() {

    companion object {
        private const val MAX_HISTORY_LENGTH = 5
    }

    override suspend fun doExecute(param: Unit, logger: Logger) = extrusionHistoryRepository.history.map { history ->
        val favorites = history.filter { it.isFavorite }.sortedBy { it.distanceMm }
        val others = history.filter { !it.isFavorite }.sortedByDescending { it.lastUsed }.take(MAX_HISTORY_LENGTH).sortedBy { it.distanceMm }
        listOf(favorites, others).flatten()
    }
}