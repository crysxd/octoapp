package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.data.repository.base.HistoryRepository
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import kotlinx.datetime.Clock
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


class GcodeHistoryRepository(
    settings: SettingsStore,
    json: Json,
) : HistoryRepository<GcodeHistoryItem, String>(
    settings = settings.forNameSpace("gcode-history"),
    defaults = listOf("M500", "G28", "G29").map { GcodeHistoryItem(it, isFavorite = it == "M500") },
    key = "history",
    json = json,
) {

    override val GcodeHistoryItem.order get() = lastUsed
    override val GcodeHistoryItem.id get() = command

    override fun createNew(id: String) = GcodeHistoryItem(command = id)
    override fun Json.serialize(list: List<GcodeHistoryItem>): String = encodeToString(list)
    override fun Json.deserialize(text: String): List<GcodeHistoryItem> = decodeFromString<List<GcodeHistoryItem>>(text).distinctBy { it.command }

    suspend fun setLabelForGcode(command: String, label: String?) = updateHistory(command) {
        it.copy(label = label)
    }

    suspend fun recordGcodeSend(command: String) = updateHistory(command) {
        it.copy(usageCount = it.usageCount + 1, lastUsed = Clock.System.now().toEpochMilliseconds())
    }

    suspend fun setFavorite(command: String, favorite: Boolean) = updateHistory(command) {
        it.copy(isFavorite = favorite)
    }
}