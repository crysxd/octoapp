package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.ext.awaitFileChange
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin

class MoveFileUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val downloadFileUseCase: DownloadFileUseCase,
    private val uploadFileUseCase: UploadFileUseCase,
) : UseCase2<MoveFileUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        if (param.sourceInstanceId == param.destinationInstanceId) {
            copyLocal(
                instanceId = param.sourceInstanceId,
                copyFile = param.copyFile,
                source = param.file,
                destinationPath = param.destinationPath
            )
        } else {
            copyRemote(
                source = requireNotNull(param.file as FileObject.File) { "Can only copy files remotely but ${param.file.path} is a folder" },
                destinationPath = param.destinationPath,
                destinationInstanceId = param.destinationInstanceId,
                sourceInstanceId = param.sourceInstanceId,
                destinationOrigin = param.destinationOrigin,
                progressUpdate = param.progressUpdate
            )
        }

        // Await changes to take affect
        printerEngineProvider.awaitFileChange(instanceId = param.destinationInstanceId)
    }

    private suspend fun copyLocal(
        instanceId: String,
        source: FileObject,
        destinationPath: String,
        copyFile: Boolean,
    ) = printerEngineProvider.printer(instanceId).filesApi.executeFileCommand(
        file = source,
        command = when (copyFile) {
            true -> FileCommand.CopyFile(
                destination = destinationPath
            )

            false -> FileCommand.MoveFile(
                destination = destinationPath
            )
        }
    )

    private suspend fun copyRemote(
        source: FileObject.File,
        destinationPath: String,
        destinationInstanceId: String,
        destinationOrigin: FileOrigin,
        sourceInstanceId: String,
        progressUpdate: (Float) -> Unit,
    ) {
        val localPath = downloadFileUseCase.execute(
            DownloadFileUseCase.Params(
                file = source,
                instanceId = sourceInstanceId,
                progressUpdate = { progressUpdate(it.coerceAtLeast(0f) / 2) }
            )
        ).localFilePath

        uploadFileUseCase.execute(
            UploadFileUseCase.Params(
                instanceId = destinationInstanceId,
                progressUpdate = { progressUpdate(it.coerceAtLeast(0f) / 2 + 0.5f) },
                localPath = localPath,
                parent = FileObject.Folder(
                    path = destinationPath,
                    origin = destinationOrigin,
                    name = destinationPath.split("/").last(),
                )
            )
        )
    }

    data class Params(
        val file: FileObject,
        val destinationPath: String,
        val destinationOrigin: FileOrigin,
        val copyFile: Boolean = false,
        val destinationInstanceId: String,
        val sourceInstanceId: String,
        val progressUpdate: (Float) -> Unit = {},
    )
}