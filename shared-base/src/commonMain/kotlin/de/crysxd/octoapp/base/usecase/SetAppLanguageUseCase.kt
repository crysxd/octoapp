package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences


class SetAppLanguageUseCase(
    private val octoPreferences: OctoPreferences,
) : UseCase2<SetAppLanguageUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) {
        logger.i("Setting language to ${param.language}")
        octoPreferences.appLanguage = param.language

        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.AppLanguage, param.language)
        OctoAnalytics.logEvent(
            OctoAnalytics.Event.AppLanguageChanged,
            mapOf(
                "language" to (param.language ?: "reset")
            )
        )
    }

    data class Param(
        val language: String?,
    )
}