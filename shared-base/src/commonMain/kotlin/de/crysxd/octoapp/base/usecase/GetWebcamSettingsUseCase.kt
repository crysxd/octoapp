package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.Hls
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.Iframe
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.IpCamera
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.JmuxerRaw
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.Mjpeg
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.Rtsp
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.WebRtcCameraStreamer
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.WebRtcGoRtc
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.WebRtcJanus
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type.WebRtcMediaMtx
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.config.hostNameOrIp
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import de.crysxd.octoapp.sharedcommon.http.framework.extractAndRemoveBasicAuth
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import io.ktor.http.HttpHeaders
import io.ktor.http.Url
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class GetWebcamSettingsUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val localDnsResolver: CachedDns,
    private val getActiveHttpUrlUseCase: GetActiveHttpUrlUseCase,
    private val platform: Platform,
) : UseCase2<PrinterConfigurationV3, Flow<List<ResolvedWebcamSettings>>>() {

    companion object {
        private val protocolRegex = Regex("([a-z0-9A-Z]+://)?.+")
    }

    override suspend fun doExecute(param: PrinterConfigurationV3, logger: Logger): Flow<List<ResolvedWebcamSettings>> {
        val activeWebUrlFlow = getActiveHttpUrlUseCase.execute(param)

        // Get settings
        val settingsFlow = printerConfigurationRepository.instanceInformationFlow(param.id)
            .distinctUntilChangedBy { it?.settings }
            .map { it?.settings ?: printerEngineProvider.printer().settingsApi.getSettings() }
            .distinctUntilChanged()

        // Compile webcam settings
        return activeWebUrlFlow.distinctUntilChanged().combine(settingsFlow) { activeWebUrl, settings ->
            logger.d("Compiling webcam settings for $activeWebUrl")

            // Disabled? Empty list
            val needsMultiCamLegacy = settings.webcam.webcams.none { it.provider == "multicam" }
            if (settings.webcam.webcamEnabled) {
                // Add all webcam
                val webcams = listOfNotNull(
                    settings.webcam.webcams,
                    settings.plugins.multiCamSettings?.webcams?.takeIf { needsMultiCamLegacy }
                ).flatten()

                // If active URL is Obico Tunnel, we need to use the dedicated Obico stack
                when {
                    activeWebUrl.isObicoUrl() -> buildObicoSettings(webcams)
                    else -> buildSettings(logger, webcams, activeWebUrl)
                }.also {
                    logger.d("Settings: $it")
                }
            } else {
                logger.d("Webcam disabled")
                emptyList()
            }
        }
    }

    private fun buildObicoSettings(
        webcamSettings: List<WebcamSettings.Webcam>
    ) = webcamSettings.mapIndexed { index, webcam ->
        ResolvedWebcamSettings.ObicoSettings(
            webcam = webcam.copy(displayName = "Obico"),
            webcamIndex = index,
        )
    }.take(1) // Obico only supports one camera with index 0 via the tunnel

    private fun String.isHttpUrl() = lowercase().startsWith("http://") || lowercase().startsWith("https://")

    private suspend fun WebcamSettings.Webcam.resolvedUrl(logger: Logger, activeWebUrl: Url): Pair<String?, String?> = try {
        val httpProtocol = "http://"
        streamUrl?.let { originalUrl ->
            val originalProtocol = protocolRegex.matchEntire(originalUrl)?.groupValues?.getOrNull(1)?.takeIf { it.isNotEmpty() }
            val httpUrl = if (originalProtocol != null) {
                (httpProtocol + originalUrl.removePrefix(originalProtocol)).toUrl()
            } else {
                activeWebUrl.resolve(originalUrl)
            }.run {
                // For Mjpeg we use our own HTTP stack, no need to resolve. For anything else we need to make sure to resolve .local and UPnP addresses
                if (type != Mjpeg) resolveLocalDns() else this
            }
            val (cleanedHttpUrl, basicAuth) = httpUrl.extractAndRemoveBasicAuth()
            val resolvedUrl = if (originalProtocol != null) {
                originalProtocol + cleanedHttpUrl.toString().removePrefix(httpProtocol)
            } else {
                cleanedHttpUrl.toString()
            }
            basicAuth to resolvedUrl
        }
    } catch (e: Exception) {
        logger.e(e)
        streamUrl?.toUrl()?.extractAndRemoveBasicAuth()?.let { it.second to it.first.toString() }
    } ?: (null to null)


    private suspend fun buildSettings(
        logger: Logger,
        webcam: List<WebcamSettings.Webcam>,
        activeWebUrl: Url
    ): List<ResolvedWebcamSettings> {

        val newSettings = webcam.mapNotNull { ws ->
            try {
                //region Resolve Url
                val (basicAuth, resolvedUrl) = ws.resolvedUrl(logger, activeWebUrl)
                logger.d("Resolved URL: ${ws.streamUrl} -> $resolvedUrl (auth=${basicAuth != null})")
                resolvedUrl ?: return@mapNotNull null
                val extraHeaders = listOfNotNull(
                    basicAuth?.let { HttpHeaders.Authorization to it }
                ).toMap()
                //endregion
                //region Map
                when (ws.type) {
                    Hls -> ResolvedWebcamSettings.HlsSettings(url = resolvedUrl.toUrl(), webcam = ws, basicAuth = basicAuth)
                    Mjpeg -> ResolvedWebcamSettings.MjpegSettings(url = resolvedUrl.toUrl(), webcam = ws, extraHeaders = extraHeaders)
                    Rtsp -> ResolvedWebcamSettings.RtspSettings(url = resolvedUrl, webcam = ws, basicAuth = basicAuth)
                    WebRtcCameraStreamer -> ResolvedWebcamSettings.WebPageWebcamSettings(url = resolvedUrl.toUrl(), webcam = ws, extraHeaders = extraHeaders)
                    WebRtcJanus -> ResolvedWebcamSettings.WebPageWebcamSettings(url = resolvedUrl.toUrl(), webcam = ws, extraHeaders = extraHeaders)
                    WebRtcMediaMtx -> ResolvedWebcamSettings.WebPageWebcamSettings(url = resolvedUrl.toUrl(), webcam = ws, extraHeaders = extraHeaders)
                    WebRtcGoRtc -> ResolvedWebcamSettings.WebPageWebcamSettings(url = resolvedUrl.toUrl(), webcam = ws, extraHeaders = extraHeaders)
                    Iframe -> ResolvedWebcamSettings.WebPageWebcamSettings(url = resolvedUrl.toUrl(), webcam = ws, extraHeaders = extraHeaders)
                    JmuxerRaw -> ResolvedWebcamSettings.Unsupported(displayName = "Jmuxer", webcam = ws)
                    IpCamera -> ResolvedWebcamSettings.Unsupported(displayName = "IpCamera", webcam = ws)
                    null -> null
                }
                //endregion
            } catch (e: Exception) {
                logger.e("Failed to resolve URL", e)
                null
            }
        }

        OctoAnalytics.setUserProperty(
            OctoAnalytics.UserProperty.WebCamAvailable,
            when {
                newSettings.any { it is ResolvedWebcamSettings.WebPageWebcamSettings } -> "webrtc"
                newSettings.any { it is ResolvedWebcamSettings.RtspSettings } -> "rtsp"
                newSettings.any { it is ResolvedWebcamSettings.HlsSettings } -> "hls"
                newSettings.any { it is ResolvedWebcamSettings.MjpegSettings } -> "mjpeg"
                else -> "false"
            }
        )

        return newSettings
    }

    private suspend fun Url.resolveLocalDns() = if (platform.isAndroid() && (host.endsWith(".local") || host.startsWith(UPNP_ADDRESS_PREFIX))) {
        withContext(Dispatchers.SharedIO) {
            withHost(localDnsResolver.lookup(host).firstOrNull()?.hostNameOrIp() ?: host)
        }
    } else {
        this
    }
}
