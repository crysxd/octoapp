package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.FileListRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import kotlinx.coroutines.flow.Flow

class LoadFilesUseCase(
    private val fileRepository: FileListRepository,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<LoadFilesUseCase.Params, Flow<FlowState<out FileObject>>>() {

    override suspend fun doExecute(param: Params, logger: Logger): Flow<FlowState<out FileObject>> {
        val instanceId = param.instanceId ?: printerConfigurationRepository.getActiveInstanceSnapshot()?.id ?: throw IllegalStateException("No OctoPrint is active")

        if (param.skipCache) {
            fileRepository.invalidate(
                instanceId = instanceId,
                fileOrigin = param.fileOrigin,
            )
        }

        return fileRepository.observePath(
            instanceId = instanceId,
            fileOrigin = param.fileOrigin,
            path = param.path
        )
    }

    data class Params(
        val fileOrigin: FileOrigin,
        val instanceId: String?,
        val skipCache: Boolean = false,
        val path: String,
    )
}
