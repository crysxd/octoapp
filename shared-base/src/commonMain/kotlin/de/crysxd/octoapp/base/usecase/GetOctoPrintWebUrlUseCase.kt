package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.config.hostNameOrIp
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import de.crysxd.octoapp.sharedcommon.url.isSharedOctoEverywhereUrl
import de.crysxd.octoapp.sharedexternalapis.octoeverywhere.OctoEverywhereAppConnectionApi
import io.ktor.http.Url
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext

class GetOctoPrintWebUrlUseCase(
    private val getActiveHttpUrlUseCase: GetActiveHttpUrlUseCase,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val localDnsResolver: CachedDns,
) : UseCase2<GetOctoPrintWebUrlUseCase.Params, Url>() {

    override suspend fun doExecute(param: Params, logger: Logger): Url {
        val config = requireNotNull(printerConfigurationRepository.get(param.instanceId)) { "Missing config for ${param.instanceId}" }
        val webUrl = getActiveHttpUrlUseCase.execute(config).first()
        val host = webUrl.host
        val resolvedUrl = when {
            host.startsWith(UPNP_ADDRESS_PREFIX) || host.endsWith(".local") -> {
                val resolvedHost = withContext(Dispatchers.SharedIO) { localDnsResolver.lookup(host).first() }
                webUrl.withHost(resolvedHost.hostNameOrIp())
            }

            webUrl.isOctoEverywhereUrl() && !webUrl.isSharedOctoEverywhereUrl() -> {
                val instance = printerConfigurationRepository.get(param.instanceId)
                val connection = requireNotNull(instance?.octoEverywhereConnection) { "No OctoEverywhere connection for ${param.instanceId}" }
                val api = OctoEverywhereAppConnectionApi(appToken = connection.apiToken)
                Url(requireNotNull(api.getWebUrl()) { "Didn't receive URL" })
            }

            else -> {
                webUrl
            }
        }

        return resolvedUrl
    }

    data class Params(
        val instanceId: String? = null,
    )
}