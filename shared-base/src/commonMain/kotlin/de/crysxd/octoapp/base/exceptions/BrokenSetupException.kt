package de.crysxd.octoapp.base.exceptions

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException

class BrokenSetupException(
    val original: NetworkException,
    override val userMessage: String,
    val needsRepair: Boolean,
    val instance: PrinterConfigurationV3,
) : Exception("Broken setup: ${original.message}", original), UserMessageException, SuppressedException