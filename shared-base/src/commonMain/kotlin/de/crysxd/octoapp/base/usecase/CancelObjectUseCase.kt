package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.exceptions.MissingPluginException
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine

class CancelObjectUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<CancelObjectUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        when (val engine = printerEngineProvider.printer(param.instanceId)) {
            is OctoPrintEngine -> {
                if (printerConfigurationRepository.get(param.instanceId)?.hasPlugin(OctoPlugins.CancelObject) != true) {
                    throw MissingPluginException(OctoPlugins.CancelObject)
                }

                // Cancel object and then trigger initial message so all clients get a message about it
                engine.cancelObjectApi.let {
                    it.cancelObject(param.objectId.toInt())
                    it.triggerInitialMessage()
                }
            }

            is MoonrakerEngine -> engine.printerApi.executeGcodeCommand(
                cmd = GcodeCommand.Single(
                    command = "EXCLUDE_OBJECT NAME=${param.objectId}"
                )
            )
        }
    }

    data class Params(
        val instanceId: String?,
        val objectId: String,
    )
}