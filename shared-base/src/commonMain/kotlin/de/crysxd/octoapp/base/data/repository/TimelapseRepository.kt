package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseStatus
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.datetime.Instant

@OptIn(ExperimentalCoroutinesApi::class)
class TimelapseRepository(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) {

    private val tag = "TimelapseRepository2"
    private val flows = mutableMapOf<String, MutableStateFlow<TimelapseStatus?>>()

    init {
        AppScope.launch(Dispatchers.Default) {
            Napier.i(tag = tag, message = "Collecting time")

            printerConfigurationRepository.allInstanceInformationFlow()
                .distinctUntilChangedBy { it.keys }
                .flatMapLatest { instances ->
                    instances.map {
                        printerEngineProvider
                            .passiveEventFlow(instanceId = it.key)
                            .extractTimelapseEvents(instanceId = it.key)
                    }.merge()
                }.retry {
                    Napier.e(tag = tag, message = "Exception in timelapse event flow", throwable = it)
                    delay(1000)
                    true
                }.collect()
        }
    }

    private fun Flow<Event>.extractTimelapseEvents(instanceId: String): Flow<Event> = onEach {
        if (it is Event.MessageReceived && (it.message is Message.Event.MovieFailed || it.message is Message.Event.MovieDone || it.message is Message.Event.MovieRendering)) {
            try {
                fetchLatest(instanceId)
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to update timelapse status", throwable = e)
            }
            getFlow(instanceId).value = printerEngineProvider.printer(instanceId = instanceId).timelapseApi.getStatus()
        }
    }

    suspend fun updateConfig(instanceId: String, block: TimelapseConfig.() -> TimelapseConfig) {
        val flow = getFlow(instanceId)
        val current = flow.value ?: fetchLatest(instanceId)
        val new = block(current.config)
        Napier.d(tag = tag, message = "Updating timelapse config $current -> $new")
        flow.value = printerEngineProvider.printer().timelapseApi.updateConfig(new)
    }


    suspend fun fetchLatest(instanceId: String): TimelapseStatus {
        val value = printerEngineProvider.printer(instanceId = instanceId).timelapseApi.getStatus()
        getFlow(instanceId).value = withContext(Dispatchers.Default) {
            value.copy(
                unrendered = value.unrendered.sortedByDescending { it.date ?: Instant.DISTANT_PAST },
                files = value.files.sortedByDescending { it.date ?: Instant.DISTANT_PAST },
            )
        }
        return value
    }

    fun getState(instanceId: String) = getFlow(instanceId).asStateFlow()

    private fun getFlow(instanceId: String) = flows.getOrPut(instanceId) { MutableStateFlow(null) }

}