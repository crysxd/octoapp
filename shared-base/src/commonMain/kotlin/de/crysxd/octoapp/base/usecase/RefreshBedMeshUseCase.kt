package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.GcodeCommand

class RefreshBedMeshUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<RefreshBedMeshUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) {
        val settings = printerConfigurationRepository.get(id = param.instanceId)?.settings
        val command = requireNotNull(settings?.bedMesh?.refreshCommand) { "Command to redfresh mesh not available" }
        val octoPrint = printerEngineProvider.printer(instanceId = param.instanceId)
        octoPrint.printerApi.executeGcodeCommand(GcodeCommand.Batch(commands = command.split("\n")))
    }

    data class Param(
        val instanceId: String,
    )
}