package de.crysxd.octoapp.base.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException


class MissingPluginException(pluginId: String) : SuppressedIllegalStateException("Missing plugin '$pluginId'")