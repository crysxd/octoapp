package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import io.ktor.http.encodeURLQueryComponent
import kotlinx.coroutines.withTimeoutOrNull
import kotlin.time.Duration.Companion.seconds


class GetRemoteServiceConnectUrlUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<GetRemoteServiceConnectUrlUseCase.Params, GetRemoteServiceConnectUrlUseCase.Result>() {

    companion object {
        const val OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH = "connect-octoeverywhere"
        const val OBICO_APP_PORTAL_CALLBACK_PATH = "connect-spaghetti-detective"
        const val INSTANCE_ID_PATH_PREFIX = "instance-"
    }

    override suspend fun doExecute(param: Params, logger: Logger) = try {
        val service = param.remoteService
        val engine = printerEngineProvider.printer(instanceId = param.instanceId)
        val printerId = withTimeoutOrNull(5.seconds) { service.getPrinterId(engine) }
        val callbackPath = service.getCallbackPath(param.instanceId)
        logger.w("Printer id $printerId")
        logger.w("callback $callbackPath")
        val url = service.getConnectUrl(engine)
            .replace("{{{printerid}}}", (printerId ?: "").encodeURLQueryComponent(encodeFull = true))
            .replace("{{{callbackPath}}}", callbackPath.encodeURLQueryComponent(encodeFull = true))
        logger.w("Url $url")
        service.recordStartEvent()
        Result.Success(url)
    } catch (e: Exception) {
        Result.Error(e)
    }

    class OctoEverywhereNotInstalledException : SuppressedIllegalStateException("OctoEverywhere not installed"), UserMessageException {
        override val userMessage = getString("configure_remote_acces___octoeverywhere___error_install_plugin")
    }

    sealed class RemoteService {
        abstract suspend fun getPrinterId(engine: PrinterEngine): String?
        abstract fun getConnectUrl(engine: PrinterEngine): String
        abstract fun getCallbackPath(instanceId: String): String
        abstract fun recordStartEvent()

        data object OctoEverywhere : RemoteService() {
            private const val tag = "OctoEverywhere"
            override fun getConnectUrl(engine: PrinterEngine) = OctoConfig.get(OctoConfigField.OctoEverywhereAppPortalUrl).let { url ->
                if (engine is MoonrakerEngine) "$url&moonraker=1" else url
            }

            override fun getCallbackPath(instanceId: String) = listOf(OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH, "$INSTANCE_ID_PATH_PREFIX$instanceId").joinToString("/")
            override fun recordStartEvent() = OctoAnalytics.logEvent(OctoAnalytics.Event.OctoEverywhereConnectStarted)
            override suspend fun getPrinterId(engine: PrinterEngine): String? = try {
                engine.octoEverywhereApi.getInfo().printerId
            } catch (e: PrinterApiException) {
                if (engine is OctoPrintEngine && (e.responseCode == 400 || e.responseCode == 404)) {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.OctoEverywherePluginMissing)
                    throw OctoEverywhereNotInstalledException()
                } else {
                    Napier.e(tag = tag, throwable = e, message = "Unable to determine printer ID")
                    null
                }
            } catch (e: Exception) {
                Napier.e(tag = tag, throwable = e, message = "Unable to determine printer ID")
                null
            }
        }

        data class Obico(val baseUrl: Url?) : RemoteService() {
            private val tag = "Obico"

            override suspend fun getPrinterId(engine: PrinterEngine): String? = try {
                engine.obicoApi.getPluginStatus().printerId
            } catch (e: Exception) {
                if (engine is OctoPrintEngine) {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.ObicoPluginMissing)
                }

                Napier.e(tag = tag, throwable = e, message = "Unable to determine printer ID")
                null
            }

            override fun recordStartEvent() = OctoAnalytics.logEvent(OctoAnalytics.Event.ObicoConnectStarted)
            override fun getCallbackPath(instanceId: String) =
                listOf(OBICO_APP_PORTAL_CALLBACK_PATH, "$INSTANCE_ID_PATH_PREFIX$instanceId").joinToString("/")

            override fun getConnectUrl(engine: PrinterEngine): String =
                (baseUrl?.toString() ?: "https://app.obico.io/").removeSuffix("/") + "/" + OctoConfig.get(OctoConfigField.ObicoAppPortalUrl)
        }
    }

    data class Params(
        val remoteService: RemoteService,
        val instanceId: String,
    )

    sealed class Result {
        data class Error(val exception: Exception) : Result()
        data class Success(val url: String) : Result()
    }
}