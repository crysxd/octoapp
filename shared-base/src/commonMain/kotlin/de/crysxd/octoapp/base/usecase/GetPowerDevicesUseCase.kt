package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeoutOrNull
import kotlin.time.Duration.Companion.seconds

@Suppress("EXPERIMENTAL_API_USAGE")
class GetPowerDevicesUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<GetPowerDevicesUseCase.Params, GetPowerDevicesUseCase.Result>() {

    override suspend fun doExecute(param: Params, logger: Logger): Result = withContext(Dispatchers.SharedIO) {
        val instance = printerConfigurationRepository.get(param.instanceId)
        val settings = instance?.settings ?: if (param.allowNetwork) {
            printerEngineProvider.printer(param.instanceId).settingsApi.getSettings()
        } else {
            logger.e("Not allowed to use network but settings not available")
            return@withContext Result(emptyList())
        }

        // We request the devices from the API but this should not result in a network request. We use a super
        // tight timeout that in case we do have a network request this results in an error
        val timeout = 5.seconds
        val devices = withTimeoutOrNull(timeout) {
            printerEngineProvider.printer(param.instanceId).powerDevicesApi.getDevices(
                settings = settings,
                availablePlugins = instance?.availablePlugins
            )
        } ?: throw SuppressedIllegalStateException("Trying to get devices took more than $timeout, assuming network activity was made.")

        // Emit without power state
        devices
            .filter { param.onlyGetDeviceWithUniqueId == null || param.onlyGetDeviceWithUniqueId == it.uniqueId }
            .filter { it.capabilities.containsAll(param.requiredCapabilities) }
            .sortedBy { it.displayName + it.uniqueId }
            .map { device ->
                Result.Item(
                    device = device,
                    state = device.createFlowPromise(logger)
                )
            }
            .let { Result(it) }
    }

    private fun PowerDevice.createFlowPromise(logger: Logger): () -> Flow<PowerState> = {
        when {
            PowerDevice.ControlMethod.RgbwColor in controlMethods -> colorFlow().map { colors ->
                colors ?: return@map PowerState.Unknown
                PowerState.Color(colors)
            }

            PowerDevice.ControlMethod.Pwm in controlMethods -> pwmDutyCycleFlow().map { percent ->
                percent ?: return@map PowerState.Unknown
                PowerState.Percentage(percent = percent)
            }

            else -> isOnFlow().map {
                when (it) {
                    true -> PowerState.On
                    false -> PowerState.Off
                    null -> PowerState.Unknown
                }
            }
        }.catch { e ->
            emit(PowerState.Unknown)
            logger.e(e)
        }.flowOn(Dispatchers.SharedIO)
    }

    data class Params(
        val instanceId: String? = null,
        val allowNetwork: Boolean = true,
        val onlyGetDeviceWithUniqueId: String? = null,
        val requiredCapabilities: List<PowerDevice.Capability> = emptyList()
    )

    data class Result(
        val results: List<Item>
    ) {
        data class Item(
            val device: PowerDevice,
            val state: () -> Flow<PowerState>
        ) {
            suspend fun queryState() = state().first()
        }
    }

    sealed class PowerState(val isOn: Boolean) : CommonParcelable {

        @CommonParcelize
        data object On : PowerState(isOn = true)

        @CommonParcelize
        data object Off : PowerState(isOn = false)

        @CommonParcelize
        data class Percentage(val percent: Float) : PowerState(isOn = percent > 0)

        @CommonParcelize
        data class Color(val colors: List<PowerDevice.RgbwColor>) : PowerState(isOn = colors.firstOrNull()?.run { r > 0 || g > 0 || b > 0 || w > 0 } == true)

        @CommonParcelize
        data object Unknown : PowerState(isOn = false)
    }
}
