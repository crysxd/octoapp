package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.NetworkService
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.base.network.NetworkServiceDiscovery
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DiscoverOctoPrintUseCase(
    private val sensitiveDataMask: SensitiveDataMask,
    private val testFullNetworkStackUseCase: TestFullNetworkStackUseCase,
    private val networkServiceDiscovery: NetworkServiceDiscovery,
) : UseCase2<Unit, Flow<DiscoverOctoPrintUseCase.Result>>() {

    companion object {
        var suppressDiscovery = false
    }

    override suspend fun doExecute(param: Unit, logger: Logger): Flow<Result> = withContext(Dispatchers.SharedIO) {
        val flow = MutableStateFlow(Result(emptyList()))
        val discoveredInstances = mutableListOf<NetworkService>()
        val submitResult: (NetworkService) -> Unit = { discovered ->
            if (!discoveredInstances.any { it.webUrl == discovered.webUrl }) {
                discoveredInstances.add(discovered)
                val uniqueDevices = discoveredInstances.groupBy { it.id }.values.mapNotNull {
                    it.maxByOrNull { i -> i.originQuality }
                }.sortedBy { it.label }

                if (suppressDiscovery) {
                    logger.w("OctoPrint discovery SUPPRESSED")
                } else {
                    flow.value = Result(uniqueDevices)
                }
            }
        }

        // Start discovery and return results
        val coroutineJob = SupervisorJob()
        val coroutineScope = CoroutineScope(coroutineJob + Dispatchers.Main.immediate + CoroutineExceptionHandler { _, t -> logger.e(t) })
        return@withContext flow.onStart {
            emit(Result(emptyList()))
            logger.i("Starting OctoPrint discovery")
            coroutineScope.launch {
                networkServiceDiscovery.discover { service ->
                    coroutineScope.launch {
                        testDiscoveredInstanceAndPublishResult(logger, service, submitResult)
                    }
                }
            }
        }.onCompletion {
            logger.i("Finishing OctoPrint discovery")
            coroutineJob.cancel()
            coroutineScope.cancel()
        }
    }

    private suspend fun testDiscoveredInstanceAndPublishResult(logger: Logger, service: NetworkService, submitResult: (NetworkService) -> Unit) {
        sensitiveDataMask.registerWebUrl(service.webUrl.toUrl())
        logger.i("Probing for '${service.label}' at ${service.webUrl} using ${service.origin} (${service.id})")
        try {
            val result = testFullNetworkStackUseCase.execute(
                TestFullNetworkStackUseCase.Target.Printer(
                    webUrl = service.webUrl,
                    apiKey = ""
                )
            )

            when (result) {
                is TestFullNetworkStackUseCase.Finding.OctoPrintReady,
                is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> {
                    logger.i("Probe for '${service.label}' at ${service.webUrl} was SUCCESS 🥳")
                    submitResult(service)
                }
                else -> logger.i("Probe for '${service.label}' at ${service.webUrl} was FAILURE 😭 (finding=$result)")
            }
        } catch (e: Exception) {
            logger.i("Probe for '${service.label}' at ${service.webUrl} was FAILURE because of an error 😭 (error=${e.message})")
        }
    }

    data class Result(
        val services: List<NetworkService>
    )
}