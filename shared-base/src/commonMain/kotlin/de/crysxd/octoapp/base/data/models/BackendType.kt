package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class BackendType {
    @SerialName("octoprint")
    OctoPrint,

    @SerialName("moonraker")
    Moonraker
}