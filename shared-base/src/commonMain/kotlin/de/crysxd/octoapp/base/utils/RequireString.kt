package de.crysxd.octoapp.base.utils

import de.crysxd.octoapp.sharedcommon.utils.getString

fun requireString(id: String) = requireNotNull(getString(id).takeUnless { it == id }) { "String for key $id was not found!" }