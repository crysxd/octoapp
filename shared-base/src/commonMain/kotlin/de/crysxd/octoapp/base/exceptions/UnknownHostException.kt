package de.crysxd.octoapp.base.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import io.ktor.utils.io.errors.IOException

class UnknownHostException(host: String) : IOException("Unable to resolve $host"), UserMessageException {
    override val userMessage: String = "Failed to resolve $host, check your network"
}