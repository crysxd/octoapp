package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import io.github.aakira.napier.Napier
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retryWhen
import kotlinx.coroutines.flow.sample
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@OptIn(FlowPreview::class)
fun <T> Flow<T>.rateLimit(rateMs: Long, debounce: Boolean = false): Flow<T> = flow {
    emit(this@rateLimit.first())
    if (debounce) {
        emitAll(debounce(rateMs.milliseconds))
    } else {
        emitAll(sample(rateMs.milliseconds))
    }
}


inline fun <reified T : Message> Flow<Event>.filterEventsForMessageType() = mapNotNull { (it as? Event.MessageReceived)?.message as? T }

suspend fun <T> Flow<T>.collectSaveWithRetry(block: suspend (T) -> Unit) {
    onEach {
        block(it)
    }.retryWhen { cause, attempt ->
        Napier.e(tag = "FlowExt.collectSaveWithRetry", throwable = cause, message = "Error in attempt $attempt")
        delay((1.seconds * attempt.toInt()).coerceAtMost(20.seconds))
        true
    }.catch {
        // Nothing
        Napier.e(tag = "FlowExt.collectSaveWithRetry", throwable = it, message = "Uncontained exception")
    }.collect()
}