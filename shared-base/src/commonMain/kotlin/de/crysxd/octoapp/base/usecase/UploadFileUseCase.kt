package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.io.DefaultFileSystem
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.ktor.client.request.forms.InputProvider
import okio.FileSystem

class UploadFileUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val platform: Platform,
    private val fileSystem: FileSystem = DefaultFileSystem,
) : UseCase2<UploadFileUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) = try {
        UploadFileHandle(
            path = param.localPath,
            platform = platform,
            fileSystem = fileSystem
        ).let { file ->
            checkValidFile(param, file)
            logger.i("File passed checks")
            upload(param, file, logger)
            logger.i("Upload complete")
        }
    } finally {
        param.progressUpdate(1f)
    }

    private fun checkValidFile(
        param: Params,
        file: UploadFileHandle,
    ) = when {
        SharedBaseInjector.get().printerConfigRepository.get(param.instanceId)?.hasPlugin(OctoPlugins.UploadAnything) == true -> null
        else -> listOf("g", "gco", "gcode", "ufp")
    }?.let { validExtensions ->
        if (validExtensions.none { ext -> file.name.endsWith(".$ext") }) {
            throw InvalidFileException(path = param.localPath, name = file.name, validExtensions = validExtensions)
        }
    }

    private suspend fun upload(param: Params, file: UploadFileHandle, logger: Logger) = file.withInput { input ->
        logger.d("Upload block start for ${file.name}")

        printerEngineProvider.printer(param.instanceId).filesApi.uploadFile(
            input = input,
            name = file.name,
            progressUpdate = param.progressUpdate,
            parent = param.parent,
        )

        logger.d("Upload block end")
    }

    class InvalidFileException(path: String, name: String, validExtensions: List<String>) :
        SuppressedIllegalStateException("File at $path ($name) is not a valid file: ${validExtensions.joinToString()}"),
        UserMessageException {
        override val userMessage = getString("file_manager___upload___error_invalid_file_type", validExtensions.joinToString())
    }

    data class Params(
        val localPath: String,
        val parent: FileObject.Folder,
        val instanceId: String,
        val progressUpdate: (Float) -> Unit,
    )

    internal data class UploadFileHandle(
        val name: String,
        val withInput: suspend (suspend (InputProvider) -> Unit) -> Unit,
    )
}

internal expect fun UploadFileHandle(path: String, platform: Platform, fileSystem: FileSystem): UploadFileUseCase.UploadFileHandle