package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase.UiState
import de.crysxd.octoapp.engine.asMoonraker
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import de.crysxd.octoapp.sharedcommon.http.framework.withoutQuery
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.isActive
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class ConnectPrinterUseCaseMoonrakerCore(
    private val octoPreferences: OctoPreferences,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
) : ConnectPrinterUseCase.Core {

    private val tag = "ConnectPrinterUseCase/Moonraker"
    private val notAvailableTimeout = 10.seconds
    private val instanceIdFlow = MutableStateFlow<String?>(null)
    private val interfaceLabel get() = printerConfigurationRepository.get(instanceIdFlow.value)?.systemInfo?.interfaceType.label
    private val instanceName get() = printerConfigurationRepository.get(instanceIdFlow.value)?.label ?: interfaceLabel
    private val engineFlow = instanceIdFlow.filterNotNull().flatMapLatest { printerEngineProvider.printerFlow(instanceId = it) }
    private val printerStateFlow = engineFlow.filterNotNull().flatMapLatest { engine ->
        flow {
            emit(null to null)
            var lastState: PrinterState.State? = null
            var error503Counter = 0

            while (currentCoroutineContext().isActive) {
                try {
                    //region Get state and emit
                    lastState = engine.asMoonraker().printerApi.getPrinterState().state
                    error503Counter = 0
                    emit(lastState to null)
                    //endregion
                } catch (e: CancellationException) {
                    throw e
                } catch (e: PrinterApiException) {
                    //region Check for error code, allow up to 3 503 errors
                    if (e.responseCode == 503 && lastState != null && error503Counter < 3) {
                        // 503 may be emitted briefly after a firmware restart. Accept up to three 503 responses
                        emit(lastState to null)
                        Napier.w(tag = tag, message = "Suppressing 503 state (error503Counter=$error503Counter, message=${e.body})")
                        error503Counter++
                    } else {
                        emit(null to e)
                    }
                    //endregion
                } catch (e: Exception) {
                    //region Emit error
                    Napier.w(tag = tag, message = "Error in printer state loop", throwable = e)
                    emit(null to e)
                    //endregion
                }

                delay(1.seconds)
            }
        }
    }
    private val notAvailableFlow = flow {
        emit(false)
        delay(notAvailableTimeout)
        emit(true)
    }
    private val activeUrlFlow = engineFlow.filterNotNull().flatMapLatest { engine ->
        engine.baseUrl
    }

    override suspend fun doExecute(param: ConnectPrinterUseCase.Params, logger: UseCase2.Logger) = combine(
        printerStateFlow,
        notAvailableFlow,
        activeUrlFlow,
    ) { (printerState, exception), notAvailable, activeUrl ->
        logger.d(
            message = """
                    ----
                      PrinterState: ${printerState?.copy(text = printerState.text?.replace("\n", " ")?.take(32))}
                      Exception: $exception
                      NotAvailable: $notAvailable
                      ActiveUrl: $activeUrl
                    ----
                """.trimIndent()
        )

        createState(
            printerState = printerState,
            exception = exception,
            notAvailable = notAvailable,
            activeUrl = activeUrl
        )
    }.onStart {
        instanceIdFlow.emit(param.instanceId)
    }

    private fun createState(
        printerState: PrinterState.State?,
        exception: Exception?,
        notAvailable: Boolean,
        activeUrl: Url,
    ) = when {
        exception != null -> createMoonrakerNotAvailableState(
            exception = exception,
            url = activeUrl
        )

        printerState != null -> when {
            printerState.flags.isOperational() -> createConnectedState()
            printerState.flags.isStarting() -> createConnectingState()
            else -> createPrinterOfflineState(detail = printerState.text)
        }

        notAvailable -> createMoonrakerNotAvailableState(
            exception = IllegalStateException("No response within $notAvailableTimeout, still trying to connect"),
            url = activeUrl
        )

        else -> createInitializingState()
    }

    override suspend fun executeAction(actionType: ConnectPrinterUseCase.ActionType) = engineFlow.first().let { engine ->
        requireNotNull(engine) { "Missing engine" }

        when (actionType) {
            ConnectPrinterUseCase.ActionType.Retry -> {
                engine.printerApi.executeGcodeCommand(GcodeCommand.Single("FIRMWARE_RESTART"))

                // We should move to the "connecting" state shortly, let's keep the button loading until then
                delay(5.seconds)
            }

            else -> throw IllegalStateException("Unable to handle action $actionType")
        }
    }

    private fun createInitializingState() = UiState.Initializing.copy(
        title = getString("connect_printer___searching_for_moonraker_title", instanceName)
    )

    private fun createMoonrakerNotAvailableState(exception: Throwable, url: Url) = UiState(
        title = getString("connect_printer___moonraker_not_available_title", instanceName),
        detail = "${url.withoutBasicAuth().withoutQuery()}\n\n" + getString("connect_printer___moonraker_not_available_detail", instanceName),
        avatarState = ConnectPrinterUseCase.AvatarState.Idle,
        exception = exception,
        isNotAvailable = true,
    )

    private fun createConnectingState() = UiState(
        title = getString("connect_printer___printer_is_connecting_title"),
        detail = null,
        avatarState = ConnectPrinterUseCase.AvatarState.Swim,
    )

    private fun createPrinterOfflineState(detail: String?) = UiState(
        title = getString("connect_printer___printer_offline_title"),
        detail = detail,
        avatarState = ConnectPrinterUseCase.AvatarState.Idle,
        action = getString("connect_printer___moonraker_firmware_restart"),
        actionType = ConnectPrinterUseCase.ActionType.Retry,
    )

    private fun createConnectedState() = UiState(
        title = getString("connect_printer___printer_connected_title"),
        detail = getString("connect_printer___printer_connected_detail_1"),
        avatarState = ConnectPrinterUseCase.AvatarState.Party,
    )
}