package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class TunePrintUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<TunePrintUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) = withContext(Dispatchers.SharedIO) {
        val octoPrint = printerEngineProvider.printer(instanceId = param.instanceId)
        //region Feed and Flow rate
        val commandsDeferred = async {
            val commands = listOfNotNull(
                param.feedRate?.coerceIn(0..250)?.let { "M220 S$it" },
                param.flowRate?.coerceIn(0..250)?.let { "M221 S$it" },
            )
            if (commands.isNotEmpty()) {
                octoPrint.printerApi.executeGcodeCommand(GcodeCommand.Batch(commands))
            }
        }
        //endregion
        //region Fan speeds
        val fanDeferred = param.fanSpeeds.map { (key, value) ->
            async {
                if (value != null) {
                    octoPrint.printerApi.setFanSpeed(component = key, percent = value)
                }
            }
        }
        //endregion
        fanDeferred.forEach { it.await() }
        commandsDeferred.await()
    }

    data class Param(
        val feedRate: Int?,
        val flowRate: Int?,
        val fanSpeeds: Map<String, Float?>,
        val instanceId: String?,
    )
}