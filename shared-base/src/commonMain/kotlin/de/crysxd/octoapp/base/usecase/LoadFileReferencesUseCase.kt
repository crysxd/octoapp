package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.FileListRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.base.utils.mapData
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import kotlinx.coroutines.flow.Flow

class LoadFileReferencesUseCase(
    private val fileRepository: FileListRepository,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<LoadFileReferencesUseCase.Params, Flow<FlowState<FileReference.Folder>>>() {

    override suspend fun doExecute(param: Params, logger: Logger): Flow<FlowState<FileReference.Folder>> {
        val instanceId = param.instanceId ?: printerConfigurationRepository.getActiveInstanceSnapshot()?.id ?: throw IllegalStateException("No OctoPrint is active")

        if (param.skipCache) {
            fileRepository.invalidate(
                instanceId = instanceId,
                fileOrigin = param.fileOrigin,
            )
        }

        return fileRepository.observeTree(
            instanceId = instanceId,
            fileOrigin = param.fileOrigin,
        ).mapData { list ->
            list.files
        }
    }

    data class Params(
        val fileOrigin: FileOrigin,
        val instanceId: String?,
        val skipCache: Boolean,
    )
}
