package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.ktor.http.Url
import kotlinx.datetime.Clock

class ExtrudeFilamentUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
    private val executeGcodeCommandUseCase: ExecuteGcodeCommandUseCase,
    private val extrusionHistoryRepository: ExtrusionHistoryRepository,
    private val activateToolUseCase: ActivateToolUseCase,
) : UseCase2<ExtrudeFilamentUseCase.Param, Unit>() {

    private val m302ResponsePattern = Regex("^Recv:\\s+echo:.*(disabled|enabled).*min\\s+temp\\s+(\\d+)")

    override suspend fun doExecute(param: Param, logger: Logger) {
        // Activate the tool select by the user in app settings
        val tool = activateToolUseCase.execute(ActivateToolUseCase.Params(instanceId = param.instanceId))
        val printer = printerEngineProvider.printer(instanceId = param.instanceId)
        val speed = printerConfigurationRepository.get(param.instanceId)?.appSettings?.extrudeFeedRate

        // Check ready to extrude
        when (printer) {
            is OctoPrintEngine -> checkMinTempOctoPrint(printer = printer, tool = tool, logger = logger)
            is MoonrakerEngine -> checkMinTempMoonraker(printer = printer, tool = tool)
        }

        // Update history
        extrusionHistoryRepository.updateHistory(id = param.extrudeLengthMm) {
            it.copy(lastUsed = Clock.System.now().toEpochMilliseconds(), usageCount = it.usageCount + 1)
        }


        // Extrude, temperature ok or unknown
        printer.printerApi.extrudeFilament(
            length = param.extrudeLengthMm.toFloat(),
            speedMmMin = speed?.toFloat()
        )
    }

    private suspend fun checkMinTempMoonraker(
        printer: MoonrakerEngine,
        tool: String?,
    ) {
        if (!printer.extruderApi.checkExtruderCanExtrude(tool ?: "extruder")) {
            throw ColdExtrusionException(
                minTemp = null,
                webUrl = printer.baseUrl.value,
            )
        }
    }

    private suspend fun checkMinTempOctoPrint(
        printer: OctoPrintEngine,
        tool: String?,
        logger: Logger,
    ) {
        // Check if printing
        // When we are printing, we don't check temperatures. Usually this means we are paused because the extrude controls
        // are only available during pause, but we don't care here. M302 is not reliable during prints/paused so we skip it
        // and let OctoPrint/the printer handle cold extrude (#948)
        val state = printer.printerApi.getPrinterState()
        val currentTemp = state.temperature[tool]?.actual?.toInt() ?: Int.MAX_VALUE
        val isPrinting = state.state.flags.isPrinting()

        // Check if we can actually extrude. Some older Marlin printers will crash
        // if we attempt a cold extrusion
        val minTemp = try {
            if (isPrinting) {
                logger.i("Print active, omitting min temperature request and assuming very low minimum of 50°C")
                50
            } else {
                // Check minimum extrusion temp
                val response = executeGcodeCommandUseCase.execute(
                    ExecuteGcodeCommandUseCase.Param(
                        command = GcodeCommand.Single("M302"),
                        fromUser = false,
                        recordResponse = true,
                        recordTimeoutMs = 3_000L,
                    )
                )
                val minTemp = response.mapNotNull {
                    (it as? ExecuteGcodeCommandUseCase.Response.RecordedResponse)?.responseLines
                }.flatten().firstNotNullOfOrNull {
                    val match = m302ResponsePattern.find(it)
                    if (match != null) {
                        val disabled = match.groupValues[1] == "disabled"
                        val minTemp = match.groupValues[2].toIntOrNull() ?: 0
                        if (disabled) minTemp else 0
                    } else {
                        null
                    }
                } ?: let {
                    logger.e("Unable to get min temp from response: $response")
                    0
                }

                logger.i("Determined temperatures:  minTemp=$minTemp currentTemp=$currentTemp")
                minTemp
            }
        } catch (e: Exception) {
            // We tried our best, let's continue without temp check
            logger.e(e)
            0
        }

        // Check if current temp is below minimum
        if (minTemp > currentTemp) {
            throw ColdExtrusionException(
                minTemp = minTemp,
                webUrl = printer.baseUrl.value,
            )
        }
    }

    data class Param(
        val extrudeLengthMm: Int,
        val instanceId: String
    )

    class ColdExtrusionException(val minTemp: Int?, webUrl: Url) : NetworkException(webUrl = webUrl, userFacingMessage = getString("error_cold_extrusion"))
}