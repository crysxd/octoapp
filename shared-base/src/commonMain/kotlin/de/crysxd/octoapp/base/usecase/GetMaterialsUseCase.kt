package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.material.Material

class GetMaterialsUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository
) : UseCase2<GetMaterialsUseCase.Params, List<Material>>() {

    override suspend fun doExecute(param: Params, logger: Logger): List<Material> {
        val octoPrint = printerEngineProvider.printer(param.instanceId)
        val settings = printerConfigurationRepository.getActiveInstanceSnapshot()?.settings
            ?: octoPrint.settingsApi.getSettings()
        val materials = octoPrint.materialsApi.getMaterials(settings)

        // Eliminate duplicate names by adding vendor
        val duplicateNames = mutableSetOf<String>()
        val duplicateNameResolutions = mutableMapOf<String, NameConflictResolution>()
        materials.forEach { m ->
            // Already identified as duplicate, skip
            if (duplicateNames.contains(m.displayName)) return@forEach

            val sameNames = materials.filter { it.displayName == m.displayName }
            if (sameNames.size == 1) return@forEach

            // Alter names to append material
            val materialNames = sameNames.map { it.displayName + it.material }
            val vendorNames = sameNames.map { it.displayName + it.vendor }
            val materialAndVendorNames = sameNames.map { (it.color?.toHexString() ?: "") + it.displayName + it.vendor + it.material }
            val materialNamesAreDistinct = materialNames.distinct().size == sameNames.size
            val vendorNamesAreDistinct = vendorNames.distinct().size == sameNames.size
            val materialAndVendorNamesAreDistinct = materialAndVendorNames.distinct().size == sameNames.size
            duplicateNameResolutions[m.displayName] = when {
                materialNamesAreDistinct -> NameConflictResolution.AddMaterial
                vendorNamesAreDistinct -> NameConflictResolution.AddVendor
                materialAndVendorNamesAreDistinct -> NameConflictResolution.AddMaterialAndVendor
                else -> NameConflictResolution.AddMaterialAndVendorAndId
            }
        }

        // Copy materials and alter attributes if required to make them unique
        return materials.map {
            when (duplicateNameResolutions[it.displayName]) {
                NameConflictResolution.AddMaterial -> it.copyWithAttributes(it.material)
                NameConflictResolution.AddVendor -> it.copyWithAttributes(it.vendor)
                NameConflictResolution.AddMaterialAndVendor -> it.copyWithAttributes(it.material, it.vendor)
                NameConflictResolution.AddMaterialAndVendorAndId -> it.copyWithAttributes(it.material, it.vendor, it.id.id)
                null -> it
            }
        }.sortedWith(compareBy<Material> { it.isActivated }.thenBy { it.displayName.lowercase() })
    }

    private sealed class NameConflictResolution {
        data object AddVendor : NameConflictResolution()
        data object AddMaterialAndVendorAndId : NameConflictResolution()
        data object AddMaterial : NameConflictResolution()
        data object AddMaterialAndVendor : NameConflictResolution()
    }

    data class Params(
        val instanceId: String?
    )
}