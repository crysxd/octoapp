package de.crysxd.octoapp.base.models

data class BillingProduct(
    val productId: String,
    val productName: String,
    val price: String,
    val priceOrder: Int,
    val nativeProduct: Any
)