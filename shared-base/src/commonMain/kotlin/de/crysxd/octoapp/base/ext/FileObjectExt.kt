package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.base.data.models.FileManagerSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileReference
import kotlinx.datetime.Instant

fun List<FileReference>.sorted() = SharedBaseInjector.get().preferences.fileManagerSettings.let { settings ->
    val fileComparator = when (settings.sortBy) {
        FileManagerSettings.SortBy.UploadTime -> compareBy<FileObject.File> { it.date }.thenBy { it.name.lowercase() }
        FileManagerSettings.SortBy.PrintTime -> compareBy<FileObject.File> { it.prints?.last?.date ?: Instant.DISTANT_PAST }.thenBy { it.date }
            .thenBy { it.display.lowercase() }

        FileManagerSettings.SortBy.FileSize -> compareBy<FileObject.File> { it.size }.thenBy { it.name.lowercase() }
        FileManagerSettings.SortBy.Name -> compareBy { it.name.lowercase() }
    }.thenBy { it.path }

    val files = filterIsInstance<FileObject.File>()
        .filter { f -> f.prints?.last == null || !settings.hidePrintedFiles }
        .sortedWith(fileComparator).let {
            when (settings.sortDirection) {
                FileManagerSettings.SortDirection.Ascending -> it
                FileManagerSettings.SortDirection.Descending -> it.reversed()
            }
        }

    val folders = filterIsInstance<FileObject.Folder>()
        .sortedBy { it.display.lowercase() }

    listOf(
        folders,
        files,
    ).flatten()
}

fun FileReference.searchInTree(term: String): List<FileReference> {
    fun FileReference.isMatch() = display.contains(term, ignoreCase = true) || name.contains(term, ignoreCase = true)
    fun FileReference.flatten(): List<FileReference> = when (this) {
        is FileReference.File -> listOf(this)
        is FileReference.Folder -> children?.flatMap { it.flatten() }?.let {
            if (isMatch()) it + this else it
        } ?: emptyList()

        else -> throw IllegalStateException("Can only handle documents and folders")
    }

    return if (term.isEmpty()) {
        flatten()
    } else {
        flatten().filter { it.isMatch() }
    } - this
}

fun FileReference.searchInObjectTree(term: String): List<FileReference> {
    fun FileReference.isMatch() = display.contains(term, ignoreCase = true) || name.contains(term, ignoreCase = true)
    fun FileReference.flatten(): List<FileReference> = when (this) {
        is FileReference.File -> listOf(this)
        is FileReference.Folder -> children?.flatMap { it.flatten() }?.let {
            if (isMatch()) it + this else it
        } ?: emptyList()

        else -> throw IllegalStateException("Can only handle documents and folders")
    }

    return if (term.isEmpty()) {
        flatten()
    } else {
        flatten().filter { it.isMatch() }
    } - this
}
