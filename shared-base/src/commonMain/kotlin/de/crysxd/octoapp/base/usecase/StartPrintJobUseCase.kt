package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.system.SystemInfo.Capability.Timelapse
import de.crysxd.octoapp.engine.octoprint.OctoPlugins

class StartPrintJobUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val octoPreferences: OctoPreferences,
) : UseCase2<StartPrintJobUseCase.Params, StartPrintJobUseCase.Result>() {

    override suspend fun doExecute(param: Params, logger: Logger): Result {
        val octoprint = printerEngineProvider.printer(param.instanceId)
        val instance = printerConfigurationRepository.get(param.instanceId)
        val settings = instance?.settings ?: octoprint.settingsApi.getSettings()
        val materialManagerAvailable = octoprint.materialsApi.isMaterialManagerAvailable(settings)
        val timelapseConfigRequired = octoPreferences.askForTimelapseBeforePrinting && instance?.systemInfo?.capabilities?.contains(Timelapse) == true
        val hasMmu = instance?.hasPlugin(OctoPlugins.Mmu2FilamentSelect)
        logger.i("materialManagerAvailable=$materialManagerAvailable timelapseConfigRequired=$timelapseConfigRequired hasMmu=$hasMmu")

        // If a material manager is present and the selection was not confirmed, we need material selection
        // If the MMU2 plugin is installed, we skip this step as the MMU2 plugin will ask for the material
        if (materialManagerAvailable && !param.materialSelectionConfirmed && hasMmu != true) {
            return Result.MaterialSelectionRequired
        }

        if (timelapseConfigRequired && !param.timelapseConfigConfirmed) {
            return Result.TimelapseConfigRequired
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.PrintStartedByApp)
        printerEngineProvider.printer(param.instanceId).filesApi.executeFileCommand(param.file, FileCommand.PrintFile)
        return Result.PrintStarted
    }

    data class Params(
        val file: FileObject.File,
        val materialSelectionConfirmed: Boolean,
        val timelapseConfigConfirmed: Boolean,
        val instanceId: String? = null
    )

    sealed class Result {
        data object PrintStarted : Result()
        data object MaterialSelectionRequired : Result()
        data object TimelapseConfigRequired : Result()
    }
}