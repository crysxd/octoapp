package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetGcodeShortcutsUseCase(
    private val gcodeHistoryRepository: GcodeHistoryRepository
) : UseCase2<Unit, Flow<List<GcodeHistoryItem>>>() {

    companion object {
        private const val MAX_HISTORY_LENGTH = 5
    }

    override suspend fun doExecute(param: Unit, logger: Logger) = gcodeHistoryRepository.history.map { history ->
        val favorites = history.filter { it.isFavorite }.sortedBy { it.command }
        val others = history.filter { !it.isFavorite }.sortedByDescending { it.lastUsed }.take(MAX_HISTORY_LENGTH)
        listOf(favorites, others).flatten()
    }
}