package de.crysxd.octoapp.base.network

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.exceptions.BrokenSetupException
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.ext.composeMessageStack
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.framework.ExceptionInspector
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterHttpsException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere.OctoEverywhereSubscriptionMissingException
import de.crysxd.octoapp.sharedcommon.url.isBasedOn
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

class DetectBrokenSetupInterceptor(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val octoPrintId: String,
) : ExceptionInspector {

    companion object {
        var enabled = true
        private var disabledUntil = Instant.DISTANT_PAST
        private const val Tag = "DetectBrokenSetupInterceptor"
    }

    override fun inspect(e: Throwable) {
        if (enabled && e is NetworkException && isBrokenSetup(e)) {
            val isForActive = printerConfigurationRepository.getActiveInstanceSnapshot()?.id == octoPrintId
            val instance = printerConfigurationRepository.get(octoPrintId)
            val isForRemote = e.webUrl.isBasedOn(instance?.alternativeWebUrl)
            Napier.w(tag = Tag, message = "Handling ${e::class.simpleName} on ${instance?.id}")

            // Only handle if the OctoPrint having the issue is active
            if (!isForActive) {
                return
            }

            // Remove remote connection if broken
            if (e is RemoteServiceConnectionBrokenException || isForRemote) {
                if (!isForRemote) {
                    Napier.e(tag = Tag, message = "Received remote error for ${e.webUrl} but not based on current ${instance?.alternativeWebUrl} -> Ignoring")
                    return
                }

                runBlocking {
                    Napier.w(tag = Tag, message = "Caught OctoEverywhere/Obico/ngrok exception, removing connection")
                    (e as? RemoteServiceConnectionBrokenException)?.logEvent()
                    printerConfigurationRepository.update(octoPrintId) {
                        it.copy(
                            alternativeWebUrl = null,
                            octoEverywhereConnection = null,
                            remoteConnectionFailure = RemoteConnectionFailure(
                                errorMessage = e.composeErrorMessage().toString().replace("<br>", "\n"),
                                errorMessageStack = e.composeMessageStack().toString().replace("<br>", "\n"),
                                stackTrace = e.stackTraceToString(),
                                remoteServiceName = (e as? RemoteServiceConnectionBrokenException)?.remoteServiceName ?: when (instance?.alternativeWebUrlPlugin) {
                                    OctoPlugins.OctoEverywhere -> RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OCTO_EVERYWHERE
                                    OctoPlugins.Obico -> RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OBICO
                                    OctoPlugins.Ngrok -> RemoteServiceConnectionBrokenException.REMOTE_SERVICE_NGROK
                                    else -> when {
                                        e.webUrl.isOctoEverywhereUrl() -> RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OCTO_EVERYWHERE
                                        e.webUrl.isObicoUrl() -> RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OBICO
                                        e.webUrl.isNgrokUrl() -> RemoteServiceConnectionBrokenException.REMOTE_SERVICE_NGROK
                                        else -> RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OTHER
                                    }
                                },
                                dateMillis = Clock.System.now().toEpochMilliseconds(),
                                message = when (e) {
                                    is OctoEverywhereSubscriptionMissingException -> "OctoEverywhere reports your supporter status expired, the connection to OctoApp was removed"
                                    else -> null
                                },
                                learnMoreUrl = when (e) {
                                    is OctoEverywhereSubscriptionMissingException -> "https://octoeverywhere.com/appportal/v1/nosupporterperks?appid=octoapp"
                                    else -> null
                                }
                            )
                        )
                    }
                }
            }

            // Dispatch to user
            if (Clock.System.now() > disabledUntil) {
                disabledUntil = Clock.System.now() + 15.seconds
                ExceptionReceivers.dispatchException(
                    BrokenSetupException(
                        original = e,
                        userMessage = when (e) {
                            is PrinterHttpsException -> getString("sign_in___broken_setup___https_issue", e.originalCause?.message ?: e.message ?: "")
                            is BasicAuthRequiredException -> getString("sign_in___broken_setup___basic_auth_required")
                            is InvalidApiKeyException -> getString("sign_in___broken_setup___api_key_revoked")
                            else -> e.composeErrorMessage()
                        }.toString(),
                        instance = requireNotNull(instance) { "Broken instance $octoPrintId not found" },
                        needsRepair = e !is RemoteServiceConnectionBrokenException
                    )
                )
            }
        }
    }

    private fun RemoteServiceConnectionBrokenException.logEvent() {
        OctoAnalytics.logEvent(
            event = when (remoteServiceName) {
                RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OBICO -> OctoAnalytics.Event.ObicoBroken
                RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OCTO_EVERYWHERE -> OctoAnalytics.Event.OctoEverywhereBroken
                RemoteServiceConnectionBrokenException.REMOTE_SERVICE_NGROK -> OctoAnalytics.Event.NgrokBroken
                else -> return
            },
            params = mapOf("reason" to this::class.simpleName)
        )
    }

    private fun isBrokenSetup(e: Throwable) =
        e is BasicAuthRequiredException || e is PrinterHttpsException || e is InvalidApiKeyException || e is RemoteServiceConnectionBrokenException

    fun interface Factory {
        fun buildFor(octoPrintId: String): DetectBrokenSetupInterceptor
    }
}