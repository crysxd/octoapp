package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.usecase.ActivateMaterialUseCase
import de.crysxd.octoapp.base.usecase.ActivateToolUseCase
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase
import de.crysxd.octoapp.base.usecase.ApplyWebcamTransformationsUseCase
import de.crysxd.octoapp.base.usecase.AutoConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.CancelObjectUseCase
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.ChangeZOffsetUseCase
import de.crysxd.octoapp.base.usecase.ClearPublicFilesUseCase
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.CreateFolderUseCase
import de.crysxd.octoapp.base.usecase.CyclePsuUseCase
import de.crysxd.octoapp.base.usecase.DeleteFileUseCase
import de.crysxd.octoapp.base.usecase.DeleteTimelapseUseCase
import de.crysxd.octoapp.base.usecase.DisconnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.DiscoverOctoPrintUseCase
import de.crysxd.octoapp.base.usecase.DownloadFileUseCase
import de.crysxd.octoapp.base.usecase.DownloadTimelapseUseCase
import de.crysxd.octoapp.base.usecase.EmergencyStopUseCase
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.base.usecase.ExecuteSystemCommandUseCase
import de.crysxd.octoapp.base.usecase.ExtrudeFilamentUseCase
import de.crysxd.octoapp.base.usecase.GenerateOctoEverywhereLiveLinkUseCase
import de.crysxd.octoapp.base.usecase.GenericDownloadUseCase
import de.crysxd.octoapp.base.usecase.GetActiveHttpUrlUseCase
import de.crysxd.octoapp.base.usecase.GetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.GetCurrentPrinterProfileUseCase
import de.crysxd.octoapp.base.usecase.GetExtrusionShortcutsUseCase
import de.crysxd.octoapp.base.usecase.GetGcodeShortcutsUseCase
import de.crysxd.octoapp.base.usecase.GetMaterialsUseCase
import de.crysxd.octoapp.base.usecase.GetOctoPrintWebUrlUseCase
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.base.usecase.GetPrintHistoryUseCase
import de.crysxd.octoapp.base.usecase.GetPrinterConnectionUseCase
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.GetTuneStateUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSettingsUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.usecase.GetWidgetDataUseCase
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.usecase.HandleObicoAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HandleOctoEverywhereAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HomePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.JogPrintHeadUseCase
import de.crysxd.octoapp.base.usecase.LoadFileReferencesUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.MoveFileUseCase
import de.crysxd.octoapp.base.usecase.MovePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.PerformPendingSettingsSave
import de.crysxd.octoapp.base.usecase.RefreshBedMeshUseCase
import de.crysxd.octoapp.base.usecase.RegisterWithCompanionPluginUseCase
import de.crysxd.octoapp.base.usecase.RequestApiAccessUseCase
import de.crysxd.octoapp.base.usecase.ResetZOffsetUseCase
import de.crysxd.octoapp.base.usecase.SaveZOffsetToConfigUseCase
import de.crysxd.octoapp.base.usecase.SelectMmu2FilamentUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.base.usecase.SetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTemperatureOffsetUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.usecase.TestNewUrlUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.usecase.TriggerInitialCancelObjectMessageUseCase
import de.crysxd.octoapp.base.usecase.TunePrintUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.base.usecase.UploadFileUseCase
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import org.koin.dsl.module

class UseCaseModule : BaseModule {

    override val koinModule = module {
        factory { GetActiveHttpUrlUseCase(get(), get()) }
        factory { GetWebcamSettingsUseCase(get(), get(), get(), get(), get()) }
        factory { ApplyWebcamTransformationsUseCase() }
        factory { GetPowerDevicesUseCase(get(), get()) }
        factory { HandleAutomaticLightEventUseCase(get(), get()) }
        factory { TestFullNetworkStackUseCase(get(), get(), get(), get(), get(), get()) }
        factory { DiscoverOctoPrintUseCase(get(), get(), get()) }
        factory { RequestApiAccessUseCase(get()) }
        factory { GetAppLanguageUseCase(get(), get()) }
        factory { UpdateInstanceCapabilitiesUseCase(get(), get(), get(), get(), get()) }
        factory { GetWebcamSnapshotUseCase(get(), get(), get(), get(), get(), get(), get()) }
        factory { GetWidgetDataUseCase({ get() }, { get() }, { get() }, { get() }, { get() }) }
        factory { TogglePausePrintJobUseCase(get()) }
        factory { CancelPrintJobUseCase(get(), get()) }
        factory { SetTargetTemperaturesUseCase(get(), get()) }
        factory { SetTemperatureOffsetUseCase(get(), get()) }
        factory { GetCurrentPrinterProfileUseCase(get()) }
        factory { LoadFilesUseCase(get(), get()) }
        factory { LoadFileReferencesUseCase(get(), get()) }
        factory { StartPrintJobUseCase(get(), get(), get()) }
        factory { CreateBugReportUseCase(get(), get(), get(), get(), get()) }
        factory { GetRemoteServiceConnectUrlUseCase(get()) }
        factory { HandleOctoEverywhereAppPortalSuccessUseCase(get(), get()) }
        factory { HandleObicoAppPortalSuccessUseCase(get(), get()) }
        factory { SetAlternativeWebUrlUseCase(get(), get()) }
        factory { UpdateNgrokTunnelUseCase(get(), get()) }
        factory { ConnectPrinterUseCase(get(), get(), get(), get(), get(), get(), get()) }
        factory { DisconnectPrinterUseCase(get()) }
        factory { GetPrinterConnectionUseCase(get()) }
        factory { AutoConnectPrinterUseCase(get()) }
        factory { SetAppLanguageUseCase(get()) }
        factory { ExecuteSystemCommandUseCase(get()) }
        factory { GetOctoPrintWebUrlUseCase(get(), get(), get()) }
        factory { EmergencyStopUseCase(get()) }
        factory { GetMaterialsUseCase(get(), get()) }
        factory { ActivateMaterialUseCase(get()) }
        factory { GetPrintHistoryUseCase(get(), get()) }
        factory { CyclePsuUseCase() }
        factory { HomePrintHeadUseCase(get()) }
        factory { JogPrintHeadUseCase(get(), get(), get()) }
        factory { MovePrintHeadUseCase(get(), get(), get()) }
        factory { ExecuteGcodeCommandUseCase(get(), get(), get()) }
        factory { RefreshBedMeshUseCase(get(), get()) }
        factory { ExtrudeFilamentUseCase(get(), get(), get(), get(), get()) }
        factory { GetExtrusionShortcutsUseCase(get()) }
        factory { GetGcodeShortcutsUseCase(get()) }
        factory { TunePrintUseCase(get()) }
        factory { ActivateToolUseCase(get(), get()) }
        factory { TriggerInitialCancelObjectMessageUseCase(get(), get()) }
        factory { CancelObjectUseCase(get(), get()) }
        factory { RegisterWithCompanionPluginUseCase(get(), get(), get(), get(), get()) }
        factory { DownloadTimelapseUseCase(get(), get()) }
        factory { DeleteTimelapseUseCase(get(), get(), get()) }
        factory { ClearPublicFilesUseCase(get()) }
        factory { SelectMmu2FilamentUseCase(get()) }
        factory { GenerateOctoEverywhereLiveLinkUseCase(get()) }
        factory { MoveFileUseCase(get(), get(), get()) }
        factory { ApplyTemperaturePresetUseCase(get(), get(), get()) }
        factory { DeleteFileUseCase(get()) }
        factory { DownloadFileUseCase(get(), get()) }
        factory { CreateFolderUseCase(get()) }
        factory { UploadFileUseCase(get(), get()) }
        factory { GetTuneStateUseCase(get(), get(), get(), get()) }
        factory { ChangeZOffsetUseCase(get()) }
        factory { ResetZOffsetUseCase(get()) }
        factory { SaveZOffsetToConfigUseCase(get()) }
        factory { TestNewUrlUseCase(get(), get()) }
        factory { PerformPendingSettingsSave(get()) }
        factory { GenericDownloadUseCase(get()) }
    }
}
