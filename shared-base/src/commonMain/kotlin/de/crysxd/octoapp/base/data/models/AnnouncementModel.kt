package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.Serializable

@Serializable
data class AnnouncementModel(
    val id: String? = null,
    val text: String? = null,
    val learnMoreText: String? = null,
    val learnMoreLink: String? = null,
    val canHide: Boolean = true,
    val redColor: Boolean = false,
    val confettiBackground: Boolean = false,
) {
    companion object {
        val Empty = AnnouncementModel("")
    }
}