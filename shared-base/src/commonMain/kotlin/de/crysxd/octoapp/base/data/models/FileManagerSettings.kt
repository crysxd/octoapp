package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.Serializable

@Serializable
data class FileManagerSettings(
    val hidePrintedFiles: Boolean = false,
    val sortBy: SortBy = SortBy.UploadTime,
    val sortDirection: SortDirection = SortDirection.Descending,
) {
    @Serializable
    enum class SortBy {
        UploadTime, PrintTime, FileSize, Name;
    }

    @Serializable
    enum class SortDirection {
        Ascending, Descending;
    }
}