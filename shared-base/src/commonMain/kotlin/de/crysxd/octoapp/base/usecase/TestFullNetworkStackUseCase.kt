package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.BackendType
import de.crysxd.octoapp.base.data.models.BackendType.Moonraker
import de.crysxd.octoapp.base.data.models.BackendType.OctoPrint
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.exceptions.UnknownHostException
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.exceptions.WebSocketUpgradeFailedException
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.ImageByteCacheOverflowException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterHttpsException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.IpAddress
import de.crysxd.octoapp.sharedcommon.http.config.X509CertificateWrapper
import de.crysxd.octoapp.sharedcommon.http.config.assertPortOpen
import de.crysxd.octoapp.sharedcommon.http.config.assertReachable
import de.crysxd.octoapp.sharedcommon.http.config.hostNameOrIp
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.dropWhile
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import okio.IOException
import kotlin.time.Duration.Companion.seconds

class TestFullNetworkStackUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val getWebcamSnapshotUseCase: GetWebcamSnapshotUseCase,
    private val getWebcamSettingsUseCase: GetWebcamSettingsUseCase,
    private val localDnsResolver: CachedDns,
    private val httpClientSettings: HttpClientSettings,
) : UseCase2<TestFullNetworkStackUseCase.Target, TestFullNetworkStackUseCase.Finding>() {

    companion object {
        private val PING_TIMEOUT = 5.seconds
        private val SOCKET_TIMEOUT = 5.seconds
    }

    override suspend fun doExecute(param: Target, logger: Logger): Finding = withContext(Dispatchers.SharedIO) {
        var webUrl: String? = null

        try {
            //region See if it magically works (on iOS this triggers the local network permission dialog)
            if (param is Target.Printer) param.webUrl.toUrlOrNull()?.let { url ->
                testPrinter(logger, url, param, url.host).let {
                    if (it is Finding.InvalidApiKey || it is Finding.OctoPrintReady || it is Finding.MoonrakerReady) {
                        return@withContext it
                    }
                }
            }
            //endregion
            //region Extract webUrl
            val webcamSettings = (param as? Target.Webcam)?.let {
                val instance = requireNotNull(printerConfigurationRepository.get(param.instanceId)) { "Unable to find OctoPrint for id ${param.instanceId}" }
                getWebcamSettingsUseCase.execute(instance).first()[param.webcamIndex] as? ResolvedWebcamSettings.MjpegSettings ?: throw WebcamUnsupportedException()
            }


            webUrl = when {
                param is Target.Printer -> param.webUrl
                webcamSettings?.url?.toString() == null -> return@withContext Finding.EmptyUrl()
                else -> webcamSettings.url.toString()
            }
            //endregion
            //region Parse URL
            logger.i("Testing URL syntax")
            val (baseUrl, host) = try {
                val url = webUrl.toUrl()
                url to url.host
            } catch (e: Exception) {
                return@withContext Finding.InvalidUrl(input = webUrl, exception = e)
            }
            logger.i("Passed")
            //endregion
            //region Test DNS
            logger.i("Testing DNS resolution")
            val (ips, dnsFinding) = testDns(host = host, webUrl = baseUrl, logger = logger)
            dnsFinding?.let { return@withContext it }
            ips.takeUnless { it.isEmpty() } ?: throw RuntimeException("IP should be set if no finding was returned")
            logger.i("Passed")
            //endregion
            val results = ips.map { ip ->
                async(Dispatchers.SharedIO) {
                    //region Test reachability
                    logger.i("Testing reachability for $ip")
                    val reachable = testReachability(host = host, ip = ip, webUrl = baseUrl, logger = logger)
                    logger.i(if (reachable == null) "Passed for $ip" else "Failed for $ip")
                    //endregion
                    //region Test port open
                    if (!this.coroutineContext.isActive) return@async reachable
                    logger.i("Testing port access for $ip")
                    val portOpen = testPortOpen(host = host, ip = ip, webUrl = baseUrl, logger = logger)
                    logger.i(if (portOpen == null) "Passed for $ip" else "Failed for $ip")

                    // Return reachability issue or port open issue if reachable
                    // This setup will ignore reachability issues in case the port is open
                    // Some servers can't be pinged, this solves the issue
                    if (portOpen == null) null else reachable ?: portOpen
                    //endregion
                }.also {
                    //region Error handling
                    it.invokeOnCompletion { t ->
                        if (t != null) {
                            logger.i("Cancelling checks for $ip, caused by ${t::class.simpleName}")
                        }
                    }
                    //endregion
                }
            }.let { results ->
                //region Await first, cancel others
                while (results.any { !it.isCompleted }) {
                    delay(1000)
                    val anyNull = results.filter { it.isCompleted }.any { it.await() == null }
                    if (anyNull) {
                        logger.i("One did pass, cancelling others")
                        break
                    }
                }

                results.map {
                    if (it.isCompleted) {
                        it.await()
                    } else {
                        it.cancel()
                        it.cancel(CancellationException("No longer needed"))
                        null
                    }
                }
                //endregion
            }

            //region Return potential error from results
            if (results.all { it != null }) {
                return@withContext results.first()!!
            }
            //endregion
            //region Test Access
            val res = when (param) {
                is Target.Printer -> testPrinter(
                    logger = logger,
                    webUrl = baseUrl,
                    target = param,
                    host = host
                )

                is Target.Webcam -> testWebcam(
                    logger = logger,
                    host = host,
                    webcamSettings = requireNotNull(webcamSettings) { "Settings must not be null, tested before" },
                    instanceId = param.instanceId,
                    webcamIndex = param.webcamIndex,
                )
            }
            res
            //endregion
        } catch (e: Exception) {
            Finding.UnexpectedIssue(
                webUrl = webUrl?.toUrlOrNull(),
                exception = e
            )
        }
    }

    private suspend fun testPrinter(logger: Logger, webUrl: Url, target: Target.Printer, host: String): Finding {
        // Test HTTP(S) access
        // Using the full stack here, just to be sure that the stack can also resolve the DNS
        // (should though as using same resolver)
        logger.i("Testing HTTP(S) connection")
        testHttpAccess(webUrl = webUrl, host = host, logger = logger)?.let { return it }
        logger.i("Passed")

        // Determine host type
        logger.i("Determining server type")
        val (type, finding) = determineHostType(webUrl = webUrl, logger = logger)
        finding?.let { return it }
        requireNotNull(type) { "Finding was null but type was null as well" }
        logger.i("Determined type: $type")

        // Test that we actually are talking to an OctoPrint
        logger.i("Testing API key")
        testApiKeyValid(webUrl = webUrl, host = host, apiKey = target.apiKey, logger = logger, type = type)?.let { return it }
        logger.i("Passed")

        // Test the websocket
        logger.i("Test web socket is working")
        testWebSocket(webUrl = webUrl, apiKey = target.apiKey, host = host, logger = logger, type = type)?.let { return it }
        logger.i("Passed")

        return if (type == OctoPrint) {
            Finding.OctoPrintReady(
                webUrl = webUrl,
                apiKey = target.apiKey,
            )
        } else {
            Finding.MoonrakerReady(
                webUrl = webUrl,
                apiKey = target.apiKey,
            )
        }
    }

    private suspend fun testWebcam(
        logger: Logger,
        webcamSettings: ResolvedWebcamSettings.MjpegSettings,
        instanceId: String?,
        webcamIndex: Int,
        host: String
    ) = try {
        //region Webcam
        val (frame, fps) = withTimeout(30000) {
            logger.i("Test webcam video")
            var startTime = Instant.DISTANT_PAST
            val frames = 20
            val frame = MjpegConnection3(
                streamUrl = webcamSettings.url.toString().toUrl(),
                name = "test",
                throwExceptions = true,
                httpSettings = httpClientSettings.copy(
                    extraHeaders = webcamSettings.extraHeaders + httpClientSettings.extraHeaders
                ),
            ).load().mapNotNull { it as? MjpegConnection3.MjpegSnapshot.Frame }.onEach {
                startTime = startTime.takeIf { it > Instant.DISTANT_PAST } ?: Clock.System.now()
            }.take(frames).toList().last()
            val endTime = Clock.System.now()
            val fps = 1000 / ((endTime - startTime).inWholeMilliseconds / frames.toFloat())
            frame to fps
        }
        logger.i("Passed ($fps FPS)")
        //endregion
        //region Data saver (optional)
        var snapshot: GetWebcamSnapshotUseCase.Snapshot? = null
        var snapshotError: Exception? = null
        withTimeoutOrNull(30000) {

            logger.i("Test data saver webcam")
            try {
                snapshot = getWebcamSnapshotUseCase.execute(
                    GetWebcamSnapshotUseCase.Params(
                        instanceId = instanceId,
                        webcamIndex = webcamIndex,
                        illuminateIfPossible = true,
                        maxSize = 720,
                        interval = null
                    )
                ).first()
            } catch (e: Exception) {
                logger.e(e)
                snapshotError = e
            }
        }
        logger.i(if (snapshot == null) "Failed" else "Passed")
        //endregion

        Finding.WebcamReady(
            webUrl = webcamSettings.url.toString().toUrl(),
            fps = fps,
            image = frame.frame,
            dataSaverException = snapshotError,
            dataSaverImage = snapshot?.bitmap,
            dataSaverSource = snapshot?.source,
        )
    } catch (e: PrinterApiException) {
        logger.compactWarn(e)
        when (e.responseCode) {
            404 -> Finding.NotFound(
                host = host,
                webUrl = webcamSettings.url.toString().toUrl(),
            )

            else -> Finding.UnexpectedHttpIssue(
                webUrl = webcamSettings.url.toString().toUrl(),
                exception = e,
                host = host
            )
        }
    } catch (e: BasicAuthRequiredException) {
        logger.compactWarn(e)
        Finding.BasicAuthRequired(
            host = host,
            userRealm = e.userRealm,
            webUrl = webcamSettings.url.toString().toUrl(),
        )
    } catch (e: IOException) {
        logger.compactWarn(e)
        if (e.message?.contains("Connection broken", ignoreCase = true) == true) {
            Finding.NoImage(webUrl = webcamSettings.url.toString().toUrl(), host = host)
        } else {
            Finding.UnexpectedHttpIssue(webUrl = webcamSettings.url.toString().toUrl(), host = host, exception = e)
        }
    } catch (e: ImageByteCacheOverflowException) {
        Finding.ImageCacheOverflow(webcamSettings.url, e)
    } catch (e: Exception) {
        Finding.UnexpectedIssue(webcamSettings.url.toString().toUrl(), e)
    }

    private fun CoroutineScope.testDns(host: String, webUrl: Url, logger: Logger): Pair<List<IpAddress>, Finding?> = try {
        val ips = localDnsResolver.lookup(host)
        if (ips.isEmpty()) throw UnknownHostException(host = host)
        ips to null
    } catch (e: Exception) {
        if (coroutineContext.isActive) logger.compactWarn(e)

        // KTOR and java.io.UnknownHostException
        val localResult = emptyList<IpAddress>() to Finding.LocalDnsFailure(host = host, webUrl = webUrl)
        val remoteResult = emptyList<IpAddress>() to Finding.DnsFailure(host = host, webUrl = webUrl)

        when (e::class.simpleName) {
            "UnknownHostException" -> when {
                host.endsWith(".local") -> localResult
                host.endsWith(".home") -> localResult
                host.endsWith(".lan") -> localResult
                else -> remoteResult
            }

            else -> throw e
        }
    }

    private suspend fun testReachability(host: String, ip: IpAddress, webUrl: Url, logger: Logger): Finding? = try {
        ip.assertReachable(PING_TIMEOUT)
        null
    } catch (e: Exception) {
        if (currentCoroutineContext().isActive) logger.compactWarn(e)
        Finding.HostNotReachable(
            host = host,
            ip = ip.hostNameOrIp(),
            timeoutMs = PING_TIMEOUT.inWholeMilliseconds,
            webUrl = webUrl
        )
    }

    private suspend fun testPortOpen(host: String, ip: IpAddress, webUrl: Url, logger: Logger): Finding? = try {
        ip.assertPortOpen(webUrl.port, timeout = SOCKET_TIMEOUT)
        null
    } catch (e: Exception) {
        logger.compactWarn(e)
        Finding.PortClosed(
            host = host,
            port = webUrl.port,
            webUrl = webUrl
        )
    }

    private suspend fun testHttpAccess(webUrl: Url, host: String, logger: Logger): Finding? = try {
        printerEngineProvider.createAdHocPrinter(
            instance = PrinterConfigurationV3(id = "adhoc", webUrl = webUrl.toString().toUrl(), apiKey = "notanapikey"),
            logTag = logger.httpTag,
        ).probe(throwException = true)

        // All good
        null
    } catch (e: PrinterApiException) {
        if (e.responseCode == 404 || e.responseCode == 302 || e.responseCode == 401) {
            null
        } else {
            Finding.UnexpectedHttpIssue(
                webUrl = webUrl,
                host = host,
                exception = IOException("Unexpected HTTP response code ${e.responseCode}")
            )
        }
    } catch (e: PrinterHttpsException) {
        logger.compactWarn(e)
        Finding.HttpsNotTrusted(
            webUrl = webUrl,
            host = host,
            certificate = e.certificate,
            weakHostnameVerificationRequired = e.weakHostnameVerificationRequired,
        )
    } catch (e: BasicAuthRequiredException) {
        logger.compactWarn(e)
        Finding.BasicAuthRequired(
            host = host,
            userRealm = e.userRealm,
            webUrl = webUrl,
        )
    } catch (e: Exception) {
        logger.compactWarn(e)
        Finding.UnexpectedHttpIssue(
            webUrl = webUrl,
            exception = e,
            host = host,
        )
    }

    private suspend fun determineHostType(webUrl: Url, logger: Logger): Pair<BackendType?, Finding?> {
        val scope = CoroutineScope(Dispatchers.SharedIO)
        return try {
            val resultFlow = MutableStateFlow<List<BackendType?>>(emptyList())
            val base = PrinterConfigurationV3(
                id = "adhoc",
                webUrl = webUrl.toString().toUrl(),
                apiKey = "",
            )

            fun testInstance(type: BackendType) {
                scope.launch {
                    logger.d("Testing for $type")
                    try {
                        val instance = printerEngineProvider.createAdHocPrinter(instance = base.copy(type = type), logTag = logger.httpTag)
                        if (instance.probe()) {
                            logger.d("Testing for $type is positive")
                            resultFlow.update { it + type }
                        } else {
                            logger.d("Testing for $type is negative")
                            resultFlow.update { it + null }
                        }
                    } catch (e: Exception) {
                        logger.d("Testing for $type failed")
                        logger.compactWarn(e)
                        resultFlow.update { it + null }
                    }
                }
            }

            testInstance(type = OctoPrint)
            testInstance(type = Moonraker)

            logger.d("Waiting for type test results")
            val result = resultFlow.dropWhile { it.size < 2 }.first()
            logger.d("Type result: $result")
            val type = result.filterNotNull().firstOrNull()
            val finding = Finding.ServerIsNotOctoPrint(webUrl = webUrl, host = webUrl.host).takeIf { type == null }
            type to finding
        } finally {
            scope.cancel()
        }
    }

    private suspend fun testApiKeyValid(webUrl: Url, host: String, apiKey: String, logger: Logger, type: BackendType): Finding? = try {
        val octoPrint = printerEngineProvider.createAdHocPrinter(
            instance = PrinterConfigurationV3(id = "adhoc", webUrl = webUrl.toString().toUrl(), apiKey = apiKey, type = type),
            logTag = logger.httpTag
        )
        val isApiKeyValid = octoPrint.userApi.getCurrentUser().isGuest.not()
        if (isApiKeyValid) {
            null
        } else {
            Finding.InvalidApiKey(webUrl = webUrl, host = host, type = type)
        }
    } catch (e: PrinterApiException) {
        logger.compactWarn(e)
        if (e.responseCode == 404) {
            Finding.NotFound(webUrl = webUrl, host = host)
        } else {
            Finding.UnexpectedHttpIssue(
                webUrl = webUrl,
                exception = e,
                host = host,
            )
        }
    } catch (e: Exception) {
        logger.compactWarn(e)
        Finding.UnexpectedHttpIssue(
            webUrl = webUrl,
            exception = e,
            host = host,
        )
    }

    private suspend fun testWebSocket(webUrl: Url, apiKey: String, host: String, logger: Logger, type: BackendType) = try {
        withTimeout(10000) {
            val instance = PrinterConfigurationV3(id = "adhoc", webUrl = webUrl.toString().toUrl(), apiKey = apiKey, type = type)
            when (val event = printerEngineProvider.createAdHocPrinter(
                instance = instance,
                logTag = logger.httpTag
            ).eventSource.eventFlow("test").filter { it !is Event.MessageReceived }.first()) {
                is Event.Connected -> null
                is Event.Disconnected -> when (event.exception) {
                    is WebSocketUpgradeFailedException -> Finding.WebSocketUpgradeFailed(
                        webUrl = webUrl,
                        host = host,
                        webSocketUrl = (event.exception as WebSocketUpgradeFailedException).webSocketUrl.toString(),
                        responseCode = (event.exception as WebSocketUpgradeFailedException).responseCode
                    )

                    else -> Finding.UnexpectedIssue(
                        webUrl = webUrl,
                        exception = event.exception ?: RuntimeException("Received unexpected null exception (1)")
                    )
                }

                else -> Finding.UnexpectedIssue(webUrl = webUrl, exception = RuntimeException("Received unexpected ${event::class.simpleName} (2)"))
            }
        }
    } catch (e: TimeoutCancellationException) {
        logger.compactWarn(e)
        Finding.UnexpectedIssue(webUrl = webUrl, Exception("Web socket test timed out"))
    }

    private fun Logger.compactWarn(e: Throwable, cause: Boolean = false) {
        val prefix = if (cause) "Caused by " else ""
        w("$prefix${e::class.simpleName}: ${e.message}")
        e.cause?.let { compactWarn(it, cause = true) }
    }

    // URL is a String here because it might come directly from user input. This way we can also test URL syntax
    sealed class Target {

        data class Webcam(
            val instanceId: String,
            val webcamIndex: Int,
        ) : Target()

        data class Printer(
            val webUrl: String,
            val apiKey: String,
        ) : Target()
    }

    sealed class Finding {
        abstract val webUrl: Url?

        data class EmptyUrl(
            override val webUrl: Url? = null,
        ) : Finding()

        data class InvalidUrl(
            override val webUrl: Url? = null,
            val input: String,
            val exception: Exception,
        ) : Finding()

        data class LocalDnsFailure(
            override val webUrl: Url,
            val host: String
        ) : Finding()

        data class DnsFailure(
            override val webUrl: Url,
            val host: String
        ) : Finding()

        data class HostNotReachable(
            override val webUrl: Url,
            val host: String,
            val ip: String,
            val timeoutMs: Long
        ) : Finding()

        data class PortClosed(
            override val webUrl: Url,
            val host: String,
            val port: Int
        ) : Finding()

        data class BasicAuthRequired(
            override val webUrl: Url,
            val host: String,
            val userRealm: String
        ) : Finding()

        data class HttpsNotTrusted(
            override val webUrl: Url,
            val host: String,
            val certificate: X509CertificateWrapper? = null,
            val weakHostnameVerificationRequired: Boolean = false,
        ) : Finding()

        data class NotFound(
            override val webUrl: Url,
            val host: String,
        ) : Finding()

        data class UnexpectedHttpIssue(
            override val webUrl: Url,
            val host: String,
            val exception: Throwable
        ) : Finding()

        data class ServerIsNotOctoPrint(
            override val webUrl: Url,
            val host: String,
        ) : Finding()

        data class InvalidApiKey(
            override val webUrl: Url,
            val type: BackendType,
            val host: String,
        ) : Finding()

        data class UnexpectedIssue(
            override val webUrl: Url?,
            val exception: Throwable
        ) : Finding()

        data class ImageCacheOverflow(
            override val webUrl: Url?,
            val exception: ImageByteCacheOverflowException
        ) : Finding()

        data class WebSocketUpgradeFailed(
            override val webUrl: Url,
            val host: String,
            val webSocketUrl: String,
            val responseCode: Int,
        ) : Finding()

        data class NoImage(
            override val webUrl: Url,
            val host: String,
        ) : Finding()

        data class MoonrakerReady(
            override val webUrl: Url,
            val apiKey: String?,
        ) : Finding()

        data class OctoPrintReady(
            override val webUrl: Url,
            val apiKey: String,
        ) : Finding()

        data class WebcamReady(
            override val webUrl: Url,
            val dataSaverImage: Image?,
            val dataSaverSource: GetWebcamSnapshotUseCase.SnapshotSource?,
            val dataSaverException: Exception?,
            val fps: Float,
            val image: Image
        ) : Finding()
    }

    class WebcamUnsupportedException : IllegalArgumentException("Only MJPEG webcams are supported")

}