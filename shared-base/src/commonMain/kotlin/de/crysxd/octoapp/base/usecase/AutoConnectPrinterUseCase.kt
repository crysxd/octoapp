package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.engine.models.commands.ConnectionCommand


class AutoConnectPrinterUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<AutoConnectPrinterUseCase.Params, AutoConnectPrinterUseCase.Result>() {

    companion object {
        const val AUTO_PORT = "AUTO"
    }

    override suspend fun doExecute(param: Params, logger: Logger): Result {
        val octoPrint = printerEngineProvider.printer(instanceId = param.instanceId).asOctoPrint()
        val flags = try {
            octoPrint.printerApi.getPrinterState().state.flags
        } catch (e: PrinterNotOperationalException) {
            // This means printer is not connected -> acceptable result
            null
        }

        return if (flags != null && flags.isPrinting()) {
            logger.w("Printer found to be busy, cancelling connection attempt: $flags")
            Result.PrinterBusy
        } else {
            logger.w("Printer found to be idle, starting connection attempt: $flags")
            octoPrint.connectionApi.executeConnectionCommand(ConnectionCommand.Connect(port = param.port))
            Result.ConnectionTriggered
        }
    }

    data class Params(
        val port: String = AUTO_PORT,
        val instanceId: String? = null
    )

    enum class Result {
        ConnectionTriggered,
        PrinterBusy,
    }
}