package de.crysxd.octoapp.base.migrations

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.system.SystemInfo
import io.github.aakira.napier.Napier

internal class MoonrakerDetectionChangeMigration : AllSharedMigrations.Migration {

    private val configRepo by lazy { SharedBaseInjector.get().printerConfigRepository }

    override suspend fun runMigration() {
        configRepo.getAll().forEach { config ->
            if (config.affectedByMoonrakerDetectionChange == null) {
                configRepo.update(config.id) {
                    val affected = it.systemInfo?.interfaceType == SystemInfo.Interface.MoonrakerMixed
                    Napier.i(tag = "MoonrakerDetectionChangeMigration", message = "Checking ${it.id}: affected=$affected")
                    it.copy(affectedByMoonrakerDetectionChange = affected)
                }
            }
        }
    }
}