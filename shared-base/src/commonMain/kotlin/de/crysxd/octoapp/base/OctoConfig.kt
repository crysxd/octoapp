package de.crysxd.octoapp.base

import de.crysxd.octoapp.base.data.models.AnnouncementModel
import io.github.aakira.napier.Napier
import kotlinx.serialization.json.Json
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds
import de.crysxd.octoapp.base.data.models.PurchaseOffers as PurchaseOffersModel

const val PREMIUM_FEATURES_EVERYTHING = "everything_premium"

expect object OctoConfig {
    fun get(field: OctoConfigField<Long>): Long
    fun get(field: OctoConfigField<String>): String
    fun get(field: OctoConfigField<Boolean>): Boolean
    suspend fun fetchAndActivate(): Boolean
}

fun OctoConfig.get(field: OctoConfigField<List<String>>) = get(
    object : OctoConfigField<String>(key = field.key, default = "") {}
).let { string ->
    if (string.isEmpty()) {
        emptyList()
    } else {
        string.split(",").map { it.trim() }
    }
}

inline fun <reified T> OctoConfig.get(field: OctoSerializableConfigField<T>): T = get(
    object : OctoConfigField<String>(key = field.key, default = "") {}
).takeUnless { it.isEmpty() }?.let { text ->
    field.parse(text = text, default = field.default)
} ?: field.default

fun OctoConfig.get(field: OctoDurationConfigField): Duration {
    val value = get(field.field)
    return field.unit.parse(value)
}

abstract class OctoSerializableConfigField<T> internal constructor(
    val key: String,
    val default: T,
) {
    val json = Json {
        ignoreUnknownKeys = true
        coerceInputValues = true
    }

    inline fun <reified T> parse(text: String, default: T): T = try {
        json.decodeFromString(text)
    } catch (e: Exception) {
        Napier.e(tag = "OctoSerializableConfigField", message = "Failed to deserialize: $text", throwable = e)
        default
    }
}

abstract class OctoDurationConfigField internal constructor(
    key: String,
    default: Long,
    internal val unit: TimeUnit
) {
    val field = object : OctoConfigField<Long>(key, default) {}

    internal interface TimeUnit {
        companion object {
            val Milliseconds = object : TimeUnit {
                override fun parse(value: Long) = value.milliseconds
                override fun format(value: Duration) = value.inWholeMilliseconds
            }
            val Seconds = object : TimeUnit {
                override fun parse(value: Long) = value.seconds
                override fun format(value: Duration) = value.inWholeSeconds
            }
        }

        fun parse(value: Long): Duration
        fun format(value: Duration): Long
    }
}

abstract class OctoConfigField<T>(val key: String, val default: T) {
    object ConnectionTimeout : OctoDurationConfigField("connection_timeout_ms", default = 0, unit = TimeUnit.Milliseconds)
    object ReadWriteTimeout : OctoDurationConfigField("read_write_timeout_ms", default = 0, unit = TimeUnit.Milliseconds)
    object WebSocketPingPongTimeout : OctoDurationConfigField("web_socket_ping_pong_timeout_ms", default = 0, unit = TimeUnit.Milliseconds)
    object SignInHelpUrl : OctoConfigField<String>("help_url_sign_in", "https://youtu.be/mlWfaCuvDL8")
    object DefaultPlugins : OctoConfigField<String>("default_plugins", default = "")
    object AndroidNewDnsSdEnabled : OctoConfigField<Boolean>("new_dnssd_enabled", default = true)
    object Faq : OctoConfigField<String>("faq", default = "[]")
    object KnownBugs : OctoConfigField<String>("known_bugs", default = "[]")
    object ContactTimeZone : OctoConfigField<String>("contact_timezone", default = "CET")
    object ContactEmail : OctoConfigField<String>("contact_email", default = "hello@octoapp.eu")
    object IntroVideoUrl : OctoConfigField<String>("introduction_video_url", default = "https://youtu.be/lKJhWnLUrHA")
    object PrinterConnectionTimeout : OctoDurationConfigField("printer_connection_timeout_sec", default = 10, TimeUnit.Seconds)
    object GcodeResponseEndLinePattern : OctoConfigField<String>("gcode_response_end_line_pattern", default = "^Recv:\\s*ok.*\$")
    object PremiumFeatures : OctoConfigField<List<String>>("premium_features", default = listOf(PREMIUM_FEATURES_EVERYTHING))
    object PurchaseOffers : OctoSerializableConfigField<PurchaseOffersModel>("purchase_offers", default = PurchaseOffersModel.Default)
    object Announcement : OctoSerializableConfigField<AnnouncementModel>("announcement", default = AnnouncementModel.Empty)
    object ReviewFlowConditionsMinAppLaunches : OctoConfigField<Long>("review_flow_condition_min_app_launches", default = 3)
    object ReviewFlowConditionsAppUsageMinutes : OctoConfigField<Long>("review_flow_condition_min_app_usage_min", default = 60)
    object ReviewFlowConditionsPrintWasActiveRequired : OctoConfigField<Boolean>("review_flow_condition_print_was_active_required", default = true)
    object ReviewFlowConditionsPauseMinutes : OctoConfigField<Long>("review_flow_condition_pause_between_review_requests_minutes", default = 60 * 24 * 3)
    object UseComposeWebcamAndGcodePreview : OctoConfigField<Boolean>("use_compose_webcam_gcode_preview", default = true)

    object GcodeResponseSpamPattern : OctoConfigField<String>(
        "gcode_response_spam_pattern",
        default = "(Recv:.*echo:busy.*)|(.*Printer seems to support the busy protocol.*)|((Send: (N\\d+\\s+)?M105)|(Recv:\\s+(ok\\s+(([PBN])\\d+\\s+)*)?([BT]\\d*):\\d+).*)"
    )

    object OctoEverywhereAppPortalUrl : OctoConfigField<String>(
        key = "octoeverywhere_app_portal_url",
        default = "https://octoeverywhere.com/appportal/v1?appId=octoapp&authType=enhanced&returnUrl=octoapp%3A%2F%2Fapp.octoapp.eu%2F{{{callbackPath}}}&printerId={{{printerid}}}&appLogoUrl=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Foctoapp-4e438.appspot.com%2Fo%2Fresources%252Foctoeverywhere%252Flogo.png%3Falt%3Dmedia%26token%3D22e614ef-87a9-44a9-96d1-54a3f81dfcad"
    )

    object ObicoAppPortalUrl : OctoConfigField<String>(
        key = "spaghetti_detective_app_portal_url",
        default = "tunnels/new/?app=OctoApp&printer_id={{{printerid}}}&success_redirect_url=octoapp%3A%2F%2Fapp.octoapp.eu%2F{{{callbackPath}}}"
    )
}

