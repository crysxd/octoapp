package de.crysxd.octoapp.base.migrations

import io.github.aakira.napier.Napier
import kotlinx.coroutines.runBlocking

class AllSharedMigrations {

    private val tag = "AllMigrations"
    private val migrations = listOf(
        MoonrakerDetectionChangeMigration()
    )

    suspend fun migrate() {
        migrations.forEach {
            Napier.d(tag = tag, message = "Running ${it::class.simpleName}....")
            it.runMigration()
            Napier.d(tag = tag, message = "Completed ${it::class.simpleName}....")
        }
    }

    fun migrateBlocking() = runBlocking {
        migrate()
    }

    internal interface Migration {
        suspend fun runMigration()
    }
}