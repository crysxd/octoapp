package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.texts.PrinterFindingDescriptionLibrary
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.http.framework.extractAndRemoveBasicAuth
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedcommon.utils.parseHtml
import io.ktor.http.Url
import kotlinx.coroutines.withTimeout
import kotlin.time.Duration.Companion.seconds

class TestNewUrlUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerRepository: PrinterConfigurationRepository,
) : UseCase2<TestNewUrlUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        //region Prepare
        SensitiveDataMask.registerWebUrl(param.newUrl)
        val before = requireNotNull(printerRepository.get(param.instanceId)) { "Instance ${param.instanceId} not found" }
        val finding = when {
            param.fullTest -> {
                logger.i("Performing full test")
                SharedBaseInjector.get().testFullNetworkStackUseCase().execute(
                    TestFullNetworkStackUseCase.Target.Printer(
                        webUrl = param.newUrl.toString(),
                        apiKey = before.apiKey,
                    )
                )
            }

            before.systemInfo?.interfaceType == SystemInfo.Interface.OctoPrint -> {
                logger.i("Skipping test, using OctoPrint")
                TestFullNetworkStackUseCase.Finding.OctoPrintReady(param.newUrl, before.apiKey)
            }

            else -> {
                logger.i("Skipping test, using Moonraker")
                TestFullNetworkStackUseCase.Finding.MoonrakerReady(
                    webUrl = param.newUrl,
                    apiKey = before.apiKey,
                )
            }
        }

        //endregion
        //region Check
        when (finding) {
            is TestFullNetworkStackUseCase.Finding.OctoPrintReady -> withTimeout(30.seconds) {
                val adhoc = SharedBaseInjector.get().printerEngineProvider.createAdHocPrinter(before.copy(webUrl = finding.webUrl), logTag = logger.httpTag)
                try {
                    adhoc.printerProfileApi.getPrinterProfiles()
                } catch (e: InvalidApiKeyException) {
                    throw UnableToVerifySamePrinterException(
                        message = "API key not accepted after URL change",
                        afterUrl = finding.webUrl,
                        beforeUrl = before.webUrl,
                    )
                }
            }

            is TestFullNetworkStackUseCase.Finding.MoonrakerReady -> withTimeout(30.seconds) {
                // Let check we talk to the same instance based on printer profile id, which is a mix of the MCU ids
                logger.i("Creating adhoc instance")
                val adhoc = SharedBaseInjector.get().printerEngineProvider.createAdHocPrinter(before.copy(webUrl = finding.webUrl), logTag = logger.httpTag)
                logger.i("Waiting for printer profiles")
                val afterProfile = adhoc.printerProfileApi.getPrinterProfiles().values.first()
                logger.i("Printer profiles received")
                val beforeProfile = before.activeProfile
                if (beforeProfile?.id != afterProfile.id) {
                    throw UnableToVerifySamePrinterException(
                        message = "Profile ID mismatch: $beforeProfile <==> $afterProfile",
                        afterUrl = finding.webUrl,
                        beforeUrl = before.webUrl,
                    )
                }
            }

            is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> {
                throw UnableToVerifySamePrinterException(
                    message = "API key not accepted after URL change",
                    afterUrl = finding.webUrl,
                    beforeUrl = before.webUrl,
                )
            }

            else -> throw FindingException(finding)
        }
        //endregion
    }

    data class Params(
        val instanceId: String,
        val newUrl: Url,
        val fullTest: Boolean = true,
    )

    class UnableToVerifySamePrinterException(
        message: String,
        beforeUrl: Url,
        afterUrl: Url,
    ) : SuppressedIllegalStateException(message), UserMessageException {
        override val userMessage = getString(
            "sign_in___probe_finding___unable_to_confirm_same_printer",
            beforeUrl.extractAndRemoveBasicAuth().toString(),
            afterUrl.extractAndRemoveBasicAuth().toString()
        )
    }

    private class FindingException(finding: TestFullNetworkStackUseCase.Finding) : SuppressedIllegalStateException(finding::class.simpleName), UserMessageException {
        override val userMessage = PrinterFindingDescriptionLibrary().getExplainerForFinding(finding).parseHtml()
    }
}