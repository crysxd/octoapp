package de.crysxd.octoapp.base.utils.texts

import de.crysxd.octoapp.base.ext.composeTechnicalErrorMessage
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import de.crysxd.octoapp.sharedcommon.utils.getString

class PrinterFindingDescriptionLibrary {

    fun getTitleForFinding(finding: TestFullNetworkStackUseCase.Finding) = when (finding) {
        is TestFullNetworkStackUseCase.Finding.BasicAuthRequired -> getString("sign_in___probe_finding___title_basic_auth")
        is TestFullNetworkStackUseCase.Finding.DnsFailure -> getString("sign_in___probe_finding___title_dns_failure", finding.host)
        is TestFullNetworkStackUseCase.Finding.LocalDnsFailure -> getString("sign_in___probe_finding___title_local_dns_failure", finding.host)
        is TestFullNetworkStackUseCase.Finding.HostNotReachable -> getString("sign_in___probe_finding___title_host_unreachable", finding.host)
        is TestFullNetworkStackUseCase.Finding.HttpsNotTrusted -> getString("sign_in___probe_finding___title_https_not_trusted", finding.host)
        is TestFullNetworkStackUseCase.Finding.InvalidUrl -> getString("sign_in___probe_finding___title_url_syntax")
        is TestFullNetworkStackUseCase.Finding.NotFound -> getString("sign_in___probe_finding___title_octoprint_not_found")
        is TestFullNetworkStackUseCase.Finding.PortClosed -> getString("sign_in___probe_finding___title_port_closed", finding.host, finding.port)
        is TestFullNetworkStackUseCase.Finding.UnexpectedHttpIssue -> getString(
            "sign_in___probe_finding___title_failed_to_connect_via_http",
            finding.host
        )
        is TestFullNetworkStackUseCase.Finding.UnexpectedIssue -> getString("sign_in___probe_finding___title_unexpected_issue")
        is TestFullNetworkStackUseCase.Finding.ServerIsNotOctoPrint -> getString("sign_in___probe_finding___title_might_not_be_octoprint", finding.host)
        is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> getString("sign_in___probe_finding___title_invalid_api_key_moonraker")
        is TestFullNetworkStackUseCase.Finding.OctoPrintReady -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.WebSocketUpgradeFailed -> getString(
            "sign_in___probe_finding___title_websocket_upgrade_failed",
            finding.host
        )

        is TestFullNetworkStackUseCase.Finding.EmptyUrl -> getString("sign_in___probe_finding___title_url_syntax")
        is TestFullNetworkStackUseCase.Finding.MoonrakerReady -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.NoImage -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.WebcamReady -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.ImageCacheOverflow -> "" // Never shown
    }.toString()

    fun getExplainerForFinding(finding: TestFullNetworkStackUseCase.Finding) = when (finding) {
        is TestFullNetworkStackUseCase.Finding.BasicAuthRequired -> getString("sign_in___probe_finding___explainer_basic_auth")
        is TestFullNetworkStackUseCase.Finding.DnsFailure -> getString(
            "sign_in___probe_finding___explainer_dns_failure",
            finding.host,
            finding.webUrl.withoutBasicAuth()
        )
        is TestFullNetworkStackUseCase.Finding.LocalDnsFailure -> getString(
            "sign_in___probe_finding___explainer_local_dns_failure",
            finding.host,
            finding.webUrl.withoutBasicAuth()
        )
        is TestFullNetworkStackUseCase.Finding.HostNotReachable -> getString(
            "sign_in___probe_finding___explainer_host_unreachable",
            finding.host,
            finding.ip,
            finding.webUrl.withoutBasicAuth()
        )
        is TestFullNetworkStackUseCase.Finding.HttpsNotTrusted -> when {
            finding.certificate == null -> getString(
                "sign_in___probe_finding___explainer_https_not_trusted_no_cert",
                finding.webUrl.withoutBasicAuth()
            )
            finding.weakHostnameVerificationRequired -> getString("sign_in___probe_finding___explainer_https_not_trusted_weak_hostname_verification")
            else -> getString("sign_in___probe_finding___explainer_https_not_trusted")
        }
        is TestFullNetworkStackUseCase.Finding.InvalidUrl -> getString(
            "sign_in___probe_finding___explainer_url_syntax",
            finding.webUrl?.withoutBasicAuth() ?: "",
            finding.exception.composeTechnicalErrorMessage()
        )
        is TestFullNetworkStackUseCase.Finding.NotFound -> getString(
            "sign_in___probe_finding___explainer_octoprint_not_found",
            finding.webUrl.withoutBasicAuth()
        )
        is TestFullNetworkStackUseCase.Finding.PortClosed -> getString(
            "sign_in___probe_finding___explainer_port_closed",
            finding.host,
            finding.port,
            finding.webUrl.withoutBasicAuth()
        )
        is TestFullNetworkStackUseCase.Finding.UnexpectedHttpIssue -> getString(
            "sign_in___probe_finding___explainer_failed_to_connect_via_http",
            finding.host,
            finding.exception.composeTechnicalErrorMessage()
        )
        is TestFullNetworkStackUseCase.Finding.UnexpectedIssue -> getString(
            "sign_in___probe_finding___explainer_unexpected_issue",
            finding.exception.composeTechnicalErrorMessage()
        )

        is TestFullNetworkStackUseCase.Finding.ServerIsNotOctoPrint -> getString(
            "sign_in___probe_finding___explainer_might_not_be_octoprint",
            finding.host
        )

        is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> getString("sign_in___probe_finding___explainer_invalid_api_key_moonraker", finding.webUrl.toString())
        is TestFullNetworkStackUseCase.Finding.OctoPrintReady -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.WebSocketUpgradeFailed -> getString(
            "sign_in___probe_finding___explainer_websocket_upgrade_failed",
            finding.host,
            finding.responseCode,
            finding.webSocketUrl
        )

        is TestFullNetworkStackUseCase.Finding.EmptyUrl -> getString(
            "sign_in___probe_finding___explainer_url_syntax",
            finding.webUrl?.withoutBasicAuth().toString(),
            "URL empty"
        )

        is TestFullNetworkStackUseCase.Finding.MoonrakerReady -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.NoImage -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.WebcamReady -> "" // Never shown
        is TestFullNetworkStackUseCase.Finding.ImageCacheOverflow -> "" // Never shown
    }.toString()
}