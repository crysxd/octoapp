package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.DetectBrokenSetupInterceptor
import de.crysxd.octoapp.base.network.NetworkServiceDiscovery
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.cache.HttpDiskCache
import de.crysxd.octoapp.sharedcommon.http.cache.OkioCacheStorage
import de.crysxd.octoapp.sharedcommon.http.config.Dns
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector
import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import org.koin.dsl.module

class NetworkModule(
    private val networkServiceDiscovery: () -> NetworkServiceDiscovery,
    private val dns: () -> CachedDns,
) : BaseModule {

    private fun provideBrokenSetupInterceptorFactory(
        printerConfigRepository: PrinterConfigurationRepository,
    ) = DetectBrokenSetupInterceptor.Factory { octoPrintId ->
        DetectBrokenSetupInterceptor(printerConfigRepository, octoPrintId)
    }

    private fun provideHttpSettings(
        keyStore: KeyStoreProvider,
        proxySelector: ProxySelector,
        cachedDns: CachedDns,
    ) = HttpClientSettings(
        timeouts = Timeouts(),
        logLevel = LogLevel.Production,
        keyStore = keyStore,
        proxySelector = proxySelector,
        dns = cachedDns.takeUnless { it == CachedDns.Noop },
        logTag = "HTTP/Unknown"
    )

    private fun provideHttpCache(
        octoPreferences: OctoPreferences,
    ): HttpDiskCache = OkioCacheStorage(
        fileManager = SharedCommonInjector.get().fileManager,
        settingsStore = SharedCommonInjector.get().settings,
        maxSize = { octoPreferences.httpCacheSize },

        // We ONLY store media and we assume it's immutable. This is the case for OctoPrint although
        // the headers say something else
        allowedContentTypes = OkioCacheStorage.MediaTypes,
        ignoreCacheHeadersAndStoreForEternity = true
    )

    override val koinModule = module {
        single { PrinterEngineProvider(get(), get(), get(), OctoAnalytics, get(), get(), get()) }
        single { provideBrokenSetupInterceptorFactory(get()) }
        single { createKeyStoreProvider(get()) }
        single { createProxySelector(get(), get<CachedDns>()) }
        single { networkServiceDiscovery() }
        single { dns() }
        single { provideHttpCache(get()) }

        factory { provideHttpSettings(get(), get(), get()) }
    }
}

internal expect fun createKeyStoreProvider(platform: Platform): KeyStoreProvider
internal expect fun createProxySelector(platform: Platform, dns: Dns): ProxySelector