package de.crysxd.octoapp.base

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig.DEFAULT_VALUE_FOR_STRING
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.octoapp.base.ext.blockingAwait
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

actual object OctoConfig {
    actual fun get(field: OctoConfigField<Long>): Long = try {
        Firebase.remoteConfig.getString(field.key)
            .takeUnless { it == DEFAULT_VALUE_FOR_STRING }
            ?.toLongOrNull()
            ?: field.default
    } catch (e: IllegalStateException) {
        Napier.w(tag = "OctoConfig", message = "Firebase not initialized, using defaults")
        field.default
    }

    actual fun get(field: OctoConfigField<String>): String = try {
        Firebase.remoteConfig.getString(field.key)
            .takeUnless { it == DEFAULT_VALUE_FOR_STRING }
            ?: field.default
    } catch (e: IllegalStateException) {
        Napier.w(tag = "OctoConfig", message = "Firebase not initialized, using defaults")
        field.default
    }

    actual fun get(field: OctoConfigField<Boolean>): Boolean = try {
        Firebase.remoteConfig.getString(field.key)
            .takeUnless { it == DEFAULT_VALUE_FOR_STRING }
            ?.toBoolean()
            ?: field.default
    } catch (e: IllegalStateException) {
        Napier.w(tag = "OctoConfig", message = "Firebase not initialized, using defaults")
        false
    }

    actual suspend fun fetchAndActivate(): Boolean = withContext(Dispatchers.IO) {
        try {
            Firebase.remoteConfig.fetchAndActivate().blockingAwait()
        } catch (e: Exception) {
            Napier.w(tag = "OctoConfig", message = "Fetch failed", throwable = SuppressedIllegalStateException(cause = e))
            false
        }
    }
}