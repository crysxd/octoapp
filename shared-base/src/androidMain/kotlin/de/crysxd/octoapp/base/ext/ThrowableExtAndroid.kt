package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.ProxyException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.utils.getString
import java.io.IOException

actual fun Throwable.composeErrorMessage(): CharSequence = when (this) {
    is ProxyException -> original.composeErrorMessage()
    is UserMessageException -> userMessage
    is NetworkException -> userFacingMessage ?: message ?: technicalMessage
    is IOException -> message ?: "Failed to resolve host"
    else -> getString("error_general")
}

actual fun Throwable.composeTechnicalErrorMessage(): CharSequence = when (this) {
    is ProxyException -> original.composeTechnicalErrorMessage()
    is NetworkException -> technicalMessage
    else -> message ?: ""
}

internal actual fun Throwable.getStackAddresses() = emptyList<Long>()