package de.crysxd.octoapp.base.logging

import com.google.firebase.crashlytics.FirebaseCrashlytics

class AndroidFirebaseAdapter : FirebaseAntiLog.Adapter {

    private val instance: FirebaseCrashlytics by lazy { FirebaseCrashlytics.getInstance() }

    override fun log(line: String) {
        instance.log(line)
    }

    override fun log(throwableClassName: String, stackTraceAddresses: List<Long>, throwable: Throwable) {
        instance.recordException(throwable)
    }
}