package de.crysxd.octoapp.base.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.utils.ConnectivityHelper.ConnectivityType.Metered
import de.crysxd.octoapp.base.utils.ConnectivityHelper.ConnectivityType.Unmetered
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.isActive
import kotlin.time.Duration.Companion.seconds

private const val Tag = "ConnectivityFlow"

@SuppressLint("MissingPermission")
internal actual val connectivityFlow = flow {
    val manager = SharedCommonInjector.get().platform.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    // Not ideal...but ok for now
    while (currentCoroutineContext().isActive) {
        emit(if (manager.isActiveNetworkMetered) Metered else Unmetered)
        delay(1.seconds)
    }
}.distinctUntilChanged().onEach {
    Napier.i(tag = Tag, message = "[GCD] Connectivity changed: $it")
}.onStart {
    Napier.d(tag = Tag, message = "[GCD] Download flow started")
}.onCompletion {
    Napier.d(tag = Tag, message = "[GCD] Download flow stopped")
}.shareIn(
    scope = AppScope,
    started = SharingStarted.WhileSubscribedOctoDelay,
    replay = 1
) as Flow<ConnectivityHelper.ConnectivityType>