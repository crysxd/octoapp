package de.crysxd.octoapp.base.network.dnssd

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.os.Build
import androidx.core.content.getSystemService
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.utils.AppScope
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import java.net.InetAddress
import java.net.UnknownHostException

class AndroidDnsSd(context: Context) : DnsSd {

    companion object {
        private var instanceCounter = 0
    }

    private val tag = "AndroidDnsSd/${instanceCounter++}"
    private val nsdManager = context.getSystemService<NsdManager>()
    private val lock = Mutex()

    override suspend fun browse(
        regType: String,
        serviceFound: (DnsSd.ServiceData) -> Unit,
        serviceLost: (DnsSd.ServiceData) -> Unit,
        failure: (Throwable) -> Unit
    ): DnsSd.Operation {
        if (nsdManager == null) {
            failure(IllegalStateException("NsdManager not available"))
            return object : DnsSd.Operation {
                override fun stop() = Unit
            }
        } else {
            val discoveryListener = object : NsdManager.DiscoveryListener {

                // Called as soon as service discovery begins.
                override fun onDiscoveryStarted(regType: String) {
                    Napier.d(tag = tag, message = "Service discovery started")
                }

                override fun onServiceFound(service: NsdServiceInfo) {
                    // A service was found! Do something with it.
                    Napier.d(tag = tag, message = "Service discovery success $service")
                    serviceFound(AndroidServiceData(service))
                }

                override fun onServiceLost(service: NsdServiceInfo) {
                    Napier.d(tag = tag, message = "service lost: $service")
                }

                override fun onDiscoveryStopped(serviceType: String) {
                    Napier.d(tag = tag, message = "Discovery stopped : $serviceType")
                }

                override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
                    Napier.d(tag = tag, message = "Discovery failed : Error code:$errorCode")
                    nsdManager.stopServiceDiscovery(this)
                    failure(Exception("NsdManager failed with error code $errorCode"))
                }

                override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
                    Napier.d(tag = tag, message = "Discovery failed : Error code:$errorCode")
                    nsdManager.stopServiceDiscovery(this)
                    failure(Exception("NsdManager failed with error code $errorCode"))
                }
            }

            nsdManager.discoverServices(
                regType,
                NsdManager.PROTOCOL_DNS_SD,
                discoveryListener
            )

            return object : DnsSd.Operation {
                override fun stop() = nsdManager.stopServiceDiscovery(discoveryListener)
            }
        }
    }

    override suspend fun resolve(
        hostName: String,
        resolved: (InetAddress) -> Unit,
        failure: (Throwable) -> Unit
    ): DnsSd.Operation {
        val job = AppScope.launch(Dispatchers.IO) {
            resolveHostNameWithVariances(hostName)?.first?.let {
                resolved(it)
            } ?: failure(UnknownHostException("Unable to resolve $hostName using Android mDNS"))
        }

        return object : DnsSd.Operation {
            override fun stop() = job.cancel()
        }
    }

    override suspend fun resolve(service: DnsSd.ServiceData, resolved: (DnsSd.Service) -> Unit, failure: (Throwable) -> Unit): DnsSd.Operation {
        require(service is AndroidServiceData)
        if (nsdManager == null) {
            failure(IllegalStateException("NsdManager not available"))
            return object : DnsSd.Operation {
                override fun stop() = Unit
            }
        } else {
            val resolveListener = object : NsdManager.ResolveListener {
                override fun onResolveFailed(service: NsdServiceInfo, errorCode: Int) {
                    Napier.d(tag = tag, message = "Resolve failed : Error code:$errorCode")
                    failure(Exception("NsdManager failed with error code $errorCode"))
                    lock.unlock()
                }

                override fun onServiceResolved(service: NsdServiceInfo) {
                    Napier.d(tag = tag, message = "Resolve success : $service")
                    requireNotNull(service.primaryHost)

                    val hostName = resolveHostNameWithVariances(service.primaryHost.hostName)?.second
                        ?: return failure(Exception("Unusable result: ${service.primaryHost.hostName}"))

                    SharedBaseInjector.get().dnsResolver.addCacheEntry(
                        CachedDns.Entry(
                            hostname = hostName,
                            resolvedIpString = listOfNotNull(service.primaryHost.hostAddress),
                        )
                    )

                    resolved(
                        DnsSd.Service(
                            label = service.serviceName,
                            host = service.primaryHost,
                            hostname = hostName,
                            port = service.port,
                            webUrl = "http://$hostName:${service.port}/"
                        )
                    )
                    lock.unlock()
                }
            }

            lock.lock()
            Napier.i(tag = tag, message = "Resolving ${service.info}")

            // Let's accept this deprecation. We more and more rely on Android to resolve .local anyways, so this is on it's way out.
            // Our architecture also doesn't allow updates, so we couldn't handle updates to the resolved service as provided by registerServiceInfoCallback(...).
            @Suppress("DEPRECATION")
            nsdManager.resolveService(
                service.info,
                resolveListener
            )

            return object : DnsSd.Operation {
                override fun stop() {
                    Napier.d(tag = tag, message = "Stopped for ${service.info}")
                    lock.unlock()
                }
            }
        }
    }

    private fun resolveHostNameWithVariances(hostName: String) = listOf(
        (hostName.removeSuffix(".home") + ".local").replace(".local.local", ".local"),
        (hostName.removeSuffix(".local") + ".home").replace(".home.home", ".home"),
        hostName,
    ).distinct().firstNotNullOfOrNull {
        try {
            InetAddress.getByName(it) to it
        } catch (e: UnknownHostException) {
            Napier.v(tag = tag, message = "Unable to resolve $it")
            null
        }
    }

    private val NsdServiceInfo.primaryHost
        get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            hostAddresses.first()
        } else {
            @Suppress("DEPRECATION")
            host
        }

    data class AndroidServiceData(
        val info: NsdServiceInfo,
    ) : DnsSd.ServiceData {
        override val description = "${info.serviceType} ${info.serviceName}"
    }
}