package de.crysxd.octoapp.base.ext

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.PurchaseOffers
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier

val FirebaseRemoteConfig.purchaseOffers: PurchaseOffers
    get() = if (BillingManager.shouldAdvertisePremium()) {
        purchaseOffersForced
    } else {
        PurchaseOffers.Default
    }


val FirebaseRemoteConfig.purchaseOffersForced: PurchaseOffers
    get() = try {
        val json = getString("purchase_offers").takeIf { it.isNotEmpty() } ?: "{}"
        val m = SharedCommonInjector.get().json.decodeFromString<PurchaseOffers>(json)
        // "Testing" the object to ensure everything is decoded correctly
        m.activeConfig.textsWithData.highlightBanner
        m.activeConfig.sellingPoints.firstOrNull()
        m.baseConfig.textsWithData.highlightBanner
        m.baseConfig.sellingPoints.firstOrNull()
        m.purchaseSku
        m.subscriptionSku
        m.saleConfigs
        m
    } catch (e: Exception) {
        Napier.w(tag = "FirebaseRemoteConfigExt", throwable = e, message = "Failed to decode offers, falling back")
        null
    } ?: PurchaseOffers.Default
