package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.sharedcommon.Platform
import io.ktor.client.request.forms.InputProvider
import io.ktor.utils.io.bits.Memory
import io.ktor.utils.io.bits.storeByteArray
import io.ktor.utils.io.core.Input
import okio.FileSystem
import okio.Path.Companion.toPath
import okio.buffer
import okio.use
import kotlin.math.min

internal actual fun UploadFileHandle(
    path: String,
    platform: Platform,
    fileSystem: FileSystem
) = UploadFileUseCase.UploadFileHandle(
    name = path.toPath().name,
    withInput = { block ->
        fileSystem.openReadOnly(path.toPath()).use { file ->
            val provider = InputProvider(file.size() - 1) {
                object : Input() {
                    val source = file.source().buffer()
                    val buffer = ByteArray(8192)
                    var written = 0

                    override fun closeSource() = file.close()

                    override fun fill(destination: Memory, offset: Int, length: Int): Int {
                        var leftToRead = length
                        var totalCount = 0

                        do {
                            // Read, up to buffer.size but at most what's requested
                            val requestCount = min(leftToRead, buffer.size)
                            val actualCount = source.read(sink = buffer, offset = 0, byteCount = requestCount)


                            // If we got data, forward
                            if (actualCount >= 0) {
                                destination.storeByteArray(offset = offset + totalCount, source = buffer, sourceOffset = 0, count = actualCount)
                            }

                            // Count
                            leftToRead -= actualCount
                            totalCount += actualCount
                        } while (leftToRead > 0 && actualCount >= 0)
                        written += totalCount

                        if (totalCount < 0) {
                            closeSource()
                            return 0
                        }

                        // Can't return -1
                        return totalCount.coerceAtLeast(0)
                    }
                }
            }

            block(provider)
        }
    },
)