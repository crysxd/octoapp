package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.http.config.Dns
import de.crysxd.octoapp.sharedcommon.http.config.KeyStore
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector

actual fun createKeyStoreProvider(platform: Platform) = object : KeyStoreProvider {
    override fun loadKeyStore(): KeyStore? = null
    override fun getWeakVerificationForHosts(): List<String> = emptyList()
}

actual fun createProxySelector(platform: Platform, dns: Dns): ProxySelector = ProxySelector.Noop