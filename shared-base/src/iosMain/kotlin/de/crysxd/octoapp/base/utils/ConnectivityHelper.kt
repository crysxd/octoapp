package de.crysxd.octoapp.base.utils

import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart

interface ConnectivityHelperCore {

    companion object {
        lateinit var factory: () -> ConnectivityHelperCore
    }

    fun startObserving(onChange: (ConnectivityHelper.ConnectivityType) -> Unit)
    fun stopObserving()
}

private const val tag = "ConnectivityHelperCore"

internal actual val connectivityFlow = flow {
    val core = ConnectivityHelperCore.factory()
    val internalFlow = MutableStateFlow<ConnectivityHelper.ConnectivityType?>(null)
    val internalFlowWithEvents = internalFlow
        .filterNotNull()
        .onStart {
            Napier.d(tag = tag, message = "Starting connectivity flow")
            core.startObserving { internalFlow.value = it }
        }
        .onCompletion {
            Napier.d(tag = tag, message = "Completing connectivity flow")
            core.stopObserving()
        }
        .distinctUntilChanged()

    emitAll(internalFlowWithEvents)
}