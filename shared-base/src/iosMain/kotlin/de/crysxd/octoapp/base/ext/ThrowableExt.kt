package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.ProxyException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedcommon.utils.parseHtml
import io.ktor.client.engine.darwin.DarwinHttpRequestException
import kotlin.experimental.ExperimentalNativeApi

actual fun Throwable.composeErrorMessage(): CharSequence = when (this) {
    is ProxyException -> original.composeErrorMessage()
    is UserMessageException -> userMessage
    is NetworkException -> (originalCause as? DarwinHttpRequestException)?.composeErrorMessage() ?: (userFacingMessage ?: message ?: technicalMessage)
    is DarwinHttpRequestException -> origin.localizedDescription
    is okio.IOException, is io.ktor.utils.io.errors.IOException -> message ?: "Unknown network error"
    else -> getString("error_general")
}.parseHtml().toString()


actual fun Throwable.composeTechnicalErrorMessage(): CharSequence = when (this) {
    is ProxyException -> original.composeTechnicalErrorMessage()
    is NetworkException -> when (val oc = originalCause) {
        // Prefer iOS's error message in case it's a DarwinHttpRequestException
        is DarwinHttpRequestException -> oc.composeTechnicalErrorMessage()
        else -> technicalMessage
    }
    is DarwinHttpRequestException -> "${origin.localizedDescription} (${origin.userInfo["NSErrorFailingURLStringKey"] ?: origin.userInfo})"
    else -> message ?: ""
}.parseHtml().toString()

@ExperimentalNativeApi
internal actual fun Throwable.getStackAddresses(): List<Long> {
    val addresses = getStackTraceAddresses()
    val boilerplateCount = getStackTrace().takeWhile { element ->
        element.contains("Exceptions.kt") || throwableBoilerplate(element, "Exception") || throwableBoilerplate(element, "Throwable")
    }.size

    return addresses.drop(boilerplateCount)
}

private fun throwableBoilerplate(frameString: String, lookFor: String): Boolean {
    return frameString.contains("kotlin.$lookFor") || frameString.contains("$lookFor#<init>")
}