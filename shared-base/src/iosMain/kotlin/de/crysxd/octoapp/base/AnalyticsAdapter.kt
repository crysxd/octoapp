package de.crysxd.octoapp.base


actual class AnalyticsAdapter actual constructor() {
    actual fun logEvent(event: String, params: Map<String, Any?>) = Unit
    actual fun setUserProperty(property: String, value: String?) = Unit
}