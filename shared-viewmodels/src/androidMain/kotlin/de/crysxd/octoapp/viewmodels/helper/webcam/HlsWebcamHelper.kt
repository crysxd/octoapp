package de.crysxd.octoapp.viewmodels.helper.webcam

import androidx.annotation.OptIn
import androidx.media3.common.util.UnstableApi
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import kotlinx.coroutines.flow.FlowCollector

internal actual class HlsWebcamHelper actual constructor(private val core: BaseViewModelCore) {

    private val tag = "HlsWebcamHelper"
    private val exoHelper = ExoWebcamHelper()

    @OptIn(UnstableApi::class)
    actual suspend fun emit(
        collector: FlowCollector<WebcamControlsViewModelCore.State>,
        settings: ResolvedWebcamSettings.HlsSettings,
        activeWebcam: Int,
        webcamCount: Int,
    ) = exoHelper.emit(
        collector = collector,
        webcamCount = webcamCount,
        activeWebcam = activeWebcam,
        webcam = settings.webcam,
        url = settings.url.toString(),
        authHeader = settings.basicAuth,
        featureName = "HLS"
    )
}