package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State
import kotlinx.coroutines.flow.FlowCollector

internal expect class HlsWebcamHelper(core: BaseViewModelCore) {

    suspend fun emit(
        collector: FlowCollector<State>,
        settings: ResolvedWebcamSettings.HlsSettings,
        activeWebcam: Int,
        webcamCount: Int,
    )
}
