package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.PrintNameStyle.Full
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.PrintNameStyle.None
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.models.ProgressControlsRole
import de.crysxd.octoapp.base.models.ProgressControlsRole.ForNormal
import de.crysxd.octoapp.base.models.ProgressControlsRole.ForWebcamFullscreen
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.base.utils.mapData
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_ANALYSIS
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_AVERAGE
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_ESTIMATE
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_GENIUS
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_LINEAR
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_MIXED_ANALYSIS
import de.crysxd.octoapp.engine.models.job.ProgressInformation.Companion.ORIGIN_MIXED_AVERAGE
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.SystemInfo.Capability.RealTimeStats
import de.crysxd.octoapp.engine.octoprint.dto.message.CompanionPluginMessage
import de.crysxd.octoapp.sharedcommon.ext.formatAsLayerHeight
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsDuration
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsEta
import de.crysxd.octoapp.sharedcommon.ext.formatAsSpeed
import de.crysxd.octoapp.sharedcommon.ext.formatAsVolumeOverTime
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class ProgressControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "ProgressControlsViewModelCore"
    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val preferences = SharedBaseInjector.get().preferences
    private val loadFileUseCase = SharedBaseInjector.get().loadFilesUseCase()
    private val configRepository = SharedBaseInjector.get().printerConfigRepository
    private val gcodeCore = GcodePreviewControlsViewModelCore(instanceId)

    companion object {
        val UPDATE_INTERVAL = 0.35.seconds
    }

    private val currentMessageFlow = printerProvider.passiveCurrentMessageFlow(
        tag = "progress/current",
        instanceId = instanceId
    )

    private val pluginMessageFlow = printerProvider.passiveCachedMessageFlow(
        tag = "progress/plugin",
        instanceId = instanceId,
        clazz = CompanionPluginMessage::class
    ).onStart { emit(null) }

    private val activeFile = currentMessageFlow.distinctUntilChangedBy { it.job?.file }.flatMapLatest {
        val activeFile = it.job?.file ?: return@flatMapLatest flowOf(FlowState.Loading())
        val params = LoadFilesUseCase.Params(fileOrigin = activeFile.origin, path = activeFile.path, instanceId = instanceId, skipCache = false)
        loadFileUseCase.execute(params).mapData { s ->
            s as FileObject.File
        }
    }.catch {
        FlowState.Error<FlowState<FileObject.File>>(it)
        Napier.e(tag = tag, message = "Getting file failed", throwable = it)
    }

    private val settingsFlow = preferences.updatedFlow2.combine(
        configRepository.instanceInformationFlow(instanceId = instanceId)
    ) { prefs, config ->
        prefs.progressWidgetSettings.adoptForCapabilities(
            capabilities = config?.systemInfo?.capabilities ?: emptyList()
        )
    }.distinctUntilChanged()

    private val fullscreenSettingsFlow = preferences.updatedFlow2.combine(
        configRepository.instanceInformationFlow(instanceId = instanceId)
    ) { prefs, config ->
        prefs.webcamFullscreenProgressWidgetSettings.adoptForCapabilities(
            capabilities = config?.systemInfo?.capabilities ?: emptyList()
        )
    }.distinctUntilChanged()

    val state = combine(
        currentMessageFlow,
        pluginMessageFlow,
        settingsFlow.combine(fullscreenSettingsFlow) { a, b -> a to b },
        gcodeCore.state,
        activeFile,
    ) { current, plugin, (settings, fullscreenSettings), gcode, file ->
        State(
            activeFile = file,
            settings = settings,
            fullscreenSettings = fullscreenSettings,
            dataItems = createDataItems(
                current = current,
                settings = settings,
                gcode = gcode,
                displayMessage = plugin?.m117 ?: current.displayMessage,
            ),
            fullscreenDataItems = createProgressDataItem(current) + createDataItems(
                current = current,
                settings = fullscreenSettings,
                gcode = gcode,
                displayMessage = plugin?.m117 ?: current.displayMessage,
            ),
            completion = current.progress?.completion?.div(100f) ?: 0f,
            status = current.statusText,
            printing = current.state.flags.isPrinting()
        )
    }.distinctUntilChanged().flowOn(Dispatchers.Default).rateLimit(UPDATE_INTERVAL.inWholeMilliseconds)

    fun createDataItems(
        current: Message.Current?,
        settings: ProgressWidgetSettings,
        displayMessage: String?,
        gcode: GcodePreviewControlsViewModelCore.State?,
    ) = createTimeDataItem(
        current = current,
        settings = settings,
    ) + createRealTimeDataItems(
        current = current,
        settings = settings
    ) + createGcodeDataItems(
        gcode = gcode,
        settings = settings,
    ) + createAdditionalDataItems(
        current = current,
        settings = settings,
        displayMessage = displayMessage
    )

    private fun createProgressDataItem(
        current: Message.Current?,
    ) = listOf(
        State.DataItem(
            label = getString("progress_widget___title"),
            value = current?.progress?.completion?.formatAsPercent() ?: "",
        )
    )

    private fun createTimeDataItem(
        current: Message.Current?,
        settings: ProgressWidgetSettings,
    ) = listOfNotNull(
        State.DataItem(
            label = getString("progress_widget___time_spent"),
            value = current?.progress?.printTime?.formatAsSecondsDuration() ?: "",
        ).takeIf { settings.showUsedTime },
        State.DataItem(
            label = getString("progress_widget___time_left"),
            value = current?.progress?.printTimeLeft?.formatAsSecondsDuration() ?: ""
        ).takeIf { settings.showLeftTime },
        State.DataItem(
            label = getString("progress_widget___eta"),
            value = current?.progress?.printTimeLeft?.formatAsSecondsEta(
                useCompactFutureDate = settings.etaStyle == ProgressWidgetSettings.EtaStyle.Compact,
                allowRelative = false,
                showLabel = false,
            ) ?: "",
            iconColor = when (current?.progress?.printTimeLeftOrigin) {
                ORIGIN_LINEAR -> State.Color.Red
                ORIGIN_ANALYSIS, ORIGIN_MIXED_ANALYSIS -> State.Color.Orange
                ORIGIN_AVERAGE, ORIGIN_MIXED_AVERAGE, ORIGIN_ESTIMATE -> State.Color.Green
                ORIGIN_GENIUS -> State.Color.Yellow
                else -> null
            },
            icon = when (current?.progress?.printTimeLeftOrigin) {
                ORIGIN_GENIUS -> State.Icon.Star
                else -> State.Icon.Circle
            },
        ).takeIf { settings.etaStyle != ProgressWidgetSettings.EtaStyle.None },
    )

    private fun createRealTimeDataItems(
        current: Message.Current?,
        settings: ProgressWidgetSettings,
    ) = listOfNotNull(
        State.DataItem(
            label = getString("progress_widget___speed"),
            value = current?.realTimeStats?.toolhead?.speedMmPerS?.formatAsSpeedDefault() ?: "",
            valueLayoutClue = "00" + 0.formatAsSpeedDefault(),
        ).takeIf { settings.showSpeed },
        State.DataItem(
            label = getString("progress_widget___max_speed"),
            value = current?.realTimeStats?.toolhead?.maxpeedMmPerS?.formatAsSpeedDefault() ?: "",
            valueLayoutClue = "00" + 0.formatAsSpeedDefault(),
        ).takeIf { settings.showMaxSpeed },
        State.DataItem(
            label = getString("progress_widget___flow"),
            value = current?.realTimeStats?.extruder?.flowMm3PerS?.formatAsVolumeOverTimeDefault() ?: "",
            valueLayoutClue = "0" + 0.formatAsVolumeOverTimeDefault(),
        ).takeIf { settings.showFlow },
        State.DataItem(
            label = getString("progress_widget___max_flow"),
            value = current?.realTimeStats?.extruder?.maxFlowMm3PerS?.formatAsVolumeOverTimeDefault() ?: "",
            valueLayoutClue = "0" + 0.formatAsVolumeOverTimeDefault(),
        ).takeIf { settings.showMaxFlow },

        State.DataItem(
            label = getString("progress_widget___filament_used"),
            value = current?.realTimeStats?.extruder?.filamentUsedMm?.formatAsLength(minDecimals = 1, maxDecimals = 1) ?: "",
            valueLayoutClue = "0" + 0.formatAsVolumeOverTimeDefault(),
        ).takeIf { settings.showFilamentUsed },
    )

    private fun createAdditionalDataItems(
        current: Message.Current?,
        settings: ProgressWidgetSettings,
        displayMessage: String?,
    ) = listOfNotNull(
        State.DataItem(
            label = getString("progress_widget___print_name"),
            value = current?.job?.file?.display ?: "",
            largeValue = true,
            maxLines = if (settings.printNameStyle == Full) 5 else 1,
        ).takeIf { settings.printNameStyle != None },
        State.DataItem(
            label = getString("progress_widget___printer_message"),
            value = displayMessage ?: getString("progress_widget___printer_message_no_message"),
            largeValue = true,
            maxLines = 2,
        ).takeIf { settings.showPrinterMessage },
    )

    private fun createGcodeDataItems(
        gcode: GcodePreviewControlsViewModelCore.State?,
        settings: ProgressWidgetSettings,
    ): List<State.DataItem> {
        if (!settings.showLayer && !settings.showZHeight) return emptyList()

        val unavailable = getString("progress_widget___gcode_unavailable")
        val largeFile = getString("progress_widget___gcode_large_file")
        val loading = getString("progress_widget___gcode_loading")
        val (layer, zHeight) = when (gcode) {
            null -> unavailable to unavailable
            is GcodePreviewControlsViewModelCore.State.DataReady -> gcode.renderContext.currentData.let { context ->
                val layerNumber = context.layerNumberDisplay(context.layerNumber)
                val layerCount = context.layerCountDisplay(context.layerCount)
                val layer = "$layerNumber/$layerCount (${context.layerProgress.times(100).formatAsPercent(minDecimals = 1, maxDecimals = 1)})"
                val zHeight = context.layerZHeight.formatAsLayerHeight()
                layer to zHeight
            }

            is GcodePreviewControlsViewModelCore.State.Error -> unavailable to unavailable
            is GcodePreviewControlsViewModelCore.State.FeatureDisabled -> unavailable to unavailable
            is GcodePreviewControlsViewModelCore.State.LargeFileDownloadRequired -> largeFile to largeFile
            is GcodePreviewControlsViewModelCore.State.Loading -> loading to loading
            is GcodePreviewControlsViewModelCore.State.PrintingFromSdCard -> unavailable to unavailable
        }

        return listOfNotNull(
            State.DataItem(
                label = getString("progress_widget___layer"),
                value = layer,
            ).takeIf { settings.showLayer },
            State.DataItem(
                label = getString("progress_widget___z_height"),
                value = zHeight,
            ).takeIf { settings.showZHeight },
        )
    }

    private fun Number.formatAsSpeedDefault() = formatAsSpeed(maxDecimals = 0)

    private fun Number.formatAsVolumeOverTimeDefault() = formatAsVolumeOverTime(minDecimals = 1, maxDecimals = 1)

    private fun ProgressWidgetSettings.adoptForCapabilities(capabilities: List<SystemInfo.Capability>) = copy(
        showFlow = showFlow && RealTimeStats in capabilities,
        showSpeed = showSpeed && RealTimeStats in capabilities,
        showMaxFlow = showMaxFlow && RealTimeStats in capabilities,
        showMaxSpeed = showMaxSpeed && RealTimeStats in capabilities,
        showFilamentUsed = showFilamentUsed && RealTimeStats in capabilities,
    )

    data class State(
        val activeFile: FlowState<FileObject.File>,
        val settings: ProgressWidgetSettings,
        val fullscreenSettings: ProgressWidgetSettings,
        val fullscreenDataItems: List<DataItem>,
        val dataItems: List<DataItem>,
        val completion: Float,
        val status: String,
        val printing: Boolean,
    ) {
        data class DataItem(
            val label: String,
            val value: String,
            val largeValue: Boolean = false,
            val valueLayoutClue: String = "",
            val maxLines: Int = 1,
            val icon: Icon? = null,
            val iconColor: Color? = null,
        )

        enum class Icon {
            Circle,
            Star,
        }

        enum class Color {
            Red,
            Orange,
            Yellow,
            Green,
        }


        fun settingsForRole(role: ProgressControlsRole) = when (role) {
            ForNormal -> settings
            ForWebcamFullscreen -> fullscreenSettings
        }

        fun dataItemsForRole(role: ProgressControlsRole) = when (role) {
            ForNormal -> dataItems
            ForWebcamFullscreen -> fullscreenDataItems
        }
    }
}