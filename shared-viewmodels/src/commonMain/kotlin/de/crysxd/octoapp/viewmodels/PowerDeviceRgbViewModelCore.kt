package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

class PowerDeviceRgbViewModelCore(
    instanceId: String,
    type: String,
) : BaseViewModelCore(instanceId) {

    val colors = flow {
        val nested = SharedBaseInjector.getOrNull()?.getPowerDevicesUseCase()?.execute(
            param = GetPowerDevicesUseCase.Params(onlyGetDeviceWithUniqueId = type.split("/").last())
        )?.results?.firstOrNull()?.state?.invoke()?.map { state ->
            val colors = (state as? GetPowerDevicesUseCase.PowerState.Color)?.colors ?: emptyList()
            Colors(colors.take(50))
        } ?: emptyFlow()

        emitAll(nested)
    }

    data class Colors(
        val colors: List<PowerDevice.RgbwColor>
    )
}