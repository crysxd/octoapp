package de.crysxd.octoapp.viewmodels.helper.startprint

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

class StartPrintHelper(private val viewModelCore: BaseViewModelCore) {

    private val tag = "StartPrintHelper"
    private val startPrintJobUseCase = SharedBaseInjector.get().startPrintJobUseCase()
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    suspend fun startPrint(file: FileObject.File?, materialConfirmed: Boolean, timelapseConfirmed: Boolean): Result = try {
        val res = startPrintJobUseCase.execute(
            StartPrintJobUseCase.Params(
                file = requireNotNull(file) { "Illegal state: file is null" },
                materialSelectionConfirmed = materialConfirmed,
                timelapseConfigConfirmed = timelapseConfirmed,
                instanceId = viewModelCore.instanceId,
            )
        )

        // Ensure the instance we started the print on is activated
        AppScope.launch {
            delay(1.seconds)
            printerConfigRepository.setActive(instanceId = viewModelCore.instanceId, trigger = "start-print")
        }

        when (res) {
            StartPrintJobUseCase.Result.MaterialSelectionRequired -> Result.MaterialConfirmationRequired(timelapseConfirmed = timelapseConfirmed)
            StartPrintJobUseCase.Result.TimelapseConfigRequired -> Result.TimelapseConfirmationRequired(materialConfirmed = materialConfirmed)
            StartPrintJobUseCase.Result.PrintStarted -> Result.Started
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to start print job", throwable = e)
        viewModelCore.reportException(e)
        Result.Failed
    }

    sealed class Result {
        data object Started : Result()
        data object Failed : Result()
        data class MaterialConfirmationRequired(val timelapseConfirmed: Boolean) : Result()
        data class TimelapseConfirmationRequired(val materialConfirmed: Boolean) : Result()
    }
}