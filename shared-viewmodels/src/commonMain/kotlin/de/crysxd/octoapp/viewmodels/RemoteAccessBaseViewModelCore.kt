package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow

abstract class RemoteAccessBaseViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    protected abstract val tag: String
    protected abstract val remoteServiceName: String
    protected abstract val loadingFlow: Flow<Boolean>
    private val getUrlUseCase = SharedBaseInjector.get().getRemoteServiceConnectUrlUseCase()
    private val setAlternativeWebUrlUseCase = SharedBaseInjector.get().setAlternativeWebUrlUseCase()
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    private val safeLoadingFlow = flow {
        emit(false)
        emitAll(loadingFlow)
    }

    val connectedState = printerConfigRepository.instanceInformationFlow(instanceId)
        .distinctUntilChangedBy { it?.alternativeWebUrl to it?.remoteConnectionFailure }
        .combine(safeLoadingFlow) { config, loading ->
            State(
                connected = config?.alternativeWebUrl?.isMyUrl() == true,
                url = config?.alternativeWebUrl?.let(::modifyUrl)?.takeIf { it.isMyUrl() }?.toString(),
                failure = config?.remoteConnectionFailure?.takeIf { it.remoteServiceName == remoteServiceName },
                loading = loading,
            )
        }

    open fun modifyUrl(url: Url): Url = url

    abstract fun Url.isMyUrl(): Boolean

    protected suspend fun getLoginUrl(remoteService: GetRemoteServiceConnectUrlUseCase.RemoteService): String? = try {
        val params = GetRemoteServiceConnectUrlUseCase.Params(
            remoteService = remoteService,
            instanceId = instanceId
        )

        when (val res = getUrlUseCase.execute(params)) {
            is GetRemoteServiceConnectUrlUseCase.Result.Error -> throw res.exception
            is GetRemoteServiceConnectUrlUseCase.Result.Success -> res.url
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to get OctoEverywhere login URL", throwable = e)
        reportException(e)
        null
    }

    suspend fun disconnect() = applyUrl(url = "", user = "", password = "", skipTest = false)

    protected open suspend fun applyUrl(url: String, user: String, password: String, skipTest: Boolean): Boolean = try {
        val params = SetAlternativeWebUrlUseCase.Params(
            instanceId = instanceId,
            webUrl = url,
            username = user,
            password = password,
            bypassChecks = skipTest
        )
        when (val res = setAlternativeWebUrlUseCase.execute(params)) {
            is SetAlternativeWebUrlUseCase.Result.Failure -> throw RemoteServiceLoginException(userMessage = res.errorMessage, cause = res.exception)
            SetAlternativeWebUrlUseCase.Result.Success -> true
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to apply url", throwable = e)
        reportException(e)
        false
    }

    data class State(
        val connected: Boolean,
        val loading: Boolean,
        val url: String?,
        val failure: RemoteConnectionFailure?,
    )

    class RemoteServiceLoginException(override val userMessage: String, cause: Throwable) : SuppressedIllegalStateException(userMessage, cause), UserMessageException


}