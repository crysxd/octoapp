package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.models.Faq
import de.crysxd.octoapp.base.models.KnownBug
import de.crysxd.octoapp.base.utils.isForPlatform
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier
import kotlinx.serialization.json.Json

class HelpLauncherViewModelCore : BaseViewModelCore("") {

    private val tag = "HelpLauncherViewModelCore"

    val faq: List<Faq>
        get() = try {
            val platform = SharedCommonInjector.get().platform
            Json.decodeFromString<List<Faq>>(OctoConfig.get(OctoConfigField.Faq)).filter {
                it.platform.isForPlatform(platform)
            }.filter {
                !it.title.isNullOrBlank() && !it.content.isNullOrBlank()
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to decode FAQ", throwable = e)
            emptyList()
        }

    val knownBugs: List<KnownBug>
        get() = try {
            val platform = SharedCommonInjector.get().platform
            Json.decodeFromString<List<KnownBug>>(OctoConfig.get(OctoConfigField.KnownBugs)).filter {
                it.platform.isForPlatform(platform)
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to decode known bugs. Unknown bug?", throwable = e)
            emptyList()
        }

    val introUrl: String = OctoConfig.get(OctoConfigField.IntroVideoUrl)
}