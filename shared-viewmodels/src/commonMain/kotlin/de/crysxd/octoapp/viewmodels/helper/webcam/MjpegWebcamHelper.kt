package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import de.crysxd.octoapp.sharedexternalapis.mjpeg.heightPx
import de.crysxd.octoapp.sharedexternalapis.mjpeg.widthPx
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State
import de.crysxd.octoapp.viewmodels.helper.LowOverheadMediator
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.datetime.Clock

internal class MjpegWebcamHelper(private val core: BaseViewModelCore) {

    private val httpClientSettings get() = SharedBaseInjector.get().httpClientSettings()
    private val tag = "MjpegWebcamHelper"

    suspend fun emit(
        collector: FlowCollector<State>,
        settings: ResolvedWebcamSettings.MjpegSettings,
        activeWebcam: Int,
        webcamCount: Int,
    ) {
        var mediator: LowOverheadMediator<State.MjpegReady.Frame>? = null

        val connection = MjpegConnection3(
            httpSettings = httpClientSettings.copy(
                extraHeaders = httpClientSettings.extraHeaders + settings.extraHeaders
            ),
            streamUrl = settings.url,
            name = "WebcamControlsViewModel",
            maxSize = null,
        )

        Napier.i(tag = tag, message = "Creating new flow")
        val flow = connection.load().buffer(capacity = 4).map { ms ->
            when (ms) {
                is MjpegConnection3.MjpegSnapshot.Frame -> {
                    val frame = State.MjpegReady.Frame(
                        image = ms.frame,
                        fps = ms.analytics.fps,
                        frameTime = Clock.System.now().toEpochMilliseconds(),
                        frameInterval = -1,
                    )

                    val m = mediator?.also { it.publishData(frame) } ?: let {
                        val m2 = LowOverheadMediator(frame)
                        Napier.i(tag = tag, message = "Creating new mediator: $m2")
                        m2
                    }
                    mediator = m

                    State.MjpegReady(
                        frames = m,
                        flipH = settings.webcam.flipH,
                        flipV = settings.webcam.flipV,
                        rotate90 = settings.webcam.rotate90,
                        webcamCount = webcamCount,
                        activeWebcam = activeWebcam,
                        frameWidth = ms.frame.widthPx,
                        frameHeight = ms.frame.heightPx,
                        frameInterval = -1,
                        displayName = settings.webcam.displayName,
                    )
                }

                MjpegConnection3.MjpegSnapshot.Loading -> State.Loading(
                    webcamCount = webcamCount,
                    activeWebcam = activeWebcam,
                    displayName = settings.webcam.displayName,
                )
            }
        }.distinctUntilChangedBy { (it as? State.MjpegReady)?.frames }.onStart {
            Napier.i(tag = tag, message = "Starting to emit")
        }.onCompletion {
            Napier.i(tag = tag, message = "Stopping")
        }

        collector.emitAll(flow)
    }
}


