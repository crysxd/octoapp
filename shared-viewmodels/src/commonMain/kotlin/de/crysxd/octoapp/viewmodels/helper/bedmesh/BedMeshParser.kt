package de.crysxd.octoapp.viewmodels.helper.bedmesh

import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.viewmodels.helper.bedmesh.ParsedBedMesh.Sector
import io.github.aakira.napier.Napier
import kotlin.math.pow
import kotlin.math.sqrt

class BedMeshParser {

    private val tag = "BedMeshParser"

    fun parse(
        meshX: List<Float>,
        meshY: List<Float>,
        mesh: List<List<Float?>>,
        colorScale: List<Pair<Float, HexColor>>,
        graphZMin: Float,
        graphZMax: Float,
        bedMinX: Float,
        bedMinY: Float,
        bedMaxX: Float,
        bedMaxY: Float,
    ): ParsedBedMesh {
        if (meshX.isEmpty() || meshY.isEmpty() || mesh.isEmpty()) {
            return ParsedBedMesh(sectors = emptyList(), legend = emptyList())
        }

        require(colorScale.isNotEmpty()) { "Color scale cannot be empty" }
        require(graphZMin < graphZMax) { "graphZMin must be less than graphZMax" }
        require(meshX.isNotEmpty()) { "meshX cannot be empty" }
        require(meshY.isNotEmpty()) { "meshY cannot be empty" }
        require(mesh.isNotEmpty()) { "mesh cannot be empty" }

        if (mesh.flatten().filterNotNull().isEmpty()) {
            Napier.i(tag = tag, message = "Bed mesh is empty, returning empty result")
            return ParsedBedMesh(sectors = emptyList(), legend = emptyList())
        }

        Napier.i(tag = tag, message = "Parsing bed mesh with ${mesh.size}x${mesh.firstOrNull()?.size} points")
        val size = (meshX.size + meshY.size) / 2
        val fillIterations = when (size) {
            in 2..3 -> 4
            in 3..5 -> 3
            in 5..8 -> 2
            in 8..12 -> 1
            else -> 0
        }
        val meshXFilled = meshX.fillList(iterations = fillIterations)
        val meshYFilled = meshY.fillList(iterations = fillIterations)

        val (minZ, maxZ) = mesh.flatten().let { it.filterNotNull().min() to it.filterNotNull().max() }
        val midZ = (minZ + maxZ) / 2
        val legend = listOf(
            minZ to getColor(z = minZ, colorScale = colorScale, graphZMax = graphZMax, graphZMin = graphZMin),
            midZ to getColor(z = midZ, colorScale = colorScale, graphZMax = graphZMax, graphZMin = graphZMin),
            maxZ to getColor(z = maxZ, colorScale = colorScale, graphZMax = graphZMax, graphZMin = graphZMin),
        )
        val sectors = mutableListOf<Sector>()

        meshXFilled.forEachIndexed { xIndex, x ->
            meshYFilled.forEachIndexed { yIndex, y ->
                val (left, right) = meshXFilled.getBounds(xIndex)
                val (top, bottom) = meshYFilled.getBounds(yIndex)

                // Skip cut off rects
                if (left > bedMinX && top > bedMinY && right < bedMaxX && bottom < bedMaxY) {
                    getColorFor(
                        x = x,
                        y = y,
                        graphZMax = graphZMax,
                        graphZMin = graphZMin,
                        colorScale = colorScale,
                        mesh = mesh,
                        meshX = meshX,
                        meshY = meshY
                    )?.let { color ->
                        sectors += Sector(
                            left = left,
                            top = if (meshY.size == 1) bedMaxY else top,
                            right = right,
                            bottom = if (meshY.size == 1) bedMinY else bottom,
                            color = color,
                        )
                    }
                }
            }
        }

        return ParsedBedMesh(
            sectors = sectors,
            legend = legend
        )
    }

    private fun List<Float>.maxStep() = mapIndexedNotNull { index, y ->
        val next = getOrNull(index + 1) ?: return@mapIndexedNotNull null
        next - y
    }.maxOrNull() ?: 0f


    private fun List<Float>.getBounds(index: Int): Pair<Float, Float> {
        val prev = getOrNull(index - 1)
        val next = getOrNull(index + 1)
        val center = get(index)

        return if (prev != null && next != null) {
            val halfPrev = prev + (center - prev) / 2
            val halfNext = center + (next - center) / 2
            halfPrev to halfNext
        } else {
            val half = maxStep() / 2
            (center - half) to (center + half)
        }
    }

    private fun getColorFor(
        x: Float,
        y: Float,
        meshX: List<Float>,
        meshY: List<Float>,
        mesh: List<List<Float?>>,
        colorScale: List<Pair<Float, HexColor>>,
        graphZMin: Float,
        graphZMax: Float,
    ): HexColor? {
        return try {
            // Interpolate Z
            val adjacentPoints = findAdjacentPoints(
                x = x,
                y = y,
                meshX = meshX,
                meshY = meshY,
                mesh = mesh
            ) ?: let {
                Napier.d(tag = tag, message = "No adjacent points found for x=$x y=$y")
                return null
            }
            val z = when (adjacentPoints.size) {
                2 -> calculateZByLinearInterpolation(adjacentPoints = adjacentPoints, x = x)
                4 -> calculateZByBilinearInterpolation(adjacentPoints = adjacentPoints, x = x, y = y) ?: return null
                else -> throw IllegalStateException("Expected 2 or 4 adjacent points, got ${adjacentPoints.size}")
            } ?: return null
            //endregion
            //region Calc color
            getColor(z = z, colorScale = colorScale, graphZMin = graphZMin, graphZMax = graphZMax)
            //endregion
        } catch (e: IndexOutOfBoundsException) {
            // Seemingly this happens sometimes when data is incomplete
            Napier.d(tag = tag, message = "Bounds violation at x=$x y=$y", throwable = e)
            null
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Exception while interpolating color: meshX=$meshX meshY=$meshY mesh=$mesh x=$x y=$y", throwable = e)
            null
        } ?: HexColor("#AAAAAA")
    }

    private fun findAdjacentPoints(
        x: Float,
        y: Float,
        meshX: List<Float>,
        meshY: List<Float>,
        mesh: List<List<Float?>>
    ): List<Point>? {
        val points = mutableListOf<Point>()

        // Find the nearest X indices
        val xLeftIndex = meshX.indexOfLast { it <= x }.let {
            // Left can never be the last, so the right one can be the last. This is important for the last row where x == maxX
            if (it == meshX.lastIndex) it - 1 else it
        }.takeIf { it >= 0 } ?: return null
        val xRightIndex = meshX.indexOfFirst { it > meshX[xLeftIndex] && it >= x }.takeIf { it >= 0 }
            ?: return null
        // Find the nearest Y indices
        val yBelowIndex = meshY.indexOfLast { it <= y }.let {
            // Same as left above
            if (it == meshX.lastIndex) it - 1 else it
        }.takeIf { it >= 0 } ?: return null

        // Get left and right points
        if (xLeftIndex in meshX.indices) {
            points.add(Point(meshX[xLeftIndex], meshY[yBelowIndex], mesh[yBelowIndex][xLeftIndex]))
        }
        if (xRightIndex in meshX.indices) {
            points.add(Point(meshX[xRightIndex], meshY[yBelowIndex], mesh[yBelowIndex][xRightIndex]))
        }

        // If meshY has only one row, skip above/below checks
        if (meshY.size > 1) {
            val yAboveIndex = meshY.indexOfFirst { it > meshY[yBelowIndex] && it >= y }
                .takeIf { it >= 0 } ?: return null

            if (yAboveIndex in meshY.indices) {
                points.add(Point(meshX[xRightIndex], meshY[yAboveIndex], mesh[yAboveIndex].getOrNull(xRightIndex)))
                points.add(Point(meshX[xLeftIndex], meshY[yAboveIndex], mesh[yAboveIndex].getOrNull(xLeftIndex)))
            }
        }

        return points
    }

    private fun getColor(
        z: Float,
        graphZMin: Float,
        graphZMax: Float,
        colorScale: List<Pair<Float, HexColor>>
    ): HexColor {
        // Normalize z to the scale range (-1 to 1)
        val normalizedZ = (z - graphZMin) / (graphZMax - graphZMin) * 2 - 1

        // Find the two closest color points
        val lower = colorScale.lastOrNull { it.first <= normalizedZ } ?: colorScale.first()
        val upper = colorScale.firstOrNull { it.first >= normalizedZ } ?: colorScale.last()

        // If exact match, return directly
        if (lower.first == upper.first) return lower.second

        // Linear interpolation of colors
        val t = (normalizedZ - lower.first) / (upper.first - lower.first)
        return blendColor(lower.second, upper.second, t)
    }

    private fun calculateZByBilinearInterpolation(adjacentPoints: List<Point>, x: Float, y: Float): Float? {
        // From https://stackoverflow.com/questions/8661537/how-to-perform-bilinear-interpolation-in-python
        val sortedPoints = adjacentPoints.sorted()
        require(sortedPoints.size == 4) { "Requiring 4 points, got $sortedPoints" }
        val (x1, y1, q11) = sortedPoints[0]
        val (_x1, y2, q12) = sortedPoints[1]
        val (x2, _y1, q21) = sortedPoints[2]
        val (_x2, _y2, q22) = sortedPoints[3]
        require(x1 == _x1 && x2 == _x2 && y1 == _y1 && y2 == _y2) { "Requiring points to form a rectangle, got $sortedPoints" }
        require(x in x1..x2 && y in y1..y2) { "Requiring ($x|$y) to be in rectangle $sortedPoints" }
        if (q11 == null || q12 == null || q21 == null || q22 == null) return null

        return (q11 * (x2 - x) * (y2 - y) +
                q21 * (x - x1) * (y2 - y) +
                q12 * (x2 - x) * (y - y1) +
                q22 * (x - x1) * (y - y1)
                ) / ((x2 - x1) * (y2 - y1) + 0f)
    }

    private fun calculateZByLinearInterpolation(adjacentPoints: List<Point>, x: Float): Float? {
        require(adjacentPoints.size == 2) { "Requiring 2 points, got $adjacentPoints" }
        val (x1, y1, z1) = adjacentPoints[0]
        val (x2, y2, z2) = adjacentPoints[1]
        require(y1 == y2) { "Requiring points to be on the same Y level, got $adjacentPoints" }
        require(x in x1..x2) { "Requiring x=$x to be between $x1 and $x2" }
        if (z1 == null || z2 == null) return null

        return z1 + (x - x1) * (z2 - z1) / (x2 - x1)
    }

    private data class Point(val x: Float, val y: Float, val z: Float?) : Comparable<Point> {

        companion object {
            private val comparator = compareBy<Point> { it.x }.thenBy { it.y }
        }

        override fun compareTo(other: Point) = comparator.compare(this, other)
    }

    private infix fun Float.halfWayUntil(b: Float) = blend(a = this, b = b, ratio = 0.5f)

    private fun blend(a: Float, b: Float, ratio: Float = 0.5f) = a + ((b - a) * ratio)

    private fun blendColor(a: HexColor, b: HexColor, progress: Float) = HexColor(
        red = sqrt(blend(a = a.red.pow(2), b = b.red.pow(2), ratio = progress)),
        green = sqrt(blend(a = a.green.pow(2), b = b.green.pow(2), ratio = progress)),
        blue = sqrt(blend(a = a.blue.pow(2), b = b.blue.pow(2), ratio = progress)),
        rawValue = null
    )

    private fun List<Float>.fillList(iterations: Int): List<Float> {
        if (iterations == 0) return this

        val mutated = mutableListOf<Float>()
        forEachIndexed { index, current ->
            mutated += current

            // Interpolate middle
            getOrNull(index + 1)?.let { next ->
                mutated += current halfWayUntil next
            }
        }

        return mutated.fillList(iterations - 1)
    }
}