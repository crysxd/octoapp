package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ScaleType
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

internal class WebcamSettingsHelper(private val core: BaseViewModelCore) {

    private val tag = "WebcamSettingsManager"
    private val configRepository = SharedBaseInjector.get().printerConfigRepository

    val configAspectRatio = configRepository.instanceInformationFlow(core.instanceId).map { info ->
        val active = info?.appSettings?.activeWebcamIndex ?: 0
        info?.settings?.webcam?.webcams?.getOrNull(active)?.streamRatio?.streamRatioFloat ?: (16 / 9f)
    }

    fun storeScaleType(scaleType: ScaleType, isFullscreen: Boolean) {
        AppScope.launch(core.coroutineExceptionHandler) {
            configRepository.updateAppSettings(core.instanceId) {
                if (isFullscreen) {
                    it.copy(webcamFullscreenScaleType = scaleType.ordinal)
                } else {
                    it.copy(webcamScaleType = scaleType.ordinal)
                }
            }
        }
    }

    fun storeAspectRatio(ratio: String) {
        AppScope.launch(core.coroutineExceptionHandler) {
            configRepository.updateAppSettings(core.instanceId) {
                it.copy(webcamLastAspectRatio = ratio)
            }
        }
    }

    fun nextWebcam(webcamCount: Int) = switchWebcam(index = null, webcamCount = webcamCount)

    private fun switchWebcam(index: Int?, webcamCount: Int) = AppScope.launch(core.coroutineExceptionHandler) {
        configRepository.updateAppSettings(core.instanceId) {
            val activeIndex = index ?: ((it.activeWebcamIndex + 1) % webcamCount)
            Napier.i(tag = tag, message = "Switching to webcam $activeIndex")
            it.copy(activeWebcamIndex = activeIndex)
        }
    }

    fun getScaleType(isFullscreen: Boolean, default: ScaleType) = ScaleType.entries[
            configRepository.get(core.instanceId)?.appSettings?.let {
                if (isFullscreen) it.webcamFullscreenScaleType else it.webcamScaleType
            } ?: default.ordinal
    ]

    fun getInitialAspectRatio(): Float {
        val index = configRepository.get(core.instanceId)?.appSettings?.activeWebcamIndex ?: 0
        val aspectRatio = configRepository.get(core.instanceId)?.let {
            it.appSettings?.webcamLastAspectRatio ?: it.settings?.webcam?.webcams?.getOrNull(index)?.streamRatio
        } ?: "16:9"

        return aspectRatio.streamRatioFloat
    }

    private val String.streamRatioFloat
        get() = try {
            val (w, h) = split(":")
            w.toFloat() / h.toFloat()
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to get aspect ratio from '$this'", throwable = e)
            16 / 9f
        }
}