package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import kotlinx.coroutines.withTimeoutOrNull
import kotlin.time.Duration.Companion.seconds

class HelpContactViewModelCore {

    private val tag = "HelpContactViewModelCore"
    private val createBugReportUseCase = SharedBaseInjector.get().createBugReportUseCase()

    private val contactEmail get() = OctoConfig.get(OctoConfigField.ContactEmail)
    val contactTimeZone get() = OctoConfig.get(OctoConfigField.ContactTimeZone)

    suspend fun prepareFeedbackMail(
        sendLogs: Boolean,
        sendOctoPrintInfo: Boolean,
        sendPhoneInfo: Boolean,
    ): Mail? = withTimeoutOrNull(10.seconds) {
        val params = CreateBugReportUseCase.Params(
            sendLogs = sendLogs,
            sendOctoPrintInfo = sendOctoPrintInfo,
            sendPhoneInfo = sendPhoneInfo,
        )

        val bugReport = createBugReportUseCase.execute(params)
        Mail(
            receipientAddress = contactEmail,
            subject = "Feedback OctoApp for ${bugReport.platformName} ${bugReport.appVersion}",
            attachments = bugReport.files.map {
                Attachment(
                    bytes = it.bytes,
                    name = it.name,
                )
            }
        )
    }

    class Attachment(
        val bytes: ByteArray,
        val name: String,
    )

    data class Mail(
        val receipientAddress: String,
        val subject: String,
        val attachments: List<Attachment>?,
    )
}