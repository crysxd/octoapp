package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import io.ktor.http.Url
import kotlinx.coroutines.flow.flowOf

class RemoteAccessManualViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    override val tag = "RemoteAccessManualViewModelCore"
    override val remoteServiceName = "none"
    override val loadingFlow = flowOf(false)

    override fun Url.isMyUrl() = !isNgrokUrl() && !isOctoEverywhereUrl() && !isObicoUrl()

    suspend fun setManual(
        url: String,
        user: String,
        password: String,
        skipTest: Boolean
    ) = applyUrl(
        url = url,
        user = user,
        password = password,
        skipTest = skipTest,
    )
}