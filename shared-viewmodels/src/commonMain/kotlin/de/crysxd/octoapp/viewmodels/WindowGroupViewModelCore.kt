package de.crysxd.octoapp.viewmodels

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.OBICO_APP_PORTAL_CALLBACK_PATH
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withoutQuery
import io.github.aakira.napier.Napier
import io.ktor.http.Url

class WindowGroupViewModelCore {

    private val tag = "WindowGroupViewModelCore"
    private val handleOctoEverywhereAppPortalSuccessUseCase get() = SharedBaseInjector.get().handleOctoEverywhereAppPortalSuccessUseCase()
    private val handleObicoAppPortalSuccessUseCase get() = SharedBaseInjector.get().handleObicoAppPortalSuccessUseCase()
    private val unconsumedDeeplinkHandlers = mutableListOf<Pair<String, (Url) -> Unit>>()

    fun registerForUnconsumedDeeplink(callback: (Url) -> Unit): String {
        val token = uuid4().toString()
        unconsumedDeeplinkHandlers += token to callback
        Napier.d(tag = tag, message = "Registered new unconsumed deeplink handler, now ${unconsumedDeeplinkHandlers.size} active ($token)")
        return token
    }

    fun unregisterForUnconsumedDeeplink(token: String) {
        unconsumedDeeplinkHandlers.removeAll { (t, _) -> t == token }
        Napier.d(tag = tag, message = "Unegistered new unconsumed deeplink handler, now ${unconsumedDeeplinkHandlers.size} active ($token)")
    }

    suspend fun consumeDeeplink(link: String): Boolean = try {
        Napier.i(tag = tag, message = "Opening URL: ${link.toUrl().withoutQuery()}")
        Napier.v(tag = tag, message = "Detailed URL: ${link.toUrl()}")
        val url = Url(link)

        val pathSegment = url.pathSegments.firstOrNull { it.isNotBlank() }
        Napier.i(tag = tag, message = "Path segment: $pathSegment")

        when (pathSegment) {
            OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH -> {
                handleOctoEverywhereAppPortalSuccessUseCase.execute(link.toUrl())
                true
            }

            OBICO_APP_PORTAL_CALLBACK_PATH -> {
                handleObicoAppPortalSuccessUseCase.execute(link.toUrl())
                true
            }

            else -> {
                // Publish the link to the latest handler
                unconsumedDeeplinkHandlers.lastOrNull()?.let { (token, callback) ->
                    Napier.i(tag = tag, message = "No default handling for URL: ${url.withoutQuery()}, publishing to $token")
                    callback(url)
                } ?: Napier.i(tag = tag, message = "No default handling for URL: ${url.withoutQuery()} but no handlers registered")
                false
            }
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to handle deep link", throwable = e)
        ExceptionReceivers.dispatchException(e)
        true
    }
}