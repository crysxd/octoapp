package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.http.framework.isLocalNetwork
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn

class ConnectionControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    companion object {
        private var counter = 0
    }

    private val tag = "ConnectionControlsViewModel/${counter++}"
    private val useCase = SharedBaseInjector.get().connectPrinterUseCase()
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    init {
        Napier.i(tag = tag, message = "Create new connection VM")
    }

    val state: Flow<State> = useCase.executeBlocking(ConnectPrinterUseCase.Params(instanceId = instanceId))
        .onCompletion { Napier.i(tag = tag, message = "Stop shared connection attempts on $instanceId") }
        .onStart { Napier.i(tag = tag, message = "Start shared connecting on $instanceId") }
        .rateLimit(200)
        .combine(printerConfigRepository.instanceInformationFlow(instanceId = instanceId)) { uiState, instance -> uiState to instance }
        .map { (uiState, instance) -> enrichStateWithRemoteConnectionAd(uiState, instance) }
        .shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay)
        .onCompletion { Napier.i(tag = tag, message = "Stop connection attempts on $instanceId") }
        .onStart { Napier.i(tag = tag, message = "Start connecting on $instanceId") }

    private fun enrichStateWithRemoteConnectionAd(uiState: ConnectPrinterUseCase.UiState, instance: PrinterConfigurationV3?): State {
        val notConfigured = instance?.let { i ->
            i.alternativeWebUrl == null || i.alternativeWebUrl?.isLocalNetwork() == true
        } ?: false

        val services = listOf(
            OctoPlugins.OctoEverywhere,
            OctoPlugins.Ngrok,
            OctoPlugins.Obico,
        ).filter {
            instance?.hasPlugin(it) == true
        }

        return State(
            uiState = uiState,
            advertise = uiState.isNotAvailable && notConfigured,
            service = services.firstOrNull().takeIf { services.size == 1 },
        )
    }

    suspend fun executeAction(actionType: ConnectPrinterUseCase.ActionType) =
        useCase.executeAction(actionType)

    data class State(
        val advertise: Boolean,
        val service: String?,
        val uiState: ConnectPrinterUseCase.UiState,
    )
}