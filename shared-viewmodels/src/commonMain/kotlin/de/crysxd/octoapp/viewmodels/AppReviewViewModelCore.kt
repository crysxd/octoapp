package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.minutes

@OptIn(ExperimentalCoroutinesApi::class)
class AppReviewViewModelCore : BaseViewModelCore(instanceId = "") {

    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val preferences = SharedBaseInjector.get().preferences
    private val tag = "AppReviewViewModelCore"

    val reviewFlowTrigger = printerProvider.passiveCurrentMessageFlow(instanceId = null, tag = "app-review")
        .onEach {
            if (!preferences.reviewFlowConditions.printWasActive) {
                preferences.reviewFlowConditions = preferences.reviewFlowConditions.copy(printWasActive = true)
            }
        }
        .distinctUntilChangedBy { true }
        .flatMapLatest { preferences.updatedFlow2 }
        .map { it.reviewFlowConditions }
        .distinctUntilChangedBy { it.copy(lastRequest = Instant.DISTANT_PAST) }
        .filter { conditions ->
            OctoAnalytics.logEvent(OctoAnalytics.Event.ReviewFlowConsidered)
            val minAppLaunches = OctoConfig.get(OctoConfigField.ReviewFlowConditionsMinAppLaunches)
            val minAppUsage = OctoConfig.get(OctoConfigField.ReviewFlowConditionsAppUsageMinutes).minutes
            val printRequired = OctoConfig.get(OctoConfigField.ReviewFlowConditionsPrintWasActiveRequired)
            val pause = OctoConfig.get(OctoConfigField.ReviewFlowConditionsPauseMinutes).minutes
            Napier.d(
                tag = tag,
                message = "Checking conditions: $conditions <--> minAppLaunches=$minAppLaunches, minAppUsageMinutes=$minAppUsage, printRequired=$printRequired pause=$pause"
            )

            val appLaunchesOk = conditions.appLaunchCounter >= minAppLaunches
            val appUsageOk = (Clock.System.now() - conditions.firstStart) >= minAppUsage
            val printStateOk = (conditions.printWasActive || !printRequired)
            val timeSinceLastOk = (Clock.System.now() - conditions.lastRequest) > pause

            if (appLaunchesOk && appUsageOk && printStateOk && timeSinceLastOk) {
                Napier.i(tag = tag, message = "Conditions met")
                true
            } else {
                Napier.d(tag = tag, message = "Conditions not met")
                false
            }
        }.map {
            Trigger(id = it.hashCode())
        }.distinctUntilChanged().onEach {
            preferences.reviewFlowConditions = preferences.reviewFlowConditions.copy(lastRequest = Clock.System.now())
        }

    data class Trigger(
        val id: Int
    )
}