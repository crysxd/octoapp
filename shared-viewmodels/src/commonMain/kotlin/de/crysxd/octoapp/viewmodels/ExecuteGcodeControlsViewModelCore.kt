package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.BackendType
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.data.repository.MacroGroupsRepository.Companion.GroupDefault
import de.crysxd.octoapp.base.data.repository.MacroGroupsRepository.Companion.GroupHidden
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.macro.Macro
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

@OptIn(ExperimentalCoroutinesApi::class)
class ExecuteGcodeControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId = instanceId) {

    private val tag = "ExecuteGcodeControlsViewModelCore"
    private val getGcodeShortcutsUseCase = SharedBaseInjector.get().getGcodeShortcutsUseCase()
    private val executeGcodeCommandUseCase = SharedBaseInjector.get().executeGcodeCommandUseCase()
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val octoPreferences = SharedBaseInjector.get().preferences
    private val macroGroupsRepository = SharedBaseInjector.get().macroGroupsRepository
    private var lastItems: List<Group> = emptyList()

    private val gcodeHistory = flow { emit(getGcodeShortcutsUseCase.execute(Unit)) }
        .flatMapLatest { it }
        .distinctUntilChanged()
        .map { items ->
            items.map { item ->
                Item(
                    inputs = emptyList(),
                    label = item.name,
                    command = item.command,
                    isFavorite = item.isFavorite,
                    raw = RawItem.HistoryItem(item),
                    id = "history/${item.command.hashCode()}",
                )
            }
        }

    private val macros = printerConfigRepository.instanceInformationFlow(instanceId)
        .map { it?.macros ?: emptyList() }
        .combine(macroGroupsRepository.updateFlow) { macros, _ ->
            macros.asSequence().map { macro ->
                macroGroupsRepository.getGroup(macroId = macro.id) to macro
            }.groupBy { (group, _) ->
                group
            }.map { (key, value) ->
                key to value.map { it.second }
            }.sortedWith(
                compareBy<Pair<String, List<Macro>>> { (group, _) ->
                    group == GroupDefault
                }.thenBy { (group, _) ->
                    macroGroupsRepository.getLabelFor(group).first
                }.thenBy { (group, _) ->
                    macroGroupsRepository.getLabelFor(group).second
                }
            ).filter { (group, _) ->
                group != GroupHidden
            }.map { (group, macros) ->
                val (_, label) = macroGroupsRepository.getLabelFor(group)

                val items = macros.map { macro ->
                    Item(
                        inputs = macro.inputs,
                        label = macro.name,
                        command = macro.id,
                        isFavorite = false,
                        raw = RawItem.MacroItem(macro),
                        id = "macro/${macro.id.hashCode()}"
                    )
                }.sortedBy { macro ->
                    macro.label + macro.id
                }

                Group(label, items)
            }.toList()
        }

    val items = combine(gcodeHistory, macros) { history, macros ->
        listOf(Group(null, history)) + macros
    }.distinctUntilChanged().map { items ->
        lastItems = items
        State(items)
    }.flowOn(Dispatchers.Default)

    val visible = octoPrintProvider.passiveCurrentMessageFlow(tag = "ExecuteGcodeControlsViewModelCore", instanceId = instanceId)
        .combine(printerConfigRepository.instanceInformationFlow(instanceId)) { current, config ->
            val printing = current.state.flags.isPrinting()
            val paused = current.state.flags.paused
            val alwaysShown = octoPreferences.allowTerminalDuringPrint
            val isKlipper = config?.type == BackendType.Moonraker

            !printing || paused || alwaysShown || isKlipper
        }
        .map { Visibility(it) }
        .distinctUntilChanged()

    suspend fun executeGcodeCommand(
        id: String,
        inputs: Map<String, String>,
        confirmed: Boolean,
    ) = try {
        val raw = getRawItem(id)
        when {
            ((raw as? RawItem.MacroItem)?.item?.inputs?.size ?: 0) != inputs.size -> Result.NeedsInputs
            !confirmed && octoPrintProvider.getLastCurrentMessage(instanceId)?.state?.flags?.isPrinting() == true -> Result.NeedsConfirmation
            else -> {
                val gcode = when (raw) {
                    is RawItem.HistoryItem -> raw.item.command
                    is RawItem.MacroItem -> raw.item.createGcodeCommand(params = inputs)
                }

                executeGcodeCommandUseCase.execute(
                    ExecuteGcodeCommandUseCase.Param(
                        command = GcodeCommand.Batch(gcode.split("\n")),
                        instanceId = instanceId,
                        fromUser = raw is RawItem.HistoryItem,
                        recordResponse = false,
                    )
                )

                Result.Executed
            }
        }
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Failed to execute Gcode", throwable = e)
        Result.Failed
    }

    private fun getRawItem(id: String) = lastItems.map { (_, items) -> items }.flatten().first { it.id == id }.raw

    fun getMacroId(id: String) = try {
        (getRawItem(id) as RawItem.MacroItem).item.id
    } catch (e: Exception) {
        reportException(e)
        null
    }

    data class State(
        val groups: List<Group> = emptyList(),
    )

    data class Group(
        val label: String?,
        val items: List<Item>
    )

    data class Item(
        val id: String,
        val label: String,
        val isFavorite: Boolean = false,
        val inputs: List<Macro.Input> = emptyList(),
        val command: String,
        val raw: RawItem,
    )

    sealed class RawItem {
        data class HistoryItem(val item: GcodeHistoryItem) : RawItem()
        data class MacroItem(val item: Macro) : RawItem()
    }

    data class Visibility(val visible: Boolean)

    enum class Result {
        Executed, NeedsConfirmation, Failed, NeedsInputs
    }
}