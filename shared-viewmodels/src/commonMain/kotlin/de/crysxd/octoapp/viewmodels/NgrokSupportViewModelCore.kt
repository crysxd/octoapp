package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.hasPlugin
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.message.NgrokPluginMessage
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry

class NgrokSupportViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "NgrokSupportViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val updateNgrokTunnelUseCase get() = SharedBaseInjector.get().updateNgrokTunnelUseCase()

    private val credentialsChanges = printerConfigRepository.instanceInformationFlow(instanceId)
        .distinctUntilChangedBy { it?.settings?.plugins?.ngrok }
        .map { instance ->
            instance?.alternativeWebUrl?.takeIf { it.isNgrokUrl() }?.let {
                Napier.i(tag = tag, message = "Instance with ngrok configuration was updated, updating ngrok as well")
                try {
                    updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.Tunnel(tunnel = it.toString(), instanceId = instanceId))
                } catch (e: Exception) {
                    Napier.e(tag = tag, message = "Failed to update ngrok config", throwable = e)
                }
            }
        }

    private val connectionEvents = printerProvider.passiveConnectionEventFlow(tag = "ngrok-support", instanceId = instanceId)
        .onEach { event ->
            if (event?.connectionType == ConnectionType.Default && printerConfigRepository.get(instanceId).hasPlugin(Settings.Ngrok::class)) {
                Napier.i(tag = tag, message = "Instance with ngrok configuration was connected, updating ngrok")
                try {
                    updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.FetchConfig(instanceId = instanceId))
                } catch (e: Exception) {
                    Napier.e(tag = tag, message = "Failed to update ngrok config", throwable = e)
                }
            }
        }

    private val messages = printerProvider.passiveCachedMessageFlow(tag = "ngrok-support", instanceId = instanceId, clazz = NgrokPluginMessage::class)
        .filterNotNull()
        .onEach {
            Napier.i(tag = tag, message = "Received new ngrok configuration: $it")
            try {
                updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.Tunnel(tunnel = it.tunnel, instanceId = instanceId))
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to update ngrok config", throwable = e)
            }
        }

    @OptIn(ExperimentalCoroutinesApi::class)
    val events = printerConfigRepository.instanceInformationFlow(instanceId).flatMapLatest { config ->
        if (config?.hasPlugin(OctoPlugins.Ngrok) == true) {
            combine(credentialsChanges, connectionEvents, messages) { _, _, _ -> }
        } else {
            emptyFlow()
        }
    }.retry {
        delay(1000)
        Napier.e(tag = tag, message = "Failed to update ngrok config", throwable = it)
        true
    }
}