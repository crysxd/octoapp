package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.sorted
import de.crysxd.octoapp.base.models.JobHistoryItem
import de.crysxd.octoapp.base.usecase.GetPrintHistoryUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.base.utils.mapData
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.utils.runForAtLeast
import de.crysxd.octoapp.viewmodels.helper.startprint.StartPrintHelper
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class FileListViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "FileListViewModelCore"
    private val startPrintHelper = StartPrintHelper(this)
    private val loadFilesUseCase = SharedBaseInjector.get().loadFilesUseCase()
    private val getPrintHistoryUseCase = SharedBaseInjector.get().getPrintHistoryUseCase()
    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val printerConfig = SharedBaseInjector.get().printerConfigRepository
    private val preferences = SharedBaseInjector.get().preferences
    private var file: FileObject.File? = null

    private val loadTrigger = MutableStateFlow<LoadTrigger?>(null)

    // Yes, this must be eventFlow and not passiveEventFlow ;)
    // In case the user switches the instance for the file list we need this
    private val flags = printerProvider.eventFlow(
        tag = "file-list",
        instanceId = instanceId,
    ).mapNotNull {
        // We can't use passive here as we can open file lists for any instance, might not be conected
        ((it as? Event.MessageReceived)?.message as? Message.Current)?.state?.flags
    }.distinctUntilChanged().onStart {
        emit(PrinterState.Flags())
    }

    val printHistory = loadTrigger
        .filterNotNull()
        .flatMapLatest<LoadTrigger, FlowState<List<JobHistoryItem>>?> { trigger ->
            Napier.d(tag = this@FileListViewModelCore.tag, message = "Trigger use: $trigger")
            getPrintHistoryUseCase.execute(
                param = GetPrintHistoryUseCase.Params(
                    instanceId = instanceId,
                    skipCache = trigger.skipCache,
                )
            )
        }
        .onStart { emit(null) }


    val fileListState = loadTrigger
        .filterNotNull()
        .flatMapLatest { trigger ->
            preferences.updatedFlow2.map { it.fileManagerSettings to trigger }
        }
        .distinctUntilChanged()
        .flatMapLatest { (_, trigger) ->
            Napier.d(tag = this@FileListViewModelCore.tag, message = "Trigger use: $trigger")
            runForAtLeast(duration = if (trigger.skipCache) 500.milliseconds else 0.seconds) {
                loadFilesUseCase.execute(
                    param = LoadFilesUseCase.Params(
                        path = trigger.path,
                        instanceId = instanceId,
                        fileOrigin = FileOrigin.Gcode,
                        skipCache = trigger.skipCache
                    )
                )
            }
        }.combine(printHistory) { files, printHistory ->
            // Ignore if print history fails
            when {
                files is FlowState.Loading || printHistory is FlowState.Loading -> FlowState.Loading()
                files is FlowState.Error -> FlowState.Error(files.throwable)
                else -> FlowState.Ready((files as FlowState.Ready).data to (printHistory as? FlowState.Ready)?.data)
            }
        }.mapData { (fileObject, printHistory) ->
            Napier.d(tag = this@FileListViewModelCore.tag, message = "Children: ${(fileObject as? FileObject.Folder)?.children?.map { it.path }}")
            file = fileObject as? FileObject.File
            if (fileObject is FileObject.Folder) {
                fileObject.copy(
                    children = fileObject.children
                        ?.sorted()
                        ?.map { it.enrichWithHistory(printHistory ?: emptyList()) }
                )
            } else {
                fileObject
            }
        }.combine(flags) { state, flags ->
            Napier.d(message = "${loadTrigger.value?.path}: ${state::class.simpleName}")
            State(canStartPrint = flags.isOperational() && !flags.isPrinting(), state = state)
        }.catch {
            Napier.e(tag = this@FileListViewModelCore.tag, message = "Failed to load ${loadTrigger.value?.path}", throwable = it)
            reportException(it)
            emit(State(state = FlowState.Error(it), canStartPrint = false))
        }.onEach {
            if (it.state is FlowState.Error && it.state.throwable !is CancellationException) {
                Napier.e(tag = this@FileListViewModelCore.tag, message = "Failed to load ${loadTrigger.value?.path}", throwable = it.state.throwable)
            }
            Napier.d(tag = this@FileListViewModelCore.tag, message = "Emitting ${loadTrigger.value?.path}: ${it.state::class.simpleName}")
        }.onStart {
            emit(State(state = FlowState.Loading(), canStartPrint = false))
        }

    private val hideInformAboutThumbnailsUntil = preferences.updatedFlow2.map { it.hideThumbnailHintUntil }
    val informAboutThumbnails = printerConfig.instanceInformationFlow(instanceId).combine(hideInformAboutThumbnailsUntil) { config, hideUntil ->
        val hasPrusa = config?.hasPlugin(OctoPlugins.PrusaSlicerThumbnails) == true
        val hasCura = config?.hasPlugin(OctoPlugins.UltimakerFormatPackage) == true
        val hasAny = hasCura || hasPrusa
        val isOctoPrint = config?.systemInfo?.interfaceType in listOf(SystemInfo.Interface.OctoPrint)
        val isHidden = hideUntil > Clock.System.now()

        !hasAny && !isHidden && isOctoPrint
    }

    fun loadFiles(path: String, skipCache: Boolean) {
        loadTrigger.update {
            if (it?.path != path || skipCache) {
                LoadTrigger(path = path, skipCache = skipCache, id = (it?.id ?: 0) + 1)
            } else {
                it
            }
        }
    }

    fun dismissThumbnailInformation() {
        preferences.hideThumbnailHintUntil = Clock.System.now() + 30.days
    }

    private fun FileReference.enrichWithHistory(printHistory: List<JobHistoryItem>) = when (this) {
        is FileObject.File -> if (this.prints == null) {
            // Create a history
            copy(
                prints = createPrintHistory(
                    id = id,
                    path = path,
                    printHistory = printHistory
                )
            )
        } else {
            // Trust the history we have (usually OctoPrint)
            this
        }

        is FileReference.File -> FileObject.File(
            path = path,
            origin = origin,
            name = name,
            display = display,
            size = size,
            prints = createPrintHistory(
                id = id,
                path = path,
                printHistory = printHistory
            ),
        )

        else -> this
    }

    private fun createPrintHistory(id: String, path: String, printHistory: List<JobHistoryItem>) = FileObject.PrintHistory(
        last = printHistory.firstOrNull {
            // Usually we match with id, path is fallback
            it.fileId == id || it.path.removeSurrounding("/") == path.removeSurrounding("/")
        }?.let {
            FileObject.PrintHistory.LastPrint(
                date = it.time,
                success = it.success,
            )
        }
    )

    suspend fun startPrint(materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result = startPrintHelper.startPrint(
        file = requireNotNull(file) { "No file active, can't start print" },
        materialConfirmed = materialConfirmed,
        timelapseConfirmed = timelapseConfirmed
    )

    data class State(
        val state: FlowState<out FileObject>,
        val canStartPrint: Boolean,
    )

    private data class LoadTrigger(
        val path: String,
        val id: Int,
        val skipCache: Boolean,
    )
}