package de.crysxd.octoapp.viewmodels

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.exceptions.BrokenSetupException
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.ext.composeMessageStack
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.ConnectivityHelper
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class OrchestratorViewModelCore(
    private val triggerCapabilityUpdates: Boolean
) {

    private val tag = "Orchestrator"
    private val configRepository = SharedBaseInjector.get().printerConfigRepository
    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val preferences = SharedBaseInjector.get().preferences
    private val updateInstanceCapabilitiesUseCase get() = SharedBaseInjector.get().updateInstanceCapabilitiesUseCase()
    private val activeConfig = configRepository.instanceInformationFlow().distinctUntilChanged()
    private val connectivityHelper = ConnectivityHelper()

    private var capabilityUpdateMutex = Mutex()
    private val manualNavigationState = MutableStateFlow<Pair<NavigationState, Instant>?>(null)
    private val currentNavigationState = MutableStateFlow<Pair<NavigationState, Instant>?>(null)
    private var appOpenTime = MutableStateFlow(Instant.DISTANT_PAST)
    private var appOpen = MutableStateFlow(true)

    // We store configHash per connection type as some remote services modify settings on the fly
    // but might not update the config hash
    private var configHashes = mutableMapOf<String, String?>()
    private var connectionTypes = mutableMapOf<String, ConnectionType>()

    private val showInstanceInBannerFlow = preferences.updatedFlow2
        .map { it.showActiveInstanceInStatusBar }
        .distinctUntilChanged()

    val errorEvents = ExceptionReceivers.flow.map {
        Napier.w(tag = tag, message = "Showing error dialog for ${it::class.simpleName} (${it.message})")
        ErrorEvent(
            message = it.composeErrorMessage(),
            messageString = it.composeErrorMessage().toString(),
            detailsMessage = it.composeMessageStack(),
            detailsMessageString = it.composeMessageStack().toString(),
            action = when {
                it is BrokenSetupException && it.needsRepair -> {
                    { manualNavigationState.value = NavigationState.Repair(it.instance.id) to Clock.System.now() }
                }

                else -> null
            }
        )
    }

    val navigationState = activeConfig.map { config ->
        when (config) {
            null -> NavigationState.Login
            else -> NavigationState.Active(
                instanceId = config.id,
                colors = config.colors,
                instanceLabel = config.label,
                hasCompanion = config.hasPlugin(plugin = OctoPlugins.OctoApp, minVersion = "2.0.0"),
                companionRunning = config.settings?.plugins?.octoAppCompanion?.running == true,
                systemInfo = config.systemInfo ?: SystemInfo()
            )
        } to Clock.System.now()
    }.combine(manualNavigationState) { auto, manual ->
        (manual?.takeIf { it.second > auto.second } ?: auto).first
    }.distinctUntilChanged().onStart {
        Napier.i(tag = tag, message = "Navigation state started")
    }.onCompletion {
        Napier.i(tag = tag, message = "Navigation state ended")
    }.onEach {
        Napier.i(tag = tag, message = "Navigation state now $it")
    }

    private val appOpenFlow = appOpen.mapLatest { open ->
        if (open) {
            Napier.w(tag = tag, message = "App open (debounced)")
            open
        } else {
            delay(3.seconds)
            Napier.w(tag = tag, message = "App closed (debounced)")
            open
        }
    }

    val uiSettings = preferences.updatedFlow2.map {
        UiSettings(
            leftyMode = it.leftHandMode,
            compactLayout = it.compactLayout,
        )
    }.distinctUntilChanged()

    private val processEventFlow = activeConfig
        .distinctUntilChangedBy { it?.id }
        .combine(appOpenFlow) { instance, appOpen -> if (appOpen) instance else null }
        .combine(navigationState) { instance, state -> if (state is NavigationState.Active) instance else null }
        .flatMapLatest { instance ->
            if (instance == null) {
                emptyFlow()
            } else {
                printerProvider.eventFlow(tag = "orchestrator", instanceId = instance.id)
            }
        }
        .combine(activeConfig.filterNotNull()) { event, config -> config to event }
        .buffer()
        .onEach { (instance, event) -> processEvent(instance.id, event) }
        .map { }
        .distinctUntilChanged()
        .onStart {
            Napier.i(tag = tag, message = "Event flow started")
        }
        .onCompletion {
            Napier.i(tag = tag, message = "Event flow ended")
        }

    private val connectivityState = combine(connectivityHelper.connectivityState, activeConfig.filterNotNull(), appOpenTime) { state, config, time ->
        (state to time) to config.id
    }.distinctUntilChanged().map { (trigger, id) ->
        Napier.e(tag = tag, message = "Reporting connectivity change to $id (trigger=$trigger) [BaseUrl]")
        printerProvider.printer(instanceId = id).notifyConnectionChange()
    }.onStart {
        emit(Unit)
    }

    val connectionState = activeConfig
        .filterNotNull()
        .map { it }
        .combine(showInstanceInBannerFlow) { instance, showLabelInBanner -> instance to showLabelInBanner }
        .flatMapLatest { (instance, showLabelInBanner) ->
            printerProvider.passiveConnectionEventFlow(tag = "orchestrator-passive", instanceId = instance.id).map { Triple(instance, it, showLabelInBanner) }
        }.map { (instance, connection, showLabelInBanner) ->
            // We use the appOpenTime as the eventTime so the reconnection banner is shown at least once after the app is opened but not randomly if
            // the connection is reset
            ConnectionState(
                connectionType = connection?.connectionType,
                instanceLabel = instance.label,
                colors = instance.colors,
                showLabelInBanner = showLabelInBanner,
                eventTime = appOpenTime.value
            )
        }.combine(processEventFlow) { state, _ ->
            state
        }.combine(connectivityState) { state, _ ->
            state
        }.distinctUntilChanged()

    constructor() : this(triggerCapabilityUpdates = true)

    fun activateInstance(instanceId: String) {
        configRepository.setActive(instanceId, trigger = "switch")
    }

    fun onAppOpen() = try {
        Napier.i(tag = tag, message = "UI started")
        appOpenTime.value = Clock.System.now()
        appOpen.value = true
        preferences.reviewFlowConditions = preferences.reviewFlowConditions.copy(appLaunchCounter = preferences.reviewFlowConditions.appLaunchCounter + 1)
        BillingManager.onResume()
    } catch (e: Exception) {
        // Something threw here
        Napier.e(tag = tag, message = "Failed to handle open app event", throwable = e)
    }

    fun onAppClose() = try {
        Napier.i(tag = tag, message = "UI closed")
        appOpen.value = false
        BillingManager.onPause()
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to handle close app event", throwable = e)
    }


    private suspend fun processEvent(instanceId: String, event: Event) = when (event) {
        is Event.Connected -> {
            Napier.i(tag = tag, message = "CONNECTED to $instanceId")
            connectionTypes[instanceId] = event.connectionType
        }

        is Event.MessageReceived -> processMessageReceived(message = event.message, instanceId = instanceId)

        is Event.Disconnected -> {
            Napier.i(tag = tag, message = "DISCONNECTED from $instanceId")
        }
    }

    private suspend fun processMessageReceived(message: Message, instanceId: String) {
        // Get config hash hash. Use a random hash for last one if not set, this will force another update
        // next time...but that's fine
        val lastConfigHash = configHashes[instanceId] ?: "default"

        when (message) {
            is Message.Connected -> considerUpdateCapabilities(
                trigger = "connected",
                instanceId = instanceId,
                configHash = message.configHash
            )

            is Message.Event.SettingsUpdated -> updateCapabilities(
                trigger = "settings-update",
                instanceId = instanceId,
                configHash = message.configHash
            )

            is Message.Event.PrinterProfileModified,
            is Message.Event.PrinterConnected -> updateCapabilities(
                trigger = "printer-or-profile",
                instanceId = instanceId,
                configHash = lastConfigHash,
            )

            else -> Unit
        }
    }

    private fun considerUpdateCapabilities(
        trigger: String,
        instanceId: String,
        configHash: String,
    ) = when (configHashes[instanceId]) {
        null -> {
            Napier.i(tag = tag, message = "Config hash not stored, updating capabilities")
            updateCapabilities(trigger = trigger, instanceId = instanceId, configHash = configHash)
        }

        configHash -> {
            Napier.i(tag = tag, message = "Config hash match, skipping capabilities update")
        }

        else -> {
            Napier.i(tag = tag, message = "Config hash mismatch, updating capabilities")
            updateCapabilities(trigger = trigger, instanceId = instanceId, configHash = configHash)
        }
    }

    private fun updateCapabilities(trigger: String, instanceId: String, configHash: String?) {
        if (!triggerCapabilityUpdates) return

        AppScope.launch(Dispatchers.Default) {
            if (capabilityUpdateMutex.tryLock()) {
                val id = uuid4().toString().takeLast(6)
                try {
                    Napier.w(tag = tag, message = "Capability update triggered from $trigger with $configHash ($id)")
                    updateInstanceCapabilitiesUseCase.execute(UpdateInstanceCapabilitiesUseCase.Params(instanceId = instanceId))
                    configHashes[instanceId] = configHash
                } catch (e: Exception) {
                    Napier.e(tag = tag, message = "Failed to update capabilities", throwable = e)
                } finally {
                    capabilityUpdateMutex.unlock()
                    Napier.w(tag = tag, message = "Capability update done ($id)")
                }
            } else {
                Napier.w(tag = tag, message = "Capability update busy, skipping")
            }
        }
    }

    data class ErrorEvent(
        val message: CharSequence,
        val messageString: String,
        val detailsMessage: CharSequence?,
        val detailsMessageString: String?,
        val action: (() -> Unit)?,
    )

    data class ConnectionState(
        val eventTime: Instant,
        val connectionType: ConnectionType?,
        val instanceLabel: String,
        val colors: Settings.Appearance.Colors,
        val showLabelInBanner: Boolean,
    )

    data class UiSettings(
        val leftyMode: Boolean,
        val compactLayout: Boolean
    )

    sealed class NavigationState {
        data object Login : NavigationState()

        data class Active(
            val instanceId: String,
            val colors: Settings.Appearance.Colors,
            val instanceLabel: String,
            val hasCompanion: Boolean,
            val companionRunning: Boolean,
            val systemInfo: SystemInfo,
        ) : NavigationState()

        data class Repair(
            val instanceId: String
        ) : NavigationState()
    }
}