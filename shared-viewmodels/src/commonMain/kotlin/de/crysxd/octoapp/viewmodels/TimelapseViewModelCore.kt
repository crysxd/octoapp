package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.DeleteTimelapseUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.update

@OptIn(ExperimentalCoroutinesApi::class)
class TimelapseViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "TimelapseViewModelCore"
    private val repository = SharedBaseInjector.get().timelapseRepository
    private val downloadUseCase = SharedBaseInjector.get().downloadTimelapseUseCase()
    private val deleteUseCase = SharedBaseInjector.get().deleteTimelapseUseCase()
    private val reloadTrigger = MutableStateFlow(0)

    private val fetchLatestAsync = flow {
        emit(Unit)
        emit(repository.fetchLatest(instanceId = instanceId))
    }

    val state = reloadTrigger.flatMapLatest {
        repository.getState(instanceId = instanceId).combine(fetchLatestAsync) { state, _ ->
            when (state) {
                null -> FlowState.Loading()
                else -> FlowState.Ready(
                    State(
                        unrendered = state.unrendered,
                        rendered = state.files,
                    )
                )
            }
        }.catch {
            Napier.e(tag = tag, message = "Failed to get timelapse state", throwable = it)
            emit(FlowState.Error(it))
        }
    }

    suspend fun reload() {
        reloadTrigger.update { it + 1 }
        state.first { it !is FlowState.Loading }
    }

    suspend fun delete(timelapseFile: TimelapseFile) = try {
        deleteUseCase.execute(
            param = DeleteTimelapseUseCase.Params(
                timelapseFile = timelapseFile,
                instanceId = instanceId
            )
        )
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to download timelapse from ${timelapseFile.downloadPath}", throwable = e)
        reportException(e)
        null
    }

    data class State(
        val unrendered: List<TimelapseFile>,
        val rendered: List<TimelapseFile>,
    )
}