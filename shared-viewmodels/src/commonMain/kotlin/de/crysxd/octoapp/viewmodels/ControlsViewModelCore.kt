package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.ControlType
import de.crysxd.octoapp.base.data.models.ControlsPreferences
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository.Companion.LIST_CONNECT
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository.Companion.LIST_PREPARE
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository.Companion.LIST_PRINT
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.EmergencyStopUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.event.Event
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.datetime.Clock
import kotlin.reflect.KClass
import kotlin.time.Duration.Companion.seconds

class ControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "ControlsViewModelCore"
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val cancelPrintJobUseCase = SharedBaseInjector.get().cancelPrintJobUseCase()
    private val togglePausePrintJobUseCase = SharedBaseInjector.get().togglePausePrintJobUseCase()
    private val emergencyStopUseCase = SharedBaseInjector.get().emergencyStopUseCase()
    private val widgetPreferences = SharedBaseInjector.get().controlsPreferencesRepository
    private var currentListId: String? = null
    private val controlTypes = mapOf(
        State.Connect::class to listOf(
            ControlType.Connect,
            ControlType.AnnouncementWidget,
            ControlType.WebcamWidget,
            ControlType.QuickAccessWidget,
        ),
        State.Prepare::class to listOf(
            ControlType.AnnouncementWidget,
            ControlType.SaveConfig,
            ControlType.ControlTemperatureWidget,
            ControlType.WebcamWidget,
            ControlType.MultiTool,
            ControlType.MoveToolWidget,
            ControlType.ExtrudeWidget,
            ControlType.QuickPrint,
            ControlType.SendGcodeWidget,
            ControlType.BedMesh,
            ControlType.QuickAccessWidget,
        ),
        State.Print::class to listOf(
            ControlType.AnnouncementWidget,
            ControlType.ProgressWidget,
            ControlType.ControlTemperatureWidget,
            ControlType.WebcamWidget,
            ControlType.MoveToolWidget,
            ControlType.GcodePreviewWidget,
            ControlType.CancelObject,
            ControlType.TuneWidget,
            ControlType.MultiTool,
            ControlType.ExtrudeWidget,
            ControlType.SendGcodeWidget,
            ControlType.QuickAccessWidget
        ),
    )
    private val forceConnect = octoPrintProvider.passiveEventFlow(instanceId = instanceId).filter {
        (Clock.System.now() - it.localTime) < 1.seconds
    }.map {
        // If we are disconnected for more than 3 attempts, force move to connect state
        val counter = (it as? Event.Disconnected)?.connectionAttemptCounter
        (counter ?: 0) >= 3
    }.distinctUntilChanged().onEach {
        Napier.i(tag = tag, message = "Force connect: $it")
    }.onStart {
        emit(false)
    }

    private val controls = widgetPreferences.getWidgetOrderFlow().map { controls ->
        listOf(State.Connect::class, State.Prepare::class, State.Print::class).associateWith {
            controls[it.listId].toControls(it)
        }
    }

    private val currentMessage = merge(
        flowOf(null),
        octoPrintProvider.passiveCurrentMessageFlow(tag = "controls-vm/state", instanceId = instanceId)
    )

    // Init with null state as if the printer was never connected we won't get a current message. We then default to "connect"
    val state = combine(currentMessage, controls, forceConnect) { current, controls, forceConnect ->
        when {
            forceConnect -> State.Connect(controls.connect)
            current == null -> State.Connect(controls.connect)
            current.state.flags.isPrinting() -> State.Print(
                progress = current.progress?.completion?.toInt() ?: 0,
                pausing = current.state.flags.pausing,
                paused = current.state.flags.paused,
                cancelling = current.state.flags.cancelling,
                controls = controls.print,
            )

            current.state.flags.isOperational() -> State.Prepare(controls = controls.prepare)
            else -> State.Connect(controls = controls.connect)
        }
    }.onEach {
        currentListId = it::class.listId
    }.onStart {
        val controls = State.Connect::class.let { widgetPreferences.getWidgetOrder(it.listId).toControls(it) }
        emit(State.Connect(controls = controls))
    }.distinctUntilChanged().onEach {
        Napier.i(tag = tag, message = "Controls for ${it::class.simpleName}: ${it.controls}")
    }.rateLimit(rateMs = 500, debounce = true)

    suspend fun cancelPrint() = ExceptionReceivers.runWithErrorMessage(tag = tag) {
        cancelPrintJobUseCase.execute(
            CancelPrintJobUseCase.Params(restoreTemperatures = false, instanceId = instanceId)
        )
    }

    suspend fun togglePause() = ExceptionReceivers.runWithErrorMessage(tag = tag) {
        togglePausePrintJobUseCase.execute(
            TogglePausePrintJobUseCase.Params(instanceId = instanceId)
        )
    }

    suspend fun emergencyStop() = ExceptionReceivers.runWithErrorMessage(tag = tag) {
        emergencyStopUseCase.execute(
            EmergencyStopUseCase.Params(instanceId = instanceId)
        )
    }

    fun updateOrder(controls: List<ControlType>) {
        val listId = currentListId ?: return Napier.w(tag = tag, message = "No currentListId")
        val current = widgetPreferences.getWidgetOrder(listId) ?: ControlsPreferences(listId)
        widgetPreferences.setWidgetOrder(
            listId = listId,
            preferences = current.copy(items = controls)
        )
    }

    fun toggleVisible(type: ControlType) {
        val listId = currentListId ?: return Napier.w(tag = tag, message = "No currentListId")
        val current = (widgetPreferences.getWidgetOrder(listId) ?: ControlsPreferences(listId))
        widgetPreferences.setWidgetOrder(
            listId = listId,
            preferences = current.copy(hidden = if (type in current.hidden) (current.hidden - type) else (current.hidden + type))
        )
    }

    private fun ControlsPreferences?.toControls(type: KClass<out State>): List<Control> {
        val preferences = this ?: ControlsPreferences("ad-hoc")
        val controls = requireNotNull(controlTypes[type]) { "No controls for $type" }

        return preferences.prepare(controls).map { (type, hidden) ->
            Control(type = type, visible = !hidden)
        }
    }

    private val Map<KClass<out State>, List<Control>>.connect
        get() = requireNotNull(this[State.Connect::class]) { "Missing connect" }

    private val Map<KClass<out State>, List<Control>>.prepare
        get() = requireNotNull(this[State.Prepare::class]) { "Missing prepare" }

    private val Map<KClass<out State>, List<Control>>.print
        get() = requireNotNull(this[State.Print::class]) { "Missing print" }

    private val KClass<out State>.listId
        get() = when (this) {
            State.Connect::class -> LIST_CONNECT
            State.Prepare::class -> LIST_PREPARE
            State.Print::class -> LIST_PRINT
            else -> throw IllegalStateException("Unknown type: $this")
        }

    sealed class State {
        abstract val controls: List<Control>

        data class Connect(override val controls: List<Control>) : State()
        data class Prepare(override val controls: List<Control>) : State()
        data class Print(val progress: Int, val pausing: Boolean, val paused: Boolean, val cancelling: Boolean, override val controls: List<Control>) : State()
    }

    data class Control(
        val type: ControlType,
        val visible: Boolean,
    )
}