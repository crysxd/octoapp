package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.HandleObicoAppPortalSuccessUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.update

@OptIn(ExperimentalCoroutinesApi::class)
class RemoteAccessObicoViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    override val tag = "RemoteAccessObicoViewModelCore"
    override val remoteServiceName = RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OBICO
    override val loadingFlow = HandleObicoAppPortalSuccessUseCase.loadingFlow
    private val printerConfig = SharedBaseInjector.get().printerConfigRepository
    private val printerProvider = SharedBaseInjector.get().printerEngineProvider

    private val reloadTrigger = MutableStateFlow(0)
    val dataUsage = reloadTrigger
        .combine(printerConfig.instanceInformationFlow(instanceId = instanceId)) { _, instance -> instance }
        .distinctUntilChangedBy { it?.alternativeWebUrl }
        .flatMapLatest { instance ->
            flow {
                emit(FlowState.Loading())

                // Ensure user sees loading :)
                delay(350)

                if (instance?.alternativeWebUrl?.isObicoUrl() == true) {
                    val obicoUrl = requireNotNull(instance.alternativeWebUrl)
                    val obicoOnlyInstance = instance.copy(webUrl = obicoUrl, alternativeWebUrl = null)
                    val state = printerProvider.createAdHocPrinter(obicoOnlyInstance, logTag = "HTTP/RemoteAccessObicoViewModelCore").obicoApi.getDataUsage()

                    emit(FlowState.Ready(state))
                } else {
                    emit(FlowState.Loading())
                }
            }
        }.catch {
            Napier.e(tag = tag, message = "Failed to load Obico data usage", throwable = it)
            emit(FlowState.Error(it))
        }

    override fun Url.isMyUrl() = isObicoUrl()

    suspend fun getLoginUrl(baseUrl: String?): String? = getLoginUrl(
        GetRemoteServiceConnectUrlUseCase.RemoteService.Obico(baseUrl = baseUrl?.toUrl())
    )

    suspend fun reloadDataUsage() {
        reloadTrigger.update { it + 1 }
        dataUsage.first { it !is FlowState.Loading }
    }
}