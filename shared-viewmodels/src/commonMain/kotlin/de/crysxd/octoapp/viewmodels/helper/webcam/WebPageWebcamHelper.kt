package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

internal expect class WebPageWebcamHelper(core: BaseViewModelCore) {

    suspend fun emit(
        collector: FlowCollector<State>,
        settings: ResolvedWebcamSettings.WebPageWebcamSettings,
        activeWebcam: Int,
        webcamCount: Int,
    )
}

internal object WebPageWebcamHelperCore {

    fun constructHtml(settings: ResolvedWebcamSettings.WebPageWebcamSettings) = when (val type = settings.webcam.type) {
        WebcamSettings.Webcam.Type.WebRtcCameraStreamer -> WebPageWebcamConstants.cameraStreamerHtml
        WebcamSettings.Webcam.Type.WebRtcJanus -> WebPageWebcamConstants.janusHtml
        WebcamSettings.Webcam.Type.WebRtcGoRtc -> WebPageWebcamConstants.go2rtcHtml
        WebcamSettings.Webcam.Type.WebRtcMediaMtx -> WebPageWebcamConstants.mediaMtxHtml
        WebcamSettings.Webcam.Type.Hls -> WebPageWebcamConstants.hlsHtml
        WebcamSettings.Webcam.Type.Iframe -> WebPageWebcamConstants.iFrameHtml
        else -> throw IllegalStateException("$type is not supported!")
    }
        .replace(WebPageWebcamConstants.URL_PLACEHOLDER, settings.url.toString())
        .replace(WebPageWebcamConstants.EXTRA_HEAERS_PLACEHOLDER, Json.encodeToString(settings.extraHeaders))
        .replace(WebPageWebcamConstants.ADDITIONAL_JS_PLACEHOLDER, WebPageWebcamConstants.ADDITIONAL_JS_PLACEHOLDER)
}


object WebPageWebcamConstants {
    const val URL_PLACEHOLDER = "{{{url}}}"
    const val EXTRA_HEAERS_PLACEHOLDER = "{{{extraheaders}}}"
    const val ADDITIONAL_JS_PLACEHOLDER = "{{{js}}}"

    private const val D = "$"

    //region private val jqueryJs = ...
    private val jqueryJs = """
        /*! jQuery v3.7.1 -ajax,-ajax/jsonp,-ajax/load,-ajax/script,-ajax/var/location,-ajax/var/nonce,-ajax/var/rquery,-ajax/xhr,-manipulation/_evalUrl,-deprecated/ajax-event-alias,-effects,-effects/animatedSelector,-effects/Tween | (c) OpenJS Foundation and other contributors | jquery.org/license */
        !function(e,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new Error("jQuery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof window?window:this,function(ie,e){"use strict";var oe=[],r=Object.getPrototypeOf,ae=oe.slice,g=oe.flat?function(e){return oe.flat.call(e)}:function(e){return oe.concat.apply([],e)},s=oe.push,se=oe.indexOf,n={},i=n.toString,ue=n.hasOwnProperty,o=ue.toString,a=o.call(Object),le={},v=function(e){return"function"==typeof e&&"number"!=typeof e.nodeType&&"function"!=typeof e.item},y=function(e){return null!=e&&e===e.window},m=ie.document,u={type:!0,src:!0,nonce:!0,noModule:!0};function b(e,t,n){var r,i,o=(n=n||m).createElement("script");if(o.text=e,t)for(r in u)(i=t[r]||t.getAttribute&&t.getAttribute(r))&&o.setAttribute(r,i);n.head.appendChild(o).parentNode.removeChild(o)}function x(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?n[i.call(e)]||"object":typeof e}var t="3.7.1 -ajax,-ajax/jsonp,-ajax/load,-ajax/script,-ajax/var/location,-ajax/var/nonce,-ajax/var/rquery,-ajax/xhr,-manipulation/_evalUrl,-deprecated/ajax-event-alias,-effects,-effects/animatedSelector,-effects/Tween",l=/HTML${'$'}/i,ce=function(e,t){return new ce.fn.init(e,t)};function c(e){var t=!!e&&"length"in e&&e.length,n=x(e);return!v(e)&&!y(e)&&("array"===n||0===t||"number"==typeof t&&0<t&&t-1 in e)}function fe(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()}ce.fn=ce.prototype={jquery:t,constructor:ce,length:0,toArray:function(){return ae.call(this)},get:function(e){return null==e?ae.call(this):e<0?this[e+this.length]:this[e]},pushStack:function(e){var t=ce.merge(this.constructor(),e);return t.prevObject=this,t},each:function(e){return ce.each(this,e)},map:function(n){return this.pushStack(ce.map(this,function(e,t){return n.call(e,t,e)}))},slice:function(){return this.pushStack(ae.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},even:function(){return this.pushStack(ce.grep(this,function(e,t){return(t+1)%2}))},odd:function(){return this.pushStack(ce.grep(this,function(e,t){return t%2}))},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(0<=n&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:s,sort:oe.sort,splice:oe.splice},ce.extend=ce.fn.extend=function(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof a&&(l=a,a=arguments[s]||{},s++),"object"==typeof a||v(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)r=e[t],"__proto__"!==t&&a!==r&&(l&&r&&(ce.isPlainObject(r)||(i=Array.isArray(r)))?(n=a[t],o=i&&!Array.isArray(n)?[]:i||ce.isPlainObject(n)?n:{},i=!1,a[t]=ce.extend(l,o,r)):void 0!==r&&(a[t]=r));return a},ce.extend({expando:"jQuery"+(t+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isPlainObject:function(e){var t,n;return!(!e||"[object Object]"!==i.call(e))&&(!(t=r(e))||"function"==typeof(n=ue.call(t,"constructor")&&t.constructor)&&o.call(n)===a)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},globalEval:function(e,t,n){b(e,{nonce:t&&t.nonce},n)},each:function(e,t){var n,r=0;if(c(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in e)if(!1===t.call(e[r],r,e[r]))break;return e},text:function(e){var t,n="",r=0,i=e.nodeType;if(!i)while(t=e[r++])n+=ce.text(t);return 1===i||11===i?e.textContent:9===i?e.documentElement.textContent:3===i||4===i?e.nodeValue:n},makeArray:function(e,t){var n=t||[];return null!=e&&(c(Object(e))?ce.merge(n,"string"==typeof e?[e]:e):s.call(n,e)),n},inArray:function(e,t,n){return null==t?-1:se.call(t,e,n)},isXMLDoc:function(e){var t=e&&e.namespaceURI,n=e&&(e.ownerDocument||e).documentElement;return!l.test(t||n&&n.nodeName||"HTML")},merge:function(e,t){for(var n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var r=[],i=0,o=e.length,a=!n;i<o;i++)!t(e[i],i)!==a&&r.push(e[i]);return r},map:function(e,t,n){var r,i,o=0,a=[];if(c(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&a.push(i);else for(o in e)null!=(i=t(e[o],o,n))&&a.push(i);return g(a)},guid:1,support:le}),"function"==typeof Symbol&&(ce.fn[Symbol.iterator]=oe[Symbol.iterator]),ce.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){n["[object "+t+"]"]=t.toLowerCase()});var de=oe.pop,pe=oe.sort,he=oe.splice,ge="[\\x20\\t\\r\\n\\f]",ve=new RegExp("^"+ge+"+|((?:^|[^\\\\])(?:\\\\.)*)"+ge+"+${'$'}","g");ce.contains=function(e,t){var n=t&&t.parentNode;return e===n||!(!n||1!==n.nodeType||!(e.contains?e.contains(n):e.compareDocumentPosition&&16&e.compareDocumentPosition(n)))};var f=/([\0-\x1f\x7f]|^-?\d)|^-${'$'}|[^\x80-\uFFFF\w-]/g;function d(e,t){return t?"\0"===e?"\ufffd":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e}ce.escapeSelector=function(e){return(e+"").replace(f,d)};var ye=m,me=s;!function(){var e,x,w,o,a,C,r,T,p,i,E=me,k=ce.expando,S=0,n=0,s=W(),c=W(),u=W(),h=W(),l=function(e,t){return e===t&&(a=!0),0},f="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",t="(?:\\\\[\\da-fA-F]{1,6}"+ge+"?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",d="\\["+ge+"*("+t+")(?:"+ge+"*([*^${'$'}|!~]?=)"+ge+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+t+"))|)"+ge+"*\\]",g=":("+t+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+d+")*)|.*)\\)|)",v=new RegExp(ge+"+","g"),y=new RegExp("^"+ge+"*,"+ge+"*"),m=new RegExp("^"+ge+"*([>+~]|"+ge+")"+ge+"*"),b=new RegExp(ge+"|>"),A=new RegExp(g),D=new RegExp("^"+t+"${'$'}"),N={ID:new RegExp("^#("+t+")"),CLASS:new RegExp("^\\.("+t+")"),TAG:new RegExp("^("+t+"|[*])"),ATTR:new RegExp("^"+d),PSEUDO:new RegExp("^"+g),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+ge+"*(even|odd|(([+-]|)(\\d*)n|)"+ge+"*(?:([+-]|)"+ge+"*(\\d+)|))"+ge+"*\\)|)","i"),bool:new RegExp("^(?:"+f+")${'$'}","i"),needsContext:new RegExp("^"+ge+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+ge+"*((?:-\\d)?\\d*)"+ge+"*\\)|)(?=[^-]|${'$'})","i")},L=/^(?:input|select|textarea|button)${'$'}/i,j=/^h\d${'$'}/i,O=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))${'$'}/,P=/[+~]/,H=new RegExp("\\\\[\\da-fA-F]{1,6}"+ge+"?|\\\\([^\\r\\n\\f])","g"),q=function(e,t){var n="0x"+e.slice(1)-65536;return t||(n<0?String.fromCharCode(n+65536):String.fromCharCode(n>>10|55296,1023&n|56320))},R=function(){V()},M=K(function(e){return!0===e.disabled&&fe(e,"fieldset")},{dir:"parentNode",next:"legend"});try{E.apply(oe=ae.call(ye.childNodes),ye.childNodes),oe[ye.childNodes.length].nodeType}catch(e){E={apply:function(e,t){me.apply(e,ae.call(t))},call:function(e){me.apply(e,ae.call(arguments,1))}}}function I(t,e,n,r){var i,o,a,s,u,l,c,f=e&&e.ownerDocument,d=e?e.nodeType:9;if(n=n||[],"string"!=typeof t||!t||1!==d&&9!==d&&11!==d)return n;if(!r&&(V(e),e=e||C,T)){if(11!==d&&(u=O.exec(t)))if(i=u[1]){if(9===d){if(!(a=e.getElementById(i)))return n;if(a.id===i)return E.call(n,a),n}else if(f&&(a=f.getElementById(i))&&I.contains(e,a)&&a.id===i)return E.call(n,a),n}else{if(u[2])return E.apply(n,e.getElementsByTagName(t)),n;if((i=u[3])&&e.getElementsByClassName)return E.apply(n,e.getElementsByClassName(i)),n}if(!(h[t+" "]||p&&p.test(t))){if(c=t,f=e,1===d&&(b.test(t)||m.test(t))){(f=P.test(t)&&X(e.parentNode)||e)==e&&le.scope||((s=e.getAttribute("id"))?s=ce.escapeSelector(s):e.setAttribute("id",s=k)),o=(l=Y(t)).length;while(o--)l[o]=(s?"#"+s:":scope")+" "+G(l[o]);c=l.join(",")}try{return E.apply(n,f.querySelectorAll(c)),n}catch(e){h(t,!0)}finally{s===k&&e.removeAttribute("id")}}}return re(t.replace(ve,"${'$'}1"),e,n,r)}function W(){var r=[];return function e(t,n){return r.push(t+" ")>x.cacheLength&&delete e[r.shift()],e[t+" "]=n}}function B(e){return e[k]=!0,e}function F(e){var t=C.createElement("fieldset");try{return!!e(t)}catch(e){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function ${'$'}(t){return function(e){return fe(e,"input")&&e.type===t}}function _(t){return function(e){return(fe(e,"input")||fe(e,"button"))&&e.type===t}}function z(t){return function(e){return"form"in e?e.parentNode&&!1===e.disabled?"label"in e?"label"in e.parentNode?e.parentNode.disabled===t:e.disabled===t:e.isDisabled===t||e.isDisabled!==!t&&M(e)===t:e.disabled===t:"label"in e&&e.disabled===t}}function U(a){return B(function(o){return o=+o,B(function(e,t){var n,r=a([],e.length,o),i=r.length;while(i--)e[n=r[i]]&&(e[n]=!(t[n]=e[n]))})})}function X(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}function V(e){var t,n=e?e.ownerDocument||e:ye;return n!=C&&9===n.nodeType&&n.documentElement&&(r=(C=n).documentElement,T=!ce.isXMLDoc(C),i=r.matches||r.webkitMatchesSelector||r.msMatchesSelector,r.msMatchesSelector&&ye!=C&&(t=C.defaultView)&&t.top!==t&&t.addEventListener("unload",R),le.getById=F(function(e){return r.appendChild(e).id=ce.expando,!C.getElementsByName||!C.getElementsByName(ce.expando).length}),le.disconnectedMatch=F(function(e){return i.call(e,"*")}),le.scope=F(function(){return C.querySelectorAll(":scope")}),le.cssHas=F(function(){try{return C.querySelector(":has(*,:jqfake)"),!1}catch(e){return!0}}),le.getById?(x.filter.ID=function(e){var t=e.replace(H,q);return function(e){return e.getAttribute("id")===t}},x.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&T){var n=t.getElementById(e);return n?[n]:[]}}):(x.filter.ID=function(e){var n=e.replace(H,q);return function(e){var t="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return t&&t.value===n}},x.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&T){var n,r,i,o=t.getElementById(e);if(o){if((n=o.getAttributeNode("id"))&&n.value===e)return[o];i=t.getElementsByName(e),r=0;while(o=i[r++])if((n=o.getAttributeNode("id"))&&n.value===e)return[o]}return[]}}),x.find.TAG=function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):t.querySelectorAll(e)},x.find.CLASS=function(e,t){if("undefined"!=typeof t.getElementsByClassName&&T)return t.getElementsByClassName(e)},p=[],F(function(e){var t;r.appendChild(e).innerHTML="<a id='"+k+"' href='' disabled='disabled'></a><select id='"+k+"-\r\\' disabled='disabled'><option selected=''></option></select>",e.querySelectorAll("[selected]").length||p.push("\\["+ge+"*(?:value|"+f+")"),e.querySelectorAll("[id~="+k+"-]").length||p.push("~="),e.querySelectorAll("a#"+k+"+*").length||p.push(".#.+[+~]"),e.querySelectorAll(":checked").length||p.push(":checked"),(t=C.createElement("input")).setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),r.appendChild(e).disabled=!0,2!==e.querySelectorAll(":disabled").length&&p.push(":enabled",":disabled"),(t=C.createElement("input")).setAttribute("name",""),e.appendChild(t),e.querySelectorAll("[name='']").length||p.push("\\["+ge+"*name"+ge+"*="+ge+"*(?:''|\"\")")}),le.cssHas||p.push(":has"),p=p.length&&new RegExp(p.join("|")),l=function(e,t){if(e===t)return a=!0,0;var n=!e.compareDocumentPosition-!t.compareDocumentPosition;return n||(1&(n=(e.ownerDocument||e)==(t.ownerDocument||t)?e.compareDocumentPosition(t):1)||!le.sortDetached&&t.compareDocumentPosition(e)===n?e===C||e.ownerDocument==ye&&I.contains(ye,e)?-1:t===C||t.ownerDocument==ye&&I.contains(ye,t)?1:o?se.call(o,e)-se.call(o,t):0:4&n?-1:1)}),C}for(e in I.matches=function(e,t){return I(e,null,null,t)},I.matchesSelector=function(e,t){if(V(e),T&&!h[t+" "]&&(!p||!p.test(t)))try{var n=i.call(e,t);if(n||le.disconnectedMatch||e.document&&11!==e.document.nodeType)return n}catch(e){h(t,!0)}return 0<I(t,C,null,[e]).length},I.contains=function(e,t){return(e.ownerDocument||e)!=C&&V(e),ce.contains(e,t)},I.attr=function(e,t){(e.ownerDocument||e)!=C&&V(e);var n=x.attrHandle[t.toLowerCase()],r=n&&ue.call(x.attrHandle,t.toLowerCase())?n(e,t,!T):void 0;return void 0!==r?r:e.getAttribute(t)},I.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},ce.uniqueSort=function(e){var t,n=[],r=0,i=0;if(a=!le.sortStable,o=!le.sortStable&&ae.call(e,0),pe.call(e,l),a){while(t=e[i++])t===e[i]&&(r=n.push(i));while(r--)he.call(e,n[r],1)}return o=null,e},ce.fn.uniqueSort=function(){return this.pushStack(ce.uniqueSort(ae.apply(this)))},(x=ce.expr={cacheLength:50,createPseudo:B,match:N,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(H,q),e[3]=(e[3]||e[4]||e[5]||"").replace(H,q),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||I.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&I.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return N.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&A.test(n)&&(t=Y(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(H,q).toLowerCase();return"*"===e?function(){return!0}:function(e){return fe(e,t)}},CLASS:function(e){var t=s[e+" "];return t||(t=new RegExp("(^|"+ge+")"+e+"("+ge+"|${'$'})"))&&s(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(n,r,i){return function(e){var t=I.attr(e,n);return null==t?"!="===r:!r||(t+="","="===r?t===i:"!="===r?t!==i:"^="===r?i&&0===t.indexOf(i):"*="===r?i&&-1<t.indexOf(i):"${'$'}="===r?i&&t.slice(-i.length)===i:"~="===r?-1<(" "+t.replace(v," ")+" ").indexOf(i):"|="===r&&(t===i||t.slice(0,i.length+1)===i+"-"))}},CHILD:function(p,e,t,h,g){var v="nth"!==p.slice(0,3),y="last"!==p.slice(-4),m="of-type"===e;return 1===h&&0===g?function(e){return!!e.parentNode}:function(e,t,n){var r,i,o,a,s,u=v!==y?"nextSibling":"previousSibling",l=e.parentNode,c=m&&e.nodeName.toLowerCase(),f=!n&&!m,d=!1;if(l){if(v){while(u){o=e;while(o=o[u])if(m?fe(o,c):1===o.nodeType)return!1;s=u="only"===p&&!s&&"nextSibling"}return!0}if(s=[y?l.firstChild:l.lastChild],y&&f){d=(a=(r=(i=l[k]||(l[k]={}))[p]||[])[0]===S&&r[1])&&r[2],o=a&&l.childNodes[a];while(o=++a&&o&&o[u]||(d=a=0)||s.pop())if(1===o.nodeType&&++d&&o===e){i[p]=[S,a,d];break}}else if(f&&(d=a=(r=(i=e[k]||(e[k]={}))[p]||[])[0]===S&&r[1]),!1===d)while(o=++a&&o&&o[u]||(d=a=0)||s.pop())if((m?fe(o,c):1===o.nodeType)&&++d&&(f&&((i=o[k]||(o[k]={}))[p]=[S,d]),o===e))break;return(d-=g)===h||d%h==0&&0<=d/h}}},PSEUDO:function(e,o){var t,a=x.pseudos[e]||x.setFilters[e.toLowerCase()]||I.error("unsupported pseudo: "+e);return a[k]?a(o):1<a.length?(t=[e,e,"",o],x.setFilters.hasOwnProperty(e.toLowerCase())?B(function(e,t){var n,r=a(e,o),i=r.length;while(i--)e[n=se.call(e,r[i])]=!(t[n]=r[i])}):function(e){return a(e,0,t)}):a}},pseudos:{not:B(function(e){var r=[],i=[],s=ne(e.replace(ve,"${'$'}1"));return s[k]?B(function(e,t,n,r){var i,o=s(e,null,r,[]),a=e.length;while(a--)(i=o[a])&&(e[a]=!(t[a]=i))}):function(e,t,n){return r[0]=e,s(r,null,n,i),r[0]=null,!i.pop()}}),has:B(function(t){return function(e){return 0<I(t,e).length}}),contains:B(function(t){return t=t.replace(H,q),function(e){return-1<(e.textContent||ce.text(e)).indexOf(t)}}),lang:B(function(n){return D.test(n||"")||I.error("unsupported lang: "+n),n=n.replace(H,q).toLowerCase(),function(e){var t;do{if(t=T?e.lang:e.getAttribute("xml:lang")||e.getAttribute("lang"))return(t=t.toLowerCase())===n||0===t.indexOf(n+"-")}while((e=e.parentNode)&&1===e.nodeType);return!1}}),target:function(e){var t=ie.location&&ie.location.hash;return t&&t.slice(1)===e.id},root:function(e){return e===r},focus:function(e){return e===function(){try{return C.activeElement}catch(e){}}()&&C.hasFocus()&&!!(e.type||e.href||~e.tabIndex)},enabled:z(!1),disabled:z(!0),checked:function(e){return fe(e,"input")&&!!e.checked||fe(e,"option")&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,!0===e.selected},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!x.pseudos.empty(e)},header:function(e){return j.test(e.nodeName)},input:function(e){return L.test(e.nodeName)},button:function(e){return fe(e,"input")&&"button"===e.type||fe(e,"button")},text:function(e){var t;return fe(e,"input")&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:U(function(){return[0]}),last:U(function(e,t){return[t-1]}),eq:U(function(e,t,n){return[n<0?n+t:n]}),even:U(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:U(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:U(function(e,t,n){var r;for(r=n<0?n+t:t<n?t:n;0<=--r;)e.push(r);return e}),gt:U(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}}).pseudos.nth=x.pseudos.eq,{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})x.pseudos[e]=${'$'}(e);for(e in{submit:!0,reset:!0})x.pseudos[e]=_(e);function Q(){}function Y(e,t){var n,r,i,o,a,s,u,l=c[e+" "];if(l)return t?0:l.slice(0);a=e,s=[],u=x.preFilter;while(a){for(o in n&&!(r=y.exec(a))||(r&&(a=a.slice(r[0].length)||a),s.push(i=[])),n=!1,(r=m.exec(a))&&(n=r.shift(),i.push({value:n,type:r[0].replace(ve," ")}),a=a.slice(n.length)),x.filter)!(r=N[o].exec(a))||u[o]&&!(r=u[o](r))||(n=r.shift(),i.push({value:n,type:o,matches:r}),a=a.slice(n.length));if(!n)break}return t?a.length:a?I.error(e):c(e,s).slice(0)}function G(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function K(a,e,t){var s=e.dir,u=e.next,l=u||s,c=t&&"parentNode"===l,f=n++;return e.first?function(e,t,n){while(e=e[s])if(1===e.nodeType||c)return a(e,t,n);return!1}:function(e,t,n){var r,i,o=[S,f];if(n){while(e=e[s])if((1===e.nodeType||c)&&a(e,t,n))return!0}else while(e=e[s])if(1===e.nodeType||c)if(i=e[k]||(e[k]={}),u&&fe(e,u))e=e[s]||e;else{if((r=i[l])&&r[0]===S&&r[1]===f)return o[2]=r[2];if((i[l]=o)[2]=a(e,t,n))return!0}return!1}}function J(i){return 1<i.length?function(e,t,n){var r=i.length;while(r--)if(!i[r](e,t,n))return!1;return!0}:i[0]}function Z(e,t,n,r,i){for(var o,a=[],s=0,u=e.length,l=null!=t;s<u;s++)(o=e[s])&&(n&&!n(o,r,i)||(a.push(o),l&&t.push(s)));return a}function ee(p,h,g,v,y,e){return v&&!v[k]&&(v=ee(v)),y&&!y[k]&&(y=ee(y,e)),B(function(e,t,n,r){var i,o,a,s,u=[],l=[],c=t.length,f=e||function(e,t,n){for(var r=0,i=t.length;r<i;r++)I(e,t[r],n);return n}(h||"*",n.nodeType?[n]:n,[]),d=!p||!e&&h?f:Z(f,u,p,n,r);if(g?g(d,s=y||(e?p:c||v)?[]:t,n,r):s=d,v){i=Z(s,l),v(i,[],n,r),o=i.length;while(o--)(a=i[o])&&(s[l[o]]=!(d[l[o]]=a))}if(e){if(y||p){if(y){i=[],o=s.length;while(o--)(a=s[o])&&i.push(d[o]=a);y(null,s=[],i,r)}o=s.length;while(o--)(a=s[o])&&-1<(i=y?se.call(e,a):u[o])&&(e[i]=!(t[i]=a))}}else s=Z(s===t?s.splice(c,s.length):s),y?y(null,t,s,r):E.apply(t,s)})}function te(e){for(var i,t,n,r=e.length,o=x.relative[e[0].type],a=o||x.relative[" "],s=o?1:0,u=K(function(e){return e===i},a,!0),l=K(function(e){return-1<se.call(i,e)},a,!0),c=[function(e,t,n){var r=!o&&(n||t!=w)||((i=t).nodeType?u(e,t,n):l(e,t,n));return i=null,r}];s<r;s++)if(t=x.relative[e[s].type])c=[K(J(c),t)];else{if((t=x.filter[e[s].type].apply(null,e[s].matches))[k]){for(n=++s;n<r;n++)if(x.relative[e[n].type])break;return ee(1<s&&J(c),1<s&&G(e.slice(0,s-1).concat({value:" "===e[s-2].type?"*":""})).replace(ve,"${'$'}1"),t,s<n&&te(e.slice(s,n)),n<r&&te(e=e.slice(n)),n<r&&G(e))}c.push(t)}return J(c)}function ne(e,t){var n,v,y,m,b,r,i=[],o=[],a=u[e+" "];if(!a){t||(t=Y(e)),n=t.length;while(n--)(a=te(t[n]))[k]?i.push(a):o.push(a);(a=u(e,(v=o,m=0<(y=i).length,b=0<v.length,r=function(e,t,n,r,i){var o,a,s,u=0,l="0",c=e&&[],f=[],d=w,p=e||b&&x.find.TAG("*",i),h=S+=null==d?1:Math.random()||.1,g=p.length;for(i&&(w=t==C||t||i);l!==g&&null!=(o=p[l]);l++){if(b&&o){a=0,t||o.ownerDocument==C||(V(o),n=!T);while(s=v[a++])if(s(o,t||C,n)){E.call(r,o);break}i&&(S=h)}m&&((o=!s&&o)&&u--,e&&c.push(o))}if(u+=l,m&&l!==u){a=0;while(s=y[a++])s(c,f,t,n);if(e){if(0<u)while(l--)c[l]||f[l]||(f[l]=de.call(r));f=Z(f)}E.apply(r,f),i&&!e&&0<f.length&&1<u+y.length&&ce.uniqueSort(r)}return i&&(S=h,w=d),c},m?B(r):r))).selector=e}return a}function re(e,t,n,r){var i,o,a,s,u,l="function"==typeof e&&e,c=!r&&Y(e=l.selector||e);if(n=n||[],1===c.length){if(2<(o=c[0]=c[0].slice(0)).length&&"ID"===(a=o[0]).type&&9===t.nodeType&&T&&x.relative[o[1].type]){if(!(t=(x.find.ID(a.matches[0].replace(H,q),t)||[])[0]))return n;l&&(t=t.parentNode),e=e.slice(o.shift().value.length)}i=N.needsContext.test(e)?0:o.length;while(i--){if(a=o[i],x.relative[s=a.type])break;if((u=x.find[s])&&(r=u(a.matches[0].replace(H,q),P.test(o[0].type)&&X(t.parentNode)||t))){if(o.splice(i,1),!(e=r.length&&G(o)))return E.apply(n,r),n;break}}}return(l||ne(e,c))(r,t,!T,n,!t||P.test(e)&&X(t.parentNode)||t),n}Q.prototype=x.filters=x.pseudos,x.setFilters=new Q,le.sortStable=k.split("").sort(l).join("")===k,V(),le.sortDetached=F(function(e){return 1&e.compareDocumentPosition(C.createElement("fieldset"))}),ce.find=I,ce.expr[":"]=ce.expr.pseudos,ce.unique=ce.uniqueSort,I.compile=ne,I.select=re,I.setDocument=V,I.tokenize=Y,I.escape=ce.escapeSelector,I.getText=ce.text,I.isXML=ce.isXMLDoc,I.selectors=ce.expr,I.support=ce.support,I.uniqueSort=ce.uniqueSort}();var p=function(e,t,n){var r=[],i=void 0!==n;while((e=e[t])&&9!==e.nodeType)if(1===e.nodeType){if(i&&ce(e).is(n))break;r.push(e)}return r},h=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},w=ce.expr.match.needsContext,C=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)${'$'}/i;function T(e,n,r){return v(n)?ce.grep(e,function(e,t){return!!n.call(e,t,e)!==r}):n.nodeType?ce.grep(e,function(e){return e===n!==r}):"string"!=typeof n?ce.grep(e,function(e){return-1<se.call(n,e)!==r}):ce.filter(n,e,r)}ce.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?ce.find.matchesSelector(r,e)?[r]:[]:ce.find.matches(e,ce.grep(t,function(e){return 1===e.nodeType}))},ce.fn.extend({find:function(e){var t,n,r=this.length,i=this;if("string"!=typeof e)return this.pushStack(ce(e).filter(function(){for(t=0;t<r;t++)if(ce.contains(i[t],this))return!0}));for(n=this.pushStack([]),t=0;t<r;t++)ce.find(e,i[t],n);return 1<r?ce.uniqueSort(n):n},filter:function(e){return this.pushStack(T(this,e||[],!1))},not:function(e){return this.pushStack(T(this,e||[],!0))},is:function(e){return!!T(this,"string"==typeof e&&w.test(e)?ce(e):e||[],!1).length}});var E,k=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))${'$'}/;(ce.fn.init=function(e,t,n){var r,i;if(!e)return this;if(n=n||E,"string"==typeof e){if(!(r="<"===e[0]&&">"===e[e.length-1]&&3<=e.length?[null,e,null]:k.exec(e))||!r[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(r[1]){if(t=t instanceof ce?t[0]:t,ce.merge(this,ce.parseHTML(r[1],t&&t.nodeType?t.ownerDocument||t:m,!0)),C.test(r[1])&&ce.isPlainObject(t))for(r in t)v(this[r])?this[r](t[r]):this.attr(r,t[r]);return this}return(i=m.getElementById(r[2]))&&(this[0]=i,this.length=1),this}return e.nodeType?(this[0]=e,this.length=1,this):v(e)?void 0!==n.ready?n.ready(e):e(ce):ce.makeArray(e,this)}).prototype=ce.fn,E=ce(m);var S=/^(?:parents|prev(?:Until|All))/,A={children:!0,contents:!0,next:!0,prev:!0};function D(e,t){while((e=e[t])&&1!==e.nodeType);return e}ce.fn.extend({has:function(e){var t=ce(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(ce.contains(this,t[e]))return!0})},closest:function(e,t){var n,r=0,i=this.length,o=[],a="string"!=typeof e&&ce(e);if(!w.test(e))for(;r<i;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?-1<a.index(n):1===n.nodeType&&ce.find.matchesSelector(n,e))){o.push(n);break}return this.pushStack(1<o.length?ce.uniqueSort(o):o)},index:function(e){return e?"string"==typeof e?se.call(ce(e),this[0]):se.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(ce.uniqueSort(ce.merge(this.get(),ce(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}}),ce.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return p(e,"parentNode")},parentsUntil:function(e,t,n){return p(e,"parentNode",n)},next:function(e){return D(e,"nextSibling")},prev:function(e){return D(e,"previousSibling")},nextAll:function(e){return p(e,"nextSibling")},prevAll:function(e){return p(e,"previousSibling")},nextUntil:function(e,t,n){return p(e,"nextSibling",n)},prevUntil:function(e,t,n){return p(e,"previousSibling",n)},siblings:function(e){return h((e.parentNode||{}).firstChild,e)},children:function(e){return h(e.firstChild)},contents:function(e){return null!=e.contentDocument&&r(e.contentDocument)?e.contentDocument:(fe(e,"template")&&(e=e.content||e),ce.merge([],e.childNodes))}},function(r,i){ce.fn[r]=function(e,t){var n=ce.map(this,i,e);return"Until"!==r.slice(-5)&&(t=e),t&&"string"==typeof t&&(n=ce.filter(t,n)),1<this.length&&(A[r]||ce.uniqueSort(n),S.test(r)&&n.reverse()),this.pushStack(n)}});var N=/[^\x20\t\r\n\f]+/g;function L(e){return e}function j(e){throw e}function O(e,t,n,r){var i;try{e&&v(i=e.promise)?i.call(e).done(t).fail(n):e&&v(i=e.then)?i.call(e,t,n):t.apply(void 0,[e].slice(r))}catch(e){n.apply(void 0,[e])}}ce.Callbacks=function(r){var e,n;r="string"==typeof r?(e=r,n={},ce.each(e.match(N)||[],function(e,t){n[t]=!0}),n):ce.extend({},r);var i,t,o,a,s=[],u=[],l=-1,c=function(){for(a=a||r.once,o=i=!0;u.length;l=-1){t=u.shift();while(++l<s.length)!1===s[l].apply(t[0],t[1])&&r.stopOnFalse&&(l=s.length,t=!1)}r.memory||(t=!1),i=!1,a&&(s=t?[]:"")},f={add:function(){return s&&(t&&!i&&(l=s.length-1,u.push(t)),function n(e){ce.each(e,function(e,t){v(t)?r.unique&&f.has(t)||s.push(t):t&&t.length&&"string"!==x(t)&&n(t)})}(arguments),t&&!i&&c()),this},remove:function(){return ce.each(arguments,function(e,t){var n;while(-1<(n=ce.inArray(t,s,n)))s.splice(n,1),n<=l&&l--}),this},has:function(e){return e?-1<ce.inArray(e,s):0<s.length},empty:function(){return s&&(s=[]),this},disable:function(){return a=u=[],s=t="",this},disabled:function(){return!s},lock:function(){return a=u=[],t||i||(s=t=""),this},locked:function(){return!!a},fireWith:function(e,t){return a||(t=[e,(t=t||[]).slice?t.slice():t],u.push(t),i||c()),this},fire:function(){return f.fireWith(this,arguments),this},fired:function(){return!!o}};return f},ce.extend({Deferred:function(e){var o=[["notify","progress",ce.Callbacks("memory"),ce.Callbacks("memory"),2],["resolve","done",ce.Callbacks("once memory"),ce.Callbacks("once memory"),0,"resolved"],["reject","fail",ce.Callbacks("once memory"),ce.Callbacks("once memory"),1,"rejected"]],i="pending",a={state:function(){return i},always:function(){return s.done(arguments).fail(arguments),this},"catch":function(e){return a.then(null,e)},pipe:function(){var i=arguments;return ce.Deferred(function(r){ce.each(o,function(e,t){var n=v(i[t[4]])&&i[t[4]];s[t[1]](function(){var e=n&&n.apply(this,arguments);e&&v(e.promise)?e.promise().progress(r.notify).done(r.resolve).fail(r.reject):r[t[0]+"With"](this,n?[e]:arguments)})}),i=null}).promise()},then:function(t,n,r){var u=0;function l(i,o,a,s){return function(){var n=this,r=arguments,e=function(){var e,t;if(!(i<u)){if((e=a.apply(n,r))===o.promise())throw new TypeError("Thenable self-resolution");t=e&&("object"==typeof e||"function"==typeof e)&&e.then,v(t)?s?t.call(e,l(u,o,L,s),l(u,o,j,s)):(u++,t.call(e,l(u,o,L,s),l(u,o,j,s),l(u,o,L,o.notifyWith))):(a!==L&&(n=void 0,r=[e]),(s||o.resolveWith)(n,r))}},t=s?e:function(){try{e()}catch(e){ce.Deferred.exceptionHook&&ce.Deferred.exceptionHook(e,t.error),u<=i+1&&(a!==j&&(n=void 0,r=[e]),o.rejectWith(n,r))}};i?t():(ce.Deferred.getErrorHook?t.error=ce.Deferred.getErrorHook():ce.Deferred.getStackHook&&(t.error=ce.Deferred.getStackHook()),ie.setTimeout(t))}}return ce.Deferred(function(e){o[0][3].add(l(0,e,v(r)?r:L,e.notifyWith)),o[1][3].add(l(0,e,v(t)?t:L)),o[2][3].add(l(0,e,v(n)?n:j))}).promise()},promise:function(e){return null!=e?ce.extend(e,a):a}},s={};return ce.each(o,function(e,t){var n=t[2],r=t[5];a[t[1]]=n.add,r&&n.add(function(){i=r},o[3-e][2].disable,o[3-e][3].disable,o[0][2].lock,o[0][3].lock),n.add(t[3].fire),s[t[0]]=function(){return s[t[0]+"With"](this===s?void 0:this,arguments),this},s[t[0]+"With"]=n.fireWith}),a.promise(s),e&&e.call(s,s),s},when:function(e){var n=arguments.length,t=n,r=Array(t),i=ae.call(arguments),o=ce.Deferred(),a=function(t){return function(e){r[t]=this,i[t]=1<arguments.length?ae.call(arguments):e,--n||o.resolveWith(r,i)}};if(n<=1&&(O(e,o.done(a(t)).resolve,o.reject,!n),"pending"===o.state()||v(i[t]&&i[t].then)))return o.then();while(t--)O(i[t],a(t),o.reject);return o.promise()}});var P=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error${'$'}/;ce.Deferred.exceptionHook=function(e,t){ie.console&&ie.console.warn&&e&&P.test(e.name)&&ie.console.warn("jQuery.Deferred exception: "+e.message,e.stack,t)},ce.readyException=function(e){ie.setTimeout(function(){throw e})};var H=ce.Deferred();function q(){m.removeEventListener("DOMContentLoaded",q),ie.removeEventListener("load",q),ce.ready()}ce.fn.ready=function(e){return H.then(e)["catch"](function(e){ce.readyException(e)}),this},ce.extend({isReady:!1,readyWait:1,ready:function(e){(!0===e?--ce.readyWait:ce.isReady)||(ce.isReady=!0)!==e&&0<--ce.readyWait||H.resolveWith(m,[ce])}}),ce.ready.then=H.then,"complete"===m.readyState||"loading"!==m.readyState&&!m.documentElement.doScroll?ie.setTimeout(ce.ready):(m.addEventListener("DOMContentLoaded",q),ie.addEventListener("load",q));var R=function(e,t,n,r,i,o,a){var s=0,u=e.length,l=null==n;if("object"===x(n))for(s in i=!0,n)R(e,t,s,n[s],!0,o,a);else if(void 0!==r&&(i=!0,v(r)||(a=!0),l&&(a?(t.call(e,r),t=null):(l=t,t=function(e,t,n){return l.call(ce(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return i?e:l?t.call(e):u?t(e[0],n):o},M=/^-ms-/,I=/-([a-z])/g;function W(e,t){return t.toUpperCase()}function B(e){return e.replace(M,"ms-").replace(I,W)}var F=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};function ${'$'}(){this.expando=ce.expando+${'$'}.uid++}${'$'}.uid=1,${'$'}.prototype={cache:function(e){var t=e[this.expando];return t||(t={},F(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,i=this.cache(e);if("string"==typeof t)i[B(t)]=n;else for(r in t)i[B(r)]=t[r];return i},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][B(t)]},access:function(e,t,n){return void 0===t||t&&"string"==typeof t&&void 0===n?this.get(e,t):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r=e[this.expando];if(void 0!==r){if(void 0!==t){n=(t=Array.isArray(t)?t.map(B):(t=B(t))in r?[t]:t.match(N)||[]).length;while(n--)delete r[t[n]]}(void 0===t||ce.isEmptyObject(r))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!ce.isEmptyObject(t)}};var _=new ${'$'},z=new ${'$'},U=/^(?:\{[\w\W]*\}|\[[\w\W]*\])${'$'}/,X=/[A-Z]/g;function V(e,t,n){var r,i;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(X,"-${'$'}&").toLowerCase(),"string"==typeof(n=e.getAttribute(r))){try{n="true"===(i=n)||"false"!==i&&("null"===i?null:i===+i+""?+i:U.test(i)?JSON.parse(i):i)}catch(e){}z.set(e,t,n)}else n=void 0;return n}ce.extend({hasData:function(e){return z.hasData(e)||_.hasData(e)},data:function(e,t,n){return z.access(e,t,n)},removeData:function(e,t){z.remove(e,t)},_data:function(e,t,n){return _.access(e,t,n)},_removeData:function(e,t){_.remove(e,t)}}),ce.fn.extend({data:function(n,e){var t,r,i,o=this[0],a=o&&o.attributes;if(void 0===n){if(this.length&&(i=z.get(o),1===o.nodeType&&!_.get(o,"hasDataAttrs"))){t=a.length;while(t--)a[t]&&0===(r=a[t].name).indexOf("data-")&&(r=B(r.slice(5)),V(o,r,i[r]));_.set(o,"hasDataAttrs",!0)}return i}return"object"==typeof n?this.each(function(){z.set(this,n)}):R(this,function(e){var t;if(o&&void 0===e)return void 0!==(t=z.get(o,n))?t:void 0!==(t=V(o,n))?t:void 0;this.each(function(){z.set(this,n,e)})},null,e,1<arguments.length,null,!0)},removeData:function(e){return this.each(function(){z.remove(this,e)})}}),ce.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=_.get(e,t),n&&(!r||Array.isArray(n)?r=_.access(e,t,ce.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=ce.queue(e,t),r=n.length,i=n.shift(),o=ce._queueHooks(e,t);"inprogress"===i&&(i=n.shift(),r--),i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,function(){ce.dequeue(e,t)},o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return _.get(e,n)||_.access(e,n,{empty:ce.Callbacks("once memory").add(function(){_.remove(e,[t+"queue",n])})})}}),ce.fn.extend({queue:function(t,n){var e=2;return"string"!=typeof t&&(n=t,t="fx",e--),arguments.length<e?ce.queue(this[0],t):void 0===n?this:this.each(function(){var e=ce.queue(this,t,n);ce._queueHooks(this,t),"fx"===t&&"inprogress"!==e[0]&&ce.dequeue(this,t)})},dequeue:function(e){return this.each(function(){ce.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,i=ce.Deferred(),o=this,a=this.length,s=function(){--r||i.resolveWith(o,[o])};"string"!=typeof e&&(t=e,e=void 0),e=e||"fx";while(a--)(n=_.get(o[a],e+"queueHooks"))&&n.empty&&(r++,n.empty.add(s));return s(),i.promise(t)}});var Q=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,Y=new RegExp("^(?:([+-])=|)("+Q+")([a-z%]*)${'$'}","i"),G=["Top","Right","Bottom","Left"],K=m.documentElement,J=function(e){return ce.contains(e.ownerDocument,e)},Z={composed:!0};K.getRootNode&&(J=function(e){return ce.contains(e.ownerDocument,e)||e.getRootNode(Z)===e.ownerDocument});var ee=function(e,t){return"none"===(e=t||e).style.display||""===e.style.display&&J(e)&&"none"===ce.css(e,"display")};var te={};function ne(e,t){for(var n,r,i,o,a,s,u,l=[],c=0,f=e.length;c<f;c++)(r=e[c]).style&&(n=r.style.display,t?("none"===n&&(l[c]=_.get(r,"display")||null,l[c]||(r.style.display="")),""===r.style.display&&ee(r)&&(l[c]=(u=a=o=void 0,a=(i=r).ownerDocument,s=i.nodeName,(u=te[s])||(o=a.body.appendChild(a.createElement(s)),u=ce.css(o,"display"),o.parentNode.removeChild(o),"none"===u&&(u="block"),te[s]=u)))):"none"!==n&&(l[c]="none",_.set(r,"display",n)));for(c=0;c<f;c++)null!=l[c]&&(e[c].style.display=l[c]);return e}ce.fn.extend({show:function(){return ne(this,!0)},hide:function(){return ne(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){ee(this)?ce(this).show():ce(this).hide()})}});var re,be,xe=/^(?:checkbox|radio)${'$'}/i,we=/<([a-z][^\/\0>\x20\t\r\n\f]*)/i,Ce=/^${'$'}|^module${'$'}|\/(?:java|ecma)script/i;re=m.createDocumentFragment().appendChild(m.createElement("div")),(be=m.createElement("input")).setAttribute("type","radio"),be.setAttribute("checked","checked"),be.setAttribute("name","t"),re.appendChild(be),le.checkClone=re.cloneNode(!0).cloneNode(!0).lastChild.checked,re.innerHTML="<textarea>x</textarea>",le.noCloneChecked=!!re.cloneNode(!0).lastChild.defaultValue,re.innerHTML="<option></option>",le.option=!!re.lastChild;var Te={thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};function Ee(e,t){var n;return n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[],void 0===t||t&&fe(e,t)?ce.merge([e],n):n}function ke(e,t){for(var n=0,r=e.length;n<r;n++)_.set(e[n],"globalEval",!t||_.get(t[n],"globalEval"))}Te.tbody=Te.tfoot=Te.colgroup=Te.caption=Te.thead,Te.th=Te.td,le.option||(Te.optgroup=Te.option=[1,"<select multiple='multiple'>","</select>"]);var Se=/<|&#?\w+;/;function Ae(e,t,n,r,i){for(var o,a,s,u,l,c,f=t.createDocumentFragment(),d=[],p=0,h=e.length;p<h;p++)if((o=e[p])||0===o)if("object"===x(o))ce.merge(d,o.nodeType?[o]:o);else if(Se.test(o)){a=a||f.appendChild(t.createElement("div")),s=(we.exec(o)||["",""])[1].toLowerCase(),u=Te[s]||Te._default,a.innerHTML=u[1]+ce.htmlPrefilter(o)+u[2],c=u[0];while(c--)a=a.lastChild;ce.merge(d,a.childNodes),(a=f.firstChild).textContent=""}else d.push(t.createTextNode(o));f.textContent="",p=0;while(o=d[p++])if(r&&-1<ce.inArray(o,r))i&&i.push(o);else if(l=J(o),a=Ee(f.appendChild(o),"script"),l&&ke(a),n){c=0;while(o=a[c++])Ce.test(o.type||"")&&n.push(o)}return f}var De=/^([^.]*)(?:\.(.+)|)/;function Ne(){return!0}function Le(){return!1}function je(e,t,n,r,i,o){var a,s;if("object"==typeof t){for(s in"string"!=typeof n&&(r=r||n,n=void 0),t)je(e,s,n,r,t[s],o);return e}if(null==r&&null==i?(i=n,r=n=void 0):null==i&&("string"==typeof n?(i=r,r=void 0):(i=r,r=n,n=void 0)),!1===i)i=Le;else if(!i)return e;return 1===o&&(a=i,(i=function(e){return ce().off(e),a.apply(this,arguments)}).guid=a.guid||(a.guid=ce.guid++)),e.each(function(){ce.event.add(this,t,i,r,n)})}function Oe(e,r,t){t?(_.set(e,r,!1),ce.event.add(e,r,{namespace:!1,handler:function(e){var t,n=_.get(this,r);if(1&e.isTrigger&&this[r]){if(n)(ce.event.special[r]||{}).delegateType&&e.stopPropagation();else if(n=ae.call(arguments),_.set(this,r,n),this[r](),t=_.get(this,r),_.set(this,r,!1),n!==t)return e.stopImmediatePropagation(),e.preventDefault(),t}else n&&(_.set(this,r,ce.event.trigger(n[0],n.slice(1),this)),e.stopPropagation(),e.isImmediatePropagationStopped=Ne)}})):void 0===_.get(e,r)&&ce.event.add(e,r,Ne)}ce.event={global:{},add:function(t,e,n,r,i){var o,a,s,u,l,c,f,d,p,h,g,v=_.get(t);if(F(t)){n.handler&&(n=(o=n).handler,i=o.selector),i&&ce.find.matchesSelector(K,i),n.guid||(n.guid=ce.guid++),(u=v.events)||(u=v.events=Object.create(null)),(a=v.handle)||(a=v.handle=function(e){return"undefined"!=typeof ce&&ce.event.triggered!==e.type?ce.event.dispatch.apply(t,arguments):void 0}),l=(e=(e||"").match(N)||[""]).length;while(l--)p=g=(s=De.exec(e[l])||[])[1],h=(s[2]||"").split(".").sort(),p&&(f=ce.event.special[p]||{},p=(i?f.delegateType:f.bindType)||p,f=ce.event.special[p]||{},c=ce.extend({type:p,origType:g,data:r,handler:n,guid:n.guid,selector:i,needsContext:i&&ce.expr.match.needsContext.test(i),namespace:h.join(".")},o),(d=u[p])||((d=u[p]=[]).delegateCount=0,f.setup&&!1!==f.setup.call(t,r,h,a)||t.addEventListener&&t.addEventListener(p,a)),f.add&&(f.add.call(t,c),c.handler.guid||(c.handler.guid=n.guid)),i?d.splice(d.delegateCount++,0,c):d.push(c),ce.event.global[p]=!0)}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,f,d,p,h,g,v=_.hasData(e)&&_.get(e);if(v&&(u=v.events)){l=(t=(t||"").match(N)||[""]).length;while(l--)if(p=g=(s=De.exec(t[l])||[])[1],h=(s[2]||"").split(".").sort(),p){f=ce.event.special[p]||{},d=u[p=(r?f.delegateType:f.bindType)||p]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|${'$'})"),a=o=d.length;while(o--)c=d[o],!i&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||r&&r!==c.selector&&("**"!==r||!c.selector)||(d.splice(o,1),c.selector&&d.delegateCount--,f.remove&&f.remove.call(e,c));a&&!d.length&&(f.teardown&&!1!==f.teardown.call(e,h,v.handle)||ce.removeEvent(e,p,v.handle),delete u[p])}else for(p in u)ce.event.remove(e,p+t[l],n,r,!0);ce.isEmptyObject(u)&&_.remove(e,"handle events")}},dispatch:function(e){var t,n,r,i,o,a,s=new Array(arguments.length),u=ce.event.fix(e),l=(_.get(this,"events")||Object.create(null))[u.type]||[],c=ce.event.special[u.type]||{};for(s[0]=u,t=1;t<arguments.length;t++)s[t]=arguments[t];if(u.delegateTarget=this,!c.preDispatch||!1!==c.preDispatch.call(this,u)){a=ce.event.handlers.call(this,u,l),t=0;while((i=a[t++])&&!u.isPropagationStopped()){u.currentTarget=i.elem,n=0;while((o=i.handlers[n++])&&!u.isImmediatePropagationStopped())u.rnamespace&&!1!==o.namespace&&!u.rnamespace.test(o.namespace)||(u.handleObj=o,u.data=o.data,void 0!==(r=((ce.event.special[o.origType]||{}).handle||o.handler).apply(i.elem,s))&&!1===(u.result=r)&&(u.preventDefault(),u.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,u),u.result}},handlers:function(e,t){var n,r,i,o,a,s=[],u=t.delegateCount,l=e.target;if(u&&l.nodeType&&!("click"===e.type&&1<=e.button))for(;l!==this;l=l.parentNode||this)if(1===l.nodeType&&("click"!==e.type||!0!==l.disabled)){for(o=[],a={},n=0;n<u;n++)void 0===a[i=(r=t[n]).selector+" "]&&(a[i]=r.needsContext?-1<ce(i,this).index(l):ce.find(i,this,null,[l]).length),a[i]&&o.push(r);o.length&&s.push({elem:l,handlers:o})}return l=this,u<t.length&&s.push({elem:l,handlers:t.slice(u)}),s},addProp:function(t,e){Object.defineProperty(ce.Event.prototype,t,{enumerable:!0,configurable:!0,get:v(e)?function(){if(this.originalEvent)return e(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[t]},set:function(e){Object.defineProperty(this,t,{enumerable:!0,configurable:!0,writable:!0,value:e})}})},fix:function(e){return e[ce.expando]?e:new ce.Event(e)},special:{load:{noBubble:!0},click:{setup:function(e){var t=this||e;return xe.test(t.type)&&t.click&&fe(t,"input")&&Oe(t,"click",!0),!1},trigger:function(e){var t=this||e;return xe.test(t.type)&&t.click&&fe(t,"input")&&Oe(t,"click"),!0},_default:function(e){var t=e.target;return xe.test(t.type)&&t.click&&fe(t,"input")&&_.get(t,"click")||fe(t,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},ce.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},ce.Event=function(e,t){if(!(this instanceof ce.Event))return new ce.Event(e,t);e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&!1===e.returnValue?Ne:Le,this.target=e.target&&3===e.target.nodeType?e.target.parentNode:e.target,this.currentTarget=e.currentTarget,this.relatedTarget=e.relatedTarget):this.type=e,t&&ce.extend(this,t),this.timeStamp=e&&e.timeStamp||Date.now(),this[ce.expando]=!0},ce.Event.prototype={constructor:ce.Event,isDefaultPrevented:Le,isPropagationStopped:Le,isImmediatePropagationStopped:Le,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=Ne,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=Ne,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=Ne,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},ce.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,code:!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:!0},ce.event.addProp),ce.each({focus:"focusin",blur:"focusout"},function(r,i){function o(e){if(m.documentMode){var t=_.get(this,"handle"),n=ce.event.fix(e);n.type="focusin"===e.type?"focus":"blur",n.isSimulated=!0,t(e),n.target===n.currentTarget&&t(n)}else ce.event.simulate(i,e.target,ce.event.fix(e))}ce.event.special[r]={setup:function(){var e;if(Oe(this,r,!0),!m.documentMode)return!1;(e=_.get(this,i))||this.addEventListener(i,o),_.set(this,i,(e||0)+1)},trigger:function(){return Oe(this,r),!0},teardown:function(){var e;if(!m.documentMode)return!1;(e=_.get(this,i)-1)?_.set(this,i,e):(this.removeEventListener(i,o),_.remove(this,i))},_default:function(e){return _.get(e.target,r)},delegateType:i},ce.event.special[i]={setup:function(){var e=this.ownerDocument||this.document||this,t=m.documentMode?this:e,n=_.get(t,i);n||(m.documentMode?this.addEventListener(i,o):e.addEventListener(r,o,!0)),_.set(t,i,(n||0)+1)},teardown:function(){var e=this.ownerDocument||this.document||this,t=m.documentMode?this:e,n=_.get(t,i)-1;n?_.set(t,i,n):(m.documentMode?this.removeEventListener(i,o):e.removeEventListener(r,o,!0),_.remove(t,i))}}}),ce.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,i){ce.event.special[e]={delegateType:i,bindType:i,handle:function(e){var t,n=e.relatedTarget,r=e.handleObj;return n&&(n===this||ce.contains(this,n))||(e.type=r.origType,t=r.handler.apply(this,arguments),e.type=i),t}}}),ce.fn.extend({on:function(e,t,n,r){return je(this,e,t,n,r)},one:function(e,t,n,r){return je(this,e,t,n,r,1)},off:function(e,t,n){var r,i;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,ce(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(i in e)this.off(i,t,e[i]);return this}return!1!==t&&"function"!=typeof t||(n=t,t=void 0),!1===n&&(n=Le),this.each(function(){ce.event.remove(this,e,n,t)})}});var Pe=/<script|<style|<link/i,He=/checked\s*(?:[^=]|=\s*.checked.)/i,qe=/^\s*<!\[CDATA\[|\]\]>\s*${'$'}/g;function Re(e,t){return fe(e,"table")&&fe(11!==t.nodeType?t:t.firstChild,"tr")&&ce(e).children("tbody")[0]||e}function Me(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function Ie(e){return"true/"===(e.type||"").slice(0,5)?e.type=e.type.slice(5):e.removeAttribute("type"),e}function We(e,t){var n,r,i,o,a,s;if(1===t.nodeType){if(_.hasData(e)&&(s=_.get(e).events))for(i in _.remove(t,"handle events"),s)for(n=0,r=s[i].length;n<r;n++)ce.event.add(t,i,s[i][n]);z.hasData(e)&&(o=z.access(e),a=ce.extend({},o),z.set(t,a))}}function Be(n,r,i,o){r=g(r);var e,t,a,s,u,l,c=0,f=n.length,d=f-1,p=r[0],h=v(p);if(h||1<f&&"string"==typeof p&&!le.checkClone&&He.test(p))return n.each(function(e){var t=n.eq(e);h&&(r[0]=p.call(this,e,t.html())),Be(t,r,i,o)});if(f&&(t=(e=Ae(r,n[0].ownerDocument,!1,n,o)).firstChild,1===e.childNodes.length&&(e=t),t||o)){for(s=(a=ce.map(Ee(e,"script"),Me)).length;c<f;c++)u=e,c!==d&&(u=ce.clone(u,!0,!0),s&&ce.merge(a,Ee(u,"script"))),i.call(n[c],u,c);if(s)for(l=a[a.length-1].ownerDocument,ce.map(a,Ie),c=0;c<s;c++)u=a[c],Ce.test(u.type||"")&&!_.access(u,"globalEval")&&ce.contains(l,u)&&(u.src&&"module"!==(u.type||"").toLowerCase()?ce._evalUrl&&!u.noModule&&ce._evalUrl(u.src,{nonce:u.nonce||u.getAttribute("nonce")},l):b(u.textContent.replace(qe,""),u,l))}return n}function Fe(e,t,n){for(var r,i=t?ce.filter(t,e):e,o=0;null!=(r=i[o]);o++)n||1!==r.nodeType||ce.cleanData(Ee(r)),r.parentNode&&(n&&J(r)&&ke(Ee(r,"script")),r.parentNode.removeChild(r));return e}ce.extend({htmlPrefilter:function(e){return e},clone:function(e,t,n){var r,i,o,a,s,u,l,c=e.cloneNode(!0),f=J(e);if(!(le.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||ce.isXMLDoc(e)))for(a=Ee(c),r=0,i=(o=Ee(e)).length;r<i;r++)s=o[r],u=a[r],void 0,"input"===(l=u.nodeName.toLowerCase())&&xe.test(s.type)?u.checked=s.checked:"input"!==l&&"textarea"!==l||(u.defaultValue=s.defaultValue);if(t)if(n)for(o=o||Ee(e),a=a||Ee(c),r=0,i=o.length;r<i;r++)We(o[r],a[r]);else We(e,c);return 0<(a=Ee(c,"script")).length&&ke(a,!f&&Ee(e,"script")),c},cleanData:function(e){for(var t,n,r,i=ce.event.special,o=0;void 0!==(n=e[o]);o++)if(F(n)){if(t=n[_.expando]){if(t.events)for(r in t.events)i[r]?ce.event.remove(n,r):ce.removeEvent(n,r,t.handle);n[_.expando]=void 0}n[z.expando]&&(n[z.expando]=void 0)}}}),ce.fn.extend({detach:function(e){return Fe(this,e,!0)},remove:function(e){return Fe(this,e)},text:function(e){return R(this,function(e){return void 0===e?ce.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return Be(this,arguments,function(e){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||Re(this,e).appendChild(e)})},prepend:function(){return Be(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=Re(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return Be(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return Be(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(ce.cleanData(Ee(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return ce.clone(this,e,t)})},html:function(e){return R(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!Pe.test(e)&&!Te[(we.exec(e)||["",""])[1].toLowerCase()]){e=ce.htmlPrefilter(e);try{for(;n<r;n++)1===(t=this[n]||{}).nodeType&&(ce.cleanData(Ee(t,!1)),t.innerHTML=e);t=0}catch(e){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var n=[];return Be(this,arguments,function(e){var t=this.parentNode;ce.inArray(this,n)<0&&(ce.cleanData(Ee(this)),t&&t.replaceChild(e,this))},n)}}),ce.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,a){ce.fn[e]=function(e){for(var t,n=[],r=ce(e),i=r.length-1,o=0;o<=i;o++)t=o===i?this:this.clone(!0),ce(r[o])[a](t),s.apply(n,t.get());return this.pushStack(n)}});var ${'$'}e=new RegExp("^("+Q+")(?!px)[a-z%]+${'$'}","i"),_e=/^--/,ze=function(e){var t=e.ownerDocument.defaultView;return t&&t.opener||(t=ie),t.getComputedStyle(e)},Ue=function(e,t,n){var r,i,o={};for(i in t)o[i]=e.style[i],e.style[i]=t[i];for(i in r=n.call(e),t)e.style[i]=o[i];return r},Xe=new RegExp(G.join("|"),"i");function Ve(e,t,n){var r,i,o,a,s=_e.test(t),u=e.style;return(n=n||ze(e))&&(a=n.getPropertyValue(t)||n[t],s&&a&&(a=a.replace(ve,"${'$'}1")||void 0),""!==a||J(e)||(a=ce.style(e,t)),!le.pixelBoxStyles()&&${'$'}e.test(a)&&Xe.test(t)&&(r=u.width,i=u.minWidth,o=u.maxWidth,u.minWidth=u.maxWidth=u.width=a,a=n.width,u.width=r,u.minWidth=i,u.maxWidth=o)),void 0!==a?a+"":a}function Qe(e,t){return{get:function(){if(!e())return(this.get=t).apply(this,arguments);delete this.get}}}!function(){function e(){if(l){u.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",l.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",K.appendChild(u).appendChild(l);var e=ie.getComputedStyle(l);n="1%"!==e.top,s=12===t(e.marginLeft),l.style.right="60%",o=36===t(e.right),r=36===t(e.width),l.style.position="absolute",i=12===t(l.offsetWidth/3),K.removeChild(u),l=null}}function t(e){return Math.round(parseFloat(e))}var n,r,i,o,a,s,u=m.createElement("div"),l=m.createElement("div");l.style&&(l.style.backgroundClip="content-box",l.cloneNode(!0).style.backgroundClip="",le.clearCloneStyle="content-box"===l.style.backgroundClip,ce.extend(le,{boxSizingReliable:function(){return e(),r},pixelBoxStyles:function(){return e(),o},pixelPosition:function(){return e(),n},reliableMarginLeft:function(){return e(),s},scrollboxSize:function(){return e(),i},reliableTrDimensions:function(){var e,t,n,r;return null==a&&(e=m.createElement("table"),t=m.createElement("tr"),n=m.createElement("div"),e.style.cssText="position:absolute;left:-11111px;border-collapse:separate",t.style.cssText="box-sizing:content-box;border:1px solid",t.style.height="1px",n.style.height="9px",n.style.display="block",K.appendChild(e).appendChild(t).appendChild(n),r=ie.getComputedStyle(t),a=parseInt(r.height,10)+parseInt(r.borderTopWidth,10)+parseInt(r.borderBottomWidth,10)===t.offsetHeight,K.removeChild(e)),a}}))}();var Ye=["Webkit","Moz","ms"],Ge=m.createElement("div").style,Ke={};function Je(e){var t=ce.cssProps[e]||Ke[e];return t||(e in Ge?e:Ke[e]=function(e){var t=e[0].toUpperCase()+e.slice(1),n=Ye.length;while(n--)if((e=Ye[n]+t)in Ge)return e}(e)||e)}var Ze,et,tt=/^(none|table(?!-c[ea]).+)/,nt={position:"absolute",visibility:"hidden",display:"block"},rt={letterSpacing:"0",fontWeight:"400"};function it(e,t,n){var r=Y.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function ot(e,t,n,r,i,o){var a="width"===t?1:0,s=0,u=0,l=0;if(n===(r?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(l+=ce.css(e,n+G[a],!0,i)),r?("content"===n&&(u-=ce.css(e,"padding"+G[a],!0,i)),"margin"!==n&&(u-=ce.css(e,"border"+G[a]+"Width",!0,i))):(u+=ce.css(e,"padding"+G[a],!0,i),"padding"!==n?u+=ce.css(e,"border"+G[a]+"Width",!0,i):s+=ce.css(e,"border"+G[a]+"Width",!0,i));return!r&&0<=o&&(u+=Math.max(0,Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-o-u-s-.5))||0),u+l}function at(e,t,n){var r=ze(e),i=(!le.boxSizingReliable()||n)&&"border-box"===ce.css(e,"boxSizing",!1,r),o=i,a=Ve(e,t,r),s="offset"+t[0].toUpperCase()+t.slice(1);if(${'$'}e.test(a)){if(!n)return a;a="auto"}return(!le.boxSizingReliable()&&i||!le.reliableTrDimensions()&&fe(e,"tr")||"auto"===a||!parseFloat(a)&&"inline"===ce.css(e,"display",!1,r))&&e.getClientRects().length&&(i="border-box"===ce.css(e,"boxSizing",!1,r),(o=s in e)&&(a=e[s])),(a=parseFloat(a)||0)+ot(e,t,n||(i?"border":"content"),o,r,a)+"px"}ce.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=Ve(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,aspectRatio:!0,borderImageSlice:!0,columnCount:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,gridArea:!0,gridColumn:!0,gridColumnEnd:!0,gridColumnStart:!0,gridRow:!0,gridRowEnd:!0,gridRowStart:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,scale:!0,widows:!0,zIndex:!0,zoom:!0,fillOpacity:!0,floodOpacity:!0,stopOpacity:!0,strokeMiterlimit:!0,strokeOpacity:!0},cssProps:{},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var i,o,a,s=B(t),u=_e.test(t),l=e.style;if(u||(t=Je(s)),a=ce.cssHooks[t]||ce.cssHooks[s],void 0===n)return a&&"get"in a&&void 0!==(i=a.get(e,!1,r))?i:l[t];"string"===(o=typeof n)&&(i=Y.exec(n))&&i[1]&&(n=function(e,t,n,r){var i,o,a=20,s=r?function(){return r.cur()}:function(){return ce.css(e,t,"")},u=s(),l=n&&n[3]||(ce.cssNumber[t]?"":"px"),c=e.nodeType&&(ce.cssNumber[t]||"px"!==l&&+u)&&Y.exec(ce.css(e,t));if(c&&c[3]!==l){u/=2,l=l||c[3],c=+u||1;while(a--)ce.style(e,t,c+l),(1-o)*(1-(o=s()/u||.5))<=0&&(a=0),c/=o;c*=2,ce.style(e,t,c+l),n=n||[]}return n&&(c=+c||+u||0,i=n[1]?c+(n[1]+1)*n[2]:+n[2],r&&(r.unit=l,r.start=c,r.end=i)),i}(e,t,i),o="number"),null!=n&&n==n&&("number"!==o||u||(n+=i&&i[3]||(ce.cssNumber[s]?"":"px")),le.clearCloneStyle||""!==n||0!==t.indexOf("background")||(l[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u?l.setProperty(t,n):l[t]=n))}},css:function(e,t,n,r){var i,o,a,s=B(t);return _e.test(t)||(t=Je(s)),(a=ce.cssHooks[t]||ce.cssHooks[s])&&"get"in a&&(i=a.get(e,!0,n)),void 0===i&&(i=Ve(e,t,r)),"normal"===i&&t in rt&&(i=rt[t]),""===n||n?(o=parseFloat(i),!0===n||isFinite(o)?o||0:i):i}}),ce.each(["height","width"],function(e,u){ce.cssHooks[u]={get:function(e,t,n){if(t)return!tt.test(ce.css(e,"display"))||e.getClientRects().length&&e.getBoundingClientRect().width?at(e,u,n):Ue(e,nt,function(){return at(e,u,n)})},set:function(e,t,n){var r,i=ze(e),o=!le.scrollboxSize()&&"absolute"===i.position,a=(o||n)&&"border-box"===ce.css(e,"boxSizing",!1,i),s=n?ot(e,u,n,a,i):0;return a&&o&&(s-=Math.ceil(e["offset"+u[0].toUpperCase()+u.slice(1)]-parseFloat(i[u])-ot(e,u,"border",!1,i)-.5)),s&&(r=Y.exec(t))&&"px"!==(r[3]||"px")&&(e.style[u]=t,t=ce.css(e,u)),it(0,t,s)}}}),ce.cssHooks.marginLeft=Qe(le.reliableMarginLeft,function(e,t){if(t)return(parseFloat(Ve(e,"marginLeft"))||e.getBoundingClientRect().left-Ue(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),ce.each({margin:"",padding:"",border:"Width"},function(i,o){ce.cssHooks[i+o]={expand:function(e){for(var t=0,n={},r="string"==typeof e?e.split(" "):[e];t<4;t++)n[i+G[t]+o]=r[t]||r[t-2]||r[0];return n}},"margin"!==i&&(ce.cssHooks[i+o].set=it)}),ce.fn.extend({css:function(e,t){return R(this,function(e,t,n){var r,i,o={},a=0;if(Array.isArray(t)){for(r=ze(e),i=t.length;a<i;a++)o[t[a]]=ce.css(e,t[a],!1,r);return o}return void 0!==n?ce.style(e,t,n):ce.css(e,t)},e,t,1<arguments.length)}}),ce.fn.delay=function(r,e){return r=ce.fx&&ce.fx.speeds[r]||r,e=e||"fx",this.queue(e,function(e,t){var n=ie.setTimeout(e,r);t.stop=function(){ie.clearTimeout(n)}})},Ze=m.createElement("input"),et=m.createElement("select").appendChild(m.createElement("option")),Ze.type="checkbox",le.checkOn=""!==Ze.value,le.optSelected=et.selected,(Ze=m.createElement("input")).value="t",Ze.type="radio",le.radioValue="t"===Ze.value;var st,ut=ce.expr.attrHandle;ce.fn.extend({attr:function(e,t){return R(this,ce.attr,e,t,1<arguments.length)},removeAttr:function(e){return this.each(function(){ce.removeAttr(this,e)})}}),ce.extend({attr:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof e.getAttribute?ce.prop(e,t,n):(1===o&&ce.isXMLDoc(e)||(i=ce.attrHooks[t.toLowerCase()]||(ce.expr.match.bool.test(t)?st:void 0)),void 0!==n?null===n?void ce.removeAttr(e,t):i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:(e.setAttribute(t,n+""),n):i&&"get"in i&&null!==(r=i.get(e,t))?r:null==(r=ce.find.attr(e,t))?void 0:r)},attrHooks:{type:{set:function(e,t){if(!le.radioValue&&"radio"===t&&fe(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r=0,i=t&&t.match(N);if(i&&1===e.nodeType)while(n=i[r++])e.removeAttribute(n)}}),st={set:function(e,t,n){return!1===t?ce.removeAttr(e,n):e.setAttribute(n,n),n}},ce.each(ce.expr.match.bool.source.match(/\w+/g),function(e,t){var a=ut[t]||ce.find.attr;ut[t]=function(e,t,n){var r,i,o=t.toLowerCase();return n||(i=ut[o],ut[o]=r,r=null!=a(e,t,n)?o:null,ut[o]=i),r}});var lt=/^(?:input|select|textarea|button)${'$'}/i,ct=/^(?:a|area)${'$'}/i;function ft(e){return(e.match(N)||[]).join(" ")}function dt(e){return e.getAttribute&&e.getAttribute("class")||""}function pt(e){return Array.isArray(e)?e:"string"==typeof e&&e.match(N)||[]}ce.fn.extend({prop:function(e,t){return R(this,ce.prop,e,t,1<arguments.length)},removeProp:function(e){return this.each(function(){delete this[ce.propFix[e]||e]})}}),ce.extend({prop:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&ce.isXMLDoc(e)||(t=ce.propFix[t]||t,i=ce.propHooks[t]),void 0!==n?i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:e[t]=n:i&&"get"in i&&null!==(r=i.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=ce.find.attr(e,"tabindex");return t?parseInt(t,10):lt.test(e.nodeName)||ct.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),le.optSelected||(ce.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),ce.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){ce.propFix[this.toLowerCase()]=this}),ce.fn.extend({addClass:function(t){var e,n,r,i,o,a;return v(t)?this.each(function(e){ce(this).addClass(t.call(this,e,dt(this)))}):(e=pt(t)).length?this.each(function(){if(r=dt(this),n=1===this.nodeType&&" "+ft(r)+" "){for(o=0;o<e.length;o++)i=e[o],n.indexOf(" "+i+" ")<0&&(n+=i+" ");a=ft(n),r!==a&&this.setAttribute("class",a)}}):this},removeClass:function(t){var e,n,r,i,o,a;return v(t)?this.each(function(e){ce(this).removeClass(t.call(this,e,dt(this)))}):arguments.length?(e=pt(t)).length?this.each(function(){if(r=dt(this),n=1===this.nodeType&&" "+ft(r)+" "){for(o=0;o<e.length;o++){i=e[o];while(-1<n.indexOf(" "+i+" "))n=n.replace(" "+i+" "," ")}a=ft(n),r!==a&&this.setAttribute("class",a)}}):this:this.attr("class","")},toggleClass:function(t,n){var e,r,i,o,a=typeof t,s="string"===a||Array.isArray(t);return v(t)?this.each(function(e){ce(this).toggleClass(t.call(this,e,dt(this),n),n)}):"boolean"==typeof n&&s?n?this.addClass(t):this.removeClass(t):(e=pt(t),this.each(function(){if(s)for(o=ce(this),i=0;i<e.length;i++)r=e[i],o.hasClass(r)?o.removeClass(r):o.addClass(r);else void 0!==t&&"boolean"!==a||((r=dt(this))&&_.set(this,"__className__",r),this.setAttribute&&this.setAttribute("class",r||!1===t?"":_.get(this,"__className__")||""))}))},hasClass:function(e){var t,n,r=0;t=" "+e+" ";while(n=this[r++])if(1===n.nodeType&&-1<(" "+ft(dt(n))+" ").indexOf(t))return!0;return!1}});var ht=/\r/g;ce.fn.extend({val:function(n){var r,e,i,t=this[0];return arguments.length?(i=v(n),this.each(function(e){var t;1===this.nodeType&&(null==(t=i?n.call(this,e,ce(this).val()):n)?t="":"number"==typeof t?t+="":Array.isArray(t)&&(t=ce.map(t,function(e){return null==e?"":e+""})),(r=ce.valHooks[this.type]||ce.valHooks[this.nodeName.toLowerCase()])&&"set"in r&&void 0!==r.set(this,t,"value")||(this.value=t))})):t?(r=ce.valHooks[t.type]||ce.valHooks[t.nodeName.toLowerCase()])&&"get"in r&&void 0!==(e=r.get(t,"value"))?e:"string"==typeof(e=t.value)?e.replace(ht,""):null==e?"":e:void 0}}),ce.extend({valHooks:{option:{get:function(e){var t=ce.find.attr(e,"value");return null!=t?t:ft(ce.text(e))}},select:{get:function(e){var t,n,r,i=e.options,o=e.selectedIndex,a="select-one"===e.type,s=a?null:[],u=a?o+1:i.length;for(r=o<0?u:a?o:0;r<u;r++)if(((n=i[r]).selected||r===o)&&!n.disabled&&(!n.parentNode.disabled||!fe(n.parentNode,"optgroup"))){if(t=ce(n).val(),a)return t;s.push(t)}return s},set:function(e,t){var n,r,i=e.options,o=ce.makeArray(t),a=i.length;while(a--)((r=i[a]).selected=-1<ce.inArray(ce.valHooks.option.get(r),o))&&(n=!0);return n||(e.selectedIndex=-1),o}}}}),ce.each(["radio","checkbox"],function(){ce.valHooks[this]={set:function(e,t){if(Array.isArray(t))return e.checked=-1<ce.inArray(ce(e).val(),t)}},le.checkOn||(ce.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})}),ce.parseXML=function(e){var t,n;if(!e||"string"!=typeof e)return null;try{t=(new ie.DOMParser).parseFromString(e,"text/xml")}catch(e){}return n=t&&t.getElementsByTagName("parsererror")[0],t&&!n||ce.error("Invalid XML: "+(n?ce.map(n.childNodes,function(e){return e.textContent}).join("\n"):e)),t};var gt=/^(?:focusinfocus|focusoutblur)${'$'}/,vt=function(e){e.stopPropagation()};ce.extend(ce.event,{trigger:function(e,t,n,r){var i,o,a,s,u,l,c,f,d=[n||m],p=ue.call(e,"type")?e.type:e,h=ue.call(e,"namespace")?e.namespace.split("."):[];if(o=f=a=n=n||m,3!==n.nodeType&&8!==n.nodeType&&!gt.test(p+ce.event.triggered)&&(-1<p.indexOf(".")&&(p=(h=p.split(".")).shift(),h.sort()),u=p.indexOf(":")<0&&"on"+p,(e=e[ce.expando]?e:new ce.Event(p,"object"==typeof e&&e)).isTrigger=r?2:3,e.namespace=h.join("."),e.rnamespace=e.namespace?new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|${'$'})"):null,e.result=void 0,e.target||(e.target=n),t=null==t?[e]:ce.makeArray(t,[e]),c=ce.event.special[p]||{},r||!c.trigger||!1!==c.trigger.apply(n,t))){if(!r&&!c.noBubble&&!y(n)){for(s=c.delegateType||p,gt.test(s+p)||(o=o.parentNode);o;o=o.parentNode)d.push(o),a=o;a===(n.ownerDocument||m)&&d.push(a.defaultView||a.parentWindow||ie)}i=0;while((o=d[i++])&&!e.isPropagationStopped())f=o,e.type=1<i?s:c.bindType||p,(l=(_.get(o,"events")||Object.create(null))[e.type]&&_.get(o,"handle"))&&l.apply(o,t),(l=u&&o[u])&&l.apply&&F(o)&&(e.result=l.apply(o,t),!1===e.result&&e.preventDefault());return e.type=p,r||e.isDefaultPrevented()||c._default&&!1!==c._default.apply(d.pop(),t)||!F(n)||u&&v(n[p])&&!y(n)&&((a=n[u])&&(n[u]=null),ce.event.triggered=p,e.isPropagationStopped()&&f.addEventListener(p,vt),n[p](),e.isPropagationStopped()&&f.removeEventListener(p,vt),ce.event.triggered=void 0,a&&(n[u]=a)),e.result}},simulate:function(e,t,n){var r=ce.extend(new ce.Event,n,{type:e,isSimulated:!0});ce.event.trigger(r,null,t)}}),ce.fn.extend({trigger:function(e,t){return this.each(function(){ce.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return ce.event.trigger(e,t,n,!0)}});var yt,mt=/\[\]${'$'}/,bt=/\r?\n/g,xt=/^(?:submit|button|image|reset|file)${'$'}/i,wt=/^(?:input|select|textarea|keygen)/i;function Ct(n,e,r,i){var t;if(Array.isArray(e))ce.each(e,function(e,t){r||mt.test(n)?i(n,t):Ct(n+"["+("object"==typeof t&&null!=t?e:"")+"]",t,r,i)});else if(r||"object"!==x(e))i(n,e);else for(t in e)Ct(n+"["+t+"]",e[t],r,i)}ce.param=function(e,t){var n,r=[],i=function(e,t){var n=v(t)?t():t;r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(null==n?"":n)};if(null==e)return"";if(Array.isArray(e)||e.jquery&&!ce.isPlainObject(e))ce.each(e,function(){i(this.name,this.value)});else for(n in e)Ct(n,e[n],t,i);return r.join("&")},ce.fn.extend({serialize:function(){return ce.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=ce.prop(this,"elements");return e?ce.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!ce(this).is(":disabled")&&wt.test(this.nodeName)&&!xt.test(e)&&(this.checked||!xe.test(e))}).map(function(e,t){var n=ce(this).val();return null==n?null:Array.isArray(n)?ce.map(n,function(e){return{name:t.name,value:e.replace(bt,"\r\n")}}):{name:t.name,value:n.replace(bt,"\r\n")}}).get()}}),ce.fn.extend({wrapAll:function(e){var t;return this[0]&&(v(e)&&(e=e.call(this[0])),t=ce(e,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstElementChild)e=e.firstElementChild;return e}).append(this)),this},wrapInner:function(n){return v(n)?this.each(function(e){ce(this).wrapInner(n.call(this,e))}):this.each(function(){var e=ce(this),t=e.contents();t.length?t.wrapAll(n):e.append(n)})},wrap:function(t){var n=v(t);return this.each(function(e){ce(this).wrapAll(n?t.call(this,e):t)})},unwrap:function(e){return this.parent(e).not("body").each(function(){ce(this).replaceWith(this.childNodes)}),this}}),ce.expr.pseudos.hidden=function(e){return!ce.expr.pseudos.visible(e)},ce.expr.pseudos.visible=function(e){return!!(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},le.createHTMLDocument=((yt=m.implementation.createHTMLDocument("").body).innerHTML="<form></form><form></form>",2===yt.childNodes.length),ce.parseHTML=function(e,t,n){return"string"!=typeof e?[]:("boolean"==typeof t&&(n=t,t=!1),t||(le.createHTMLDocument?((r=(t=m.implementation.createHTMLDocument("")).createElement("base")).href=m.location.href,t.head.appendChild(r)):t=m),o=!n&&[],(i=C.exec(e))?[t.createElement(i[1])]:(i=Ae([e],t,o),o&&o.length&&ce(o).remove(),ce.merge([],i.childNodes)));var r,i,o},ce.offset={setOffset:function(e,t,n){var r,i,o,a,s,u,l=ce.css(e,"position"),c=ce(e),f={};"static"===l&&(e.style.position="relative"),s=c.offset(),o=ce.css(e,"top"),u=ce.css(e,"left"),("absolute"===l||"fixed"===l)&&-1<(o+u).indexOf("auto")?(a=(r=c.position()).top,i=r.left):(a=parseFloat(o)||0,i=parseFloat(u)||0),v(t)&&(t=t.call(e,n,ce.extend({},s))),null!=t.top&&(f.top=t.top-s.top+a),null!=t.left&&(f.left=t.left-s.left+i),"using"in t?t.using.call(e,f):c.css(f)}},ce.fn.extend({offset:function(t){if(arguments.length)return void 0===t?this:this.each(function(e){ce.offset.setOffset(this,t,e)});var e,n,r=this[0];return r?r.getClientRects().length?(e=r.getBoundingClientRect(),n=r.ownerDocument.defaultView,{top:e.top+n.pageYOffset,left:e.left+n.pageXOffset}):{top:0,left:0}:void 0},position:function(){if(this[0]){var e,t,n,r=this[0],i={top:0,left:0};if("fixed"===ce.css(r,"position"))t=r.getBoundingClientRect();else{t=this.offset(),n=r.ownerDocument,e=r.offsetParent||n.documentElement;while(e&&(e===n.body||e===n.documentElement)&&"static"===ce.css(e,"position"))e=e.parentNode;e&&e!==r&&1===e.nodeType&&((i=ce(e).offset()).top+=ce.css(e,"borderTopWidth",!0),i.left+=ce.css(e,"borderLeftWidth",!0))}return{top:t.top-i.top-ce.css(r,"marginTop",!0),left:t.left-i.left-ce.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent;while(e&&"static"===ce.css(e,"position"))e=e.offsetParent;return e||K})}}),ce.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(t,i){var o="pageYOffset"===i;ce.fn[t]=function(e){return R(this,function(e,t,n){var r;if(y(e)?r=e:9===e.nodeType&&(r=e.defaultView),void 0===n)return r?r[i]:e[t];r?r.scrollTo(o?r.pageXOffset:n,o?n:r.pageYOffset):e[t]=n},t,e,arguments.length)}}),ce.each(["top","left"],function(e,n){ce.cssHooks[n]=Qe(le.pixelPosition,function(e,t){if(t)return t=Ve(e,n),${'$'}.test(t)?ce(e).position()[n]+"px":t})}),ce.each({Height:"height",Width:"width"},function(a,s){ce.each({padding:"inner"+a,content:s,"":"outer"+a},function(r,o){ce.fn[o]=function(e,t){var n=arguments.length&&(r||"boolean"!=typeof e),i=r||(!0===e||!0===t?"margin":"border");return R(this,function(e,t,n){var r;return y(e)?0===o.indexOf("outer")?e["inner"+a]:e.document.documentElement["client"+a]:9===e.nodeType?(r=e.documentElement,Math.max(e.body["scroll"+a],r["scroll"+a],e.body["offset"+a],r["offset"+a],r["client"+a])):void 0===n?ce.css(e,t,i):ce.style(e,t,n,i)},s,n?e:void 0,n)}})}),ce.fn.extend({bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)},hover:function(e,t){return this.on("mouseenter",e).on("mouseleave",t||e)}}),ce.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(e,n){ce.fn[n]=function(e,t){return 0<arguments.length?this.on(n,null,e,t):this.trigger(n)}});var Tt=/^[\s\uFEFF\xA0]+|([^\s\uFEFF\xA0])[\s\uFEFF\xA0]+${'$'}/g;ce.proxy=function(e,t){var n,r,i;if("string"==typeof t&&(n=e[t],t=e,e=n),v(e))return r=ae.call(arguments,2),(i=function(){return e.apply(t||this,r.concat(ae.call(arguments)))}).guid=e.guid=e.guid||ce.guid++,i},ce.holdReady=function(e){e?ce.readyWait++:ce.ready(!0)},ce.isArray=Array.isArray,ce.parseJSON=JSON.parse,ce.nodeName=fe,ce.isFunction=v,ce.isWindow=y,ce.camelCase=B,ce.type=x,ce.now=Date.now,ce.isNumeric=function(e){var t=ce.type(e);return("number"===t||"string"===t)&&!isNaN(e-parseFloat(e))},ce.trim=function(e){return null==e?"":(e+"").replace(Tt,"${'$'}1")},"function"==typeof define&&define.amd&&define("jquery",[],function(){return ce});var Et=ie.jQuery,kt=ie.${'$'};return ce.noConflict=function(e){return ie.${'$'}===ce&&(ie.${'$'}=kt),e&&ie.jQuery===ce&&(ie.jQuery=Et),ce},"undefined"==typeof e&&(ie.jQuery=ie.${'$'}=ce),ce});
    """.trimIndent()

    //endregion
    //region private val sharedJs = ...
    private val sharedJs = """
        var pipActive = false
        var webRtcPlaying = false
        var airplaySupported = false
        var reconnectTimeoutId = null
        var lastState = null
        var iOSBugTimeoutId = null
        
        function captureLog(level, args) {
            if (window.webkit) {
                window.webkit.messageHandlers.logHandler.postMessage({level: level, message: Array.from(args).join(' ')});
            }
            
            if (hasOctoApp()) {
                octoapp.log(level, Array.from(args).join(' '));
            }
        };
        
        function hasOctoApp() {
            return typeof octoapp !== 'undefined'
        }

        window.console.log = function () { captureLog('i', arguments); }
        window.console.error = function () { captureLog('e', arguments); }
        window.console.warn = function () { captureLog('w', arguments); }
        window.console.debug = function () { captureLog('d', arguments); }
        window.console.info = function () { captureLog('i', arguments); }
        window.console.verbose = function () { captureLog('v', arguments); }
        window.onerror = (msg, url, line, column, error) => {
            console.error(`JS ERROR: $D{url}:$D{line}:${'$'}{column} -> $D{msg}`);
            moveToState({state: 'error', error: msg, errorMsg: `Internal error, failed to load WebRTC.`})
        };
        
        function setupVideoCallbacksFor(video) {
            video.addEventListener('error', (event) => {
                console.error("Error in video", JSON.stringify(event))
                moveToState({state: 'error', error: JSON.stringify(event), errorMsg: `Failed to play ${'$'}{JSON.stringify(event)}`})
                scheduleReconnect()
            });
            video.addEventListener('enterpictureinpicture', () => {
                console.debug("Entering PiP")
                pipActive = true
                moveToState(JSON.parse(JSON.stringify(lastState)))
            })
            video.addEventListener('leavepictureinpicture', () => {
                console.debug("Leaving PiP")
                pipActive = false
                moveToState(JSON.parse(JSON.stringify(lastState)))
            })
            video.addEventListener('pause', (e) => {
                console.debug("Paused")
                
                if (stopWebRTC != undefined && webRtcPlaying) {
                    stopWebRTC()
                }
            })
            video.addEventListener('playing', () => {
                console.log("Playing")
                window.clearTimeout(reconnectTimeoutId)
                window.clearTimeout(iOSBugTimeoutId)
                reconnectTimeoutId = null
                moveToState({state: 'playing', width: video.videoWidth, height: video.videoHeight})
            })
            video.addEventListener('waiting', () => {
                console.debug("Waiting for data")
            })
            video.addEventListener('loadedmetadata', () => {
                console.debug("Metadata loaded")
            })
            video.addEventListener('loadstart', () => {
                console.debug("Loading data")
            })
            video.addEventListener('loadeddata', () => {
                console.debug("Loaded data")
                window.clearTimeout(iOSBugTimeoutId)
                iOSBugTimeoutId = window.setTimeout(() => {
                    // https://developer.apple.com/forums/thread/739686
                    console.error("Playback didn't start 10s after data was available -> iOS bug detected")
                    moveToState({state: 'error', errorMsg: 'Due to a bug in iOS, WebRTC is not supported on this device'})
                }, 10000)
            })
            video.addEventListener('waiting', () => {
                console.debug("Waiting for data")
            })
            video.addEventListener('stalled', () => {
                console.log("Playback stalled, scheduling reconnect")
                scheduleReconnect()
            })
        }
        
        function moveToState(state) {
            state.airplaySupported = airplaySupported
            state.pipActive = pipActive
            
            if (JSON.stringify(lastState) == JSON.stringify(state)) return
            console.log("State now", JSON.stringify(state))
            lastState = JSON.parse(JSON.stringify(state));

            
            if (window.webkit) {
                window.webkit.messageHandlers.stateHandler.postMessage(state);
            } 
            
            if (hasOctoApp()) {
                octoapp.moveToState(JSON.stringify(state));
            }
        }
        
        function takeSnapshot() {
            console.debug("Taking snapshot")
            
            const video = getVideo();
            var canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            var dataUri = canvas.toDataURL('image/jpeg');
  
            if (hasOctoApp()) {
                return dataUri;
            } 
            
            if(window.webkit) {
                window.webkit.messageHandlers.snapshotHandler.postMessage({dataUri: dataUri});
            }
            
            return true;
        }
        
        function getVideo() {
            const video = document.getElementsByTagName('video')
            if (!video || !video[0]) {
                console.error("Video not found")
                throw "Video not found"
            } else {
                return video[0]
            }
        }
        
        function enterPip() {
            console.debug("Requesting pip")
            getVideo().requestPictureInPicture()
            return true
        }

        function scheduleReconnect() {
            if (reconnectTimeoutId) {
                window.clearTimeout(reconnectTimeoutId)
            }
            
            console.log("Reconnect scheduled in 10s")
            reconnectTimeoutId = window.setTimeout(() => {
                startWebRTC()
            }, 10000)
        }
        
        moveToState({state: 'loading'});
                         
        window.onload = function() {
            console.log("Loading")
            setupVideoCallbacks();
            startWebRTC();
        }
        
        window.onunload = function() {
           console.log("Unloading");
        }
    """.trimIndent()

    //endregion
    //region private val sharedCss = ...
    private val sharedCss = """
        body {
            margin:0;
            padding:0;
            background-color:#000;
        }
        
        .streamstage {
            position:fixed;
            top:0;
            left:0;
            width:100%;
            height:100%;
        }
        
        .streamstage:before {
            content: '';
            box-sizing: border-box;
            position: absolute;
            top: 50%;
            left: 50%;
            width: 2rem;
            height: 2rem;
            margin-top: -1rem;
            margin-left: -1rem;
        }
        
        .streamstage video {
            max-height: 100%;
            max-width: 100%;
            margin: auto;
            position: absolute;
            top: 0; left: 0; bottom: 0; right: 0;
        }
    """.trimIndent()

    //endregion
    //region private val janusAdapterJs = ...
    private val janusAdapterJs = """
        !function(e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):("undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this).adapter=e()}(function(){return function n(i,o,a){function s(t,e){if(!o[t]){if(!i[t]){var r="function"==typeof require&&require;if(!e&&r)return r(t,!0);if(c)return c(t,!0);throw(r=new Error("Cannot find module '"+t+"'")).code="MODULE_NOT_FOUND",r}r=o[t]={exports:{}},i[t][0].call(r.exports,function(e){return s(i[t][1][e]||e)},r,r.exports,n,i,o,a)}return o[t].exports}for(var c="function"==typeof require&&require,e=0;e<a.length;e++)s(a[e]);return s}({1:[function(e,t,r){"use strict";e=(0,e("./adapter_factory.js").adapterFactory)({window:"undefined"==typeof window?void 0:window});t.exports=e},{"./adapter_factory.js":2}],2:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.adapterFactory=function(){var e=(0<arguments.length&&void 0!==arguments[0]?arguments[0]:{}).window,t=1<arguments.length&&void 0!==arguments[1]?arguments[1]:{shimChrome:!0,shimFirefox:!0,shimSafari:!0},r=o.log,n=o.detectBrowser(e),i={browserDetails:n,commonShim:p,extractVersion:o.extractVersion,disableLog:o.disableLog,disableWarnings:o.disableWarnings,sdp:d};switch(n.browser){case"chrome":if(!a||!a.shimPeerConnection||!t.shimChrome)return r("Chrome shim is not included in this adapter release."),i;if(null===n.version)return r("Chrome shim can not determine version, not shimming."),i;r("adapter.js shimming chrome."),i.browserShim=a,p.shimAddIceCandidateNullOrEmpty(e,n),p.shimParameterlessSetLocalDescription(e,n),a.shimGetUserMedia(e,n),a.shimMediaStream(e,n),a.shimPeerConnection(e,n),a.shimOnTrack(e,n),a.shimAddTrackRemoveTrack(e,n),a.shimGetSendersWithDtmf(e,n),a.shimGetStats(e,n),a.shimSenderReceiverGetStats(e,n),a.fixNegotiationNeeded(e,n),p.shimRTCIceCandidate(e,n),p.shimConnectionState(e,n),p.shimMaxMessageSize(e,n),p.shimSendThrowTypeError(e,n),p.removeExtmapAllowMixed(e,n);break;case"firefox":if(!s||!s.shimPeerConnection||!t.shimFirefox)return r("Firefox shim is not included in this adapter release."),i;r("adapter.js shimming firefox."),i.browserShim=s,p.shimAddIceCandidateNullOrEmpty(e,n),p.shimParameterlessSetLocalDescription(e,n),s.shimGetUserMedia(e,n),s.shimPeerConnection(e,n),s.shimOnTrack(e,n),s.shimRemoveStream(e,n),s.shimSenderGetStats(e,n),s.shimReceiverGetStats(e,n),s.shimRTCDataChannel(e,n),s.shimAddTransceiver(e,n),s.shimGetParameters(e,n),s.shimCreateOffer(e,n),s.shimCreateAnswer(e,n),p.shimRTCIceCandidate(e,n),p.shimConnectionState(e,n),p.shimMaxMessageSize(e,n),p.shimSendThrowTypeError(e,n);break;case"safari":if(!c||!t.shimSafari)return r("Safari shim is not included in this adapter release."),i;r("adapter.js shimming safari."),i.browserShim=c,p.shimAddIceCandidateNullOrEmpty(e,n),p.shimParameterlessSetLocalDescription(e,n),c.shimRTCIceServerUrls(e,n),c.shimCreateOfferLegacy(e,n),c.shimCallbacksAPI(e,n),c.shimLocalStreamsAPI(e,n),c.shimRemoteStreamsAPI(e,n),c.shimTrackEventTransceiver(e,n),c.shimGetUserMedia(e,n),c.shimAudioContext(e,n),p.shimRTCIceCandidate(e,n),p.shimMaxMessageSize(e,n),p.shimSendThrowTypeError(e,n),p.removeExtmapAllowMixed(e,n);break;default:r("Unsupported browser!")}return i};var o=n(e("./utils")),a=n(e("./chrome/chrome_shim")),s=n(e("./firefox/firefox_shim")),c=n(e("./safari/safari_shim")),p=n(e("./common_shim")),d=n(e("sdp"));function n(e){if(e&&e.__esModule)return e;var t={};if(null!=e)for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t.default=e,t}},{"./chrome/chrome_shim":3,"./common_shim":6,"./firefox/firefox_shim":7,"./safari/safari_shim":10,"./utils":11,sdp:12}],3:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.shimGetDisplayMedia=r.shimGetUserMedia=void 0;var s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},n=e("./getusermedia");Object.defineProperty(r,"shimGetUserMedia",{enumerable:!0,get:function(){return n.shimGetUserMedia}});var i=e("./getdisplaymedia");Object.defineProperty(r,"shimGetDisplayMedia",{enumerable:!0,get:function(){return i.shimGetDisplayMedia}}),r.shimMediaStream=function(e){e.MediaStream=e.MediaStream||e.webkitMediaStream},r.shimOnTrack=function(o){{var e;"object"!==(void 0===o?"undefined":s(o))||!o.RTCPeerConnection||"ontrack"in o.RTCPeerConnection.prototype?a.wrapPeerConnectionEvent(o,"track",function(e){return e.transceiver||Object.defineProperty(e,"transceiver",{value:{receiver:e.receiver}}),e}):(Object.defineProperty(o.RTCPeerConnection.prototype,"ontrack",{get:function(){return this._ontrack},set:function(e){this._ontrack&&this.removeEventListener("track",this._ontrack),this.addEventListener("track",this._ontrack=e)},enumerable:!0,configurable:!0}),e=o.RTCPeerConnection.prototype.setRemoteDescription,o.RTCPeerConnection.prototype.setRemoteDescription=function(){var i=this;return this._ontrackpoly||(this._ontrackpoly=function(n){n.stream.addEventListener("addtrack",function(t){var e=void 0,e=o.RTCPeerConnection.prototype.getReceivers?i.getReceivers().find(function(e){return e.track&&e.track.id===t.track.id}):{track:t.track},r=new Event("track");r.track=t.track,r.receiver=e,r.transceiver={receiver:e},r.streams=[n.stream],i.dispatchEvent(r)}),n.stream.getTracks().forEach(function(t){var e=void 0,e=o.RTCPeerConnection.prototype.getReceivers?i.getReceivers().find(function(e){return e.track&&e.track.id===t.id}):{track:t},r=new Event("track");r.track=t,r.receiver=e,r.transceiver={receiver:e},r.streams=[n.stream],i.dispatchEvent(r)})},this.addEventListener("addstream",this._ontrackpoly)),e.apply(this,arguments)})}},r.shimGetSendersWithDtmf=function(e){{var n,i,t,r,o,a;"object"===(void 0===e?"undefined":s(e))&&e.RTCPeerConnection&&!("getSenders"in e.RTCPeerConnection.prototype)&&"createDTMFSender"in e.RTCPeerConnection.prototype?(n=function(e,t){return{track:t,get dtmf(){return void 0===this._dtmf&&("audio"===t.kind?this._dtmf=e.createDTMFSender(t):this._dtmf=null),this._dtmf},_pc:e}},e.RTCPeerConnection.prototype.getSenders||(e.RTCPeerConnection.prototype.getSenders=function(){return this._senders=this._senders||[],this._senders.slice()},i=e.RTCPeerConnection.prototype.addTrack,e.RTCPeerConnection.prototype.addTrack=function(e,t){var r=i.apply(this,arguments);return r||(r=n(this,e),this._senders.push(r)),r},t=e.RTCPeerConnection.prototype.removeTrack,e.RTCPeerConnection.prototype.removeTrack=function(e){t.apply(this,arguments);e=this._senders.indexOf(e);-1!==e&&this._senders.splice(e,1)}),r=e.RTCPeerConnection.prototype.addStream,e.RTCPeerConnection.prototype.addStream=function(e){var t=this;this._senders=this._senders||[],r.apply(this,[e]),e.getTracks().forEach(function(e){t._senders.push(n(t,e))})},o=e.RTCPeerConnection.prototype.removeStream,e.RTCPeerConnection.prototype.removeStream=function(e){var r=this;this._senders=this._senders||[],o.apply(this,[e]),e.getTracks().forEach(function(t){var e=r._senders.find(function(e){return e.track===t});e&&r._senders.splice(r._senders.indexOf(e),1)})}):"object"===(void 0===e?"undefined":s(e))&&e.RTCPeerConnection&&"getSenders"in e.RTCPeerConnection.prototype&&"createDTMFSender"in e.RTCPeerConnection.prototype&&e.RTCRtpSender&&!("dtmf"in e.RTCRtpSender.prototype)&&(a=e.RTCPeerConnection.prototype.getSenders,e.RTCPeerConnection.prototype.getSenders=function(){var t=this,e=a.apply(this,[]);return e.forEach(function(e){return e._pc=t}),e},Object.defineProperty(e.RTCRtpSender.prototype,"dtmf",{get:function(){return void 0===this._dtmf&&("audio"===this.track.kind?this._dtmf=this._pc.createDTMFSender(this.track):this._dtmf=null),this._dtmf}}))}},r.shimGetStats=function(e){var a;e.RTCPeerConnection&&(a=e.RTCPeerConnection.prototype.getStats,e.RTCPeerConnection.prototype.getStats=function(){var r=this,e=Array.prototype.slice.call(arguments),t=e[0],n=e[1],e=e[2];if(0<arguments.length&&"function"==typeof t)return a.apply(this,arguments);if(0===a.length&&(0===arguments.length||"function"!=typeof t))return a.apply(this,[]);var i=function(e){var n={};return e.result().forEach(function(t){var r={id:t.id,timestamp:t.timestamp,type:{localcandidate:"local-candidate",remotecandidate:"remote-candidate"}[t.type]||t.type};t.names().forEach(function(e){r[e]=t.stat(e)}),n[r.id]=r}),n},o=function(t){return new Map(Object.keys(t).map(function(e){return[e,t[e]]}))};if(2<=arguments.length)return a.apply(this,[function(e){n(o(i(e)))},t]);return new Promise(function(t,e){a.apply(r,[function(e){t(o(i(e)))},e])}).then(n,e)})},r.shimSenderReceiverGetStats=function(e){var r,t,n,o;"object"===(void 0===e?"undefined":s(e))&&e.RTCPeerConnection&&e.RTCRtpSender&&e.RTCRtpReceiver&&("getStats"in e.RTCRtpSender.prototype||((r=e.RTCPeerConnection.prototype.getSenders)&&(e.RTCPeerConnection.prototype.getSenders=function(){var t=this,e=r.apply(this,[]);return e.forEach(function(e){return e._pc=t}),e}),(t=e.RTCPeerConnection.prototype.addTrack)&&(e.RTCPeerConnection.prototype.addTrack=function(){var e=t.apply(this,arguments);return e._pc=this,e}),e.RTCRtpSender.prototype.getStats=function(){var t=this;return this._pc.getStats().then(function(e){return a.filterStats(e,t.track,!0)})}),"getStats"in e.RTCRtpReceiver.prototype||((n=e.RTCPeerConnection.prototype.getReceivers)&&(e.RTCPeerConnection.prototype.getReceivers=function(){var t=this,e=n.apply(this,[]);return e.forEach(function(e){return e._pc=t}),e}),a.wrapPeerConnectionEvent(e,"track",function(e){return e.receiver._pc=e.srcElement,e}),e.RTCRtpReceiver.prototype.getStats=function(){var t=this;return this._pc.getStats().then(function(e){return a.filterStats(e,t.track,!1)})}),"getStats"in e.RTCRtpSender.prototype&&"getStats"in e.RTCRtpReceiver.prototype&&(o=e.RTCPeerConnection.prototype.getStats,e.RTCPeerConnection.prototype.getStats=function(){if(0<arguments.length&&arguments[0]instanceof e.MediaStreamTrack){var t=arguments[0],r=void 0,n=void 0,i=void 0;return this.getSenders().forEach(function(e){e.track===t&&(r?i=!0:r=e)}),this.getReceivers().forEach(function(e){return e.track===t&&(n?i=!0:n=e),e.track===t}),i||r&&n?Promise.reject(new DOMException("There are more than one sender or receiver for the track.","InvalidAccessError")):r?r.getStats():n?n.getStats():Promise.reject(new DOMException("There is no sender or receiver for the track.","InvalidAccessError"))}return o.apply(this,arguments)}))},r.shimAddTrackRemoveTrackWithNative=p,r.shimAddTrackRemoveTrack=function(i,e){if(i.RTCPeerConnection){if(i.RTCPeerConnection.prototype.addTrack&&65<=e.version)return p(i);var r=i.RTCPeerConnection.prototype.getLocalStreams;i.RTCPeerConnection.prototype.getLocalStreams=function(){var t=this,e=r.apply(this);return this._reverseStreams=this._reverseStreams||{},e.map(function(e){return t._reverseStreams[e.id]})};var n=i.RTCPeerConnection.prototype.addStream;i.RTCPeerConnection.prototype.addStream=function(e){var t,r=this;this._streams=this._streams||{},this._reverseStreams=this._reverseStreams||{},e.getTracks().forEach(function(t){if(r.getSenders().find(function(e){return e.track===t}))throw new DOMException("Track already exists.","InvalidAccessError")}),this._reverseStreams[e.id]||(t=new i.MediaStream(e.getTracks()),this._streams[e.id]=t,this._reverseStreams[t.id]=e,e=t),n.apply(this,[e])};var t=i.RTCPeerConnection.prototype.removeStream;i.RTCPeerConnection.prototype.removeStream=function(e){this._streams=this._streams||{},this._reverseStreams=this._reverseStreams||{},t.apply(this,[this._streams[e.id]||e]),delete this._reverseStreams[(this._streams[e.id]||e).id],delete this._streams[e.id]},i.RTCPeerConnection.prototype.addTrack=function(t,e){var r=this;if("closed"===this.signalingState)throw new DOMException("The RTCPeerConnection's signalingState is 'closed'.","InvalidStateError");var n=[].slice.call(arguments,1);if(1!==n.length||!n[0].getTracks().find(function(e){return e===t}))throw new DOMException("The adapter.js addTrack polyfill only supports a single  stream which is associated with the specified track.","NotSupportedError");if(this.getSenders().find(function(e){return e.track===t}))throw new DOMException("Track already exists.","InvalidAccessError");this._streams=this._streams||{},this._reverseStreams=this._reverseStreams||{};n=this._streams[e.id];return n?(n.addTrack(t),Promise.resolve().then(function(){r.dispatchEvent(new Event("negotiationneeded"))})):(n=new i.MediaStream([t]),this._streams[e.id]=n,this._reverseStreams[n.id]=e,this.addStream(n)),this.getSenders().find(function(e){return e.track===t})},["createOffer","createAnswer"].forEach(function(e){var n=i.RTCPeerConnection.prototype[e],t=c({},e,function(){var t=this,r=arguments;return arguments.length&&"function"==typeof arguments[0]?n.apply(this,[function(e){e=s(t,e);r[0].apply(null,[e])},function(e){r[1]&&r[1].apply(null,e)},arguments[2]]):n.apply(this,arguments).then(function(e){return s(t,e)})});i.RTCPeerConnection.prototype[e]=t[e]});var o=i.RTCPeerConnection.prototype.setLocalDescription;i.RTCPeerConnection.prototype.setLocalDescription=function(){return arguments.length&&arguments[0].type&&(arguments[0]=(r=this,n=(e=arguments[0]).sdp,Object.keys(r._reverseStreams||[]).forEach(function(e){var t=r._reverseStreams[e],e=r._streams[t.id];n=n.replace(new RegExp(t.id,"g"),e.id)}),new RTCSessionDescription({type:e.type,sdp:n}))),o.apply(this,arguments);var r,e,n};var a=Object.getOwnPropertyDescriptor(i.RTCPeerConnection.prototype,"localDescription");Object.defineProperty(i.RTCPeerConnection.prototype,"localDescription",{get:function(){var e=a.get.apply(this);return""===e.type?e:s(this,e)}}),i.RTCPeerConnection.prototype.removeTrack=function(t){var r=this;if("closed"===this.signalingState)throw new DOMException("The RTCPeerConnection's signalingState is 'closed'.","InvalidStateError");if(!t._pc)throw new DOMException("Argument 1 of RTCPeerConnection.removeTrack does not implement interface RTCRtpSender.","TypeError");if(!(t._pc===this))throw new DOMException("Sender was not created by this connection.","InvalidAccessError");this._streams=this._streams||{};var n=void 0;Object.keys(this._streams).forEach(function(e){r._streams[e].getTracks().find(function(e){return t.track===e})&&(n=r._streams[e])}),n&&(1===n.getTracks().length?this.removeStream(this._reverseStreams[n.id]):n.removeTrack(t.track),this.dispatchEvent(new Event("negotiationneeded")))}}function s(r,e){var n=e.sdp;return Object.keys(r._reverseStreams||[]).forEach(function(e){var t=r._reverseStreams[e],e=r._streams[t.id];n=n.replace(new RegExp(e.id,"g"),t.id)}),new RTCSessionDescription({type:e.type,sdp:n})}},r.shimPeerConnection=function(n,e){!n.RTCPeerConnection&&n.webkitRTCPeerConnection&&(n.RTCPeerConnection=n.webkitRTCPeerConnection);n.RTCPeerConnection&&e.version<53&&["setLocalDescription","setRemoteDescription","addIceCandidate"].forEach(function(e){var t=n.RTCPeerConnection.prototype[e],r=c({},e,function(){return arguments[0]=new("addIceCandidate"===e?n.RTCIceCandidate:n.RTCSessionDescription)(arguments[0]),t.apply(this,arguments)});n.RTCPeerConnection.prototype[e]=r[e]})},r.fixNegotiationNeeded=function(e,r){a.wrapPeerConnectionEvent(e,"negotiationneeded",function(e){var t=e.target;if(!(r.version<72||t.getConfiguration&&"plan-b"===t.getConfiguration().sdpSemantics)||"stable"===t.signalingState)return e})};var a=function(e){{if(e&&e.__esModule)return e;var t={};if(null!=e)for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t.default=e,t}}(e("../utils.js"));function c(e,t,r){return t in e?Object.defineProperty(e,t,{value:r,enumerable:!0,configurable:!0,writable:!0}):e[t]=r,e}function p(e){e.RTCPeerConnection.prototype.getLocalStreams=function(){var t=this;return this._shimmedLocalStreams=this._shimmedLocalStreams||{},Object.keys(this._shimmedLocalStreams).map(function(e){return t._shimmedLocalStreams[e][0]})};var n=e.RTCPeerConnection.prototype.addTrack;e.RTCPeerConnection.prototype.addTrack=function(e,t){if(!t)return n.apply(this,arguments);this._shimmedLocalStreams=this._shimmedLocalStreams||{};var r=n.apply(this,arguments);return this._shimmedLocalStreams[t.id]?-1===this._shimmedLocalStreams[t.id].indexOf(r)&&this._shimmedLocalStreams[t.id].push(r):this._shimmedLocalStreams[t.id]=[t,r],r};var i=e.RTCPeerConnection.prototype.addStream;e.RTCPeerConnection.prototype.addStream=function(e){var r=this;this._shimmedLocalStreams=this._shimmedLocalStreams||{},e.getTracks().forEach(function(t){if(r.getSenders().find(function(e){return e.track===t}))throw new DOMException("Track already exists.","InvalidAccessError")});var t=this.getSenders();i.apply(this,arguments);var n=this.getSenders().filter(function(e){return-1===t.indexOf(e)});this._shimmedLocalStreams[e.id]=[e].concat(n)};var t=e.RTCPeerConnection.prototype.removeStream;e.RTCPeerConnection.prototype.removeStream=function(e){return this._shimmedLocalStreams=this._shimmedLocalStreams||{},delete this._shimmedLocalStreams[e.id],t.apply(this,arguments)};var o=e.RTCPeerConnection.prototype.removeTrack;e.RTCPeerConnection.prototype.removeTrack=function(r){var n=this;return this._shimmedLocalStreams=this._shimmedLocalStreams||{},r&&Object.keys(this._shimmedLocalStreams).forEach(function(e){var t=n._shimmedLocalStreams[e].indexOf(r);-1!==t&&n._shimmedLocalStreams[e].splice(t,1),1===n._shimmedLocalStreams[e].length&&delete n._shimmedLocalStreams[e]}),o.apply(this,arguments)}}},{"../utils.js":11,"./getdisplaymedia":4,"./getusermedia":5}],4:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.shimGetDisplayMedia=function(o,e){o.navigator.mediaDevices&&"getDisplayMedia"in o.navigator.mediaDevices||o.navigator.mediaDevices&&("function"==typeof e?o.navigator.mediaDevices.getDisplayMedia=function(i){return e(i).then(function(e){var t=i.video&&i.video.width,r=i.video&&i.video.height,n=i.video&&i.video.frameRate;return i.video={mandatory:{chromeMediaSource:"desktop",chromeMediaSourceId:e,maxFrameRate:n||3}},t&&(i.video.mandatory.maxWidth=t),r&&(i.video.mandatory.maxHeight=r),o.navigator.mediaDevices.getUserMedia(i)})}:console.error("shimGetDisplayMedia: getSourceId argument is not a function"))}},{}],5:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0});var c="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};r.shimGetUserMedia=function(e,t){var a,n,i,r,s=e&&e.navigator;s.mediaDevices&&(a=function(i){if("object"!==(void 0===i?"undefined":c(i))||i.mandatory||i.optional)return i;var o={};return Object.keys(i).forEach(function(t){var r,n,e;"require"!==t&&"advanced"!==t&&"mediaSource"!==t&&(void 0!==(r="object"===c(i[t])?i[t]:{ideal:i[t]}).exact&&"number"==typeof r.exact&&(r.min=r.max=r.exact),n=function(e,t){return e?e+t.charAt(0).toUpperCase()+t.slice(1):"deviceId"===t?"sourceId":t},void 0!==r.ideal&&(o.optional=o.optional||[],e={},"number"==typeof r.ideal?(e[n("min",t)]=r.ideal,o.optional.push(e),(e={})[n("max",t)]=r.ideal):e[n("",t)]=r.ideal,o.optional.push(e)),void 0!==r.exact&&"number"!=typeof r.exact?(o.mandatory=o.mandatory||{},o.mandatory[n("",t)]=r.exact):["min","max"].forEach(function(e){void 0!==r[e]&&(o.mandatory=o.mandatory||{},o.mandatory[n(e,t)]=r[e])}))}),i.advanced&&(o.optional=(o.optional||[]).concat(i.advanced)),o},n=function(r,n){if(61<=t.version)return n(r);if((r=JSON.parse(JSON.stringify(r)))&&"object"===c(r.audio)&&((e=function(e,t,r){t in e&&!(r in e)&&(e[r]=e[t],delete e[t])})((r=JSON.parse(JSON.stringify(r))).audio,"autoGainControl","googAutoGainControl"),e(r.audio,"noiseSuppression","googNoiseSuppression"),r.audio=a(r.audio)),r&&"object"===c(r.video)){var i=(i=r.video.facingMode)&&("object"===(void 0===i?"undefined":c(i))?i:{ideal:i}),e=t.version<66;if(i&&("user"===i.exact||"environment"===i.exact||"user"===i.ideal||"environment"===i.ideal)&&(!s.mediaDevices.getSupportedConstraints||!s.mediaDevices.getSupportedConstraints().facingMode||e)){delete r.video.facingMode;var o=void 0;if("environment"===i.exact||"environment"===i.ideal?o=["back","rear"]:"user"!==i.exact&&"user"!==i.ideal||(o=["front"]),o)return s.mediaDevices.enumerateDevices().then(function(e){var t=(e=e.filter(function(e){return"videoinput"===e.kind})).find(function(t){return o.some(function(e){return t.label.toLowerCase().includes(e)})});return(t=!t&&e.length&&o.includes("back")?e[e.length-1]:t)&&(r.video.deviceId=i.exact?{exact:t.deviceId}:{ideal:t.deviceId}),r.video=a(r.video),p("chrome: "+JSON.stringify(r)),n(r)})}r.video=a(r.video)}return p("chrome: "+JSON.stringify(r)),n(r)},i=function(e){return 64<=t.version?e:{name:{PermissionDeniedError:"NotAllowedError",PermissionDismissedError:"NotAllowedError",InvalidStateError:"NotAllowedError",DevicesNotFoundError:"NotFoundError",ConstraintNotSatisfiedError:"OverconstrainedError",TrackStartError:"NotReadableError",MediaDeviceFailedDueToShutdown:"NotAllowedError",MediaDeviceKillSwitchOn:"NotAllowedError",TabCaptureError:"AbortError",ScreenCaptureError:"AbortError",DeviceCaptureError:"AbortError"}[e.name]||e.name,message:e.message,constraint:e.constraint||e.constraintName,toString:function(){return this.name+(this.message&&": ")+this.message}}},s.getUserMedia=function(e,t,r){n(e,function(e){s.webkitGetUserMedia(e,t,function(e){r&&r(i(e))})})}.bind(s),s.mediaDevices.getUserMedia&&(r=s.mediaDevices.getUserMedia.bind(s.mediaDevices),s.mediaDevices.getUserMedia=function(e){return n(e,function(t){return r(t).then(function(e){if(t.audio&&!e.getAudioTracks().length||t.video&&!e.getVideoTracks().length)throw e.getTracks().forEach(function(e){e.stop()}),new DOMException("","NotFoundError");return e},function(e){return Promise.reject(i(e))})})}))};var p=function(e){{if(e&&e.__esModule)return e;var t={};if(null!=e)for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t.default=e,t}}(e("../utils.js")).log},{"../utils.js":11}],6:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0});var o="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};r.shimRTCIceCandidate=function(t){var i;!t.RTCIceCandidate||t.RTCIceCandidate&&"foundation"in t.RTCIceCandidate.prototype||(i=t.RTCIceCandidate,t.RTCIceCandidate=function(e){if("object"===(void 0===e?"undefined":o(e))&&e.candidate&&0===e.candidate.indexOf("a=")&&((e=JSON.parse(JSON.stringify(e))).candidate=e.candidate.substr(2)),e.candidate&&e.candidate.length){var t=new i(e),r=a.default.parseCandidate(e.candidate),n=Object.assign(t,r);return n.toJSON=function(){return{candidate:n.candidate,sdpMid:n.sdpMid,sdpMLineIndex:n.sdpMLineIndex,usernameFragment:n.usernameFragment}},n}return new i(e)},t.RTCIceCandidate.prototype=i.prototype,s.wrapPeerConnectionEvent(t,"icecandidate",function(e){return e.candidate&&Object.defineProperty(e,"candidate",{value:new t.RTCIceCandidate(e.candidate),writable:"false"}),e}))},r.shimMaxMessageSize=function(e,i){var o;e.RTCPeerConnection&&("sctp"in e.RTCPeerConnection.prototype||Object.defineProperty(e.RTCPeerConnection.prototype,"sctp",{get:function(){return void 0===this._sctp?null:this._sctp}}),o=e.RTCPeerConnection.prototype.setRemoteDescription,e.RTCPeerConnection.prototype.setRemoteDescription=function(){var e,t,r,n;return this._sctp=null,"chrome"===i.browser&&76<=i.version&&"plan-b"===this.getConfiguration().sdpSemantics&&Object.defineProperty(this,"sctp",{get:function(){return void 0===this._sctp?null:this._sctp},enumerable:!0,configurable:!0}),function(e){if(!e||!e.sdp)return!1;e=a.default.splitSections(e.sdp);return e.shift(),e.some(function(e){e=a.default.parseMLine(e);return e&&"application"===e.kind&&-1!==e.protocol.indexOf("SCTP")})}(arguments[0])&&(t=function(e){e=e.sdp.match(/mozilla...THIS_IS_SDPARTA-(\d+)/);if(null===e||e.length<2)return-1;e=parseInt(e[1],10);return e!=e?-1:e}(arguments[0]),r=t,n=65536,n=n="firefox"===i.browser?i.version<57?-1===r?16384:2147483637:i.version<60?57===i.version?65535:65536:2147483637:n,t=function(e,t){var r=65536;"firefox"===i.browser&&57===i.version&&(r=65535);e=a.default.matchPrefix(e.sdp,"a=max-message-size:");return 0<e.length?r=parseInt(e[0].substr(19),10):"firefox"===i.browser&&-1!==t&&(r=2147483637),r}(arguments[0],t),e=void 0,e=0===n&&0===t?Number.POSITIVE_INFINITY:0===n||0===t?Math.max(n,t):Math.min(n,t),t={},Object.defineProperty(t,"maxMessageSize",{get:function(){return e}}),this._sctp=t),o.apply(this,arguments)})},r.shimSendThrowTypeError=function(e){var t;function r(t,r){var n=t.send;t.send=function(){var e=arguments[0],e=e.length||e.size||e.byteLength;if("open"===t.readyState&&r.sctp&&e>r.sctp.maxMessageSize)throw new TypeError("Message too large (can send a maximum of "+r.sctp.maxMessageSize+" bytes)");return n.apply(t,arguments)}}e.RTCPeerConnection&&"createDataChannel"in e.RTCPeerConnection.prototype&&(t=e.RTCPeerConnection.prototype.createDataChannel,e.RTCPeerConnection.prototype.createDataChannel=function(){var e=t.apply(this,arguments);return r(e,this),e},s.wrapPeerConnectionEvent(e,"datachannel",function(e){return r(e.channel,e.target),e}))},r.shimConnectionState=function(e){var r;!e.RTCPeerConnection||"connectionState"in e.RTCPeerConnection.prototype||(r=e.RTCPeerConnection.prototype,Object.defineProperty(r,"connectionState",{get:function(){return{completed:"connected",checking:"connecting"}[this.iceConnectionState]||this.iceConnectionState},enumerable:!0,configurable:!0}),Object.defineProperty(r,"onconnectionstatechange",{get:function(){return this._onconnectionstatechange||null},set:function(e){this._onconnectionstatechange&&(this.removeEventListener("connectionstatechange",this._onconnectionstatechange),delete this._onconnectionstatechange),e&&this.addEventListener("connectionstatechange",this._onconnectionstatechange=e)},enumerable:!0,configurable:!0}),["setLocalDescription","setRemoteDescription"].forEach(function(e){var t=r[e];r[e]=function(){return this._connectionstatechangepoly||(this._connectionstatechangepoly=function(e){var t,r=e.target;return r._lastConnectionState!==r.connectionState&&(r._lastConnectionState=r.connectionState,t=new Event("connectionstatechange",e),r.dispatchEvent(t)),e},this.addEventListener("iceconnectionstatechange",this._connectionstatechangepoly)),t.apply(this,arguments)}}))},r.removeExtmapAllowMixed=function(r,e){var n;r.RTCPeerConnection&&("chrome"===e.browser&&71<=e.version||"safari"===e.browser&&605<=e.version||(n=r.RTCPeerConnection.prototype.setRemoteDescription,r.RTCPeerConnection.prototype.setRemoteDescription=function(e){var t;return e&&e.sdp&&-1!==e.sdp.indexOf("\na=extmap-allow-mixed")&&(t=e.sdp.split("\n").filter(function(e){return"a=extmap-allow-mixed"!==e.trim()}).join("\n"),r.RTCSessionDescription&&e instanceof r.RTCSessionDescription?arguments[0]=new r.RTCSessionDescription({type:e.type,sdp:t}):e.sdp=t),n.apply(this,arguments)}))},r.shimAddIceCandidateNullOrEmpty=function(e,t){var r;e.RTCPeerConnection&&e.RTCPeerConnection.prototype&&((r=e.RTCPeerConnection.prototype.addIceCandidate)&&0!==r.length&&(e.RTCPeerConnection.prototype.addIceCandidate=function(){return arguments[0]?("chrome"===t.browser&&t.version<78||"firefox"===t.browser&&t.version<68||"safari"===t.browser)&&arguments[0]&&""===arguments[0].candidate?Promise.resolve():r.apply(this,arguments):(arguments[1]&&arguments[1].apply(null),Promise.resolve())}))},r.shimParameterlessSetLocalDescription=function(e,t){var r;e.RTCPeerConnection&&e.RTCPeerConnection.prototype&&((r=e.RTCPeerConnection.prototype.setLocalDescription)&&0!==r.length&&(e.RTCPeerConnection.prototype.setLocalDescription=function(){var t=this,e=arguments[0]||{};if("object"!==(void 0===e?"undefined":o(e))||e.type&&e.sdp)return r.apply(this,arguments);if(!(e={type:e.type,sdp:e.sdp}).type)switch(this.signalingState){case"stable":case"have-local-offer":case"have-remote-pranswer":e.type="offer";break;default:e.type="answer"}return e.sdp||"offer"!==e.type&&"answer"!==e.type?r.apply(this,[e]):("offer"===e.type?this.createOffer:this.createAnswer).apply(this).then(function(e){return r.apply(t,[e])})}))};var n,i=e("sdp"),a=(n=i)&&n.__esModule?n:{default:n},s=function(e){{if(e&&e.__esModule)return e;var t={};if(null!=e)for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t.default=e,t}}(e("./utils"))},{"./utils":11,sdp:12}],7:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.shimGetDisplayMedia=r.shimGetUserMedia=void 0;var a="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},n=e("./getusermedia");Object.defineProperty(r,"shimGetUserMedia",{enumerable:!0,get:function(){return n.shimGetUserMedia}});var i=e("./getdisplaymedia");Object.defineProperty(r,"shimGetDisplayMedia",{enumerable:!0,get:function(){return i.shimGetDisplayMedia}}),r.shimOnTrack=function(e){"object"===(void 0===e?"undefined":a(e))&&e.RTCTrackEvent&&"receiver"in e.RTCTrackEvent.prototype&&!("transceiver"in e.RTCTrackEvent.prototype)&&Object.defineProperty(e.RTCTrackEvent.prototype,"transceiver",{get:function(){return{receiver:this.receiver}}})},r.shimPeerConnection=function(n,i){var o,r;"object"===(void 0===n?"undefined":a(n))&&(n.RTCPeerConnection||n.mozRTCPeerConnection)&&(!n.RTCPeerConnection&&n.mozRTCPeerConnection&&(n.RTCPeerConnection=n.mozRTCPeerConnection),i.version<53&&["setLocalDescription","setRemoteDescription","addIceCandidate"].forEach(function(e){var t=n.RTCPeerConnection.prototype[e],r=function(e,t,r){t in e?Object.defineProperty(e,t,{value:r,enumerable:!0,configurable:!0,writable:!0}):e[t]=r;return e}({},e,function(){return arguments[0]=new("addIceCandidate"===e?n.RTCIceCandidate:n.RTCSessionDescription)(arguments[0]),t.apply(this,arguments)});n.RTCPeerConnection.prototype[e]=r[e]}),o={inboundrtp:"inbound-rtp",outboundrtp:"outbound-rtp",candidatepair:"candidate-pair",localcandidate:"local-candidate",remotecandidate:"remote-candidate"},r=n.RTCPeerConnection.prototype.getStats,n.RTCPeerConnection.prototype.getStats=function(){var e=Array.prototype.slice.call(arguments),t=e[0],n=e[1],e=e[2];return r.apply(this,[t||null]).then(function(r){if(i.version<53&&!n)try{r.forEach(function(e){e.type=o[e.type]||e.type})}catch(e){if("TypeError"!==e.name)throw e;r.forEach(function(e,t){r.set(t,Object.assign({},e,{type:o[e.type]||e.type}))})}return r}).then(n,e)})},r.shimSenderGetStats=function(e){var r,t;"object"===(void 0===e?"undefined":a(e))&&e.RTCPeerConnection&&e.RTCRtpSender&&(e.RTCRtpSender&&"getStats"in e.RTCRtpSender.prototype||((r=e.RTCPeerConnection.prototype.getSenders)&&(e.RTCPeerConnection.prototype.getSenders=function(){var t=this,e=r.apply(this,[]);return e.forEach(function(e){return e._pc=t}),e}),(t=e.RTCPeerConnection.prototype.addTrack)&&(e.RTCPeerConnection.prototype.addTrack=function(){var e=t.apply(this,arguments);return e._pc=this,e}),e.RTCRtpSender.prototype.getStats=function(){return this.track?this._pc.getStats(this.track):Promise.resolve(new Map)}))},r.shimReceiverGetStats=function(e){var r;"object"===(void 0===e?"undefined":a(e))&&e.RTCPeerConnection&&e.RTCRtpSender&&(e.RTCRtpSender&&"getStats"in e.RTCRtpReceiver.prototype||((r=e.RTCPeerConnection.prototype.getReceivers)&&(e.RTCPeerConnection.prototype.getReceivers=function(){var t=this,e=r.apply(this,[]);return e.forEach(function(e){return e._pc=t}),e}),o.wrapPeerConnectionEvent(e,"track",function(e){return e.receiver._pc=e.srcElement,e}),e.RTCRtpReceiver.prototype.getStats=function(){return this._pc.getStats(this.track)}))},r.shimRemoveStream=function(e){!e.RTCPeerConnection||"removeStream"in e.RTCPeerConnection.prototype||(e.RTCPeerConnection.prototype.removeStream=function(t){var r=this;o.deprecated("removeStream","removeTrack"),this.getSenders().forEach(function(e){e.track&&t.getTracks().includes(e.track)&&r.removeTrack(e)})})},r.shimRTCDataChannel=function(e){e.DataChannel&&!e.RTCDataChannel&&(e.RTCDataChannel=e.DataChannel)},r.shimAddTransceiver=function(e){var i;"object"!==(void 0===e?"undefined":a(e))||!e.RTCPeerConnection||(i=e.RTCPeerConnection.prototype.addTransceiver)&&(e.RTCPeerConnection.prototype.addTransceiver=function(){this.setParametersPromises=[];var e=arguments[1],t=e&&"sendEncodings"in e;t&&e.sendEncodings.forEach(function(e){if("rid"in e)if(!/^[a-z0-9]{0,16}${'$'}/i.test(e.rid))throw new TypeError("Invalid RID value provided.");if("scaleResolutionDownBy"in e&&!(1<=parseFloat(e.scaleResolutionDownBy)))throw new RangeError("scale_resolution_down_by must be >= 1.0");if("maxFramerate"in e&&!(0<=parseFloat(e.maxFramerate)))throw new RangeError("max_framerate must be >= 0.0")});var r,n=i.apply(this,arguments);return t&&("encodings"in(t=(r=n.sender).getParameters())&&(1!==t.encodings.length||0!==Object.keys(t.encodings[0]).length)||(t.encodings=e.sendEncodings,r.sendEncodings=e.sendEncodings,this.setParametersPromises.push(r.setParameters(t).then(function(){delete r.sendEncodings}).catch(function(){delete r.sendEncodings})))),n})},r.shimGetParameters=function(e){var t;"object"!==(void 0===e?"undefined":a(e))||!e.RTCRtpSender||(t=e.RTCRtpSender.prototype.getParameters)&&(e.RTCRtpSender.prototype.getParameters=function(){var e=t.apply(this,arguments);return"encodings"in e||(e.encodings=[].concat(this.sendEncodings||[{}])),e})},r.shimCreateOffer=function(e){var r;"object"===(void 0===e?"undefined":a(e))&&e.RTCPeerConnection&&(r=e.RTCPeerConnection.prototype.createOffer,e.RTCPeerConnection.prototype.createOffer=function(){var e=this,t=arguments;return this.setParametersPromises&&this.setParametersPromises.length?Promise.all(this.setParametersPromises).then(function(){return r.apply(e,t)}).finally(function(){e.setParametersPromises=[]}):r.apply(this,arguments)})},r.shimCreateAnswer=function(e){var r;"object"===(void 0===e?"undefined":a(e))&&e.RTCPeerConnection&&(r=e.RTCPeerConnection.prototype.createAnswer,e.RTCPeerConnection.prototype.createAnswer=function(){var e=this,t=arguments;return this.setParametersPromises&&this.setParametersPromises.length?Promise.all(this.setParametersPromises).then(function(){return r.apply(e,t)}).finally(function(){e.setParametersPromises=[]}):r.apply(this,arguments)})};var o=function(e){{if(e&&e.__esModule)return e;var t={};if(null!=e)for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t.default=e,t}}(e("../utils"))},{"../utils":11,"./getdisplaymedia":8,"./getusermedia":9}],8:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0}),r.shimGetDisplayMedia=function(t,r){t.navigator.mediaDevices&&"getDisplayMedia"in t.navigator.mediaDevices||t.navigator.mediaDevices&&(t.navigator.mediaDevices.getDisplayMedia=function(e){if(e&&e.video)return!0===e.video?e.video={mediaSource:r}:e.video.mediaSource=r,t.navigator.mediaDevices.getUserMedia(e);e=new DOMException("getDisplayMedia without video constraints is undefined");return e.name="NotFoundError",e.code=8,Promise.reject(e)})}},{}],9:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0});var s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};r.shimGetUserMedia=function(e,t){var n=e&&e.navigator,e=e&&e.MediaStreamTrack;{var r,i,o,a;n.getUserMedia=function(e,t,r){c.deprecated("navigator.getUserMedia","navigator.mediaDevices.getUserMedia"),n.mediaDevices.getUserMedia(e).then(t,r)},55<t.version&&"autoGainControl"in n.mediaDevices.getSupportedConstraints()||(r=function(e,t,r){t in e&&!(r in e)&&(e[r]=e[t],delete e[t])},i=n.mediaDevices.getUserMedia.bind(n.mediaDevices),n.mediaDevices.getUserMedia=function(e){return"object"===(void 0===e?"undefined":s(e))&&"object"===s(e.audio)&&(e=JSON.parse(JSON.stringify(e)),r(e.audio,"autoGainControl","mozAutoGainControl"),r(e.audio,"noiseSuppression","mozNoiseSuppression")),i(e)},e&&e.prototype.getSettings&&(o=e.prototype.getSettings,e.prototype.getSettings=function(){var e=o.apply(this,arguments);return r(e,"mozAutoGainControl","autoGainControl"),r(e,"mozNoiseSuppression","noiseSuppression"),e}),e&&e.prototype.applyConstraints&&(a=e.prototype.applyConstraints,e.prototype.applyConstraints=function(e){return"audio"===this.kind&&"object"===(void 0===e?"undefined":s(e))&&(e=JSON.parse(JSON.stringify(e)),r(e,"autoGainControl","mozAutoGainControl"),r(e,"noiseSuppression","mozNoiseSuppression")),a.apply(this,[e])}))}};var c=function(e){{if(e&&e.__esModule)return e;var t={};if(null!=e)for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t.default=e,t}}(e("../utils"))},{"../utils":11}],10:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0});var c="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};r.shimLocalStreamsAPI=function(e){var o;"object"===(void 0===e?"undefined":c(e))&&e.RTCPeerConnection&&("getLocalStreams"in e.RTCPeerConnection.prototype||(e.RTCPeerConnection.prototype.getLocalStreams=function(){return this._localStreams||(this._localStreams=[]),this._localStreams}),"addStream"in e.RTCPeerConnection.prototype||(o=e.RTCPeerConnection.prototype.addTrack,e.RTCPeerConnection.prototype.addStream=function(t){var r=this;this._localStreams||(this._localStreams=[]),this._localStreams.includes(t)||this._localStreams.push(t),t.getAudioTracks().forEach(function(e){return o.call(r,e,t)}),t.getVideoTracks().forEach(function(e){return o.call(r,e,t)})},e.RTCPeerConnection.prototype.addTrack=function(e){for(var t=this,r=arguments.length,n=Array(1<r?r-1:0),i=1;i<r;i++)n[i-1]=arguments[i];return n&&n.forEach(function(e){t._localStreams?t._localStreams.includes(e)||t._localStreams.push(e):t._localStreams=[e]}),o.apply(this,arguments)}),"removeStream"in e.RTCPeerConnection.prototype||(e.RTCPeerConnection.prototype.removeStream=function(e){var t=this;this._localStreams||(this._localStreams=[]);var r,n=this._localStreams.indexOf(e);-1!==n&&(this._localStreams.splice(n,1),r=e.getTracks(),this.getSenders().forEach(function(e){r.includes(e.track)&&t.removeTrack(e)}))}))},r.shimRemoteStreamsAPI=function(e){var t;"object"===(void 0===e?"undefined":c(e))&&e.RTCPeerConnection&&("getRemoteStreams"in e.RTCPeerConnection.prototype||(e.RTCPeerConnection.prototype.getRemoteStreams=function(){return this._remoteStreams||[]}),"onaddstream"in e.RTCPeerConnection.prototype||(Object.defineProperty(e.RTCPeerConnection.prototype,"onaddstream",{get:function(){return this._onaddstream},set:function(e){var r=this;this._onaddstream&&(this.removeEventListener("addstream",this._onaddstream),this.removeEventListener("track",this._onaddstreampoly)),this.addEventListener("addstream",this._onaddstream=e),this.addEventListener("track",this._onaddstreampoly=function(e){e.streams.forEach(function(e){var t;r._remoteStreams||(r._remoteStreams=[]),r._remoteStreams.includes(e)||(r._remoteStreams.push(e),(t=new Event("addstream")).stream=e,r.dispatchEvent(t))})})}}),t=e.RTCPeerConnection.prototype.setRemoteDescription,e.RTCPeerConnection.prototype.setRemoteDescription=function(){var r=this;return this._onaddstreampoly||this.addEventListener("track",this._onaddstreampoly=function(e){e.streams.forEach(function(e){var t;r._remoteStreams||(r._remoteStreams=[]),0<=r._remoteStreams.indexOf(e)||(r._remoteStreams.push(e),(t=new Event("addstream")).stream=e,r.dispatchEvent(t))})}),t.apply(r,arguments)}))},r.shimCallbacksAPI=function(e){var t,n,i,o,a,s;"object"===(void 0===e?"undefined":c(e))&&e.RTCPeerConnection&&(t=e.RTCPeerConnection.prototype,n=t.createOffer,i=t.createAnswer,o=t.setLocalDescription,a=t.setRemoteDescription,s=t.addIceCandidate,t.createOffer=function(e,t){var r=n.apply(this,[2<=arguments.length?arguments[2]:e]);return t?(r.then(e,t),Promise.resolve()):r},t.createAnswer=function(e,t){var r=i.apply(this,[2<=arguments.length?arguments[2]:e]);return t?(r.then(e,t),Promise.resolve()):r},e=function(e,t,r){e=o.apply(this,[e]);return r?(e.then(t,r),Promise.resolve()):e},t.setLocalDescription=e,e=function(e,t,r){e=a.apply(this,[e]);return r?(e.then(t,r),Promise.resolve()):e},t.setRemoteDescription=e,e=function(e,t,r){e=s.apply(this,[e]);return r?(e.then(t,r),Promise.resolve()):e},t.addIceCandidate=e)},r.shimGetUserMedia=function(e){var n=e&&e.navigator;{var t;n.mediaDevices&&n.mediaDevices.getUserMedia&&(e=n.mediaDevices,t=e.getUserMedia.bind(e),n.mediaDevices.getUserMedia=function(e){return t(i(e))})}!n.getUserMedia&&n.mediaDevices&&n.mediaDevices.getUserMedia&&(n.getUserMedia=function(e,t,r){n.mediaDevices.getUserMedia(e).then(t,r)}.bind(n))},r.shimConstraints=i,r.shimRTCIceServerUrls=function(e){var o;e.RTCPeerConnection&&(o=e.RTCPeerConnection,e.RTCPeerConnection=function(e,t){if(e&&e.iceServers){for(var r=[],n=0;n<e.iceServers.length;n++){var i=e.iceServers[n];!i.hasOwnProperty("urls")&&i.hasOwnProperty("url")?(a.deprecated("RTCIceServer.url","RTCIceServer.urls"),(i=JSON.parse(JSON.stringify(i))).urls=i.url,delete i.url,r.push(i)):r.push(e.iceServers[n])}e.iceServers=r}return new o(e,t)},e.RTCPeerConnection.prototype=o.prototype,"generateCertificate"in o&&Object.defineProperty(e.RTCPeerConnection,"generateCertificate",{get:function(){return o.generateCertificate}}))},r.shimTrackEventTransceiver=function(e){"object"===(void 0===e?"undefined":c(e))&&e.RTCTrackEvent&&"receiver"in e.RTCTrackEvent.prototype&&!("transceiver"in e.RTCTrackEvent.prototype)&&Object.defineProperty(e.RTCTrackEvent.prototype,"transceiver",{get:function(){return{receiver:this.receiver}}})},r.shimCreateOfferLegacy=function(e){var r=e.RTCPeerConnection.prototype.createOffer;e.RTCPeerConnection.prototype.createOffer=function(e){var t;return e&&(void 0!==e.offerToReceiveAudio&&(e.offerToReceiveAudio=!!e.offerToReceiveAudio),t=this.getTransceivers().find(function(e){return"audio"===e.receiver.track.kind}),!1===e.offerToReceiveAudio&&t?"sendrecv"===t.direction?t.setDirection?t.setDirection("sendonly"):t.direction="sendonly":"recvonly"===t.direction&&(t.setDirection?t.setDirection("inactive"):t.direction="inactive"):!0!==e.offerToReceiveAudio||t||this.addTransceiver("audio",{direction:"recvonly"}),void 0!==e.offerToReceiveVideo&&(e.offerToReceiveVideo=!!e.offerToReceiveVideo),t=this.getTransceivers().find(function(e){return"video"===e.receiver.track.kind}),!1===e.offerToReceiveVideo&&t?"sendrecv"===t.direction?t.setDirection?t.setDirection("sendonly"):t.direction="sendonly":"recvonly"===t.direction&&(t.setDirection?t.setDirection("inactive"):t.direction="inactive"):!0!==e.offerToReceiveVideo||t||this.addTransceiver("video",{direction:"recvonly"})),r.apply(this,arguments)}},r.shimAudioContext=function(e){"object"!==(void 0===e?"undefined":c(e))||e.AudioContext||(e.AudioContext=e.webkitAudioContext)};var a=function(e){{if(e&&e.__esModule)return e;var t={};if(null!=e)for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&(t[r]=e[r]);return t.default=e,t}}(e("../utils"));function i(e){return e&&void 0!==e.video?Object.assign({},e,{video:a.compactObject(e.video)}):e}},{"../utils":11}],11:[function(e,t,r){"use strict";Object.defineProperty(r,"__esModule",{value:!0});var n="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};function a(e,t,r){return t in e?Object.defineProperty(e,t,{value:r,enumerable:!0,configurable:!0,writable:!0}):e[t]=r,e}r.extractVersion=s,r.wrapPeerConnectionEvent=function(e,n,i){var o,a;e.RTCPeerConnection&&(e=e.RTCPeerConnection.prototype,o=e.addEventListener,e.addEventListener=function(e,t){if(e!==n)return o.apply(this,arguments);function r(e){(e=i(e))&&(t.handleEvent?t.handleEvent(e):t(e))}return this._eventMap=this._eventMap||{},this._eventMap[n]||(this._eventMap[n]=new Map),this._eventMap[n].set(t,r),o.apply(this,[e,r])},a=e.removeEventListener,e.removeEventListener=function(e,t){if(e!==n||!this._eventMap||!this._eventMap[n])return a.apply(this,arguments);if(!this._eventMap[n].has(t))return a.apply(this,arguments);var r=this._eventMap[n].get(t);return this._eventMap[n].delete(t),0===this._eventMap[n].size&&delete this._eventMap[n],0===Object.keys(this._eventMap).length&&delete this._eventMap,a.apply(this,[e,r])},Object.defineProperty(e,"on"+n,{get:function(){return this["_on"+n]},set:function(e){this["_on"+n]&&(this.removeEventListener(n,this["_on"+n]),delete this["_on"+n]),e&&this.addEventListener(n,this["_on"+n]=e)},enumerable:!0,configurable:!0}))},r.disableLog=function(e){return"boolean"==typeof e?(i=e)?"adapter.js logging disabled":"adapter.js logging enabled":new Error("Argument type: "+(void 0===e?"undefined":n(e))+". Please use a boolean.")},r.disableWarnings=function(e){return"boolean"==typeof e?(o=!e,"adapter.js deprecation warnings "+(e?"disabled":"enabled")):new Error("Argument type: "+(void 0===e?"undefined":n(e))+". Please use a boolean.")},r.log=function(){"object"===("undefined"==typeof window?"undefined":n(window))&&(i||"undefined"!=typeof console&&"function"==typeof console.log&&console.log.apply(console,arguments))},r.deprecated=function(e,t){o&&console.warn(e+" is deprecated, please use "+t+" instead.")},r.detectBrowser=function(e){var t={browser:null,version:null};if(void 0===e||!e.navigator)return t.browser="Not a browser.",t;var r=e.navigator;if(r.mozGetUserMedia)t.browser="firefox",t.version=s(r.userAgent,/Firefox\/(\d+)\./,1);else if(r.webkitGetUserMedia||!1===e.isSecureContext&&e.webkitRTCPeerConnection&&!e.RTCIceGatherer)t.browser="chrome",t.version=s(r.userAgent,/Chrom(e|ium)\/(\d+)\./,2);else{if(!e.RTCPeerConnection||!r.userAgent.match(/AppleWebKit\/(\d+)\./))return t.browser="Not a supported browser.",t;t.browser="safari",t.version=s(r.userAgent,/AppleWebKit\/(\d+)\./,1),t.supportsUnifiedPlan=e.RTCRtpTransceiver&&"currentDirection"in e.RTCRtpTransceiver.prototype}return t},r.compactObject=function i(o){if(!c(o))return o;return Object.keys(o).reduce(function(e,t){var r=c(o[t]),n=r?i(o[t]):o[t],r=r&&!Object.keys(n).length;return void 0===n||r?e:Object.assign(e,a({},t,n))},{})},r.walkStats=p,r.filterStats=function(r,t,e){var n=e?"outbound-rtp":"inbound-rtp",i=new Map;if(null===t)return i;var o=[];return r.forEach(function(e){"track"===e.type&&e.trackIdentifier===t.id&&o.push(e)}),o.forEach(function(t){r.forEach(function(e){e.type===n&&e.trackId===t.id&&p(r,e,i)})}),i};var i=!0,o=!0;function s(e,t,r){t=e.match(t);return t&&t.length>=r&&parseInt(t[r],10)}function c(e){return"[object Object]"===Object.prototype.toString.call(e)}function p(t,r,n){r&&!n.has(r.id)&&(n.set(r.id,r),Object.keys(r).forEach(function(e){e.endsWith("Id")?p(t,t.get(r[e]),n):e.endsWith("Ids")&&r[e].forEach(function(e){p(t,t.get(e),n)})}))}},{}],12:[function(e,t,r){"use strict";var n="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},p={generateIdentifier:function(){return Math.random().toString(36).substr(2,10)}};p.localCName=p.generateIdentifier(),p.splitLines=function(e){return e.trim().split("\n").map(function(e){return e.trim()})},p.splitSections=function(e){return e.split("\nm=").map(function(e,t){return(0<t?"m="+e:e).trim()+"\r\n"})},p.getDescription=function(e){e=p.splitSections(e);return e&&e[0]},p.getMediaSections=function(e){e=p.splitSections(e);return e.shift(),e},p.matchPrefix=function(e,t){return p.splitLines(e).filter(function(e){return 0===e.indexOf(t)})},p.parseCandidate=function(e){for(var t=void 0,r={foundation:(t=(0===e.indexOf("a=candidate:")?e.substring(12):e.substring(10)).split(" "))[0],component:{1:"rtp",2:"rtcp"}[t[1]]||t[1],protocol:t[2].toLowerCase(),priority:parseInt(t[3],10),ip:t[4],address:t[4],port:parseInt(t[5],10),type:t[7]},n=8;n<t.length;n+=2)switch(t[n]){case"raddr":r.relatedAddress=t[n+1];break;case"rport":r.relatedPort=parseInt(t[n+1],10);break;case"tcptype":r.tcpType=t[n+1];break;case"ufrag":r.ufrag=t[n+1],r.usernameFragment=t[n+1];break;default:void 0===r[t[n]]&&(r[t[n]]=t[n+1])}return r},p.writeCandidate=function(e){var t=[];t.push(e.foundation);var r=e.component;"rtp"===r?t.push(1):"rtcp"===r?t.push(2):t.push(r),t.push(e.protocol.toUpperCase()),t.push(e.priority),t.push(e.address||e.ip),t.push(e.port);r=e.type;return t.push("typ"),t.push(r),"host"!==r&&e.relatedAddress&&e.relatedPort&&(t.push("raddr"),t.push(e.relatedAddress),t.push("rport"),t.push(e.relatedPort)),e.tcpType&&"tcp"===e.protocol.toLowerCase()&&(t.push("tcptype"),t.push(e.tcpType)),(e.usernameFragment||e.ufrag)&&(t.push("ufrag"),t.push(e.usernameFragment||e.ufrag)),"candidate:"+t.join(" ")},p.parseIceOptions=function(e){return e.substr(14).split(" ")},p.parseRtpMap=function(e){var t=e.substr(9).split(" "),e={payloadType:parseInt(t.shift(),10)},t=t[0].split("/");return e.name=t[0],e.clockRate=parseInt(t[1],10),e.channels=3===t.length?parseInt(t[2],10):1,e.numChannels=e.channels,e},p.writeRtpMap=function(e){var t=e.payloadType;void 0!==e.preferredPayloadType&&(t=e.preferredPayloadType);var r=e.channels||e.numChannels||1;return"a=rtpmap:"+t+" "+e.name+"/"+e.clockRate+(1!==r?"/"+r:"")+"\r\n"},p.parseExtmap=function(e){e=e.substr(9).split(" ");return{id:parseInt(e[0],10),direction:0<e[0].indexOf("/")?e[0].split("/")[1]:"sendrecv",uri:e[1]}},p.writeExtmap=function(e){return"a=extmap:"+(e.id||e.preferredId)+(e.direction&&"sendrecv"!==e.direction?"/"+e.direction:"")+" "+e.uri+"\r\n"},p.parseFmtp=function(e){for(var t={},r=void 0,n=e.substr(e.indexOf(" ")+1).split(";"),i=0;i<n.length;i++)t[(r=n[i].trim().split("="))[0].trim()]=r[1];return t},p.writeFmtp=function(t){var r,e="",n=t.payloadType;return void 0!==t.preferredPayloadType&&(n=t.preferredPayloadType),t.parameters&&Object.keys(t.parameters).length&&(r=[],Object.keys(t.parameters).forEach(function(e){void 0!==t.parameters[e]?r.push(e+"="+t.parameters[e]):r.push(e)}),e+="a=fmtp:"+n+" "+r.join(";")+"\r\n"),e},p.parseRtcpFb=function(e){e=e.substr(e.indexOf(" ")+1).split(" ");return{type:e.shift(),parameter:e.join(" ")}},p.writeRtcpFb=function(e){var t="",r=e.payloadType;return void 0!==e.preferredPayloadType&&(r=e.preferredPayloadType),e.rtcpFeedback&&e.rtcpFeedback.length&&e.rtcpFeedback.forEach(function(e){t+="a=rtcp-fb:"+r+" "+e.type+(e.parameter&&e.parameter.length?" "+e.parameter:"")+"\r\n"}),t},p.parseSsrcMedia=function(e){var t=e.indexOf(" "),r={ssrc:parseInt(e.substr(7,t-7),10)},n=e.indexOf(":",t);return-1<n?(r.attribute=e.substr(t+1,n-t-1),r.value=e.substr(n+1)):r.attribute=e.substr(t+1),r},p.parseSsrcGroup=function(e){e=e.substr(13).split(" ");return{semantics:e.shift(),ssrcs:e.map(function(e){return parseInt(e,10)})}},p.getMid=function(e){e=p.matchPrefix(e,"a=mid:")[0];if(e)return e.substr(6)},p.parseFingerprint=function(e){e=e.substr(14).split(" ");return{algorithm:e[0].toLowerCase(),value:e[1].toUpperCase()}},p.getDtlsParameters=function(e,t){return{role:"auto",fingerprints:p.matchPrefix(e+t,"a=fingerprint:").map(p.parseFingerprint)}},p.writeDtlsParameters=function(e,t){var r="a=setup:"+t+"\r\n";return e.fingerprints.forEach(function(e){r+="a=fingerprint:"+e.algorithm+" "+e.value+"\r\n"}),r},p.parseCryptoLine=function(e){e=e.substr(9).split(" ");return{tag:parseInt(e[0],10),cryptoSuite:e[1],keyParams:e[2],sessionParams:e.slice(3)}},p.writeCryptoLine=function(e){return"a=crypto:"+e.tag+" "+e.cryptoSuite+" "+("object"===n(e.keyParams)?p.writeCryptoKeyParams(e.keyParams):e.keyParams)+(e.sessionParams?" "+e.sessionParams.join(" "):"")+"\r\n"},p.parseCryptoKeyParams=function(e){if(0!==e.indexOf("inline:"))return null;e=e.substr(7).split("|");return{keyMethod:"inline",keySalt:e[0],lifeTime:e[1],mkiValue:e[2]?e[2].split(":")[0]:void 0,mkiLength:e[2]?e[2].split(":")[1]:void 0}},p.writeCryptoKeyParams=function(e){return e.keyMethod+":"+e.keySalt+(e.lifeTime?"|"+e.lifeTime:"")+(e.mkiValue&&e.mkiLength?"|"+e.mkiValue+":"+e.mkiLength:"")},p.getCryptoParameters=function(e,t){return p.matchPrefix(e+t,"a=crypto:").map(p.parseCryptoLine)},p.getIceParameters=function(e,t){var r=p.matchPrefix(e+t,"a=ice-ufrag:")[0],t=p.matchPrefix(e+t,"a=ice-pwd:")[0];return r&&t?{usernameFragment:r.substr(12),password:t.substr(10)}:null},p.writeIceParameters=function(e){var t="a=ice-ufrag:"+e.usernameFragment+"\r\na=ice-pwd:"+e.password+"\r\n";return e.iceLite&&(t+="a=ice-lite\r\n"),t},p.parseRtpParameters=function(e){for(var t={codecs:[],headerExtensions:[],fecMechanisms:[],rtcp:[]},r=p.splitLines(e)[0].split(" "),n=3;n<r.length;n++){var i=r[n],o=p.matchPrefix(e,"a=rtpmap:"+i+" ")[0];if(o){var a=p.parseRtpMap(o),o=p.matchPrefix(e,"a=fmtp:"+i+" ");switch(a.parameters=o.length?p.parseFmtp(o[0]):{},a.rtcpFeedback=p.matchPrefix(e,"a=rtcp-fb:"+i+" ").map(p.parseRtcpFb),t.codecs.push(a),a.name.toUpperCase()){case"RED":case"ULPFEC":t.fecMechanisms.push(a.name.toUpperCase())}}}return p.matchPrefix(e,"a=extmap:").forEach(function(e){t.headerExtensions.push(p.parseExtmap(e))}),t},p.writeRtpDescription=function(e,t){var r="";r+="m="+e+" ",r+=0<t.codecs.length?"9":"0",r+=" UDP/TLS/RTP/SAVPF ",r+=t.codecs.map(function(e){return void 0!==e.preferredPayloadType?e.preferredPayloadType:e.payloadType}).join(" ")+"\r\n",r+="c=IN IP4 0.0.0.0\r\n",r+="a=rtcp:9 IN IP4 0.0.0.0\r\n",t.codecs.forEach(function(e){r+=p.writeRtpMap(e),r+=p.writeFmtp(e),r+=p.writeRtcpFb(e)});var n=0;return t.codecs.forEach(function(e){e.maxptime>n&&(n=e.maxptime)}),0<n&&(r+="a=maxptime:"+n+"\r\n"),t.headerExtensions&&t.headerExtensions.forEach(function(e){r+=p.writeExtmap(e)}),r},p.parseRtpEncodingParameters=function(e){var t=[],r=p.parseRtpParameters(e),n=-1!==r.fecMechanisms.indexOf("RED"),i=-1!==r.fecMechanisms.indexOf("ULPFEC"),o=p.matchPrefix(e,"a=ssrc:").map(function(e){return p.parseSsrcMedia(e)}).filter(function(e){return"cname"===e.attribute}),a=0<o.length&&o[0].ssrc,s=void 0,o=p.matchPrefix(e,"a=ssrc-group:FID").map(function(e){return e.substr(17).split(" ").map(function(e){return parseInt(e,10)})});0<o.length&&1<o[0].length&&o[0][0]===a&&(s=o[0][1]),r.codecs.forEach(function(e){"RTX"===e.name.toUpperCase()&&e.parameters.apt&&(e={ssrc:a,codecPayloadType:parseInt(e.parameters.apt,10)},a&&s&&(e.rtx={ssrc:s}),t.push(e),n&&((e=JSON.parse(JSON.stringify(e))).fec={ssrc:a,mechanism:i?"red+ulpfec":"red"},t.push(e)))}),0===t.length&&a&&t.push({ssrc:a});var c=p.matchPrefix(e,"b=");return c.length&&(c=0===c[0].indexOf("b=TIAS:")?parseInt(c[0].substr(7),10):0===c[0].indexOf("b=AS:")?1e3*parseInt(c[0].substr(5),10)*.95-16e3:void 0,t.forEach(function(e){e.maxBitrate=c})),t},p.parseRtcpParameters=function(e){var t={},r=p.matchPrefix(e,"a=ssrc:").map(function(e){return p.parseSsrcMedia(e)}).filter(function(e){return"cname"===e.attribute})[0];r&&(t.cname=r.value,t.ssrc=r.ssrc);r=p.matchPrefix(e,"a=rtcp-rsize");t.reducedSize=0<r.length,t.compound=0===r.length;e=p.matchPrefix(e,"a=rtcp-mux");return t.mux=0<e.length,t},p.writeRtcpParameters=function(e){var t="";return e.reducedSize&&(t+="a=rtcp-rsize\r\n"),e.mux&&(t+="a=rtcp-mux\r\n"),void 0!==e.ssrc&&e.cname&&(t+="a=ssrc:"+e.ssrc+" cname:"+e.cname+"\r\n"),t},p.parseMsid=function(e){var t=void 0,r=p.matchPrefix(e,"a=msid:");if(1===r.length)return{stream:(t=r[0].substr(7).split(" "))[0],track:t[1]};e=p.matchPrefix(e,"a=ssrc:").map(function(e){return p.parseSsrcMedia(e)}).filter(function(e){return"msid"===e.attribute});return 0<e.length?{stream:(t=e[0].value.split(" "))[0],track:t[1]}:void 0},p.parseSctpDescription=function(e){var t=p.parseMLine(e),r=p.matchPrefix(e,"a=max-message-size:"),n=void 0;0<r.length&&(n=parseInt(r[0].substr(19),10)),isNaN(n)&&(n=65536);r=p.matchPrefix(e,"a=sctp-port:");if(0<r.length)return{port:parseInt(r[0].substr(12),10),protocol:t.fmt,maxMessageSize:n};e=p.matchPrefix(e,"a=sctpmap:");if(0<e.length){e=e[0].substr(10).split(" ");return{port:parseInt(e[0],10),protocol:e[1],maxMessageSize:n}}},p.writeSctpDescription=function(e,t){var r=[],r="DTLS/SCTP"!==e.protocol?["m="+e.kind+" 9 "+e.protocol+" "+t.protocol+"\r\n","c=IN IP4 0.0.0.0\r\n","a=sctp-port:"+t.port+"\r\n"]:["m="+e.kind+" 9 "+e.protocol+" "+t.port+"\r\n","c=IN IP4 0.0.0.0\r\n","a=sctpmap:"+t.port+" "+t.protocol+" 65535\r\n"];return void 0!==t.maxMessageSize&&r.push("a=max-message-size:"+t.maxMessageSize+"\r\n"),r.join("")},p.generateSessionId=function(){return Math.random().toString().substr(2,21)},p.writeSessionBoilerplate=function(e,t,r){t=void 0!==t?t:2;return"v=0\r\no="+(r||"thisisadapterortc")+" "+(e||p.generateSessionId())+" "+t+" IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\n"},p.getDirection=function(e,t){for(var r=p.splitLines(e),n=0;n<r.length;n++)switch(r[n]){case"a=sendrecv":case"a=sendonly":case"a=recvonly":case"a=inactive":return r[n].substr(2)}return t?p.getDirection(t):"sendrecv"},p.getKind=function(e){return p.splitLines(e)[0].split(" ")[0].substr(2)},p.isRejected=function(e){return"0"===e.split(" ",2)[1]},p.parseMLine=function(e){e=p.splitLines(e)[0].substr(2).split(" ");return{kind:e[0],port:parseInt(e[1],10),protocol:e[2],fmt:e.slice(3).join(" ")}},p.parseOLine=function(e){e=p.matchPrefix(e,"o=")[0].substr(2).split(" ");return{username:e[0],sessionId:e[1],sessionVersion:parseInt(e[2],10),netType:e[3],addressType:e[4],address:e[5]}},p.isValidSDP=function(e){if("string"!=typeof e||0===e.length)return!1;for(var t=p.splitLines(e),r=0;r<t.length;r++)if(t[r].length<2||"="!==t[r].charAt(1))return!1;return!0},"object"===(void 0===t?"undefined":n(t))&&(t.exports=p)},{}]},{},[1])(1)});
    """.trimIndent()

    //endregion
    //region private val janusJs = ...
    private val janusJs = """
        "use strict";
        /*
        	The MIT License (MIT)

        	Copyright (c) 2016 Meetecho

        	Permission is hereby granted, free of charge, to any person obtaining
        	a copy of this software and associated documentation files (the "Software"),
        	to deal in the Software without restriction, including without limitation
        	the rights to use, copy, modify, merge, publish, distribute, sublicense,
        	and/or sell copies of the Software, and to permit persons to whom the
        	Software is furnished to do so, subject to the following conditions:

        	The above copyright notice and this permission notice shall be included
        	in all copies or substantial portions of the Software.

        	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
        	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
        	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
        	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
        	ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
        	OTHER DEALINGS IN THE SOFTWARE.
         */

        // List of sessions
        Janus.sessions = {};

        Janus.isExtensionEnabled = function() {
        	if(navigator.mediaDevices && navigator.mediaDevices.getDisplayMedia) {
        		// No need for the extension, getDisplayMedia is supported
        		return true;
        	}
        	if(window.navigator.userAgent.match('Chrome')) {
        		var chromever = parseInt(window.navigator.userAgent.match(/Chrome\/(.*) /)[1], 10);
        		var maxver = 33;
        		if(window.navigator.userAgent.match('Linux'))
        			maxver = 35;	// "known" crash in chrome 34 and 35 on linux
        		if(chromever >= 26 && chromever <= maxver) {
        			// Older versions of Chrome don't support this extension-based approach, so lie
        			return true;
        		}
        		return Janus.extension.isInstalled();
        	} else {
        		// Firefox and others, no need for the extension (but this doesn't mean it will work)
        		return true;
        	}
        };

        var defaultExtension = {
        	// Screensharing Chrome Extension ID
        	extensionId: 'hapfgfdkleiggjjpfpenajgdnfckjpaj',
        	isInstalled: function() { return document.querySelector('#janus-extension-installed') !== null; },
        	getScreen: function (callback) {
        		var pending = window.setTimeout(function () {
        			var error = new Error('NavigatorUserMediaError');
        			error.name = 'The required Chrome extension is not installed: click <a href="#">here</a> to install it. (NOTE: this will need you to refresh the page)';
        			return callback(error);
        		}, 1000);
        		this.cache[pending] = callback;
        		window.postMessage({ type: 'janusGetScreen', id: pending }, '*');
        	},
        	init: function () {
        		var cache = {};
        		this.cache = cache;
        		// Wait for events from the Chrome Extension
        		window.addEventListener('message', function (event) {
        			if(event.origin != window.location.origin)
        				return;
        			if(event.data.type == 'janusGotScreen' && cache[event.data.id]) {
        				var callback = cache[event.data.id];
        				delete cache[event.data.id];

        				if (event.data.sourceId === '') {
        					// user canceled
        					var error = new Error('NavigatorUserMediaError');
        					error.name = 'You cancelled the request for permission, giving up...';
        					callback(error);
        				} else {
        					callback(null, event.data.sourceId);
        				}
        			} else if (event.data.type == 'janusGetScreenPending') {
        				console.log('clearing ', event.data.id);
        				window.clearTimeout(event.data.id);
        			}
        		});
        	}
        };

        Janus.useDefaultDependencies = function (deps) {
        	var f = (deps && deps.fetch) || fetch;
        	var p = (deps && deps.Promise) || Promise;
        	var socketCls = (deps && deps.WebSocket) || WebSocket;

        	return {
        		newWebSocket: function(server, proto) { return new socketCls(server, proto); },
        		extension: (deps && deps.extension) || defaultExtension,
        		isArray: function(arr) { return Array.isArray(arr); },
        		webRTCAdapter: (deps && deps.adapter) || adapter,
        		httpAPICall: function(url, options) {
                    const headers = JSON.parse('$EXTRA_HEAERS_PLACEHOLDER')
                    headers['Accept'] = 'application/json, text/plain, */*'
        			var fetchOptions = {
        				method: options.verb,
        				headers: headers,
        				cache: 'no-cache'
        			};
        			if(options.verb === "POST") {
        				fetchOptions.headers['Content-Type'] = 'application/json';
        			}
        			if(options.withCredentials !== undefined) {
        				fetchOptions.credentials = options.withCredentials === true ? 'include' : (options.withCredentials ? options.withCredentials : 'omit');
        			}
        			if(options.body) {
        				fetchOptions.body = JSON.stringify(options.body);
        			}

        			var fetching = f(url, fetchOptions).catch(function(error) {
        				return p.reject({message: 'Probably a network error, is the server down?', error: error});
        			});

        			/*
        			 * fetch() does not natively support timeouts.
        			 * Work around this by starting a timeout manually, and racing it agains the fetch() to see which thing resolves first.
        			 */

        			if(options.timeout) {
        				var timeout = new p(function(resolve, reject) {
        					var timerId = setTimeout(function() {
        						clearTimeout(timerId);
        						return reject({message: 'Request timed out', timeout: options.timeout});
        					}, options.timeout);
        				});
        				fetching = p.race([fetching, timeout]);
        			}

        			fetching.then(function(response) {
        				if(response.ok) {
        					if(typeof(options.success) === typeof(Janus.noop)) {
        						return response.json().then(function(parsed) {
        							try {
        								options.success(parsed);
        							} catch(error) {
        								Janus.error('Unhandled httpAPICall success callback error', error);
        							}
        						}, function(error) {
        							return p.reject({message: 'Failed to parse response body', error: error, response: response});
        						});
        					}
        				}
        				else {
        					return p.reject({message: 'API call failed', response: JSON.stringify(response)});
        				}
        			}).catch(function(error) {
        				if(typeof(options.error) === typeof(Janus.noop)) {
        					options.error(error.message || '<< internal error >>', error);
        				}
        			});

        			return fetching;
        		}
        	}
        };

        Janus.useOldDependencies = function (deps) {
        	var jq = (deps && deps.jQuery) || jQuery;
        	var socketCls = (deps && deps.WebSocket) || WebSocket;
        	return {
        		newWebSocket: function(server, proto) { return new socketCls(server, proto); },
        		isArray: function(arr) { return jq.isArray(arr); },
        		extension: (deps && deps.extension) || defaultExtension,
        		webRTCAdapter: (deps && deps.adapter) || adapter,
        		httpAPICall: function(url, options) {
        			var payload = options.body !== undefined ? {
        				contentType: 'application/json',
        				data: JSON.stringify(options.body)
        			} : {};
        			var credentials = options.withCredentials !== undefined ? {xhrFields: {withCredentials: options.withCredentials}} : {};

        			return jq.ajax(jq.extend(payload, credentials, {
        				url: url,
        				type: options.verb,
        				cache: false,
        				dataType: 'json',
        				async: options.async,
        				timeout: options.timeout,
        				success: function(result) {
        					if(typeof(options.success) === typeof(Janus.noop)) {
        						options.success(result);
        					}
        				},
        				error: function(xhr, status, err) {
        					if(typeof(options.error) === typeof(Janus.noop)) {
        						options.error(status, err);
        					}
        				}
        			}));
        		}
        	};
        };

        Janus.noop = function() {};

        Janus.dataChanDefaultLabel = "JanusDataChannel";

        // Note: in the future we may want to change this, e.g., as was
        // attempted in https://github.com/meetecho/janus-gateway/issues/1670
        Janus.endOfCandidates = null;

        // Stop all tracks from a given stream
        Janus.stopAllTracks = function(stream) {
        	try {
        		// Try a MediaStreamTrack.stop() for each track
        		var tracks = stream.getTracks();
        		for(var mst of tracks) {
        			console.log(mst);
        			if(mst) {
        				mst.stop();
        			}
        		}
        	} catch(e) {
        		// Do nothing if this fails
        	}
        }

        // Initialization
        Janus.init = function(options) {
        	options = options || {};
        	options.callback = (typeof options.callback == "function") ? options.callback : Janus.noop;
        	if(Janus.initDone) {
        		// Already initialized
        		options.callback();
        	} else {
        		if(typeof console.log == "undefined") {
        			console.log = function() {};
        		}
        		// Console logging (all debugging disabled by default)
        		Janus.trace = Janus.noop;
        		Janus.vdebug = Janus.noop;
        		Janus.error = Janus.noop;
        		if(options.debug === true || options.debug === "all") {
        			// Enable all debugging levels
        			Janus.trace = console.trace.bind(console);
        			Janus.vdebug = console.debug.bind(console);
        			Janus.error = console.error.bind(console);
        		} else if(Array.isArray(options.debug)) {
        			for(var d of options.debug) {
        				switch(d) {
        					case "trace":
        						Janus.trace = console.trace.bind(console);
        						break;
        					case "debug":
        						console.debug = console.debug.bind(console);
        						break;
        					case "vdebug":
        						Janus.vdebug = console.debug.bind(console);
        						break;
        					case "log":
        						console.log = console.log.bind(console);
        						break;
        					case "warn":
        						console.warn = console.warn.bind(console);
        						break;
        					case "error":
        						Janus.error = console.error.bind(console);
        						break;
        					default:
        						console.error("Unknown debugging option '" + d + "' (supported: 'trace', 'debug', 'vdebug', 'log', warn', 'error')");
        						break;
        				}
        			}
        		}
        		console.log("Initializing library");

        		var usedDependencies = options.dependencies || Janus.useDefaultDependencies();
        		Janus.isArray = usedDependencies.isArray;
        		Janus.webRTCAdapter = usedDependencies.webRTCAdapter;
        		Janus.httpAPICall = usedDependencies.httpAPICall;
        		Janus.newWebSocket = usedDependencies.newWebSocket;
        		Janus.extension = usedDependencies.extension;
        		Janus.extension.init();

        		// Helper method to enumerate devices
        		Janus.listDevices = function(callback, config) {
        			callback = (typeof callback == "function") ? callback : Janus.noop;
        			if (config == null) config = { audio: true, video: true };
        			if(Janus.isGetUserMediaAvailable()) {
        				navigator.mediaDevices.getUserMedia(config)
        				.then(function(stream) {
        					navigator.mediaDevices.enumerateDevices().then(function(devices) {
        						console.debug(devices);
        						callback(devices);
        						// Get rid of the now useless stream
        						Janus.stopAllTracks(stream)
        					});
        				})
        				.catch(function(err) {
        					Janus.error(err);
        					callback([]);
        				});
        			} else {
        				console.warn("navigator.mediaDevices unavailable");
        				callback([]);
        			}
        		};
        		// Helper methods to attach/reattach a stream to a video element (previously part of adapter.js)
        		Janus.attachMediaStream = function(element, stream) {
        			try {
        				element.srcObject = stream;
        			} catch (e) {
        				try {
        					element.src = URL.createObjectURL(stream);
        				} catch (e) {
        					Janus.error("Error attaching stream to element");
        				}
        			}
        		};
        		Janus.reattachMediaStream = function(to, from) {
        			try {
        				to.srcObject = from.srcObject;
        			} catch (e) {
        				try {
        					to.src = from.src;
        				} catch (e) {
        					Janus.error("Error reattaching stream to element");
        				}
        			}
        		};
        		// Detect tab close: make sure we don't loose existing onbeforeunload handlers
        		// (note: for iOS we need to subscribe to a different event, 'pagehide', see
        		// https://gist.github.com/thehunmonkgroup/6bee8941a49b86be31a787fe8f4b8cfe)
        		var iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;
        		var eventName = iOS ? 'pagehide' : 'beforeunload';
        		var oldOBF = window["on" + eventName];
        		window.addEventListener(eventName, function() {
        			console.log("Closing window");
        			for(var s in Janus.sessions) {
        				if(Janus.sessions[s] && Janus.sessions[s].destroyOnUnload) {
        					console.log("Destroying session " + s);
        					Janus.sessions[s].destroy({unload: true, notifyDestroyed: false});
        				}
        			}
        			if(oldOBF && typeof oldOBF == "function") {
        				oldOBF();
        			}
        		});
        		// If this is a Safari Technology Preview, check if VP8 is supported
        		Janus.safariVp8 = false;
        		if(Janus.webRTCAdapter.browserDetails.browser === 'safari' &&
        				Janus.webRTCAdapter.browserDetails.version >= 605) {
        			// Let's see if RTCRtpSender.getCapabilities() is there
        			if(RTCRtpSender && RTCRtpSender.getCapabilities && RTCRtpSender.getCapabilities("video") &&
        					RTCRtpSender.getCapabilities("video").codecs && RTCRtpSender.getCapabilities("video").codecs.length) {
        				for(var codec of RTCRtpSender.getCapabilities("video").codecs) {
        					if(codec && codec.mimeType && codec.mimeType.toLowerCase() === "video/vp8") {
        						Janus.safariVp8 = true;
        						break;
        					}
        				}
        				if(Janus.safariVp8) {
        					console.log("This version of Safari supports VP8");
        				} else {
        					console.warn("This version of Safari does NOT support VP8: if you're using a Technology Preview, " +
        						"try enabling the 'WebRTC VP8 codec' setting in the 'Experimental Features' Develop menu");
        				}
        			} else {
        				// We do it in a very ugly way, as there's no alternative...
        				// We create a PeerConnection to see if VP8 is in an offer
        				var testpc = new RTCPeerConnection({});
        				testpc.createOffer({offerToReceiveVideo: true}).then(function(offer) {
        					Janus.safariVp8 = offer.sdp.indexOf("VP8") !== -1;
        					if(Janus.safariVp8) {
        						console.log("This version of Safari supports VP8");
        					} else {
        						console.warn("This version of Safari does NOT support VP8: if you're using a Technology Preview, " +
        							"try enabling the 'WebRTC VP8 codec' setting in the 'Experimental Features' Develop menu");
        					}
        					testpc.close();
        					testpc = null;
        				});
        			}
        		}
        		// Check if this browser supports Unified Plan and transceivers
        		// Based on https://codepen.io/anon/pen/ZqLwWV?editors=0010
        		Janus.unifiedPlan = false;
        		if(Janus.webRTCAdapter.browserDetails.browser === 'firefox' &&
        				Janus.webRTCAdapter.browserDetails.version >= 59) {
        			// Firefox definitely does, starting from version 59
        			Janus.unifiedPlan = true;
        		} else if(Janus.webRTCAdapter.browserDetails.browser === 'chrome' &&
        				Janus.webRTCAdapter.browserDetails.version >= 72) {
        			// Chrome does, but it's only usable from version 72 on
        			Janus.unifiedPlan = true;
        		} else if(!window.RTCRtpTransceiver || !('currentDirection' in RTCRtpTransceiver.prototype)) {
        			// Safari supports addTransceiver() but not Unified Plan when
        			// currentDirection is not defined (see codepen above).
        			Janus.unifiedPlan = false;
        		} else {
        			// Check if addTransceiver() throws an exception
        			var tempPc = new RTCPeerConnection();
        			try {
        				tempPc.addTransceiver('audio');
        				Janus.unifiedPlan = true;
        			} catch (e) {}
        			tempPc.close();
        		}
        		Janus.initDone = true;
        		options.callback();
        	}
        };

        // Helper method to check whether WebRTC is supported by this browser
        Janus.isWebrtcSupported = function() {
        	return !!window.RTCPeerConnection;
        };
        // Helper method to check whether devices can be accessed by this browser (e.g., not possible via plain HTTP)
        Janus.isGetUserMediaAvailable = function() {
        	return navigator.mediaDevices && navigator.mediaDevices.getUserMedia;
        };

        // Helper method to create random identifiers (e.g., transaction)
        Janus.randomString = function(len) {
        	var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        	var randomString = '';
        	for (var i = 0; i < len; i++) {
        		var randomPoz = Math.floor(Math.random() * charSet.length);
        		randomString += charSet.substring(randomPoz,randomPoz+1);
        	}
        	return randomString;
        };

        function Janus(gatewayCallbacks) {
        	gatewayCallbacks = gatewayCallbacks || {};
        	gatewayCallbacks.success = (typeof gatewayCallbacks.success == "function") ? gatewayCallbacks.success : Janus.noop;
        	gatewayCallbacks.error = (typeof gatewayCallbacks.error == "function") ? gatewayCallbacks.error : Janus.noop;
        	gatewayCallbacks.destroyed = (typeof gatewayCallbacks.destroyed == "function") ? gatewayCallbacks.destroyed : Janus.noop;
        	if(!Janus.initDone) {
        		gatewayCallbacks.error("Library not initialized");
        		return {};
        	}
        	if(!Janus.isWebrtcSupported()) {
        		gatewayCallbacks.error("WebRTC not supported by this browser");
        		return {};
        	}
        	console.log("Library initialized: " + Janus.initDone);
        	if(!gatewayCallbacks.server) {
        		gatewayCallbacks.error("Invalid server url");
        		return {};
        	}
        	var websockets = false;
        	var ws = null;
        	var wsHandlers = {};
        	var wsKeepaliveTimeoutId = null;
        	var servers = null;
        	var serversIndex = 0;
        	var server = gatewayCallbacks.server;
        	if(Janus.isArray(server)) {
        		console.log("Multiple servers provided (" + server.length + "), will use the first that works");
        		server = null;
        		servers = gatewayCallbacks.server;
        		console.debug(servers);
        	} else {
        		if(server.indexOf("ws") === 0) {
        			websockets = true;
        			console.log("Using WebSockets to contact Janus: " + server);
        		} else {
        			websockets = false;
        			console.log("Using REST API to contact Janus: " + server);
        		}
        	}
        	var iceServers = gatewayCallbacks.iceServers || [{urls: "stun:stun.l.google.com:19302"}];
        	var iceTransportPolicy = gatewayCallbacks.iceTransportPolicy;
        	var bundlePolicy = gatewayCallbacks.bundlePolicy;
        	// Whether IPv6 candidates should be gathered
        	var ipv6Support = (gatewayCallbacks.ipv6 === true);
        	// Whether we should enable the withCredentials flag for XHR requests
        	var withCredentials = false;
        	if(gatewayCallbacks.withCredentials !== undefined && gatewayCallbacks.withCredentials !== null)
        		withCredentials = gatewayCallbacks.withCredentials === true;
        	// Optional max events
        	var maxev = 10;
        	if(gatewayCallbacks.max_poll_events !== undefined && gatewayCallbacks.max_poll_events !== null)
        		maxev = gatewayCallbacks.max_poll_events;
        	if(maxev < 1)
        		maxev = 1;
        	// Token to use (only if the token based authentication mechanism is enabled)
        	var token = null;
        	if(gatewayCallbacks.token !== undefined && gatewayCallbacks.token !== null)
        		token = gatewayCallbacks.token;
        	// API secret to use (only if the shared API secret is enabled)
        	var apisecret = null;
        	if(gatewayCallbacks.apisecret !== undefined && gatewayCallbacks.apisecret !== null)
        		apisecret = gatewayCallbacks.apisecret;
        	// Whether we should destroy this session when onbeforeunload is called
        	this.destroyOnUnload = true;
        	if(gatewayCallbacks.destroyOnUnload !== undefined && gatewayCallbacks.destroyOnUnload !== null)
        		this.destroyOnUnload = (gatewayCallbacks.destroyOnUnload === true);
        	// Some timeout-related values
        	var keepAlivePeriod = 25000;
        	if(gatewayCallbacks.keepAlivePeriod !== undefined && gatewayCallbacks.keepAlivePeriod !== null)
        		keepAlivePeriod = gatewayCallbacks.keepAlivePeriod;
        	if(isNaN(keepAlivePeriod))
        		keepAlivePeriod = 25000;
        	var longPollTimeout = 60000;
        	if(gatewayCallbacks.longPollTimeout !== undefined && gatewayCallbacks.longPollTimeout !== null)
        		longPollTimeout = gatewayCallbacks.longPollTimeout;
        	if(isNaN(longPollTimeout))
        		longPollTimeout = 60000;

        	// overrides for default maxBitrate values for simulcasting
        	function getMaxBitrates(simulcastMaxBitrates) {
        		var maxBitrates = {
        			high: 900000,
        			medium: 300000,
        			low: 100000,
        		};

        		if (simulcastMaxBitrates !== undefined && simulcastMaxBitrates !== null) {
        			if (simulcastMaxBitrates.high)
        				maxBitrates.high = simulcastMaxBitrates.high;
        			if (simulcastMaxBitrates.medium)
        				maxBitrates.medium = simulcastMaxBitrates.medium;
        			if (simulcastMaxBitrates.low)
        				maxBitrates.low = simulcastMaxBitrates.low;
        		}

        		return maxBitrates;
        	}

        	var connected = false;
        	var sessionId = null;
        	var pluginHandles = {};
        	var that = this;
        	var retries = 0;
        	var transactions = {};
        	createSession(gatewayCallbacks);

        	// Public methods
        	this.getServer = function() { return server; };
        	this.isConnected = function() { return connected; };
        	this.reconnect = function(callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		callbacks["reconnect"] = true;
        		createSession(callbacks);
        	};
        	this.getSessionId = function() { return sessionId; };
        	this.getInfo = function(callbacks) { getInfo(callbacks); };
        	this.destroy = function(callbacks) { destroySession(callbacks); };
        	this.attach = function(callbacks) { createHandle(callbacks); };

        	function eventHandler() {
        		if(sessionId == null)
        			return;
        		console.debug('Long poll...');
        		if(!connected) {
        			console.warn("Is the server down? (connected=false)");
        			return;
        		}
        		var longpoll = server + "/" + sessionId + "?rid=" + new Date().getTime();
        		if(maxev)
        			longpoll = longpoll + "&maxev=" + maxev;
        		if(token)
        			longpoll = longpoll + "&token=" + encodeURIComponent(token);
        		if(apisecret)
        			longpoll = longpoll + "&apisecret=" + encodeURIComponent(apisecret);
        		Janus.httpAPICall(longpoll, {
        			verb: 'GET',
        			withCredentials: withCredentials,
        			success: handleEvent,
        			timeout: longPollTimeout,
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);
        				retries++;
        				if(retries > 3) {
        					// Did we just lose the server? :-(
        					connected = false;
        					gatewayCallbacks.error("Lost connection to the server (is it down?)");
        					return;
        				}
        				eventHandler();
        			}
        		});
        	}

        	// Private event handler: this will trigger plugin callbacks, if set
        	function handleEvent(json, skipTimeout) {
        		retries = 0;
        		if(!websockets && sessionId !== undefined && sessionId !== null && skipTimeout !== true)
        			eventHandler();
        		if(!websockets && Janus.isArray(json)) {
        			// We got an array: it means we passed a maxev > 1, iterate on all objects
        			for(var i=0; i<json.length; i++) {
        				handleEvent(json[i], true);
        			}
        			return;
        		}
        		if(json["janus"] === "keepalive") {
        			// Nothing happened
        			Janus.vdebug("Got a keepalive on session " + sessionId);
        			return;
        		} else if(json["janus"] === "server_info") {
        			// Just info on the Janus instance
        			console.debug("Got info on the Janus instance");
        			console.debug(json);
        			const transaction = json["transaction"];
        			if(transaction) {
        				const reportSuccess = transactions[transaction];
        				if(reportSuccess)
        					reportSuccess(json);
        				delete transactions[transaction];
        			}
        			return;
        		} else if(json["janus"] === "ack") {
        			// Just an ack, we can probably ignore
        			console.debug("Got an ack on session " + sessionId);
        			console.debug(json);
        			const transaction = json["transaction"];
        			if(transaction) {
        				const reportSuccess = transactions[transaction];
        				if(reportSuccess)
        					reportSuccess(json);
        				delete transactions[transaction];
        			}
        			return;
        		} else if(json["janus"] === "success") {
        			// Success!
        			console.debug("Got a success on session " + sessionId);
        			console.debug(json);
        			const transaction = json["transaction"];
        			if(transaction) {
        				const reportSuccess = transactions[transaction];
        				if(reportSuccess)
        					reportSuccess(json);
        				delete transactions[transaction];
        			}
        			return;
        		} else if(json["janus"] === "trickle") {
        			// We got a trickle candidate from Janus
        			const sender = json["sender"];
        			if(!sender) {
        				console.warn("Missing sender...");
        				return;
        			}
        			const pluginHandle = pluginHandles[sender];
        			if(!pluginHandle) {
        				console.debug("This handle is not attached to this session");
        				return;
        			}
        			var candidate = json["candidate"];
        			console.debug("Got a trickled candidate on session " + sessionId);
        			console.debug(candidate);
        			var config = pluginHandle.webrtcStuff;
        			if(config.pc && config.remoteSdp) {
        				// Add candidate right now
        				console.debug("Adding remote candidate:", candidate);
        				if(!candidate || candidate.completed === true) {
        					// end-of-candidates
        					config.pc.addIceCandidate(Janus.endOfCandidates);
        				} else {
        					// New candidate
        					config.pc.addIceCandidate(candidate);
        				}
        			} else {
        				// We didn't do setRemoteDescription (trickle got here before the offer?)
        				console.debug("We didn't do setRemoteDescription (trickle got here before the offer?), caching candidate");
        				if(!config.candidates)
        					config.candidates = [];
        				config.candidates.push(candidate);
        				console.debug(config.candidates);
        			}
        		} else if(json["janus"] === "webrtcup") {
        			// The PeerConnection with the server is up! Notify this
        			console.debug("Got a webrtcup event on session " + sessionId);
        			console.debug(json);
        			const sender = json["sender"];
        			if(!sender) {
        				console.warn("Missing sender...");
        				return;
        			}
        			const pluginHandle = pluginHandles[sender];
        			if(!pluginHandle) {
        				console.debug("This handle is not attached to this session");
        				return;
        			}
        			pluginHandle.webrtcState(true);
        			return;
        		} else if(json["janus"] === "hangup") {
        			// A plugin asked the core to hangup a PeerConnection on one of our handles
        			console.debug("Got a hangup event on session " + sessionId);
        			console.debug(json);
        			const sender = json["sender"];
        			if(!sender) {
        				console.warn("Missing sender...");
        				return;
        			}
        			const pluginHandle = pluginHandles[sender];
        			if(!pluginHandle) {
        				console.debug("This handle is not attached to this session");
        				return;
        			}
        			pluginHandle.webrtcState(false, json["reason"]);
        			pluginHandle.hangup();
        		} else if(json["janus"] === "detached") {
        			// A plugin asked the core to detach one of our handles
        			console.debug("Got a detached event on session " + sessionId);
        			console.debug(json);
        			const sender = json["sender"];
        			if(!sender) {
        				console.warn("Missing sender...");
        				return;
        			}
        			const pluginHandle = pluginHandles[sender];
        			if(!pluginHandle) {
        				// Don't warn here because destroyHandle causes this situation.
        				return;
        			}
        			pluginHandle.ondetached();
        			pluginHandle.detach();
        		} else if(json["janus"] === "media") {
        			// Media started/stopped flowing
        			console.debug("Got a media event on session " + sessionId);
        			console.debug(json);
        			const sender = json["sender"];
        			if(!sender) {
        				console.warn("Missing sender...");
        				return;
        			}
        			const pluginHandle = pluginHandles[sender];
        			if(!pluginHandle) {
        				console.debug("This handle is not attached to this session");
        				return;
        			}
        			pluginHandle.mediaState(json["type"], json["receiving"], json["mid"]);
        		} else if(json["janus"] === "slowlink") {
        			console.debug("Got a slowlink event on session " + sessionId);
        			console.debug(json);
        			// Trouble uplink or downlink
        			const sender = json["sender"];
        			if(!sender) {
        				console.warn("Missing sender...");
        				return;
        			}
        			const pluginHandle = pluginHandles[sender];
        			if(!pluginHandle) {
        				console.debug("This handle is not attached to this session");
        				return;
        			}
        			pluginHandle.slowLink(json["uplink"], json["lost"], json["mid"]);
        		} else if(json["janus"] === "error") {
        			// Oops, something wrong happened
        			Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        			console.debug(json);
        			var transaction = json["transaction"];
        			if(transaction) {
        				var reportSuccess = transactions[transaction];
        				if(reportSuccess) {
        					reportSuccess(json);
        				}
        				delete transactions[transaction];
        			}
        			return;
        		} else if(json["janus"] === "event") {
        			console.debug("Got a plugin event on session " + sessionId);
        			console.debug(json);
        			const sender = json["sender"];
        			if(!sender) {
        				console.warn("Missing sender...");
        				return;
        			}
        			var plugindata = json["plugindata"];
        			if(!plugindata) {
        				console.warn("Missing plugindata...");
        				return;
        			}
        			console.debug("  -- Event is coming from " + sender + " (" + plugindata["plugin"] + ")");
        			var data = plugindata["data"];
        			console.debug(data);
        			const pluginHandle = pluginHandles[sender];
        			if(!pluginHandle) {
        				console.warn("This handle is not attached to this session");
        				return;
        			}
        			var jsep = json["jsep"];
        			if(jsep) {
        				console.debug("Handling SDP as well...");
        				console.debug(jsep);
        			}
        			var callback = pluginHandle.onmessage;
        			if(callback) {
        				console.debug("Notifying application...");
        				// Send to callback specified when attaching plugin handle
        				callback(data, jsep);
        			} else {
        				// Send to generic callback (?)
        				console.debug("No provided notification callback");
        			}
        		} else if(json["janus"] === "timeout") {
        			Janus.error("Timeout on session " + sessionId);
        			console.debug(json);
        			if (websockets) {
        				ws.close(3504, "Gateway timeout");
        			}
        			return;
        		} else {
        			console.warn("Unknown message/event  '" + json["janus"] + "' on session " + sessionId);
        			console.debug(json);
        		}
        	}

        	// Private helper to send keep-alive messages on WebSockets
        	function keepAlive() {
        		if(!server || !websockets || !connected)
        			return;
        		wsKeepaliveTimeoutId = setTimeout(keepAlive, keepAlivePeriod);
        		var request = { "janus": "keepalive", "session_id": sessionId, "transaction": Janus.randomString(12) };
        		if(token)
        			request["token"] = token;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		ws.send(JSON.stringify(request));
        	}

        	// Private method to create a session
        	function createSession(callbacks) {
        		var transaction = Janus.randomString(12);
        		var request = { "janus": "create", "transaction": transaction };
        		if(callbacks["reconnect"]) {
        			// We're reconnecting, claim the session
        			connected = false;
        			request["janus"] = "claim";
        			request["session_id"] = sessionId;
        			// If we were using websockets, ignore the old connection
        			if(ws) {
        				ws.onopen = null;
        				ws.onerror = null;
        				ws.onclose = null;
        				if(wsKeepaliveTimeoutId) {
        					clearTimeout(wsKeepaliveTimeoutId);
        					wsKeepaliveTimeoutId = null;
        				}
        			}
        		}
        		if(token)
        			request["token"] = token;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		if(!server && Janus.isArray(servers)) {
        			// We still need to find a working server from the list we were given
        			server = servers[serversIndex];
        			if(server.indexOf("ws") === 0) {
        				websockets = true;
        				console.log("Server #" + (serversIndex+1) + ": trying WebSockets to contact Janus (" + server + ")");
        			} else {
        				websockets = false;
        				console.log("Server #" + (serversIndex+1) + ": trying REST API to contact Janus (" + server + ")");
        			}
        		}
        		if(websockets) {
        			ws = Janus.newWebSocket(server, 'janus-protocol');
        			wsHandlers = {
        				'error': function() {
        					Janus.error("Error connecting to the Janus WebSockets server... " + server);
        					if (Janus.isArray(servers) && !callbacks["reconnect"]) {
        						serversIndex++;
        						if (serversIndex === servers.length) {
        							// We tried all the servers the user gave us and they all failed
        							callbacks.error("Error connecting to any of the provided Janus servers: Is the server down?");
        							return;
        						}
        						// Let's try the next server
        						server = null;
        						setTimeout(function() {
        							createSession(callbacks);
        						}, 200);
        						return;
        					}
        					callbacks.error("Error connecting to the Janus WebSockets server: Is the server down?");
        				},

        				'open': function() {
        					// We need to be notified about the success
        					transactions[transaction] = function(json) {
        						console.debug(json);
        						if (json["janus"] !== "success") {
        							Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        							callbacks.error(json["error"].reason);
        							return;
        						}
        						wsKeepaliveTimeoutId = setTimeout(keepAlive, keepAlivePeriod);
        						connected = true;
        						sessionId = json["session_id"] ? json["session_id"] : json.data["id"];
        						if(callbacks["reconnect"]) {
        							console.log("Claimed session: " + sessionId);
        						} else {
        							console.log("Created session: " + sessionId);
        						}
        						Janus.sessions[sessionId] = that;
        						callbacks.success();
        					};
        					ws.send(JSON.stringify(request));
        				},

        				'message': function(event) {
        					handleEvent(JSON.parse(event.data));
        				},

        				'close': function() {
        					if (!server || !connected) {
        						return;
        					}
        					connected = false;
        					// FIXME What if this is called when the page is closed?
        					gatewayCallbacks.error("Lost connection to the server (is it down?)");
        				}
        			};

        			for(var eventName in wsHandlers) {
        				ws.addEventListener(eventName, wsHandlers[eventName]);
        			}

        			return;
        		}
        		Janus.httpAPICall(server, {
        			verb: 'POST',
        			withCredentials: withCredentials,
        			body: request,
        			success: function(json) {
        				console.debug(json);
        				if(json["janus"] !== "success") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        					callbacks.error(json["error"].reason);
        					return;
        				}
        				connected = true;
        				sessionId = json["session_id"] ? json["session_id"] : json.data["id"];
        				if(callbacks["reconnect"]) {
        					console.log("Claimed session: " + sessionId);
        				} else {
        					console.log("Created session: " + sessionId);
        				}
        				Janus.sessions[sessionId] = that;
        				eventHandler();
        				callbacks.success();
        			},
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);	// FIXME
        				if(Janus.isArray(servers) && !callbacks["reconnect"]) {
        					serversIndex++;
        					if(serversIndex === servers.length) {
        						// We tried all the servers the user gave us and they all failed
        						callbacks.error("Error connecting to any of the provided Janus servers: Is the server down?");
        						return;
        					}
        					// Let's try the next server
        					server = null;
        					setTimeout(function() { createSession(callbacks); }, 200);
        					return;
        				}
        				if(errorThrown === "")
        					callbacks.error(textStatus + ": Is the server down?");
        				else
        					callbacks.error(textStatus + ": " + errorThrown);
        			}
        		});
        	}

        	// Private method to get info on the server
        	function getInfo(callbacks) {
        		callbacks = callbacks || {};
        		// FIXME This method triggers a success even when we fail
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		console.log("Getting info on Janus instance");
        		if(!connected) {
        			console.warn("Is the server down? (connected=false)");
        			callbacks.error("Is the server down? (connected=false)");
        			return;
        		}
        		// We just need to send an "info" request
        		var transaction = Janus.randomString(12);
        		var request = { "janus": "info", "transaction": transaction };
        		if(token)
        			request["token"] = token;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		if(websockets) {
        			transactions[transaction] = function(json) {
        				console.log("Server info:");
        				console.debug(json);
        				if(json["janus"] !== "server_info") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        				}
        				callbacks.success(json);
        			}
        			ws.send(JSON.stringify(request));
        			return;
        		}
        		Janus.httpAPICall(server, {
        			verb: 'POST',
        			withCredentials: withCredentials,
        			body: request,
        			success: function(json) {
        				console.log("Server info:");
        				console.debug(json);
        				if(json["janus"] !== "server_info") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        				}
        				callbacks.success(json);
        			},
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);	// FIXME
        				if(errorThrown === "")
        					callbacks.error(textStatus + ": Is the server down?");
        				else
        					callbacks.error(textStatus + ": " + errorThrown);
        			}
        		});
        	}

        	// Private method to destroy a session
        	function destroySession(callbacks) {
        		callbacks = callbacks || {};
        		// FIXME This method triggers a success even when we fail
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		var unload = (callbacks.unload === true);
        		var notifyDestroyed = true;
        		if(callbacks.notifyDestroyed !== undefined && callbacks.notifyDestroyed !== null)
        			notifyDestroyed = (callbacks.notifyDestroyed === true);
        		var cleanupHandles = (callbacks.cleanupHandles === true);
        		console.log("Destroying session " + sessionId + " (unload=" + unload + ")");
        		if(!sessionId) {
        			console.warn("No session to destroy");
        			callbacks.success();
        			if(notifyDestroyed)
        				gatewayCallbacks.destroyed();
        			return;
        		}
        		if(cleanupHandles) {
        			for(var handleId in pluginHandles)
        				destroyHandle(handleId, { noRequest: true });
        		}
        		if(!connected) {
        			console.warn("Is the server down? (connected=false)");
        			sessionId = null;
        			callbacks.success();
        			return;
        		}
        		// No need to destroy all handles first, Janus will do that itself
        		var request = { "janus": "destroy", "transaction": Janus.randomString(12) };
        		if(token)
        			request["token"] = token;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		if(unload) {
        			// We're unloading the page: use sendBeacon for HTTP instead,
        			// or just close the WebSocket connection if we're using that
        			if(websockets) {
        				ws.onclose = null;
        				ws.close();
        				ws = null;
        			} else {
        				navigator.sendBeacon(server + "/" + sessionId, JSON.stringify(request));
        			}
        			console.log("Destroyed session:");
        			sessionId = null;
        			connected = false;
        			callbacks.success();
        			if(notifyDestroyed)
        				gatewayCallbacks.destroyed();
        			return;
        		}
        		if(websockets) {
        			request["session_id"] = sessionId;

        			var unbindWebSocket = function() {
        				for(var eventName in wsHandlers) {
        					ws.removeEventListener(eventName, wsHandlers[eventName]);
        				}
        				ws.removeEventListener('message', onUnbindMessage);
        				ws.removeEventListener('error', onUnbindError);
        				if(wsKeepaliveTimeoutId) {
        					clearTimeout(wsKeepaliveTimeoutId);
        				}
        				ws.close();
        			};

        			var onUnbindMessage = function(event){
        				var data = JSON.parse(event.data);
        				if(data.session_id == request.session_id && data.transaction == request.transaction) {
        					unbindWebSocket();
        					callbacks.success();
        					if(notifyDestroyed)
        						gatewayCallbacks.destroyed();
        				}
        			};
        			var onUnbindError = function() {
        				unbindWebSocket();
        				callbacks.error("Failed to destroy the server: Is the server down?");
        				if(notifyDestroyed)
        					gatewayCallbacks.destroyed();
        			};

        			ws.addEventListener('message', onUnbindMessage);
        			ws.addEventListener('error', onUnbindError);

        			if (ws.readyState === 1) {
        				ws.send(JSON.stringify(request));
        			} else {
        				onUnbindError();
        			}

        			return;
        		}
        		Janus.httpAPICall(server + "/" + sessionId, {
        			verb: 'POST',
        			withCredentials: withCredentials,
        			body: request,
        			success: function(json) {
        				console.log("Destroyed session:");
        				console.debug(json);
        				sessionId = null;
        				connected = false;
        				if(json["janus"] !== "success") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        				}
        				callbacks.success();
        				if(notifyDestroyed)
        					gatewayCallbacks.destroyed();
        			},
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);	// FIXME
        				// Reset everything anyway
        				sessionId = null;
        				connected = false;
        				callbacks.success();
        				if(notifyDestroyed)
        					gatewayCallbacks.destroyed();
        			}
        		});
        	}

        	// Private method to create a plugin handle
        	function createHandle(callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		callbacks.dataChannelOptions = callbacks.dataChannelOptions || { ordered: true };
        		callbacks.consentDialog = (typeof callbacks.consentDialog == "function") ? callbacks.consentDialog : Janus.noop;
        		callbacks.iceState = (typeof callbacks.iceState == "function") ? callbacks.iceState : Janus.noop;
        		callbacks.mediaState = (typeof callbacks.mediaState == "function") ? callbacks.mediaState : Janus.noop;
        		callbacks.webrtcState = (typeof callbacks.webrtcState == "function") ? callbacks.webrtcState : Janus.noop;
        		callbacks.slowLink = (typeof callbacks.slowLink == "function") ? callbacks.slowLink : Janus.noop;
        		callbacks.onmessage = (typeof callbacks.onmessage == "function") ? callbacks.onmessage : Janus.noop;
        		callbacks.onlocaltrack = (typeof callbacks.onlocaltrack == "function") ? callbacks.onlocaltrack : Janus.noop;
        		callbacks.onremotetrack = (typeof callbacks.onremotetrack == "function") ? callbacks.onremotetrack : Janus.noop;
        		callbacks.ondata = (typeof callbacks.ondata == "function") ? callbacks.ondata : Janus.noop;
        		callbacks.ondataopen = (typeof callbacks.ondataopen == "function") ? callbacks.ondataopen : Janus.noop;
        		callbacks.oncleanup = (typeof callbacks.oncleanup == "function") ? callbacks.oncleanup : Janus.noop;
        		callbacks.ondetached = (typeof callbacks.ondetached == "function") ? callbacks.ondetached : Janus.noop;
        		if(!connected) {
        			console.warn("Is the server down? (connected=false)");
        			callbacks.error("Is the server down? (connected=false)");
        			return;
        		}
        		var plugin = callbacks.plugin;
        		if(!plugin) {
        			Janus.error("Invalid plugin");
        			callbacks.error("Invalid plugin");
        			return;
        		}
        		var opaqueId = callbacks.opaqueId;
        		var loopIndex = callbacks.loopIndex;
        		var handleToken = callbacks.token ? callbacks.token : token;
        		var transaction = Janus.randomString(12);
        		var request = { "janus": "attach", "plugin": plugin, "opaque_id": opaqueId, "loop_index": loopIndex, "transaction": transaction };
        		if(handleToken)
        			request["token"] = handleToken;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		if(websockets) {
        			transactions[transaction] = function(json) {
        				console.debug(json);
        				if(json["janus"] !== "success") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        					callbacks.error("Ooops: " + json["error"].code + " " + json["error"].reason);
        					return;
        				}
        				var handleId = json.data["id"];
        				console.log("Created handle: " + handleId);
        				var pluginHandle =
        					{
        						session : that,
        						plugin : plugin,
        						id : handleId,
        						token : handleToken,
        						detached : false,
        						webrtcStuff : {
        							started : false,
        							myStream : null,
        							streamExternal : false,
        							remoteStream : null,
        							mySdp : null,
        							mediaConstraints : null,
        							pc : null,
        							dataChannelOptions: callbacks.dataChannelOptions,
        							dataChannel : {},
        							dtmfSender : null,
        							trickle : true,
        							iceDone : false,
        							bitrate : {}
        						},
        						getId : function() { return handleId; },
        						getPlugin : function() { return plugin; },
        						getVolume : function(mid, result) { return getVolume(handleId, mid, true, result); },
        						getRemoteVolume : function(mid, result) { return getVolume(handleId, mid, true, result); },
        						getLocalVolume : function(mid, result) { return getVolume(handleId, mid, false, result); },
        						isAudioMuted : function(mid) { return isMuted(handleId, mid, false); },
        						muteAudio : function(mid) { return mute(handleId, mid, false, true); },
        						unmuteAudio : function(mid) { return mute(handleId, mid, false, false); },
        						isVideoMuted : function(mid) { return isMuted(handleId, mid, true); },
        						muteVideo : function(mid) { return mute(handleId, mid, true, true); },
        						unmuteVideo : function(mid) { return mute(handleId, mid, true, false); },
        						getBitrate : function(mid) { return getBitrate(handleId, mid); },
        						send : function(callbacks) { sendMessage(handleId, callbacks); },
        						data : function(callbacks) { sendData(handleId, callbacks); },
        						dtmf : function(callbacks) { sendDtmf(handleId, callbacks); },
        						consentDialog : callbacks.consentDialog,
        						iceState : callbacks.iceState,
        						mediaState : callbacks.mediaState,
        						webrtcState : callbacks.webrtcState,
        						slowLink : callbacks.slowLink,
        						onmessage : callbacks.onmessage,
        						createOffer : function(callbacks) { prepareWebrtc(handleId, true, callbacks); },
        						createAnswer : function(callbacks) { prepareWebrtc(handleId, false, callbacks); },
        						handleRemoteJsep : function(callbacks) { prepareWebrtcPeer(handleId, callbacks); },
        						onlocaltrack : callbacks.onlocaltrack,
        						onremotetrack : callbacks.onremotetrack,
        						ondata : callbacks.ondata,
        						ondataopen : callbacks.ondataopen,
        						oncleanup : callbacks.oncleanup,
        						ondetached : callbacks.ondetached,
        						hangup : function(sendRequest) { cleanupWebrtc(handleId, sendRequest === true); },
        						detach : function(callbacks) { destroyHandle(handleId, callbacks); }
        					};
        				pluginHandles[handleId] = pluginHandle;
        				callbacks.success(pluginHandle);
        			};
        			request["session_id"] = sessionId;
        			ws.send(JSON.stringify(request));
        			return;
        		}
        		Janus.httpAPICall(server + "/" + sessionId, {
        			verb: 'POST',
        			withCredentials: withCredentials,
        			body: request,
        			success: function(json) {
        				console.debug(json);
        				if(json["janus"] !== "success") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        					callbacks.error("Ooops: " + json["error"].code + " " + json["error"].reason);
        					return;
        				}
        				var handleId = json.data["id"];
        				console.log("Created handle: " + handleId);
        				var pluginHandle =
        					{
        						session : that,
        						plugin : plugin,
        						id : handleId,
        						token : handleToken,
        						detached : false,
        						webrtcStuff : {
        							started : false,
        							myStream : null,
        							streamExternal : false,
        							remoteStream : null,
        							mySdp : null,
        							mediaConstraints : null,
        							pc : null,
        							dataChannelOptions: callbacks.dataChannelOptions,
        							dataChannel : {},
        							dtmfSender : null,
        							trickle : true,
        							iceDone : false,
        							bitrate: {}
        						},
        						getId : function() { return handleId; },
        						getPlugin : function() { return plugin; },
        						getVolume : function(mid, result) { return getVolume(handleId, mid, true, result); },
        						getRemoteVolume : function(mid, result) { return getVolume(handleId, mid, true, result); },
        						getLocalVolume : function(mid, result) { return getVolume(handleId, mid, false, result); },
        						isAudioMuted : function(mid) { return isMuted(handleId, mid, false); },
        						muteAudio : function(mid) { return mute(handleId, mid, false, true); },
        						unmuteAudio : function(mid) { return mute(handleId, mid, false, false); },
        						isVideoMuted : function(mid) { return isMuted(handleId, mid, true); },
        						muteVideo : function(mid) { return mute(handleId, mid, true, true); },
        						unmuteVideo : function(mid) { return mute(handleId, mid, true, false); },
        						getBitrate : function(mid) { return getBitrate(handleId, mid); },
        						send : function(callbacks) { sendMessage(handleId, callbacks); },
        						data : function(callbacks) { sendData(handleId, callbacks); },
        						dtmf : function(callbacks) { sendDtmf(handleId, callbacks); },
        						consentDialog : callbacks.consentDialog,
        						iceState : callbacks.iceState,
        						mediaState : callbacks.mediaState,
        						webrtcState : callbacks.webrtcState,
        						slowLink : callbacks.slowLink,
        						onmessage : callbacks.onmessage,
        						createOffer : function(callbacks) { prepareWebrtc(handleId, true, callbacks); },
        						createAnswer : function(callbacks) { prepareWebrtc(handleId, false, callbacks); },
        						handleRemoteJsep : function(callbacks) { prepareWebrtcPeer(handleId, callbacks); },
        						onlocaltrack : callbacks.onlocaltrack,
        						onremotetrack : callbacks.onremotetrack,
        						ondata : callbacks.ondata,
        						ondataopen : callbacks.ondataopen,
        						oncleanup : callbacks.oncleanup,
        						ondetached : callbacks.ondetached,
        						hangup : function(sendRequest) { cleanupWebrtc(handleId, sendRequest === true); },
        						detach : function(callbacks) { destroyHandle(handleId, callbacks); }
        					}
        				pluginHandles[handleId] = pluginHandle;
        				callbacks.success(pluginHandle);
        			},
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);	// FIXME
        				if(errorThrown === "")
        					callbacks.error(textStatus + ": Is the server down?");
        				else
        					callbacks.error(textStatus + ": " + errorThrown);
        			}
        		});
        	}

        	// Private method to send a message
        	function sendMessage(handleId, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		if(!connected) {
        			console.warn("Is the server down? (connected=false)");
        			callbacks.error("Is the server down? (connected=false)");
        			return;
        		}
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var message = callbacks.message;
        		var jsep = callbacks.jsep;
        		var transaction = Janus.randomString(12);
        		var request = { "janus": "message", "body": message, "transaction": transaction };
        		if(pluginHandle.token)
        			request["token"] = pluginHandle.token;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		if(jsep) {
        			request.jsep = {
        				type: jsep.type,
        				sdp: jsep.sdp
        			};
        			if(jsep.e2ee)
        				request.jsep.e2ee = true;
        			if(jsep.rid_order === "hml" || jsep.rid_order === "lmh")
        				request.jsep.rid_order = jsep.rid_order;
        			if(jsep.force_relay)
        				request.jsep.force_relay = true;
        		}
        		console.debug("Sending message to plugin (handle=" + handleId + "):");
        		console.debug(request);
        		if(websockets) {
        			request["session_id"] = sessionId;
        			request["handle_id"] = handleId;
        			transactions[transaction] = function(json) {
        				console.debug("Message sent!");
        				console.debug(JSON.stringify(json));
        				if(json["janus"] === "success") {
        					// We got a success, must have been a synchronous transaction
        					var plugindata = json["plugindata"];
        					if(!plugindata) {
        						console.warn("Request succeeded, but missing plugindata...");
        						callbacks.success();
        						return;
        					}
        					console.log("Synchronous transaction successful (" + plugindata["plugin"] + ")");
        					var data = plugindata["data"];
        					console.debug(data);
        					callbacks.success(data);
        					return;
        				} else if(json["janus"] !== "ack") {
        					// Not a success and not an ack, must be an error
        					if(json["error"]) {
        						Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        						callbacks.error(json["error"].code + " " + json["error"].reason);
        					} else {
        						Janus.error("Unknown error");	// FIXME
        						callbacks.error("Unknown error");
        					}
        					return;
        				}
        				// If we got here, the plugin decided to handle the request asynchronously
        				callbacks.success();
        			};
        			ws.send(JSON.stringify(request));
        			return;
        		}
        		Janus.httpAPICall(server + "/" + sessionId + "/" + handleId, {
        			verb: 'POST',
        			withCredentials: withCredentials,
        			body: request,
        			success: function(json) {
        				console.debug("Message sent!");
        				console.debug(json);
        				if(json["janus"] === "success") {
        					// We got a success, must have been a synchronous transaction
        					var plugindata = json["plugindata"];
        					if(!plugindata) {
        						console.warn("Request succeeded, but missing plugindata...");
        						callbacks.success();
        						return;
        					}
        					console.log("Synchronous transaction successful (" + plugindata["plugin"] + ")");
        					var data = plugindata["data"];
        					console.debug(data);
        					callbacks.success(data);
        					return;
        				} else if(json["janus"] !== "ack") {
        					// Not a success and not an ack, must be an error
        					if(json["error"]) {
        						Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        						callbacks.error(json["error"].code + " " + json["error"].reason);
        					} else {
        						Janus.error("Unknown error");	// FIXME
        						callbacks.error("Unknown error");
        					}
        					return;
        				}
        				// If we got here, the plugin decided to handle the request asynchronously
        				callbacks.success();
        			},
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);	// FIXME
        				callbacks.error(textStatus + ": " + errorThrown);
        			}
        		});
        	}

        	// Private method to send a trickle candidate
        	function sendTrickleCandidate(handleId, candidate) {
        		if(!connected) {
        			console.warn("Is the server down? (connected=false)");
        			return;
        		}
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			return;
        		}
        		var request = { "janus": "trickle", "candidate": candidate, "transaction": Janus.randomString(12) };
        		if(pluginHandle.token)
        			request["token"] = pluginHandle.token;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		Janus.vdebug("Sending trickle candidate (handle=" + handleId + "):");
        		Janus.vdebug(request);
        		if(websockets) {
        			request["session_id"] = sessionId;
        			request["handle_id"] = handleId;
        			ws.send(JSON.stringify(request));
        			return;
        		}
        		Janus.httpAPICall(server + "/" + sessionId + "/" + handleId, {
        			verb: 'POST',
        			withCredentials: withCredentials,
        			body: request,
        			success: function(json) {
        				Janus.vdebug("Candidate sent!");
        				Janus.vdebug(json);
        				if(json["janus"] !== "ack") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        					return;
        				}
        			},
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);	// FIXME
        			}
        		});
        	}

        	// Private method to create a data channel
        	function createDataChannel(handleId, dclabel, dcprotocol, incoming, pendingData) {
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		if(!config.pc) {
        			console.warn("Invalid PeerConnection");
        			return;
        		}
        		var onDataChannelMessage = function(event) {
        			console.log('Received message on data channel:', event);
        			var label = event.target.label;
        			pluginHandle.ondata(event.data, label);
        		};
        		var onDataChannelStateChange = function(event) {
        			console.log('Received state change on data channel:', event);
        			var label = event.target.label;
        			var protocol = event.target.protocol;
        			var dcState = config.dataChannel[label] ? config.dataChannel[label].readyState : "null";
        			console.log('State change on <' + label + '> data channel: ' + dcState);
        			if(dcState === 'open') {
        				// Any pending messages to send?
        				if(config.dataChannel[label].pending && config.dataChannel[label].pending.length > 0) {
        					console.log("Sending pending messages on <" + label + ">:", config.dataChannel[label].pending.length);
        					for(var data of config.dataChannel[label].pending) {
        						console.log("Sending data on data channel <" + label + ">");
        						console.debug(data);
        						config.dataChannel[label].send(data);
        					}
        					config.dataChannel[label].pending = [];
        				}
        				// Notify the open data channel
        				pluginHandle.ondataopen(label, protocol);
        			}
        		};
        		var onDataChannelError = function(error) {
        			Janus.error('Got error on data channel:', error);
        			// TODO
        		};
        		if(!incoming) {
        			// FIXME Add options (ordered, maxRetransmits, etc.)
        			var dcoptions = config.dataChannelOptions;
        			if(dcprotocol)
        				dcoptions.protocol = dcprotocol;
        			config.dataChannel[dclabel] = config.pc.createDataChannel(dclabel, dcoptions);
        		} else {
        			// The channel was created by Janus
        			config.dataChannel[dclabel] = incoming;
        		}
        		config.dataChannel[dclabel].onmessage = onDataChannelMessage;
        		config.dataChannel[dclabel].onopen = onDataChannelStateChange;
        		config.dataChannel[dclabel].onclose = onDataChannelStateChange;
        		config.dataChannel[dclabel].onerror = onDataChannelError;
        		config.dataChannel[dclabel].pending = [];
        		if(pendingData)
        			config.dataChannel[dclabel].pending.push(pendingData);
        	}

        	// Private method to send a data channel message
        	function sendData(handleId, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		var data = callbacks.text || callbacks.data;
        		if(!data) {
        			console.warn("Invalid data");
        			callbacks.error("Invalid data");
        			return;
        		}
        		var label = callbacks.label ? callbacks.label : Janus.dataChanDefaultLabel;
        		if(!config.dataChannel[label]) {
        			// Create new data channel and wait for it to open
        			createDataChannel(handleId, label, callbacks.protocol, false, data, callbacks.protocol);
        			callbacks.success();
        			return;
        		}
        		if(config.dataChannel[label].readyState !== "open") {
        			config.dataChannel[label].pending.push(data);
        			callbacks.success();
        			return;
        		}
        		console.log("Sending data on data channel <" + label + ">");
        		console.debug(data);
        		config.dataChannel[label].send(data);
        		callbacks.success();
        	}

        	// Private method to send a DTMF tone
        	function sendDtmf(handleId, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		if(!config.dtmfSender) {
        			// Create the DTMF sender the proper way, if possible
        			if(config.pc) {
        				var senders = config.pc.getSenders();
        				var audioSender = senders.find(function(sender) {
        					return sender.track && sender.track.kind === 'audio';
        				});
        				if(!audioSender) {
        					console.warn("Invalid DTMF configuration (no audio track)");
        					callbacks.error("Invalid DTMF configuration (no audio track)");
        					return;
        				}
        				config.dtmfSender = audioSender.dtmf;
        				if(config.dtmfSender) {
        					console.log("Created DTMF Sender");
        					config.dtmfSender.ontonechange = function(tone) { console.debug("Sent DTMF tone: " + tone.tone); };
        				}
        			}
        			if(!config.dtmfSender) {
        				console.warn("Invalid DTMF configuration");
        				callbacks.error("Invalid DTMF configuration");
        				return;
        			}
        		}
        		var dtmf = callbacks.dtmf;
        		if(!dtmf) {
        			console.warn("Invalid DTMF parameters");
        			callbacks.error("Invalid DTMF parameters");
        			return;
        		}
        		var tones = dtmf.tones;
        		if(!tones) {
        			console.warn("Invalid DTMF string");
        			callbacks.error("Invalid DTMF string");
        			return;
        		}
        		var duration = (typeof dtmf.duration === 'number') ? dtmf.duration : 500; // We choose 500ms as the default duration for a tone
        		var gap = (typeof dtmf.gap === 'number') ? dtmf.gap : 50; // We choose 50ms as the default gap between tones
        		console.debug("Sending DTMF string " + tones + " (duration " + duration + "ms, gap " + gap + "ms)");
        		config.dtmfSender.insertDTMF(tones, duration, gap);
        		callbacks.success();
        	}

        	// Private method to destroy a plugin handle
        	function destroyHandle(handleId, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		var noRequest = (callbacks.noRequest === true);
        		console.log("Destroying handle " + handleId + " (only-locally=" + noRequest + ")");
        		cleanupWebrtc(handleId);
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || pluginHandle.detached) {
        			// Plugin was already detached by Janus, calling detach again will return a handle not found error, so just exit here
        			delete pluginHandles[handleId];
        			callbacks.success();
        			return;
        		}
        		pluginHandle.detached = true;
        		if(noRequest) {
        			// We're only removing the handle locally
        			delete pluginHandles[handleId];
        			callbacks.success();
        			return;
        		}
        		if(!connected) {
        			console.warn("Is the server down? (connected=false)");
        			callbacks.error("Is the server down? (connected=false)");
        			return;
        		}
        		var request = { "janus": "detach", "transaction": Janus.randomString(12) };
        		if(pluginHandle.token)
        			request["token"] = pluginHandle.token;
        		if(apisecret)
        			request["apisecret"] = apisecret;
        		if(websockets) {
        			request["session_id"] = sessionId;
        			request["handle_id"] = handleId;
        			ws.send(JSON.stringify(request));
        			delete pluginHandles[handleId];
        			callbacks.success();
        			return;
        		}
        		Janus.httpAPICall(server + "/" + sessionId + "/" + handleId, {
        			verb: 'POST',
        			withCredentials: withCredentials,
        			body: request,
        			success: function(json) {
        				console.log("Destroyed handle:");
        				console.debug(json);
        				if(json["janus"] !== "success") {
        					Janus.error("Ooops: " + json["error"].code + " " + json["error"].reason);	// FIXME
        				}
        				delete pluginHandles[handleId];
        				callbacks.success();
        			},
        			error: function(textStatus, errorThrown) {
        				Janus.error(textStatus + ":", errorThrown);	// FIXME
        				// We cleanup anyway
        				delete pluginHandles[handleId];
        				callbacks.success();
        			}
        		});
        	}

        	// WebRTC stuff
        	function streamsDone(handleId, jsep, media, callbacks, stream) {
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			// Close all tracks if the given stream has been created internally
        			if(!callbacks.stream) {
        				Janus.stopAllTracks(stream);
        			}
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		console.debug("streamsDone:", stream);
        		if(stream) {
        			console.debug("  -- Audio tracks:", stream.getAudioTracks());
        			console.debug("  -- Video tracks:", stream.getVideoTracks());
        		}
        		// We're now capturing the new stream: check if we're updating or if it's a new thing
        		var addTracks = false;
        		if(!config.myStream || !media.update || (config.streamExternal && !media.replaceAudio && !media.replaceVideo)) {
        			config.myStream = stream;
        			addTracks = true;
        		} else {
        			// We only need to update the existing stream
        			if(((!media.update && isAudioSendEnabled(media)) || (media.update && (media.addAudio || media.replaceAudio))) &&
        					stream.getAudioTracks() && stream.getAudioTracks().length) {
        				config.myStream.addTrack(stream.getAudioTracks()[0]);
        				if(Janus.unifiedPlan) {
        					// Use Transceivers
        					console.log((media.replaceAudio ? "Replacing" : "Adding") + " audio track:", stream.getAudioTracks()[0]);
        					var audioTransceiver = null;
        					const transceivers = config.pc.getTransceivers();
        					if(transceivers && transceivers.length > 0) {
        						for(const t of transceivers) {
        							if((t.sender && t.sender.track && t.sender.track.kind === "audio") ||
        									(t.receiver && t.receiver.track && t.receiver.track.kind === "audio")) {
        								audioTransceiver = t;
        								break;
        							}
        						}
        					}
        					if(audioTransceiver && audioTransceiver.sender) {
        						audioTransceiver.sender.replaceTrack(stream.getAudioTracks()[0]);
        					} else {
        						config.pc.addTrack(stream.getAudioTracks()[0], stream);
        					}
        				} else {
        					console.log((media.replaceAudio ? "Replacing" : "Adding") + " audio track:", stream.getAudioTracks()[0]);
        					config.pc.addTrack(stream.getAudioTracks()[0], stream);
        				}
        			}
        			if(((!media.update && isVideoSendEnabled(media)) || (media.update && (media.addVideo || media.replaceVideo))) &&
        					stream.getVideoTracks() && stream.getVideoTracks().length) {
        				config.myStream.addTrack(stream.getVideoTracks()[0]);
        				if(Janus.unifiedPlan) {
        					// Use Transceivers
        					console.log((media.replaceVideo ? "Replacing" : "Adding") + " video track:", stream.getVideoTracks()[0]);
        					var videoTransceiver = null;
        					const transceivers = config.pc.getTransceivers();
        					if(transceivers && transceivers.length > 0) {
        						for(const t of transceivers) {
        							if((t.sender && t.sender.track && t.sender.track.kind === "video") ||
        									(t.receiver && t.receiver.track && t.receiver.track.kind === "video")) {
        								videoTransceiver = t;
        								break;
        							}
        						}
        					}
        					if(videoTransceiver && videoTransceiver.sender) {
        						videoTransceiver.sender.replaceTrack(stream.getVideoTracks()[0]);
        					} else {
        						config.pc.addTrack(stream.getVideoTracks()[0], stream);
        					}
        				} else {
        					console.log((media.replaceVideo ? "Replacing" : "Adding") + " video track:", stream.getVideoTracks()[0]);
        					config.pc.addTrack(stream.getVideoTracks()[0], stream);
        				}
        			}
        		}
        		// If we still need to create a PeerConnection, let's do that
        		if(!config.pc) {
        			var pc_config = {"iceServers": iceServers, "iceTransportPolicy": iceTransportPolicy, "bundlePolicy": bundlePolicy};
        			if(Janus.webRTCAdapter.browserDetails.browser === "chrome") {
        				// For Chrome versions before 72, we force a plan-b semantic, and unified-plan otherwise
        				pc_config["sdpSemantics"] = (Janus.webRTCAdapter.browserDetails.version < 72) ? "plan-b" : "unified-plan";
        			}
        			var pc_constraints = {
        				"optional": [{"DtlsSrtpKeyAgreement": true}]
        			};
        			if(ipv6Support) {
        				pc_constraints.optional.push({"googIPv6":true});
        			}
        			// Any custom constraint to add?
        			if(callbacks.rtcConstraints && typeof callbacks.rtcConstraints === 'object') {
        				console.debug("Adding custom PeerConnection constraints:", callbacks.rtcConstraints);
        				for(var i in callbacks.rtcConstraints) {
        					pc_constraints.optional.push(callbacks.rtcConstraints[i]);
        				}
        			}
        			if(Janus.webRTCAdapter.browserDetails.browser === "edge") {
        				// This is Edge, enable BUNDLE explicitly
        				pc_config.bundlePolicy = "max-bundle";
        			}
        			// Check if a sender or receiver transform has been provided
        			if(RTCRtpSender && (RTCRtpSender.prototype.createEncodedStreams ||
        					(RTCRtpSender.prototype.createEncodedAudioStreams &&
        					RTCRtpSender.prototype.createEncodedVideoStreams)) &&
        					(callbacks.senderTransforms || callbacks.receiverTransforms)) {
        				config.senderTransforms = callbacks.senderTransforms;
        				config.receiverTransforms = callbacks.receiverTransforms;
        				pc_config["forceEncodedAudioInsertableStreams"] = true;
        				pc_config["forceEncodedVideoInsertableStreams"] = true;
        				pc_config["encodedInsertableStreams"] = true;
        			}
        			console.log("Creating PeerConnection");
        			console.debug(pc_constraints);
        			config.pc = new RTCPeerConnection(pc_config, pc_constraints);
        			console.debug(config.pc);
        			if(config.pc.getStats) {	// FIXME
        				config.volume = {};
        				config.bitrate.value = "0 kbits/sec";
        			}
        			console.log("Preparing local SDP and gathering candidates (trickle=" + config.trickle + ")");
        			config.pc.oniceconnectionstatechange = function() {
        				if(config.pc)
        					pluginHandle.iceState(config.pc.iceConnectionState);
        			};
        			config.pc.onicecandidate = function(event) {
        				if (!event.candidate ||
        						(Janus.webRTCAdapter.browserDetails.browser === 'edge' && event.candidate.candidate.indexOf('endOfCandidates') > 0)) {
        					console.log("End of candidates.");
        					config.iceDone = true;
        					if(config.trickle === true) {
        						// Notify end of candidates
        						sendTrickleCandidate(handleId, {"completed": true});
        					} else {
        						// No trickle, time to send the complete SDP (including all candidates)
        						sendSDP(handleId, callbacks);
        					}
        				} else {
        					// JSON.stringify doesn't work on some WebRTC objects anymore
        					// See https://code.google.com/p/chromium/issues/detail?id=467366
        					var candidate = {
        						"candidate": event.candidate.candidate,
        						"sdpMid": event.candidate.sdpMid,
        						"sdpMLineIndex": event.candidate.sdpMLineIndex
        					};
        					if(config.trickle === true) {
        						// Send candidate
        						sendTrickleCandidate(handleId, candidate);
        					}
        				}
        			};
        			config.pc.ontrack = function(event) {
        				console.log("Handling Remote Track");
        				console.debug(event);
        				if(!event.streams)
        					return;
        				config.remoteStream = event.streams[0];
        				if(!event.track)
        					return;
        				// Notify about the new track event
        				var mid = event.transceiver ? event.transceiver.mid : event.track.id;
        				try {
        					pluginHandle.onremotetrack(event.track, mid, true);
        				} catch(e) {
        					Janus.error(e);
        				}
        				if(event.track.onended)
        					return;
        				if(config.receiverTransforms) {
        					var receiverStreams = null;
        					if(RTCRtpSender.prototype.createEncodedStreams) {
        						receiverStreams = event.receiver.createEncodedStreams();
        					} else if(RTCRtpSender.prototype.createAudioEncodedStreams || RTCRtpSender.prototype.createEncodedVideoStreams) {
        						if(event.track.kind === "audio" && config.receiverTransforms["audio"]) {
        							receiverStreams = event.receiver.createEncodedAudioStreams();
        						} else if(event.track.kind === "video" && config.receiverTransforms["video"]) {
        							receiverStreams = event.receiver.createEncodedVideoStreams();
        						}
        					}
        					if(receiverStreams) {
        						console.log(receiverStreams);
        						if(receiverStreams.readableStream && receiverStreams.writableStream) {
        							receiverStreams.readableStream
        								.pipeThrough(config.receiverTransforms[event.track.kind])
        								.pipeTo(receiverStreams.writableStream);
        						} else if(receiverStreams.readable && receiverStreams.writable) {
        							receiverStreams.readable
        								.pipeThrough(config.receiverTransforms[event.track.kind])
        								.pipeTo(receiverStreams.writable);
        						}
        					}
        				}
        				var trackMutedTimeoutId = null;
        				console.log("Adding onended callback to track:", event.track);
        				event.track.onended = function(ev) {
        					console.log("Remote track removed:", ev);
        					if(config.remoteStream) {
        						clearTimeout(trackMutedTimeoutId);
        						config.remoteStream.removeTrack(ev.target);
        						// Notify the application
        						var mid = ev.target.id;
        						if(Janus.unifiedPlan) {
        							var transceiver = config.pc.getTransceivers().find(
        								t => t.receiver.track === ev.target);
        							mid = transceiver.mid;
        						}
        						try {
        							pluginHandle.onremotetrack(ev.target, mid, false);
        						} catch(e) {
        							Janus.error(e);
        						}
        					}
        				};
        				event.track.onmute = function(ev) {
        					console.log("Remote track muted:", ev);
        					if(config.remoteStream && trackMutedTimeoutId == null) {
        						trackMutedTimeoutId = setTimeout(function() {
        							console.log("Removing remote track");
        							if (config.remoteStream) {
        								config.remoteStream.removeTrack(ev.target);
        								// Notify the application the track is gone
        								var mid = ev.target.id;
        								if(Janus.unifiedPlan) {
        									var transceiver = config.pc.getTransceivers().find(
        										t => t.receiver.track === ev.target);
        									mid = transceiver.mid;
        								}
        								try {
        									pluginHandle.onremotetrack(ev.target, mid, false);
        								} catch(e) {
        									Janus.error(e);
        								}
        							}
        							trackMutedTimeoutId = null;
        						// Chrome seems to raise mute events only at multiples of 834ms;
        						// we set the timeout to three times this value (rounded to 840ms)
        						}, 3 * 840);
        					}
        				};
        				event.track.onunmute = function(ev) {
        					console.log("Remote track flowing again:", ev);
        					if(trackMutedTimeoutId != null) {
        						clearTimeout(trackMutedTimeoutId);
        						trackMutedTimeoutId = null;
        					} else {
        						try {
        							config.remoteStream.addTrack(ev.target);
        							// Notify the application the track is back
        							var mid = ev.target.id;
        							if(Janus.unifiedPlan) {
        								var transceiver = config.pc.getTransceivers().find(
        									t => t.receiver.track === ev.target);
        								mid = transceiver.mid;
        							}
        							pluginHandle.onremotetrack(ev.target, mid, true);
        						} catch(e) {
        							Janus.error(e);
        						}
        					}
        				};
        			};
        		}
        		if(addTracks && stream) {
        			console.log('Adding local stream');
        			var simulcast = (callbacks.simulcast === true || callbacks.simulcast2 === true) && Janus.unifiedPlan;
        			var svc = callbacks.svc;
        			stream.getTracks().forEach(function(track) {
        				console.log('Adding local track:', track);
        				var sender = null;
        				if((!simulcast && !svc) || track.kind === 'audio') {
        					sender = config.pc.addTrack(track, stream);
        				} else if(simulcast) {
        					console.log('Enabling rid-based simulcasting:', track);
        					let maxBitrates = getMaxBitrates(callbacks.simulcastMaxBitrates);
        					let tr = config.pc.addTransceiver(track, {
        						direction: "sendrecv",
        						streams: [stream],
        						sendEncodings: callbacks.sendEncodings || [
        							{ rid: "h", active: true, maxBitrate: maxBitrates.high },
        							{ rid: "m", active: true, maxBitrate: maxBitrates.medium, scaleResolutionDownBy: 2 },
        							{ rid: "l", active: true, maxBitrate: maxBitrates.low, scaleResolutionDownBy: 4 }
        						]
        					});
        					if(tr)
        						sender = tr.sender;
        				} else {
        					console.log('Enabling SVC (' + svc + '):', track);
        					let tr = config.pc.addTransceiver(track, {
        						direction: "sendrecv",
        						streams: [stream],
        						sendEncodings: [
        							{ scalabilityMode: svc }
        						]
        					});
        					if(tr)
        						sender = tr.sender;
        				}
        				// Check if insertable streams are involved
        				if(sender && config.senderTransforms) {
        					var senderStreams = null;
        					if(RTCRtpSender.prototype.createEncodedStreams) {
        						senderStreams = sender.createEncodedStreams();
        					} else if(RTCRtpSender.prototype.createAudioEncodedStreams || RTCRtpSender.prototype.createEncodedVideoStreams) {
        						if(sender.track.kind === "audio" && config.senderTransforms["audio"]) {
        							senderStreams = sender.createEncodedAudioStreams();
        						} else if(sender.track.kind === "video" && config.senderTransforms["video"]) {
        							senderStreams = sender.createEncodedVideoStreams();
        						}
        					}
        					if(senderStreams) {
        						console.log(senderStreams);
        						if(senderStreams.readableStream && senderStreams.writableStream) {
        							senderStreams.readableStream
        								.pipeThrough(config.senderTransforms[sender.track.kind])
        								.pipeTo(senderStreams.writableStream);
        						} else if(senderStreams.readable && senderStreams.writable) {
        							senderStreams.readable
        								.pipeThrough(config.senderTransforms[sender.track.kind])
        								.pipeTo(senderStreams.writable);
        						}
        					}
        				}
        			});
        		}
        		// Any data channel to create?
        		if(isDataEnabled(media) && !config.dataChannel[Janus.dataChanDefaultLabel]) {
        			console.log("Creating default data channel");
        			createDataChannel(handleId, Janus.dataChanDefaultLabel, null, false);
        			config.pc.ondatachannel = function(event) {
        				console.log("Data channel created by Janus:", event);
        				createDataChannel(handleId, event.channel.label, event.channel.protocol, event.channel);
        			};
        		}
        		// If there's a new local stream, let's notify the application
        		if(config.myStream) {
        			var tracks = config.myStream.getTracks();
        			for(let i in tracks) {
        				var track = tracks[i];
        				track.onended = function(ev) {
        					// FIXME What does this event contain? Is there a reference to the track?
        					console.log("Local track removed:", ev);
        					try {
        						pluginHandle.onlocaltrack(ev.track, false);
        					} catch(e) {
        						Janus.error(e);
        					}
        				}
        				try {
        					pluginHandle.onlocaltrack(track, true);
        				} catch(e) {
        					Janus.error(e);
        				}
        			}
        		}
        		// Create offer/answer now
        		if(!jsep) {
        			createOffer(handleId, media, callbacks);
        		} else {
        			config.pc.setRemoteDescription(jsep)
        				.then(function() {
        					console.log("Remote description accepted!");
        					config.remoteSdp = jsep.sdp;
        					// Any trickle candidate we cached?
        					if(config.candidates && config.candidates.length > 0) {
        						for(var i = 0; i< config.candidates.length; i++) {
        							var candidate = config.candidates[i];
        							console.debug("Adding remote candidate:", candidate);
        							if(!candidate || candidate.completed === true) {
        								// end-of-candidates
        								config.pc.addIceCandidate(Janus.endOfCandidates);
        							} else {
        								// New candidate
        								config.pc.addIceCandidate(candidate);
        							}
        						}
        						config.candidates = [];
        					}
        					// Create the answer now
        					createAnswer(handleId, media, callbacks);
        				}, callbacks.error);
        		}
        	}

        	function prepareWebrtc(handleId, offer, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : webrtcError;
        		var jsep = callbacks.jsep;
        		if(offer && jsep) {
        			Janus.error("Provided a JSEP to a createOffer");
        			callbacks.error("Provided a JSEP to a createOffer");
        			return;
        		} else if(!offer && (!jsep || !jsep.type || !jsep.sdp)) {
        			Janus.error("A valid JSEP is required for createAnswer");
        			callbacks.error("A valid JSEP is required for createAnswer");
        			return;
        		}
        		/* Check that callbacks.media is a (not null) Object */
        		callbacks.media = (typeof callbacks.media === 'object' && callbacks.media) ? callbacks.media : { audio: true, video: true };
        		var media = callbacks.media;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		config.trickle = isTrickleEnabled(callbacks.trickle);
        		// Are we updating a session?
        		if(!config.pc) {
        			// Nope, new PeerConnection
        			media.update = false;
        			media.keepAudio = false;
        			media.keepVideo = false;
        		} else {
        			console.log("Updating existing media session");
        			media.update = true;
        			// Check if there's anything to add/remove/replace, or if we
        			// can go directly to preparing the new SDP offer or answer
        			if(callbacks.stream) {
        				// External stream: is this the same as the one we were using before?
        				if(callbacks.stream !== config.myStream) {
        					console.log("Renegotiation involves a new external stream");
        				}
        			} else {
        				// Check if there are changes on audio
        				if(media.addAudio) {
        					media.keepAudio = false;
        					media.replaceAudio = false;
        					media.removeAudio = false;
        					media.audioSend = true;
        					if(config.myStream && config.myStream.getAudioTracks() && config.myStream.getAudioTracks().length) {
        						Janus.error("Can't add audio stream, there already is one");
        						callbacks.error("Can't add audio stream, there already is one");
        						return;
        					}
        				} else if(media.removeAudio) {
        					media.keepAudio = false;
        					media.replaceAudio = false;
        					media.addAudio = false;
        					media.audioSend = false;
        				} else if(media.replaceAudio) {
        					media.keepAudio = false;
        					media.addAudio = false;
        					media.removeAudio = false;
        					media.audioSend = true;
        				}
        				if(!config.myStream) {
        					// No media stream: if we were asked to replace, it's actually an "add"
        					if(media.replaceAudio) {
        						media.keepAudio = false;
        						media.replaceAudio = false;
        						media.addAudio = true;
        						media.audioSend = true;
        					}
        					if(isAudioSendEnabled(media)) {
        						media.keepAudio = false;
        						media.addAudio = true;
        					}
        				} else {
        					if(!config.myStream.getAudioTracks() || config.myStream.getAudioTracks().length === 0) {
        						// No audio track: if we were asked to replace, it's actually an "add"
        						if(media.replaceAudio) {
        							media.keepAudio = false;
        							media.replaceAudio = false;
        							media.addAudio = true;
        							media.audioSend = true;
        						}
        						if(isAudioSendEnabled(media)) {
        							media.keepAudio = false;
        							media.addAudio = true;
        						}
        					} else {
        						// We have an audio track: should we keep it as it is?
        						if(isAudioSendEnabled(media) &&
        								!media.removeAudio && !media.replaceAudio) {
        							media.keepAudio = true;
        						}
        					}
        				}
        				// Check if there are changes on video
        				if(media.addVideo) {
        					media.keepVideo = false;
        					media.replaceVideo = false;
        					media.removeVideo = false;
        					media.videoSend = true;
        					if(config.myStream && config.myStream.getVideoTracks() && config.myStream.getVideoTracks().length) {
        						Janus.error("Can't add video stream, there already is one");
        						callbacks.error("Can't add video stream, there already is one");
        						return;
        					}
        				} else if(media.removeVideo) {
        					media.keepVideo = false;
        					media.replaceVideo = false;
        					media.addVideo = false;
        					media.videoSend = false;
        				} else if(media.replaceVideo) {
        					media.keepVideo = false;
        					media.addVideo = false;
        					media.removeVideo = false;
        					media.videoSend = true;
        				}
        				if(!config.myStream) {
        					// No media stream: if we were asked to replace, it's actually an "add"
        					if(media.replaceVideo) {
        						media.keepVideo = false;
        						media.replaceVideo = false;
        						media.addVideo = true;
        						media.videoSend = true;
        					}
        					if(isVideoSendEnabled(media)) {
        						media.keepVideo = false;
        						media.addVideo = true;
        					}
        				} else {
        					if(!config.myStream.getVideoTracks() || config.myStream.getVideoTracks().length === 0) {
        						// No video track: if we were asked to replace, it's actually an "add"
        						if(media.replaceVideo) {
        							media.keepVideo = false;
        							media.replaceVideo = false;
        							media.addVideo = true;
        							media.videoSend = true;
        						}
        						if(isVideoSendEnabled(media)) {
        							media.keepVideo = false;
        							media.addVideo = true;
        						}
        					} else {
        						// We have a video track: should we keep it as it is?
        						if(isVideoSendEnabled(media) && !media.removeVideo && !media.replaceVideo) {
        							media.keepVideo = true;
        						}
        					}
        				}
        				// Data channels can only be added
        				if(media.addData) {
        					media.data = true;
        				}
        			}
        			// If we're updating and keeping all tracks, let's skip the getUserMedia part
        			if((isAudioSendEnabled(media) && media.keepAudio) &&
        					(isVideoSendEnabled(media) && media.keepVideo)) {
        				pluginHandle.consentDialog(false);
        				streamsDone(handleId, jsep, media, callbacks, config.myStream);
        				return;
        			}
        		}
        		// If we're updating, check if we need to remove/replace one of the tracks
        		if(media.update && (!config.streamExternal || (config.streamExternal && (media.replaceAudio || media.replaceVideo)))) {
        			if(media.removeAudio || media.replaceAudio) {
        				if(config.myStream && config.myStream.getAudioTracks() && config.myStream.getAudioTracks().length) {
        					var at = config.myStream.getAudioTracks()[0];
        					console.log("Removing audio track:", at);
        					config.myStream.removeTrack(at);
        					try {
        						pluginHandle.onlocaltrack(at, false);
        					} catch(e) {
        						Janus.error(e);
        					}
        					try {
        						at.stop();
        					} catch(e) {}
        				}
        				if(config.pc.getSenders() && config.pc.getSenders().length) {
        					var ra = true;
        					if(media.replaceAudio && Janus.unifiedPlan) {
        						// We can use replaceTrack
        						ra = false;
        					}
        					if(ra) {
        						for(var asnd of config.pc.getSenders()) {
        							if(asnd && asnd.track && asnd.track.kind === "audio") {
        								console.log("Removing audio sender:", asnd);
        								config.pc.removeTrack(asnd);
        							}
        						}
        					}
        				}
        			}
        			if(media.removeVideo || media.replaceVideo) {
        				if(config.myStream && config.myStream.getVideoTracks() && config.myStream.getVideoTracks().length) {
        					var vt = config.myStream.getVideoTracks()[0];
        					console.log("Removing video track:", vt);
        					config.myStream.removeTrack(vt);
        					try {
        						pluginHandle.onlocaltrack(vt, false);
        					} catch(e) {
        						Janus.error(e);
        					}
        					try {
        						vt.stop();
        					} catch(e) {}
        				}
        				if(config.pc.getSenders() && config.pc.getSenders().length) {
        					var rv = true;
        					if(media.replaceVideo && Janus.unifiedPlan) {
        						// We can use replaceTrack
        						rv = false;
        					}
        					if(rv) {
        						for(var vsnd of config.pc.getSenders()) {
        							if(vsnd && vsnd.track && vsnd.track.kind === "video") {
        								console.log("Removing video sender:", vsnd);
        								config.pc.removeTrack(vsnd);
        							}
        						}
        					}
        				}
        			}
        		}
        		// Was a MediaStream object passed, or do we need to take care of that?
        		if(callbacks.stream) {
        			var stream = callbacks.stream;
        			console.log("MediaStream provided by the application");
        			console.debug(stream);
        			// If this is an update, let's check if we need to release the previous stream
        			if(media.update && config.myStream && config.myStream !== callbacks.stream && !config.streamExternal && !media.replaceAudio && !media.replaceVideo) {
        				// We're replacing a stream we captured ourselves with an external one
        				Janus.stopAllTracks(config.myStream);
        				config.myStream = null;
        			}
        			// Skip the getUserMedia part
        			config.streamExternal = true;
        			pluginHandle.consentDialog(false);
        			streamsDone(handleId, jsep, media, callbacks, stream);
        			return;
        		}
        		if(isAudioSendEnabled(media) || isVideoSendEnabled(media)) {
        			if(!Janus.isGetUserMediaAvailable()) {
        				callbacks.error("getUserMedia not available");
        				return;
        			}
        			var constraints = { mandatory: {}, optional: []};
        			pluginHandle.consentDialog(true);
        			var audioSupport = isAudioSendEnabled(media);
        			if(audioSupport && media && typeof media.audio === 'object')
        				audioSupport = media.audio;
        			var videoSupport = isVideoSendEnabled(media);
        			if(videoSupport && media) {
        				var simulcast = (callbacks.simulcast === true || callbacks.simulcast2 === true);
        				var svc = callbacks.svc;
        				if((simulcast || svc) && !jsep && !media.video)
        					media.video = "hires";
        				if(media.video && media.video != 'screen' && media.video != 'window') {
        					if(typeof media.video === 'object') {
        						videoSupport = media.video;
        					} else {
        						var width = 0;
        						var height = 0;
        						if(media.video === 'lowres') {
        							// Small resolution, 4:3
        							height = 240;
        							width = 320;
        						} else if(media.video === 'lowres-16:9') {
        							// Small resolution, 16:9
        							height = 180;
        							width = 320;
        						} else if(media.video === 'hires' || media.video === 'hires-16:9' || media.video === 'hdres') {
        							// High(HD) resolution is only 16:9
        							height = 720;
        							width = 1280;
        						} else if(media.video === 'fhdres') {
        							// Full HD resolution is only 16:9
        							height = 1080;
        							width = 1920;
        						} else if(media.video === '4kres') {
        							// 4K resolution is only 16:9
        							height = 2160;
        							width = 3840;
        						} else if(media.video === 'stdres') {
        							// Normal resolution, 4:3
        							height = 480;
        							width = 640;
        						} else if(media.video === 'stdres-16:9') {
        							// Normal resolution, 16:9
        							height = 360;
        							width = 640;
        						} else {
        							console.log("Default video setting is stdres 4:3");
        							height = 480;
        							width = 640;
        						}
        						console.log("Adding media constraint:", media.video);
        						videoSupport = {
        							'height': {'ideal': height},
        							'width': {'ideal': width}
        						};
        						console.log("Adding video constraint:", videoSupport);
        					}
        				} else if(media.video === 'screen' || media.video === 'window') {
        					if(navigator.mediaDevices && navigator.mediaDevices.getDisplayMedia) {
        						// The new experimental getDisplayMedia API is available, let's use that
        						// https://groups.google.com/forum/#!topic/discuss-webrtc/Uf0SrR4uxzk
        						// https://webrtchacks.com/chrome-screensharing-getdisplaymedia/
        						constraints.video = {};
        						if(media.screenshareFrameRate) {
        							constraints.video.frameRate = media.screenshareFrameRate;
        						}
        						if(media.screenshareHeight) {
        							constraints.video.height = media.screenshareHeight;
        						}
        						if(media.screenshareWidth) {
        							constraints.video.width = media.screenshareWidth;
        						}
        						constraints.audio = media.captureDesktopAudio;
        						navigator.mediaDevices.getDisplayMedia(constraints)
        							.then(function(stream) {
        								pluginHandle.consentDialog(false);
        								if(isAudioSendEnabled(media) && !media.keepAudio) {
        									navigator.mediaDevices.getUserMedia({ audio: true, video: false })
        									.then(function (audioStream) {
        										stream.addTrack(audioStream.getAudioTracks()[0]);
        										streamsDone(handleId, jsep, media, callbacks, stream);
        									})
        								} else {
        									streamsDone(handleId, jsep, media, callbacks, stream);
        								}
        							}, function (error) {
        								pluginHandle.consentDialog(false);
        								callbacks.error(error);
        							});
        						return;
        					}
        					// We're going to try and use the extension for Chrome 34+, the old approach
        					// for older versions of Chrome, or the experimental support in Firefox 33+
        					const callbackUserMedia = function(error, stream) {
        						pluginHandle.consentDialog(false);
        						if(error) {
        							callbacks.error(error);
        						} else {
        							streamsDone(handleId, jsep, media, callbacks, stream);
        						}
        					}
        					const getScreenMedia = function(constraints, gsmCallback, useAudio) {
        						console.log("Adding media constraint (screen capture)");
        						console.debug(constraints);
        						navigator.mediaDevices.getUserMedia(constraints)
        							.then(function(stream) {
        								if(useAudio) {
        									navigator.mediaDevices.getUserMedia({ audio: true, video: false })
        									.then(function (audioStream) {
        										stream.addTrack(audioStream.getAudioTracks()[0]);
        										gsmCallback(null, stream);
        									})
        								} else {
        									gsmCallback(null, stream);
        								}
        							})
        							.catch(function(error) { pluginHandle.consentDialog(false); gsmCallback(error); });
        					}
        					if(Janus.webRTCAdapter.browserDetails.browser === 'chrome') {
        						var chromever = Janus.webRTCAdapter.browserDetails.version;
        						var maxver = 33;
        						if(window.navigator.userAgent.match('Linux'))
        							maxver = 35;	// "known" crash in chrome 34 and 35 on linux
        						if(chromever >= 26 && chromever <= maxver) {
        							// Chrome 26->33 requires some awkward chrome://flags manipulation
        							constraints = {
        								video: {
        									mandatory: {
        										googLeakyBucket: true,
        										maxWidth: window.screen.width,
        										maxHeight: window.screen.height,
        										minFrameRate: media.screenshareFrameRate,
        										maxFrameRate: media.screenshareFrameRate,
        										chromeMediaSource: 'screen'
        									}
        								},
        								audio: isAudioSendEnabled(media) && !media.keepAudio
        							};
        							getScreenMedia(constraints, callbackUserMedia);
        						} else {
        							// Chrome 34+ requires an extension
        							Janus.extension.getScreen(function (error, sourceId) {
        								if (error) {
        									pluginHandle.consentDialog(false);
        									return callbacks.error(error);
        								}
        								constraints = {
        									audio: false,
        									video: {
        										mandatory: {
        											chromeMediaSource: 'desktop',
        											maxWidth: window.screen.width,
        											maxHeight: window.screen.height,
        											minFrameRate: media.screenshareFrameRate,
        											maxFrameRate: media.screenshareFrameRate,
        										},
        										optional: [
        											{googLeakyBucket: true},
        											{googTemporalLayeredScreencast: true}
        										]
        									}
        								};
        								constraints.video.mandatory.chromeMediaSourceId = sourceId;
        								getScreenMedia(constraints, callbackUserMedia,
        									isAudioSendEnabled(media) && !media.keepAudio);
        							});
        						}
        					} else if(Janus.webRTCAdapter.browserDetails.browser === 'firefox') {
        						if(Janus.webRTCAdapter.browserDetails.version >= 33) {
        							// Firefox 33+ has experimental support for screen sharing
        							constraints = {
        								video: {
        									mozMediaSource: media.video,
        									mediaSource: media.video
        								},
        								audio: isAudioSendEnabled(media) && !media.keepAudio
        							};
        							getScreenMedia(constraints, function (err, stream) {
        								callbackUserMedia(err, stream);
        								// Workaround for https://bugzilla.mozilla.org/show_bug.cgi?id=1045810
        								if (!err) {
        									var lastTime = stream.currentTime;
        									var polly = window.setInterval(function () {
        										if(!stream)
        											window.clearInterval(polly);
        										if(stream.currentTime == lastTime) {
        											window.clearInterval(polly);
        											if(stream.onended) {
        												stream.onended();
        											}
        										}
        										lastTime = stream.currentTime;
        									}, 500);
        								}
        							});
        						} else {
        							var error = new Error('NavigatorUserMediaError');
        							error.name = 'Your version of Firefox does not support screen sharing, please install Firefox 33 (or more recent versions)';
        							pluginHandle.consentDialog(false);
        							callbacks.error(error);
        							return;
        						}
        					}
        					return;
        				}
        			}
        			// If we got here, we're not screensharing
        			if(!media || media.video !== 'screen') {
        				// Check whether all media sources are actually available or not
        				navigator.mediaDevices.enumerateDevices().then(function(devices) {
        					var audioExist = devices.some(function(device) {
        						return device.kind === 'audioinput';
        					}),
        					videoExist = isScreenSendEnabled(media) || devices.some(function(device) {
        						return device.kind === 'videoinput';
        					});

        					// Check whether a missing device is really a problem
        					var audioSend = isAudioSendEnabled(media);
        					var videoSend = isVideoSendEnabled(media);
        					var needAudioDevice = isAudioSendRequired(media);
        					var needVideoDevice = isVideoSendRequired(media);
        					if(audioSend || videoSend || needAudioDevice || needVideoDevice) {
        						// We need to send either audio or video
        						var haveAudioDevice = audioSend ? audioExist : false;
        						var haveVideoDevice = videoSend ? videoExist : false;
        						if(!haveAudioDevice && !haveVideoDevice) {
        							// FIXME Should we really give up, or just assume recvonly for both?
        							pluginHandle.consentDialog(false);
        							callbacks.error('No capture device found');
        							return false;
        						} else if(!haveAudioDevice && needAudioDevice) {
        							pluginHandle.consentDialog(false);
        							callbacks.error('Audio capture is required, but no capture device found');
        							return false;
        						} else if(!haveVideoDevice && needVideoDevice) {
        							pluginHandle.consentDialog(false);
        							callbacks.error('Video capture is required, but no capture device found');
        							return false;
        						}
        					}

        					var gumConstraints = {
        						audio: (audioExist && !media.keepAudio) ? audioSupport : false,
        						video: (videoExist && !media.keepVideo) ? videoSupport : false
        					};
        					console.debug("getUserMedia constraints", gumConstraints);
        					if (!gumConstraints.audio && !gumConstraints.video) {
        						pluginHandle.consentDialog(false);
        						streamsDone(handleId, jsep, media, callbacks, stream);
        					} else {
        						navigator.mediaDevices.getUserMedia(gumConstraints)
        							.then(function(stream) {
        								pluginHandle.consentDialog(false);
        								streamsDone(handleId, jsep, media, callbacks, stream);
        							}).catch(function(error) {
        								pluginHandle.consentDialog(false);
        								callbacks.error({code: error.code, name: error.name, message: error.message});
        							});
        					}
        				})
        				.catch(function(error) {
        					pluginHandle.consentDialog(false);
        					callbacks.error(error);
        				});
        			}
        		} else {
        			// No need to do a getUserMedia, create offer/answer right away
        			streamsDone(handleId, jsep, media, callbacks);
        		}
        	}

        	function prepareWebrtcPeer(handleId, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : webrtcError;
        		callbacks.customizeSdp = (typeof callbacks.customizeSdp == "function") ? callbacks.customizeSdp : Janus.noop;
        		var jsep = callbacks.jsep;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		if(jsep) {
        			if(!config.pc) {
        				console.warn("Wait, no PeerConnection?? if this is an answer, use createAnswer and not handleRemoteJsep");
        				callbacks.error("No PeerConnection: if this is an answer, use createAnswer and not handleRemoteJsep");
        				return;
        			}
        			callbacks.customizeSdp(jsep);
        			config.pc.setRemoteDescription(jsep)
        				.then(function() {
        					console.log("Remote description accepted!");
        					config.remoteSdp = jsep.sdp;
        					// Any trickle candidate we cached?
        					if(config.candidates && config.candidates.length > 0) {
        						for(var i = 0; i< config.candidates.length; i++) {
        							var candidate = config.candidates[i];
        							console.debug("Adding remote candidate:", candidate);
        							if(!candidate || candidate.completed === true) {
        								// end-of-candidates
        								config.pc.addIceCandidate(Janus.endOfCandidates);
        							} else {
        								// New candidate
        								config.pc.addIceCandidate(candidate);
        							}
        						}
        						config.candidates = [];
        					}
        					// Done
        					callbacks.success();
        				}, callbacks.error);
        		} else {
        			callbacks.error("Invalid JSEP");
        		}
        	}

        	function createOffer(handleId, media, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		callbacks.customizeSdp = (typeof callbacks.customizeSdp == "function") ? callbacks.customizeSdp : Janus.noop;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		var simulcast = (callbacks.simulcast === true || callbacks.simulcast2 === true);
        		if(!simulcast) {
        			console.log("Creating offer (iceDone=" + config.iceDone + ")");
        		} else {
        			console.log("Creating offer (iceDone=" + config.iceDone + ", simulcast=" + simulcast + ")");
        		}
        		// https://code.google.com/p/webrtc/issues/detail?id=3508
        		var mediaConstraints = {};
        		if(Janus.unifiedPlan) {
        			// We can use Transceivers
        			var audioTransceiver = null, videoTransceiver = null;
        			var transceivers = config.pc.getTransceivers();
        			if(transceivers && transceivers.length > 0) {
        				for(var t of transceivers) {
        					if((t.sender && t.sender.track && t.sender.track.kind === "audio") ||
        							(t.receiver && t.receiver.track && t.receiver.track.kind === "audio")) {
        						if(!audioTransceiver) {
        							audioTransceiver = t;
        						}
        						continue;
        					}
        					if((t.sender && t.sender.track && t.sender.track.kind === "video") ||
        							(t.receiver && t.receiver.track && t.receiver.track.kind === "video")) {
        						if(!videoTransceiver) {
        							videoTransceiver = t;
        						}
        						continue;
        					}
        				}
        			}
        			// Handle audio (and related changes, if any)
        			var audioSend = isAudioSendEnabled(media);
        			var audioRecv = isAudioRecvEnabled(media);
        			if(!audioSend && !audioRecv) {
        				// Audio disabled: have we removed it?
        				if(media.removeAudio && audioTransceiver) {
        					if (audioTransceiver.setDirection) {
        						audioTransceiver.setDirection("inactive");
        					} else {
        						audioTransceiver.direction = "inactive";
        					}
        					console.log("Setting audio transceiver to inactive:", audioTransceiver);
        				}
        			} else {
        				// Take care of audio m-line
        				if(audioSend && audioRecv) {
        					if(audioTransceiver) {
        						if (audioTransceiver.setDirection) {
        							audioTransceiver.setDirection("sendrecv");
        						} else {
        							audioTransceiver.direction = "sendrecv";
        						}
        						console.log("Setting audio transceiver to sendrecv:", audioTransceiver);
        					}
        				} else if(audioSend && !audioRecv) {
        					if(audioTransceiver) {
        						if (audioTransceiver.setDirection) {
        							audioTransceiver.setDirection("sendonly");
        						} else {
        							audioTransceiver.direction = "sendonly";
        						}
        						console.log("Setting audio transceiver to sendonly:", audioTransceiver);
        					}
        				} else if(!audioSend && audioRecv) {
        					if(audioTransceiver) {
        						if (audioTransceiver.setDirection) {
        							audioTransceiver.setDirection("recvonly");
        						} else {
        							audioTransceiver.direction = "recvonly";
        						}
        						console.log("Setting audio transceiver to recvonly:", audioTransceiver);
        					} else {
        						// In theory, this is the only case where we might not have a transceiver yet
        						audioTransceiver = config.pc.addTransceiver("audio", { direction: "recvonly" });
        						console.log("Adding recvonly audio transceiver:", audioTransceiver);
        					}
        				}
        			}
        			// Handle video (and related changes, if any)
        			var videoSend = isVideoSendEnabled(media);
        			var videoRecv = isVideoRecvEnabled(media);
        			if(!videoSend && !videoRecv) {
        				// Video disabled: have we removed it?
        				if(media.removeVideo && videoTransceiver) {
        					if (videoTransceiver.setDirection) {
        						videoTransceiver.setDirection("inactive");
        					} else {
        						videoTransceiver.direction = "inactive";
        					}
        					console.log("Setting video transceiver to inactive:", videoTransceiver);
        				}
        			} else {
        				// Take care of video m-line
        				if(videoSend && videoRecv) {
        					if(videoTransceiver) {
        						if (videoTransceiver.setDirection) {
        							videoTransceiver.setDirection("sendrecv");
        						} else {
        							videoTransceiver.direction = "sendrecv";
        						}
        						console.log("Setting video transceiver to sendrecv:", videoTransceiver);
        					}
        				} else if(videoSend && !videoRecv) {
        					if(videoTransceiver) {
        						if (videoTransceiver.setDirection) {
        							videoTransceiver.setDirection("sendonly");
        						} else {
        							videoTransceiver.direction = "sendonly";
        						}
        						console.log("Setting video transceiver to sendonly:", videoTransceiver);
        					}
        				} else if(!videoSend && videoRecv) {
        					if(videoTransceiver) {
        						if (videoTransceiver.setDirection) {
        							videoTransceiver.setDirection("recvonly");
        						} else {
        							videoTransceiver.direction = "recvonly";
        						}
        						console.log("Setting video transceiver to recvonly:", videoTransceiver);
        					} else {
        						// In theory, this is the only case where we might not have a transceiver yet
        						videoTransceiver = config.pc.addTransceiver("video", { direction: "recvonly" });
        						console.log("Adding recvonly video transceiver:", videoTransceiver);
        					}
        				}
        			}
        		} else {
        			mediaConstraints["offerToReceiveAudio"] = isAudioRecvEnabled(media);
        			mediaConstraints["offerToReceiveVideo"] = isVideoRecvEnabled(media);
        		}
        		var iceRestart = (callbacks.iceRestart === true);
        		if(iceRestart) {
        			mediaConstraints["iceRestart"] = true;
        		}
        		console.debug(mediaConstraints);
        		// Check if this is Firefox and we've been asked to do simulcasting
        		var sendVideo = isVideoSendEnabled(media);
        		if(sendVideo && simulcast && Janus.webRTCAdapter.browserDetails.browser === "firefox") {
        			// FIXME Based on https://gist.github.com/voluntas/088bc3cc62094730647b
        			console.log("Enabling Simulcasting for Firefox (RID)");
        			var sender = config.pc.getSenders().find(function(s) {return s.track && s.track.kind === "video"});
        			if(sender) {
        				var parameters = sender.getParameters();
        				if(!parameters) {
        					parameters = {};
        				}
        				var maxBitrates = getMaxBitrates(callbacks.simulcastMaxBitrates);
        				parameters.encodings = callbacks.sendEncodings || [
        					{ rid: "h", active: true, maxBitrate: maxBitrates.high },
        					{ rid: "m", active: true, maxBitrate: maxBitrates.medium, scaleResolutionDownBy: 2 },
        					{ rid: "l", active: true, maxBitrate: maxBitrates.low, scaleResolutionDownBy: 4 }
        				];
        				sender.setParameters(parameters);
        			}
        		}
        		config.pc.createOffer(mediaConstraints)
        			.then(function(offer) {
        				console.debug(offer);
        				// JSON.stringify doesn't work on some WebRTC objects anymore
        				// See https://code.google.com/p/chromium/issues/detail?id=467366
        				var jsep = {
        					"type": offer.type,
        					"sdp": offer.sdp
        				};
        				callbacks.customizeSdp(jsep);
        				offer.sdp = jsep.sdp;
        				console.log("Setting local description");
        				if(sendVideo && simulcast && !Janus.unifiedPlan) {
        					// We only do simulcast via SDP munging on older versions of Chrome and Safari
        					if(Janus.webRTCAdapter.browserDetails.browser === "chrome" ||
        							Janus.webRTCAdapter.browserDetails.browser === "safari") {
        						console.log("Enabling Simulcasting for Chrome (SDP munging)");
        						offer.sdp = mungeSdpForSimulcasting(offer.sdp);
        					}
        				}
        				config.mySdp = {
        					type: "offer",
        					sdp: offer.sdp
        				};
        				config.pc.setLocalDescription(offer)
        					.catch(callbacks.error);
        				config.mediaConstraints = mediaConstraints;
        				if(!config.iceDone && !config.trickle) {
        					// Don't do anything until we have all candidates
        					console.log("Waiting for all candidates...");
        					return;
        				}
        				// If transforms are present, notify Janus that the media is end-to-end encrypted
        				if(config.senderTransforms || config.receiverTransforms) {
        					offer["e2ee"] = true;
        				}
        				callbacks.success(offer);
        			}, callbacks.error);
        	}

        	function createAnswer(handleId, media, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		callbacks.customizeSdp = (typeof callbacks.customizeSdp == "function") ? callbacks.customizeSdp : Janus.noop;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			callbacks.error("Invalid handle");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		var simulcast = (callbacks.simulcast === true || callbacks.simulcast2 === true);
        		if(!simulcast) {
        			console.log("Creating answer (iceDone=" + config.iceDone + ")");
        		} else {
        			console.log("Creating answer (iceDone=" + config.iceDone + ", simulcast=" + simulcast + ")");
        		}
        		var mediaConstraints = null;
        		if(Janus.unifiedPlan) {
        			// We can use Transceivers
        			mediaConstraints = {};
        			var audioTransceiver = null, videoTransceiver = null;
        			var transceivers = config.pc.getTransceivers();
        			if(transceivers && transceivers.length > 0) {
        				for(var t of transceivers) {
        					if((t.sender && t.sender.track && t.sender.track.kind === "audio") ||
        							(t.receiver && t.receiver.track && t.receiver.track.kind === "audio")) {
        						if(!audioTransceiver)
        							audioTransceiver = t;
        						continue;
        					}
        					if((t.sender && t.sender.track && t.sender.track.kind === "video") ||
        							(t.receiver && t.receiver.track && t.receiver.track.kind === "video")) {
        						if(!videoTransceiver)
        							videoTransceiver = t;
        						continue;
        					}
        				}
        			}
        			// Handle audio (and related changes, if any)
        			var audioSend = isAudioSendEnabled(media);
        			var audioRecv = isAudioRecvEnabled(media);
        			if(!audioSend && !audioRecv) {
        				// Audio disabled: have we removed it?
        				if(media.removeAudio && audioTransceiver) {
        					try {
        						if (audioTransceiver.setDirection) {
        							audioTransceiver.setDirection("inactive");
        						} else {
        							audioTransceiver.direction = "inactive";
        						}
        						console.log("Setting audio transceiver to inactive:", audioTransceiver);
        					} catch(e) {
        						Janus.error(e);
        					}
        				}
        			} else {
        				// Take care of audio m-line
        				if(audioSend && audioRecv) {
        					if(audioTransceiver) {
        						try {
        							if (audioTransceiver.setDirection) {
        								audioTransceiver.setDirection("sendrecv");
        							} else {
        								audioTransceiver.direction = "sendrecv";
        							}
        							console.log("Setting audio transceiver to sendrecv:", audioTransceiver);
        						} catch(e) {
        							Janus.error(e);
        						}
        					}
        				} else if(audioSend && !audioRecv) {
        					try {
        						if(audioTransceiver) {
        							if (audioTransceiver.setDirection) {
        								audioTransceiver.setDirection("sendonly");
        							} else {
        								audioTransceiver.direction = "sendonly";
        							}
        							console.log("Setting audio transceiver to sendonly:", audioTransceiver);
        						}
        					} catch(e) {
        						Janus.error(e);
        					}
        				} else if(!audioSend && audioRecv) {
        					if(audioTransceiver) {
        						try {
        							if (audioTransceiver.setDirection) {
        								audioTransceiver.setDirection("recvonly");
        							} else {
        								audioTransceiver.direction = "recvonly";
        							}
        							console.log("Setting audio transceiver to recvonly:", audioTransceiver);
        						} catch(e) {
        							Janus.error(e);
        						}
        					} else {
        						// In theory, this is the only case where we might not have a transceiver yet
        						audioTransceiver = config.pc.addTransceiver("audio", { direction: "recvonly" });
        						console.log("Adding recvonly audio transceiver:", audioTransceiver);
        					}
        				}
        			}
        			// Handle video (and related changes, if any)
        			var videoSend = isVideoSendEnabled(media);
        			var videoRecv = isVideoRecvEnabled(media);
        			if(!videoSend && !videoRecv) {
        				// Video disabled: have we removed it?
        				if(media.removeVideo && videoTransceiver) {
        					try {
        						if (videoTransceiver.setDirection) {
        							videoTransceiver.setDirection("inactive");
        						} else {
        							videoTransceiver.direction = "inactive";
        						}
        						console.log("Setting video transceiver to inactive:", videoTransceiver);
        					} catch(e) {
        						Janus.error(e);
        					}
        				}
        			} else {
        				// Take care of video m-line
        				if(videoSend && videoRecv) {
        					if(videoTransceiver) {
        						try {
        							if (videoTransceiver.setDirection) {
        								videoTransceiver.setDirection("sendrecv");
        							} else {
        								videoTransceiver.direction = "sendrecv";
        							}
        							console.log("Setting video transceiver to sendrecv:", videoTransceiver);
        						} catch(e) {
        							Janus.error(e);
        						}
        					}
        				} else if(videoSend && !videoRecv) {
        					if(videoTransceiver) {
        						try {
        							if (videoTransceiver.setDirection) {
        								videoTransceiver.setDirection("sendonly");
        							} else {
        								videoTransceiver.direction = "sendonly";
        							}
        							console.log("Setting video transceiver to sendonly:", videoTransceiver);
        						} catch(e) {
        							Janus.error(e);
        						}
        					}
        				} else if(!videoSend && videoRecv) {
        					if(videoTransceiver) {
        						try {
        							if (videoTransceiver.setDirection) {
        								videoTransceiver.setDirection("recvonly");
        							} else {
        								videoTransceiver.direction = "recvonly";
        							}
        							console.log("Setting video transceiver to recvonly:", videoTransceiver);
        						} catch(e) {
        							Janus.error(e);
        						}
        					} else {
        						// In theory, this is the only case where we might not have a transceiver yet
        						videoTransceiver = config.pc.addTransceiver("video", { direction: "recvonly" });
        						console.log("Adding recvonly video transceiver:", videoTransceiver);
        					}
        				}
        			}
        		} else {
        			if(Janus.webRTCAdapter.browserDetails.browser === "firefox" || Janus.webRTCAdapter.browserDetails.browser === "edge") {
        				mediaConstraints = {
        					offerToReceiveAudio: isAudioRecvEnabled(media),
        					offerToReceiveVideo: isVideoRecvEnabled(media)
        				};
        			} else {
        				mediaConstraints = {
        					mandatory: {
        						OfferToReceiveAudio: isAudioRecvEnabled(media),
        						OfferToReceiveVideo: isVideoRecvEnabled(media)
        					}
        				};
        			}
        		}
        		console.debug(mediaConstraints);
        		// Check if this is Firefox and we've been asked to do simulcasting
        		var sendVideo = isVideoSendEnabled(media);
        		if(sendVideo && simulcast && Janus.webRTCAdapter.browserDetails.browser === "firefox") {
        			// FIXME Based on https://gist.github.com/voluntas/088bc3cc62094730647b
        			console.log("Enabling Simulcasting for Firefox (RID)");
        			var sender = config.pc.getSenders()[1];
        			console.log(sender);
        			var parameters = sender.getParameters();
        			console.log(parameters);

        			var maxBitrates = getMaxBitrates(callbacks.simulcastMaxBitrates);
        			sender.setParameters({encodings: callbacks.sendEncodings || [
        				{ rid: "h", active: true, maxBitrate: maxBitrates.high },
        				{ rid: "m", active: true, maxBitrate: maxBitrates.medium, scaleResolutionDownBy: 2},
        				{ rid: "l", active: true, maxBitrate: maxBitrates.low, scaleResolutionDownBy: 4}
        			]});
        		}
        		config.pc.createAnswer(mediaConstraints)
        			.then(function(answer) {
        				console.debug(answer);
        				// JSON.stringify doesn't work on some WebRTC objects anymore
        				// See https://code.google.com/p/chromium/issues/detail?id=467366
        				var jsep = {
        					"type": answer.type,
        					"sdp": answer.sdp
        				};
        				callbacks.customizeSdp(jsep);
        				answer.sdp = jsep.sdp;
        				console.log("Setting local description");
        				if(sendVideo && simulcast && !Janus.unifiedPlan) {
        					// We only do simulcast via SDP munging on older versions of Chrome and Safari
        					if(Janus.webRTCAdapter.browserDetails.browser === "chrome") {
        						// FIXME Apparently trying to simulcast when answering breaks video in Chrome...
        						//~ console.log("Enabling Simulcasting for Chrome (SDP munging)");
        						//~ answer.sdp = mungeSdpForSimulcasting(answer.sdp);
        						console.warn("simulcast=true, but this is an answer, and video breaks in Chrome if we enable it");
        					}
        				}
        				config.mySdp = {
        					type: "answer",
        					sdp: answer.sdp
        				};
        				config.pc.setLocalDescription(answer)
        					.catch(callbacks.error);
        				config.mediaConstraints = mediaConstraints;
        				if(!config.iceDone && !config.trickle) {
        					// Don't do anything until we have all candidates
        					console.log("Waiting for all candidates...");
        					return;
        				}
        				// If transforms are present, notify Janus that the media is end-to-end encrypted
        				if(config.senderTransforms || config.receiverTransforms) {
        					answer["e2ee"] = true;
        				}
        				callbacks.success(answer);
        			}, callbacks.error);
        	}

        	function sendSDP(handleId, callbacks) {
        		callbacks = callbacks || {};
        		callbacks.success = (typeof callbacks.success == "function") ? callbacks.success : Janus.noop;
        		callbacks.error = (typeof callbacks.error == "function") ? callbacks.error : Janus.noop;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle, not sending anything");
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		console.log("Sending offer/answer SDP...");
        		if(!config.mySdp) {
        			console.warn("Local SDP instance is invalid, not sending anything...");
        			return;
        		}
        		config.mySdp = {
        			"type": config.pc.localDescription.type,
        			"sdp": config.pc.localDescription.sdp
        		};
        		if(config.trickle === false)
        			config.mySdp["trickle"] = false;
        		console.debug(callbacks);
        		config.sdpSent = true;
        		callbacks.success(config.mySdp);
        	}

        	function getVolume(handleId, mid, remote, result) {
        		result = (typeof result == "function") ? result : Janus.noop;
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			result(0);
        			return;
        		}
        		var stream = remote ? "remote" : "local";
        		var config = pluginHandle.webrtcStuff;
        		if(!config.volume[stream])
        			config.volume[stream] = { value: 0 };
        		// Start getting the volume, if audioLevel in getStats is supported (apparently
        		// they're only available in Chrome/Safari right now: https://webrtc-stats.callstats.io/)
        		if(config.pc.getStats && (Janus.webRTCAdapter.browserDetails.browser === "chrome" ||
        				Janus.webRTCAdapter.browserDetails.browser === "safari")) {
        			if(remote && !config.remoteStream) {
        				console.warn("Remote stream unavailable");
        				result(0);
        				return;
        			} else if(!remote && !config.myStream) {
        				console.warn("Local stream unavailable");
        				result(0);
        				return;
        			}
        			// Are we interested in a mid in particular? (only if transceivers are in use)
        			var query = config.pc;
        			if(mid && Janus.unifiedPlan) {
        				var transceiver = config.pc.getTransceivers()
        					.find(t => (t.mid === mid && t.receiver.track.kind === "audio"));
        				if(!transceiver) {
        					console.warn("No audio transceiver with mid " + mid);
        					result(0);
        					return;
        				}
        				if(remote && !transceiver.receiver) {
        					console.warn("Remote transceiver track unavailable");
        					result(0);
        					return;
        				} else if(!remote && !transceiver.sender) {
        					console.warn("Local transceiver track unavailable");
        					result(0);
        					return;
        				}
        				query = remote ? transceiver.receiver : transceiver.sender;
        			}
        			query.getStats()
        				.then(function(stats) {
        					stats.forEach(function (res) {
        						if(!res || res.kind !== "audio")
        							return;
        						if((remote && !res.remoteSource) || (!remote && res.type !== "media-source"))
        							return;
        						result(res.audioLevel ? res.audioLevel : 0);
        					});
        				});
        			return config.volume[stream].value;
        		} else {
        			// audioInputLevel and audioOutputLevel seem only available in Chrome? audioLevel
        			// seems to be available on Chrome and Firefox, but they don't seem to work
        			console.warn("Getting the " + stream + " volume unsupported by browser");
        			result(0);
        			return;
        		}
        	}

        	function isMuted(handleId, mid, video) {
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			return true;
        		}
        		var config = pluginHandle.webrtcStuff;
        		if(!config.pc) {
        			console.warn("Invalid PeerConnection");
        			return true;
        		}
        		if(!config.myStream) {
        			console.warn("Invalid local MediaStream");
        			return true;
        		}
        		if(video) {
        			// Check video track
        			if(!config.myStream.getVideoTracks() || config.myStream.getVideoTracks().length === 0) {
        				console.warn("No video track");
        				return true;
        			}
        			if(mid && Janus.unifiedPlan) {
        				let transceiver = config.pc.getTransceivers()
        					.find(t => (t.mid === mid && t.receiver.track.kind === "video"));
        				if(!transceiver) {
        					console.warn("No video transceiver with mid " + mid);
        					return true;
        				}
        				if(!transceiver.sender || !transceiver.sender.track) {
        					console.warn("No video sender with mid " + mid);
        					return true;
        				}
        				return !transceiver.sender.track.enabled;
        			} else {
        				return !config.myStream.getVideoTracks()[0].enabled;
        			}
        		} else {
        			// Check audio track
        			if(!config.myStream.getAudioTracks() || config.myStream.getAudioTracks().length === 0) {
        				console.warn("No audio track");
        				return true;
        			}
        			if(mid && Janus.unifiedPlan) {
        				let transceiver = config.pc.getTransceivers()
        					.find(t => (t.mid === mid && t.receiver.track.kind === "audio"));
        				if(!transceiver) {
        					console.warn("No audio transceiver with mid " + mid);
        					return true;
        				}
        				if(!transceiver.sender || !transceiver.sender.track) {
        					console.warn("No audio sender with mid " + mid);
        					return true;
        				}
        				return !transceiver.sender.track.enabled;
        			} else {
        				return !config.myStream.getAudioTracks()[0].enabled;
        			}
        		}
        	}

        	function mute(handleId, mid, video, mute) {
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			return false;
        		}
        		var config = pluginHandle.webrtcStuff;
        		if(!config.pc) {
        			console.warn("Invalid PeerConnection");
        			return false;
        		}
        		if(!config.myStream) {
        			console.warn("Invalid local MediaStream");
        			return false;
        		}
        		if(video) {
        			// Mute/unmute video track
        			if(!config.myStream.getVideoTracks() || config.myStream.getVideoTracks().length === 0) {
        				console.warn("No video track");
        				return false;
        			}
        			if(mid && Janus.unifiedPlan) {
        				let transceiver = config.pc.getTransceivers()
        					.find(t => (t.mid === mid && t.receiver.track.kind === "video"));
        				if(!transceiver) {
        					console.warn("No video transceiver with mid " + mid);
        					return false;
        				}
        				if(!transceiver.sender || !transceiver.sender.track) {
        					console.warn("No video sender with mid " + mid);
        					return false;
        				}
        				transceiver.sender.track.enabled = mute ? false : true;
        			} else {
        				config.myStream.getVideoTracks()[0].enabled = mute ? false : true;
        			}
        		} else {
        			// Mute/unmute audio track
        			if(!config.myStream.getAudioTracks() || config.myStream.getAudioTracks().length === 0) {
        				console.warn("No audio track");
        				return false;
        			}
        			if(mid && Janus.unifiedPlan) {
        				let transceiver = config.pc.getTransceivers()
        					.find(t => (t.mid === mid && t.receiver.track.kind === "audio"));
        				if(!transceiver) {
        					console.warn("No audio transceiver with mid " + mid);
        					return false;
        				}
        				if(!transceiver.sender || !transceiver.sender.track) {
        					console.warn("No audio sender with mid " + mid);
        					return false;
        				}
        				transceiver.sender.track.enabled = mute ? false : true;
        			} else {
        				config.myStream.getAudioTracks()[0].enabled = mute ? false : true;
        			}
        		}
        		return true;
        	}

        	function getBitrate(handleId, mid) {
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle || !pluginHandle.webrtcStuff) {
        			console.warn("Invalid handle");
        			return "Invalid handle";
        		}
        		var config = pluginHandle.webrtcStuff;
        		if(!config.pc)
        			return "Invalid PeerConnection";
        		// Start getting the bitrate, if getStats is supported
        		if(config.pc.getStats) {
        			var query = config.pc;
        			var target = mid ? mid : "default";
        			if(mid && Janus.unifiedPlan) {
        				var transceiver = config.pc.getTransceivers()
        					.find(t => (t.mid === mid && t.receiver.track.kind === "video"));
        				if(!transceiver) {
        					console.warn("No video transceiver with mid " + mid);
        					return ("No video transceiver with mid " + mid);
        				}
        				if(!transceiver.receiver) {
        					console.warn("No video receiver with mid " + mid);
        					return ("No video receiver with mid " + mid);
        				}
        				query = transceiver.receiver;
        			}
        			if(!config.bitrate[target]) {
        				config.bitrate[target] = {
        					timer: null,
        					bsnow: null,
        					bsbefore: null,
        					tsnow: null,
        					tsbefore: null,
        					value: "0 kbits/sec"
        				};
        			}
        			if(!config.bitrate[target].timer) {
        				console.log("Starting bitrate timer" + (mid ? (" for mid " + mid) : "") + " (via getStats)");
        				config.bitrate[target].timer = setInterval(function() {
        					query.getStats()
        						.then(function(stats) {
        							stats.forEach(function (res) {
        								if(!res)
        									return;
        								var inStats = false;
        								// Check if these are statistics on incoming media
        								if((res.mediaType === "video" || res.id.toLowerCase().indexOf("video") > -1) &&
        										res.type === "inbound-rtp" && res.id.indexOf("rtcp") < 0) {
        									// New stats
        									inStats = true;
        								} else if(res.type == 'ssrc' && res.bytesReceived &&
        										(res.googCodecName === "VP8" || res.googCodecName === "")) {
        									// Older Chromer versions
        									inStats = true;
        								}
        								// Parse stats now
        								if(inStats) {
        									config.bitrate[target].bsnow = res.bytesReceived;
        									config.bitrate[target].tsnow = res.timestamp;
        									if(config.bitrate[target].bsbefore === null || config.bitrate[target].tsbefore === null) {
        										// Skip this round
        										config.bitrate[target].bsbefore = config.bitrate[target].bsnow;
        										config.bitrate[target].tsbefore = config.bitrate[target].tsnow;
        									} else {
        										// Calculate bitrate
        										var timePassed = config.bitrate[target].tsnow - config.bitrate[target].tsbefore;
        										if(Janus.webRTCAdapter.browserDetails.browser === "safari")
        											timePassed = timePassed/1000;	// Apparently the timestamp is in microseconds, in Safari
        										var bitRate = Math.round((config.bitrate[target].bsnow - config.bitrate[target].bsbefore) * 8 / timePassed);
        										if(Janus.webRTCAdapter.browserDetails.browser === "safari")
        											bitRate = parseInt(bitRate/1000);
        										config.bitrate[target].value = bitRate + ' kbits/sec';
        										//~ console.log("Estimated bitrate is " + config.bitrate.value);
        										config.bitrate[target].bsbefore = config.bitrate[target].bsnow;
        										config.bitrate[target].tsbefore = config.bitrate[target].tsnow;
        									}
        								}
        							});
        						});
        				}, 1000);
        				return "0 kbits/sec";	// We don't have a bitrate value yet
        			}
        			return config.bitrate[target].value;
        		} else {
        			console.warn("Getting the video bitrate unsupported by browser");
        			return "Feature unsupported by browser";
        		}
        	}

        	function webrtcError(error) {
        		Janus.error("WebRTC error:", error);
        	}

        	function cleanupWebrtc(handleId, hangupRequest) {
        		console.log("Cleaning WebRTC stuff");
        		var pluginHandle = pluginHandles[handleId];
        		if(!pluginHandle) {
        			// Nothing to clean
        			return;
        		}
        		var config = pluginHandle.webrtcStuff;
        		if(config) {
        			if(hangupRequest === true) {
        				// Send a hangup request (we don't really care about the response)
        				var request = { "janus": "hangup", "transaction": Janus.randomString(12) };
        				if(pluginHandle.token)
        					request["token"] = pluginHandle.token;
        				if(apisecret)
        					request["apisecret"] = apisecret;
        				console.debug("Sending hangup request (handle=" + handleId + "):");
        				console.debug(request);
        				if(websockets) {
        					request["session_id"] = sessionId;
        					request["handle_id"] = handleId;
        					ws.send(JSON.stringify(request));
        				} else {
        					Janus.httpAPICall(server + "/" + sessionId + "/" + handleId, {
        						verb: 'POST',
        						withCredentials: withCredentials,
        						body: request
        					});
        				}
        			}
        			// Cleanup stack
        			config.remoteStream = null;
        			if(config.volume) {
        				if(config.volume["local"] && config.volume["local"].timer)
        					clearInterval(config.volume["local"].timer);
        				if(config.volume["remote"] && config.volume["remote"].timer)
        					clearInterval(config.volume["remote"].timer);
        			}
        			for(var i in config.bitrate) {
        				if(config.bitrate[i].timer)
        					clearInterval(config.bitrate[i].timer);
        			}
        			config.bitrate = {};
        			if(!config.streamExternal && config.myStream) {
        				console.log("Stopping local stream tracks");
        				Janus.stopAllTracks(config.myStream);
        			}
        			config.streamExternal = false;
        			config.myStream = null;
        			// Close PeerConnection
        			try {
        				config.pc.close();
        			} catch(e) {
        				// Do nothing
        			}
        			config.pc = null;
        			config.candidates = null;
        			config.mySdp = null;
        			config.remoteSdp = null;
        			config.iceDone = false;
        			config.dataChannel = {};
        			config.dtmfSender = null;
        			config.senderTransforms = null;
        			config.receiverTransforms = null;
        		}
        		pluginHandle.oncleanup();
        	}

        	// Helper method to munge an SDP to enable simulcasting (Chrome only)
        	function mungeSdpForSimulcasting(sdp) {
        		// Let's munge the SDP to add the attributes for enabling simulcasting
        		// (based on https://gist.github.com/ggarber/a19b4c33510028b9c657)
        		var lines = sdp.split("\r\n");
        		var video = false;
        		var ssrc = [ -1 ], ssrc_fid = [ -1 ];
        		var cname = null, msid = null, mslabel = null, label = null;
        		var insertAt = -1;
        		for(let i=0; i<lines.length; i++) {
        			const mline = lines[i].match(/m=(\w+) */);
        			if(mline) {
        				const medium = mline[1];
        				if(medium === "video") {
        					// New video m-line: make sure it's the first one
        					if(ssrc[0] < 0) {
        						video = true;
        					} else {
        						// We're done, let's add the new attributes here
        						insertAt = i;
        						break;
        					}
        				} else {
        					// New non-video m-line: do we have what we were looking for?
        					if(ssrc[0] > -1) {
        						// We're done, let's add the new attributes here
        						insertAt = i;
        						break;
        					}
        				}
        				continue;
        			}
        			if(!video)
        				continue;
        			var sim = lines[i].match(/a=ssrc-group:SIM (\d+) (\d+) (\d+)/);
        			if(sim) {
        				console.warn("The SDP already contains a SIM attribute, munging will be skipped");
        				return sdp;
        			}
        			var fid = lines[i].match(/a=ssrc-group:FID (\d+) (\d+)/);
        			if(fid) {
        				ssrc[0] = fid[1];
        				ssrc_fid[0] = fid[2];
        				lines.splice(i, 1); i--;
        				continue;
        			}
        			if(ssrc[0]) {
        				var match = lines[i].match('a=ssrc:' + ssrc[0] + ' cname:(.+)')
        				if(match) {
        					cname = match[1];
        				}
        				match = lines[i].match('a=ssrc:' + ssrc[0] + ' msid:(.+)')
        				if(match) {
        					msid = match[1];
        				}
        				match = lines[i].match('a=ssrc:' + ssrc[0] + ' mslabel:(.+)')
        				if(match) {
        					mslabel = match[1];
        				}
        				match = lines[i].match('a=ssrc:' + ssrc[0] + ' label:(.+)')
        				if(match) {
        					label = match[1];
        				}
        				if(lines[i].indexOf('a=ssrc:' + ssrc_fid[0]) === 0) {
        					lines.splice(i, 1); i--;
        					continue;
        				}
        				if(lines[i].indexOf('a=ssrc:' + ssrc[0]) === 0) {
        					lines.splice(i, 1); i--;
        					continue;
        				}
        			}
        			if(lines[i].length == 0) {
        				lines.splice(i, 1); i--;
        				continue;
        			}
        		}
        		if(ssrc[0] < 0) {
        			// Couldn't find a FID attribute, let's just take the first video SSRC we find
        			insertAt = -1;
        			video = false;
        			for(let i=0; i<lines.length; i++) {
        				const mline = lines[i].match(/m=(\w+) */);
        				if(mline) {
        					const medium = mline[1];
        					if(medium === "video") {
        						// New video m-line: make sure it's the first one
        						if(ssrc[0] < 0) {
        							video = true;
        						} else {
        							// We're done, let's add the new attributes here
        							insertAt = i;
        							break;
        						}
        					} else {
        						// New non-video m-line: do we have what we were looking for?
        						if(ssrc[0] > -1) {
        							// We're done, let's add the new attributes here
        							insertAt = i;
        							break;
        						}
        					}
        					continue;
        				}
        				if(!video)
        					continue;
        				if(ssrc[0] < 0) {
        					var value = lines[i].match(/a=ssrc:(\d+)/);
        					if(value) {
        						ssrc[0] = value[1];
        						lines.splice(i, 1); i--;
        						continue;
        					}
        				} else {
        					let match = lines[i].match('a=ssrc:' + ssrc[0] + ' cname:(.+)')
        					if(match) {
        						cname = match[1];
        					}
        					match = lines[i].match('a=ssrc:' + ssrc[0] + ' msid:(.+)')
        					if(match) {
        						msid = match[1];
        					}
        					match = lines[i].match('a=ssrc:' + ssrc[0] + ' mslabel:(.+)')
        					if(match) {
        						mslabel = match[1];
        					}
        					match = lines[i].match('a=ssrc:' + ssrc[0] + ' label:(.+)')
        					if(match) {
        						label = match[1];
        					}
        					if(lines[i].indexOf('a=ssrc:' + ssrc_fid[0]) === 0) {
        						lines.splice(i, 1); i--;
        						continue;
        					}
        					if(lines[i].indexOf('a=ssrc:' + ssrc[0]) === 0) {
        						lines.splice(i, 1); i--;
        						continue;
        					}
        				}
        				if(lines[i].length === 0) {
        					lines.splice(i, 1); i--;
        					continue;
        				}
        			}
        		}
        		if(ssrc[0] < 0) {
        			// Still nothing, let's just return the SDP we were asked to munge
        			console.warn("Couldn't find the video SSRC, simulcasting NOT enabled");
        			return sdp;
        		}
        		if(insertAt < 0) {
        			// Append at the end
        			insertAt = lines.length;
        		}
        		// Generate a couple of SSRCs (for retransmissions too)
        		// Note: should we check if there are conflicts, here?
        		ssrc[1] = Math.floor(Math.random()*0xFFFFFFFF);
        		ssrc[2] = Math.floor(Math.random()*0xFFFFFFFF);
        		ssrc_fid[1] = Math.floor(Math.random()*0xFFFFFFFF);
        		ssrc_fid[2] = Math.floor(Math.random()*0xFFFFFFFF);
        		// Add attributes to the SDP
        		for(var i=0; i<ssrc.length; i++) {
        			if(cname) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc[i] + ' cname:' + cname);
        				insertAt++;
        			}
        			if(msid) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc[i] + ' msid:' + msid);
        				insertAt++;
        			}
        			if(mslabel) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc[i] + ' mslabel:' + mslabel);
        				insertAt++;
        			}
        			if(label) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc[i] + ' label:' + label);
        				insertAt++;
        			}
        			// Add the same info for the retransmission SSRC
        			if(cname) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc_fid[i] + ' cname:' + cname);
        				insertAt++;
        			}
        			if(msid) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc_fid[i] + ' msid:' + msid);
        				insertAt++;
        			}
        			if(mslabel) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc_fid[i] + ' mslabel:' + mslabel);
        				insertAt++;
        			}
        			if(label) {
        				lines.splice(insertAt, 0, 'a=ssrc:' + ssrc_fid[i] + ' label:' + label);
        				insertAt++;
        			}
        		}
        		lines.splice(insertAt, 0, 'a=ssrc-group:FID ' + ssrc[2] + ' ' + ssrc_fid[2]);
        		lines.splice(insertAt, 0, 'a=ssrc-group:FID ' + ssrc[1] + ' ' + ssrc_fid[1]);
        		lines.splice(insertAt, 0, 'a=ssrc-group:FID ' + ssrc[0] + ' ' + ssrc_fid[0]);
        		lines.splice(insertAt, 0, 'a=ssrc-group:SIM ' + ssrc[0] + ' ' + ssrc[1] + ' ' + ssrc[2]);
        		sdp = lines.join("\r\n");
        		if(!sdp.endsWith("\r\n"))
        			sdp += "\r\n";
        		return sdp;
        	}

        	// Helper methods to parse a media object
        	function isAudioSendEnabled(media) {
        		console.debug("isAudioSendEnabled:", media);
        		if(!media)
        			return true;	// Default
        		if(media.audio === false)
        			return false;	// Generic audio has precedence
        		if(media.audioSend === undefined || media.audioSend === null)
        			return true;	// Default
        		return (media.audioSend === true);
        	}

        	function isAudioSendRequired(media) {
        		console.debug("isAudioSendRequired:", media);
        		if(!media)
        			return false;	// Default
        		if(media.audio === false || media.audioSend === false)
        			return false;	// If we're not asking to capture audio, it's not required
        		if(media.failIfNoAudio === undefined || media.failIfNoAudio === null)
        			return false;	// Default
        		return (media.failIfNoAudio === true);
        	}

        	function isAudioRecvEnabled(media) {
        		console.debug("isAudioRecvEnabled:", media);
        		if(!media)
        			return true;	// Default
        		if(media.audio === false)
        			return false;	// Generic audio has precedence
        		if(media.audioRecv === undefined || media.audioRecv === null)
        			return true;	// Default
        		return (media.audioRecv === true);
        	}

        	function isVideoSendEnabled(media) {
        		console.debug("isVideoSendEnabled:", media);
        		if(!media)
        			return true;	// Default
        		if(media.video === false)
        			return false;	// Generic video has precedence
        		if(media.videoSend === undefined || media.videoSend === null)
        			return true;	// Default
        		return (media.videoSend === true);
        	}

        	function isVideoSendRequired(media) {
        		console.debug("isVideoSendRequired:", media);
        		if(!media)
        			return false;	// Default
        		if(media.video === false || media.videoSend === false)
        			return false;	// If we're not asking to capture video, it's not required
        		if(media.failIfNoVideo === undefined || media.failIfNoVideo === null)
        			return false;	// Default
        		return (media.failIfNoVideo === true);
        	}

        	function isVideoRecvEnabled(media) {
        		console.debug("isVideoRecvEnabled:", media);
        		if(!media)
        			return true;	// Default
        		if(media.video === false)
        			return false;	// Generic video has precedence
        		if(media.videoRecv === undefined || media.videoRecv === null)
        			return true;	// Default
        		return (media.videoRecv === true);
        	}

        	function isScreenSendEnabled(media) {
        		console.debug("isScreenSendEnabled:", media);
        		if (!media)
        			return false;
        		if (typeof media.video !== 'object' || typeof media.video.mandatory !== 'object')
        			return false;
        		var constraints = media.video.mandatory;
        		if (constraints.chromeMediaSource)
        			return constraints.chromeMediaSource === 'desktop' || constraints.chromeMediaSource === 'screen';
        		else if (constraints.mozMediaSource)
        			return constraints.mozMediaSource === 'window' || constraints.mozMediaSource === 'screen';
        		else if (constraints.mediaSource)
        			return constraints.mediaSource === 'window' || constraints.mediaSource === 'screen';
        		return false;
        	}

        	function isDataEnabled(media) {
        		console.debug("isDataEnabled:", media);
        		if(Janus.webRTCAdapter.browserDetails.browser === "edge") {
        			console.warn("Edge doesn't support data channels yet");
        			return false;
        		}
        		if(media === undefined || media === null)
        			return false;	// Default
        		return (media.data === true);
        	}

        	function isTrickleEnabled(trickle) {
        		console.debug("isTrickleEnabled:", trickle);
        		return (trickle === false) ? false : true;
        	}
        }
    """.trimIndent()

    //endregion
    //region private val mediaMtxJs = ...
    private val mediaMtxJs = """
        'use strict';

        (() => {

          const supportsNonAdvertisedCodec = (codec, fmtp) => (
            new Promise((resolve) => {
              const payloadType = 118; // TODO: dynamic
              const pc = new RTCPeerConnection({ iceServers: [] });
              const mediaType = 'audio';
              pc.addTransceiver(mediaType, { direction: 'recvonly' });
              pc.createOffer()
                .then((offer) => {
                  if (offer.sdp.includes(' ' + codec)) { // codec is advertised, there's no need to add it manually
                    throw new Error('already present');
                  }
                  const sections = offer.sdp.split(`m=$D{mediaType}`);
                  const lines = sections[1].split('\r\n');
                  lines[0] += ` $D{payloadType}`;
                  lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} $D{codec}`);
                  if (fmtp !== undefined) {
                    lines.splice(lines.length - 1, 0, `a=fmtp:$D{payloadType} $D{fmtp}`);
                  }
                  sections[1] = lines.join('\r\n');
                  offer.sdp = sections.join(`m=$D{mediaType}`);
                  return pc.setLocalDescription(offer);
                })
                .then(() => {
                  return pc.setRemoteDescription(new RTCSessionDescription({
                    type: 'answer',
                    sdp: 'v=0\r\n'
                    + 'o=- 6539324223450680508 0 IN IP4 0.0.0.0\r\n'
                    + 's=-\r\n'
                    + 't=0 0\r\n'
                    + 'a=fingerprint:sha-256 0D:9F:78:15:42:B5:4B:E6:E2:94:3E:5B:37:78:E1:4B:54:59:A3:36:3A:E5:05:EB:27:EE:8F:D2:2D:41:29:25\r\n'
                    + `m=$D{mediaType} 9 UDP/TLS/RTP/SAVPF $D{payloadType}` + '\r\n'
                    + 'c=IN IP4 0.0.0.0\r\n'
                    + 'a=ice-pwd:7c3bf4770007e7432ee4ea4d697db675\r\n'
                    + 'a=ice-ufrag:29e036dc\r\n'
                    + 'a=sendonly\r\n'
                    + 'a=rtcp-mux\r\n'
                    + `a=rtpmap:$D{payloadType} $D{codec}` + '\r\n'
                    + ((fmtp !== undefined) ? `a=fmtp:$D{payloadType} $D{fmtp}` + '\r\n' : ''),
                  }));
                })
                .then(() => {
                  resolve(true);
                })
                .catch(() => {
                  resolve(false);
                })
                .finally(() => {
                  pc.close();
                });
            })
          );

          const unquoteCredential = (v) => (
            JSON.parse(`"$D{v}"`)
          );

          const linkToIceServers = (links) => (
            (links !== null) ? links.split(', ').map((link) => {
              const m = link.match(/^<(.+?)>; rel="ice-server"(; username="(.*?)"; credential="(.*?)"; credential-type="password")?/i);
              const ret = {
                urls: [m[1]],
              };

              if (m[3] !== undefined) {
                ret.username = unquoteCredential(m[3]);
                ret.credential = unquoteCredential(m[4]);
                ret.credentialType = 'password';
              }

              return ret;
            }) : []
          );

          const parseOffer = (sdp) => {
            const ret = {
              iceUfrag: '',
              icePwd: '',
              medias: [],
            };

            for (const line of sdp.split('\r\n')) {
              if (line.startsWith('m=')) {
                ret.medias.push(line.slice('m='.length));
              } else if (ret.iceUfrag === '' && line.startsWith('a=ice-ufrag:')) {
                ret.iceUfrag = line.slice('a=ice-ufrag:'.length);
              } else if (ret.icePwd === '' && line.startsWith('a=ice-pwd:')) {
                ret.icePwd = line.slice('a=ice-pwd:'.length);
              }
            }

            return ret;
          };

          const findFreePayloadType = (firstLine) => {
            const payloadTypes = firstLine.split(' ').slice(3);
            for (let i = 96; i <= 127; i++) {
              if (!payloadTypes.includes(i.toString())) {
                return i.toString();
              }
            }
            throw Error('unable to find a free payload type');
          };

          const enableStereoPcmau = (section) => {
            let lines = section.split('\r\n');

            let payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} PCMU/8000/2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} PCMA/8000/2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            return lines.join('\r\n');
          };

          const enableMultichannelOpus = (section) => {
            let lines = section.split('\r\n');

            let payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} multiopus/48000/3`);
            lines.splice(lines.length - 1, 0, `a=fmtp:$D{payloadType} channel_mapping=0,2,1;num_streams=2;coupled_streams=1`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} multiopus/48000/4`);
            lines.splice(lines.length - 1, 0, `a=fmtp:$D{payloadType} channel_mapping=0,1,2,3;num_streams=2;coupled_streams=2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} multiopus/48000/5`);
            lines.splice(lines.length - 1, 0, `a=fmtp:$D{payloadType} channel_mapping=0,4,1,2,3;num_streams=3;coupled_streams=2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} multiopus/48000/6`);
            lines.splice(lines.length - 1, 0, `a=fmtp:$D{payloadType} channel_mapping=0,4,1,2,3,5;num_streams=4;coupled_streams=2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} multiopus/48000/7`);
            lines.splice(lines.length - 1, 0, `a=fmtp:$D{payloadType} channel_mapping=0,4,1,2,3,5,6;num_streams=4;coupled_streams=4`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} multiopus/48000/8`);
            lines.splice(lines.length - 1, 0, `a=fmtp:$D{payloadType} channel_mapping=0,6,1,4,5,2,3,7;num_streams=5;coupled_streams=4`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            return lines.join('\r\n');
          };

          const enableL16 = (section) => {
            let lines = section.split('\r\n');

            let payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} L16/8000/2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} L16/16000/2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            payloadType = findFreePayloadType(lines[0]);
            lines[0] += ` $D{payloadType}`;
            lines.splice(lines.length - 1, 0, `a=rtpmap:$D{payloadType} L16/48000/2`);
            lines.splice(lines.length - 1, 0, `a=rtcp-fb:$D{payloadType} transport-cc`);

            return lines.join('\r\n');
          };

          const enableStereoOpus = (section) => {
            let opusPayloadFormat = '';
            let lines = section.split('\r\n');

            for (let i = 0; i < lines.length; i++) {
              if (lines[i].startsWith('a=rtpmap:') && lines[i].toLowerCase().includes('opus/')) {
                opusPayloadFormat = lines[i].slice('a=rtpmap:'.length).split(' ')[0];
                break;
              }
            }

            if (opusPayloadFormat === '') {
              return section;
            }

            for (let i = 0; i < lines.length; i++) {
              if (lines[i].startsWith('a=fmtp:' + opusPayloadFormat + ' ')) {
                if (!lines[i].includes('stereo')) {
                  lines[i] += ';stereo=1';
                }
                if (!lines[i].includes('sprop-stereo')) {
                  lines[i] += ';sprop-stereo=1';
                }
              }
            }

            return lines.join('\r\n');
          };

          const editOffer = (sdp, nonAdvertisedCodecs) => {
            const sections = sdp.split('m=');

            for (let i = 0; i < sections.length; i++) {
              if (sections[i].startsWith('audio')) {
                sections[i] = enableStereoOpus(sections[i]);

                if (nonAdvertisedCodecs.includes('pcma/8000/2')) {
                  sections[i] = enableStereoPcmau(sections[i]);
                }
                if (nonAdvertisedCodecs.includes('multiopus/48000/6')) {
                  sections[i] = enableMultichannelOpus(sections[i]);
                }
                if (nonAdvertisedCodecs.includes('L16/48000/2')) {
                  sections[i] = enableL16(sections[i]);
                }

                break;
              }
            }

            return sections.join('m=');
          };

          const generateSdpFragment = (od, candidates) => {
            const candidatesByMedia = {};
            for (const candidate of candidates) {
              const mid = candidate.sdpMLineIndex;
              if (candidatesByMedia[mid] === undefined) {
                candidatesByMedia[mid] = [];
              }
              candidatesByMedia[mid].push(candidate);
            }

            let frag = 'a=ice-ufrag:' + od.iceUfrag + '\r\n'
              + 'a=ice-pwd:' + od.icePwd + '\r\n';

            let mid = 0;

            for (const media of od.medias) {
              if (candidatesByMedia[mid] !== undefined) {
                frag += 'm=' + media + '\r\n'
                  + 'a=mid:' + mid + '\r\n';

                for (const candidate of candidatesByMedia[mid]) {
                  frag += 'a=' + candidate.candidate + '\r\n';
                }
              }
              mid++;
            }

            return frag;
          };

          const retryPause = 2000;

          class MediaMTXWebRTCReader {
            constructor(conf) {
              this.conf = conf;
              this.state = 'initializing';
              this.restartTimeout = null;
              this.pc = null;
              this.offerData = null;
              this.sessionUrl = null;
              this.queuedCandidates = [];

              this.getNonAdvertisedCodecs()
                .then(() => this.start())
                .catch((err) => {
                  this.handleError(err);
                });
            }

            handleError = (err) => {
              if (this.state === 'restarting' || this.state === 'error') {
                return;
              }

              if (this.pc !== null) {
                this.pc.close();
                this.pc = null;
              }

              this.offerData = null;

              if (this.sessionUrl !== null) {
                fetch(this.sessionUrl, {
                  method: 'DELETE',
                });
                this.sessionUrl = null;
              }

              this.queuedCandidates = [];

              if (this.state === 'running') {
                this.state = 'restarting';

                this.restartTimeout = window.setTimeout(() => {
                  this.restartTimeout = null;
                  this.start();
                }, retryPause);

                if (this.conf.onError !== undefined) {
                  this.conf.onError(err + ', retrying in some seconds');
                }
              } else {
                this.state = 'error';

                if (this.conf.onError !== undefined) {
                  this.conf.onError(err);
                }
              }
            };

            getNonAdvertisedCodecs = () => {
              return Promise.all([
                ['pcma/8000/2'],
                ['multiopus/48000/6', 'channel_mapping=0,4,1,2,3,5;num_streams=4;coupled_streams=2'],
                ['L16/48000/2'],
              ]
                .map((c) => supportsNonAdvertisedCodec(c[0], c[1]).then((r) => (r) ? c[0] : false)))
                .then((c) => c.filter((e) => e !== false))
                .then((codecs) => {
                  this.nonAdvertisedCodecs = codecs;
                });
            };

            start = () => {
              this.state = 'running';

              this.requestICEServers()
                .then((iceServers) => this.setupPeerConnection(iceServers))
                .then((offer) => this.sendOffer(offer))
                .then((answer) => this.setAnswer(answer))
                .catch((err) => {
                  this.handleError(err.toString());
                });
            };

            requestICEServers = () => {
              return fetch(this.conf.url, {
                method: 'OPTIONS',
              })
                .then((res) => linkToIceServers(res.headers.get('Link')))
            };

            setupPeerConnection = (iceServers) => {
              this.pc = new RTCPeerConnection({
                iceServers,
                // https://webrtc.org/getting-started/unified-plan-transition-guide
                sdpSemantics: 'unified-plan',
              });

              const direction = 'sendrecv';
              this.pc.addTransceiver('video', { direction });
              this.pc.addTransceiver('audio', { direction });

              this.pc.onicecandidate = (evt) => this.onLocalCandidate(evt);
              this.pc.oniceconnectionstatechange = () => this.onConnectionState();
              this.pc.ontrack = (evt) => this.onTrack(evt);

              return this.pc.createOffer()
                .then((offer) => {
                  offer.sdp = editOffer(offer.sdp, this.nonAdvertisedCodecs);
                  this.offerData = parseOffer(offer.sdp);

                  return this.pc.setLocalDescription(offer)
                    .then(() => offer.sdp);
                });
            };

            sendOffer = (offer) => {
              return fetch(this.conf.url, {
                method: 'POST',
                headers: {'Content-Type': 'application/sdp'},
                body: offer,
              })
                .then((res) => {
                  switch (res.status) {
                  case 201:
                    break;
                  case 404:
                    throw new Error('stream not found');
                  case 400:
                    return res.json().then((e) => { throw new Error(e.error); });
                  default:
                    throw new Error(`bad status code $D{res.status}`);
                  }

                  this.sessionUrl = new URL(res.headers.get('location'), this.conf.url).toString();

                  return res.text();
                });
            };

            setAnswer = (answer) => {
              if (this.state !== 'running') {
                return;
              }

              return this.pc.setRemoteDescription(new RTCSessionDescription({
                type: 'answer',
                sdp: answer,
              }))
                .then(() => {
                  if (this.queuedCandidates.length !== 0) {
                    this.sendLocalCandidates(this.queuedCandidates);
                    this.queuedCandidates = [];
                  }
                });
            };

            onLocalCandidate = (evt) => {
              if (this.state !== 'running') {
                return;
              }

              if (evt.candidate !== null) {
                if (this.sessionUrl === null) {
                  this.queuedCandidates.push(evt.candidate);
                } else {
                  this.sendLocalCandidates([evt.candidate]);
                }
              }
            };

            sendLocalCandidates = (candidates) => {
              fetch(this.sessionUrl, {
                method: 'PATCH',
                headers: {
                  'Content-Type': 'application/trickle-ice-sdpfrag',
                  'If-Match': '*',
                },
                body: generateSdpFragment(this.offerData, candidates),
              })
                .then((res) => {
                  switch (res.status) {
                  case 204:
                    break;
                  case 404:
                    throw new Error('stream not found');
                  default:
                    throw new Error(`bad status code $D{res.status}`);
                  }
                })
                .catch((err) => {
                  this.handleError(err.toString());
                });
            };

            onConnectionState = () => {
              if (this.state !== 'running') {
                return;
              }

              if (this.pc.iceConnectionState === 'failed') {
                this.handleError('peer connection closed');
              }
            };

            onTrack = (evt) => {
              if (this.conf.onTrack !== undefined) {
                this.conf.onTrack(evt);
              }
            };

          }

          window.MediaMTXWebRTCReader = MediaMTXWebRTCReader;

        })();

    """.trimIndent()

    //endregion
    //region private val go2rtcJs = ...
    private val go2rtcJs = """
            /**
             * VideoRTC v1.6.0 - Video player for go2rtc streaming application.
             *
             * All modern web technologies are supported in almost any browser except Apple Safari.
             *
             * Support:
             * - ECMAScript 2017 (ES8) = ES6 + async
             * - RTCPeerConnection for Safari iOS 11.0+
             * - IntersectionObserver for Safari iOS 12.2+
             * - ManagedMediaSource for Safari 17+
             *
             * Doesn't support:
             * - MediaSource for Safari iOS
             * - Customized built-in elements (extends HTMLVideoElement) because Safari
             * - Autoplay for WebRTC in Safari
             */
            export class VideoRTC extends HTMLElement {
                constructor() {
                    super();

                    this.DISCONNECT_TIMEOUT = 5000;
                    this.RECONNECT_TIMEOUT = 30000;

                    this.CODECS = [
                        'avc1.640029',      // H.264 high 4.1 (Chromecast 1st and 2nd Gen)
                        'avc1.64002A',      // H.264 high 4.2 (Chromecast 3rd Gen)
                        'avc1.640033',      // H.264 high 5.1 (Chromecast with Google TV)
                        'hvc1.1.6.L153.B0', // H.265 main 5.1 (Chromecast Ultra)
                        'mp4a.40.2',        // AAC LC
                        'mp4a.40.5',        // AAC HE
                        'flac',             // FLAC (PCM compatible)
                        'opus',             // OPUS Chrome, Firefox
                    ];

                    /**
                     * [config] Supported modes (webrtc, webrtc/tcp, mse, hls, mp4, mjpeg).
                     * @type {string}
                     */
                    this.mode = 'webrtc,mse,hls,mjpeg';

                    /**
                     * [Config] Requested medias (video, audio, microphone).
                     * @type {string}
                     */
                    this.media = 'video,audio';

                    /**
                     * [config] Run stream when not displayed on the screen. Default `false`.
                     * @type {boolean}
                     */
                    this.background = false;

                    /**
                     * [config] Run stream only when player in the viewport. Stop when user scroll out player.
                     * Value is percentage of visibility from `0` (not visible) to `1` (full visible).
                     * Default `0` - disable;
                     * @type {number}
                     */
                    this.visibilityThreshold = 0;

                    /**
                     * [config] Run stream only when browser page on the screen. Stop when user change browser
                     * tab or minimise browser windows.
                     * @type {boolean}
                     */
                    this.visibilityCheck = true;

                    /**
                     * [config] WebRTC configuration
                     * @type {RTCConfiguration}
                     */
                    this.pcConfig = {
                        iceServers: [{urls: 'stun:stun.l.google.com:19302'}],
                        sdpSemantics: 'unified-plan',  // important for Chromecast 1
                    };

                    /**
                     * [info] WebSocket connection state. Values: CONNECTING, OPEN, CLOSED
                     * @type {number}
                     */
                    this.wsState = WebSocket.CLOSED;

                    /**
                     * [info] WebRTC connection state.
                     * @type {number}
                     */
                    this.pcState = WebSocket.CLOSED;

                    /**
                     * @type {HTMLVideoElement}
                     */
                    this.video = null;

                    /**
                     * @type {WebSocket}
                     */
                    this.ws = null;

                    /**
                     * @type {string|URL}
                     */
                    this.wsURL = '';

                    /**
                     * @type {RTCPeerConnection}
                     */
                    this.pc = null;

                    /**
                     * @type {number}
                     */
                    this.connectTS = 0;

                    /**
                     * @type {string}
                     */
                    this.mseCodecs = '';

                    /**
                     * [internal] Disconnect TimeoutID.
                     * @type {number}
                     */
                    this.disconnectTID = 0;

                    /**
                     * [internal] Reconnect TimeoutID.
                     * @type {number}
                     */
                    this.reconnectTID = 0;

                    /**
                     * [internal] Handler for receiving Binary from WebSocket.
                     * @type {Function}
                     */
                     console.log("Clearing this.ondata 1")
                    this.ondata = null;

                    /**
                     * [internal] Handlers list for receiving JSON from WebSocket.
                     * @type {Object.<string,Function>}
                     */
                    this.onmessage = null;
                }

                /**
                 * Set video source (WebSocket URL). Support relative path.
                 * @param {string|URL} value
                 */
                set src(value) {
                    if (typeof value !== 'string') value = value.toString();
                    if (value.startsWith('http')) {
                        value = 'ws' + value.substring(4);
                    } else if (value.startsWith('/')) {
                        value = 'ws' + location.origin.substring(4) + value;
                    }

                    this.wsURL = value;

                    this.onconnect();
                }

                /**
                 * Play video. Support automute when autoplay blocked.
                 * https://developer.chrome.com/blog/autoplay/
                 */
                play() {
                    this.video.play().catch(() => {
                        if (!this.video.muted) {
                            this.video.muted = true;
                            this.video.play().catch(er => {
                                console.warn(er);
                            });
                        }
                    });
                }

                /**
                 * Send message to server via WebSocket
                 * @param {Object} value
                 */
                send(value) {
                    if (this.ws) this.ws.send(JSON.stringify(value));
                }

                /** @param {Function} isSupported */
                codecs(isSupported) {
                    return this.CODECS
                        .filter(codec => this.media.indexOf(codec.indexOf('vc1') > 0 ? 'video' : 'audio') >= 0)
                        .filter(codec => isSupported(`video/mp4; codecs="${'$'}{codec}"`)).join();
                }

                /**
                 * `CustomElement`. Invoked each time the custom element is appended into a
                 * document-connected element.
                 */
                connectedCallback() {
                    if (this.disconnectTID) {
                        clearTimeout(this.disconnectTID);
                        this.disconnectTID = 0;
                    }

                    // because video autopause on disconnected from DOM
                    if (this.video) {
                        const seek = this.video.seekable;
                        if (seek.length > 0) {
                            this.video.currentTime = seek.end(seek.length - 1);
                        }
                        this.play();
                    } else {
                        this.oninit();
                    }

                    this.onconnect();
                }

                /**
                 * `CustomElement`. Invoked each time the custom element is disconnected from the
                 * document's DOM.
                 */
                disconnectedCallback() {
                    if (this.background || this.disconnectTID) return;
                    if (this.wsState === WebSocket.CLOSED && this.pcState === WebSocket.CLOSED) return;

                    this.disconnectTID = setTimeout(() => {
                        if (this.reconnectTID) {
                            clearTimeout(this.reconnectTID);
                            this.reconnectTID = 0;
                        }

                        this.disconnectTID = 0;

                        this.ondisconnect();
                    }, this.DISCONNECT_TIMEOUT);
                }

                /**
                 * Creates child DOM elements. Called automatically once on `connectedCallback`.
                 */
                oninit() {
                    this.video = document.createElement('video');
                    this.video.controls = false;
                    this.video.playsInline = true;
                    this.video.preload = 'auto';

                    this.video.style.display = 'block'; // fix bottom margin 4px
                    this.video.style.width = '100%';
                    this.video.style.height = '100%';

                    this.appendChild(this.video);

                    // all Safari lies about supported audio codecs
                    const m = window.navigator.userAgent.match(/Version\/(\d+).+Safari/);
                    if (m) {
                        // AAC from v13, FLAC from v14, OPUS - unsupported
                        const skip = m[1] < '13' ? 'mp4a.40.2' : m[1] < '14' ? 'flac' : 'opus';
                        this.CODECS.splice(this.CODECS.indexOf(skip));
                    }

                    if (this.background) return;

                    if ('hidden' in document && this.visibilityCheck) {
                        document.addEventListener('visibilitychange', () => {
                            if (document.hidden) {
                                this.disconnectedCallback();
                            } else if (this.isConnected) {
                                this.connectedCallback();
                            }
                        });
                    }

                    if ('IntersectionObserver' in window && this.visibilityThreshold) {
                        const observer = new IntersectionObserver(entries => {
                            entries.forEach(entry => {
                                if (!entry.isIntersecting) {
                                    this.disconnectedCallback();
                                } else if (this.isConnected) {
                                    this.connectedCallback();
                                }
                            });
                        }, {threshold: this.visibilityThreshold});
                        observer.observe(this);
                    }
                }

                /**
                 * Connect to WebSocket. Called automatically on `connectedCallback`.
                 * @return {boolean} true if the connection has started.
                 */
                onconnect() {
                    if (!this.isConnected || !this.wsURL || this.ws || this.pc) return false;

                    // CLOSED or CONNECTING => CONNECTING
                    this.wsState = WebSocket.CONNECTING;

                    this.connectTS = Date.now();

                    this.ws = new WebSocket(this.wsURL);
                    this.ws.binaryType = 'arraybuffer';
                    this.ws.addEventListener('open', () => this.onopen());
                    this.ws.addEventListener('close', () => this.onclose());

                    return true;
                }

                ondisconnect() {
                    this.wsState = WebSocket.CLOSED;
                    if (this.ws) {
                        this.ws.close();
                        this.ws = null;
                    }

                    this.pcState = WebSocket.CLOSED;
                    if (this.pc) {
                        this.pc.getSenders().forEach(sender => {
                            if (sender.track) sender.track.stop();
                        });
                        this.pc.close();
                        this.pc = null;
                    }

                    this.video.src = '';
                    this.video.srcObject = null;
                }

                /**
                 * @returns {Array.<string>} of modes (mse, webrtc, etc.)
                 */
                onopen() {
                    // CONNECTING => OPEN
                    this.wsState = WebSocket.OPEN;

                    this.ws.addEventListener('message', ev => {
                        if (typeof ev.data === 'string') {
                            const msg = JSON.parse(ev.data);
                            for (const mode in this.onmessage) {
                                this.onmessage[mode](msg);
                            }
                        } else {
                            this.ondata(ev.data);
                        }
                    });

                    console.log("Clearing this.ondata 2")
                    this.ondata = null;
                    this.onmessage = {};

                    const modes = [];

                    if (this.mode.indexOf('mse') >= 0 && ('MediaSource' in window || 'ManagedMediaSource' in window)) {
                        modes.push('mse');
                        this.onmse();
                    } else if (this.mode.indexOf('hls') >= 0 && this.video.canPlayType('application/vnd.apple.mpegurl')) {
                        modes.push('hls');
                        this.onhls();
                    } else if (this.mode.indexOf('mp4') >= 0) {
                        modes.push('mp4');
                        this.onmp4();
                    }

                    if (this.mode.indexOf('webrtc') >= 0 && 'RTCPeerConnection' in window) {
                        modes.push('webrtc');
                        this.onwebrtc();
                    }

                    if (this.mode.indexOf('mjpeg') >= 0) {
                        if (modes.length) {
                            this.onmessage['mjpeg'] = msg => {
                                if (msg.type !== 'error' || msg.value.indexOf(modes[0]) !== 0) return;
                                this.onmjpeg();
                            };
                        } else {
                            modes.push('mjpeg');
                            this.onmjpeg();
                        }
                    }

                    return modes;
                }

                /**
                 * @return {boolean} true if reconnection has started.
                 */
                onclose() {
                    if (this.wsState === WebSocket.CLOSED) return false;

                    // CONNECTING, OPEN => CONNECTING
                    this.wsState = WebSocket.CONNECTING;
                    this.ws = null;

                    // reconnect no more than once every X seconds
                    const delay = Math.max(this.RECONNECT_TIMEOUT - (Date.now() - this.connectTS), 0);

                    this.reconnectTID = setTimeout(() => {
                        this.reconnectTID = 0;
                        this.onconnect();
                    }, delay);

                    return true;
                }

                onmse() {
                    /** @type {MediaSource} */
                    let ms;

                    if ('ManagedMediaSource' in window) {
                        const MediaSource = window.ManagedMediaSource;

                        ms = new MediaSource();
                        ms.addEventListener('sourceopen', () => {
                            this.send({type: 'mse', value: this.codecs(MediaSource.isTypeSupported)});
                        }, {once: true});

                        this.video.disableRemotePlayback = true;
                        this.video.srcObject = ms;
                    } else {
                        ms = new MediaSource();
                        ms.addEventListener('sourceopen', () => {
                            URL.revokeObjectURL(this.video.src);
                            this.send({type: 'mse', value: this.codecs(MediaSource.isTypeSupported)});
                        }, {once: true});

                        this.video.src = URL.createObjectURL(ms);
                        this.video.srcObject = null;
                    }

                    this.play();

                    this.mseCodecs = '';

                    this.onmessage['mse'] = msg => {
                        if (msg.type !== 'mse') return;

                        this.mseCodecs = msg.value;

                        const sb = ms.addSourceBuffer(msg.value);
                        sb.mode = 'segments'; // segments or sequence
                        sb.addEventListener('updateend', () => {
                            if (sb.updating) return;

                            try {
                                if (bufLen > 0) {
                                    const data = buf.slice(0, bufLen);
                                    bufLen = 0;
                                    sb.appendBuffer(data);
                                } else if (sb.buffered && sb.buffered.length) {
                                    const end = sb.buffered.end(sb.buffered.length - 1) - 15;
                                    const start = sb.buffered.start(0);
                                    if (end > start) {
                                        sb.remove(start, end);
                                        ms.setLiveSeekableRange(end, end + 15);
                                    }
                                    // console.debug("VideoRTC.buffered", start, end);
                                }
                            } catch (e) {
                                // console.debug(e);
                            }
                        });

                        const buf = new Uint8Array(2 * 1024 * 1024);
                        let bufLen = 0;

                        console.log("Setting this.ondata mse")
                        this.ondata = data => {
                            if (sb.updating || bufLen > 0) {
                                const b = new Uint8Array(data);
                                buf.set(b, bufLen);
                                bufLen += b.byteLength;
                                // console.debug("VideoRTC.buffer", b.byteLength, bufLen);
                            } else {
                                try {
                                    sb.appendBuffer(data);
                                } catch (e) {
                                    // console.debug(e);
                                }
                            }
                        };
                    };
                }

                onwebrtc() {
                    const pc = new RTCPeerConnection(this.pcConfig);

                    pc.addEventListener('icecandidate', ev => {
                        if (ev.candidate && this.mode.indexOf('webrtc/tcp') >= 0 && ev.candidate.protocol === 'udp') return;

                        const candidate = ev.candidate ? ev.candidate.toJSON().candidate : '';
                        this.send({type: 'webrtc/candidate', value: candidate});
                    });

                    pc.addEventListener('connectionstatechange', () => {
                        if (pc.connectionState === 'connected') {
                            const tracks = pc.getReceivers().map(receiver => receiver.track);
                            /** @type {HTMLVideoElement} */
                            const video2 = document.createElement('video');
                            video2.addEventListener('loadeddata', () => this.onpcvideo(video2), {once: true});
                            video2.srcObject = new MediaStream(tracks);
                        } else if (pc.connectionState === 'failed' || pc.connectionState === 'disconnected') {
                            pc.close(); // stop next events

                            this.pcState = WebSocket.CLOSED;
                            this.pc = null;

                            this.onconnect();
                        }
                    });

                    this.onmessage['webrtc'] = msg => {
                        switch (msg.type) {
                            case 'webrtc/candidate':
                                if (this.mode.indexOf('webrtc/tcp') >= 0 && msg.value.indexOf(' udp ') > 0) return;

                                pc.addIceCandidate({candidate: msg.value, sdpMid: '0'}).catch(er => {
                                    console.warn(er);
                                });
                                break;
                            case 'webrtc/answer':
                                pc.setRemoteDescription({type: 'answer', sdp: msg.value}).catch(er => {
                                    console.warn(er);
                                });
                                break;
                            case 'error':
                                if (msg.value.indexOf('webrtc/offer') < 0) return;
                                pc.close();
                        }
                    };

                    this.createOffer(pc).then(offer => {
                        this.send({type: 'webrtc/offer', value: offer.sdp});
                    });

                    this.pcState = WebSocket.CONNECTING;
                    this.pc = pc;
                }

                /**
                 * @param pc {RTCPeerConnection}
                 * @return {Promise<RTCSessionDescriptionInit>}
                 */
                async createOffer(pc) {
                    try {
                        if (this.media.indexOf('microphone') >= 0) {
                            const media = await navigator.mediaDevices.getUserMedia({audio: true});
                            media.getTracks().forEach(track => {
                                pc.addTransceiver(track, {direction: 'sendonly'});
                            });
                        }
                    } catch (e) {
                        console.warn(e);
                    }

                    for (const kind of ['video', 'audio']) {
                        if (this.media.indexOf(kind) >= 0) {
                            pc.addTransceiver(kind, {direction: 'recvonly'});
                        }
                    }

                    const offer = await pc.createOffer();
                    await pc.setLocalDescription(offer);
                    return offer;
                }

                /**
                 * @param video2 {HTMLVideoElement}
                 */
                onpcvideo(video2) {
                    if (this.pc) {
                        // Video+Audio > Video, H265 > H264, Video > Audio, WebRTC > MSE
                        let rtcPriority = 0, msePriority = 0;

                        /** @type {MediaStream} */
                        const stream = video2.srcObject;
                        if (stream.getVideoTracks().length > 0) rtcPriority += 0x220;
                        if (stream.getAudioTracks().length > 0) rtcPriority += 0x102;

                        if (this.mseCodecs.indexOf('hvc1.') >= 0) msePriority += 0x230;
                        if (this.mseCodecs.indexOf('avc1.') >= 0) msePriority += 0x210;
                        if (this.mseCodecs.indexOf('mp4a.') >= 0) msePriority += 0x101;

                        if (rtcPriority >= msePriority) {
                            this.video.srcObject = stream;
                            this.play();

                            this.pcState = WebSocket.OPEN;

                            this.wsState = WebSocket.CLOSED;
                            if (this.ws) {
                                this.ws.close();
                                this.ws = null;
                            }
                        } else {
                            this.pcState = WebSocket.CLOSED;
                            if (this.pc) {
                                this.pc.close();
                                this.pc = null;
                            }
                        }
                    }

                    video2.srcObject = null;
                }

                onmjpeg() {
                    console.log("Setting this.ondata MJPEG")
                    this.ondata = data => {
                        this.video.controls = false;
                        if (moveToState) {
                            const blob = new Blob([data], { type: 'image/jpeg' });
                            const img = new Image();
                            img.onload = () => {
                                moveToState({ state: 'playing', width: img.width, height: img.height });
                                URL.revokeObjectURL(img.src); // Free up memory
                            };
                            img.src = URL.createObjectURL(blob);
                        }
                        this.video.poster = 'data:image/jpeg;base64,' + VideoRTC.btoa(data);
                    };

                    this.send({type: 'mjpeg'});
                }

                onhls() {
                    this.onmessage['hls'] = msg => {
                        if (msg.type !== 'hls') return;

                        const url = 'http' + this.wsURL.substring(2, this.wsURL.indexOf('/ws')) + '/hls/';
                        const playlist = msg.value.replace('hls/', url);
                        this.video.src = 'data:application/vnd.apple.mpegurl;base64,' + btoa(playlist);
                        this.play();
                    };

                    this.send({type: 'hls', value: this.codecs(type => this.video.canPlayType(type))});
                }

                onmp4() {
                    /** @type {HTMLCanvasElement} **/
                    const canvas = document.createElement('canvas');
                    /** @type {CanvasRenderingContext2D} */
                    let context;

                    /** @type {HTMLVideoElement} */
                    const video2 = document.createElement('video');
                    video2.autoplay = true;
                    video2.playsInline = true;
                    video2.muted = true;

                    video2.addEventListener('loadeddata', () => {
                        if (!context) {
                            canvas.width = video2.videoWidth;
                            canvas.height = video2.videoHeight;
                            context = canvas.getContext('2d');
                        }

                        context.drawImage(video2, 0, 0, canvas.width, canvas.height);

                        this.video.controls = false;
                        this.video.poster = canvas.toDataURL('image/jpeg');
                    });

                    console.log("Setting this.ondata mp4")
                    this.ondata = data => {
                        video2.src = 'data:video/mp4;base64,' + VideoRTC.btoa(data);
                    };

                    this.send({type: 'mp4', value: this.codecs(this.video.canPlayType)});
                }

                static btoa(buffer) {
                    const bytes = new Uint8Array(buffer);
                    const len = bytes.byteLength;
                    let binary = '';
                    for (let i = 0; i < len; i++) {
                        binary += String.fromCharCode(bytes[i]);
                    }
                    return window.btoa(binary);
                }
            }

            /**
             * This is example, how you can extend VideoRTC player for your app.
             * Also you can check this example: https://github.com/AlexxIT/WebRTC
             */
            class VideoStream extends VideoRTC {
                set divMode(value) {
                    this.querySelector('.mode').innerText = value;
                    this.querySelector('.status').innerText = '';
                }

                set divError(value) {
                    const state = this.querySelector('.mode').innerText;
                    if (state !== 'loading') return;
                    this.querySelector('.mode').innerText = 'error';
                    this.querySelector('.status').innerText = value;
                }

                /**
                 * Custom GUI
                 */
                oninit() {
                    console.debug('stream.oninit');
                    super.oninit();

                    this.innerHTML = `
                    <style>
                    video-stream {
                        position: relative;
                    }
                    .info {
                        position: absolute;
                        top: 0;
                        left: 0;
                        right: 0;
                        padding: 12px;
                        color: white;
                        display: flex;
                        justify-content: space-between;
                        pointer-events: none;
                    }
                    </style>
                    <div class="info">
                        <div class="status"></div>
                        <div class="mode"></div>
                    </div>
                    `;

                    const info = this.querySelector('.info');
                    this.insertBefore(this.video, info);
                }

                onconnect() {
                    console.debug('stream.onconnect');
                    const result = super.onconnect();
                    if (result) this.divMode = 'loading';
                    return result;
                }

                ondisconnect() {
                    console.debug('stream.ondisconnect');
                    super.ondisconnect();
                }

                onopen() {
                    console.debug('stream.onopen');
                    const result = super.onopen();

                    this.onmessage['stream'] = msg => {
                        console.debug('stream.onmessge', msg);
                        switch (msg.type) {
                            case 'error':
                                this.divError = msg.value;
                                break;
                            case 'mse':
                            case 'hls':
                            case 'mp4':
                            case 'mjpeg':
                                this.divMode = msg.type.toUpperCase();
                                break;
                        }
                    };

                    return result;
                }

                onclose() {
                    console.debug('stream.onclose');
                    return super.onclose();
                }

                onpcvideo(ev) {
                    console.debug('stream.onpcvideo');
                    super.onpcvideo(ev);

                    if (this.pcState !== WebSocket.CLOSED) {
                        this.divMode = 'RTC';
                    }
                }
            }

            customElements.define('video-stream', VideoStream);

    """.trimIndent()

    //endregion
    //region val cameraStreamerHtml = ...
    val cameraStreamerHtml = """
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
            <title>camerastreamer</title>
            <link href="data:;base64,iVBORw0KGgo=" rel="icon">
            <style>
            $sharedCss
            </style>
        </head>
        <body>
        <div id="streamtage" class="streamstage">
            <video autoplay id="stream" muted playsinline></video>
        </div>

        <script>
                $sharedJs
    
                const url = "$URL_PLACEHOLDER"
                const video = document.getElementById("stream")
                const httpHeaders = JSON.parse('$EXTRA_HEAERS_PLACEHOLDER')
                httpHeaders['Content-Type'] = 'application/json' 
                function setupVideoCallbacks() {
                    setupVideoCallbacksFor(video)
                }
                
                function startWebRTC(preventAutoReconnect) {
                    webRtcPlaying = true
                    console.log("Starting WebRTC with", url)
                    
                    var config = {
                        sdpSemantics: 'unified-plan',
                        iceServers: [{urls: ['stun:stun.l.google.com:19302']}]
                    };
                    
                    pc = new RTCPeerConnection(config);
                    pc.addTransceiver('video', {direction: 'recvonly'});
                    //pc.addTransceiver('audio', {direction: 'recvonly'});
                    pc.addEventListener('track', function(evt) {
                        console.log("track event " + evt.track.kind);
                        if (evt.track.kind == 'video') {
                            if (video) {
                                video.srcObject = evt.streams[0];
                            }
                        } else {
                            if (document.getElementById('audio'))
                            document.getElementById('audio').srcObject = evt.streams[0];
                        }
                    });
                    pc.addEventListener("connectionstatechange", (evt) => {
                        const status = pc.connectionState.toString()
                        console.debug("Peer connection is now", status)
                        if (status == "closed") {
                            moveToState({state: 'error', errorMsg: "Connection is closed", error: status})
                            scheduleReconnect()
                        } else if (status == "failed") {
                            moveToState({state: 'error', errorMsg: "Connection failed", error: status})
                            scheduleReconnect()
                        } else if (status == "disconnected") {
                            moveToState({state: 'error', errorMsg: "Connection lost", error: status})
                            scheduleReconnect()
                        }
                    });

                    
                    const urlSearchParams = new URLSearchParams(window.location.search);
                    const params = Object.fromEntries(urlSearchParams.entries());
                    
                    console.debug("Sending request to", url)
                    fetch(url, {
                        body: JSON.stringify({
                            type: 'request',
                            res: params.res
                        }),
                        headers: httpHeaders,
                        method: 'POST'
                    }).catch(function(error) {
                        console.debug("Initial request failed", error)
                        throw `Unable to conenct to ${D}{url}`
                    }).then(function(response) {
                        if (response.status != 200) {
                            if (!preventAutoReconnect) window.setTimeout(() => startWebRTC(true), 0);
                            throw `Unexpected HTTP status $D{response.status} code, check your webcam server (1)`
                        }
                        
                        console.debug("Got response")
                        return response.json();
                    }).then(function(answer) {
                        console.debug("Got answer", JSON.stringify(answer))
                        pc.remote_pc_id = answer.id;
                        return pc.setRemoteDescription(answer);
                    }).then(function() {
                        console.debug("Creating answer")
                        return pc.createAnswer();
                    }).then(function(answer) {
                        console.debug("Setting answer", JSON.stringify(answer))
                        return pc.setLocalDescription(answer);
                    }).then(function() {
                        console.debug("Starting ICE")
                        // wait for ICE gathering to complete
                        return new Promise(function(resolve) {
                            if (pc.iceGatheringState === 'complete') {
                                console.debug("ICE already completed")
                                resolve();
                            } else {
                                function checkState() {
                                    console.debug("ICE", pc.iceGatheringState)
                                    if (pc.iceGatheringState === 'complete') {
                                        pc.removeEventListener('icegatheringstatechange', checkState);
                                        resolve();
                                    }
                                }
                                pc.addEventListener('icegatheringstatechange', checkState);
                            }
                        });
                    }).then(function(answer) {
                        var offer = pc.localDescription;
                        console.debug("Making offer")
                        return fetch(url, {
                            body: JSON.stringify({
                                type: offer.type,
                                id: pc.remote_pc_id,
                                sdp: offer.sdp,
                            }),
                            headers: httpHeaders,
                            method: 'POST'
                        })
                    }).then(function(response) {
                        if (response.status != 200) {
                            if (!preventAutoReconnect) window.setTimeout(() => startWebRTC(true), 0);
                            throw `Unexpected HTTP status ${D}response.status} code, check your webcam server (2)`
                        }
                        
                        console.debug("Received offer response")
                        return response.json();
                    }).catch(function(e) {
                        console.error("Failed to initiate WebRTC", e, JSON.stringify(e))
                        moveToState({state: 'error', errorMsg: e, error: e.toString()})
                    });
                }
                
                function stopWebRTC() {
                    if (webRtcPlaying) {
                        webRtcPlaying = false
                        console.log("Stopping WebRTC")
                        pc.close();
                    }
                }
        </script>
        </body>
        </html>
    """.trimIndent()
    //endregion
    //region val janusHtml = ...
    val janusHtml = """
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
            <title>Janus</title>
            <link href="data:;base64,iVBORw0KGgo=" rel="icon">
            <style>
            $sharedCss
            </style>
            <script>
            $jqueryJs
            </script>
            <script>
            $janusAdapterJs
            </script>
            <script>
            $janusJs
            </script>
        </head>
        <body>
            <div id="mstreamvideo" class="streamstage"></div>
            <div id="mstreamv" class="streamstage"></div>
    
            <script>
                const url = "$URL_PLACEHOLDER";
    
                var janus = null;
                var streaming = null;
                var opaqueId = "octoapp-"+Janus.randomString(12);
                var remoteTracks = {}, remoteVideos = 0, dataMid = null;
                var bitrateTimer = {};
                var spinner = {};
                var simulcastStarted = {}, svcStarted = {};
                var streamsList = {};
                var selectedStream = null;
                
                console.log("JANUS!")
               
                function setupVideoCallbacks() {}
                
                function startStream() {
                    console.log("Selected video id #" + selectedStream);
                    // Prepare the request to start streaming and send it
                    var body = { request: "watch", id: parseInt(selectedStream) || selectedStream };
                    // Notice that, for RTP mountpoints, you can subscribe to a subset
                    // of the mountpoint media, rather than them all, by adding a "stream"
                    // array containing the list of stream mids you're interested in, e.g.:
                    //
                    //		body.streams = [ "0", "2" ];
                    //
                    // to only subscribe to the first and third stream, and skip the second
                    // (assuming those are the mids you got from a "list" or "info" request).
                    // By default, you always subscribe to all the streams in a mountpoint
                    streaming.send({ message: body });
                    // Get some more info for the mountpoint to display, if any
                }
                
                
                function startWebRTC(preventAutoReconnect) {
                    webRtcPlaying = true
                    Janus.init({debug: ["error", "warn"], callback: function() {
                        janus = new Janus({
                            server: url,
                            success: function() {
                                // Attach to Streaming plugin
                                janus.attach({
                                    plugin: "janus.plugin.streaming",
                                    opaqueId: opaqueId,
                                    success: function(pluginHandle) {
                                        console.log('Plugin attached! (' + pluginHandle.getPlugin() + ', id=' + pluginHandle.getId() + ')')
                                        streaming = pluginHandle
                                        const body = { 'request': 'list' }
                                        console.debug('Sending message (' + JSON.stringify(body) + ')')
                                        pluginHandle.send({
                                            'message': body, success: function (result) {
                                            console.log("Got streams:", JSON.stringify(result.list));
                                            streamsList = result.list
                                            selectedStream = 0
                                            startStream()
                                            }
                                        })
                                    },
                                    error: function(error) {
                                        moveToState({state: 'error', error: JSON.stringify(error), errorMsg: `Failed to attach plugin ${'$'}{url}`})
                                        scheduleReconnect()
                                    },
                                    iceState: function(state) {
                                        console.log("ICE state changed to " + state);
                                    },
                                    webrtcState: function(on) {
                                        console.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
                                    },
                                    slowLink: function(uplink, lost, mid) {
                                        console.warn("Janus reports problems " + (uplink ? "sending" : "receiving") +
                                            " packets on mid " + mid + " (" + lost + " lost packets)");
                                    },
                                    onmessage: function(msg, jsep) {
                                        console.info(" ::: Got a message :::", JSON.stringify(msg), JSON.stringify(jsep));
                                        var result = msg["result"];
                                        if(result) {
                                            if(result["status"]) {
                                                var status = result["status"];
                                                if(status === 'starting')
                                                    ${'$'}('#status').removeClass('hide').text("Starting, please wait...").show();
                                                else if(status === 'started')
                                                    ${'$'}('#status').removeClass('hide').text("Started").show();
                                                else if(status === 'stopped')
                                                    stopStream();
                                            } else if(msg["streaming"] === "event") {
                                                // Does this event refer to a mid in particular?
                                                var mid = result["mid"] ? result["mid"] : "0";
                                                // Is simulcast in place?
                                                var substream = result["substream"];
                                                var temporal = result["temporal"];
                                                if((substream !== null && substream !== undefined) || (temporal !== null && temporal !== undefined)) {
                                                    if(!simulcastStarted[mid]) {
                                                        simulcastStarted[mid] = true;
                                                        addSimulcastButtons(mid);
                                                    }
                                                    // We just received notice that there's been a switch, update the buttons
                                                    updateSimulcastButtons(mid, substream, temporal);
                                                }
                                                // Is VP9/SVC in place?
                                                var spatial = result["spatial_layer"];
                                                temporal = result["temporal_layer"];
                                                if((spatial !== null && spatial !== undefined) || (temporal !== null && temporal !== undefined)) {
                                                    if(!svcStarted[mid]) {
                                                        svcStarted[mid] = true;
                                                        addSvcButtons(mid);
                                                    }
                                                    // We just received notice that there's been a switch, update the buttons
                                                    updateSvcButtons(mid, spatial, temporal);
                                                }
                                            }
                                        } else if(msg["error"]) {
                                            bootbox.alert(msg["error"]);
                                            stopStream();
                                            return;
                                        }
                                        if(jsep) {
                                            console.debug("Handling SDP as well...", jsep);
                                            var stereo = (jsep.sdp.indexOf("stereo=1") !== -1);
                                            // Offer from the plugin, let's answer
                                            streaming.createAnswer(
                                                {
                                                    jsep: jsep,
                                                    // We want recvonly audio/video and, if negotiated, datachannels
                                                    media: { audioSend: false, videoSend: false, data: true },
                                                    customizeSdp: function(jsep) {
                                                        if(stereo && jsep.sdp.indexOf("stereo=1") == -1) {
                                                            // Make sure that our offer contains stereo too
                                                            jsep.sdp = jsep.sdp.replace("useinbandfec=1", "useinbandfec=1;stereo=1");
                                                        }
                                                    },
                                                    success: function(jsep) {
                                                        console.debug("Got SDP!", jsep);
                                                        var body = { request: "start" };
                                                        streaming.send({ message: body, jsep: jsep });
                                                        ${'$'}('#watch').html("Stop").removeAttr('disabled').unbind('click').click(stopStream);
                                                    },
                                                    error: function(error) {
                                                        Janus.error("WebRTC error:", error);
                                                        bootbox.alert("WebRTC error... " + error.message);
                                                    }
                                                });
                                        }
                                    },
                                    onremotetrack: function(track, mid, on) {
                                        console.warn("Remote track (mid=" + mid + ") " + (on ? "added" : "removed") + ":", track);
                                        if(track.kind !== "video") return
                                        
                                        var mstreamId = "mstream"+mid;
                                        if(streamsList[selectedStream] && streamsList[selectedStream].legacy)
                                            mstreamId = "mstream0";
                                        if(!on) {
                                            // Track removed, get rid of the stream and the rendering
                                            var stream = remoteTracks[mid];
                                            if(stream) {
                                                try {
                                                    var tracks = stream.getTracks();
                                                    for(var i in tracks) {
                                                        var mst = tracks[i];
                                                        if(mst)
                                                            mst.stop();
                                                    }
                                                } catch(e) {}
                                            }
                                            ${'$'}('#remotevideo' + mid).remove();
                                            delete remoteTracks[mid];
                                            return;
                                        }
                                        // If we're here, a new track was added
                                        var stream = null;
    
                                        // New video track: create a stream out of it
                                        stream = new MediaStream();
                                        stream.addTrack(track.clone());
                                        remoteTracks[mid] = stream;
                                        console.log("Created remote video stream:", stream);
                                        if (${'$'}('#remotevideo' + mid).length === 0) {
                                            ${'$'}('#'+mstreamId).append('<video class="rounded centered hide" id="remotevideo' + mid + '" width="100%" height="100%" playsinline/>');
                                        }
                                        ${'$'}('#remotevideo'+mid).get(0).volume = 0;
                                        // Use a custom timer for this stream
                                        if(!bitrateTimer[mid]) {
                                            ${'$'}('#curbitrate'+mid).removeClass('hide').show();
                                            bitrateTimer[mid] = setInterval(function() {
                                                if(!${'$'}("#remotevideo" + mid).get(0))
                                                    return;
                                                // Display updated bitrate, if supported
                                                var bitrate = streaming.getBitrate(mid);
                                                ${'$'}('#curbitrate'+mid).text(bitrate);
                                                // Check if the resolution changed too
                                                var width = ${'$'}("#remotevideo" + mid).get(0).videoWidth;
                                                var height = ${'$'}("#remotevideo" + mid).get(0).videoHeight;
                                                if(width > 0 && height > 0) {
                                                    moveToState({state: 'playing', width: width, height: height})
                                                }
                                            }, 1000);
                                        }
                                        
                                        // Play the stream and hide the spinner when we get a playing event
                                        ${'$'}("#remotevideo" + mid).bind("playing", function (ev) {
                                            moveToState({state: 'playing', width: this.videoWidth, height: this.videoHeight})
                                            window.clearTimeout(reconnectTimeoutId)
                                            window.clearTimeout(iOSBugTimeoutId)
                                        });
                                         ${'$'}("#remotevideo" + mid).bind("loadeddata", () => {
                                            console.debug("Loaded data")
                                            window.clearTimeout(iOSBugTimeoutId)
                                            iOSBugTimeoutId = window.setTimeout(() => {
                                                // https://developer.apple.com/forums/thread/739686
                                                console.error("Playback didn't start 10s after data was available -> iOS bug detected")
                                                moveToState({state: 'error', errorMsg: 'Due to a bug in iOS, WebRTC is not supported on this device'})
                                            }, 10000)
                                        });
                                        Janus.attachMediaStream(${'$'}('#remotevideo' + mid).get(0), stream);
                                        ${'$'}('#remotevideo' + mid).get(0).play();
                                        ${'$'}('#remotevideo' + mid).get(0).volume = 1;
                                    },
                                    oncleanup: function() {
                                        console.debug("Got cleanup, reloading")
                                        window.location.reload()
                                    }
                                });
                            },
                            error: function(error) {
                                moveToState({state: 'error', error: JSON.stringify(error), errorMsg: `Failed to play WebRTC`})
                                scheduleReconnect()
                            },
                            destroyed: function() {
                                window.location.reload();
                            }
                        });
                    }});
                }
                $sharedJs
            </script>
        </body>
        </html>
    """.trimIndent()
    //endregion
    //region val mediaMtxHtml = ...
    val mediaMtxHtml = """
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <style>
        $sharedCss
        
        #message {
        	position: absolute;
        	left: 0;
        	top: 0;
        	width: 100%;
        	height: 100%;
        	display: flex;
        	align-items: center;
        	text-align: center;
        	justify-content: center;
        	font-size: 16px;
        	font-weight: bold;
        	color: white;
        	pointer-events: none;
        	padding: 20px;
        	box-sizing: border-box;
        	text-shadow: 0 0 5px black;
        }
        </style>
        <script defer src="./reader.js"></script>
        </head>
        <body>

        <video id="video"></video>
        <div id="message"></div>

        <script>
        $sharedJs
        $mediaMtxJs

        const video = document.getElementById('video');
        const message = document.getElementById('message');
        let defaultControls = false;

        function setupVideoCallbacks() {
            setupVideoCallbacksFor(video);
        }
                
        const setMessage = (str) => {
          if (str !== '') {
            video.controls = false;
          } else {
            video.controls = defaultControls;
          }
          message.innerText = str;
        };

        const parseBoolString = (str, defaultVal) => {
          str = (str || '');

          if (['1', 'yes', 'true'].includes(str.toLowerCase())) {
            return true;
          }
          if (['0', 'no', 'false'].includes(str.toLowerCase())) {
            return false;
          }
          return defaultVal;
        };

        const loadAttributesFromQuery = () => {
          const params = new URLSearchParams(window.location.search);
          video.controls = parseBoolString(params.get('controls'), true);
          video.muted = parseBoolString(params.get('muted'), true);
          video.autoplay = parseBoolString(params.get('autoplay'), true);
          video.playsInline = parseBoolString(params.get('playsinline'), true);
          defaultControls = video.controls;
        };

       function startWebRTC() {
          webRtcPlaying = true
          console.log("Start")
          loadAttributesFromQuery();

          new MediaMTXWebRTCReader({
            url: new URL('whep', "$URL_PLACEHOLDER"),
            onError: (err) => {
              setMessage(err);
            },
            onTrack: (evt) => {
              setMessage('');
              video.srcObject = evt.streams[0];
            },
          });
       }

       function stopWebRTC() {
           webRtcPlaying = false
           video.remove()
       }
        </script>

        </body>
        </html>

    """.trimIndent()

    //endregion
    //region val go2rtcHtml = ...
    val go2rtcHtml = """
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>go2rtc</title>
            <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
            <style type="text/css">
                $sharedCss

                body {
                    display: flex;
                }

                html, body {
                    height: 100%;
                    width: 100%;
                }

                .flex {
                    flex-wrap: wrap;
                    align-content: flex-start;
                    align-items: flex-start;
                }
                
                .info {
                    display: none !important;
                }
    
            </style>
            <script type="module">
                $go2rtcJs
            </script>
        </head>
        <body>
            <script>
                $sharedJs
                
                var setupVideos = []
            
                function setupVideoCallbacks() {
                     for(const video of document.getElementsByTagName('video')) {
                        if (!setupVideos.includes(video)) {
                            console.log("Found video!");
                            setupVideoCallbacksFor(video);
                            setupVideos.push(video);
                        }
                     }
                    setTimeout(setupVideoCallbacks, 500)
                }
                
                function startWebRTC() {
                    webRtcPlaying = true
                    console.log("Start")

                    const params = new URLSearchParams(location.search);

                    // support multiple streams and multiple modes
                    const streams = params.getAll('src');
                    const modes = params.getAll('mode');
                    if (modes.length === 0) modes.push('');
                    if (modes.length === 0) streams.push('');

                    const background = params.get('background') !== 'false';
                    const width = '1 0 ' + (params.get('width') || '320px');

                    /** @type {VideoStream} */
                    const video = document.createElement('video-stream');
                    video.background = background;
                    video.mode = modes[0] || video.mode;
                    video.style.flex = width;
                    if (("$URL_PLACEHOLDER".indexOf("http://") == 0 || "$URL_PLACEHOLDER".indexOf("https://") == 0) && "$URL_PLACEHOLDER".indexOf("api/ws?src=") > 0) {
                         video.src = new URL("$URL_PLACEHOLDER");
                    } else {
                        video.src = new URL('api/ws?src=' + encodeURIComponent(streams[0]), "$URL_PLACEHOLDER");
                    }
                    document.body.appendChild(video);
                }
                
                function stopWebRTC() {
                    webRtcPlaying = false
                    for(const tag of document.getElementsByTagName('video-stream')) {
                        tag.remove()
                    }
                }
            </script>
        </body>
    """.trimIndent()
    //endregion
    //region val iFrameHtml = ...
    val iFrameHtml = """
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>iframe</title>
            <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
            <style type="text/css">
                $sharedCss
                
                html {
                    width: 100%;
                    height: 100%;
                }
                
                body {
                    width: 100%;
                    height: 100%;
                }
                
                .iframe-container {
                    position: relative;
                    padding-bottom: 56.5%;
                    height: 0;    
                }
                .iframe-container iframe {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                }
            </style>
        </head>
        <body>
            <div class="iframe-container">
                <iframe src="$URL_PLACEHOLDER"></iframe>
            </div>
            <script>
                $sharedJs
            
                function setupVideoCallbacks() {}
                function startWebRTC() {}
                
                const urlParams = new URLSearchParams(window.location.search);
                const width = parseInt(urlParams.get('octoAppVideoWidth')) || 1280;
                const height = parseInt(urlParams.get('octoAppVideoHeight')) || 960;
                moveToState({state: "playing", width: width, height: height})
            </script>
        </body>
    """.trimIndent()
    //endregion
    //region val hlsHtml = ...
    val hlsHtml = """
         <!doctype html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>HLS</title>
            <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
            <style type="text/css">
                $sharedCss

                body {
                    display: flex;
                }

                html, body {
                    height: 100%;
                    width: 100%;
                }

                .flex {
                    flex-wrap: wrap;
                    align-content: flex-start;
                    align-items: flex-start;
                }
                
                .info {
                    display: none !important;
                }
            </style>
            <script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
        </head>
        <body>
            <div class="streamstage" id="streamstage">
                <video id="video" playsinline muted></video>
            </div>
            <script>
                $sharedJs
                
                const video = document.getElementById('video');
                setupVideoCallbacksFor(video);
                
                function reportError() {
                    moveToState({
                        state: 'error',
                         error: {
                            status: xhr.status,
                            statusText: xhr.statusText
                         }, 
                         errorMsg: `Failed to play HLS: $D{xhr.status} - $D{xhr.statusText}`
                    })
                }

                function setupVideoCallbacks() {}

                function startWebRTC() {
                    stopWebRTC();
                    webRtcPlaying = true
                    console.log("Start")

                    if (Hls.isSupported()) {
                        const hls = new Hls({
                            xhrSetup: function(xhr, url) {
                                const headers = JSON.parse('$EXTRA_HEAERS_PLACEHOLDER');
                                for (const key in headers) {
                                    xhr.setRequestHeader(key, headers[key]);
                                }
                            }
                        });
                        hls.loadSource('$URL_PLACEHOLDER');
                        hls.attachMedia(video);
                        hls.on(Hls.Events.MANIFEST_PARSED, () => {
                            video.play();
                        });
                        hls.on(Hls.Events.ERROR, (error) => {
                             moveToState({
                                 state: 'error',
                                 error: JSON.stringify(error), 
                                 errorMsg: `Failed to play HLS: ${D}{JSON.stringify(error)}`
                            });
                        });
                    } else {
                         moveToState({
                            state: 'error',
                             error: {}, 
                             errorMsg: `HLS not supported`
                        })
                    }
                }
                
                function stopWebRTC() {
                    webRtcPlaying = false
                    video.src = null;
                }
            </script>
        </body>
    """.trimIndent()
    //endregion
}