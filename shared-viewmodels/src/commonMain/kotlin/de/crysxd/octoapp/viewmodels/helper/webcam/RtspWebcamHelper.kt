package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.exceptions.WebcamDisabledException
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State
import kotlinx.coroutines.flow.FlowCollector

internal class RtspWebcamHelper(private val core: BaseViewModelCore) {

    suspend fun emit(
        collector: FlowCollector<State>,
        settings: ResolvedWebcamSettings.RtspSettings,
        activeWebcam: Int,
        webcamCount: Int,
    ) {
        val error = State.Error(
            webcamCount = webcamCount,
            canRetry = false,
            canShowDetails = false,
            activeWebcam = activeWebcam,
            exception = WebcamDisabledException("RTSP webcams are not yet supported"),
            webcamUrl = settings.url,
            displayName = settings.webcam.displayName,
        )

        collector.emit(error)
    }
}


