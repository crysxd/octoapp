package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.exceptions.obico.ObicoTunnelNotFoundException
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import de.crysxd.octoapp.sharedexternalapis.printconfidence.obico.ObicoPrintConfidenceProvider
import de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere.OctoEverywherePrintConfidenceProvider
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.runningFold
import kotlinx.coroutines.flow.shareIn

class PrintConfidenceViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "PrintConfidenceViewModelCore"
    private val octoPrintRepository = SharedBaseInjector.get().printerConfigRepository
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val octoEverywherePrintConfidenceProvider = OctoEverywherePrintConfidenceProvider()
    private val obicoPrintConfidenceProvider = ObicoPrintConfidenceProvider()

    val state: Flow<State> = octoPrintRepository.instanceInformationFlow(instanceId)
        .filterNotNull()
        .map { determinePrintConfidenceService(it) }
        .distinctUntilChanged()
        .emitEvents()
        .retry(3)
        .catch { Napier.e(tag = tag, message = "Stopping print quality", throwable = it) }
        .onStart { Napier.i(tag = tag, message = "Observing print confidence for $instanceId") }
        .onCompletion { Napier.i(tag = tag, message = "Stopping print confidence for $instanceId") }
        .map { State(it) }
        .shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay, replay = 1)

    private fun determinePrintConfidenceService(instance: PrinterConfigurationV3): Service? {
        val oeToken = instance.octoEverywhereConnection?.apiToken

        return when {
            // We have any OE token?
            oeToken != null -> Service.OctoEverywhere(oeToken).also {
                Napier.i(tag = tag, message = "Using OctoEverywhere")
            }

            // Obico is primary connection?
            instance.webUrl.isObicoUrl() -> Service.Obico(
                baseUrl = instance.webUrl.toString(),
                apiKey = instance.apiKey
            ).also {
                Napier.i(tag = tag, message = "Using primary Obico connection")
            }

            // Obico is alternative connection?
            instance.alternativeWebUrl?.isObicoUrl() == true -> Service.Obico(
                baseUrl = instance.alternativeWebUrl.toString(),
                apiKey = instance.apiKey
            ).also {
                Napier.i(tag = tag, message = "Using alternative Obico connection")
            }

            // Custom Obico is alternative connection?
            instance.alternativeWebUrl != null && instance.alternativeWebUrlPlugin == OctoPlugins.Obico -> Service.Obico(
                baseUrl = instance.alternativeWebUrl.toString(),
                apiKey = instance.apiKey
            ).also {
                Napier.i(tag = tag, message = "Using alternative Obico connection with custom instance")
            }

            else -> null
        }
    }

    private fun isPrinting() = octoPrintProvider.passiveCurrentMessageFlow(tag = "print-confidence", instanceId = instanceId)
        .map { it.state.flags.isPrinting() }
        .distinctUntilChanged()

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun Flow<Service?>.emitEvents(): Flow<PrintConfidence?> {
        var skipCount = 0

        return combine(isPrinting()) { service, isPrinting ->
            service to isPrinting
        }.flatMapLatest { (service, isPrinting) ->
            // Do not use any service if we are not printing
            if (!isPrinting) return@flatMapLatest flowOf(null)

            when (service) {
                is Service.OctoEverywhere -> {
                    Napier.i(tag = tag, message = "Print confidence from OctoEverywhere's Gadget")
                    octoEverywherePrintConfidenceProvider.forAppKey(service.appKey).checkPrintQuality()
                }

                is Service.Obico -> {
                    Napier.i(tag = tag, message = "Print confidence from Obico")
                    val obicoOnlyInfo = requireNotNull(octoPrintRepository.get(instanceId)?.onlyObico()) { "Unable to find $instanceId" }
                    val obicoOnlyInstance = octoPrintProvider.createAdHocPrinter(obicoOnlyInfo, logTag = "HTTP/PrintConfidenceControlsViewModel")
                    obicoPrintConfidenceProvider.create(
                        confidence = { obicoOnlyInstance.asOctoPrint().obicoApi.getPrintPrediction().normalized },
                        shouldRetry = { it !is ObicoTunnelNotFoundException }
                    ).checkPrintQuality()
                }

                null -> {
                    Napier.i(tag = tag, message = "Print confidence not available")
                    flowOf(null)
                }
            }
        }.runningFold<PrintConfidence?, PrintConfidence?>(null) { previous, current ->
            val currentOk = current?.confidence != null

            when {
                // Current value can be emitted
                currentOk -> {
                    skipCount = 0
                    current
                }

                // Allow up to x nok values to be skipped and replaced with previous
                skipCount < 5 -> {
                    skipCount++
                    Napier.i(tag = tag, message = "Skipped value with description ${current?.description}")
                    previous ?: current
                }

                // Skipped too many, use current
                else -> current
            }
        }
    }

    private fun PrinterConfigurationV3.onlyObico() = if (webUrl.isObicoUrl()) {
        copy(alternativeWebUrl = null)
    } else if (alternativeWebUrl?.isObicoUrl() == true) {
        copy(webUrl = alternativeWebUrl!!, alternativeWebUrl = null)
    } else {
        throw IllegalStateException("Supposed to create only Obico instance but no obico URL: $webUrl $alternativeWebUrl")
    }

    data class State(
        val confidence: PrintConfidence?
    )

    private sealed class Service {
        data class OctoEverywhere(val appKey: String) : Service()
        data class Obico(val baseUrl: String, val apiKey: String) : Service()
    }
}