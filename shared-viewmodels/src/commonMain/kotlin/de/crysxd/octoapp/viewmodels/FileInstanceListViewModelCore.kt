package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.label
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy

class FileInstanceListViewModelCore : BaseViewModelCore(instanceId = "") {

    private val repository = SharedBaseInjector.get().printerConfigRepository
    private val preferences = SharedBaseInjector.get().preferences

    val instances = combine(
        repository.allInstanceInformationFlow(),
        preferences.updatedFlow2.distinctUntilChangedBy { it.controlCenterSettings },
    ) { instances, _ ->
        val mapped = instances.values.sorted().map { instance ->
            Instance(
                label = instance.label,
                type = instance.systemInfo?.interfaceType.label,
                colors = instance.colors,
                url = UriLibrary.getFileManagerUri(instanceId = instance.id, label = instance.label, path = "/").toString()
            )
        }

        InstanceList(mapped)
    }

    data class Instance(
        val label: String,
        val type: String,
        val colors: Settings.Appearance.Colors,
        val url: String
    )

    data class InstanceList(
        val instances: List<Instance>
    )
}