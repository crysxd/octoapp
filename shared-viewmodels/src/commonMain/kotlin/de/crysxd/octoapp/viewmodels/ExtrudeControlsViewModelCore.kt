package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.ExtrusionHistoryItem
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.ExtrudeFilamentUseCase
import de.crysxd.octoapp.base.usecase.execute
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

@OptIn(ExperimentalCoroutinesApi::class)
class ExtrudeControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "ExtrudeControlsViewModelCore"
    private val extrudeFilamentUseCase = SharedBaseInjector.get().extrudeFilamentUseCase()
    private val extrusionHistoryRepository = SharedBaseInjector.get().extrusionHistoryRepository
    private val getExtrusionShortcutsUseCase = SharedBaseInjector.get().getExtrusionShortcutsUseCase()
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val octoPrintRepository = SharedBaseInjector.get().printerConfigRepository

    val items = flow { emit(getExtrusionShortcutsUseCase.execute()) }
        .flatMapLatest { it }
        .distinctUntilChangedBy { it.map { item -> item.isFavorite to item.distanceMm } }
        .map { State(it) }

    val visible = octoPrintProvider.passiveCurrentMessageFlow(tag = "ExtrudeControlsViewModelCore", instanceId = instanceId)
        .combine(octoPrintRepository.instanceInformationFlow(instanceId)) { current, config ->
            val isPrinting = current.state.flags.isPrinting()
            val isPaused = current.state.flags.paused
            val hasBetterGbrl = config?.hasPlugin(OctoPlugins.BetterGrblSupport) == true
            !hasBetterGbrl && (!isPrinting || isPaused)
        }
        .map { Visibility(it) }
        .distinctUntilChanged()

    fun toggleFavourite(distanceMm: Int) = AppScope.launch(coroutineExceptionHandler) {
        extrusionHistoryRepository.toggleFavourite(distanceMm)
    }

    suspend fun extrude(distanceMm: Int, confirmed: Boolean) = try {
        if (!confirmed && octoPrintProvider.getLastCurrentMessage(instanceId)?.state?.flags?.isPrinting() == true) {
            // While we are printing this action needs separate confirmation
            Result.NeedsConfirmation
        } else {
            extrudeFilamentUseCase.execute(
                ExtrudeFilamentUseCase.Param(
                    extrudeLengthMm = distanceMm,
                    instanceId = instanceId,
                )
            )

            // Done!
            Result.Executed
        }
    } catch (e: ExtrudeFilamentUseCase.ColdExtrusionException) {
        Napier.i(tag = tag, message = "Prevented cold extrusion")
        Result.ColdExtrusionPrevented
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Failed to extrude material", throwable = e)
        Result.Failed
    }

    data class State(val items: List<ExtrusionHistoryItem>)

    data class Visibility(val visible: Boolean)

    enum class Result {
        Executed, ColdExtrusionPrevented, NeedsConfirmation, Failed
    }
}