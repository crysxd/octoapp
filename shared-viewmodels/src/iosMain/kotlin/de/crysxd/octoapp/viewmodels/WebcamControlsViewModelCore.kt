package de.crysxd.octoapp.viewmodels

actual interface VideoPlayer

actual data class WebContent(
    val html: String,
    val baseUrl: String,
)