package de.crysxd.octoapp.iosbase

import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.logging.DarwinAntilog
import de.crysxd.octoapp.base.logging.FirebaseAntiLog
import de.crysxd.octoapp.sharedcommon.Platform
import io.github.aakira.napier.Napier

fun initLogging(adapter: FirebaseAntiLog.Adapter?, isDebug: Boolean) {
    if (!Platform.isExtension) {
        Napier.base(CachedAntiLog)
    }

    if (isDebug) {
        adapter?.let(FirebaseAntiLog::initAdapter)
        Napier.base(FirebaseAntiLog)
        Napier.base(DarwinAntilog)
    }
}


object Napier {
    fun v(tag: String, message: String) = Napier.v(tag = tag, message = message)
    fun d(tag: String, message: String) = Napier.d(tag = tag, message = message)
    fun i(tag: String, message: String) = Napier.i(tag = tag, message = message)
    fun w(tag: String, message: String) = Napier.w(tag = tag, message = message)
    fun e(tag: String, message: String) = Napier.e(tag = tag, message = message)
}