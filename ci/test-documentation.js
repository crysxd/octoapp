import http from 'http'
import url from 'url'
import fs from 'fs'
import path from 'path'
import { exec } from 'child_process'

const __dirname = path.resolve();
var recordingDir = path.resolve(__dirname, '../app-phone/build/outputs/androidTest-results/recordings/')
if (fs.existsSync(recordingDir)){
    fs.rmSync(recordingDir, { recursive: true, force: true });
}
fs.mkdirSync(recordingDir, { recursive: true });

const requestListener = function (req, res) {
    var query = url.parse(req.url, true).query;
    if (!query) {
        res.writeHead(400);
        res.end('Missing quey params "command" and "name"');
        return;
    }

    var command = query.command;
    var name = query.name;
    if (!command) {
        res.writeHead(400);
        res.end('Missing quey params "command"');
        return;
    }

    if (!name) {
        res.writeHead(400);
        res.end('Missing quey params "name"');
        return;
    }

    var recordingDir = path.resolve(__dirname, '../app-phone/build/reports/androidTests/recordings/')
    var recordingPath = path.resolve(recordingDir, name + '.mp4')
    if (!fs.existsSync(recordingDir)){
        fs.mkdirSync(recordingDir, { recursive: true });
    }

    switch(command) {
        case "start":  console.log(`START ${name}`); break;
        case "drop":  console.log(`SUCCESS ${name}`); break;
        case "save":  console.log(`FAILURE ${name}`); break;
    }

    // console.log(`command=${command} name=${name} recordingPath=${recordingPath}`)
    if (command == "start") {
        res.writeHead(200);
        res.end(`Recording to ${recordingPath}`);
        exec('while killall scrcpy; do sleep 1; done', () => {
            exec(`scrcpy -r "${recordingPath}" -t -b 4M -m 600 > /dev/null &`, (error, stdout, stderr) => {
                // if (stderr) {
                //     console.log(`stderr: ${stderr}`);
                //     return;
                // }
            });
        });
    } else if (command == "save" || command == "drop") {
        exec('while killall scrcpy; do sleep 1; done', (error, stdout, stderr) => {
            if (command == "drop") {
                try {
                    fs.unlinkSync(recordingPath)
                } catch (e) {
                    console.error(`Unable to delete ${recordingPath}`)
                }
            }

            if (error) {
                res.writeHead(500);
                res.end(`${error}`);
                return;
            } else {
                res.writeHead(200);
                if (command == "drop") {
                    res.end(`Dropped file at ${recordingPath}`);
                } else {
                    res.end(`Recorded to ${recordingPath}`);
                }
            }

            if (stderr && stderr.indexOf('No matching processes') < 0) {
                console.log(`stderr: ${stderr}`);
                return;
            }
        });
    } else {
        res.writeHead(400);
        res.end(`Unknown command ${command}`);
        return;
    }
    // console.log("----------");
}

const server = http.createServer(requestListener);
server.listen(9900);