import de.crysxd.octoapp.buildscript.octoAppMultiplatformLibrary

plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.kotlinSerialization)
}

kotlin {
    androidTarget()
    iosX64()
    iosArm64()
    iosSimulatorArm64()

    sourceSets {
        commonMain {
            dependencies {
                api(libs.ktor.client.core)
                api(libs.kotlinx.serialization.json)
                api(libs.kotlinx.serialization.protobuf)
                api(libs.kotlinx.coroutines.core)
                api(libs.kotlinx.datetime)
                api(libs.napier)
                api(libs.uuid)
                api(libs.koin.core)
                api(libs.okio)
                api(libs.ktor.serialization.json)
                api(libs.ktor.client.content.negotiation)
                api(libs.ktor.client.logging)
                api(libs.ktor.client.auth)
                api(libs.ktor.client.encoding)
                api(libs.ktor.client.websockets)
                implementation(libs.colormath)
                api(libs.multiplatform.settings)
            }
        }
        commonTest {
            dependencies {
                implementation(kotlin("test"))
                api(libs.test.ktor.client.mock)
            }
        }
        androidMain {
            dependencies {
                api(libs.androidx.core.ktx)
                api(libs.ktor.client.okhttp)
                api(libs.okhttp.logging.interceptor)
            }
        }
        iosMain {
            dependencies {
                api(libs.ktor.client.darwin)
            }
        }
    }

    //region Opt-in to expect/actual + Kotlin Parcelize
    targets.configureEach {
        compilations.configureEach {
            compileTaskProvider.configure {
                compilerOptions {
                    freeCompilerArgs.addAll(
                        "-opt-in=kotlin.ExperimentalUnsignedTypes,kotlin.RequiresOptIn",
                        "-Xexpect-actual-classes"
                    )
                    freeCompilerArgs.addAll(
                        "-P",
                        "plugin:org.jetbrains.kotlin.parcelize:additionalAnnotation=de.crysxd.octoapp.sharedcommon.CommonParcelize"
                    )
                }
            }
        }
    }
    //endregion
}

octoAppMultiplatformLibrary("sharedcommon")
