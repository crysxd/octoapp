package de.crysxd.octoapp.sharedcommon.io

interface DiskCache {
    val currentSize: Long
    fun cleanUp()
    fun clear()
}