package de.crysxd.octoapp.sharedcommon.exceptions.obico

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class ObicoCantReachPrinterException(webUrl: Url) : NetworkException(
    userFacingMessage = "Obico can't reach your OctoPrint at the moment",
    technicalMessage = "Received error code 482 from Obico",
    webUrl = webUrl,
)