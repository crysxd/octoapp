package de.crysxd.octoapp.sharedcommon.url

import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import de.crysxd.octoapp.sharedcommon.http.framework.isLocalNetwork
import de.crysxd.octoapp.sharedcommon.http.framework.withHttpProtocol
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import io.ktor.http.Url

private val TailscaleIpRegex = Regex("^100.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$")

fun Url.getConnectionType() = when {
    isOctoEverywhereUrl() -> ConnectionType.OctoEverywhere
    isObicoUrl() -> ConnectionType.Obico
    isNgrokUrl() -> ConnectionType.Ngrok
    isTailscale() -> ConnectionType.Tailscale
    isLocalNetwork() -> ConnectionType.Default
    else -> ConnectionType.DefaultCloud
}

fun Url.isSharedOctoEverywhereUrl() = host.startsWith("shared-") && host.endsWith(".octoeverywhere.com")

fun Url.isNgrokUrl() = host.endsWith(".ngrok.io") || host.endsWith(".ngrok-free.app")

fun Url.isUpnPUrl() = host.startsWith(UPNP_ADDRESS_PREFIX)

fun Url.isTailscale() = TailscaleIpRegex.matches(host)

fun Url.isOctoEverywhereUrl() = host.endsWith(".octoeverywhere.com")

fun Url.isObicoUrl() = host.endsWith("thespaghettidetective.com") || host.endsWith("obico.io")

fun Url.isBasedOn(other: Url?) = other != null && this.withHttpProtocol().withoutBasicAuth().toString().removeSuffix("/")
    .startsWith(other.withHttpProtocol().withoutBasicAuth().toString().removeSuffix("/"), ignoreCase = true)
