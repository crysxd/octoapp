package de.crysxd.octoapp.sharedcommon.http.framework

import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.client.request.headers
import io.ktor.http.HttpHeaders.WWWAuthenticate
import io.ktor.http.HttpStatusCode
import io.ktor.http.takeFrom

fun HttpClient.installBasicAuthInterceptorPlugin() = plugin(HttpSend).intercept { request ->
    var authHeader: String? = null

    request.url {
        val (url, auth) = request.url.build().extractAndRemoveBasicAuth()
        authHeader = auth
        takeFrom(url)
    }

    request.headers {
        authHeader?.let { value ->
            append("Authorization", value)
        }
    }

    val call = execute(request)

    if (call.response.status == HttpStatusCode.Unauthorized) {
        // Standard case: Basic Auth
        val header = call.response.headers[WWWAuthenticate] ?: "no message"
        throw BasicAuthRequiredException(header = header, webUrl = call.request.url)
    }

    call
}