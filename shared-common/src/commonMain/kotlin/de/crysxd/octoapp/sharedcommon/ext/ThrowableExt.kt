package de.crysxd.octoapp.sharedcommon.ext

import de.crysxd.octoapp.sharedcommon.exceptions.ProxyException

//fun Throwable.isOrIsCausedBy(classNames: String): Boolean = when (this) {
//    is ProxyException -> original.isOrIsCausedBy(className) || originalCause?.isOrIsCausedBy(className) == true
//    else -> this::class.simpleName == className || this::class.qualifiedName == className || cause?.isOrIsCausedBy(className) == true
//}

inline fun <reified T : Throwable> Throwable.isOrIsCausedBy(): Boolean = T::class.qualifiedName?.let {
    isOrIsCausedBy(it)
} ?: T::class.simpleName?.let {
    isOrIsCausedBy(it)
} ?: false

fun Throwable.isOrIsCausedBy(vararg names: String): Boolean {
    fun nsErrorMatch() = this::class.simpleName == "DarwinHttpRequestException" && names.filter { it.startsWith("NS") }.any { message?.contains(it) == true }
    fun classMatch() = names.contains(this::class.simpleName) || names.contains(this::class.qualifiedName)
    return (classMatch() || nsErrorMatch()) || cause?.isOrIsCausedBy(*names) == true || (this as? ProxyException)?.original?.isOrIsCausedBy(*names) == true
}
