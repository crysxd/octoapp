package de.crysxd.octoapp.sharedcommon.http.config

import de.crysxd.octoapp.sharedcommon.http.cache.HttpDiskCache

data class HttpClientSettings(
    val timeouts: Timeouts = Timeouts(),
    val logLevel: LogLevel = LogLevel.Production,
    val logTag: String,
    val dns: Dns? = null,
    val proxySelector: ProxySelector? = null,
    val keyStore: KeyStoreProvider? = null,
    val cache: HttpDiskCache? = null,
    val extraHeaders: Map<String, String> = emptyMap(),
)