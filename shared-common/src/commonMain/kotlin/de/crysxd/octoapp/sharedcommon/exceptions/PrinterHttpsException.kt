package de.crysxd.octoapp.sharedcommon.exceptions

import de.crysxd.octoapp.sharedcommon.http.config.X509CertificateWrapper
import io.ktor.http.Url

class PrinterHttpsException(
    url: Url,
    cause: Throwable,
    val certificate: X509CertificateWrapper?,
    val weakHostnameVerificationRequired: Boolean,
) : NetworkException(
    originalCause = cause,
    webUrl = url,
    userFacingMessage = "HTTPS connection to https://${url.host}:${url.port} could not be established. Make sure you installed all required certificates on your phone."
)