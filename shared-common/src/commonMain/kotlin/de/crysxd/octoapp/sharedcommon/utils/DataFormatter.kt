package de.crysxd.octoapp.sharedcommon.utils

import de.crysxd.octoapp.sharedcommon.ext.toSuperscriptString
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.seconds

interface DataFormatter {
    fun enableTestingMode(locale: String)

    fun formatNumber(
        number: Number,
        minDecimals: Int = 0,
        maxDecimals: Int = 2,
    ): String

    fun formatDate(
        instant: Instant,
        showTime: Boolean,
        showDate: Boolean,
        showDateAlsoIfToday: Boolean,
        useCompactFutureDate: Boolean
    ): String

    fun formatNumber(
        number: Number,
        minDecimals: Int = 0,
        maxDecimals: Int = 2,
        unit: String
    ): String = formatNumber(
        number = number,
        minDecimals = minDecimals,
        maxDecimals = maxDecimals,
    ) + if (unit.isNotBlank()) " $unit" else ""

    fun formatDuration(seconds: Number): String {
        val full = seconds.toDouble().seconds
        val hours = full.inWholeHours
        val minutes = (full - hours.hours).inWholeMinutes

        val stringRes = when {
            hours > 0 -> getString("x_hours_y_mins", hours, minutes)
            minutes < 1 -> "< ${formatDuration(60)}"
            else -> getString("x_mins", minutes)
        }

        return stringRes
    }

    fun formatEta(
        seconds: Number,
        showLabel: Boolean,
        allowRelative: Boolean,
        useCompactFutureDate: Boolean,
    ): String = seconds.toDouble().let {
        val eta = Clock.System.now() + it.seconds
        val lessThanAnHour = 1.hours > it.seconds

        return when {
            lessThanAnHour && allowRelative -> formatDuration(seconds)

            showLabel -> getString(
                id = "eta_x",
                formatDate(
                    instant = eta,
                    showTime = true,
                    showDate = true,
                    useCompactFutureDate = useCompactFutureDate,
                    showDateAlsoIfToday = false,
                )
            )

            else -> formatDate(
                instant = eta,
                showTime = true,
                showDate = true,
                useCompactFutureDate = useCompactFutureDate,
                showDateAlsoIfToday = false,
            )
        }
    }

    fun formatFileSize(size: Number, minDecimals: Int = 0, maxDecimals: Int = 2): String {
        val kb = size.toDouble() / 1024f
        val mb = kb / 1024f
        val gb = mb / 1024f

        return when {
            gb >= 1 -> formatNumber(gb, maxDecimals = maxDecimals, minDecimals = minDecimals, unit = "GiB")
            mb >= 1 -> formatNumber(mb, maxDecimals = 1, minDecimals = minDecimals.coerceAtMost(1), unit = "MiB")
            else -> formatNumber(kb, maxDecimals = 0, minDecimals = 0, unit = "kiB")
        }
    }

    fun formatWeight(grams: Number, onlyGrams: Boolean = true, minDecimals: Int = 0, maxDecimals: Int = 2): String = grams.toDouble().let {
        when {
            onlyGrams -> formatNumber(grams, unit = "g")
            it.absoluteValue > 1000.0 -> formatNumber(it / 1000.0, maxDecimals = maxDecimals, minDecimals = minDecimals, unit = "kg")
            else -> formatNumber(grams, unit = "g")
        }
    }

    fun formatPercent(percent: Number, minDecimals: Int = 0, maxDecimals: Int = 0): String = formatNumber(
        number = percent,
        maxDecimals = maxDecimals,
        minDecimals = minDecimals,
        unit = "%"
    )

    fun formatTemperature(celcius: Number, minDecimals: Int = 0, maxDecimals: Int = 2): String = formatNumber(
        number = celcius,
        maxDecimals = maxDecimals,
        minDecimals = minDecimals,
        unit = "°C"
    )

    fun formatLength(mm: Number, onlyMilli: Boolean = true, pow: Int = 1, minDecimals: Int = 0, maxDecimals: Int = 2): String = mm.toDouble().let {
        val unitSuffix = when (pow) {
            1 -> ""
            else -> pow.toSuperscriptString()
        }

        when {
            onlyMilli -> formatNumber(mm, minDecimals = minDecimals, maxDecimals = maxDecimals, unit = "mm$unitSuffix")
            it.absoluteValue > 1000.0.pow(pow) -> formatNumber(it / 1000.0.pow(pow), maxDecimals = maxDecimals, minDecimals = minDecimals, unit = "m$unitSuffix")
            it.absoluteValue > 10.0.pow(pow) -> formatNumber(it / 10.0.pow(pow), maxDecimals = maxDecimals, minDecimals = minDecimals, unit = "cm$unitSuffix")
            else -> formatNumber(mm, minDecimals = minDecimals, maxDecimals = maxDecimals, unit = "mm$unitSuffix")
        }
    }

    fun formatArea(mm2: Number, onlyMilli: Boolean = true, minDecimals: Int = 0, maxDecimals: Int = 2): String = formatLength(
        mm = mm2,
        onlyMilli = onlyMilli,
        pow = 2,
        maxDecimals = maxDecimals,
        minDecimals = minDecimals,
    )

    fun formatVolume(mm3: Number, onlyMilli: Boolean = true, minDecimals: Int = 0, maxDecimals: Int = 2): String = formatLength(
        mm = mm3,
        onlyMilli = onlyMilli,
        pow = 3,
        maxDecimals = maxDecimals,
        minDecimals = minDecimals,
    )
}

expect object DefaultDataFormatter : DataFormatter