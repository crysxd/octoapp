package de.crysxd.octoapp.sharedcommon.http

import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.DownloadTooLargeException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterHttpsException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterNotFoundException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterUnavailableException
import de.crysxd.octoapp.sharedcommon.exceptions.UntrustedCertificateException
import de.crysxd.octoapp.sharedcommon.exceptions.ngrok.NgrokTunnelAuthenticationInvalidException
import de.crysxd.octoapp.sharedcommon.exceptions.ngrok.NgrokTunnelNotFoundException
import de.crysxd.octoapp.sharedcommon.exceptions.obico.ObicoCantReachPrinterException
import de.crysxd.octoapp.sharedcommon.exceptions.obico.ObicoTunnelNotFoundException
import de.crysxd.octoapp.sharedcommon.exceptions.obico.ObicoTunnelUsageLimitReachedException
import de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere.OctoEverywhereCantReachPrinterException
import de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere.OctoEverywhereConnectionNotFoundException
import de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere.OctoEverywhereSubscriptionMissingException
import de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere.OctoEverywhereTemporaryIssueException
import de.crysxd.octoapp.sharedcommon.ext.isOrIsCausedBy
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import io.github.aakira.napier.Napier
import io.ktor.client.call.HttpClientCall
import io.ktor.client.plugins.Sender
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.statement.bodyAsText
import io.ktor.http.Url
import io.ktor.util.toMap
import kotlinx.coroutines.CancellationException

class HttpExceptionGenerator {

    private val tag = "HttpExceptionGenerator"

    suspend fun generateExceptionsForCall(
        request: HttpRequestBuilder,
        sender: Sender,
        customHandling: suspend (HttpClientCall) -> Unit = {}
    ) = try {
        val call = sender.execute(request)
        customHandling(call)
        genericHandling(call)
    } catch (e: Exception) {
        rethrowException(
            exception = e,
            url = request.url.build()
        )

        // Just for the compiler. rethrowException() will already throw
        throw e
    }

    fun rethrowException(
        exception: Throwable,
        url: Url,
    ) {
        val certException = exception.findUntrustedCertificateException()
        Napier.d(tag = tag, message = "Handling ${exception::class.qualifiedName}: ${exception.message}")

        when {
            exception is CancellationException -> throw exception

            certException != null -> throw PrinterHttpsException(
                url = url,
                cause = certException,
                certificate = certException.certificate,
                weakHostnameVerificationRequired = certException.weakHostnameVerificationRequired
            )

            exception is NetworkException -> throw exception

            exception.isOrIsCausedBy(
                // DNS error
                "UnknownHostException",
                "NSURLErrorDomain Code=-1003",
                "UnresolvedAddressException",

                // Port closed
                "ConnectException",
                "NSURLErrorDomain Code=-1004",

                // Timeout
                "SocketTimeoutException",
                "ConnectTimeoutException",

                // Other
                "SocketException"
            ) -> throw PrinterUnavailableException(
                webUrl = url,
                e = exception.unpack()
            )

            exception.isOrIsCausedBy(
                "SSLHandshakeException",
                "SSLPeerUnverifiedException",
                "CertPathValidatorException",
                "NSURLErrorDomain Code=-1202",
            ) && !exception.isOrIsCausedBy(
                // We had issues with SSLHandshakeException caused by EOFException which just indicates the connection
                // was interrupted during handshake but not a actual SSL issue (happened with OctoEverywhere a lot)
                "EOFException"
            ) -> throw PrinterHttpsException(
                url = url,
                cause = exception.unpack(),
                certificate = null,
                weakHostnameVerificationRequired = false,
            )

            else -> throw NetworkException(
                originalCause = exception.unpack(),
                userFacingMessage = exception.unpack().message,
                webUrl = url,
            )
        }
    }

    private suspend fun genericHandling(
        call: HttpClientCall
    ): HttpClientCall {
        handleResponseCode(
            code = call.response.status.value,
            url = call.request.url,
            bodyAsText = { call.response.bodyAsText() },
            headers = call.response.headers.toMap()
        )

        return call
    }

    suspend fun handleResponseCode(
        code: Int,
        url: Url,
        headers: Map<String, List<String>>,
        bodyAsText: suspend () -> String?,
    ) = when (code) {
        // Generic
        101 -> Unit
        in 200..204 -> Unit
        401 -> throw generate401Exception(code = code, url = url, bodyAsText = bodyAsText, headers = headers)
        404 -> throw generate404Exception(url = url, bodyAsText = bodyAsText)
        in 500..599 -> throw generateGenericException(code = code, url = url, bodyAsText = bodyAsText)

        // OctoEverywhere
        601 -> throw OctoEverywhereCantReachPrinterException(url)
        603, 604, 606 -> throw OctoEverywhereConnectionNotFoundException(url, code)
        605 -> throw OctoEverywhereSubscriptionMissingException(url)
        607 -> throw DownloadTooLargeException(url)
        612 -> throw OctoEverywhereTemporaryIssueException(url)

        // Obico
        481 -> throw ObicoTunnelUsageLimitReachedException(url)
        482 -> throw ObicoCantReachPrinterException(url)

        else -> throw generateGenericException(code = code, url = url, bodyAsText = bodyAsText)
    }

    private suspend fun generateGenericException(
        code: Int,
        url: Url,
        bodyAsText: suspend () -> String?,
    ) = PrinterApiException(
        httpUrl = url,
        responseCode = code,
        body = bodyAsText() ?: ""
    )

    private fun Throwable.unpack(): Throwable = cause?.takeIf { this is CancellationException } ?: this

    private fun Throwable.findUntrustedCertificateException(): UntrustedCertificateException? =
        (this as? UntrustedCertificateException) ?: cause?.findUntrustedCertificateException()

    private suspend fun generate404Exception(
        url: Url,
        bodyAsText: suspend () -> String?
    ): Exception {
        val body = bodyAsText() ?: ""

        // Special handling for ngrok.com. If the body contains the error marker for tunnel gone, the setup is broken
        return if (url.isNgrokUrl() && body.contains("ERR_NGROK_3200")) {
            NgrokTunnelNotFoundException(url)
        } else {
            PrinterNotFoundException(httpUrl = url, body = body)
        }
    }

    private suspend fun generate401Exception(
        url: Url,
        code: Int,
        headers: Map<String, List<String>>,
        bodyAsText: suspend () -> String?,
    ): Exception {
        // Special case for TSD, here a 401 means the tunnel is gone (account deleted/printer removed/...)
        if (url.isObicoUrl()) {
            throw ObicoTunnelNotFoundException(url)
        }

        // Special case for OctoEverywhere, here a 401 means the tunnel is broken
        if (url.isOctoEverywhereUrl()) {
            throw OctoEverywhereConnectionNotFoundException(url, code)
        }

        // Standard case: Basic Auth
        return headers.mapKeys { (key, _) -> key.lowercase() }["www-authenticate"]?.let { authHeader ->
            val exception = BasicAuthRequiredException(header = authHeader.first(), webUrl = url)
            if (exception.userRealm == "ngrok") {
                // Special case: ngrok credentials wrong.
                throw NgrokTunnelAuthenticationInvalidException(webUrl = url)
            } else {
                throw exception
            }
        } ?: generateGenericException(
            url = url,
            code = code,
            bodyAsText = bodyAsText
        )
    }
}