package de.crysxd.octoapp.sharedcommon

expect class Platform {
    val slaveApp: Boolean
    val appVersion: String
    val appBuild: Int
    val appId: String
    val betaBuild: Boolean
    val platformVersion: String
    val platformName: String
    val cacheDirectory: String
    val debugBuild: Boolean
    val deviceName: String
    val deviceModel: String
    val deviceLanguage: String
    internal val id: String

    companion object {
        fun gc()
    }
}

val Platform.appVersionMajor: String
    get() = appVersion.split(".").take(2).joinToString(".")

internal const val PlatformAndroid = "android"
internal const val PlatformIos = "ios"
internal const val PlatformIPadOs = "ipados"

fun Platform.isAndroid() = id == PlatformAndroid
fun Platform.isDarwin() = id in listOf(PlatformIos, PlatformIPadOs)
fun Platform.isIos() = id == PlatformIos
fun Platform.isIPadOs() = id == PlatformIPadOs