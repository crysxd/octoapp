package de.crysxd.octoapp.sharedcommon.di

import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.io.FileManager
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import kotlinx.serialization.json.Json
import org.koin.core.component.inject


class SharedCommonComponent(
    platformModule: PlatformModule,
    serializationModule: SerializationModule = SerializationModule(),
    persistenceModule: PersistenceModule = PersistenceModule(),
) : BaseComponent(
    modules = listOf(
        platformModule,
        serializationModule,
        persistenceModule
    ),
) {
    val settings: SettingsStore by inject()
    val fileManager: FileManager by inject()
    val platform: Platform by inject()
    val json: Json by inject()
}
