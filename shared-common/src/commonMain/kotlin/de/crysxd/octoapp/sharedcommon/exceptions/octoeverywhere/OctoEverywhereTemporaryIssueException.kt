package de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class OctoEverywhereTemporaryIssueException(webUrl: Url) : NetworkException(
    userFacingMessage = "OctoEverywhere reported a temporary issue. If this keeps happening, please reach out to their Support via 'Learn more'.",
    technicalMessage = "Temporary issue",
    webUrl = webUrl,
    learnMoreLink = "https://octoeverywhere.com/support"
)