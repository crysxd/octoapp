package de.crysxd.octoapp.sharedcommon.di

import de.crysxd.octoapp.sharedcommon.Platform
import org.koin.core.module.Module

expect open class PlatformModule : BaseModule {
    fun providePlatform(): Platform
    override val koinModule: Module
}


