package de.crysxd.octoapp.sharedcommon.exceptions.ngrok

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_NGROK
import io.ktor.http.Url

class NgrokTunnelAuthenticationInvalidException(
    webUrl: Url
) : NetworkException(
    userFacingMessage = "The authentication credentials configured for ngrok are invalid. The ngrok tunnel is removed, you can reconfigure ngrok.",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_NGROK
}