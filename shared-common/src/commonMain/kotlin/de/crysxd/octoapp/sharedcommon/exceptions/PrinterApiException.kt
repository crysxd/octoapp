package de.crysxd.octoapp.sharedcommon.exceptions

import io.ktor.http.Url

open class PrinterApiException(
    httpUrl: Url,
    val responseCode: Int,
    val body: String
) : NetworkException(
    webUrl = httpUrl,
    technicalMessage = "Received unexpected response code $responseCode from $httpUrl (body=$body)",
    userFacingMessage = if (responseCode >= 500) {
        "Your server failed to process OctoApp's request. Try restarting your server if the issue persists. (HTTP $responseCode)"
    } else {
        "There was an error in the communication with the server, an unexpected response was received. (HTTP $responseCode)"
    }
)