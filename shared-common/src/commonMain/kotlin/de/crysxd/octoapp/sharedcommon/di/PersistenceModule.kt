package de.crysxd.octoapp.sharedcommon.di

import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.io.FileManager
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import org.koin.dsl.module


class PersistenceModule : BaseModule {

    private fun provideSettings(platform: Platform): SettingsStore = SettingsStore(platform)
    private fun provideFileManager(platform: Platform): FileManager = FileManager(platform)

    override val koinModule = module {
        single { provideSettings(get()) }
        single { provideFileManager(get()) }
    }
}