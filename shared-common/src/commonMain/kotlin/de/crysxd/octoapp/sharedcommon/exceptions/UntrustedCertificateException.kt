package de.crysxd.octoapp.sharedcommon.exceptions

import de.crysxd.octoapp.sharedcommon.http.config.X509CertificateWrapper

class UntrustedCertificateException(
    val expired: Boolean,
    val certificate: X509CertificateWrapper,
    val weakHostnameVerificationRequired: Boolean,
    val systemMessage: String,
) : Exception(if (expired) "The SSL certificate is expired (Android says: $systemMessage)" else "The SSL certificate was not trusted (Android says: $systemMessage)")