package de.crysxd.octoapp.sharedcommon.http.cache

import de.crysxd.octoapp.sharedcommon.io.DiskCache
import io.ktor.client.plugins.cache.storage.CacheStorage
import io.ktor.client.plugins.cache.storage.CachedResponseData
import io.ktor.http.Url

interface HttpDiskCache : CacheStorage, DiskCache {
    companion object Noop : HttpDiskCache {
        override suspend fun find(url: Url, varyKeys: Map<String, String>) = null
        override suspend fun findAll(url: Url): Set<CachedResponseData> = emptySet()
        override suspend fun store(url: Url, data: CachedResponseData) = Unit
        override val currentSize = 0L
        override fun cleanUp() = Unit
        override fun clear() = Unit
    }
}