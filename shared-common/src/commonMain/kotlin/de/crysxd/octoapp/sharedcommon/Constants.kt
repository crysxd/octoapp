package de.crysxd.octoapp.sharedcommon

import io.ktor.util.AttributeKey

object Constants {
    val SuppressLogging = AttributeKey<Boolean>("noLogging")
}