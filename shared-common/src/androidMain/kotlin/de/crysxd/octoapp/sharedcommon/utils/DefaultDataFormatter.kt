package de.crysxd.octoapp.sharedcommon.utils

import android.content.res.Resources
import android.text.format.DateUtils
import androidx.core.os.ConfigurationCompat
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.ext.daysFromNow
import de.crysxd.octoapp.sharedcommon.ext.isThisYear
import de.crysxd.octoapp.sharedcommon.ext.isToday
import de.crysxd.octoapp.sharedcommon.ext.toSuperscriptString
import io.github.aakira.napier.Napier
import kotlinx.datetime.Instant
import java.text.DecimalFormat
import java.util.Locale
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.seconds

actual object DefaultDataFormatter : DataFormatter {

    private val context by lazy {
        SharedCommonInjector.get().platform.context
    }

    private var localeOverride: String? = null

    override fun enableTestingMode(locale: String) {
        localeOverride = locale
    }

    override fun formatNumber(
        number: Number,
        minDecimals: Int,
        maxDecimals: Int
    ): String = DecimalFormat.getInstance(getDeviceLocale()).also {
        it.minimumFractionDigits = minDecimals
        it.maximumFractionDigits = maxDecimals
    }.format(number.toDouble())

    override fun formatDate(
        instant: Instant,
        showTime: Boolean,
        showDate: Boolean,
        showDateAlsoIfToday: Boolean,
        useCompactFutureDate: Boolean
    ): String = try {
        var flags = DateUtils.FORMAT_ABBREV_ALL
        if (showTime) flags = flags or DateUtils.FORMAT_SHOW_TIME
        if (showDate && !useCompactFutureDate && (!instant.isToday() || showDateAlsoIfToday)) flags = flags or DateUtils.FORMAT_SHOW_DATE
        if (showDate && instant.isThisYear()) flags = flags or DateUtils.FORMAT_NO_YEAR

        val result = DateUtils.formatDateTime(context, instant.toEpochMilliseconds(), flags)

        if (useCompactFutureDate && !instant.isToday()) {
            "$result${instant.daysFromNow().toSuperscriptString()}"
        } else {
            result
        }
    } catch (e: Exception) {
        Napier.e(tag = "DefaultDataFormatter", message = "Failed to format", throwable = e)
        instant.toString()
    }

    override fun formatDuration(seconds: Number) = seconds.toDouble().let {
        val full = seconds.toDouble().seconds
        val hours = full.inWholeHours
        val minutes = (full - hours.hours).inWholeMinutes

        when {
            hours > 0 -> getString("x_hours_y_mins", hours, minutes)
            minutes < 1 -> getString("less_than_a_minute")
            else -> getString("x_mins", minutes)
        }
    }

    // We can't use Locale.getDefault() because we overwrite the app language but we _always_ want to use the user's local preferences for time
    private fun getDeviceLocale() = try {
        localeOverride?.let { Locale(it) } ?: ConfigurationCompat.getLocales(Resources.getSystem().configuration)[0]
    } catch (e: Exception) {
        // Just precautionary try/catch
        Napier.e(tag = "DefaultDataFormatter", message = "Unexpected exception", throwable = e)
        null
    } ?: Locale.getDefault()
}