package de.crysxd.octoapp.sharedcommon.http

import android.annotation.SuppressLint
import de.crysxd.octoapp.sharedcommon.exceptions.UntrustedCertificateException
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.KeyStore
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.X509CertificateWrapper
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.statement.HttpReceivePipeline
import io.ktor.http.Url
import okhttp3.Call
import okhttp3.Dns
import okhttp3.EventListener
import okhttp3.Handshake
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.internal.connection.RealCall
import okhttp3.internal.tls.OkHostnameVerifier
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.Proxy.NO_PROXY
import java.net.Proxy.Type
import java.net.ProxySelector
import java.net.SocketAddress
import java.net.URI
import java.security.MessageDigest
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager
import kotlin.math.absoluteValue
import de.crysxd.octoapp.sharedcommon.http.config.Dns as Dns2
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector as ProxySelector2

internal actual fun SpecificDefaultHttpClient(settings: HttpClientSettings, config: HttpClientConfig<*>.() -> Unit) = HttpClient(OkHttp) {
    val remoteIpHeader = "x-octo-remote-ip"

    engine {
        preconfigured = OkHttpClient.Builder()
            .pingInterval(settings.timeouts.webSocketPingPongTimeout.inWholeMilliseconds, TimeUnit.MILLISECONDS)
            .proxySelector(settings.proxySelector)
            .dns(settings.dns)
            .logging(settings.logLevel)
            .selfSignedTrustManager(
                keyStore = settings.keyStore?.loadKeyStore(),
                weakVerificationHosts = settings.keyStore?.getWeakVerificationForHosts() ?: emptyList(),
            )
            .addInterceptor { chain ->
                val response = chain.proceed(chain.request())
                (chain.call() as? RealCall)?.connection?.socket()?.inetAddress?.let { address ->
                    response.newBuilder().addHeader(remoteIpHeader, address.hostAddress ?: address.hostName).build()
                } ?: response
            }
            .build()
    }

    install("OctoRemoteIp") {
        receivePipeline.intercept(HttpReceivePipeline.State) { response ->
            response.headers[remoteIpHeader]?.let { remoteIp ->
                response.call.attributes.put(OctoRemoteIpAttributeKey, remoteIp)
            }

            proceedWith(response)
        }
    }

    config()
}

// Suppress warning. We fall back on default implementation except for pinned certs from specified hosts.
@OptIn(ExperimentalStdlibApi::class)
@SuppressLint("CustomX509TrustManager")
fun OkHttpClient.Builder.selfSignedTrustManager(
    keyStore: KeyStore?,
    weakVerificationHosts: List<String>,
): OkHttpClient.Builder {
    val sslContext = SSLContext.getInstance("SSL")
    val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
    tmf.init(keyStore)
    val systemTrustManager = tmf.trustManagers.firstNotNullOf { it as? X509TrustManager }

    val hostnameVerifier = HostnameVerifier { host, session ->
        when {
            // Rely on system for default
            OkHostnameVerifier.verify(host, session) -> true

            // If this host is not weakly verified, system has the last word
            weakVerificationHosts.none { it.equals(host, ignoreCase = true) } -> false

            // Host is weakly verified, rely on subjectDN and issuerDN. System relies on subjectAltName which is missing
            else -> (session.peerCertificates.first() as? X509Certificate)?.let { cert ->
                cert.subjectDN.name.contains("CN=$host", ignoreCase = true) ||
                        cert.issuerDN.name.contains("CN=$host", ignoreCase = true)
            } ?: false
        }
    }

    val selfSignedTrustManager = object : X509TrustManager by systemTrustManager {
        override fun checkServerTrusted(chain: Array<out X509Certificate>, authType: String) {
            try {
                // Rely on system for default
                systemTrustManager.checkServerTrusted(chain, authType)
            } catch (e: CertificateException) {
                Napier.w(tag = "X509TrustManager", message = "Certificate rejected by system: ${e.message}")

                // For some reason we sometimes have trouble with OE on Pixel phones. If the expected cert is presented, we overrule the
                // system and accept it.
                val leaf = chain[0]
                val fingerprint = MessageDigest.getInstance("SHA-256").digest(leaf.publicKey.encoded).toHexString()
                if (leaf.subjectDN.toString() == "CN=octoeverywhere.com" && fingerprint == "5a46bda3f633a6765f275adda4b920778f71544ad4175b951c98da7da26970e2") {
                    Napier.e(
                        tag = "X509TrustManager",
                        message = "Accepting OctoEverywhere certificate",
                        throwable = IllegalStateException("OctoEverywhere SSL issue detected", e)
                    )
                    return
                }

                // If system declines, check if we can trust the public cert coming from this host
                // We need weak hostname verification if the SAN field is missing
                throw UntrustedCertificateException(
                    expired = try {
                        leaf.checkValidity(); false
                    } catch (e: Exception) {
                        Napier.w(tag = "X509TrustManager", message = "Certificate not valid: ${e.message}")
                        true
                    },
                    certificate = X509CertificateWrapper(leaf),
                    weakHostnameVerificationRequired = leaf.subjectAlternativeNames.sumOf { it.size } == 0,
                    systemMessage = e.message ?: ""
                )
            }
        }
    }

    sslContext.init(null, arrayOf(selfSignedTrustManager), SecureRandom())
    return this
        .sslSocketFactory(sslContext.socketFactory, selfSignedTrustManager)
        .hostnameVerifier(hostnameVerifier)
}

fun OkHttpClient.Builder.dns(dns: Dns2?): OkHttpClient.Builder = dns?.let { nested ->
    object : Dns {
        override fun lookup(hostname: String) = nested.lookup(hostname)
    }
}?.let {
    dns(it)
} ?: this

fun OkHttpClient.Builder.proxySelector(selector: ProxySelector2?): OkHttpClient.Builder = selector?.let { nested ->
    object : ProxySelector() {
        override fun select(url: URI): MutableList<Proxy> = nested.selectProxy(Url(url.toString())).map {
            when (it) {
                ProxySelector2.Proxy.NoProxy -> NO_PROXY
                is ProxySelector2.Proxy.SocksProxy -> Proxy(Type.SOCKS, InetSocketAddress(it.host, it.port))
            }
        }.toMutableList()

        override fun connectFailed(url: URI, p1: SocketAddress, e: IOException) {
            nested.connectFailed(Url(url.toString()), e)
        }
    }
}?.let {
    proxySelector(it)
} ?: this

private val Call.id get() = request().url.hashCode().absoluteValue.toString().take(5)

fun OkHttpClient.Builder.logging(level: LogLevel): OkHttpClient.Builder = if (level == LogLevel.Verbose) {
    val tag = "OkHttp/Events"

    eventListener(object : EventListener() {
        override fun callStart(call: Call) {
            super.callStart(call)
            Napier.i(tag = tag, message = "[${call.id}] Call start to ${call.request().url}")
        }

        override fun callEnd(call: Call) {
            super.callEnd(call)
            Napier.i(tag = tag, message = "[${call.id}] Call end")
        }

        override fun callFailed(call: Call, ioe: IOException) {
            super.callFailed(call, ioe)
            Napier.i(tag = tag, message = "[${call.id}] Call failed (${ioe.message})")
        }

        override fun secureConnectStart(call: Call) {
            super.secureConnectStart(call)
            Napier.i(tag = tag, message = "[${call.id}] Secure connect start")
        }

        override fun secureConnectEnd(call: Call, handshake: Handshake?) {
            super.secureConnectEnd(call, handshake)
            Napier.i(tag = tag, message = "[${call.id}] Secure connect end")
        }

        override fun connectStart(call: Call, inetSocketAddress: InetSocketAddress, proxy: Proxy) {
            super.connectStart(call, inetSocketAddress, proxy)
            Napier.i(tag = tag, message = "[${call.id}] Connect start to $inetSocketAddress")

        }

        override fun connectEnd(call: Call, inetSocketAddress: InetSocketAddress, proxy: Proxy, protocol: Protocol?) {
            super.connectEnd(call, inetSocketAddress, proxy, protocol)
            Napier.i(tag = tag, message = "[${call.id}] Connect end to $inetSocketAddress (via $proxy)")
        }

        override fun connectFailed(call: Call, inetSocketAddress: InetSocketAddress, proxy: Proxy, protocol: Protocol?, ioe: IOException) {
            super.connectFailed(call, inetSocketAddress, proxy, protocol, ioe)
            Napier.i(tag = tag, message = "[${call.id}] Connect to $inetSocketAddress failed (via $proxy, ${ioe.message})")
        }

        override fun proxySelectStart(call: Call, url: HttpUrl) {
            super.proxySelectStart(call, url)
            Napier.i(tag = tag, message = "[${call.id}] Proxy select start")
        }

        override fun proxySelectEnd(call: Call, url: HttpUrl, proxies: List<Proxy>) {
            super.proxySelectEnd(call, url, proxies)
            Napier.i(tag = tag, message = "[${call.id}] Proxy select end: $proxies")
        }
    })
} else {
    this
}