package de.crysxd.octoapp.sharedcommon.ext

import android.content.Context
import android.content.res.Configuration
import java.util.Locale

fun Context.withLocale(locale: Locale?): Context {
    val config = Configuration(resources.configuration)
    locale?.let { config.setLocale(it) }
    return createConfigurationContext(config)
}