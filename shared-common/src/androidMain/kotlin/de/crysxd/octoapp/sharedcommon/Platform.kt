package de.crysxd.octoapp.sharedcommon

import android.app.Application
import android.content.pm.PackageManager
import android.content.res.Resources
import android.os.Build
import android.util.Log
import androidx.annotation.ChecksSdkIntAtLeast
import java.io.ByteArrayInputStream
import java.security.MessageDigest
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate

actual open class Platform(
    val context: Application,
) {

    actual companion object {
        actual fun gc() = System.gc()
    }

    actual val id: String = PlatformAndroid

    @Suppress("DEPRECATION")
    private val packageInfo by lazy {
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU -> context.packageManager.getPackageInfo(
                context.packageName,
                PackageManager.PackageInfoFlags.of(PackageManager.GET_SIGNING_CERTIFICATES.toLong())
            )

            Build.VERSION.SDK_INT >= Build.VERSION_CODES.P -> context.packageManager.getPackageInfo(
                context.packageName,
                PackageManager.GET_SIGNING_CERTIFICATES
            )

            else -> context.packageManager.getPackageInfo(
                context.packageName,
                PackageManager.GET_SIGNATURES
            )
        }
    }

    private val apkSignatures
        get() = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.P -> packageInfo.signingInfo.signingCertificateHistory
            else -> packageInfo.signatures
        }

    private val sha256SignaturesFingerprints
        get() = apkSignatures?.map {
            val certStream = ByteArrayInputStream(it.toByteArray())
            CertificateFactory.getInstance("X509").generateCertificate(certStream) as X509Certificate
        }?.map {
            val md = MessageDigest.getInstance("SHA256")
            val publicKey = md.digest(it.encoded)
            byte2HexFormatted(publicKey)
        } ?: emptyList()

    actual val appVersion by lazy {
        val version = packageInfo.versionName
        if (betaBuild) "$version-beta" else version
    }

    actual val appBuild by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            packageInfo.longVersionCode.toInt()
        } else {
            @Suppress("DEPRECATION")
            packageInfo.versionCode
        }
    }

    @ChecksSdkIntAtLeast(api = 1)
    actual val platformVersion = Build.VERSION.SDK_INT.toString()

    actual open val platformName = "Android"

    actual val appId = context.packageName

    actual val cacheDirectory: String by lazy {
        (context.externalCacheDir ?: context.cacheDir).path
    }

    actual val debugBuild = BuildConfig.DEBUG

    actual val betaBuild by lazy {
        try {
            val signatures = sha256SignaturesFingerprints
            Log.i("PLATFORM", "signatures: $signatures")
            signatures.contains("A9:54:1F:3F:39:47:04:51:D0:38:38:90:7C:35:00:79:84:DE:A2:90:7A:6B:26:95:B7:A1:C1:7B:A8:73:78:BE")
        } catch (e: Exception) {
            // Just to be sure....
            Log.e("PLATFORM", "FAILED TO GET FINGERPRINT", e)
            false
        }.also {
            Log.i("PLATFORM", "$it")
        }
    }

    actual val deviceName = "${Build.BRAND.replaceFirstChar { it.uppercase() }} ${Build.MODEL.replaceFirstChar { it.uppercase() }}"

    actual val deviceModel: String = Build.MODEL

    actual val deviceLanguage: String
        get() = Resources.getSystem().configuration.locales[0].language

    private fun byte2HexFormatted(arr: ByteArray): String {
        val str = StringBuilder(arr.size * 2)
        for (i in arr.indices) {
            var h = Integer.toHexString(arr[i].toInt())
            val l = h.length
            if (l == 1) h = "0$h"
            if (l > 2) h = h.substring(l - 2, l)
            str.append(h.uppercase())
            if (i < arr.size - 1) str.append(':')
        }
        return str.toString()
    }

    actual val slaveApp get() = context.packageManager.hasSystemFeature(PackageManager.FEATURE_WATCH)
}