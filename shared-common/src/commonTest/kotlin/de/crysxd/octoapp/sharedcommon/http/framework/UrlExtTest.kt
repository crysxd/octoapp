package de.crysxd.octoapp.sharedcommon.http.framework

import io.ktor.http.Url
import kotlin.test.Test
import kotlin.test.assertEquals

class UrlExtTest {

    @Test
    fun WHEN_url_is_parsed_THEN_it_returned() {
        assertEquals(
            expected = Url("http://localhost"),
            actual = "http://fe80::6ae8::8dac::e29b::211c:80".toUrl(),
            message = "Expected invlaid URL to fall back"
        )
        assertEquals(
            expected = Url("https://user:password@host.com/something"),
            actual = "https://user:password@host.com/something".toUrl(),
            message = "Expected URL to match"
        )
    }

    @Test
    fun WHEN_path_is_resolved_THEN_it_is_appended() {
        assertEquals(
            expected = Url("https://user:password@host.com/webcam/path?query"),
            actual = Url("https://user:password@host.com/something").resolve("/webcam/path?query"),
            message = "Expected URL to match"
        )
        assertEquals(
            expected = Url("https://user:password@host.com/something/webcam/path?query"),
            actual = Url("https://user:password@host.com/something").resolve("webcam/path?query"),
            message = "Expected URL to match"
        )
        assertEquals(
            expected = Url("https://user:password@host.com/something/webcam/path?query&other=1&other=2&something=.3"),
            actual = Url("https://user:password@host.com/something").resolve("webcam/path?query&other=1&other=2&something=.3"),
            message = "Expected URL to match"
        )
        assertEquals(
            expected = Url("https://user:password@host.com/something/webcam/"),
            actual = Url("https://user:password@host.com/something").resolve("webcam/"),
            message = "Expected URL to match"
        )
        assertEquals(
            expected = Url("https://user:password@host.com/something/webcam"),
            actual = Url("https://user:password@host.com/something").resolve("webcam"),
            message = "Expected URL to match"
        )
        assertEquals(
            expected = Url("https://user:password@host.com/something/webcam/other/something"),
            actual = Url("https://user:password@host.com/something").resolve("webcam/other/something"),
            message = "Expected URL to match"
        )
        assertEquals(
            expected = Url("https://user:password@host.com/webcam/other/something?query1=test&query2=test"),
            actual = Url("https://user:password@host.com/something?query1=test").resolve("/webcam/other/something?query2=test"),
            message = "Expected URL to match"
        )
    }

    @Test
    fun WHEN_basic_auth_is_removed_THEN_it_is_removed() {
        assertEquals(
            expected = Url("https://host.com/path?query"),
            actual = Url("https://user:password@host.com/path?query").withoutBasicAuth(),
            message = "Expected auth to be removed"
        )
        assertEquals(
            expected = Url("https://host.com/path?query"),
            actual = Url("https://user@host.com/path?query").withoutBasicAuth(),
            message = "Expected auth to be removed"
        )
    }

    @Test
    fun WHEN_query_removed_THEN_it_is_removed() {
        assertEquals(
            expected = Url("https://user:password@host.com/path"),
            actual = Url("https://user:password@host.com/path?query").withoutQuery(),
            message = "Expected query to be removed"
        )
        assertEquals(
            expected = Url("https://user@host.com/path"),
            actual = Url("https://user@host.com/path?query=value&other=").withoutQuery(),
            message = "Expected query to be removed"
        )
    }


    @Test
    fun WHEN_basic_auth_is_set_THEN_it_is_removed() {
        assertEquals(
            expected = Url("https://other:secret@host.com/path?query"),
            actual = Url("https://user:password@host.com/path?query").withBasicAuth(user = "other", password = "secret"),
            message = "Expected auth to be removed"
        )
        assertEquals(
            expected = Url("https://something@host.com/path?query"),
            actual = Url("https://user@host.com/path?query").withBasicAuth(user = "something", password = null),
            message = "Expected auth to be removed"
        )
    }

    @Test
    fun WHEN_basic_auth_is_added_THEN_it_is_removed() {
        assertEquals(
            expected = Url("https://other:secret@host.com/path?query"),
            actual = Url("https://host.com/path?query").withBasicAuth(user = "other", password = "secret"),
            message = "Expected auth to be removed"
        )
        assertEquals(
            expected = Url("https://something@host.com/path?query"),
            actual = Url("https://host.com/path?query").withBasicAuth(user = "something", password = null),
            message = "Expected auth to be removed"
        )
        assertEquals(
            expected = Url("https://something@other.com/path?query"),
            actual = Url("https://host.com/path?query").withBasicAuth(user = "something", password = null).withHost("other.com"),
            message = "Expected auth to be removed"
        )
    }

    @Test
    fun WHEN_host_is_set_THEN_it_is_removed() {
        assertEquals(
            expected = Url("https://user:password@other.com/path?query"),
            actual = Url("https://user:password@host.com/path?query").withHost(host = "other.com"),
            message = "Expected auth to be removed"
        )
        assertEquals(
            expected = Url("https://other.com/path?query"),
            actual = Url("https://host.com/path?query").withHost(host = "other.com"),
            message = "Expected auth to be removed"
        )
        assertEquals(
            expected = Url("https://test:pw@other.com/path?query"),
            actual = Url("https://host.com/path?query").withHost(host = "other.com").withBasicAuth(user = "test", password = "pw"),
            message = "Expected auth to be removed"
        )
    }

    @Test
    fun WHEN_url_is_rebuild_for_logging_THEN_the_host_and_aut_is_removed() {
        assertEquals(
            expected = Url("https://basicAuthUser:basicAuthPassword@redacted-host-11fbb3c5/path?query"),
            actual = Url("https://user:password@host.com/path?query").forLogging(),
            message = "Expected auth and host to be removed"
        )
        assertEquals(
            expected = Url("https://basicAuthUser@redacted-host-11fbb3c5/path?query"),
            actual = Url("https://user@host.com/path?query").forLogging(),
            message = "Expected auth to be removed"
        )
    }

    @Test
    fun WHEN_logging_string_is_redacted_THEN_the_host_and_aut_is_removed() {
        val log = "This is a log for https://user:password@host.com/path?query where user and password is the auth"
        assertEquals(
            expected = "This is a log for https://basicAuthUser:basicAuthPassword@redacted-host-11fbb3c5/path?query where basicAuthUser and basicAuthPassword is the auth",
            actual = Url("https://user:password@host.com/path?query").redactLoggingString(log),
            message = "Expected auth and host to be removed"
        )
        assertEquals(
            expected = "This is a log for https://basicAuthUser:password@redacted-host-11fbb3c5/path?query where basicAuthUser and password is the auth",
            actual = Url("https://user@host.com/path?query").redactLoggingString(log),
            message = "Expected password and host to be removed"
        )
        assertEquals(
            expected = "This is a log for https://user:password@redacted-host-11fbb3c5/path?query where user and password is the auth",
            actual = Url("https://host.com/path?query").redactLoggingString(log),
            message = "Expected host to be removed"
        )
    }

    @Test
    fun WHEN_a_host_is_redacted_THEN_critical_values_are_removed() {
        assertEquals(
            expected = "192.168.0.1",
            actual = Url("https://192.168.0.1").redactedHost,
            message = "Expected host to be clear"
        )
        assertEquals(
            expected = "10.0.0.2",
            actual = Url("https://10.0.0.2").redactedHost,
            message = "Expected host to be clear"
        )
        assertEquals(
            expected = "172.16.0.2",
            actual = Url("https://172.16.0.2").redactedHost,
            message = "Expected host to be clear"
        )
        assertEquals(
            expected = "redacted-44d0ea42.octoeverywhere.com",
            actual = Url("https://something.octoeverywhere.com").redactedHost,
            message = "Expected host to be redacted"
        )
        assertEquals(
            expected = "redacted-31655914.ngrok.com",
            actual = Url("https://something.ngrok.com").redactedHost,
            message = "Expected host to be redacted"
        )
        assertEquals(
            expected = "redacted-88b4bac.tunnels.app.thespaghettidetective.com",
            actual = Url("https://something.tunnels.app.thespaghettidetective.com").redactedHost,
            message = "Expected host to be redacted"
        )
        assertEquals(
            expected = "redacted-6c158cd6.tunnels.app.obico.io",
            actual = Url("https://something.tunnels.app.obico.io").redactedHost,
            message = "Expected host to be redacted"
        )
        assertEquals(
            expected = "redacted-host-5b91fbb4",
            actual = Url("https://google.com").redactedHost,
            message = "Expected host to be redacted"
        )
    }

    @Test
    fun WHEN_basic_auth_is_extracted_THEN_header_is_returned() {
        assertEquals(
            expected = Url("https://host.com/path?query") to null,
            actual = Url("https://host.com/path?query").extractAndRemoveBasicAuth(),
            message = "Expected auth to be removed"
        )
        assertEquals(
            expected = Url("https://host.com/path?query") to "Basic dXNlcjpwYXNzd29yZA==",
            actual = Url("https://user:password@host.com/path?query").extractAndRemoveBasicAuth(),
            message = "Expected auth to match"
        )
        assertEquals(
            expected = Url("https://host.com/path?query") to "Basic dXNlcg==",
            actual = Url("https://user@host.com/path?query").extractAndRemoveBasicAuth(),
            message = "Expected auth to match"
        )
    }
}