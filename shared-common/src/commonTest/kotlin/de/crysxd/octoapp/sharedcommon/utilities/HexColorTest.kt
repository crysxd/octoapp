package de.crysxd.octoapp.sharedcommon.utilities

import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.sharedcommon.utils.HexColorSerializer
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlin.test.Test
import kotlin.test.assertEquals

class HexColorTest {

    private val json = Json {
        serializersModule = SerializersModule {
            contextual(HexColor::class, HexColorSerializer())
        }
    }

    @Test
    fun WHEN_a_hex_color_is_parsed_THEN_the_correct_value_is_returned() {
        assertEquals(
            expected = HexColor(red = 1f, green = 1f, blue = 0f, rawValue = "#FFFF00"),
            actual = HexColor("#FFFF00")
        )
        assertEquals(
            expected = HexColor(red = 0f, green = 1f, blue = 0f, rawValue = "#00FF00"),
            actual = HexColor("#00FF00")
        )
        assertEquals(
            expected = HexColor(red = 0f, green = 0f, blue = 1f, rawValue = "#0000FF"),
            actual = HexColor("#0000FF")
        )
        assertEquals(
            expected = HexColor(red = 0f, green = 1f, blue = 1f, rawValue = "#00FFFF"),
            actual = HexColor("#00FFFF")
        )
        assertEquals(
            expected = HexColor(red = 0f, green = 0f, blue = 1f, rawValue = "#0000FFFF"),
            actual = HexColor("#0000FFFF")
        )
        assertEquals(
            expected = HexColor(red = "9b".toInt(16) / 255f, green = "c6".toInt(16) / 255f, blue = "3b".toInt(16) / 255f, rawValue = "#9bc63b"),
            actual = HexColor("#9bc63b")
        )
    }

    @Test
    fun WHEN_a_css_name_is_parsed_THEN_the_correct_value_is_returned() {
        assertEquals(
            expected = HexColor("#87ceeb").copy(rawValue = null),
            actual = HexColor("skyblue").copy(rawValue = null)
        )
    }

    @Test
    fun WHEN_a_random_is_parsed_THEN_red_is_returned() {
        assertEquals(
            expected = HexColor(red = 1f, green = 0f, blue = 0f, rawValue = "sodsiofh"),
            actual = HexColor("sodsiofh")
        )
    }

    @Test
    fun WHEN_a_color_is_formatted_THEN_the_correct_value_is_returned() {
        assertEquals(
            expected = "#FF0080",
            actual = HexColor(red = 1f, green = 0f, blue = 0.5f, rawValue = null).toString(),
        )
        assertEquals(
            expected = "some",
            actual = HexColor(red = 1f, green = 0f, blue = 0.5f, rawValue = "some").toString(),
        )
    }

    @Test
    fun WHEN_a_color_is_serialized_THEN_the_correct_value_is_returned() {
        assertEquals(
            expected = "\"#FF0080\"",
            actual = json.encodeToString(HexColor(red = 1f, green = 0f, blue = 0.5f, rawValue = null)),
        )
        assertEquals(
            expected = "\"some\"",
            actual = json.encodeToString(HexColor(red = 1f, green = 0f, blue = 0.5f, rawValue = "some")),
        )
    }

    @Test
    fun WHEN_a_color_is_deserialized_THEN_the_correct_value_is_returned() {
        assertEquals(
            expected = HexColor(red = 1f, green = 0f, blue = 0.5f, rawValue = "#FF0080"),
            actual = json.decodeFromString("\"#FF0080\""),
        )
        assertEquals(
            expected = HexColor(red = 1f, green = 0f, blue = 0f, rawValue = "some"),
            actual = json.decodeFromString("\"some\""),
        )
    }
}