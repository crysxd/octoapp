package de.crysxd.octoapp.sharedcommon.ext

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.time.Duration.Companion.nanoseconds

class StringExtTest {

    @Test
    fun WHEN_format_is_used_THEN_result_is_correct() {
        assertEquals(
            expected = "Test 12.112342",
            actual = "Test %f".format(12.112342)
        )
        assertEquals(
            expected = "Test 12",
            actual = "Test %d".format(12)
        )
        assertEquals(
            expected = "Test Test",
            actual = "Test %s".format("Test")
        )
        assertEquals(
            expected = "Test 32.849823ms",
            actual = "Test %s".format(32849823.nanoseconds)
        )
    }
}