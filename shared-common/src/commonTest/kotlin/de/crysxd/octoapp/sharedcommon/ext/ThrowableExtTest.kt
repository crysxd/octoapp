package de.crysxd.octoapp.sharedcommon.ext

import de.crysxd.octoapp.sharedcommon.exceptions.ProxyException
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import okio.IOException
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ThrowableExtTest {

    @Test
    fun WHEN_testing_for_exception_class_THEN_correct_result_is_returned() {
        assertTrue(IOException("").isOrIsCausedBy<IOException>())
        assertTrue(IOException("").isOrIsCausedBy("IOException"))
        assertFalse(IOException("").isOrIsCausedBy("io.ktor.utils.io.errors.IOException"))
        assertFalse(IOException("").isOrIsCausedBy("IllegalStateException"))
        assertFalse(IOException("").isOrIsCausedBy<IllegalStateException>())
        assertFalse(IllegalStateException(IllegalArgumentException()).isOrIsCausedBy<IOException>())
        assertFalse(IllegalStateException(IllegalArgumentException()).isOrIsCausedBy("IOException"))
        assertTrue(IllegalStateException(IOException()).isOrIsCausedBy<IOException>())
        assertTrue(IllegalStateException(IOException()).isOrIsCausedBy("IOException"))
        assertTrue(ProxyException.create(IOException(), "".toUrl()).isOrIsCausedBy("IOException"))
        assertTrue(ProxyException.create(IllegalStateException(IOException("")), "".toUrl()).isOrIsCausedBy("IOException"))
        assertFalse(ProxyException.create(IllegalStateException(IllegalArgumentException("")), "".toUrl()).isOrIsCausedBy("IOException"))
    }
}