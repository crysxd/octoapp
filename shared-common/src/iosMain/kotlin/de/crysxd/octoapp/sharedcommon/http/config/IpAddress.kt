package de.crysxd.octoapp.sharedcommon.http.config

import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.engine.darwin.Darwin
import io.ktor.client.engine.darwin.DarwinHttpRequestException
import io.ktor.client.request.head
import kotlinx.coroutines.withTimeout
import platform.darwin.NSInteger
import kotlin.time.Duration

actual data class IpAddress(val ip: String)

actual fun ipAddressFor(ip: String) = IpAddress(ip)

actual fun IpAddress.hostNameOrIp() = ip

actual suspend fun IpAddress.assertPortOpen(port: Int, timeout: Duration) = performTest(port, timeout)

actual suspend fun IpAddress.assertReachable(timeout: Duration) = performTest(9999, timeout, -1004, -1005)

private suspend fun IpAddress.performTest(port: Int, timeout: Duration, vararg allowedErrorCodes: NSInteger) {
    withTimeout(timeout) {
        val status = try {
            // Forces a 404 if host and port are correct
            HttpClient(Darwin).head("http://${ip}:$port/doesntexist").status
        } catch (e: DarwinHttpRequestException) {
            if (e.origin.code in allowedErrorCodes) {
                // This error is fine
                -100
            } else {
                throw e
            }
        }

        Napier.v("$status")
    }
}