package de.crysxd.octoapp.sharedcommon.ext

import platform.Foundation.NSString
import platform.Foundation.stringWithFormat

actual fun String.format(vararg args: Any): String {
    val objcFormat = replace(Regex("%((?:\\.|\\d|\\$)*)[abcdefs]"), "%$1@")

    return when (args.size) {
        0 -> objcFormat
        1 -> NSString.stringWithFormat(objcFormat, args[0])
        2 -> NSString.stringWithFormat(objcFormat, args[0], args[1])
        3 -> NSString.stringWithFormat(objcFormat, args[0], args[1], args[2])
        4 -> NSString.stringWithFormat(objcFormat, args[0], args[1], args[2], args[3])
        5 -> NSString.stringWithFormat(objcFormat, args[0], args[1], args[2], args[3], args[4])
        6 -> NSString.stringWithFormat(objcFormat, args[0], args[1], args[2], args[3], args[4], args[5])
        7 -> NSString.stringWithFormat(objcFormat, args[0], args[1], args[2], args[3], args[4], args[5], args[6])
        else -> throw IllegalArgumentException("Too many arguments.")
    }
}