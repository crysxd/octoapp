package de.crysxd.octoapp.buildscript

import com.android.build.api.dsl.ApkSigningConfig
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import java.io.File
import java.util.Properties

object OctoAppBuildConfig {
    val rootDir = File(System.getProperty("user.dir"))
    internal var project = OctoAppProject()

    val minSdk get() = requireMinSdk()
    val compileSdk get() = requireCompileSdk()
    const val basePackage = "de.crysxd.octoapp"
    val jvmVersion get() = project.jvmVersion
    internal val javaVersion = when (jvmVersion) {
        "17" -> JavaVersion.VERSION_17
        "18" -> JavaVersion.VERSION_18
        "19" -> JavaVersion.VERSION_19
        "20" -> JavaVersion.VERSION_20
        "21" -> JavaVersion.VERSION_21
        else -> throw IllegalStateException("Unsupported Java version in defaults: $jvmVersion. Add in Constants.kt!")
    }
    val jvmTarget = when (jvmVersion) {
        "17" -> JvmTarget.JVM_17
        "18" -> JvmTarget.JVM_18
        "19" -> JvmTarget.JVM_19
        "20" -> JvmTarget.JVM_20
        "21" -> JvmTarget.JVM_21
        else -> throw IllegalStateException("Unsupported Java version in defaults: $jvmVersion. Add in Constants.kt!")
    }

    private fun requireMinSdk() = requireNotNull(project.minSdk) {
        "Missing minSdk in project: Add `octoAppProject { minSdk = 26 }` to your root build.gradle!"
    }

    private fun requireCompileSdk() = requireNotNull(project.compileSdk) {
        "Missing minSdk in project: Add `octoAppProject { compileSdk = 34 }` to your root build.gradle!"
    }
}

data class OctoAppProject(
    var minSdk: Int? = null,
    var compileSdk: Int? = null,
    var basePackage: String? = null,
    var lintConfig: String = "config/lint/lint.xml",
    var jvmVersion: String = "17",
    var releaseSigning: ApkSigningConfig.(projectName: String) -> Unit = { _ ->
        storeFile = File(OctoAppBuildConfig.rootDir, "key.keystore")
        storePassword = localProperties.getProperty("signing.storePassword") ?: ""
        keyPassword = localProperties.getProperty("signing.keyPassword") ?: ""
        keyAlias = localProperties.getProperty("signing.storeAlias") ?: ""
    },
)

fun Project.octoAppProject(
    configure: OctoAppProject.() -> Unit,
) = rootProject.run {
    // Reset
    OctoAppBuildConfig.project.minSdk = null
    OctoAppBuildConfig.project.compileSdk = null
    OctoAppBuildConfig.project.basePackage = null

    //Configure
    OctoAppBuildConfig.project.configure()

    // Check required set
    OctoAppBuildConfig.minSdk
    OctoAppBuildConfig.compileSdk
    OctoAppBuildConfig.basePackage

    printSettings(project)
}

val localProperties
    get() = Properties().apply {
        val file = File(OctoAppBuildConfig.rootDir, "local.properties")
        if (file.exists()) {
            load(file.inputStream())
        }
    }