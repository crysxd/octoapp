package de.crysxd.octoapp.buildscript

import org.gradle.api.Project
import java.io.File


fun getVersionCodePhone() = System.getenv("VERSION_CODE")?.toString()?.toInt() ?: 1
fun getVersionCodeWear() = getVersionCodePhone() + 1
fun getVersionName() = System.getenv("VERSION_NAME") ?: "debug"
fun getPlayUpdatePriority() = (System.getenv("UPDATE_PRIORITY") ?: "1").toInt()
fun Project.getPlayTrack() = project.property("play.track").toString()
fun Project.getPlayFraction() = project.property("play.fraction").toString().toDouble()
fun Project.getPlatyCredentials() = File(rootProject.rootDir, "service-account.json")

fun printSettings(project: Project) {
    println("TRACK: ${project.getPlayTrack()}")
    println("FRACTION: ${project.getPlayFraction()}")
    println("VERSION: ${getVersionName()} (${getVersionCodePhone()}/${getVersionCodeWear()})")
    println("UPDATE PRIORITY: ${getPlayUpdatePriority()}")
}