plugins {
    `kotlin-dsl`
    `kotlin-dsl-precompiled-script-plugins`
    alias(libs.plugins.kotlinSerialization)
}

repositories {
    google()
    mavenCentral()
    gradlePluginPortal()
}

buildscript {
    repositories {
        gradlePluginPortal()
    }
}

dependencies {
    implementation(libs.build.agp)
    implementation(libs.build.kotlin.api)

    compileOnly(libs.build.kotlin)
}
