package de.crysxd.octoapp.ui.screens.main.page.menu.power

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.items
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOcto
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn

private const val TAG = "PowerMenu"

fun ScalingLazyListScope.powerMenu(
    powerMenuState: () -> PowerMenuState,
) {
    items(
        items = powerMenuState().devices,
        key = { "powerDevice-${it.uniqueId}" }
    ) {
        PowerDeviceMenuItem(powerDevice = it)
    }
}

@Composable
fun rememberPowerMenuState(instanceId: String) = viewModel(
    modelClass = PowerMenuViewModel::class.java,
    factory = PowerMenuViewModelFactory(instanceId),
    key = "powerMenu/$instanceId",
).devices.collectAsState(initial = PowerMenuState())

data class PowerMenuState(
    val devices: List<PowerDevice> = emptyList(),
)

@Suppress("UNCHECKED_CAST")
private class PowerMenuViewModelFactory(val instanceId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = PowerMenuViewModel(instanceId) as T
}

private class PowerMenuViewModel(val instanceId: String) : ViewModel() {
    val devices = flow {
        val params = GetPowerDevicesUseCase.Params(instanceId = instanceId, allowNetwork = true)
        emit(BaseInjector.get().getPowerDevicesUseCase().execute(params))
    }.map { result ->
        Napier.i(tag = TAG, message = "Got power devices: $result")
        PowerMenuState(result.results.map { it.device })
    }.distinctUntilChanged().retry {
        Napier.e(tag = TAG, message = "Failed", throwable = it)
        delay(1000)
        true
    }.onStart {
        Napier.i(tag = TAG, message = "Loading power devices for $instanceId")
    }.onCompletion {
        Napier.i(tag = TAG, message = "Done loading power devices for $instanceId")
    }.shareIn(viewModelScope, SharingStarted.WhileSubscribedOcto)
}
