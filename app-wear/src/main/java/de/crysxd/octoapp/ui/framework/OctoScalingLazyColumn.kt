package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.focusable
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ChevronRight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.scale
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.input.rotary.onRotaryScrollEvent
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.ScalingLazyColumn
import androidx.wear.compose.material.ScalingLazyListAnchorType
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.ScalingLazyListState
import de.crysxd.octoapp.ui.OctoAppTheme
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch

val ScalingLazyColumnAnchor = ScalingLazyListAnchorType.ItemCenter

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun OctoScalingLazyColumn(
    state: ScalingLazyListState,
    content: ScalingLazyListScope.() -> Unit
) {
    val coroutineScope = rememberCoroutineScope()
    val focusRequester = remember { FocusRequester() }

    ScalingLazyColumn(
        content = content,
        state = state,
        anchorType = ScalingLazyColumnAnchor,
        modifier = Modifier
            .fillMaxSize()
            .testTag("main-column")
            .onRotaryScrollEvent {
                coroutineScope.launch { state.scrollBy(it.verticalScrollPixels) }
                true
            }
            .focusRequester(focusRequester)
            .focusable()
            .onFocusChanged {
                Napier.i(tag = "ScalingLazyColumn", message = "Focus state: $it")
            },
    )

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
        state.scrollToItem(0, 0)
    }
}

fun ScalingLazyListScope.headerItem(
    key: String,
    content: @Composable () -> Unit
) {
    item(key = key) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 6.dp)
                .aspectRatio(1f)
                .padding(horizontal = OctoAppTheme.dimens.margin1),
        ) {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                content()
            }

            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.BottomCenter
            ) {
                Icon(
                    modifier = Modifier
                        .alpha(0.9f)
                        .scale(0.9f)
                        .rotate(90f)
                        .alpha(0.7f),
                    imageVector = Icons.Rounded.ChevronRight,
                    contentDescription = null,
                )
            }
        }
    }

    item {
        Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1))
    }
}