package de.crysxd.octoapp.ui.framework

import androidx.annotation.DrawableRes
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Bluetooth
import androidx.compose.material.icons.rounded.CloudQueue
import androidx.compose.material.icons.rounded.Wifi
import androidx.compose.material.icons.rounded.WifiOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.foundation.CurvedLayout
import androidx.wear.compose.foundation.CurvedModifier
import androidx.wear.compose.foundation.CurvedTextStyle
import androidx.wear.compose.foundation.background
import androidx.wear.compose.foundation.curvedComposable
import androidx.wear.compose.foundation.curvedRow
import androidx.wear.compose.foundation.padding
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.PositionIndicator
import androidx.wear.compose.material.Scaffold
import androidx.wear.compose.material.ScalingLazyListState
import androidx.wear.compose.material.Text
import androidx.wear.compose.material.TimeTextDefaults
import androidx.wear.compose.material.curvedText
import androidx.wear.compose.material.rememberScalingLazyListState
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.utils.isPreview
import de.crysxd.octoapp.utils.isRound
import io.ktor.http.Url
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import java.net.Proxy
import kotlin.math.pow
import kotlin.math.roundToInt

@Composable
fun MainPageScaffold(
    timeText: @Composable () -> Unit = { MainTimeText() },
    content: @Composable MainPageScaffoldContext.() -> Unit,
) {
    val scalingLazyListState = rememberScalingLazyListState()
    val height = remember { mutableStateOf(1) }
    val width = remember { mutableStateOf(1) }

    Scaffold(
        timeText = timeText,
        vignette = {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .clip(if (isRound) CircleShape else RectangleShape)
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .onGloballyPositioned {
                            height.value = it.size.height
                            width.value = it.size.width
                        }
                        .background(
                            Brush.verticalGradient(
                                (if (isRound) 0.1f else 0.1f) to MaterialTheme.colors.background,
                                (if (isRound) 0.3f else 0.15f) to MaterialTheme.colors.background.copy(alpha = 0f),
                                0.95f to MaterialTheme.colors.background.copy(alpha = 0f),
                                1f to MaterialTheme.colors.background,
                            )
                        )
                )
            }
        },
        positionIndicator = {
            // Hack to ALWAYS show the scrollbars...maybe Google happy now
            PositionIndicator(
                state = AlwaysShowScrollBarScalingLazyColumnStateAdapter(
                    state = scalingLazyListState,
                    viewportHeightPx = height,
                ),
                //region Original values from PositionIndicator
                indicatorHeight = 50.dp,
                indicatorWidth = 4.dp,
                paddingHorizontal = 5.dp,
                reverseDirection = false,
                //endregion
            )
        }
    ) {
        MainPageBox {
            content(MainPageScaffoldContext(scalingLazyListState = scalingLazyListState))
        }
    }
}

@Composable
private fun MainTimeText(round: Boolean = isRound) {
    val instance = LocalOctoPrint.current
    val baseLabel = instance?.label ?: ""
    val maxLabelLength = (10 * (1 / LocalDensity.current.fontScale.pow(6f))).roundToInt().coerceAtLeast(1)
    val label = baseLabel.take(maxLabelLength).takeUnless { it.isEmpty() }
    val shortened = baseLabel.length > maxLabelLength
    val color = OctoAppTheme.colors.octoPrintColor
    val style = CurvedTextStyle(OctoAppTheme.typography.base)
    val time = TimeTextDefaults.timeSource(TimeTextDefaults.timeFormat()).currentTime
    val background = colorResource(id = R.color.dark_grey)

    if (round) {
        CurvedLayout {
            curvedRow(
                modifier = CurvedModifier
                    .padding(outer = 5.dp, inner = 0.dp, before = 0.dp, after = 0.dp)
                    .background(background, StrokeCap.Round)
            ) {
                if (instance != null) {
                    // Show connection state
                    curvedComposable {
                        Information(
                            instance.id
                        )
                    }

                    // Show name, but limit to 10 chars
                    if (label != null) {
                        curvedText(
                            text = label + if (shortened) "…" else "",
                            color = color,
                            style = style
                        )
                    }

                    curvedText(
                        text = "·",
                        style = style,
                        modifier = CurvedModifier.padding(before = 4.dp, after = 4.dp, inner = 0.dp, outer = 0.dp)
                    )
                }

                curvedText(
                    text = time,
                    style = style,
                    modifier = CurvedModifier.padding(before = 0.dp, after = 2.dp, inner = 0.dp, outer = 0.dp)
                )
            }
        }
    } else {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 2.dp, horizontal = OctoAppTheme.dimens.margin2)
        ) {
            // Show connection state
            instance?.let {
                Information(it.id)
            }

            Spacer(modifier = Modifier.width(OctoAppTheme.dimens.margin0))

            // Show name
            label?.let {
                Text(
                    text = it,
                    maxLines = 1,
                    color = color,
                    style = OctoAppTheme.typography.base,
                )
            }
        }
    }
}

@Composable
private fun Information(instanceId: String) {
    val vm = viewModel(
        modelClass = InformationViewModel::class.java,
        factory = InformationViewModelFactory(instanceId),
        key = "information/$instanceId",
    )
    val connection by vm.connection.collectAsState(initialConnection)
    Crossfade(targetState = connection) { c ->
        val (type, proxyUsed) = c
        val modifier = Modifier
            .size(16.dp)
            .padding(end = 3.dp)

        @Composable
        fun Icon(@DrawableRes id: Int, rotation: Float = 0f) =
            androidx.wear.compose.material.Icon(
                painter = painterResource(id),
                contentDescription = null,
                modifier = modifier.rotate(rotation),
                tint = Color.Unspecified
            )

        @Composable
        fun Icon(imageVector: ImageVector) =
            androidx.wear.compose.material.Icon(imageVector = imageVector, contentDescription = null, modifier = modifier)

        when (type) {
            ConnectionType.DefaultCloud -> Icon(Icons.Rounded.CloudQueue)
            ConnectionType.Ngrok -> Icon(id = R.drawable.ic_ngrok_24px)
            ConnectionType.OctoEverywhere -> Icon(id = R.drawable.ic_octoeverywhere_24px, 45f)
            ConnectionType.Default -> Icon(if (proxyUsed) Icons.Rounded.Bluetooth else Icons.Rounded.Wifi)
            ConnectionType.Obico -> Icon(id = R.drawable.ic_obico_24px)
            ConnectionType.Tailscale -> Icon(id = R.drawable.ic_tailscale_24px)
            null -> Icon(Icons.Rounded.WifiOff)
        }
    }
}

@Suppress("UNCHECKED_CAST")
private class InformationViewModelFactory(val instanceId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = (if (isPreview) InformationViewModelMock() else InformationViewModelImpl(instanceId)) as T
}

private abstract class InformationViewModel : ViewModel() {
    abstract val connection: Flow<Pair<ConnectionType?, Boolean>>
}

private class InformationViewModelMock : InformationViewModel() {
    override val connection = flowOf(ConnectionType.Ngrok to false)
}

private class InformationViewModelImpl(instanceId: String) : InformationViewModel() {
    override val connection = BaseInjector.get().octoPrintProvider()
        .passiveConnectionEventFlow(tag = "information-vm", instanceId = instanceId)
        .map { it?.connectionType }
        .map {
            val instance = BaseInjector.get().octorPrintRepository().get(instanceId)
            if (it == ConnectionType.Default && instance != null) {
                try {
                    it to (BaseInjector.get().octoPrintProvider().httpSettings.proxySelector?.selectProxy(Url(instance.webUrl.toString()))?.first() != Proxy.NO_PROXY)
                } catch (e: Exception) {
                    it to false
                }
            } else {
                it to false
            }
        }.flowOn(Dispatchers.IO)
}

data class MainPageScaffoldContext(
    val scalingLazyListState: ScalingLazyListState,
)

@Composable
private fun MainPageBox(content: @Composable BoxScope.() -> Unit) {
    val shape = if (isRound) CircleShape else RectangleShape
    Box(
        modifier = Modifier
            .fillMaxSize()
            .shadow(elevation = 10.dp, shape = shape)
            .clip(shape)
            .background(MaterialTheme.colors.background),
        contentAlignment = Alignment.Center,
        content = content,
    )
}

private var initialConnection: Pair<ConnectionType?, Boolean> = null to false
private val testEnv = PrinterConfigurationV3(id = "res", apiKey = "", webUrl = Url("http://test.com"))

@Composable
@Preview(name = "TimeText - Square", showBackground = true, backgroundColor = 0xFF000000, device = Devices.WEAR_OS_SQUARE)
fun TimeTextPreviewSquare() = OctoAppTheme.WithTheme {
    CompositionLocalProvider(LocalOctoPrint provides testEnv) {
        initialConnection = ConnectionType.Tailscale to false
        MainTimeText(round = false)
    }
}

@Composable
@Preview(name = "TimeText - Bluetooth", showBackground = true, backgroundColor = 0xFF000000, device = Devices.WEAR_OS_SQUARE)
fun TimeTextPreviewBluetooth() = OctoAppTheme.WithTheme {
    CompositionLocalProvider(LocalOctoPrint provides testEnv) {
        initialConnection = ConnectionType.Default to true
        MainTimeText(round = false)
    }
}

@Composable
@Preview(name = "TimeText - Round", showBackground = true, backgroundColor = 0xFF000000, device = Devices.WEAR_OS_SMALL_ROUND)
fun TimeTextPreviewRound() = OctoAppTheme.WithTheme {
    CompositionLocalProvider(LocalOctoPrint provides testEnv) {
        initialConnection = ConnectionType.OctoEverywhere to false
        MainTimeText(round = true)
    }
}