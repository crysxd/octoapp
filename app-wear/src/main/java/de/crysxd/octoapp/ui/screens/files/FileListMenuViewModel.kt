package de.crysxd.octoapp.ui.screens.files

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.sorted
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.base.utils.awaitItem
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.ext.wearEventFlow
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch

class FileListViewModel(val instanceId: String, val folder: FileObject.Folder?) : ViewModel() {

    private val tag = "FileListViewModel"
    private val mutableState = MutableStateFlow<State>(State.Loading)
    val state = mutableState.asStateFlow()
    private val mutablePrinting = MutableStateFlow(false)
    val printing = mutablePrinting.asStateFlow()

    init {
        loadFiles()
        viewModelScope.launch {
            BaseInjector.get().octoPrintProvider().wearEventFlow("file-vm", instanceId)
                .onEach { event ->
                    if ((event is Event.MessageReceived) && event.message is Message.Event.UpdatedFiles) {
                        Napier.i(tag = tag, message = "Files changed, reloading")
                        loadFiles()
                    }
                    ((event as? Event.MessageReceived)?.message as? Message.Current)?.let {
                        mutablePrinting.value = it.state.flags.isPrinting()
                    }
                }.retry {
                    Napier.e(tag = tag, message = "Failed", throwable = it)
                    delay(1000)
                    true
                }.collect()
        }

        viewModelScope.launch {
            var first = true
            BaseInjector.get().octoPreferences().updatedFlow.onEach {
                if (!first) {
                    loadFiles()
                } else {
                    first = false
                }
            }.retry {
                Napier.e(tag = tag, message = "Failed", throwable = it)
                delay(1000)
                true
            }.collect()
        }
    }

    fun loadFiles() {
        viewModelScope.launch {
            try {
                Napier.i(tag = tag, message = "Loading files for ${folder?.path ?: "root"}")
                mutableState.emit(State.Loading)
                val file = BaseInjector.get().loadFilesUseCase().execute(
                    LoadFilesUseCase.Params(
                        instanceId = instanceId,
                        path = folder?.path ?: "/",
                        fileOrigin = FileOrigin.Gcode,
                    )
                ).onEach {
                    when (it) {
                        is FlowState.Error -> throw it.throwable
                        is FlowState.Ready -> mutableState.emit(State.Loading)
                        is FlowState.Loading -> Unit
                    }
                }.awaitItem()
                val files = requireNotNull((file as? FileObject.Folder)?.children) { "Missing children" }
                mutableState.emit(State.Files(files.sorted()))
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed", throwable = e)
                mutableState.emit(State.Error(e))
            }
        }
    }

    sealed class State {
        data object Loading : State()
        data class Error(val exception: Exception) : State()
        data class Files(val files: List<FileObject>) : State()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(val instanceId: String, private val folder: FileObject.Folder?) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T = FileListViewModel(instanceId, folder) as T
    }
}
