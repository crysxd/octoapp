package de.crysxd.octoapp.ui.screens.main.page.menu.gcode

import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Code
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.res.stringResource
import androidx.core.os.bundleOf
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.items
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.Confirmation
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.rememberConfirmationState
import kotlinx.parcelize.Parcelize

class SendGcodeMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        val history = rememberGcodeHistory()
                        OctoScalingLazyColumn(state = scalingLazyListState) {
                            content(history = history)
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun rememberGcodeHistory(): List<GcodeHistoryItem> {
        val repo = remember { BaseInjector.get().gcodeHistoryRepository() }
        return repo.history.collectAsState().value
    }

    private fun ScalingLazyListScope.content(history: List<GcodeHistoryItem>) {
        item {
            MenuHeader(text = stringResource(id = R.string.wear_menu___send_gcode))
        }

        items(history) { item ->
            val confirmationState by rememberConfirmationState()
            Confirmation(
                state = confirmationState,
                text = stringResource(id = R.string.widget_gcode_send___confirmation_message, item.name),
                action = {
                    BaseInjector.get().executeGcodeCommandUseCase().execute(
                        ExecuteGcodeCommandUseCase.Param(
                            command = GcodeCommand.Batch(item.command.split("\n")),
                            fromUser = true,
                            instanceId = instanceId,
                        )
                    )
                    showSuccess()
                }
            )

            MenuItemChip(
                icon = Icons.Rounded.Code,
                text = item.name,
                onClick = { confirmationState.visible = true }
            )
        }
    }

    @Parcelize
    data class Args(
        val instanceId: String,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelableCompat<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}