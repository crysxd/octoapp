package de.crysxd.octoapp.ui.screens.main.page.menu.octoprint

import android.content.Intent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Widgets
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.LocalOctoPrint
import de.crysxd.octoapp.ui.framework.MenuItemChip

@Composable
fun ShowSystemCommandsMenuItem() = LocalOctoPrint.current?.id?.let { instanceId ->
    val activity = LocalContext.current.requireActivity()
    OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.OctoPrint) {
        MenuItemChip(
            icon = Icons.Rounded.Widgets,
            text = stringResource(R.string.wear_menu___system_commands),
            showChevron = true,
            onClick = {
                Intent(activity, SystemCommandsMenuActivity::class.java).also {
                    it.putExtras(SystemCommandsMenuActivity.Args(instanceId).toBundle())
                    activity.startActivity(it)
                }
            }
        )
    }
}