package de.crysxd.octoapp.ui.screens.main

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsEta
import de.crysxd.octoapp.ui.OctoAppTheme
import io.github.aakira.napier.Napier

private const val TAG = "PrintHeader"


@Composable
fun PrintHeaderContent(currentMessageSource: () -> Message.Current?) {
    val current = currentMessageSource()
    val flags = current?.state?.flags
    val percent = current?.progress?.completion?.formatAsPercent()
    Napier.i(tag = TAG, message = "current: $current")

    if (percent != null) {
        Text(
            text = percent,
            style = OctoAppTheme.typography.titleLarge,
        )
    }

    val text = when {
        flags?.paused == true -> stringResource(id = R.string.paused)
        flags?.pausing == true -> stringResource(id = R.string.pausing)
        flags?.cancelling == true -> stringResource(id = R.string.cancelling)
        percent == null -> stringResource(id = R.string.loading)
        else -> current.progress?.printTimeLeft?.takeIf { it > 0 }?.formatAsSecondsEta(
            allowRelative = true, useCompactFutureDate = false
        ) ?: "--"
    }

    Text(
        text = text,
        style = OctoAppTheme.typography.label,
    )
}