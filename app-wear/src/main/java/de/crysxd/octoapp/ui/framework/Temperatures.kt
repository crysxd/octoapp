package de.crysxd.octoapp.ui.framework

import android.content.Context
import android.graphics.Bitmap
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Shader
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ChevronRight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.graphics.applyCanvas
import androidx.core.graphics.get
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.Text
import androidx.wear.compose.material.items
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.BaseChangeTemperaturesUseCase
import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.utils.Constants
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry

@Composable
fun Temperature(
    component: String,
    label: String,
    temp: ComponentTemperature,
    offset: Float?,
    canControl: Boolean
) {
    val colors = backgroundColor(component = component, temp = temp.actual)
    val color1 by animateColorAsState(targetValue = colors.first)
    val color2 by animateColorAsState(targetValue = colors.second)
    val context = LocalContext.current
    val instanceId = LocalOctoPrint.current?.id ?: return

    var showInput by remember { mutableStateOf(false) }
    ValueInputDialog(visible = showInput, suffix = "°C") {
        setTemperature(context, it, component = component, instanceId = instanceId)
        showInput = false
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.small)
            .clickable(role = Role.Button, enabled = canControl) {
                showInput = true
            }
            .background(Brush.horizontalGradient(listOf(color1, color2)))
            .padding(
                start = OctoAppTheme.dimens.margin1to2,
                top = OctoAppTheme.dimens.margin1to2,
                bottom = OctoAppTheme.dimens.margin1to2,
                end = OctoAppTheme.dimens.margin1to2,
            )
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                val unit = "\u2009°C"
                val isOff = temp.target == 0f
                Text(
                    // Build text
                    // 50 / 200 °C
                    // 50 °C / Off
                    text = listOfNotNull(
                        listOfNotNull(temp.actual?.toInt()?.toString(), unit.takeIf { isOff || temp.target == null }).joinToString(""),
                        if (isOff) stringResource(id = R.string.target_off) else temp.target?.toInt()?.toString()?.plus(unit)
                    ).joinToString("\u2009/\u2009"),
                    style = OctoAppTheme.typography.button,
                )
            }


            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = listOfNotNull(offset?.toInt()?.toString(), toStyledName(component, label)).joinToString("/"),
                    style = OctoAppTheme.typography.labelSmall,
                )
                Icon(
                    imageVector = Icons.Rounded.ChevronRight,
                    contentDescription = null,
                    tint = if (canControl) Color.White else Color.Transparent
                )
            }
        }
    }
}

private suspend fun setTemperature(context: Context, temp: Float?, component: String, instanceId: String) {
    val t = temp ?: return
    BaseInjector.get().setTargetTemperatureUseCase().execute(
        BaseChangeTemperaturesUseCase.Params(
            instanceId = instanceId,
            temp = BaseChangeTemperaturesUseCase.Temperature(
                temperature = t,
                component = component
            )
        )
    )
    context.showSuccess()
}

@Composable
private fun toStyledName(component: String, label: String) = when (component) {
    "tool0" -> "H0"
    "tool1" -> "H1"
    "tool2" -> "H2"
    "tool3" -> "H3"
    "tool4" -> "H3"
    "tool5" -> "H3"
    "bed" -> "B"
    "chamber" -> "C"
    else -> label.split(" ").joinToString("") { it.first().uppercase() }
}

@Composable
private fun backgroundColor(component: String, temp: Float?): Pair<Color, Color> {
    if (!::temperatureGradient.isInitialized) {
        temperatureGradient = createTemperatureGradient()
    }

    val maxTemp = when (component) {
        "tool0" -> 250
        "tool1" -> 250
        "tool3" -> 250
        "tool4" -> 250
        "bed" -> 80
        "chamber" -> 50
        else -> 100
    }

    val tempRange = 35..(maxTemp.coerceAtLeast(36))
    val cappedTemp = temp?.toInt()?.coerceIn(tempRange) ?: tempRange.first
    val tempPercent = ((cappedTemp - tempRange.first) / (tempRange.last - tempRange.first).toFloat())
    val y = (temperatureGradient.height - (tempPercent * temperatureGradient.height)).coerceAtMost(temperatureGradient.height - 1f)
    val color = Color(temperatureGradient[0, y.toInt()])
    return color.copy(alpha = 0.3f) to color.copy(alpha = 0.1f)
}

private lateinit var temperatureGradient: Bitmap

@Composable
private fun createTemperatureGradient() = Bitmap.createBitmap(
    1,
    512,
    Bitmap.Config.ARGB_8888
).applyCanvas {
    val colors = arrayOf(
        colorResource(id = R.color.color_hot).toArgb(),
        colorResource(id = R.color.color_cold).toArgb(),
    ).toIntArray()
    val positions = arrayOf(
        0.3f,
        1f
    ).toFloatArray()
    val paint = Paint().also {
        it.style = Paint.Style.FILL
        it.shader = LinearGradient(0f, 0f, 0f, height.toFloat(), colors, positions, Shader.TileMode.CLAMP)
    }
    drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
}


fun ScalingLazyListScope.temperatures(state: () -> TemperaturesState) {
    items(
        items = state().temperatures,
        key = { "temp-${it.component}" }
    ) {
        Temperature(
            component = it.component,
            temp = it.current,
            offset = it.offset,
            canControl = it.canControl,
            label = it.componentLabel,
        )
    }
}

@Composable
fun rememberTemperaturesState(instanceId: String) = viewModel(
    modelClass = TemperatureViewModel::class.java,
    factory = TemperatureViewModelFactory(instanceId),
    key = "temperature/$instanceId"
).temperatures.collectAsState(TemperaturesState(instanceId = instanceId, temperatures = emptyList()))

data class TemperaturesState(
    val instanceId: String,
    val temperatures: List<TemperatureDataRepository.TemperatureSnapshot> = emptyList(),
)


@Suppress("UNCHECKED_CAST")
private class TemperatureViewModelFactory(val instanceId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = TemperatureViewModel(instanceId) as T
}

private class TemperatureViewModel(instanceId: String) : ViewModel() {
    val temperatures = BaseInjector.get().temperatureDataRepository().flow(instanceId)
        .map { it.filter { t -> !t.isHidden && t.current.actual != null } }
        .map { TemperaturesState(instanceId, it) }
        .retry {
            Napier.e(tag = "TemperatureVieWModel", message = "Failed", throwable = it)
            delay(1000)
            true
        }
}

@Composable
@Preview(
    name = "0/200",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000
)
fun Preview() {
    OctoAppTheme().WithTheme {
        Temperature(
            component = "tool0",
            temp = ComponentTemperature(0f, 200f),
            offset = null,
            canControl = true,
            label = "XXX",
        )
    }
}

@Composable
@Preview(
    name = "50/200",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000
)
fun Preview0() = OctoAppTheme().WithTheme {
    Temperature(
        component = "tool0",
        temp = ComponentTemperature(50f, 200f),
        offset = null,
        canControl = true,
        label = "XXX",
    )
}

@Composable
@Preview(
    name = "100/200",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true, backgroundColor = 0xFF000000
)
fun Preview1() = OctoAppTheme().WithTheme {
    Temperature(
        component = "tool0",
        temp = ComponentTemperature(100f, 200f),
        offset = null,
        canControl = true,
        label = "XXX",
    )
}

@Composable
@Preview(
    name = "200/200",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000,
)
fun Preview2() = OctoAppTheme().WithTheme {
    Temperature(
        component = "tool0",
        temp = ComponentTemperature(200f, 200f),
        offset = null,
        canControl = true,
        label = "XXX",
    )
}

@Composable
@Preview(
    name = "Cooldown",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000
)
fun Preview3() = OctoAppTheme().WithTheme {
    Temperature(
        component = "tool0",
        temp = ComponentTemperature(200f, 0f),
        offset = null,
        canControl = true,
        label = "XXX",
    )
}

@Composable
@Preview(
    name = "Offset",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000
)
fun Preview4() = OctoAppTheme().WithTheme {
    Temperature(
        component = "tool0",
        temp = ComponentTemperature(200f, 210f),
        offset = -10f,
        canControl = true,
        label = "XXX",
    )
}

@Composable
@Preview(
    name = "Offset",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000,
)
fun Preview5() = OctoAppTheme().WithTheme {
    Temperature(
        component = "custom",
        temp = ComponentTemperature(200f, 210f),
        offset = null,
        canControl = false,
        label = "XXX",
    )
}