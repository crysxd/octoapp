package de.crysxd.octoapp.ui.screens.main.page.menu.temperature

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocalFireDepartment
import androidx.compose.material.icons.rounded.MoreVert
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.core.os.bundleOf
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.items
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.SplitChip
import kotlinx.parcelize.Parcelize

class TemperaturePresetMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId
    private val subMenuLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        OctoScalingLazyColumn(state = scalingLazyListState) {
                            instance?.let { content(it) }
                        }
                    }
                }
            }
        }
    }

    private fun ScalingLazyListScope.content(instance: PrinterConfigurationV3) {
        item {
            MenuHeader(text = stringResource(id = R.string.main_menu___item_temperature_presets))
        }

        items(instance.settings?.temperaturePresets ?: emptyList()) { profile ->
            val context = LocalContext.current
            SplitChip(
                text = profile.name,
                actionIcon = Icons.Rounded.MoreVert,
                icon = Icons.Rounded.LocalFireDepartment,
                textAction = {
                    BaseInjector.get().applyTemperaturePresetUseCase().execute(
                        ApplyTemperaturePresetUseCase.Params(
                            instanceId = instance.id,
                            profileName = profile.name,
                            executeGcode = true,
                        )
                    )

                    showSuccess()
                    finish()
                },
                iconAction = {
                    Intent(context, TemperaturePresetSubMenuActivity::class.java).also {intent ->
                        intent.putExtras(
                            TemperaturePresetSubMenuActivity.Args(
                                profileName = profile.name,
                                instanceId = instance.id,
                                hasBed = profile.components.any { it.isBed } && instance.activeProfile?.heatedBed != false,
                                hasChamber = profile.components.any { it.isChamber }  && instance.activeProfile?.heatedChamber != false,
                                hasExtruder = profile.components.any { it.isExtruder },
                                hasGcode = profile.gcode != null,
                            ).toBundle()
                        )
                        subMenuLauncher.launch(intent)
                    }
                },
            )
        }
    }

    @Parcelize
    data class Args(
        val instanceId: String,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelableCompat<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}