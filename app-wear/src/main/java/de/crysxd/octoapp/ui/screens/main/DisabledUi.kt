package de.crysxd.octoapp.ui.screens.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.PositionIndicator
import androidx.wear.compose.material.Text
import androidx.wear.compose.material.rememberScalingLazyListState
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.ext.openOnPhone
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.utils.Constants.PREVIEW_SIZE_DP
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.seconds

@Composable
fun WearAppDisabledUi(
    billingAvailable: () -> Boolean,
) = OctoAppTheme.WithTheme {
    val lazyListState = rememberScalingLazyListState()

    OctoScalingLazyColumn(state = lazyListState) {
        item {
            Image(
                painter = painterResource(id = R.drawable.octo_base),
                contentDescription = null,
                contentScale = ContentScale.FillWidth,
                modifier = Modifier.size(72.dp)
            )
        }

        item {
            Text(
                text = stringResource(
                    id = if (billingAvailable()) {
                        R.string.supporter_perk___title
                    } else {
                        R.string.billing_unsupported_title
                    }
                ),
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = OctoAppTheme.dimens.margin1),
                style = OctoAppTheme.typography.title,
            )
        }

        item {
            Text(
                text = if (billingAvailable()) {
                    stringResource(
                        R.string.supporter_perk___description,
                        stringResource(id = R.string.wear_app___supporter_perk_title)
                    )
                } else {
                    stringResource(R.string.billing_unsupported_detail)
                },
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
                style = OctoAppTheme.typography.base
            )
        }

        item {
            Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin2))
        }

        if (billingAvailable()) {
            item {
                val activity = LocalContext.current.requireActivity()
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Support) {
                    MenuItemChip(
                        text = stringResource(id = R.string.wear_uri___open_on_phone),
                        icon = Icons.Rounded.Favorite,
                    ) {
                        UriLibrary.getPurchaseUri().openOnPhone(activity)
                    }
                }
            }
        }
    }

    LaunchedEffect(key1 = true) {
        delay(0.5.seconds)
        lazyListState.animateScrollToItem(0, 1)
        delay(2.seconds)
        lazyListState.animateScrollToItem(0, 0)
    }

    PositionIndicator(scalingLazyListState = lazyListState)
}

@Composable
@Preview(name = "With Billing", widthDp = PREVIEW_SIZE_DP, heightDp = PREVIEW_SIZE_DP)
fun WithBillingPreview() {
    WearAppDisabledUi { true }
}

@Composable
@Preview(name = "Without Billing", widthDp = PREVIEW_SIZE_DP, heightDp = PREVIEW_SIZE_DP)
fun WithoutBillingPreview() {
    WearAppDisabledUi { false }
}
