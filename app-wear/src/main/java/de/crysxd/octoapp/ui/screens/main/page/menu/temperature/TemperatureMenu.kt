package de.crysxd.octoapp.ui.screens.main.page.menu.temperature

import android.content.Intent
import android.os.Parcelable
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocalFireDepartment
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.wear.compose.material.ScalingLazyListScope
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MenuItemChip
import kotlinx.coroutines.flow.collectLatest
import kotlinx.parcelize.Parcelize

fun ScalingLazyListScope.temperatureMenu(state: () -> TemperatureMenuState) {
    if (state().hasPresets) {
        item("temperaturePresets") {
            val activity = LocalContext.current.requireActivity()

            OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.wear_menu___temperature_presets),
                    showChevron = true,
                    onClick = {
                        Intent(activity, TemperaturePresetMenuActivity::class.java).also {
                            it.putExtras(TemperaturePresetMenuActivity.Args(state().instanceId).toBundle())
                            activity.startActivity(it)
                        }
                    }
                )
            }
        }
    }
}

@Composable
fun rememberTemperatureMenuState(instanceId: String): MutableState<TemperatureMenuState> {
    val state = rememberSaveable { mutableStateOf(TemperatureMenuState(hasPresets = false, instanceId = instanceId)) }

    LaunchedEffect(Unit) {
        BaseInjector.get().octorPrintRepository().instanceInformationFlow(instanceId).collectLatest {
            state.value = state.value.copy(hasPresets = !it?.settings?.temperaturePresets.isNullOrEmpty())
        }
    }

    return state
}

@Parcelize
data class TemperatureMenuState(
    val instanceId: String,
    val hasPresets: Boolean,
) : Parcelable