package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ChevronRight
import androidx.compose.material.icons.rounded.LocalFireDepartment
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.Chip
import androidx.wear.compose.material.ChipDefaults
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.ext.showFailure
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.utils.Constants
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch

@Composable
fun MenuItemChip(
    icon: ImageVector,
    text: String,
    modifier: Modifier = Modifier,
    iconModifier: Modifier = Modifier,
    textModifier: Modifier = Modifier,
    enabled: Boolean = true,
    showChevron: Boolean = false,
    onClick: suspend () -> Unit,
) = MenuItemChip(
    icon = icon,
    text = buildAnnotatedString { append(text) },
    onClick = onClick,
    enabled = enabled,
    showChevron = showChevron,
    textModifier = textModifier,
    modifier = modifier,
    iconModifier = iconModifier
)

@Composable
fun MenuItemChip(
    icon: ImageVector,
    text: AnnotatedString,
    modifier: Modifier = Modifier,
    iconModifier: Modifier = Modifier,
    textModifier: Modifier = Modifier,
    enabled: Boolean = true,
    showChevron: Boolean = false,
    onClick: suspend () -> Unit,
) {
    var buttonEnabled by remember { mutableStateOf(true) }
    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    val haptic = LocalHapticFeedback.current

    Chip(
        modifier = modifier.fillMaxWidth(),
        enabled = buttonEnabled && enabled,
        onClick = {
            scope.launch {
                buttonEnabled = false
                val start = System.currentTimeMillis()
                try {
                    haptic.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                    onClick()
                } catch (e: Exception) {
                    Napier.e(tag = "Chip", message = "Failed to execute action", throwable = e)
                    context.showFailure(e)
                } finally {
                    if (System.currentTimeMillis() - start > 150) {
                        haptic.performHapticFeedback(HapticFeedbackType.LongPress)
                    }
                }
                buttonEnabled = true
            }
        },
        label = {
            Box(
                modifier = modifier.fillMaxWidth(),
                contentAlignment = Alignment.CenterStart
            ) {
                Text(
                    text = text,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    style = OctoAppTheme.typography.button,
                    modifier = textModifier.padding(end = 24.dp)
                )
                if (showChevron) {
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.CenterEnd
                    ) {
                        Icon(
                            imageVector = Icons.Rounded.ChevronRight,
                            contentDescription = null,
                            modifier = Modifier.requiredSize(24.dp, 24.dp)
                        )
                    }
                }
            }

        },
        icon = {
            Icon(
                imageVector = icon,
                contentDescription = null,
                modifier = iconModifier,
            )
        },
        colors = ChipDefaults.chipColors(
            backgroundColor = OctoAppTheme.colors.menuBackground,
            iconColor = OctoAppTheme.colors.menuForeground,
            contentColor = MaterialTheme.colors.onPrimary
        ),
    )
}

@Composable
@Preview(
    name = "Normal",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    heightDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000
)
fun MenuChipPreview0() {
    OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
        MenuItemChip(
            text = "Menu",
            showChevron = true,
            icon = Icons.Rounded.LocalFireDepartment,
        ) {}
    }
}

@Composable
@Preview(
    name = "LongText",
    widthDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    heightDp = (Constants.PREVIEW_SIZE_DP * 0.8).toInt(),
    showBackground = true,
    backgroundColor = 0xFF000000
)
fun MenuChipPreview1() {
    OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
        MenuItemChip(
            text = "Menu with long text",
            showChevron = true,
            icon = Icons.Rounded.LocalFireDepartment,
        ) {}
    }
}