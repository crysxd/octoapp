package de.crysxd.octoapp.ui.screens.main.page.menu.printcontrols

import android.content.Context
import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Pause
import androidx.compose.material.icons.rounded.PlayArrow
import androidx.compose.material.icons.rounded.Stop
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.ScalingLazyListScope
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.Confirmation
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.rememberConfirmationState
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn

fun ScalingLazyListScope.printControlsMenu(state: () -> PrintMenuState) {
    val s = state()
    if (!s.printActive) return

    //region Resume / Pause
    if (s.paused) {
        option(
            text = R.string.resume,
            confirmation = R.string.resume_print_confirmation_message,
            icon = Icons.Rounded.PlayArrow,
            enabled = true,
            key = "controlResume",
            action = {
                BaseInjector.get().togglePausePrintJobUseCase().execute(TogglePausePrintJobUseCase.Params(s.instanceId))
                showSuccess()
            }
        )
    } else {
        option(
            text = R.string.pause,
            confirmation = R.string.pause_print_confirmation_message,
            icon = Icons.Rounded.Pause,
            enabled = !s.pausing,
            key = "controlPause",
            action = {
                BaseInjector.get().togglePausePrintJobUseCase().execute(TogglePausePrintJobUseCase.Params(s.instanceId))
                showSuccess()
            }
        )
    }
    //endregion
    //region Cancel
    option(
        confirmation = R.string.cancel_print_confirmation_message,
        icon = Icons.Rounded.Stop,
        text = R.string.cancel,
        enabled = !s.cancelling,
        key = "controlCancel",
        action = {
            BaseInjector.get().cancelPrintJobUseCase().execute(CancelPrintJobUseCase.Params(instanceId = s.instanceId, restoreTemperatures = false))
            showSuccess()
        }
    )
    //endregion
}

private fun ScalingLazyListScope.option(
    @StringRes text: Int,
    @StringRes confirmation: Int,
    icon: ImageVector,
    enabled: Boolean,
    key: String,
    action: suspend Context.() -> Unit
) {
    item(key = key) {
        val context = LocalContext.current
        val confirmationState by rememberConfirmationState()
        Confirmation(
            state = confirmationState,
            text = stringResource(id = confirmation),
            action = { action(context) },
        )

        OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
            MenuItemChip(
                icon = icon,
                text = stringResource(id = text),
                enabled = enabled,
                onClick = { confirmationState.visible = true }
            )
        }
    }
}

@Composable
fun rememberPrintControlsMenuState(instanceId: String) = viewModel(
    modelClass = PrintControlsMenuViewModel::class.java,
    factory = PrintControlsMenuViewModelFactory(instanceId),
    key = "printControls/$instanceId",
).printState.collectAsState(PrintMenuState(instanceId))

@Suppress("UNCHECKED_CAST")
private class PrintControlsMenuViewModelFactory(val instanceId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = PrintControlsMenuViewModel(instanceId) as T
}

data class PrintMenuState(
    val instanceId: String,
    val printActive: Boolean = false,
    val cancelling: Boolean = false,
    val pausing: Boolean = false,
    val paused: Boolean = false,
)

private class PrintControlsMenuViewModel(val instanceId: String) : ViewModel() {

    val printState = BaseInjector.get().octoPrintProvider()
        .passiveCurrentMessageFlow(tag = "print-controls-menu", instanceId = instanceId)
        .map { cur ->
            PrintMenuState(
                instanceId = instanceId,
                printActive = cur.state.flags.isPrinting(),
                cancelling = cur.state.flags.cancelling,
                pausing = cur.state.flags.pausing,
                paused = cur.state.flags.paused,
            )
        }.retry {
            Napier.e(tag = "PrintControlsMenu", message = "Failed", throwable = it)
            delay(1000)
            true
        }.shareIn(viewModelScope, SharingStarted.WhileSubscribedOctoDelay)
}
