package de.crysxd.octoapp.ui.screens.main

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import androidx.core.content.edit
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.HorizontalPageIndicator
import androidx.wear.compose.material.PageIndicatorState
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.screens.main.page.MainUiPage
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

@Composable
@OptIn(ExperimentalPagerApi::class)
fun MainUi(ambientActive: Boolean) {
    val vm = viewModel(modelClass = MainUiViewModel::class.java, factory = MainUiViewModelFactory())
    val context = LocalContext.current
    val activeInstanceIdKey = "active2"
    val preferences = remember { context.getSharedPreferences("main_fragment", Context.MODE_PRIVATE) }
    val initialActive = remember { preferences.getString(activeInstanceIdKey, null) }

    val i by vm.instances.collectAsState(initial = null)
    if (i == null) return
    val instances = i!!
    val pagerState = rememberPagerState(
        initialActive?.let { instance ->
            instances.indexOf(instance).takeIf { it >= 0 } ?: instances.size
        } ?: 0
    )

    OctoAppTheme.WithTheme {
        HorizontalPager(
            count = instances.size + 1,
            key = { position -> instances.getOrNull(position) ?: "info" },
            state = pagerState,
            itemSpacing = OctoAppTheme.dimens.margin1to2,
            modifier = Modifier
                .fillMaxSize()
                .testTag("main-pager")
                .background(Color(0xFF333333))
        ) { position ->
            val active by remember { derivedStateOf { pagerState.currentPage == position } }
            val instance = instances.getOrNull(position)
            Napier.i(tag = "MainUi", message = "Compose position: $position/$currentPage -> $instance -> $active")
            if (instance != null) {
                MainUiPage(instanceId = instance, active = active && !ambientActive)
            } else {
                InfoUi(octoPrintCount = instances.size)
            }
        }

        LaunchedEffect(key1 = pagerState.currentPage) {
            preferences.edit { putString(activeInstanceIdKey, instances.getOrNull(pagerState.currentPage)) }
        }

        if (pagerState.pageCount > 1) {
            HorizontalPageIndicator(
                pageIndicatorState = PageIndicatorStateImpl(
                    pageCount = pagerState.pageCount,
                    pageOffset = pagerState.currentPageOffset,
                    selectedPage = pagerState.currentPage
                ),
                modifier = Modifier
                    .fillMaxSize()
                    .padding(2.dp)
            )
        }
    }
}


private data class PageIndicatorStateImpl(
    override val pageCount: Int = 0,
    override val pageOffset: Float = 0f,
    override val selectedPage: Int = 0,
) : PageIndicatorState

@Suppress("UNCHECKED_CAST")
private class MainUiViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = MainUiViewModel() as T
}

private class MainUiViewModel : ViewModel() {
    val instances = BaseInjector.get().octorPrintRepository().allInstanceInformationFlow()
        .map { it.values.map { i -> i.id }.toList() }
        .distinctUntilChanged()
}
