package de.crysxd.octoapp.ui.screens.main.connect

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.LocalOctoPrint
import de.crysxd.octoapp.ui.framework.PrimaryButtonChip
import de.crysxd.octoapp.ui.framework.SecondaryButtonChip
import de.crysxd.octoapp.ui.framework.TitleDetails
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn

private const val TAG = "ConnectHeader"

@Composable
fun ConnectHeaderContent(active: Boolean) {
    val instanceInformation = LocalOctoPrint.current ?: return Napier.e(tag = TAG, message = "No OctoPrint!")
    val vm = viewModel<ConnectPrinterViewModel>(factory = ConnectPrinterViewModelFactory(instanceInformation.id))
    val uiState = if (active) {
        vm.uiState.collectAsState(initial = ConnectPrinterUseCase.UiState.Initializing).value
    } else {
        ConnectPrinterUseCase.UiState.Initializing
    }

    Crossfade(targetState = uiState) { target ->
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            TitleDetails(
                title = target.title,
                detail = target.detail,
            )

            target.action?.takeUnless {
                target.actionType in listOf(
                    ConnectPrinterUseCase.ActionType.ConfigurePsu,
                    ConnectPrinterUseCase.ActionType.AutoConnectTutorial,
                )
            }?.let { action ->
                Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1))
                if (target.primaryAction) {
                    PrimaryButtonChip(
                        text = action,
                        onClick = { target.actionType?.let { vm.executeAction(it) } }
                    )
                } else {
                    SecondaryButtonChip(
                        text = action,
                        onClick = { target.actionType?.let { vm.executeAction(it) } }
                    )
                }
            }
        }
    }
}


@Suppress("UNCHECKED_CAST")
private class ConnectPrinterViewModelFactory(private val instanceId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>) = ConnectPrinterViewModel(instanceId) as T
}

private class ConnectPrinterViewModel(private val instanceId: String) : ViewModel() {

    companion object {
        private var counter = 0
    }

    private val myCounter = counter++
    private val useCase by lazy { SharedBaseInjector.get().connectPrinterUseCase() }
    val uiState: Flow<ConnectPrinterUseCase.UiState> = useCase.executeBlocking(ConnectPrinterUseCase.Params(instanceId = instanceId))
        .onCompletion { Napier.i(tag = TAG, message = "[$myCounter] Stop connection attempts on $instanceId") }
        .onStart { Napier.i(tag = TAG, message = "[$myCounter] Start connecting on $instanceId") }
        .shareIn(viewModelScope, started = SharingStarted.WhileSubscribedOctoDelay)
        .rateLimit(200)

    suspend fun executeAction(action: ConnectPrinterUseCase.ActionType) {
        useCase.executeAction(action)
    }
}

/*
@Composable
fun PreviewContainer(content: @Composable () -> Unit) {
    OctoAppTheme.WithTheme {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(OctoAppTheme.dimens.margin1)
                .fillMaxSize()
        ) {
            content()
        }
    }
}

@Composable
@Preview(
    name = "Initial",
    widthDp = Constants.PREVIEW_SIZE_DP,
    heightDp = Constants.PREVIEW_SIZE_DP,
    showBackground = true,
    backgroundColor = 0xFF000000
)
fun PreviewInitial() {
    PreviewContainer {
        ConnectHeaderContent(
            vm = ConnectPrinterViewModel(""),
            uiState = ConnectPrinterUseCase.UiState.Initializing
        )
    }
}

    @Composable
    @Preview(
        name = "Waiting for User",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForUser() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForUser
            )
        }
    }

    @Composable
    @Preview(
        name = "OctoPrint not available",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewOctoPrintNotAvailable() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.OctoPrintNotAvailable
            )
        }
    }

    @Composable
    @Preview(
        name = "OctoPrint Starting",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewOctoPrintStarting() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.OctoPrintStarting
            )
        }
    }

    @Composable
    @Preview(
        name = "Printer Connected",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPrinterConnected() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterConnected
            )
        }
    }

    @Composable
    @Preview(
        name = "PSU Cycling",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPsuCycling() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterPsuCycling
            )
        }
    }

    @Composable
    @Preview(
        name = "Printer offline",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPrinterOffline() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterOffline(false)
            )
        }
    }

    @Composable
    @Preview(
        name = "Printer offline (PSU)",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewPrinterOfflinePsu() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.PrinterOffline(true)
            )
        }
    }

    @Composable
    @Preview(
        name = "Waiting for printer",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForPrinter() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForPrinterToComeOnline(null)
            )
        }
    }

    @Composable
    @Preview(
        name = "Waiting for printer (PSU on)",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForPrinterPsuOn() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForPrinterToComeOnline(true)
            )
        }
    }

    @Composable
    @Preview(
        name = "Waiting for printer (PSU off)",
        widthDp = Constants.PREVIEW_SIZE_DP,
        heightDp = Constants.PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun PreviewWaitingForPrinterPsuOff() {
        PreviewContainer {
            HeaderContent(
                vm = ConnectPrinterViewModel(""),
                uiState = ConnectPrinterUseCase.UiState.WaitingForPrinterToComeOnline(false)
            )
        }
    }
}
*/