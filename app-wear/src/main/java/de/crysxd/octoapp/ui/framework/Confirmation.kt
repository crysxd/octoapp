package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Check
import androidx.compose.material.icons.rounded.Close
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Text
import androidx.wear.compose.material.dialog.Alert
import androidx.wear.compose.material.dialog.Dialog
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ext.setScreenStaysOn
import de.crysxd.octoapp.ext.showFailure
import de.crysxd.octoapp.ui.OctoAppTheme
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@Composable
fun Confirmation(
    state: ConfirmationState,
    text: String,
    action: suspend () -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()
    val stateImpl = (state as ConfirmationStateImpl)
    val visible by stateImpl.visibleFlow.collectAsState()
    val loading by stateImpl.loading.collectAsState()
    val context = LocalContext.current

    // Reset loading whenever we are shown
    LaunchedEffect(visible) {
        if (visible) {
            stateImpl.loading.value = false
        }
    }

    Dialog(
        showDialog = visible,
        onDismissRequest = { state.visible = false },
    ) {
        if (loading) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(MaterialTheme.colors.background),
                contentAlignment = Alignment.Center
            ) {
                Text(text = stringResource(id = R.string.loading))
            }
        } else {
            val haptic = LocalHapticFeedback.current
            Alert(
                title = {
                    Text(text, textAlign = TextAlign.Center)
                },
                negativeButton = {
                    PrimaryIconButton(
                        icon = Icons.Rounded.Close,
                        color = OctoAppTheme.colors.errorBackground,
                        onClick = {
                            haptic.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                            state.visible = false
                        }
                    )
                },
                positiveButton = {
                    PrimaryIconButton(
                        icon = Icons.Rounded.Check,
                        onClick = {
                            coroutineScope.launch {
                                try {
                                    context.requireActivity().setScreenStaysOn(true)
                                    haptic.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                                    stateImpl.loading.value = true
                                    action()
                                } catch (e: CancellationException) {
                                    // Ignore
                                } catch (e: Exception) {
                                    Napier.e(tag = "Confirmation", message = "Failed to execute action", throwable = e)
                                    context.showFailure(e)
                                } finally {
                                    context.requireActivity().setScreenStaysOn(false)
                                    state.visible = false
                                    haptic.performHapticFeedback(HapticFeedbackType.LongPress)
                                }
                            }
                        }
                    )
                }
            )
        }
    }
}

@Composable
fun rememberConfirmationState() = remember {
    mutableStateOf<ConfirmationState>(ConfirmationStateImpl())
}

interface ConfirmationState {
    var visible: Boolean
}

private data class ConfirmationStateImpl(
    val visibleFlow: MutableStateFlow<Boolean> = MutableStateFlow(false),
    val loading: MutableStateFlow<Boolean> = MutableStateFlow(false),
) : ConfirmationState {
    override var visible
        get() = visibleFlow.value
        set(value) {
            visibleFlow.value = value
        }
}