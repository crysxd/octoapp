package de.crysxd.octoapp.ui.screens.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.BugReport
import androidx.compose.material.icons.rounded.Cloud
import androidx.compose.material.icons.rounded.CloudOff
import androidx.compose.material.icons.rounded.HelpOutline
import androidx.compose.material.icons.rounded.PhoneAndroid
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.Text
import com.google.android.gms.wearable.PutDataRequest
import com.google.android.gms.wearable.Wearable
import de.crysxd.octoapp.BuildConfig
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.blockingAwait
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.di.WearInjector
import de.crysxd.octoapp.ext.openOnPhone
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ext.showOpenPhoneSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.headerItem
import de.crysxd.octoapp.utils.Constants
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import java.util.zip.GZIPOutputStream


@Composable
fun InfoUi(
    octoPrintCount: Int?,
) {
    val nodeCount by WearInjector.get().wearDataLayerService().nodeCount.collectAsState(null)
    InfoFragmentUiInner(nodeCount = nodeCount, octoPrintCount = octoPrintCount)
}

@Composable
private fun InfoFragmentUiInner(
    nodeCount: Int?,
    octoPrintCount: Int?,
) = OctoAppTheme.WithTheme {
    MainPageScaffold {
        OctoScalingLazyColumn(state = scalingLazyListState) {
            when {
                // Initial is empty
                nodeCount == null || octoPrintCount == null -> Unit

                // No supported phone app
                nodeCount == 0 && octoPrintCount == 0 -> noSupportedApp()

                // No OctoPrints
                nodeCount >= 0 && octoPrintCount == 0 -> noOctoPrints()

                // Ready
                octoPrintCount > 0 -> ready(nodeCount = nodeCount)
            }
        }

    }
}

private fun ScalingLazyListScope.titleDetailButton(
    title: @Composable () -> String,
    detail: @Composable () -> String,
    buttonText: @Composable () -> String,
    buttonIcon: ImageVector,
    buttonUri: () -> Url,
) {
    item(key = "icon") {
        Image(
            painter = painterResource(R.drawable.octo_base),
            contentDescription = null,
            contentScale = ContentScale.FillHeight,
            modifier = Modifier
                .padding(top = 50.dp)
                .height(72.dp)
                .fillMaxWidth(),
            alignment = Alignment.Center,
        )
    }
    item(key = "title") {
        Text(
            text = title(),
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.title,
            modifier = Modifier
                .padding(bottom = OctoAppTheme.dimens.margin1)
                .fillMaxSize()
        )
    }
    item(key = "detail") {
        Text(
            text = detail(),
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.base,
            modifier = Modifier
                .padding(bottom = OctoAppTheme.dimens.margin2)
                .fillMaxSize()
        )
    }
    item(key = "action") {
        val activity = LocalContext.current.requireActivity()
        OctoAppTheme.ForMenuStyle(OctoAppTheme.MenuStyle.Settings) {
            MenuItemChip(
                text = buttonText(),
                icon = buttonIcon,
            ) {
                buttonUri().openOnPhone(activity)
            }
        }
    }
}

private fun ScalingLazyListScope.noSupportedApp() = titleDetailButton(
    title = { stringResource(R.string.sign_in___discovery___welcome_title) },
    detail = { stringResource(R.string.wear_info___empty_no_app) },
    buttonText = { stringResource(R.string.wear_empty___install_app) },
    buttonIcon = Icons.Rounded.PhoneAndroid,
    buttonUri = { UriLibrary.getLaunchAppUri() }
)

private fun ScalingLazyListScope.noOctoPrints() = titleDetailButton(
    title = { stringResource(R.string.sign_in___discovery___welcome_title) },
    detail = { stringResource(R.string.wear_empty___no_octoprints) },
    buttonText = { stringResource(R.string.wear_empty___connect_octoprint) },
    buttonIcon = Icons.Rounded.PhoneAndroid,
    buttonUri = { UriLibrary.getLaunchAppUri() }
)

private fun ScalingLazyListScope.ready(nodeCount: Int) {
    headerItem(key = "header") {
        Text(
            text = stringResource(R.string.main_menu___menu_settings_title),
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.title,
            modifier = Modifier
                .padding(vertical = OctoAppTheme.dimens.margin1)
                .fillMaxWidth()
        )

        Column(
            modifier = Modifier
                .padding(bottom = OctoAppTheme.dimens.margin1)
                .border(
                    width = 1.dp,
                    color = MaterialTheme.colors.onBackground.copy(alpha = 0.4f),
                    shape = MaterialTheme.shapes.large
                )
                .padding(OctoAppTheme.dimens.margin0to1)
        ) {
            Row(
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                Icon(
                    imageVector = if (nodeCount > 0) Icons.Rounded.Cloud else Icons.Rounded.CloudOff,
                    contentDescription = null,
                    modifier = Modifier
                        .size(20.dp)
                        .padding(end = OctoAppTheme.dimens.margin0to1)
                )
                Text(
                    text = stringResource(if (nodeCount > 0) R.string.wear_settins___phone_online else R.string.wear_settins___phone_offline),
                    textAlign = TextAlign.Center,
                    style = OctoAppTheme.typography.sectionHeader,
                )
            }

            Text(
                text = stringResource(R.string.wear_settings___phone_sync_description),
                textAlign = TextAlign.Center,
                style = OctoAppTheme.typography.labelSmall,
                modifier = Modifier
                    .fillMaxWidth()
            )
        }

    }

    item(key = "faq") {
        val activity = LocalContext.current.requireActivity()
        OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Settings) {
            MenuItemChip(
                text = stringResource(R.string.wear_settings___faq_help),
                icon = Icons.Rounded.HelpOutline,
            ) {
                UriLibrary.getHelpUri().openOnPhone(activity)
            }
        }
    }

    item(key = "bug") {
        OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Settings) {
            val context = LocalContext.current
            MenuItemChip(
                text = stringResource(R.string.wear_settings___report_bug),
                icon = Icons.Rounded.BugReport,
            ) {
                val params = CreateBugReportUseCase.Params()
                val bugReport = BaseInjector.get().createBugReportUseCase().execute(params)
                val bugReportBytes = ByteArrayOutputStream().also { bos ->
                    GZIPOutputStream(bos).use { gos ->
                        ObjectOutputStream(gos).use { oos -> oos.writeObject(bugReport) }
                    }
                }.toByteArray()
                Napier.i(tag = "RPC", message = "Sending bug report (${bugReportBytes.size} bytes)")
                val request = PutDataRequest.create(context.getString(R.string.rpc_asset_path___bug_report_bundle))
                    .setData(bugReportBytes)
                    .setUrgent()
                Wearable.getDataClient(context).putDataItem(request).blockingAwait()
                context.showOpenPhoneSuccess(context.getString(R.string.wear_uri___confirmation_open_on_phone), 3000)
            }
        }
    }

    item(key = "version") {
        Text(
            text = BuildConfig.VERSION_NAME,
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.label,
            modifier = Modifier
                .padding(top = OctoAppTheme.dimens.margin1)
                .fillMaxSize()
        )
    }
}

@Composable
@Preview(name = "Initial", widthDp = Constants.PREVIEW_SIZE_DP, heightDp = Constants.PREVIEW_SIZE_DP, showBackground = true, backgroundColor = 0xFF000000)
fun PreviewInitial() {
    InfoFragmentUiInner(
        nodeCount = null,
        octoPrintCount = null,
    )
}

@Composable
@Preview(name = "Unsupported app", widthDp = Constants.PREVIEW_SIZE_DP, heightDp = Constants.PREVIEW_SIZE_DP, showBackground = true, backgroundColor = 0xFF000000)
fun PreviewUnsupportedApp() {
    InfoFragmentUiInner(
        nodeCount = 0,
        octoPrintCount = 0,
    )
}

@Composable
@Preview(name = "Not set up", widthDp = Constants.PREVIEW_SIZE_DP, heightDp = Constants.PREVIEW_SIZE_DP, showBackground = true, backgroundColor = 0xFF000000)
fun PreviewNotSetUp() {
    InfoFragmentUiInner(
        nodeCount = 1,
        octoPrintCount = 0,
    )
}

@Composable
@Preview(name = "App disconnected", device = Devices.WEAR_OS_LARGE_ROUND, showBackground = true, backgroundColor = 0xFF000000)
fun PreviewAppDisconnected() {
    InfoFragmentUiInner(
        nodeCount = 0,
        octoPrintCount = 2,
    )
}

@Composable
@Preview(name = "App connected", device = Devices.WEAR_OS_SMALL_ROUND, showBackground = true, backgroundColor = 0xFF000000)
fun PreviewAppConnected() {
    InfoFragmentUiInner(
        nodeCount = 1,
        octoPrintCount = 2,
    )
}