package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.booleanResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ui.OctoAppTheme

@Composable
fun MenuHeader(text: String, mainTitle: Boolean = true) {
    val needsMargin = mainTitle && booleanResource(id = R.bool.round)
    val topMargin = if (needsMargin) with(LocalDensity.current) { 50.sp.toDp() } else 0.dp
    val sideMargin = if (needsMargin) with(LocalDensity.current) { 20.sp.toDp() } else 0.dp
    Text(
        text = text,
        style = OctoAppTheme.typography.sectionHeader,
        textAlign = TextAlign.Center,
        color = MaterialTheme.colors.onBackground.copy(alpha = 0.7f),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                bottom = OctoAppTheme.dimens.margin0to1,
                // This is to prevent collisions of the header with the clock. We scale it with sp as
                // this problem mostly occurs with scaled font as the clock curves further down
                top = topMargin,
                start = sideMargin,
                end = sideMargin,
            )
    )
}

fun ScalingLazyListScope.menuHeader() {
    item(key = "menuHeader") {
        MenuHeader(
            text = stringResource(id = R.string.widget_quick_access),
            mainTitle = false
        )
    }
}