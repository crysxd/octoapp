package de.crysxd.octoapp.di

import dagger.Component
import de.crysxd.octoapp.base.di.BaseComponent
import de.crysxd.octoapp.utils.GetAmbientModeStateUseCase
import de.crysxd.octoapp.wear.WearDataLayerService

@WearScope
@Component(
    modules = [
        ViewModule::class
    ],
    dependencies = [
        BaseComponent::class
    ]
)
interface WearComponent {
    fun wearDataLayerService(): WearDataLayerService
    fun getAmbientModeStateUseCase(): GetAmbientModeStateUseCase
}