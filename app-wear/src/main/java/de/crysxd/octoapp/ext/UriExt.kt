package de.crysxd.octoapp.ext

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.wear.remote.interactions.RemoteActivityHelper
import de.crysxd.octoapp.R
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.guava.asDeferred

suspend fun Url.openOnPhone(activity: Activity) = Uri.parse(toString()).openOnPhone(activity)

suspend fun Uri.openOnPhone(activity: Activity) {
    val remoteActivityHelper = RemoteActivityHelper(activity)

    val result = remoteActivityHelper.startRemoteActivity(
        Intent(Intent.ACTION_VIEW)
            .setData(this@openOnPhone)
            .addCategory(Intent.CATEGORY_BROWSABLE),
    )

    try {
        result.asDeferred().await()
        activity.showOpenPhoneSuccess(activity.getString(R.string.wear_uri___confirmation_open_on_phone))
    } catch (e: Exception) {
        Napier.e(tag = "Uri", message = "Failed to open on phone", throwable = e)
        activity.showFailure(activity.getString(R.string.wear_uri___failure_open_on_phone))
    }
}