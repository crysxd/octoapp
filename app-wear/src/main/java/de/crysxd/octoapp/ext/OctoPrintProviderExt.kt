package de.crysxd.octoapp.ext

import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.exceptions.MissingPluginException
import de.crysxd.octoapp.base.ext.WhileSubscribedOcto
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.TriggerInitialCancelObjectMessageUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.message.CancelObjectPluginMessage
import de.crysxd.octoapp.viewmodels.CancelObjectViewModelCore
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicLong

private const val TAG = "OctoPrintProvider"
private val eventFlows = mutableMapOf<String, Flow<Event>>()
private val cancelObjectFlows = mutableMapOf<String, Flow<List<CancelObjectViewModelCore.PrintObject>>>()
private val interpolationFlows = mutableMapOf<String, MutableStateFlow<Message.Current?>>()
private const val MIN_CAPABILITY_INTERVAL = 1000 * 60 * 5L

fun PrinterEngineProvider.wearEventFlow(
    tag: String,
    instanceId: String,
) = eventFlows.getOrPut(instanceId) {
    val lastConnect = AtomicLong(0)
    eventFlow(
        tag = tag,
        instanceId = instanceId,
        config = EventSource.Config(
            throttle = 10,
            requestTerminalLogs = emptyList(),
            requestPlugins = true,
        )
    ).onEach {
        handleEvent(it, instanceId, lastConnect)
    }.onStart {
        Napier.d(tag = tag, message = "Starting shared wear event flow on $instanceId")
    }.onCompletion {
        Napier.d(tag = tag, message = "Stopping shared wear event flow on $instanceId")
    }.shareIn(
        scope = AppScope,
        started = SharingStarted.WhileSubscribedOctoDelay,
        replay = 1
    )
}

private fun interpolationFlow(instanceId: String) = interpolationFlows.getOrPut(instanceId) { MutableStateFlow(null) }

fun injectCurrentMessage(instanceId: String, currentMessage: Message.Current) {
    Napier.i(tag = TAG, message = "Setting interpolation!!")
    interpolationFlow(instanceId).value = currentMessage
}

fun PrinterEngineProvider.wearPassiveCurrentMessageFlow(
    tag: String,
    instanceId: String,
) = flow {
    emit(null)
    emitAll(passiveCurrentMessageFlow(tag = tag, instanceId = instanceId))
}.combine(interpolationFlow(instanceId = instanceId)) { actual, interpolation ->
    when {
        interpolation?.serverTime == null || actual?.serverTime == null -> {
            actual ?: interpolation
        }

        interpolation.serverTime > actual.serverTime -> interpolation.also {
            Napier.i(tag = tag, message = "Interpolation for current message is being used!")
        }

        else -> actual
    }
}.filterNotNull()

fun PrinterEngineProvider.wearCancelObjectFlow(
    tag: String,
    instanceId: String,
) = cancelObjectFlows.getOrPut(instanceId) {
    combine(
        passiveCachedMessageFlow(tag = tag, instanceId = instanceId, clazz = CancelObjectPluginMessage.ObjectList::class).map { it?.objects },
        passiveCachedMessageFlow(tag = tag, instanceId = instanceId, clazz = CancelObjectPluginMessage.Active::class).map { it?.activeId },
        passiveCurrentMessageFlow(tag = tag, instanceId = instanceId).map { it.printObjects }.distinctUntilChanged()
    ) { objects, activeId, printObjects ->
        printObjects?.all?.map {
            CancelObjectViewModelCore.PrintObject(
                objectId = it.id,
                cancelled = it.id in printObjects.excluded,
                active = it.id == printObjects.currentObject,
                label = it.label ?: it.id,
                center = null,
            )
        } ?: (objects?.mapNotNull {
            if (it.ignore == true) return@mapNotNull null
            CancelObjectViewModelCore.PrintObject(
                objectId = it.id?.toString() ?: return@mapNotNull null,
                cancelled = it.cancelled == true,
                label = it.label ?: it.id.toString(),
                active = it.id == activeId,
                center = null,
            )
        } ?: emptyList())
    }.onStart {
        emit(emptyList())
        Napier.d(tag = tag, message = "Starting shared wear cancel object flow on $instanceId")
        try {
            BaseInjector.get().triggerInitialCancelObjectMessageUseCase().execute(TriggerInitialCancelObjectMessageUseCase.Params(instanceId))
            Napier.d(tag = tag, message = "Requested initial cancel object message for $instanceId")
        } catch (e: MissingPluginException) {
            Napier.d(tag = tag, message = "Cancel object plugin not installed on $instanceId")
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to request initial cancel object message for $instanceId", throwable = e)
        }
    }.onCompletion {
        Napier.d(tag = tag, message = "Stopping shared wear cancel object flow on $instanceId")
    }.shareIn(
        scope = AppScope,
        started = SharingStarted.WhileSubscribedOctoDelay,
        replay = 1
    )
}

private fun handleEvent(event: Event, instanceId: String, lastConnect: AtomicLong) = AppScope.launch {
    try {
        when ((event as? Event.MessageReceived)?.message) {
            // Something changed, refresh capabilities
            is Message.Event.SettingsUpdated -> refreshCapabilities(instanceId, false)

            // Refresh capabilities when connected after a while
            is Message.Connected -> {
                Napier.d(tag = TAG, message = "Handling connected on $instanceId")

                val timeSinceLast = (System.currentTimeMillis() - lastConnect.get())
                lastConnect.set(System.currentTimeMillis())
                if (timeSinceLast > MIN_CAPABILITY_INTERVAL) {
                    refreshCapabilities(instanceId, false)
                } else {
                    Napier.d(tag = TAG, message = "Skipping capability refresh on connect")
                }
            }

            // Default
            else -> Unit
        }
    } catch (e: Exception) {
        Napier.e(tag = TAG, message = "Failed to handle event: $event", throwable = e)
    }
}

private suspend fun refreshCapabilities(instanceId: String, loadSettings: Boolean) {
    Napier.i(tag = TAG, message = "Refreshing instance capabilities: $instanceId")
    val params = UpdateInstanceCapabilitiesUseCase.Params(
        instanceId = instanceId,
        updateCommands = false,
        updatePlugins = false,
        updateProfile = false,
        updateSettings = loadSettings,
        updateSystemInfo = false,
        updateVersion = false,
    )
    BaseInjector.get().updateInstanceCapabilitiesUseCase().execute(params)
}
