package de.crysxd.octoapp.utils

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.booleanResource
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector


val isRound: Boolean
    @Composable get() = booleanResource(id = R.bool.round)

val isPreview: Boolean
    get() = try {
        BaseInjector.get().app()
        false
    } catch (e: UninitializedPropertyAccessException) {
        true
    }