package de.crysxd.octoapp.utils

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.UseCase2
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.engine.models.event.HistoricTemperatureData
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.printer.PrinterState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import javax.inject.Inject

class GetAmbientModeStateUseCase @Inject constructor(
    val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<GetAmbientModeStateUseCase.Params, Message.Current>() {


    override suspend fun doExecute(param: Params, logger: Logger) = withContext(Dispatchers.IO) {
        supervisorScope {
            // Get status
            val octoPrint = printerEngineProvider.printer(param.instanceId)
            val deferredJob = async { octoPrint.jobApi.getJob() }
            val deferredTime = async {
                try {
                    octoPrint.systemApi.getSystemInfo().generatedAt
                } catch (e: Exception) {
                    logger.e(message = "Failed to get time from server, might be missing permissions", e = e)
                    Clock.System.now()
                }
            }
            val deferredStatus = async {
                try {
                    octoPrint.printerApi.getPrinterState()
                } catch (e: PrinterNotOperationalException) {
                    null
                }
            }
            val job = deferredJob.await()
            val status = deferredStatus.await()
            val time = deferredTime.await()

            // Create message
            Message.Current(
                logs = emptyList(),
                temps = listOf(HistoricTemperatureData(time = time.toEpochMilliseconds(), components = status?.temperature ?: emptyMap())),
                progress = job.progress,
                job = job.info,
                offsets = null,
                serverTime = time,
                state = status?.state ?: PrinterState.State(),
                isHistoryMessage = false,
            )
        }
    }

    data class Params(
        val instanceId: String,
    )
}