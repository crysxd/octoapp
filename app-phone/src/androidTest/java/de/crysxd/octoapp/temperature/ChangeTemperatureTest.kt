package de.crysxd.octoapp.temperature

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.By.text
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.idRes
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.framework.text
import de.crysxd.octoapp.sharedcommon.ext.formatAsTemperature
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import kotlin.time.Duration.Companion.seconds

class ChangeTemperatureTest {

    private val testEnvVanilla = TestEnvironmentLibrary.Terrier
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnvVanilla))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AutoConnectPrinterRule())

    @Test(timeout = 300_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_the_temperature_is_changed_THEN_the_temperature_updates() = with(uiDevice) {
        //region Setup
        BaseInjector.get().octorPrintRepository().setActive(testEnvVanilla)
        baristaRule.launchActivity()
        WorkspaceRobot.waitForPrepareWorkspace()
        //endregion
        //region Verify state
        waitForPass("Wait for temperatures", timeout = 10.seconds) {
            assert(hasObject(res(TestTags.Temperature.Label("tool0")).text(R.string.general___hotend))) { "Expected hotend to be shown" }
            assert(hasObject(res(TestTags.Temperature.Actual("tool0")).text(21.3.formatAsTemperature(minDecimals = 1)))) { "Expected actual temperature to be shown" }
            assert(hasObject(res(TestTags.Temperature.Target("tool0")).text(R.string.target_off))) { "Expected hotend target to be shown" }
            assert(hasObject(res(TestTags.Temperature.Label("bed")).text(R.string.general___bed))) { "Expected bed to be shown" }
            assert(hasObject(res(TestTags.Temperature.Actual("bed")).text(21.3.formatAsTemperature(minDecimals = 1)))) { "Expected actual bed temperature to be shown" }
            assert(hasObject(res(TestTags.Temperature.Target("bed")).text(R.string.target_off))) { "Expected bed target to be shown" }
        }
        //endregion
        //region Change hotend temp
        findObject(res(TestTags.Temperature.SetButton("tool0"))).click()
        waitForPass("Wait for edit screen", timeout = 3.seconds) {
            findObject(text(getString(R.string.x_temperature, getString(R.string.general___hotend)))).click()
        }
        findObject(idRes(R.id.textInputLayout)).findObject(idRes(R.id.input)) .text = "60"
        findObject(idRes(R.id.textInputLayout2)).findObject(idRes(R.id.input)) .text = "-5"
        findObject(text(R.string.set_temperature)).click()
        //endregion
        //region Wait for new temp
        waitForPass("Wait for temperatures", timeout = 20.seconds) {
            assert(hasObject(res(TestTags.Temperature.Label("tool0")).text(R.string.general___hotend))) { "Expected hotend to be shown" }
            assert(hasObject(res(TestTags.Temperature.Actual("tool0")).text(60.formatAsTemperature(minDecimals = 1)))) { "Expected actual temperature to be shown" }
            assert(hasObject(res(TestTags.Temperature.Target("tool0")).text(R.string.target_x_offset_y, "60", "-5"))) { "Expected hotend target to be shown" }
            assert(hasObject(res(TestTags.Temperature.Label("bed")).text(R.string.general___bed))) { "Expected bed to be shown" }
            assert(hasObject(res(TestTags.Temperature.Actual("bed")).text(21.3.formatAsTemperature(minDecimals = 1)))) { "Expected actual bed temperature to be shown" }
            assert(hasObject(res(TestTags.Temperature.Target("bed")).text(R.string.target_off))) { "Expected bed target to be shown" }
        }
        //endregion
    }
}
