package de.crysxd.octoapp.connect

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import com.google.common.truth.Truth.assertThat
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManagerTest
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.robots.ConnectRobot
import de.crysxd.octoapp.framework.robots.ControlCenterRobot
import de.crysxd.octoapp.framework.robots.SignInRobot
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import de.crysxd.octoapp.tests.utils.PsuUtils.turnAllOff
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.setVirtualPrinterEnabled
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class ConnectPrinterTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val powerControlsTestEnv = TestEnvironmentLibrary.Dachshund
    private val wrongEnv = PrinterConfigurationV3(
        id = "random",
        webUrl = "http://127.0.0.1:100".toUrl(),
        apiKey = "XXXXXXX"
    )

    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv, powerControlsTestEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AcceptAllAccessRequestRule(testEnv))
        .around(AutoConnectPrinterRule())

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_auto_connect_is_disabled_THEN_connect_button_can_be_used() = with(uiDevice) {
        //region GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        BaseInjector.get().octoPreferences().isAutoConnectPrinter = false
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        ConnectRobot.waitForConnectionState(R.string.connect_printer___waiting_for_user_title)
        ConnectRobot.useConnectAction(R.string.connect_printer___begin_connection)
        ConnectRobot.confirmManualConnectionDialog()
        //endregion
        //region THEN
        WorkspaceRobot.waitForPrepareWorkspace()
        //endregion
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_auto_connect_is_disabled_and_PSU_can_be_controlled_THEN_connect_button_can_be_used() = with(uiDevice) {
        //region GIVEN
        BaseInjector.get().octorPrintRepository().setActive(
            powerControlsTestEnv.copy(
                appSettings = AppSettings(
                    defaultPowerDevices = mapOf(AppSettings.DEFAULT_POWER_DEVICE_PSU to "psucontrol:psu")
                )
            )
        )
        BaseInjector.get().octoPreferences().isAutoConnectPrinter = false
        powerControlsTestEnv.turnAllOff()
        powerControlsTestEnv.setVirtualPrinterEnabled(false)
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        ConnectRobot.waitForConnectionState(R.string.connect_printer___waiting_for_user_title)
        ConnectRobot.useConnectAction(R.string.connect_printer___begin_connection)
        ConnectRobot.confirmManualConnectionDialog()
        ConnectRobot.assertPsuCanBeControlled(turnedOn = true, doClick = true)
        ConnectRobot.assertPsuCanBeControlled(turnedOn = false)
        powerControlsTestEnv.setVirtualPrinterEnabled(true)
        //endregion
        //region THEN
        WorkspaceRobot.waitForPrepareWorkspace()
        //endregion
    }


    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoPrint_not_available_and_no_quick_switch_THEN_other_OctoPrint_can_be_connected() = with(uiDevice) {
        //region GIVEN
        BaseInjector.get().octorPrintRepository().setActive(wrongEnv)
        BillingManagerTest.enabledForTest = false
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        ConnectRobot.waitForConnectionState(R.string.connect_printer___octoprint_not_available_title)
        findObject(By.text(getString(R.string.sign_in___connect_to_other_octoprint))).click()
        SignInRobot.waitForDiscoveryOptionsToBeShown()
        //endregion
        //region THEN
        assertThat(BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()).isNull()
        //endregion
    }

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoPrint_not_available_and_quick_switch_available_THEN_other_OctoPrint_can_be_connected() = with(uiDevice) {
        //region GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        BaseInjector.get().octorPrintRepository().setActive(wrongEnv)
        BillingManagerTest.enabledForTest = true
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        WorkspaceRobot.waitForConnectWorkspace()
        ConnectRobot.waitForConnectionState(R.string.connect_printer___octoprint_not_available_title)
        findObject(By.text(getString(R.string.sign_in___connect_to_other_octoprint))).click()
        ControlCenterRobot.waitForControlCenterOpen()
        ControlCenterRobot.activateInstance(testEnv)
        //endregion
        //region THEN
        WorkspaceRobot.waitForPrepareWorkspace()
        assertThat(BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.webUrl).isEqualTo(testEnv.webUrl)
        //endregion
    }

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_power_controls_are_available_THEN_psu_can_be_turned_on() = with(uiDevice) {
        //region GIVEN
        BaseInjector.get().octorPrintRepository().setActive(
            powerControlsTestEnv.copy(
                appSettings = AppSettings(
                    defaultPowerDevices = mapOf(AppSettings.DEFAULT_POWER_DEVICE_PSU to "psucontrol:psu")
                )
            )
        )
        powerControlsTestEnv.turnAllOff()
        powerControlsTestEnv.setVirtualPrinterEnabled(false)
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        ConnectRobot.waitForConnectionState(R.string.connect_printer___waiting_for_printer_title)
        ConnectRobot.assertPsuCanBeControlled(turnedOn = true, doClick = true)
        ConnectRobot.assertPsuCanBeControlled(turnedOn = false)
        powerControlsTestEnv.setVirtualPrinterEnabled(true)
        //endregion
        //region THEN
        WorkspaceRobot.waitForPrepareWorkspace()
        //endregion
    }

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_power_controls_are_not_set_up_THEN_psu_can_be_configured_and_turned_on() = with(uiDevice) {
        //region GIVEN
        BaseInjector.get().octorPrintRepository().setActive(powerControlsTestEnv)
        powerControlsTestEnv.turnAllOff()
        powerControlsTestEnv.setVirtualPrinterEnabled(false)
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        ConnectRobot.waitForConnectionState(R.string.connect_printer___waiting_for_printer_title)
        ConnectRobot.configureDefaultPowerDevice("PSU")
        ConnectRobot.assertPsuCanBeControlled(turnedOn = true, doClick = true)
        ConnectRobot.assertPsuCanBeControlled(turnedOn = false)
        powerControlsTestEnv.setVirtualPrinterEnabled(true)
        //endregion
        //region THEN
        WorkspaceRobot.waitForPrepareWorkspace()
        assert(
            BaseInjector.get().octorPrintRepository().get(powerControlsTestEnv.id)!!.appSettings!!.defaultPowerDevices ==
                    mapOf(AppSettings.DEFAULT_POWER_DEVICE_PSU to "psucontrol:psu")
        ) {
            "Expected default power device to be set"
        }
        //endregion
    }
}