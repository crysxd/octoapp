package de.crysxd.octoapp.remoteaccess

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.robots.ConnectRobot
import de.crysxd.octoapp.framework.robots.MenuRobot
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AbstractMockKoinRule
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere.OctoEverywhereConnectionNotFoundException
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.octoeverywhere.OctoEverywhereRobot
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.koin.dsl.module
import java.net.URLEncoder
import kotlin.time.Duration.Companion.seconds

class OctoEverywhereTest {

    private val testEnv = TestEnvironmentLibrary.Corgi
    private val remoteTestEnv = TestEnvironmentLibrary.CorgiRemote
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    companion object {
        private const val ID = "connectionid"
        private const val USER = "user"
        private const val PASSWORD = "pw"
        private const val BEARER_TOKEN = "token"
        private const val API_KEY = "api"
    }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AcceptAllAccessRequestRule(testEnv))
        .around(MockOctoEverywhereConnectionRule())
        .around(AutoConnectPrinterRule())

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoEverywhere_is_connected_THEN_then_app_can_fall_back() {
        // GIVEN
        OctoEverywhereRobot.setPremiumAccountActive(true)
        BaseInjector.get().octorPrintRepository().setActive(remoteTestEnv)

        // WHEN
        baristaRule.launchActivity()

        // THEN
        waitForPass("Expect OE to be connected", timeout = 10.seconds) {
            uiDevice.findObject(By.text(getString(R.string.main___banner_connected_via_octoeverywhere)))
        }
        WorkspaceRobot.waitForPrepareWorkspace()
    }

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoEverywhere_is_not_connected_THEN_then_we_can_connect_it() = with(uiDevice) {
        // GIVEN
        OctoEverywhereRobot.setPremiumAccountActive(true)
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        baristaRule.launchActivity()

        // WHEN
        // Go to connect screen
        WorkspaceRobot.waitForPrepareWorkspace()
        MenuRobot.openMenuWithMoreButton()
        MenuRobot.clickMenuButton(R.string.main_menu___configure_remote_access)

        // Select OE tab
        waitForPass("Waiting for OE to be completed") {
            assert(findObject(By.text(getString(R.string.configure_remote_access___title))) != null) { "Expected title to be shown" }

            findObject(By.text(getString(R.string.configure_remote_acces___octoeverywhere___title))).also {
                assert(it != null) { "Expected OE tab to be shown" }
            }.click()
            findObject(By.text(getString(R.string.configure_remote_acces___octoeverywhere___connect_button))).also {
                assert(it != null) { "Expected OE tab to be shown" }
            }.click()
        }

        // Disclaimer dialog
        waitForDialog(withText(R.string.configure_remote_acces___leaving_app___title))
        onView(withText(R.string.sign_in___continue)).inRoot(RootMatchers.isDialog()).perform(click())

        // THEN
        waitForPass("Waiting for connection to be completed") {
            assert(findObject(By.text(getString(R.string.configure_remote_acces___octoeverywhere___connected))) != null) { "Expected OE tab to be shown" }
        }

        val info = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
        assertThat(info).isNotNull()
        assertThat(info!!.alternativeWebUrl).isEqualTo(remoteTestEnv.alternativeWebUrl!!.withBasicAuth(user = USER, password = PASSWORD))
        assertThat(info.octoEverywhereConnection).isNotNull()
        assertThat(info.octoEverywhereConnection!!.apiToken).isEqualTo(API_KEY)
        assertThat(info.octoEverywhereConnection!!.basicAuthPassword).isEqualTo(PASSWORD)
        assertThat(info.octoEverywhereConnection!!.basicAuthUser).isEqualTo(USER)
        assertThat(info.octoEverywhereConnection!!.bearerToken).isEqualTo(BEARER_TOKEN)
        assertThat(info.octoEverywhereConnection!!.connectionId).isEqualTo(ID)

        // WHEN
        findObject(By.text(getString(R.string.configure_remote_acces___octoeverywhere___disconnect_button))).also {
            assert(it != null) { "Expected OE tab to be shown" }
        }.click()
        waitForPass("Waiting for connection to be removed") {
            assert(findObject(By.text(getString(R.string.configure_remote_acces___octoeverywhere___connected))) == null) { "Expected OE tab to be shown" }
        }
        val info2 = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
        assertThat(info2).isNotNull()
        assertThat(info2!!.alternativeWebUrl).isNull()
        assertThat(info2.octoEverywhereConnection).isNull()
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoEverywhere_premium_is_no_longer_available_THEN_then_we_disconnect_it() = with(uiDevice) {
        // GIVEN
        OctoEverywhereRobot.setPremiumAccountActive(false)
        BaseInjector.get().octorPrintRepository().setActive(remoteTestEnv)

        // WHEN
        baristaRule.launchActivity()

        // THEN
        waitForDialog(withText(containsString("OctoEverywhere disabled")))
        onView(withText(R.string.sign_in___continue)).perform(click())

        val info = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
        assertThat(info).isNotNull()
        assertThat(info!!.alternativeWebUrl).isNull()
        assertThat(info.octoEverywhereConnection).isNull()

        waitForPass("Waiting for remote access screen") {
            assert(findObject(By.text(getString(R.string.configure_remote_access___title))) != null) { "Expected configure screen shown" }
        }

        assert(pressBack()) { "Expected back to work" }

        ConnectRobot.waitForConnectionState(R.string.connect_printer___octoprint_not_available_title)
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoEverywhere_connection_was_deleted_THEN_then_we_disconnect_it() = with(uiDevice) {
        // GIVEN
        OctoEverywhereRobot.setPremiumAccountActive(true)
        BaseInjector.get().octorPrintRepository()
            .setActive(remoteTestEnv.copy(alternativeWebUrl = "https://shared-C2WCLVUQYA7EW6HKGAVNWA16DSHPIFV2.octoeverywhere.com".toUrl()))

        // WHEN
        baristaRule.launchActivity()

        // THEN
        waitForDialog(withText(OctoEverywhereConnectionNotFoundException(Url("http://test.com"), 604).userFacingMessage))
        val info = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
        assertThat(info).isNotNull()
        assertThat(info!!.alternativeWebUrl).isNull()
        assertThat(info.octoEverywhereConnection).isNull()

        onView(withText(R.string.sign_in___continue)).perform(click())

        waitForPass("Waiting for remote access screen") {
            assert(findObject(By.text(getString(R.string.configure_remote_access___title))) != null) { "Expected configure screen shown" }
        }

        assert(pressBack()) { "Expected back to work" }

        ConnectRobot.waitForConnectionState(R.string.connect_printer___octoprint_not_available_title)
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    inner class MockOctoEverywhereConnectionRule : AbstractMockKoinRule() {

        override fun createMockModule() = module {
            factory {
                runBlocking {
                    val mock: GetRemoteServiceConnectUrlUseCase = mock()
                    // We use the octoapp protocol to kick this link directly back to the app in a similar fashion as the web flow. Host must be test.octoapp.eu!
                    val instanceId = SharedBaseInjector.get().printerConfigRepository.getActiveInstanceSnapshot()?.id!!
                    val path = GetRemoteServiceConnectUrlUseCase.RemoteService.OctoEverywhere.getCallbackPath(instanceId)
                    val url = "octoapp://test.octoapp.eu/$path?" +
                            "id=$ID&" +
                            "url=${URLEncoder.encode(remoteTestEnv.alternativeWebUrl.toString(), "UTF-8")}&" +
                            "authbasichttpuser=$USER&" +
                            "authbasichttppassword=$PASSWORD&" +
                            "authBearerToken=$BEARER_TOKEN&" +
                            "success=true&" +
                            "appApiToken=$API_KEY&"
                    whenever(
                        mock.execute(
                            GetRemoteServiceConnectUrlUseCase.Params(
                                instanceId = instanceId,
                                remoteService = GetRemoteServiceConnectUrlUseCase.RemoteService.OctoEverywhere
                            )
                        )
                    ).thenReturn(GetRemoteServiceConnectUrlUseCase.Result.Success(url))

                    mock
                }
            }
        }
    }
}
