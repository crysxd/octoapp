package de.crysxd.octoapp.login

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManagerTest
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.robots.SignInRobot
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.MockDiscoveryRule
import de.crysxd.octoapp.framework.rules.MockTestFullNetworkStackRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class ReusePreviousInstanceTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val mockTestFullNetworkStackRule = MockTestFullNetworkStackRule()
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val discoveryRule = MockDiscoveryRule()

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AutoConnectPrinterRule())
        .around(discoveryRule)
        .around(mockTestFullNetworkStackRule)
        .around(AcceptAllAccessRequestRule(testEnv))

    @Before
    fun setUp() {
        val repo = BaseInjector.get().octorPrintRepository()
        repo.setActive(testEnv)
        repo.clearActive()
        discoveryRule.mockForRandomFound()
    }

    @After
    fun tearDown() {
        BillingManagerTest.enabledForTest = null
    }

    @Test(timeout = 30_000L)
    @AllowFlaky(attempts = 1)
    fun WHEN_feature_disabled_THEN_api_key_is_needed() {
        BillingManagerTest.enabledForTest = false
        baristaRule.launchActivity()
        mockTestFullNetworkStackRule.mockForInvalidApiKey()

        SignInRobot.waitForDiscoveryOptionsToBeShown()
        SignInRobot.scrollDown()

        onView(withText(R.string.sign_in___discovery___previously_connected_devices)).check(matches(isDisplayed()))
        onView(withText(R.string.sign_in___discovery___quick_switch_disabled_title)).check(matches(isDisplayed()))
        onView(withText(R.string.sign_in___discovery___quick_switch_disabled_subtitle)).check(matches(isDisplayed()))

        // Select env...should start API key flow
        SignInRobot.selectDiscoveryOptionWithText(testEnv.label)
        SignInRobot.waitForChecks()
        SignInRobot.waitForSignInToBeCompleted()
        WorkspaceRobot.waitForPrepareWorkspace()
    }

    @Test(timeout = 30_000L)
    @AllowFlaky(attempts = 5)
    fun WHEN_feature_enabled_THEN_sign_in_succeeds() {
        BillingManagerTest.enabledForTest = true
        baristaRule.launchActivity()

        SignInRobot.waitForDiscoveryOptionsToBeShown()
        SignInRobot.scrollDown()
        SignInRobot.selectDiscoveryOptionWithText(testEnv.label)
        WorkspaceRobot.waitForPrepareWorkspace()

        assertThat(BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.webUrl).isEqualTo(testEnv.webUrl)
        verifyZeroInteractions(BaseInjector.get().testFullNetworkStackUseCase())
    }
}