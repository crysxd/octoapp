package de.crysxd.octoapp.framework.rules

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.whenever
import de.crysxd.octoapp.base.data.models.BackendType
import de.crysxd.octoapp.base.data.models.NetworkService
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseComponent
import de.crysxd.octoapp.base.usecase.DiscoverOctoPrintUseCase
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking

class MockDiscoveryRule : AbstractUseCaseMockRule() {

    val discoverUseCase = mock<DiscoverOctoPrintUseCase>()

    init {
        mockForNothingFound()
    }

    override fun createBaseComponent(base: BaseComponent) = MockBaseComponent(base)

    fun mockForNothingFound() = runBlocking {
        reset(discoverUseCase)
        whenever(discoverUseCase.execute(Unit)).thenReturn(flowOf(DiscoverOctoPrintUseCase.Result(emptyList())))
    }

    fun mockForRandomFound(max: Int = 999) = runBlocking {
        reset(discoverUseCase)
        whenever(discoverUseCase.execute(Unit)).thenReturn(
            flowOf(
                DiscoverOctoPrintUseCase.Result(
                    listOf(
                        baseOption,
                        baseOption.copy(label = "Terrier"),
                        baseOption.copy(label = "Beagle"),
                        baseOption.copy(label = "Dachshund"),
                    ).take(max)
                )
            )
        )
    }

    fun mockForTestEnvironment(vararg envs: PrinterConfigurationV3) = runBlocking {
        reset(discoverUseCase)
        whenever(discoverUseCase.execute(Unit)).thenReturn(
            flowOf(
                DiscoverOctoPrintUseCase.Result(
                    envs.map {
                        NetworkService(
                            label = it.label,
                            detailLabel = "Discovered via mock",
                            webUrl = it.webUrl.toString(),
                            origin = NetworkService.Origin.DnsSd,
                            originQuality = 100,
                            type = BackendType.OctoPrint,
                        )
                    }
                )
            )
        )
    }

    private val baseOption = NetworkService(
        label = "Frenchie",
        detailLabel = "Discovered via mock",
        webUrl = "https://frenchie.com",
        originQuality = 100,
        origin = NetworkService.Origin.DnsSd,
        type = BackendType.OctoPrint,
    )

    inner class MockBaseComponent(real: BaseComponent) : BaseComponent by real {
        override fun discoverOctoPrintUseCase(): DiscoverOctoPrintUseCase = discoverUseCase
    }
}