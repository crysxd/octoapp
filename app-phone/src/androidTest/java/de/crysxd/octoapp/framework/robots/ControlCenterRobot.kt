package de.crysxd.octoapp.framework.robots

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.UiDevice
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.tests.condition.waitForPass

object ControlCenterRobot {

    private val uiDevice get() = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    fun waitForControlCenterOpen() = waitForPass("Waiting for control center") {
        assert(uiDevice.hasObject(res(TestTags.ControlCenter.ContentEnabled))) { "Expected control center" }
    }

    fun activateInstance(config: PrinterConfigurationV3) = uiDevice.findObject(By.text(config.label)).click()
}