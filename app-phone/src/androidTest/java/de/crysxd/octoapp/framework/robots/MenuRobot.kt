package de.crysxd.octoapp.framework.robots

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.By.text
import androidx.test.uiautomator.UiDevice
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.R
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.tests.condition.waitFor
import de.crysxd.octoapp.tests.condition.waitForPass
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

object MenuRobot {

    private val uiDevice get() = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    fun openMenuWithMoreButton() = with(uiDevice) {
        val selector = res(TestTags.BottomBar.MainMenu)
        waitFor("Main menu button shown", timeout = 10.seconds) {
            findObject(selector) != null
        }

        // Button is not 90% visible, use custom actions to circumvent check
        findObject(selector).click()
        assert(hasObject(res(TestTags.Menu.ItemMainButton(getString(R.string.main_menu___item_show_printer))))) { "Expected Printer main menu button" }
        assert(hasObject(res(TestTags.Menu.ItemMainButton(getString(R.string.main_menu___item_show_settings))))) { "Expected Printer main menu button" }
        assert(hasObject(res(TestTags.Menu.ItemMainButton(SystemInfo.Interface.OctoPrint.label)))) { "Expected Printer main menu button" }
        assert(hasObject(res(TestTags.Menu.ItemMainButton(getString(R.string.main_menu___item_show_tutorials))))) { "Expected Printer main menu button" }
    }

    fun clickMainMenuButton(label: Int) {
        uiDevice.findObject(res(TestTags.Menu.ItemMainButton(getString(label)))).click()
    }

    fun clickMenuButton(label: Int, rightDetail: String? = null) {
        clickMenuButton(label = getString(label), rightDetail = rightDetail)
    }

    fun clickMenuButton(label: String, rightDetail: String? = null) {
        val uiObject = uiDevice.findObject(createSelector(label = label, rightDetail = rightDetail))
        assert(uiObject != null) { "Expected menu item with title '$label' ${if (rightDetail != null) "and right detail '$rightDetail'" else ""}" }
        uiObject.click()
    }

    fun assertMenuTitle(title: Int) = waitForPass("Waiting for menu title", 3.seconds) {
        val titleObject = uiDevice.findObject(res(TestTags.Menu.Title))
        assert(titleObject?.text == getString(title)) { "Expected menu title to be '${getString(title)}' but was '${titleObject?.text}'" }
    }

    fun assertMenuItem(label: Int, rightDetail: String? = null, visible: Boolean = true) {
        assertMenuItem(label = getString(label), rightDetail = rightDetail, visible = visible)
    }

    fun assertMenuItem(label: String, rightDetail: String? = null, visible: Boolean = true) = waitForPass("Waiting for menu item", 1.seconds) {
        val result = uiDevice.hasObject(createSelector(label = label, rightDetail = rightDetail))
        assert(result == visible) { "Expected${if (!visible) " no " else " "}menu item with title '$label' ${if (rightDetail != null) "and right detail '$rightDetail'" else ""}" }
    }

    fun waitForMenuToBeClosed() {
        waitForPass("Menu closed", timeout = 500.milliseconds) {
            assert(!uiDevice.hasObject(res(TestTags.Menu.MenuSheet))) { "Expected menu to be gone" }
        }
    }

    private fun createSelector(label: String, rightDetail: String?) = if (rightDetail != null) {
        res(TestTags.Menu.Item)
            .hasDescendant(text(label))
            .hasDescendant(text(rightDetail))
    } else {
        res(TestTags.Menu.Item)
            .hasDescendant(text(label))
    }
}