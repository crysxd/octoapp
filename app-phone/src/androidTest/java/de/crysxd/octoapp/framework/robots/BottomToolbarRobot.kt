package de.crysxd.octoapp.framework.robots

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.Direction
import androidx.test.uiautomator.UiDevice
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.condition.waitTime
import kotlin.time.Duration.Companion.seconds

object BottomToolbarRobot {

    fun confirmButtonWithSwipe(buttonId: String) = with(UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())) {
        findObject(res(buttonId)).click()
        waitTime(1000)
        waitForPass("Swipe to confirm", timeout = 5.seconds) {
            val track = findObject(res(TestTags.BottomBar.SwipeTrack))
            track.swipe(Direction.RIGHT, 1f, 2000)
        }
    }
}