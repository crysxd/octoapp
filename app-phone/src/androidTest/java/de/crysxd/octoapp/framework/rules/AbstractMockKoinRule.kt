package de.crysxd.octoapp.framework.rules

import android.app.Application
import androidx.test.platform.app.InstrumentationRegistry
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.initializeDependencyInjection
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.koin.core.module.Module

abstract class AbstractMockKoinRule : TestRule {

    protected abstract fun createMockModule(): Module

    override fun apply(base: Statement, description: Description) = object : Statement() {
        override fun evaluate() {
            val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application

            try {
                initializeDependencyInjection(app)
                SharedBaseInjector.get().getKoin().loadModules(
                    modules = listOf(createMockModule()),
                    allowOverride = true
                )
                base.evaluate()
            } finally {
                initializeDependencyInjection(app)
            }
        }
    }
}