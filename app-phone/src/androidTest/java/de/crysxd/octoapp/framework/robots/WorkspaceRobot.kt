package de.crysxd.octoapp.framework.robots

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.UiDevice
import de.crysxd.baseui.compose.framework.helpers.TestTags

object WorkspaceRobot {

    private val uiDevice get() = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    fun waitForConnectWorkspace() {
        uiDevice.wait({ it.hasObject(res(TestTags.Controls.StepConnect)) }, 15000)
    }


    fun waitForPrepareWorkspace() {
        uiDevice.wait({ it.hasObject(res(TestTags.Controls.StepPrepare)) }, 15000)
    }


    fun waitForPrintWorkspace() {
        uiDevice.wait({ it.hasObject(res(TestTags.Controls.StepPrint)) }, 15000)
    }
}