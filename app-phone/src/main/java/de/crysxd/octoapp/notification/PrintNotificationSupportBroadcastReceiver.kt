package de.crysxd.octoapp.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.wifi.WifiManager
import androidx.core.content.ContextCompat
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import kotlin.time.Duration.Companion.seconds


class PrintNotificationSupportBroadcastReceiver : BroadcastReceiver() {

    companion object {
        private const val TAG = "PrintNotificationSupportBroadcastReceiver"
        private var pauseJob: Job? = null
        const val ACTION_DISABLE_PRINT_NOTIFICATION_UNTIL_NEXT_LAUNCH = "de.crysxd.octoapp.ACTION_DISABLE_PRINT_NOTIFICATION_UNTIL_NEXT_LAUNCH"
        private var pendingConnectionCheck: Job? = null
    }

    fun install(context: Context) {
        val intentFilter = IntentFilter()
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
        intentFilter.addAction(Intent.ACTION_SCREEN_ON)
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF)
        intentFilter.addAction(ACTION_DISABLE_PRINT_NOTIFICATION_UNTIL_NEXT_LAUNCH)
        ContextCompat.registerReceiver(context, this, intentFilter, ContextCompat.RECEIVER_NOT_EXPORTED)
    }

    override fun onReceive(context: Context, intent: Intent) {
        AppScope.launch {
            Napier.v(tag = TAG, message = "Handling ${intent.action}")
            try {
                when (intent.action) {
                    WifiManager.NETWORK_STATE_CHANGED_ACTION -> handleConnectionChange(context)
                    Intent.ACTION_SCREEN_OFF -> handleScreenOff(context)
                    Intent.ACTION_SCREEN_ON -> handleScreenOn(context)
                    ACTION_DISABLE_PRINT_NOTIFICATION_UNTIL_NEXT_LAUNCH -> handleDisablePrintNotificationUntilNextLaunch(context)
                }
            } catch (e: Exception) {
                Napier.e(tag = TAG, message = "Failed to handle boradcast", throwable = e)
            }
        }
    }

    private fun handleDisablePrintNotificationUntilNextLaunch(context: Context) {
        LiveNotificationManager.pauseNotificationsUntilNextLaunch(context)
    }

    private suspend fun handleScreenOff(context: Context) {
        if (BaseInjector.get().octoPreferences().allowNotificationBatterySaver) {
            if (LiveNotificationManager.isNotificationShowing) {
                pauseJob = AppScope.launch {
                    val delaySecs = 5L
                    Napier.i(tag = TAG, message = "Screen off, sending live notification into hibernation in ${delaySecs}s")
                    delay(TimeUnit.SECONDS.toMillis(delaySecs))
                    Napier.i(tag = TAG, message = "Sending live notification into hibernation")
                    LiveNotificationManager.hibernate(context)
                }
            }
        } else {
            Napier.d(tag = TAG, message = "Battery saver disabled, no action on screen off")
        }
    }

    private fun handleScreenOn(context: Context) {
        if (BaseInjector.get().octoPreferences().allowNotificationBatterySaver) {
            pauseJob?.let {
                pauseJob = null
                Napier.i(tag = TAG, message = "Cancelling notification hibernation")
                it.cancel()
            }
            LiveNotificationManager.wakeUp(context)
        } else {
            Napier.d(tag = TAG, message = "Battery saver disabled, no action on screen on")
        }
    }

    private suspend fun handleConnectionChange(context: Context) {
        val wasDisconnected = BaseInjector.get().octoPreferences().wasPrintNotificationDisconnected
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        @Suppress("DEPRECATION")
        val hasWifi = manager.allNetworks.map { manager.getNetworkCapabilities(it) }.any { it?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) == true }
        val connectDelayMs = 5000L

        // If WiFi got reconnected, the local URL could also be reachable again. Perform online check.
        withContext(Dispatchers.IO) {
            pendingConnectionCheck?.cancel()
            pendingConnectionCheck = launch {
                delay(2.seconds)
                Napier.i(tag = TAG, message = "Notifying of connectivity change")
                BaseInjector.get().octoPrintProvider().printer().notifyConnectionChange()
            }
        }

        if (wasDisconnected && hasWifi) {
            BaseInjector.get().octoPreferences().wasPrintNotificationDisconnected = false
            Napier.i(tag = TAG, message = "Network changed. Print notification was disconnected before, attempting to reconnect in ${connectDelayMs / 1000}s")

            // Delay for 5s to get the network settled and then connect
            delay(connectDelayMs)
            LiveNotificationManager.start(context, trigger = "Connectivity changed")
        } else {
            Napier.v(tag = TAG, message = "Not starting service (wasDisconnected=$wasDisconnected, hasWifi=$hasWifi)")
        }
    }
}