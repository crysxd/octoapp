package de.crysxd.octoapp.notification

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.MainActivity.Companion.EXTRA_CLICK_URI
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.FcmPrintEvent
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.NotificationAESCipher
import de.crysxd.octoapp.notification.PrintState.Companion.DEFAULT_FILE_NAME
import de.crysxd.octoapp.notification.PrintState.Companion.DEFAULT_FILE_TIME
import de.crysxd.octoapp.notification.PrintState.Companion.DEFAULT_PROGRESS
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.seconds

class FcmNotificationService : FirebaseMessagingService() {

    private val tag = "FcmNotificationService"
    private val printIdPrefix = "pid:"
    private val lastEventPrefix = "lastEvent:"
    private val lastUpdatePrefix = "lastUpdate:"
    private val cipher = NotificationAESCipher()
    private val prefs by lazy { getSharedPreferences("print_id_cache", Context.MODE_PRIVATE) }
    private val notificationController by lazy { PrintNotificationController.instance }
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Napier.e(tag = tag, throwable = throwable, message = "Exception in scope")
    }

    override fun onCreate() {
        super.onCreate()
        prefs.edit {
            prefs.all
                .filter { it.key.startsWith(printIdPrefix) }
                .filter { it.value is Long }
                .filter { (System.currentTimeMillis() - (it.value as Long)) > 14.days.inWholeMilliseconds }
                .forEach { remove(it.key) }
        }
    }

    private fun getLastEventTime(instanceId: String) = Instant.fromEpochMilliseconds(prefs.getLong("$lastEventPrefix$instanceId", 0))
    private fun setLastEventTime(instanceId: String, value: Instant) = prefs.edit { putLong("$lastEventPrefix$instanceId", value.toEpochMilliseconds()) }
    private fun getLastUpdateTime(instanceId: String) = Instant.fromEpochMilliseconds(prefs.getLong("$lastUpdatePrefix$instanceId", 0))
    private fun setLastUpdateTime(instanceId: String, value: Instant) = prefs.edit { putLong("$lastUpdatePrefix$instanceId", value.toEpochMilliseconds()) }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Napier.i(tag = tag, message = "New token: $token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Napier.i(tag = tag, message = "Received message, handling async")
        onMessageReceivedAsync(message)
    }

    private fun onMessageReceivedAsync(message: RemoteMessage) = AppScope.launch(Dispatchers.IO + exceptionHandler) {
        notificationController.ensureNotificationChannelCreated()
        try {
            message.data["raw"]?.let {
                handleRawDataEvent(
                    instanceId = message.data["instanceId"] ?: throw IllegalArgumentException("Not instance id"),
                    raw = it,
                    sentTime = Instant.fromEpochMilliseconds(message.sentTime),
                )
            }

            if (message.priority != message.originalPriority) {
                Napier.e(tag = tag, message = "Detected message de-prioritization:  ${message.priority} != ${message.originalPriority}")
            }

            message.notification?.let {
                handleNotification(it, message.data[EXTRA_CLICK_URI])
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Error handling FCM push notification", throwable = e)
        }
    }

    private fun handleNotification(notification: RemoteMessage.Notification, contextUri: String?) {
        Napier.i(tag = tag, message = "Showing notification")
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = getString(R.string.updates_notification_channel)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setContentText(notification.body)
            .setContentTitle(notification.title)
            .setSmallIcon(R.drawable.ic_notification_default)
            .setAutoCancel(true)
            .setColorized(true)
            .setColor(ContextCompat.getColor(this, R.color.primary_dark))
            .setContentIntent(
                PendingIntent.getActivity(
                    this,
                    notification.title.hashCode(),
                    Intent(this, MainActivity::class.java).also { it.putExtra(EXTRA_CLICK_URI, contextUri) },
                    PendingIntent.FLAG_IMMUTABLE
                )
            )
        manager.notify(BaseInjector.get().notificationIdRepository().nextUpdateNotificationId(), notificationBuilder.build())
    }

    private suspend fun handleRawDataEvent(instanceId: String, raw: String, sentTime: Instant) {
        Napier.i(tag = tag, message = "Received message with raw data for instance: $instanceId")

        // Check if for active instance or feature enabled
        val isForActive = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.id == instanceId
        if (!isForActive && !BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
            Napier.i(tag = tag, message = "Dropping message for $instanceId as it's not the active instance and quick switch is disabled")
            return
        }

        // Parse data
        val data = cipher.parseRawData(instanceId = instanceId, raw = raw) ?: return

        // Beep is special, we don't need to check the time
        when (data.type) {
            FcmPrintEvent.Type.Beep -> notificationController.notifyBeep(instanceId = instanceId)
            FcmPrintEvent.Type.FirstLayerDone -> notificationController.notifyLayerCompletion(instanceId = instanceId, layer = 1)
            FcmPrintEvent.Type.ThirdLayerDone -> notificationController.notifyLayerCompletion(instanceId = instanceId, layer = 3)
            FcmPrintEvent.Type.Custom -> notificationController.notifyCustom(instanceId = instanceId, message = data.message ?: "Gcode notification")
            else -> Unit
        }

        // Ensure print isn't over
        if (data.printId != null && data.type in listOf(FcmPrintEvent.Type.Idle, FcmPrintEvent.Type.Completed, FcmPrintEvent.Type.Error)) {
            Napier.i(tag = tag, message = "Marking ${data.printId} as ended")
            prefs.edit { putLong("$printIdPrefix${data.printId}", System.currentTimeMillis()) }
        } else if (prefs.contains("$printIdPrefix${data.printId}")) {
            Napier.i(tag = tag, message = "Print ${data.printId} is already over, dropping notification")
            return
        }

        // Ensure we don't post old data
        val serverTime = data.serverTimeFixed
        fun doTimeCheck(get: (String) -> Instant, set: (String, Instant) -> Unit): Boolean {
            val previousLast = get(instanceId)
            return if (previousLast > serverTime) {
                Napier.i(tag = tag, message = "Skipping update, last server time was $previousLast which is after $serverTime")
                false
            } else {
                Napier.i(tag = tag, message = "$serverTime after $previousLast")
                set(instanceId, serverTime)
                true
            }
        }

        val useNotification = when (data.type) {
            FcmPrintEvent.Type.Printing -> doTimeCheck(::getLastUpdateTime, ::setLastUpdateTime) && doTimeCheck(::getLastEventTime, ::setLastEventTime)
            else -> doTimeCheck(::getLastEventTime, ::setLastEventTime)
        }

        if (!useNotification) return

        // Handle event
        when (data.type) {
            FcmPrintEvent.Type.Completed -> notificationController.notifyCompleted(
                instanceId = instanceId,
                printState = data.toPrintState(sentTime) ?: return
            )

            FcmPrintEvent.Type.Mmu2FilamentSelectionStarted -> notificationController.notifyFilamentSelectionRequired(
                instanceId = instanceId,
                printState = data.toPrintState(sentTime) ?: return
            )

            FcmPrintEvent.Type.Mmu2FilamentSelectionCompleted -> notificationController.notifyFilamentSelectionCompleted(
                instanceId = instanceId,
                printState = data.toPrintState(sentTime) ?: return
            )

            FcmPrintEvent.Type.FilamentRequired -> notificationController.notifyFilamentRequired(
                instanceId = instanceId,
                printState = data.toPrintState(sentTime) ?: return
            )

            FcmPrintEvent.Type.Idle,
            FcmPrintEvent.Type.Error -> notificationController.notifyIdle(
                instanceId = instanceId
            )

            FcmPrintEvent.Type.Paused,
            FcmPrintEvent.Type.Printing,
            FcmPrintEvent.Type.FirstLayerDone,
            FcmPrintEvent.Type.ThirdLayerDone -> notificationController.update(
                instanceId = instanceId,
                printState = data.toPrintState(sentTime),
                autoCancelOtherNotification = true,
            )

            FcmPrintEvent.Type.PausedFromGcode -> notificationController.notifyPausedFromGcode(
                instanceId = instanceId,
                printState = data.toPrintState(sentTime) ?: return
            )

            null,
            FcmPrintEvent.Type.Custom,
            FcmPrintEvent.Type.Beep -> Napier.d(tag = tag, message = "Not handling notification of type ${data.type} for progress update")
        }.hashCode()
    }

    private fun FcmPrintEvent.toPrintState(sentTime: Instant): PrintState? {
        return PrintState(
            source = PrintState.Source.Remote,
            progress = progress ?: DEFAULT_PROGRESS,
            appTime = Clock.System.now(),
            sourceTime = sentTime,
            fileName = fileName ?: DEFAULT_FILE_NAME,
            fileDate = DEFAULT_FILE_TIME,
            eta = timeLeft?.let { sentTime + it.seconds },
            state = when (type) {
                FcmPrintEvent.Type.Mmu2FilamentSelectionStarted,
                FcmPrintEvent.Type.Mmu2FilamentSelectionCompleted,
                FcmPrintEvent.Type.Printing -> PrintState.State.Printing

                FcmPrintEvent.Type.Paused -> PrintState.State.Paused
                FcmPrintEvent.Type.PausedFromGcode -> PrintState.State.Paused
                FcmPrintEvent.Type.FilamentRequired -> PrintState.State.Paused
                FcmPrintEvent.Type.Completed -> PrintState.State.Idle
                FcmPrintEvent.Type.Error -> PrintState.State.Idle
                FcmPrintEvent.Type.Idle -> PrintState.State.Idle
                else -> return null
            }
        )
    }
}