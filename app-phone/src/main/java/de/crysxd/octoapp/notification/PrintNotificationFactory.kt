package de.crysxd.octoapp.notification

import android.app.NotificationChannel
import android.app.NotificationChannelGroup
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Icon
import android.media.AudioAttributes
import android.net.Uri
import androidx.compose.ui.graphics.toArgb
import androidx.core.app.NotificationCompat
import de.crysxd.baseui.ext.toCompose
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsEta
import de.crysxd.octoapp.widgets.createLaunchAppIntent
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

class PrintNotificationFactory(
    context: Context,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val octoPreferences: OctoPreferences,
) : ContextWrapper(context) {

    companion object {
        private const val TAG = "PrintNotificationFactory"
        private const val OCTOPRINT_CHANNEL_PREFIX = "octoprint_"
        private const val OCTOPRINT_CHANNEL_GROUP_ID = "octoprint"
        private const val FILAMENT_CHANGE_CHANNEL_ID = "filament_change"
        private const val BEEP_CHANNEL_ID = "beep"
        private const val LAYER_COMPLETION_CHANNEL_ID = "layer"
        private const val CUSTOM_CHANNEL_ID = "custom"
        private const val PAUSE_FROM_GCODE_CHANNEL_ID = "pause"
        private const val PROGRESS_100 = 1000
        private const val PROGRESS_99 = 990
        private val snapshotCache = mutableMapOf<String, Pair<Bitmap, Instant>>()
    }

    private val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val liveNotificationDeletedIntent by lazy {
        val intent = Intent(this, LiveNotificationDeletedReceiver::class.java)
        PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
    }

    fun createNotificationChannels() {
        Napier.i(tag = TAG, message = "Creating notification channels")
        // Delete legacy channel and channels for deleted instances
        notificationManager.deleteNotificationChannel("print_progress")
        notificationManager.notificationChannels.filter { it.id.startsWith(OCTOPRINT_CHANNEL_PREFIX) }.forEach {
            val instance = printerConfigurationRepository.get(it.id.removePrefix(OCTOPRINT_CHANNEL_PREFIX))
            if (instance == null) {
                notificationManager.deleteNotificationChannel(it.id)
            }
        }

        // Create OctoPrint group
        notificationManager.createNotificationChannelGroup(
            NotificationChannelGroup(OCTOPRINT_CHANNEL_GROUP_ID, getString(R.string.notification_channel___print_progress))
        )

        // Create missing notification channels
        printerConfigurationRepository.getAll().forEach {
            if (!notificationManager.notificationChannels.any { c -> c.id == it.channelId }) {
                createNotificationChannel(
                    name = it.label,
                    vibrationPattern = arrayOf(0L), // Needed for Wear OS. Otherwise every percent change vibrates.
                    id = it.channelId,
                    groupId = OCTOPRINT_CHANNEL_GROUP_ID,
                    soundUri = Uri.parse("android.resource://${packageName}/${R.raw.notification_print_done}"),
                )
            }
        }

        // Create filament change channel
        createNotificationChannel(
            id = FILAMENT_CHANGE_CHANNEL_ID,
            soundUri = Uri.parse("android.resource://${packageName}/${R.raw.notification_filament_change}"),
            name = getString(R.string.notification_channel___filament_change),
        )
        createNotificationChannel(
            id = PAUSE_FROM_GCODE_CHANNEL_ID,
            soundUri = Uri.parse("android.resource://${packageName}/${R.raw.notification_filament_change}"),
            name = getString(R.string.notification_channel___pause_by_gcode),
        )
        createNotificationChannel(
            id = BEEP_CHANNEL_ID,
            soundUri = Uri.parse("android.resource://${packageName}/${R.raw.notification_filament_change}"),
            name = getString(R.string.notification_channel___beep),
        )
        createNotificationChannel(
            id = LAYER_COMPLETION_CHANNEL_ID,
            soundUri = Uri.parse("android.resource://${packageName}/${R.raw.notification_filament_change}"),
            name = getString(R.string.notification_channel___layer_completion),
        )
        createNotificationChannel(
            id = CUSTOM_CHANNEL_ID,
            soundUri = Uri.parse("android.resource://${packageName}/${R.raw.notification_filament_change}"),
            name = getString(R.string.notification_channel___custom),
        )
    }

    private fun createNotificationChannel(
        name: String,
        id: String,
        groupId: String? = null,
        soundUri: Uri? = null,
        vibrationPattern: Array<Long>? = null,
        audioAttributes: AudioAttributes? = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build(),
        importance: Int = NotificationManager.IMPORTANCE_HIGH,
    ) = notificationManager.createNotificationChannel(
        NotificationChannel(id, name, importance).also { nc ->
            nc.group = groupId
            vibrationPattern?.let {
                nc.vibrationPattern = it.toLongArray()
            }
            soundUri?.let { uri ->
                audioAttributes?.let { attrs ->
                    nc.setSound(uri, attrs)
                }
            }
        }
    )

    fun createServiceNotification(
        instanceInformation: PrinterConfigurationV3?,
        statusText: String
    ) = createNotificationBuilder(
        instanceInformation = instanceInformation,
        notificationChannelId = instanceInformation?.channelId ?: getString(R.string.updates_notification_channel)
    ).setContentTitle(statusText)
        .setContentText(instanceInformation?.label ?: "OctoApp")
        .setSilent(true)
        .addStopLiveAction(PrintState.Source.Live)
        .build()
        .also {
            Napier.i(tag = TAG, message = "Creating service notification on channel ${instanceInformation?.channelId}: statusText=$statusText")
        }

    private fun createStatusNotification(
        instanceId: String,
        printState: PrintState,
        stateText: String?,
        snapshot: Bitmap? = null,
        doLog: Boolean = false,
    ) = printerConfigurationRepository.get(instanceId)?.let { config ->
        val text = printState.notificationText(config)
        val title = printState.notificationTitle(stateText)
        val progress = (PROGRESS_100 * (printState.progress / 100f)).toInt()

        if (doLog) {
            Napier.i(
                tag = TAG,
                message = "Creating status notification on channel ${config.channelId}: title=$title text=$text stateText=$stateText progress=$progress"
            )
        }

        createNotificationBuilder(instanceInformation = config, notificationChannelId = config.channelId)
            .setContentTitle(if (usesDetails) title else "$title, $text")
            .run { if (usesDetails || snapshot == null) setContentText(text) else this }
            .run { if (usesProgressBar) setProgress(PROGRESS_100, progress, false) else this }
            .setDeleteIntent(if (printState.source == PrintState.Source.Live) liveNotificationDeletedIntent else null)
            .addStopLiveAction(printState.source)
            .setOngoing(PROGRESS_99 > progress)
            .setSilent(true)
            .setVibrate(arrayOf(0L).toLongArray())
            .run {
                if (usesSnapshots && snapshot != null) {
                    setStyle(
                        NotificationCompat.BigPictureStyle()
                            .bigPicture(snapshot)
                            .bigLargeIcon(null as Icon?)
                    )
                } else {
                    this
                }
            }
            .extend(NotificationCompat.WearableExtender().setBridgeTag(getString(R.string.print_status_bridge_tag)))
            .build()
    }

    suspend fun createStatusNotification(
        instanceId: String,
        printState: PrintState,
        stateText: String?,
        doLog: Boolean = false,
    ) = createStatusNotification(
        instanceId = instanceId,
        printState = printState,
        stateText = stateText,
        snapshot = getSnapshot(instanceId),
        doLog = doLog
    )

    fun createStatusNotificationWithoutSnapshot(
        instanceId: String,
        printState: PrintState,
        stateText: String?,
        doLog: Boolean = false,
    ) = createStatusNotification(
        instanceId = instanceId,
        printState = printState,
        stateText = stateText,
        snapshot = null,
        doLog = doLog
    )

    fun createFilamentChangeNotification(
        instanceId: String,
    ) = printerConfigurationRepository.get(instanceId)?.let {
        createNotificationBuilder(instanceInformation = it, notificationChannelId = FILAMENT_CHANGE_CHANNEL_ID)
            .setContentTitle(getString(R.string.print_notification___filament_change_required_title, it.label))
            .setContentText(getString(R.string.print_notification___filament_change_required_message))
            .setAutoCancel(true)
            .build()
    }

    fun createPausedFromGcodeNotification(
        instanceId: String,
    ) = printerConfigurationRepository.get(instanceId)?.let {
        createNotificationBuilder(instanceInformation = it, notificationChannelId = PAUSE_FROM_GCODE_CHANNEL_ID)
            .setContentTitle(getString(R.string.print_notification___paused_from_gcode_title, it.label))
            .setContentText(getString(R.string.print_notification___paused_from_gcode_message))
            .setAutoCancel(true)
            .build()
    }

    fun createBeepNotification(
        instanceId: String,
    ) = printerConfigurationRepository.get(instanceId)?.let {
        createNotificationBuilder(instanceInformation = it, notificationChannelId = BEEP_CHANNEL_ID)
            .setContentTitle(getString(R.string.print_notification___beep_title))
            .setContentText(getString(R.string.print_notification___beep_message, it.label))
            .setAutoCancel(true)
            .build()
    }

    fun createLayerCompletionNotification(
        instanceId: String,
        layer: Int,
    ) = printerConfigurationRepository.get(instanceId)?.let {
        createNotificationBuilder(instanceInformation = it, notificationChannelId = LAYER_COMPLETION_CHANNEL_ID)
            .setContentTitle(getString(R.string.print_notification___layer_x_completed_title, layer))
            .setContentText(getString(R.string.print_notification___layer_x_completed_message, it.label))
            .setAutoCancel(true)
            .build()
    }

    fun createCustomNotification(
        instanceId: String,
        text: String,
    ) = printerConfigurationRepository.get(instanceId)?.let {
        createNotificationBuilder(instanceInformation = it, notificationChannelId = CUSTOM_CHANNEL_ID)
            .setContentTitle(text)
            .setContentText(getString(R.string.print_notification___layer_x_completed_message, it.label))
            .setAutoCancel(true)
            .build()
    }

    fun createFilamentSelectionNotification(
        instanceId: String,
    ) = printerConfigurationRepository.get(instanceId)?.let {
        createNotificationBuilder(instanceInformation = it, notificationChannelId = it.channelId)
            .setContentTitle(getString(R.string.print_notification___filament_change_required_title, it.label))
            .setContentText(getString(R.string.print_notification___filament_change_required_message))
            .setAutoCancel(true)
            .build()
    }

    suspend fun createPrintCompletedNotification(
        instanceId: String,
        printState: PrintState
    ) = printerConfigurationRepository.get(instanceId)?.let {
        val snapshot = getSnapshot(instanceId)
        createNotificationBuilder(instanceInformation = it, notificationChannelId = it.channelId)
            .setContentTitle(getString(R.string.print_notification___print_done_title, it.label))
            .setContentText(printState.fileName)
            .setAutoCancel(true)
            .setStyle(
                NotificationCompat.BigPictureStyle()
                    .bigPicture(snapshot)
                    .bigLargeIcon(null as Icon?)
            )
            .build()
    }

    private fun createNotificationBuilder(
        instanceInformation: PrinterConfigurationV3?,
        notificationChannelId: String
    ) = NotificationCompat.Builder(this, notificationChannelId)
        .setSmallIcon(R.drawable.ic_notification_default)
        .setContentIntent(createLaunchAppIntent(this, instanceInformation?.id))
        .also {
            val darkMode = resources.getBoolean(R.bool.night_mode)
            instanceInformation.colors.let { colors ->
                it.setColorized(true)
                it.color = (if (darkMode) colors.dark.main else colors.light.main).toCompose().toArgb()
            }
        }

    private fun PrintState.notificationTitle(stateText: String?): String {
        val title = when (state) {
            PrintState.State.Printing -> getString(R.string.print_notification___printing_title, progress)
            PrintState.State.Pausing -> getString(R.string.print_notification___pausing_title)
            PrintState.State.Paused -> getString(R.string.print_notification___paused_title)
            PrintState.State.Cancelling -> getString(R.string.print_notification___cancelling_title)
            PrintState.State.Idle -> ""
        }

        return stateText?.let {
            "$title ($stateText)"
        } ?: title
    }

    private fun isMultiPrinterActive() = printerConfigurationRepository.getAll().size > 1 && BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)

    private fun PrintState.notificationText(instanceInformation: PrinterConfigurationV3) = listOfNotNull(
        getString(R.string.print_notification___live)
            .takeIf { source == PrintState.Source.Live && !isMultiPrinterActive() },
        getString(R.string.print_notification___live_on_x, instanceInformation.label)
            .takeIf { source == PrintState.Source.Live && isMultiPrinterActive() },
        instanceInformation.label
            .takeIf { source != PrintState.Source.Live && isMultiPrinterActive() },
        eta?.let {
            val secsLeft = (it - Clock.System.now()).inWholeSeconds
            secsLeft.formatAsSecondsEta(
                showLabel = true,
                allowRelative = false,
                useCompactFutureDate = BaseInjector.get().octoPreferences().progressWidgetSettings.etaStyle == ProgressWidgetSettings.EtaStyle.Compact
            )
        }
    ).joinToString()

    private suspend fun getSnapshot(instanceId: String): Bitmap? {
        val cachedSnapshot = snapshotCache[instanceId]?.takeIf { (_, date) -> (Clock.System.now() - date) < 5.seconds }?.first
        return when {
            !usesSnapshots -> null
            cachedSnapshot != null -> cachedSnapshot
            else -> try {
                withTimeoutOrNull(5.seconds) {
                    BaseInjector.get().getWebcamSnapshotUseCase2().execute(
                        param = GetWebcamSnapshotUseCase.Params(
                            instanceId = instanceId,
                            maxSize = 1280,
                            illuminateIfPossible = true,
                            webcamIndex = 0,
                            interval = null
                        )
                    ).first()
                }?.bitmap?.also {
                    snapshotCache[instanceId] = it to Clock.System.now()
                }
            } catch (e: Exception) {
                Napier.i(tag = TAG, message = "Failed to fetch snapshot for $instanceId")
                null
            }
        }
    }

    private val usesSnapshots
        get() = octoPreferences.notificationsLayout in listOf(
            OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_PROGRESS,
            OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_SUBTITLE
        )

    private val usesDetails
        get() = octoPreferences.notificationsLayout in listOf(
            OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SUBTITLE_PROGRESS,
            OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_SUBTITLE
        )

    private val usesProgressBar
        get() = octoPreferences.notificationsLayout in listOf(
            OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SUBTITLE_PROGRESS,
            OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_PROGRESS
        )

    private val PrinterConfigurationV3.channelId get() = "$OCTOPRINT_CHANNEL_PREFIX${id}"

    private fun NotificationCompat.Builder.addStopLiveAction(source: PrintState.Source) = addAction(
        NotificationCompat.Action.Builder(
            null,
            getString(R.string.print_notification___close),
            PendingIntent.getBroadcast(
                this@PrintNotificationFactory,
                0,
                Intent(
                    this@PrintNotificationFactory,
                    PrintNotificationSupportBroadcastReceiver::class.java
                ).setAction(
                    PrintNotificationSupportBroadcastReceiver.ACTION_DISABLE_PRINT_NOTIFICATION_UNTIL_NEXT_LAUNCH
                ),
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        ).build()
    ).setDeleteIntent(if (source == PrintState.Source.Live) liveNotificationDeletedIntent else null)

}
