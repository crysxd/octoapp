package de.crysxd.octoapp.notification

import android.app.BackgroundServiceStartNotAllowedException
import android.app.ForegroundServiceStartNotAllowedException
import android.content.Context
import android.content.Intent
import android.os.Build
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

object LiveNotificationManager {

    private const val tag = "LiveNotificationManager"
    val isNotificationEnabled
        get() = BaseInjector.get().octoPreferences().isLivePrintNotificationsEnabled &&
                !BaseInjector.get().octoPreferences().wasPrintNotificationDisabledUntilNextLaunch
    val isNotificationShowing get() = startTime > 0
    internal var startTime = 0L
        set(value) {
            if (value == 0L) isHibernating = false
            field = value
        }
    private var isHibernating = false
    private var stopJob: Job? = null

    fun pauseNotificationsUntilNextLaunch(context: Context) {
        Napier.i(tag = tag, message = "Stopping notification until next launch")
        BaseInjector.get().octoPreferences().wasPrintNotificationDisabledUntilNextLaunch = true
        stop(context)
        PrintNotificationController.instance.cancelUpdateNotifications()
    }

    fun restartIfWasPaused(context: Context) {
        if (BaseInjector.get().octoPreferences().wasPrintNotificationDisabledUntilNextLaunch) {
            Napier.i(tag = tag, message = "Restarting notification after it was paused")
            BaseInjector.get().octoPreferences().wasPrintNotificationDisabledUntilNextLaunch = false
            start(context, trigger = "Restart after pause")
        }
    }

    fun start(context: Context, trigger: String) {
        try {
            // Guard: We can't start the service without an active instance
            BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
                ?: return Napier.w(tag = tag, message = "Rejecting start of service: No active instance (trigger=$trigger)")

            if (isNotificationEnabled) {
                // Already running?
                if (isHibernating) {
                    Napier.i(tag = tag, message = "Service is hibernating, waking up (trigger=$trigger)")
                    wakeUp(context)
                } else if (!isNotificationShowing) {
                    startTime = System.currentTimeMillis()
                    stopJob?.cancel()
                    val intent = Intent(context, LiveNotificationService::class.java)
                    Napier.i(tag = tag, message = "Starting notification service as foreground (trigger=$trigger)")
                    context.startForegroundService(intent)
                }
            } else {
                Napier.v(tag = tag, message = "Skipping notification service start, disabled (trigger=$trigger)")
            }
        } catch (e: Exception) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && e is BackgroundServiceStartNotAllowedException) {
                Napier.w(tag = tag, message = "Background service not allowed, can't start")
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && e is ForegroundServiceStartNotAllowedException) {
                Napier.w(tag = tag, message = "Foreground service not allowed, can't start")
            } else {
                Napier.w(tag = tag, message = "Failed to start service", throwable = e)
            }
        }
    }

    fun stop(context: Context) {
        if (stopJob?.isActive == true) return

        // We have issues with starting the service and then stopping it right after. After we started it as a foreground service,
        // we need to give it time to start and call startForeground(). Without this call being done, the app will crash, even if the service is already stopped
        //
        // Also we need to delay for at least CHECK_PRINT_ENDED_DELAY and 500ms extra to let the service wind down properly
        stopJob = AppScope.launch {
            val delay = LiveNotificationService.CHECK_PRINT_ENDED_DELAY + 500
            if (isNotificationShowing) {
                // Only print if we think we are showing to prevent spam :) But always double and triple stop to be sure it's stopped...
                Napier.i(tag = tag, message = "Stopping notification service after delay of ${delay}ms")
            }
            delay(delay)
            SharedBaseInjector.get().printerConfigRepository.getActiveInstanceSnapshot()?.id?.let { instanceId ->
                PrintNotificationController.instance.notifyIdle(instanceId)
            }
            val intent = Intent(context, LiveNotificationService::class.java)
            context.stopService(intent)
            startTime = 0
        }
    }

    fun restart(context: Context) = AppScope.launch {
        if (isNotificationShowing) {
            stop(context)
            stopJob?.join()
            start(context, trigger = "Restart")
        }
    }

    fun hibernate(context: Context) = runCatchingServiceExceptions {
        val isHibernationEnabled = BaseInjector.get().octoPreferences().allowNotificationBatterySaver
        if (isHibernationEnabled && isNotificationShowing) {
            Napier.i(tag = tag, message = "Sending service into hibernation")
            isHibernating = true
            val intent = Intent(context, LiveNotificationService::class.java)
            intent.action = LiveNotificationService.ACTION_HIBERNATE
            context.startService(intent)
        }
    }

    fun wakeUp(context: Context) = runCatchingServiceExceptions {
        if (isHibernating) {
            Napier.i(tag = tag, message = "Resuming service")
            isHibernating = false
            val intent = Intent(context, LiveNotificationService::class.java)
            intent.action = LiveNotificationService.ACTION_WAKE_UP
            context.startService(intent)
        }
    }

    private fun runCatchingServiceExceptions(block: () -> Unit) = try {
        block()
    } catch (e: java.lang.Exception) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && e is BackgroundServiceStartNotAllowedException) {
            Napier.w(tag = tag, message = "Unable to perform hibernation, background service not allowed")
        } else {
            throw e
        }
    }
}