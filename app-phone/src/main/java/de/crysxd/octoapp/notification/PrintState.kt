package de.crysxd.octoapp.notification

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class PrintState(
    val fileDate: Long,
    val fileName: String,
    val source: Source,
    val state: State,
    val progress: Float,
    val sourceTime: Instant,
    val appTime: Instant,
    val eta: Instant?,
) {

    companion object {
        const val DEFAULT_FILE_TIME = 0L
        const val DEFAULT_FILE_NAME = "unknown"
        const val DEFAULT_PROGRESS = 0f
    }

    val objectId get() = "$fileDate$fileName"

    enum class State {
        Printing,
        Pausing,
        Paused,
        Idle,
        Cancelling,
    }

    enum class Source {
        Live,
        CachedLive,
        Remote,
        CachedRemote;

        val asCached
            get() = when (this) {
                Live, CachedLive -> CachedLive
                Remote, CachedRemote -> CachedRemote
            }
    }
}