package de.crysxd.octoapp

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.map
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.google.firebase.analytics.FirebaseAnalytics
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.baseui.ext.launchWhenResumedFixed
import de.crysxd.baseui.ext.launchWhenStartedFixed
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.purchase.PurchaseConfirmationDialog
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.models.BillingEvent
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.OBICO_APP_PORTAL_CALLBACK_PATH
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.utils.BenchmarkIntentHandler
import de.crysxd.octoapp.databinding.MainActivityBinding
import de.crysxd.octoapp.engine.exceptions.WebSocketUpgradeFailedException
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.notification.LiveNotificationManager
import de.crysxd.octoapp.pluginsupport.mmu2filamentselect.Mmu2FilamentSelectSupportFragment
import de.crysxd.octoapp.pluginsupport.ngrok.NgrokSupportFragment
import de.crysxd.octoapp.pluginsupport.octolapse.OctolapseSupportFragment
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.signin.di.SignInInjector
import de.crysxd.octoapp.widgets.updateAllWidgets
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds
import de.crysxd.octoapp.engine.models.event.Message as SocketMessage

class MainActivity : OctoActivity() {

    companion object {
        const val EXTRA_TARGET_OCTOPRINT_ID = "octoprint_id"
        const val EXTRA_CLICK_URI = "clickUri"
    }

    private val tag = "MainActivity"
    private val minSameFlagsToBeSure = 2
    private lateinit var binding: MainActivityBinding
    private val preferences = BaseInjector.get().octoPreferences()
    private val viewModel by lazy { ViewModelProvider(this)[MainActivityViewModel::class.java] }
    override val lastInsets = Rect()
    override val rootLayout by lazy { binding.coordinator }
    override val navController get() = findNavController(R.id.mainNavController)
    private var enforceAutoamticNavigationAllowed = false
    private var uiStoppedAt: Long? = null
    private var updateCapabilitiesJob: Job? = null
    private var purchaseEventJob: Job? = null
    private val handler = Handler(Looper.getMainLooper())
    private var anrScope: CoroutineScope? = null
    private var insetJob: Job? = null
    private val anrDetector = AnrDetector()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        delegate.applyDayNight()

        // We need to call this before nav component grabs any link. onNewIntent
        // handles any links and removes them from the intent
        onNewIntent(intent)

        binding = MainActivityBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        // Fix fullscreen layout under system bars for frame layout
        rootLayout.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

        // Observe events, will update when instance changes
        BaseInjector.get().octoPrintProvider().eventFlow("MainActivity@events")
            .asLiveData()
            .map { it }
            .observe(this, ::onEventReceived)


        @OptIn(ExperimentalCoroutinesApi::class)
        SignInInjector.get().octoprintRepository().instanceInformationFlow()
            .flatMapLatest { instance ->
                if (instance == null) return@flatMapLatest emptyFlow()

                BaseInjector.get().octoPrintProvider().passiveCurrentMessageFlow(tag = "MainActivity@currentMessage", instanceId = instance.id)
                    .map { it.state.flags }
                    .filterWithRepeat()
                    .onEach { onCurrentMessageReceived(it, instance.id) }
            }
            .asLiveData()
            .observe(this) {
                // Nothing
            }

        lifecycleScope.launchWhenStartedFixed {
            val navHost = supportFragmentManager.findFragmentById(R.id.mainNavController) as NavHostFragment

            navController.addOnDestinationChangedListener { _, destination, _ ->
                Napier.i(tag = tag, message = "Navigated to ${destination.label}")

                // Sometimes it seems the UI is restored but the VM still remembers the last navigation, causing the user to be stuck on splash
                // Whenever we are on splash, we remove lastNavigation so the user is properly navigated again
                if (destination.id == R.id.splashFragment) {
                    Napier.i(tag = tag, message = "Restoring navigation to ${viewModel.lastNavigation} or ${viewModel.pendingFlagNavigation}")
                    viewModel.pendingNavigation = viewModel.lastNavigation ?: viewModel.pendingFlagNavigation
                    viewModel.lastNavigation = null
                }

                OctoAnalytics.logEvent(OctoAnalytics.Event.ScreenShown, mapOf(FirebaseAnalytics.Param.SCREEN_NAME to destination.label?.toString()))

                when (destination.id) {
                    R.id.discoverFragment -> OctoAnalytics.logEvent(OctoAnalytics.Event.LoginWorkspaceShown)
                    R.id.workspaceConnect -> OctoAnalytics.logEvent(OctoAnalytics.Event.ConnectWorkspaceShown)
                    R.id.workspacePrePrint -> OctoAnalytics.logEvent(OctoAnalytics.Event.PrePrintWorkspaceShown)
                    R.id.workspacePrint -> OctoAnalytics.logEvent(OctoAnalytics.Event.PrintWorkspaceShown)
                    R.id.terminalFragment -> OctoAnalytics.logEvent(OctoAnalytics.Event.TerminalWorkspaceShown)
                }

                viewModel.pendingNavigation?.let {
                    viewModel.pendingNavigation = null
                    navigate(it)
                }

                applySystemBarColors()
            }

            navHost.childFragmentManager.registerFragmentLifecycleCallbacks(
                object : FragmentManager.FragmentLifecycleCallbacks() {
                    override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                        super.onFragmentResumed(fm, f)
                        applyInsetsToScreen(f)
                    }
                },
                false
            )

            // Listen for inset changes and store them
            window.decorView.setOnApplyWindowInsetsListener { _, insets ->
                lastInsets.top = insets.systemWindowInsetTop
                lastInsets.left = insets.systemWindowInsetLeft
                lastInsets.bottom = insets.systemWindowInsetBottom
                lastInsets.right = insets.systemWindowInsetRight
                applyInsetsToCurrentScreen()
                insets.consumeSystemWindowInsets()
            }
        }

        prepareTablet()
        observeActiveInstance()
        attachSupportFragments()
    }

    private fun Flow<PrinterState.Flags>.filterWithRepeat(): Flow<PrinterState.Flags> {
        // The navigation logic needs flags to repeat a few times to be certain of changes
        // This means we can't use "distinctUntilChanged" but need to let the same value through a few times
        var sameFlagsCounter = 0
        var lastFlags: PrinterState.Flags? = null
        return filter {
            if (lastFlags == it) {
                sameFlagsCounter++
            } else {
                sameFlagsCounter = 0
            }
            lastFlags = it
            sameFlagsCounter <= minSameFlagsToBeSure
        }
    }

    private fun prepareTablet() {
        if (!isTablet() && !preferences.allowAppRotation) {
            // Stop screen rotation on phones
            @SuppressLint("SourceLockedOrientationActivity")
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

    private fun attachSupportFragments() {
        if (supportFragmentManager.findFragmentByTag("octolapse-support") != null) {
            Napier.i(tag = tag, message = "Skipping support fragment creation, already attached")
        } else {
            Napier.i(tag = tag, message = "Attaching support fragments")
            supportFragmentManager.beginTransaction()
                .add(OctolapseSupportFragment(), "octolapse-support")
                .add(Mmu2FilamentSelectSupportFragment(), "mmu2-select-filament-support")
                .add(NgrokSupportFragment(), "ngrok-support")
                .commit()
        }
    }

    private fun observeActiveInstance() = lifecycleScope.launchWhenResumedFixed {
        SignInInjector.get().octoprintRepository().instanceInformationFlow().filter {
            val webUrlAndApiKey = "${it?.webUrl}:${it?.apiKey}"
            val pass = viewModel.lastWebUrlAndApiKey != webUrlAndApiKey
            viewModel.lastWebUrlAndApiKey = webUrlAndApiKey
            Napier.i(tag = tag, message = "Instance information filter $it => $pass")
            pass
        }.collect { instance ->
            when {
                instance != null -> {
                    Napier.i(tag = tag, message = "Instance information received")
                    updateCapabilities("instance_change", updateM115 = true, escalateError = false)


                    // Go to connect screen if not yet connected
                    if (BaseInjector.get().octoPrintProvider().getLastCurrentMessage(instance.id) == null) {
                        Napier.i(tag = tag, message = "Not connected, moving to connect state")
                        navigate(MainActivityViewModel.Navigation.Connect)
                    } else {
                        Napier.i(tag = tag, message = "Already connected")
                    }

                    viewModel.pendingUri?.let {
                        viewModel.pendingUri = null
                        handleDeepLink(it)
                    }
                }

                else -> {
                    Napier.i(tag = tag, message = "No instance active $this")
                    navigate(MainActivityViewModel.Navigation.SignIn)
                    LiveNotificationManager.stop(this@MainActivity)
                }
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onNewIntent(intent: Intent) {
        Napier.i(tag = tag, message = "Handling new intent")
        BenchmarkIntentHandler.handle(intent)

        // Handle switch requests
        if (BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
            intent.getStringExtra(EXTRA_TARGET_OCTOPRINT_ID)?.let { id ->
                Napier.w(tag = tag, message = "Received intent to activate $id")
                val repo = BaseInjector.get().octorPrintRepository()
                repo.get(id)?.let {
                    repo.setActive(it, trigger = "Intent")
                }
            }
        }

        // Handle deep links
        val uri = intent.data ?: intent.getStringExtra(EXTRA_CLICK_URI)?.let { Uri.parse(it) }
        uri?.let {
            try {
                Napier.i(tag = tag, message = "Handling click URI: $it")
                if (it.host == "app.octoapp.eu" || it.host == "test.octoapp.eu") {
                    // Give a second for everything to settle
                    handleDeepLink(it)
                } else {
                    Napier.w(tag = tag, message = "Dropping URI, host is ${it.host}")
                }
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed", throwable = e)
            }
        }

        // Clean data so the nav component doesn't grab the link. We need to do this manually honoring the app state
        intent.data = null
        intent.removeExtra(EXTRA_TARGET_OCTOPRINT_ID)
        intent.removeExtra(EXTRA_CLICK_URI)
        super.onNewIntent(intent)
    }

    private fun handleDeepLink(uri: Uri) {
        Napier.i(tag = tag, message = "Hanlding deep link2")

        lifecycleScope.launchWhenResumedFixed {
            // Add minimal delay to ensure app is ready
            delay(timeMillis = 300)

            try {
                if (UriLibrary.isActiveInstanceRequired(uri.toString().toUrl()) && BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot() == null) {
                    Napier.i(tag = tag, message = "Uri requires active instance, delaying")
                    viewModel.pendingUri = uri
                } else {
                    Napier.i(tag = tag, message = "Hanlding deep link")
                    val url = uri.toString().toUrl()
                    when (url.pathSegments.firstOrNull { it.isNotBlank() }) {
                        OCTOEVERYWHERE_APP_PORTAL_CALLBACK_PATH -> BaseInjector.get().handleOctoEverywhereAppPortalSuccessUseCase().execute(url)
                        OBICO_APP_PORTAL_CALLBACK_PATH -> BaseInjector.get().handleObicoAppPortalSuccessUseCase().execute(url)
                        else -> {
                            Napier.i(tag = tag, message = "Handling generic URI: $uri")
                            uri.open(this@MainActivity)
                        }
                    }
                }
            } catch (e: Exception) {
                showDialog(e)
            }
        }
    }


    private fun isTablet() = ((this.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE)

    private fun isLandscape() = resources.getBoolean(R.bool.landscape)

    private fun isNightMode() = resources.getBoolean(R.bool.night_mode)

    private fun applyInsetsToCurrentScreen() = findCurrentScreen()?.let { applyInsetsToScreen(it) }

    private fun findCurrentScreen() = supportFragmentManager.findFragmentById(R.id.mainNavController)?.childFragmentManager?.fragments?.firstOrNull()

    private fun applyInsetsToScreen(screen: Fragment, topOverwrite: Int? = null) {
        if (screen is InsetAwareScreen) {
            fun push() = screen.handleInsets(
                Rect(
                    lastInsets.left,
                    topOverwrite ?: lastInsets.top,
                    lastInsets.right,
                    lastInsets.bottom,
                )
            )

            screen.view?.let { push() } ?: handler.postDelayed({ push() }, 100)
        } else {
            screen.view?.let { applyInsetsToView(it) }
            screen.view?.updatePadding(top = topOverwrite ?: screen.view?.paddingTop ?: 0)
        }
    }

    override fun applyInsetsToView(view: View) =
        view.updatePadding(top = lastInsets.top, bottom = lastInsets.bottom, left = lastInsets.left, right = lastInsets.right)


    override fun onStart() {
        super.onStart()
        Napier.i(tag = tag, message = "UI started")

        anrScope?.cancel("new scope")
        anrScope = CoroutineScope(Dispatchers.Default).also {
            anrDetector.startDetecting(this, it)
        }
        val stoppedAt = uiStoppedAt
        if (stoppedAt != null && (System.currentTimeMillis() - stoppedAt) > 30_000) {
            // OctoPrint might not be available, this is more like a fire and forget
            // Don't bother user with error messages
            updateCapabilities("ui_start", updateM115 = false, escalateError = false)
        } else {
            Napier.i(tag = tag, message = "Ui stopped for less than 30s, skipping capabilities update")
        }
    }

    override fun onStop() {
        super.onStop()
        val now = System.currentTimeMillis()
        uiStoppedAt = now
        Napier.i(tag = tag, message = "UI stopped")
        anrScope?.cancel("app closed")
    }

    override fun onResume() {
        super.onResume()
        BaseInjector.get().octoPreferences().wasPrintNotificationDisabledUntilNextLaunch = false
        BillingManager.onResume()
        purchaseEventJob = lifecycleScope.launchWhenResumedFixed {
            BillingManager.events.collectLatest { event ->
                if (event is BillingEvent.PurchaseCompleted) {
                    Napier.i(tag = tag, message = "Purchase completed, showing confirmation")
                    PurchaseConfirmationDialog().show(supportFragmentManager, "purchase-confirmation")
                }
            }
        }

        // Fallback....sometimes insets are not applied
        insetJob = lifecycleScope.launch {
            while (currentCoroutineContext().isActive) {
                delay(1.seconds)
                applyInsetsToCurrentScreen()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        insetJob?.cancel()
        purchaseEventJob?.cancel()
        BillingManager.onPause()
    }

    override fun enforceAllowAutomaticNavigationFromCurrentDestination() {
        Napier.i(tag = tag, message = "Enforcing auto navigation for the next navigation event")
        enforceAutoamticNavigationAllowed = true
    }

    private fun navigate(destination: MainActivityViewModel.Navigation?) {
        val destinationId = destination?.action

        if (destinationId != null) {
            // Screens which must be/can be closed automatically when the state changes
            // Other screens will stay open and we navigate to the new state-based destination after the
            // current screen is closed
            val currentDestination = navController.currentDestination?.id
            val isDuplicate = currentDestination == destination.destination || currentDestination == destination.action
            val currentDestinationAllowsAutoNavigate = listOf(
                R.id.splashFragment,
                R.id.discoverFragment,
                R.id.signInSuccessFragment,
                R.id.workspaceConnect,
                R.id.workspacePrePrint,
                R.id.workspacePrint,
                R.id.controlsFragment,
            ).contains(currentDestination)
            val currentName = currentDestination?.let(resources::getResourceEntryName)
            val destName = resources.getResourceEntryName(destinationId)

            if (isDuplicate) {
                Napier.v(
                    tag = tag, message =
                    "Skipping duplicate, action=${resources.getResourceName(destination.action)} destination=${resources.getResourceName(destination.destination)} current=${
                        currentDestination?.let(
                            resources::getResourceName
                        )
                    }"
                )
                enforceAutoamticNavigationAllowed = false
            } else if (currentDestinationAllowsAutoNavigate || enforceAutoamticNavigationAllowed) {
                Napier.d(
                    tag = tag,
                    message = "Navigating to $destName (current=$currentName currentDestinationAllowsAutoNavigate=$currentDestinationAllowsAutoNavigate enforceAutoamticNavigationAllowed=$enforceAutoamticNavigationAllowed)"
                )
                enforceAutoamticNavigationAllowed = false
                viewModel.lastNavigation = destination
                viewModel.pendingNavigation = null
                viewModel.pendingFlagNavigation = null
                navController.navigate(destinationId)
            } else {
                if (viewModel.pendingNavigation != destination) {
                    Napier.d(tag = tag, message = "Current destination $currentName does not allow auto navigate, storing navigation action as pending: $destination")
                }
                viewModel.pendingNavigation = destination
            }
        } else {
            Napier.v(tag = tag, message = "Skipping null navigation")
        }
    }

    private fun onEventReceived(e: Event) = when (e) {
        // Only show errors if we are not already in disconnected screen. We still want to show the stall warning to indicate something is wrong
        // as this might lead to the user being stuck
        is Event.Disconnected -> {
            Napier.w(tag = tag, message = "Connection lost")
            when {
                e.exception is WebSocketUpgradeFailedException -> e.exception?.let(this::showDialog)
                else -> Unit
            }
        }

        is Event.Connected -> {
            Napier.w(tag = tag, message = "Connection restored")
            updateAllWidgets()
            viewModel.connectionType = e.connectionType

            // Start LiveNotification again, might have been stopped!
            LiveNotificationManager.restartIfWasPaused(this)
        }

        is Event.MessageReceived -> onMessageReceived(e.message)

        else -> Unit
    }

    private fun onMessageReceived(e: SocketMessage) = when (e) {
        is SocketMessage.Current -> Unit // Ignore. Done via separate flow
        is SocketMessage.Event -> onEventMessageReceived(e)
        is SocketMessage.Connected -> {
            // We are connected, let's update the available capabilities of the connect Octoprint
            if ((System.currentTimeMillis() - viewModel.lastSuccessfulCapabilitiesUpdate) > 10000) {
                updateCapabilities("connected_event")
            } else Unit
        }

        else -> Unit
    }

    private fun onCurrentMessageReceived(flags: PrinterState.Flags, instanceId: String) {
        try {
            navController
        } catch (e: Exception) {
            Napier.v(tag = tag, message = "Skipping flags, not ready to navigate...")
            return
        }

        Napier.v(tag = tag, message = flags.toString())
        val lastFlags = viewModel.lastFlags.takeIf { viewModel.lastFlagsInstanceId == instanceId }
        viewModel.lastFlags = flags
        viewModel.lastFlagsInstanceId = instanceId
        if (flags == lastFlags) {
            viewModel.sameFlagsCounter++
        } else {
            viewModel.sameFlagsCounter = 0
        }

        val navigation = when {
            // We encountered an error, try reconnecting
            flags.isError() -> {
                LiveNotificationManager.stop(this)
                MainActivityViewModel.Navigation.Connect
            }

            // We are printing
            flags.isPrinting() -> {
                try {
                    LiveNotificationManager.start(this, trigger = "Print detected")
                } catch (e: IllegalStateException) {
                    // User might have closed app just in time so we can't start the service
                }
                MainActivityViewModel.Navigation.Print
            }

            // We are connected
            flags.isOperational() -> {
                LiveNotificationManager.stop(this)
                MainActivityViewModel.Navigation.Prepare
            }

            !flags.isOperational() && !flags.isPrinting() -> {
                LiveNotificationManager.stop(this)
                MainActivityViewModel.Navigation.Connect
            }

            // Fallback
            else -> viewModel.lastNavigation
        }

        // Sometimes when changing e.g. from paused to printing OctoPrint sends one wrong set of flags, so we
        // only continue if last and current are the same
        // If we have closed or error, it's always instant
        if ((viewModel.sameFlagsCounter < minSameFlagsToBeSure || lastFlags == null) && !flags.closedOrError) {
            viewModel.pendingFlagNavigation = navigation
            return Napier.d(tag = tag, message = "Skipping flag navigation, recently changed and waiting for confirmation before performing: $navigation")
        }

        if ((viewModel.sameFlagsCounter == minSameFlagsToBeSure && lastFlags != null) && !flags.closedOrError) {
            Napier.i(tag = tag, message = "Performing flag navigation to $navigation: $flags")
        }

        navigate(navigation)
    }

    private fun onEventMessageReceived(e: SocketMessage.Event) = when (e) {
        is SocketMessage.Event.PrinterConnected,
        is SocketMessage.Event.PrinterProfileModified,
        is SocketMessage.Event.SettingsUpdated -> {
            // New printer connected or settings updated, let's update capabilities
            updateCapabilities("settings_updated", updateM115 = false)
        }

        else -> Unit
    }


    override fun applySystemBarColors() = with(WindowInsetsControllerCompat(window, binding.root)) {
        val isWebcamFullscreen = try {
            navController.currentDestination?.id == R.id.webcamFragment
        } catch (e: IllegalStateException) {
            // NavController not set yet
            false
        }

        isAppearanceLightStatusBars = !isNightMode() && !isWebcamFullscreen && !lightStatusBarRequested
        isAppearanceLightNavigationBars = !isNightMode() && !isWebcamFullscreen
    }

    private fun updateCapabilities(trigger: String, updateM115: Boolean = true, escalateError: Boolean = true) {
        Napier.i(tag = tag, message = "Updating capabities (trigger=$trigger)")
        updateCapabilitiesJob = lifecycleScope.launchWhenCreatedFixed {
            try {
                viewModel.lastSuccessfulCapabilitiesUpdate = System.currentTimeMillis()
                BaseInjector.get().updateInstanceCapabilitiesUseCase().execute(UpdateInstanceCapabilitiesUseCase.Params())
                updateAllWidgets()
            } catch (e: Exception) {
                viewModel.lastSuccessfulCapabilitiesUpdate = 0
                if (escalateError) {
                    Napier.e(tag = tag, message = "Failed", throwable = e)
                    showSnackbar(
                        Message.SnackbarMessage(
                            text = { getString(R.string.capabilities_validation_error) },
                            type = Message.SnackbarMessage.Type.Negative
                        )
                    )
                }
            }
        }
    }

    override fun startPrintNotificationService() {
        LiveNotificationManager.start(this, "Start from menu")
    }
}