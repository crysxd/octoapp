package de.crysxd.octoapp.wear

import android.content.Context
import com.google.android.gms.common.ConnectionResult.API_UNAVAILABLE
import com.google.android.gms.common.ConnectionResult.RESOLUTION_ACTIVITY_NOT_FOUND
import com.google.android.gms.common.ConnectionResult.RESTRICTED_PROFILE
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.wearable.DataEventBuffer
import com.google.android.gms.wearable.PutDataRequest
import com.google.android.gms.wearable.Wearable
import com.google.gson.Gson
import de.crysxd.baseui.di.BaseUiInjector
import de.crysxd.baseui.usecase.OpenEmailClientForFeedbackUseCase
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.export
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import java.io.ByteArrayInputStream
import java.io.ObjectInputStream
import java.util.concurrent.TimeUnit
import java.util.zip.GZIPInputStream

class WearDataLayerService(private val context: Context) {

    private val tag = "WearDataLayerService"
    private val dataClient = Wearable.getDataClient(context)

    init {
        Napier.i(tag = tag, message = "Waiting for Wear OS requests")
        dataClient.addListener(::onDataReceived)

        // Push initial configuration and then listen for changes
        AppScope.launch(Dispatchers.IO) {
            BaseInjector.get().octorPrintRepository().allInstanceInformationFlow().onEach {
                putConfiguration()
            }.collectSave()
        }

        // Push initial configuration and then listen for changes
        AppScope.launch(Dispatchers.IO) {
            BaseInjector.get().gcodeHistoryRepository().history.onEach {
                putHistory(it)
            }.collectSave()
        }

        // Push initial settings and then listen for changes
        AppScope.launch(Dispatchers.IO) {
            BaseInjector.get().octoPreferences().updatedFlow.onEach {
                putSettings()
            }.collectSave()
        }

        // Push initial DNS cache and then listen for changes
        AppScope.launch(Dispatchers.IO) {
            val resolver = BaseInjector.get().localDnsResolver() as? CachedLocalDnsResolver ?: return@launch
            resolver.cacheUpdateFlow.onEach {
                putDnsCache()
            }.collectSave()
        }
    }

    private suspend fun Flow<*>.collectSave() {
        var failureCount = 0
        retry {
            val apiException = it as? ApiException ?: it.cause as? ApiException
            val apiStatusCode = apiException?.statusCode ?: -1
            suspend fun retryLater(log: (Long) -> Unit): Boolean {
                failureCount++
                val delay = (1000L * failureCount).coerceAtMost(TimeUnit.MINUTES.toMillis(15))
                log(delay)
                delay(delay)
                return true
            }

            when (apiStatusCode) {
                // Wear Api Unavailable
                17,
                API_UNAVAILABLE -> {
                    Napier.w(tag = tag, message = "Wear OS support is not available, cancelling flow collection")
                    false
                }

                // Temporary issue
                RESOLUTION_ACTIVITY_NOT_FOUND,
                RESTRICTED_PROFILE -> retryLater { delay ->
                    Napier.w(tag = tag, message = "Wear OS support is not available, retrying in ${delay}ms")
                }

                // Generic error handling
                else -> retryLater { delay ->
                    Napier.e(tag = tag, message = "Failed to put, retry in ${delay}ms", throwable = it)
                }
            }
        }.catch {
            // Nothing
        }.collect {
            failureCount = 0
        }
    }

    private fun putConfiguration() {
        val instances = BaseInjector.get().octorPrintRepository().getAll()
        val bytes = BaseInjector.get().octoPrintInstanceInformationSerializer().serialize(instances)
        val request = PutDataRequest.create(context.getString(R.string.rpc_asset_path___instance_configuration))
            .setData(bytes)
            .setUrgent()

        Napier.i(tag = tag, message = "Putting ${bytes.size} bytes for configuration to ${request.uri}")
        Tasks.await(dataClient.putDataItem(request))
        Napier.i(tag = tag, message = "Put ${bytes.size} bytes for configuration to ${request.uri}")
    }

    private fun putHistory(items: List<GcodeHistoryItem>) {
        val json = Gson().toJson(items).toByteArray()
        val request = PutDataRequest.create(context.getString(R.string.rpc_asset_path___gcode_history))
            .setData(json)
            .setUrgent()

        Napier.i(tag = tag, message = "Putting ${json.size} bytes for configuration to ${request.uri}")
        Tasks.await(dataClient.putDataItem(request))
        Napier.i(tag = tag, message = "Put ${json.size} bytes for configuration to ${request.uri}")
    }

    private fun putSettings() {
        val bytes = BaseInjector.get().octoPreferences().export()
        val request = PutDataRequest.create(context.getString(R.string.rpc_asset_path___settings))
            .setData(bytes)
            .setUrgent()

        Napier.i(tag = tag, message = "Putting ${bytes.size} bytes for settings to ${request.uri}")
        Tasks.await(dataClient.putDataItem(request))
        Napier.i(tag = tag, message = "Put ${bytes.size} bytes for settings to ${request.uri}")
    }

    private fun putDnsCache() {
        val bytes = (BaseInjector.get().localDnsResolver() as? CachedLocalDnsResolver)?.exportCache()
            ?: return Napier.w(tag = tag, message = "No DNS cache available")

        val request = PutDataRequest.create(context.getString(R.string.rpc_asset_path___dns_cache))
            .setData(bytes)
            .setUrgent()

        Napier.i(tag = tag, message = "Putting ${bytes.size} bytes for DNS cache to ${request.uri}")
        Tasks.await(dataClient.putDataItem(request))
        Napier.i(tag = tag, message = "Put ${bytes.size} bytes for DNS cache to ${request.uri}")
    }

    private fun onDataReceived(events: DataEventBuffer) {
        Napier.i(tag = tag, message = "Received data events")

        events.forEach { event ->
            when (event.dataItem.uri.path) {
                context.getString(R.string.rpc_asset_path___bug_report_bundle) -> sendBugReport(event.dataItem.data ?: ByteArray(0))
                context.getString(R.string.rpc_asset_path___instance_configuration) -> Unit // Ignore, that's us sending it
                context.getString(R.string.rpc_asset_path___settings) -> Unit // Ignore, that's us sending it
                context.getString(R.string.rpc_asset_path___dns_cache) -> Unit // Ignore, that's us sending it
                context.getString(R.string.rpc_asset_path___gcode_history) -> Unit // Ignore, that's us sending it
                else -> Napier.e(tag = tag, throwable = SuppressedIllegalStateException("Unknown data event path ${event.dataItem.uri} received"), message = "Failed")
            }
        }
    }

    private fun sendBugReport(bytes: ByteArray) = AppScope.launch(Dispatchers.IO) {
        Napier.i(tag = tag, message = "Sending bug report (${bytes.size} bytes)")
        val bugReport = ObjectInputStream(GZIPInputStream(ByteArrayInputStream(bytes))).use { it.readObject() as CreateBugReportUseCase.BugReport }
        BaseUiInjector.get().openEmailClientForFeedbackUseCase().execute(
            OpenEmailClientForFeedbackUseCase.Params(
                bugReport = bugReport,
                context = context,
                message = "PLEASE ENTER BUG DESCRIPTION",
                appVersion = bugReport.appVersion,
                useShareSheet = false
            )
        )
    }
}