package de.crysxd.octoapp.widgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.crysxd.baseui.LocalizedActivity
import de.crysxd.baseui.ext.mainActivityClass
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemLibrary
import de.crysxd.octoapp.widgets.progress.ProgressAppWidget
import de.crysxd.octoapp.widgets.webcam.BaseWebcamAppWidget
import de.crysxd.octoapp.widgets.webcam.ControlsWebcamAppWidget
import de.crysxd.octoapp.widgets.webcam.NoControlsWebcamAppWidget
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ExecuteWidgetActionActivity : LocalizedActivity(), MenuHost {

    companion object {
        private const val tag = "ExecuteWidgetActionActivity"
        private const val EXTRA_TASK = "de.crysxd.octoapp.widgets.progress.TASK"
        private const val EXTRA_APP_WIDGET_ID = "de.crysxd.octoapp.widgets.progress.APP_WIDGET_ID"
        private const val EXTRA_PLAY_LIVE = "de.crysxd.octoapp.widgets.progress.PLAY_LIVE"
        private const val EXTRA_MENU_ITEM_ID = "de.crysxd.octoapp.widgets.progress.MENU_ITEM_ID"
        private const val TASK_CANCEL = "cancel"
        private const val TASK_PAUSE = "pause"
        private const val TASK_RESUME = "resume"
        private const val TASK_REFRESH = "refresh"
        private const val TASK_CLICK_MENU_ITEM = "click"

        fun createRefreshTaskPendingIntent(context: Context, appWidgetId: Int, playLive: Boolean) =
            createTaskPendingIntent(context, TASK_REFRESH, "ExecuteWidgetActionActivity/$TASK_REFRESH/$appWidgetId/$playLive") {
                it.putExtra(EXTRA_APP_WIDGET_ID, appWidgetId)
                it.putExtra(EXTRA_PLAY_LIVE, playLive)
            }

        fun createCancelTaskPendingIntent(context: Context) = createTaskPendingIntent(context, TASK_CANCEL)
        fun createPauseTaskPendingIntent(context: Context) = createTaskPendingIntent(context, TASK_PAUSE)
        fun createResumeTaskPendingIntent(context: Context) = createTaskPendingIntent(context, TASK_RESUME)
        fun createClickMenuItemPendingIntentTemplate(context: Context) = createTaskPendingIntent(context, TASK_CLICK_MENU_ITEM)
        fun createClickMenuItemFillIntent(menuItemId: String) = Intent().also {
            it.putExtra(EXTRA_MENU_ITEM_ID, menuItemId)
        }

        private fun createTaskPendingIntent(
            context: Context,
            task: String,
            id: String = "ExecuteWidgetActionActivity/$task",
            intentUpdate: (Intent) -> Unit = {}
        ): PendingIntent =
            PendingIntent.getActivity(
                context,
                id.hashCode(),
                Intent(context, ExecuteWidgetActionActivity::class.java).also {
                    it.putExtra(EXTRA_TASK, task)
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intentUpdate(it)
                },
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
            )
    }

    private val task get() = intent.getStringExtra(EXTRA_TASK)
    private var suppressSuccessAniamtion = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Napier.i(tag = tag, message = "Starting ExecuteWidgetActionActivity for task $task")
        removeTransitions()

        if (task == TASK_REFRESH) {
            triggerRefresh()
            finish()
        } else {
            confirmAction()
        }
    }

    private fun triggerRefresh() {
        intent.getIntExtra(EXTRA_APP_WIDGET_ID, 0).takeIf { it != 0 }?.let { id ->
            val live = intent.getBooleanExtra(EXTRA_PLAY_LIVE, false)
            Napier.i(tag = tag, message = "Updating request received for $id (live=$live)")

            val manager = AppWidgetManager.getInstance(this)
            val progressWidgetIds = ComponentName(this, ProgressAppWidget::class.java).let { manager.getAppWidgetIds(it) }
            val webcamWidgetIds = listOf(
                ComponentName(this, ControlsWebcamAppWidget::class.java).let { manager.getAppWidgetIds(it) }.toList(),
                ComponentName(this, NoControlsWebcamAppWidget::class.java).let { manager.getAppWidgetIds(it) }.toList(),
            ).flatten()

            when {
                progressWidgetIds.contains(id) -> ProgressAppWidget.notifyWidgetDataChanged()
                webcamWidgetIds.contains(id) -> BaseWebcamAppWidget.updateAppWidget(id, trigger = "intent", playLive = live, isManualRefresh = true)
            }
        } ?: updateAllWidgets()

        finish()
    }

    private fun confirmAction() = lifecycleScope.launch {
        val menuItemId = intent.getStringExtra(EXTRA_MENU_ITEM_ID) ?: let {
            Napier.e(tag = tag, message = "Unale to process click, no menu item")
            return@launch showContentNotAvailableError()
        }
        val instanceId = SharedBaseInjector.get().printerConfigRepository.getActiveInstanceSnapshot()?.id ?: let {
            Napier.e(tag = tag, message = "Unale to process click on $menuItemId, no active instance")
            return@launch showContentNotAvailableError()
        }
        val menuItem = MenuItemLibrary()[menuItemId, instanceId]

        val message = when (task) {
            TASK_CANCEL -> getString(R.string.cancel_print_confirmation_message)
            TASK_PAUSE -> getString(R.string.pause_print_confirmation_message)
            TASK_RESUME -> getString(R.string.resume_print_confirmation_message)
            TASK_CLICK_MENU_ITEM -> getString(
                R.string.app_widget___click_menu_item_confirmation_message,
                menuItem?.title
            )

            else -> null
        }

        val action = when (task) {
            TASK_CANCEL -> R.string.cancel_print_confirmation_action
            TASK_PAUSE -> R.string.pause_print_confirmation_action
            TASK_RESUME -> R.string.resume_print_confirmation_action
            TASK_CLICK_MENU_ITEM -> R.string.app_widget___click_menu_item_confirmation_action
            else -> null
        }

        if (message == null || action == null) {
            Napier.e(
                tag = tag,
                message = "Did not find action",
                throwable = IllegalArgumentException("Activity started with task $task, did not find action or message")
            )
            finish()
            return@launch
        }

        MaterialAlertDialogBuilder(this@ExecuteWidgetActionActivity)
            .setMessage(message)
            .setPositiveButton(action) { _, _ ->
                Napier.i(tag = tag, message = "Task $task confirmed")
                // Activity will be finished in a millisecond, so we use Global to trigger the action
                AppScope.launch(Dispatchers.Main) {
                    try {
                        when (task) {
                            TASK_CANCEL -> BaseInjector.get().cancelPrintJobUseCase().execute(CancelPrintJobUseCase.Params(false))
                            TASK_PAUSE, TASK_RESUME -> BaseInjector.get().togglePausePrintJobUseCase().execute(TogglePausePrintJobUseCase.Params())
                            TASK_CLICK_MENU_ITEM -> menuItem?.let { menuItem -> performMenuItemClick(menuItem) }
                            else -> Unit
                        }
                    } catch (e: Exception) {
                        Napier.e(tag = tag, message = "Failed", throwable = e)
                        Toast.makeText(BaseInjector.get().context(), "Failed to execute task", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            .setNegativeButton(R.string.cancel, null)
            .setOnDismissListener {
                finish()
            }
            .show()
    }

    private fun showContentNotAvailableError() {
        MaterialAlertDialogBuilder(this@ExecuteWidgetActionActivity)
            .setMessage(getString(R.string.help___content_not_available))
            .setPositiveButton(android.R.string.ok, null)
            .setOnDismissListener { finish() }
            .show()
    }

    private suspend fun performMenuItemClick(menuItem: MenuItem) {
        // This is very ugly...
        mainActivityClass = MainActivity::class.java

        try {
            Napier.i(tag = tag, message = "Executing ${menuItem.itemId}")
            menuItem.onClicked(this)
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failed")
            Toast.makeText(
                this,
                e.composeErrorMessage(),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override suspend fun pushMenu(subMenu: Menu) = showContentNotAvailableError()

    override suspend fun popMenu() = Unit

    override suspend fun closeMenu(result: String?) = Unit

    override suspend fun reloadMenu() = Unit

    override fun openUrl(url: Url) = Uri.parse(url.toString()).open(octoActivity = this)
}