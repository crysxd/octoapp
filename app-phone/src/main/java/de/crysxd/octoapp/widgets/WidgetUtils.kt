package de.crysxd.octoapp.widgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.RemoteViews
import de.crysxd.octoapp.BuildConfig
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.MainActivity.Companion.EXTRA_TARGET_OCTOPRINT_ID
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.widgets.progress.ProgressAppWidget
import de.crysxd.octoapp.widgets.quickaccess.QuickAccessAppWidget
import de.crysxd.octoapp.widgets.webcam.BaseWebcamAppWidget
import de.crysxd.octoapp.widgets.webcam.ControlsWebcamAppWidget
import de.crysxd.octoapp.widgets.webcam.NoControlsWebcamAppWidget
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

private const val TAG = "WidgetUtils"

internal fun updateAppWidget(widgetId: Int, trigger: String) {
    val context = BaseInjector.get().localizedContext()
    val manager = AppWidgetManager.getInstance(context)
    when (val name = manager.getAppWidgetInfo(widgetId).provider.className) {
        ControlsWebcamAppWidget::class.java.name, NoControlsWebcamAppWidget::class.java.name -> BaseWebcamAppWidget.updateAppWidget(
            appWidgetId = widgetId,
            trigger = trigger
        )

        ProgressAppWidget::class.java.name -> ProgressAppWidget.notifyWidgetDataChanged()
        QuickAccessAppWidget::class.java.name -> QuickAccessAppWidget.notifyWidgetDataChanged()
        else -> Napier.e(tag = TAG, throwable = IllegalArgumentException("Supposed to update widget $widgetId with unknown provider $name"), message = "Failed")
    }
}

internal fun ensureWidgetExists(widgetId: Int) = AppWidgetManager.getInstance(BaseInjector.get().context()).getAppWidgetInfo(widgetId) != null

internal fun updateAllWidgets() {
    AppScope.launch {
        try {
            Napier.i(tag = TAG, message = "Updating all widgets")
            BaseWebcamAppWidget.notifyWidgetDataChanged()
            ProgressAppWidget.notifyWidgetDataChanged()
            QuickAccessAppWidget.notifyWidgetDataChanged()
        } catch (e: Exception) {
            Napier.e(tag = TAG, throwable = e, message = "Failed to update all widgets")
        }
    }
}

internal fun cancelAllUpdates() {
    BaseWebcamAppWidget.cancelAllUpdates()
    ProgressAppWidget.cancelAllUpdates()
}

internal fun createLaunchAppIntent(context: Context, instanceId: String?) = PendingIntent.getActivity(
    context,
    "launch_main_with_instance_$instanceId".hashCode(),
    Intent(context, MainActivity::class.java).also {
        if (BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
            it.putExtra(EXTRA_TARGET_OCTOPRINT_ID, instanceId)
        }
    },
    PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
)

internal fun createUpdateIntent(context: Context, widgetId: Int, playLive: Boolean = false) =
    ExecuteWidgetActionActivity.createRefreshTaskPendingIntent(context, widgetId, playLive)

internal fun createUpdatedNowText() = getTime()

internal fun getTime() = Clock.System.now().format(showDate = false, showTime = true)

internal fun createUpdateFailedText(context: Context, appWidgetId: Int) =
    AppWidgetPreferences.getLastUpdateTime(appWidgetId).takeIf { it > 0 }?.let {
        context.getString(R.string.app_widget___offline_since_x, Instant.fromEpochMilliseconds(it).format())
    } ?: context.getString(R.string.app_widget___update_failed)

internal fun applyDebugOptions(views: RemoteViews, appWidgetId: Int) {
    views.setTextViewText(R.id.widgetId, "$appWidgetId/${AppWidgetPreferences.getInstanceForWidgetId(appWidgetId)}")
    views.setViewVisibility(R.id.widgetId, BuildConfig.DEBUG)
}

internal fun getWidgetWidth(appWidgetId: Int) = AppWidgetPreferences.getWidgetDimensionsForWidgetId(appWidgetId).first

internal fun getWidgetHeight(appWidgetId: Int) = AppWidgetPreferences.getWidgetDimensionsForWidgetId(appWidgetId).second

internal fun getWidgetCount(context: Context) = AppWidgetManager.getInstance(context).installedProviders.asSequence().map {
    it.provider
}.filter {
    it.packageName == context.packageName
}.map {
    AppWidgetManager.getInstance(context).getAppWidgetIds(it)
}.map { widgets ->
    widgets.filter { ensureWidgetExists(it) }
}.sumOf { it.size }

fun RemoteViews.setClipToOutLine() {
    // Skipping clip outline on olderdevices, they don't all support it
    if (Build.VERSION.SDK_INT > 30) {
        setBoolean(R.id.root, "setClipToOutline", true)
    }
}