package de.crysxd.octoapp.widgets

import android.appwidget.AppWidgetHost
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.graphics.drawable.Icon
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.compose.ui.graphics.toArgb
import androidx.core.content.ContextCompat
import androidx.core.graphics.applyCanvas
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.LocalizedActivity
import de.crysxd.baseui.ext.toCompose
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.MainActivity.Companion.EXTRA_TARGET_OCTOPRINT_ID
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_INFINITE_WIDGETS
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.widgets.AppWidgetPreferences.ACTIVE_INSTANCE_MARKER
import de.crysxd.octoapp.widgets.quickaccess.QuickAccessAppWidget
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull
import java.util.UUID
import kotlin.math.roundToInt


class ConfigureAppWidgetActivity : LocalizedActivity() {

    private val tag = "ConfigureAppWidgetActivity"
    private var canceled = true
    private val appWidgetId
        get() = intent.extras?.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)
            ?: AppWidgetManager.INVALID_APPWIDGET_ID

    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)

        Napier.i(tag = tag, message = "Starting ConfigureWidgetActivity for widget $appWidgetId")
        removeTransitions()

        lifecycleScope.launch {
            // Wait for billing to be ready
            withTimeoutOrNull(1_000) {
                Napier.i(tag = tag, message = "Waiting for billing...")
                BillingManager.state.first { it.isBillingInitialized }
                Napier.i(tag = tag, message = "Billing ready")
            }

            val featureEnabled = BillingManager.isFeatureEnabled(FEATURE_INFINITE_WIDGETS)
            val activeWidgetCount = getWidgetCount(this@ConfigureAppWidgetActivity)
            val maxWidgetCount = Firebase.remoteConfig.getLong("number_of_free_app_widgets")
            Napier.i(tag = tag, message = "Widgets active: $activeWidgetCount / $maxWidgetCount (unlimited widgets: $featureEnabled)")

            if (activeWidgetCount > maxWidgetCount && !featureEnabled) {
                Napier.i(tag = tag, message = "Not allowed to add widget")

                MaterialAlertDialogBuilder(this@ConfigureAppWidgetActivity)
                    .setMessage(getString(R.string.app_widget___free_widgets_exceeded_message, maxWidgetCount))
                    .setPositiveButton(R.string.app_widget___free_widgets_exceeded_action, null)
                    .setOnDismissListener { finishWithCancel() }
                    .show()
            } else when (intent.action) {
                AppWidgetManager.ACTION_APPWIDGET_CONFIGURE -> configureAppWidget()
                Intent.ACTION_CREATE_SHORTCUT -> configureShortcut()

                else -> {
                    Napier.e(tag = tag, throwable = IllegalArgumentException("Invalid action ${intent.action}"), message = "Invalid action")
                    finishWithCancel()
                }
            }
        }
    }

    private fun createAppWidgetResultIntent() = Intent().apply {
        putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
    }

    private fun finishWithSuccess(intent: Intent? = null) {
        setResult(RESULT_OK, intent)
        canceled = false
        finish()
    }

    private fun finishWithCancel(intent: Intent? = null) {
        setResult(RESULT_CANCELED, intent)
        finish()
    }

    override fun finish() {
        super.finish()
        removeTransitions()
    }

    private fun removeWidget(appWidgetId: Int) {
        AppWidgetPreferences.deletePreferencesForWidgetId(appWidgetId)
        val host = AppWidgetHost(this, 1)
        host.deleteAppWidgetId(appWidgetId)
    }

    override fun onResume() {
        super.onResume()
        BillingManager.onResume()
    }

    override fun onPause() {
        super.onPause()
        BillingManager.onPause()
    }

    override fun onStop() {
        super.onStop()
        if (canceled && appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            removeWidget(appWidgetId)
        }

        // Can't be in background
        finish()
    }

    private fun configureShortcut() {
        showSelectionDialog(supportsActive = false) { id ->
            val instance = requireNotNull(BaseInjector.get().octorPrintRepository().get(id)) { "Unable to locate instance $id" }
            val label = instance.label
            val icon = Icon.createWithAdaptiveBitmap(instance.createLauncherBitmap())
            val manager = getSystemService(ShortcutManager::class.java)
            val info = ShortcutInfo.Builder(this, UUID.randomUUID().toString())
                .setIntent(Intent(this, MainActivity::class.java).also {
                    it.action = Intent.ACTION_VIEW
                    it.putExtra(EXTRA_TARGET_OCTOPRINT_ID, id)
                })
                .setShortLabel(label)
                .setIcon(icon)
                .build()

            manager.createShortcutResultIntent(info)
        }
    }

    private fun PrinterConfigurationV3.createLauncherBitmap() = Bitmap.createBitmap(1024, 1024, Bitmap.Config.ARGB_8888).apply {
        val bitmapWidth = width
        val bitmapHeight = height
        applyCanvas {
            fun Drawable.drawSelf(tint: HexColor?) {
                val height = ((bitmapWidth / intrinsicWidth.toFloat()) * intrinsicHeight).roundToInt()
                bounds = Rect(
                    (bitmapWidth / 2) - (bitmapWidth / 2),
                    (bitmapHeight / 2) - (height / 2),
                    (bitmapWidth / 2) + (bitmapWidth / 2),
                    (bitmapHeight / 2) + (height / 2),
                )
                tint?.let {
                    setTint(tint.toCompose().toArgb())
                }
                draw(this@applyCanvas)
            }

            ContextCompat.getDrawable(this@ConfigureAppWidgetActivity, R.mipmap.ic_launcher_background)?.drawSelf(tint = null)
            ContextCompat.getDrawable(this@ConfigureAppWidgetActivity, R.drawable.ic_launcher_octo_fill)?.drawSelf(tint = colors.light.accent)
            ContextCompat.getDrawable(this@ConfigureAppWidgetActivity, R.drawable.ic_launcher_octo_outline)?.drawSelf(tint = colors.light.main)
        }
    }

    private fun configureAppWidget() {
        if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finishWithCancel()
            return
        }

        showSelectionDialog(supportsActive = true) {
            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetPreferences.setInstanceForWidgetId(appWidgetId, it)
            updateAppWidget(appWidgetId, trigger = "configure-done")

            // Create result intnet
            createAppWidgetResultIntent()
        }
    }

    private fun showSelectionDialog(supportsActive: Boolean, result: (id: String) -> Intent?) {
        val instances = BaseInjector.get().octorPrintRepository().getAll()
        val titles = instances.map { it.label }.map { getString(R.string.app_widget___link_widget__option_x, it) }
        val ids = instances.map { it.id }

        if (ids.size <= 1 && supportsActive) {
            Napier.i(tag = tag, message = "Only ${ids.size} options, auto picking sync option")
            finishWithSuccess(result(ACTIVE_INSTANCE_MARKER))
            return
        }

        if (AppWidgetManager.getInstance(this).getAppWidgetInfo(appWidgetId)?.provider?.className == QuickAccessAppWidget::class.java.name) {
            MaterialAlertDialogBuilder(this)
                .setTitle(R.string.app_widget___information_title)
                .setMessage(R.string.app_widget___information_always_synced)
                .setPositiveButton(android.R.string.ok, null)
                .setOnDismissListener {
                    finishWithSuccess(result(ACTIVE_INSTANCE_MARKER))
                }
                .show()
            return
        }

        val allTitles = if (supportsActive) listOf(listOf(getString(R.string.app_widget___link_widget__option_synced)), titles).flatten() else titles
        val allIds = if (supportsActive) listOf(listOf(ACTIVE_INSTANCE_MARKER), ids).flatten() else ids
        var selectedId: String? = null
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.app_widget___link_widget_title)
            .setItems(allTitles.toTypedArray()) { _, selected ->
                selectedId = allIds[selected]

            }
            .setOnDismissListener {
                Handler(Looper.getMainLooper()).postDelayed({
                    Napier.i(tag = tag, message = "Closing ConfigureWidgetActivity, selected $selectedId")

                    val id = selectedId
                    if (id != null) {
                        finishWithSuccess(result(id))
                    } else {
                        finishWithCancel(createAppWidgetResultIntent())
                    }
                }, 300)
            }
            .show()
    }
}