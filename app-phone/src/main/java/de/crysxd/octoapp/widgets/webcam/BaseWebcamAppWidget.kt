package de.crysxd.octoapp.widgets.webcam

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.RemoteViews
import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.widgets.AppWidgetPreferences
import de.crysxd.octoapp.widgets.applyDebugOptions
import de.crysxd.octoapp.widgets.createLaunchAppIntent
import de.crysxd.octoapp.widgets.createUpdateFailedText
import de.crysxd.octoapp.widgets.createUpdateIntent
import de.crysxd.octoapp.widgets.createUpdatedNowText
import de.crysxd.octoapp.widgets.ensureWidgetExists
import de.crysxd.octoapp.widgets.setClipToOutLine
import de.crysxd.octoapp.widgets.setViewVisibility
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlinx.coroutines.withTimeoutOrNull
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.time.Duration.Companion.seconds

abstract class BaseWebcamAppWidget : AppWidgetProvider() {

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(BaseInjector.get().localizedContext(), intent)
    }

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        appWidgetIds.filter { ensureWidgetExists(it) }.forEach { updateAppWidget(it, trigger = "external") }
    }

    override fun onAppWidgetOptionsChanged(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int, newOptions: Bundle) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)
        AppWidgetPreferences.setWidgetDimensionsForWidgetId(appWidgetId, newOptions)
        updateLayout(appWidgetId, BaseInjector.get().localizedContext(), appWidgetManager)
    }

    override fun onDeleted(context: Context, appWidgetIds: IntArray) {
        // When the user deletes the widget, delete the preference associated with it.
        for (appWidgetId in appWidgetIds) {
            Napier.i(tag = tag, message = "Deleting widget $appWidgetId")
            lastUpdateJobs[appWidgetId]?.cancel()
            lastUpdateJobs.remove(appWidgetId)
            AppWidgetPreferences.deletePreferencesForWidgetId(appWidgetId)
        }
    }

    companion object {
        private const val tag = "WebcamAppWidget"
        private const val LIVE_FOR_MS = 30_000L
        private const val FETCH_TIMEOUT_MS = 15_000L
        private const val MAX_BITMAP_SIZE = 720
        private const val WEBCAM_LIVE_SAMPLE_RATE_MS = 500L
        private var lastUpdateJobs = mutableMapOf<Int, Job>()

        internal fun cancelAllUpdates() {
            Napier.i(tag = tag, message = "Cancelling all updated")
            lastUpdateJobs.entries.toList().forEach {
                it.value.cancel()
                lastUpdateJobs.remove(it.key)
            }
        }

        internal fun notifyWidgetDataChanged() {
            val context = BaseInjector.get().localizedContext()
            val manager = AppWidgetManager.getInstance(context)
            manager.getAppWidgetIds(ComponentName(context, NoControlsWebcamAppWidget::class.java))
                .filter { ensureWidgetExists(it) }
                .forEach {
                    updateAppWidget(it, trigger = "notify")
                }
            manager.getAppWidgetIds(ComponentName(context, ControlsWebcamAppWidget::class.java))
                .filter { ensureWidgetExists(it) }
                .forEach {
                    updateAppWidget(it, trigger = "notify")
                }
        }

        private fun showUpdating(context: Context, appWidgetId: Int) {
            Napier.i(tag = tag, message = "Applying updating state to $appWidgetId")
            val views = RemoteViews(context.packageName, R.layout.app_widget_webcam)
            views.setViewVisibility(R.id.updatedAt, true)
            views.setViewVisibility(R.id.live, false)
            views.setImageViewBitmap(R.id.webcamContentPlaceholder, generateImagePlaceHolder(appWidgetId))
            views.setTextViewText(R.id.updatedAt, context.getString(R.string.app_widget___updating))
            AppWidgetManager.getInstance(context).partiallyUpdateAppWidget(appWidgetId, views)
        }

        private fun showFailed(context: Context, appWidgetId: Int, instanceId: String?) {
            Napier.i(tag = tag, message = "Applying failed state to $appWidgetId")
            val views = RemoteViews(context.packageName, R.layout.app_widget_webcam)
            views.setViewVisibility(R.id.updatedAt, true)
            views.setViewVisibility(R.id.live, false)
            views.setImageViewBitmap(R.id.webcamContentPlaceholder, generateImagePlaceHolder(appWidgetId))
            views.setTextViewText(R.id.updatedAt, createUpdateFailedText(context, appWidgetId))
            views.setOnClickPendingIntent(R.id.buttonRefresh, createUpdateIntent(context, appWidgetId))
            views.setOnClickPendingIntent(R.id.root, createLaunchAppIntent(context, instanceId))
            AppWidgetManager.getInstance(context).partiallyUpdateAppWidget(appWidgetId, views)
        }

        internal fun updateAppWidget(appWidgetId: Int, trigger: String, playLive: Boolean = false, isManualRefresh: Boolean = false) {
            val previous = lastUpdateJobs[appWidgetId]
            val updateId = uuid4().toString().take(4)


            if (isManualRefresh) {
                Napier.d(tag = tag, message = "[$updateId] Cancelling previous update on $appWidgetId, manual refresh")
                previous?.cancel()
            }

            if (previous?.isActive == true) {
                Napier.i(tag = tag, message = "[$updateId] Skipping update on $appWidgetId, previous still active")
                return
            } else {
                Napier.i(tag = tag, message = "[$updateId] Updating webcam widget $appWidgetId triggered by $trigger")
            }

            lastUpdateJobs[appWidgetId] = AppScope.launch {
                withTimeoutOrNull(1.seconds) {
                    Napier.d(tag = tag, message = "[$updateId] Waiting for previous to finish")
                    previous?.cancelAndJoin()
                }

                Napier.d(tag = tag, message = "[$updateId] Updating webcam widget $appWidgetId")

                val context = BaseInjector.get().localizedContext()
                val appWidgetManager = AppWidgetManager.getInstance(context)
                val hasControls = appWidgetManager.getAppWidgetInfo(appWidgetId).provider.className == ControlsWebcamAppWidget::class.java.name
                val instanceId = AppWidgetPreferences.getInstanceForWidgetId(appWidgetId) ?: "noid"

                // Load frame or do live stream
                try {
                    val octoPrintInfo = BaseInjector.get().octorPrintRepository().let { repo ->
                        repo.get(instanceId)
                            ?: repo.getActiveInstanceSnapshot()
                            ?: let {
                                Napier.v(tag = tag, message = "[$updateId] Unable to find configuration for $instanceId, cancelling")
                                showFailed(context, appWidgetId, instanceId)
                                return@launch
                            }
                    }

                    // Push loading state
                    showUpdating(context, appWidgetId)

                    val frame = withContext(Dispatchers.IO) {
                        if (playLive) withTimeoutOrNull(LIVE_FOR_MS) {
                            doLiveStream(context, octoPrintInfo, instanceId, appWidgetManager, appWidgetId)
                        } else withTimeout(FETCH_TIMEOUT_MS) {
                            val illuminate = isManualRefresh || BaseInjector.get().octoPreferences().automaticLightsForWidgetRefresh
                            createBitmapFlow(octoPrintInfo, illuminate).first()
                        }
                    }

                    // Push loaded frame and end live stream
                    val views = createViews(
                        context = context,
                        widgetId = appWidgetId,
                        instanceId = instanceId,
                        updatedAtText = (if (frame == null) createUpdateFailedText(context, appWidgetId) else createUpdatedNowText()).takeIf { hasControls },
                        live = false,
                        frame = frame
                    )
                    views.setOnClickPendingIntent(R.id.buttonRefresh, createUpdateIntent(context, appWidgetId, false))
                    views.setOnClickPendingIntent(R.id.buttonLive, createUpdateIntent(context, appWidgetId, true))
                    views.setViewVisibility(R.id.buttonRefresh, hasControls)
                    views.setViewVisibility(R.id.buttonLive, hasControls)
                    appWidgetManager.updateAppWidget(appWidgetId, views)
                    frame?.let {
                        AppWidgetPreferences.setImageDimensionsForWidgetId(appWidgetId, it.width, it.height)
                    }

                    if (frame != null) {
                        AppWidgetPreferences.setLastUpdateTime(appWidgetId)
                    }

                } catch (e: CancellationException) {
                    Napier.d(tag = tag, message = "[$updateId] Update cancelled")
                    showFailed(context, appWidgetId = appWidgetId, instanceId = instanceId)
                } catch (e: Exception) {
                    Napier.e(tag = tag, throwable = e, message = "Failed")
                    showFailed(context, appWidgetId = appWidgetId, instanceId = instanceId)
                } finally {
                    Napier.i(tag = tag, message = "[$updateId] Update completed")
                }
            }
        }

        private suspend fun doLiveStream(
            context: Context,
            octoPrintInfo: PrinterConfigurationV3?,
            instanceId: String?,
            appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ): Bitmap? = withContext(Dispatchers.IO) {
            var frame: Bitmap? = null
            val lock = ReentrantLock()
            // Thread A: Load webcam images
            launch {
                createBitmapFlow(
                    octoPrintInfo,
                    sampleRateMs = WEBCAM_LIVE_SAMPLE_RATE_MS,
                    illuminateIfPossible = true
                ).collect {
                    Napier.v(tag = tag, message = "Received frame")
                    lock.withLock { frame = it }
                }
            }

            // Thread B: Update UI every 100ms
            launch {
                val start = System.currentTimeMillis()
                while (isActive) {
                    val savedFrame = lock.withLock { frame } ?: continue // No frame yet, we keep "Updating..."
                    val secsLeft = (System.currentTimeMillis() - start) / 1000
                    val views = createViews(
                        context = context,
                        widgetId = appWidgetId,
                        instanceId = instanceId,
                        updatedAtText = createLiveForText(context, secsLeft.toInt()),
                        live = true,
                        frame = savedFrame
                    )
                    views.setViewVisibility(R.id.buttonCancelLive, true)
                    views.setOnClickPendingIntent(R.id.buttonCancelLive, createUpdateIntent(context, appWidgetId, false))
                    appWidgetManager.updateAppWidget(appWidgetId, views)
                    Napier.v(tag = tag, message = "Pushed frame")
                    delay(WEBCAM_LIVE_SAMPLE_RATE_MS)
                }
            }
            return@withContext frame
        }

        private suspend fun createBitmapFlow(
            octoPrintInfo: PrinterConfigurationV3?,
            illuminateIfPossible: Boolean,
            sampleRateMs: Long = 1
        ) = BaseInjector.get().getWebcamSnapshotUseCase2().execute(
            GetWebcamSnapshotUseCase.Params(
                instanceId = octoPrintInfo?.id,
                maxSize = MAX_BITMAP_SIZE,
                interval = sampleRateMs,
                illuminateIfPossible = illuminateIfPossible,
            )
        ).map { it.bitmap }

        private fun createLiveForText(context: Context, liveSinceSecs: Int) = context.getString(R.string.app_widget___live_for_x, (LIVE_FOR_MS / 1000) - liveSinceSecs)

        private fun createViews(
            context: Context,
            widgetId: Int,
            instanceId: String?,
            updatedAtText: String?,
            live: Boolean,
            frame: Bitmap?
        ): RemoteViews {
            val views = RemoteViews(context.packageName, R.layout.app_widget_webcam)
            frame?.let {
                views.setImageViewBitmap(R.id.webcamContent, it)
            } ?: run {
                // This generated bitmap will ensure the widget has it's final dimension and layout.
                // We set it to a separate view as the webcamContent might already have a "real" image we don't know about
                views.setImageViewBitmap(R.id.webcamContentPlaceholder, generateImagePlaceHolder(widgetId))
            }
            views.setTextViewText(R.id.updatedAt, updatedAtText)
            views.setTextViewText(R.id.live, updatedAtText)
            views.setViewVisibility(R.id.updatedAt, !live)
            views.setViewVisibility(R.id.live, live)
            views.setViewVisibility(R.id.buttonCancelLive, false)
            views.setViewVisibility(R.id.buttonRefresh, false)
            views.setViewVisibility(R.id.buttonLive, false)
            views.setViewVisibility(R.id.updatedAt, !updatedAtText.isNullOrBlank())
            views.setViewVisibility(R.id.noImageCont, frame == null)
            views.setClipToOutLine()
            views.setOnClickPendingIntent(R.id.root, createLaunchAppIntent(context, instanceId))
            applyDebugOptions(views, widgetId)
            return views
        }

        private fun updateLayout(appWidgetId: Int, context: Context, manager: AppWidgetManager) {
            val views = RemoteViews(context.packageName, R.layout.app_widget_webcam)
            manager.partiallyUpdateAppWidget(appWidgetId, views)
        }

        private fun generateImagePlaceHolder(widgetId: Int) = AppWidgetPreferences.getImageDimensionsForWidgetId(widgetId).let { size ->
            Napier.d(tag = tag, message = "Generating placeholder for widget $widgetId: ${size.first}x${size.second}px")
            Bitmap.createBitmap(size.first.takeIf { it > 0 } ?: 960, size.second.takeIf { it > 0 } ?: 540, Bitmap.Config.ARGB_8888)
        }
    }
}