package de.crysxd.octoapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.play.core.review.ReviewManagerFactory
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.baseui.ext.launchWhenResumedFixed
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.collectSaveWithRetry
import de.crysxd.octoapp.viewmodels.AppReviewViewModelCore
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch

class AppReviewFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = View(requireContext())
    private val preferences by lazy { SharedBaseInjector.get().preferences }
    private val viewModel: ViewModel by viewModels()
    private val tag = "AppReviewFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            viewModel.reviewFlowTrigger.collectSaveWithRetry {
                launchReviewFlow()
            }
        }
    }

    override fun onStart() {
        super.onStart()

        // Count app launches
        preferences.reviewFlowConditions = preferences.reviewFlowConditions.copy(appLaunchCounter = preferences.reviewFlowConditions.appLaunchCounter + 1)
    }


    private fun launchReviewFlow() = lifecycleScope.launchWhenResumedFixed {
        Napier.d(tag = tag, message = "Launching review flow")
        OctoAnalytics.logEvent(OctoAnalytics.Event.ReviewFlowLaunched)
        val manager = ReviewManagerFactory.create(requireContext())
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener {
            lifecycleScope.launchWhenCreatedFixed {
                if (it.isSuccessful) {
                    // We got the ReviewInfo object
                    val reviewInfo = it.result
                    val flow = manager.launchReviewFlow(requireActivity(), reviewInfo)
                    flow.addOnCompleteListener { _ ->
                        Napier.d(tag = tag, message = "Review flow completed")
                    }
                } else {
                    OctoAnalytics.logEvent(OctoAnalytics.Event.ReviewFlowFailed, mapOf("reason" to "${it::class.qualifiedName}"))
                    Napier.w(tag = tag, message = "Review flow not successful", throwable = it.exception)
                }
            }
        }
    }

    class ViewModel : BaseViewModel() {
        private val core = AppReviewViewModelCore()
        val reviewFlowTrigger = core.reviewFlowTrigger
    }
}