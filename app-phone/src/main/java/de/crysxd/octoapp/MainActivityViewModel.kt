package de.crysxd.octoapp

import android.net.Uri
import androidx.lifecycle.ViewModel
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import io.github.aakira.napier.Napier

class MainActivityViewModel : ViewModel() {
    private val tag = "MainActivityViewModel"
    var lastSuccessfulCapabilitiesUpdate = 0L
    var pendingUri: Uri? = null
    var lastNavigation: Navigation? = null
    var lastWebUrlAndApiKey: String? = "initial"
    var pendingNavigation: Navigation? = null
    var pendingFlagNavigation: Navigation? = null
    var connectionType: ConnectionType? = null
        set(value) {
            previousConnectionType = field
            Napier.i(tag = tag, message = "Connection type now $value, was $field")
            field = value
        }
    var previousConnectionType: ConnectionType? = null
    var lastFlagsInstanceId: String? = null
    var lastFlags: PrinterState.Flags? = null
    var sameFlagsCounter = 0

    sealed class Navigation(val action: Int, val destination: Int) {
        data object SignIn : Navigation(R.id.action_sign_in_required, R.id.action_sign_in_required)
        data object Connect : Navigation(R.id.action_show_controls, R.id.controlsFragment)
        data object Prepare : Navigation(R.id.action_show_controls, R.id.controlsFragment)
        data object Print : Navigation(R.id.action_show_controls, R.id.controlsFragment)
    }
}