package de.crysxd.octoapp.engine.models.commands

sealed class JobCommand {
    data object PauseJobCommand : JobCommand()
    data object ResumeJobCommand : JobCommand()
    data object CancelJobCommand : JobCommand()
}