package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerSubscriptionResult(
    @SerialName("eventtime") val eventTime: MoonrakerInstant,
    @SerialName("status") val items: MoonrakerPrinterObjectsQuery.StatusItems = MoonrakerPrinterObjectsQuery.StatusItems()
)