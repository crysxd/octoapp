package de.crysxd.octoapp.engine.octoprint.exception

import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException

class NotSupportedOnOctoPrintException(val feature: String) : SuppressedIllegalStateException("'$feature' is not yet implemeneted"), UserMessageException {
    override val userMessage = "This feature is not yet supported on Klipper and Moonraker ($feature)"
}
