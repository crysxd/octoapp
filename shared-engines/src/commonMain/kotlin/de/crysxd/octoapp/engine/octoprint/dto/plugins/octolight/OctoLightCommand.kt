package de.crysxd.octoapp.engine.octoprint.dto.plugins.octolight

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoLightCommand(
    override val command: String = "toggle"
) : CommandBody
