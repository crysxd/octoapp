package de.crysxd.octoapp.engine.dto

import de.crysxd.octoapp.sharedcommon.utils.HexColor
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SpoolmanSpool(
    @SerialName("archived") val archived: Boolean? = null,
    @SerialName("filament") val filament: Filament? = null,
    @SerialName("first_used") val firstUsed: String? = null,
    @SerialName("id") val id: Int,
    @SerialName("last_used") val lastUsed: String? = null,
    @SerialName("registered") val registered: String? = null,
    @SerialName("remaining_length") val remainingLength: Float? = null,
    @SerialName("remaining_weight") val remainingWeight: Float? = null,
    @SerialName("used_length") val usedLength: Float? = null,
    @SerialName("used_weight") val usedWeight: Float? = null
) {
    @Serializable
    data class Filament(
        @SerialName("color_hex") val colorHex: HexColor? = null,
        @SerialName("density") val density: Float? = null,
        @SerialName("diameter") val diameter: Float? = null,
        @SerialName("id") val id: Int,
        @SerialName("material") val material: String? = null,
        @SerialName("name") val name: String? = null,
        @SerialName("price") val price: Float? = null,
        @SerialName("registered") val registered: String? = null,
        @SerialName("vendor") val vendor: Vendor? = null,
        @SerialName("weight") val weight: Float? = null
    ) {
        @Serializable
        data class Vendor(
            @SerialName("id") val id: Int,
            @SerialName("name") val name: String? = null,
            @SerialName("registered") val registered: String? = null
        )
    }
}