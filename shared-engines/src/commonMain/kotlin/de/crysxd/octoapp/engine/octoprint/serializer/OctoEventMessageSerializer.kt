package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.dto.message.CancelObjectPluginMessage
import de.crysxd.octoapp.engine.octoprint.dto.message.CompanionPluginMessage
import de.crysxd.octoapp.engine.octoprint.dto.message.NgrokPluginMessage
import de.crysxd.octoapp.engine.octoprint.dto.message.OctolapsePluginMessage
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PolymorphicKind
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonNull
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.intOrNull
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

class OctoEventMessageSerializer : KSerializer<OctoMessage?> {

    @OptIn(InternalSerializationApi::class, ExperimentalSerializationApi::class)
    override val descriptor: SerialDescriptor = buildSerialDescriptor("OctoMessage", PolymorphicKind.OPEN)

    override fun deserialize(decoder: Decoder): OctoMessage? {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonObject
        return deserialize(tree)
    }

    fun deserialize(jsonObject: JsonObject) = when {
        jsonObject.contains("current") -> EngineJson.decodeFromJsonElement<OctoMessage.Current>(jsonObject["current"]!!.jsonObject)
        jsonObject.contains("history") -> EngineJson.decodeFromJsonElement<OctoMessage.Current>(jsonObject["history"]!!.jsonObject).copy(isHistoryMessage = true)
        jsonObject.contains("connected") -> EngineJson.decodeFromJsonElement<OctoMessage.Connected>(jsonObject["connected"]!!.jsonObject)
        jsonObject.contains("reauthRequired") -> OctoMessage.ReAuthRequired()
        jsonObject.contains("plugin") -> deserializePluginMessage(jsonObject["plugin"]!!.jsonObject)
        jsonObject.contains("event") -> deserializeEventMessage(jsonObject["event"]!!.jsonObject)
        else -> OctoMessage.UnknownMessage()
    }

    private fun deserializePluginMessage(jsonObject: JsonObject): OctoMessage? = when (jsonObject["plugin"]!!.jsonPrimitive.content) {
        "octoapp" -> EngineJson.decodeFromJsonElement<CompanionPluginMessage>(jsonObject["data"]!!.jsonObject)
        "octolapse" -> EngineJson.decodeFromJsonElement<OctolapsePluginMessage>(jsonObject["data"]!!.jsonObject).takeIf {
            // Octolapse is quite spammy causing issues with skipped messages. Only take in what we need
            it.type != null || it.errors != null || it.snapshotPlanPreview != null
        }

        "ngrok" -> EngineJson.decodeFromJsonElement<NgrokPluginMessage>(jsonObject["data"]!!.jsonObject)
        "cancelobject" -> {
            val data = jsonObject["data"]!!.jsonObject
            when {
                data.containsKey("ActiveID") -> EngineJson.decodeFromJsonElement<CancelObjectPluginMessage.Active>(data)
                data.containsKey("objects") -> EngineJson.decodeFromJsonElement<CancelObjectPluginMessage.ObjectList>(data)
                else -> CancelObjectPluginMessage.Other
            }
        }

        else -> OctoMessage.UnknownMessage()
    }

    private fun deserializeEventMessage(jsonObject: JsonObject): OctoMessage.Event = when (val type = jsonObject["type"]!!.jsonPrimitive.content) {
        "Connected" -> OctoMessage.Event.PrinterConnected(
            baudrate = jsonObject["payload"]?.jsonObject?.get("baudrate")?.jsonPrimitive?.intOrNull,
            port = jsonObject["payload"]?.jsonObject?.get("port")?.jsonPrimitive?.contentOrNull
        )

        "SettingsUpdated" -> OctoMessage.Event.SettingsUpdated(
            configHash = jsonObject["payload"]?.takeUnless { it == JsonNull }?.jsonObject?.get("config_hash")?.jsonPrimitive?.contentOrNull
        )

        "FirmwareData" -> EngineJson.decodeFromJsonElement<OctoMessage.Event.FirmwareData>(jsonObject["payload"]!!)
        "FileSelected" -> EngineJson.decodeFromJsonElement<OctoMessage.Event.FileSelected>(jsonObject["payload"]!!)
        "PrinterStateChanged" -> OctoMessage.Event.PrinterStateChanged
        "Connecting" -> OctoMessage.Event.Connecting
        "Disconnected" -> OctoMessage.Event.Disconnected
        "UpdatedFiles" -> OctoMessage.Event.UpdatedFiles
        "PrintStarted" -> OctoMessage.Event.PrintStarted
        "PrintCancelling" -> OctoMessage.Event.PrintCancelling
        "PrintCancelled" -> OctoMessage.Event.PrintCancelled
        "PrintPausing" -> OctoMessage.Event.PrintPausing
        "PrintPaused" -> OctoMessage.Event.PrintPaused
        "PrintFailed" -> OctoMessage.Event.PrintFailed
        "PrintDone" -> OctoMessage.Event.PrintDone
        "PrintResumed" -> OctoMessage.Event.PrintResumed
        "PrinterProfileModified" -> OctoMessage.Event.PrinterProfileModified
        "MovieRendering" -> OctoMessage.Event.MovieRendering
        "MovieDone" -> OctoMessage.Event.MovieDone
        "MovieFailed" -> OctoMessage.Event.MovieFailed
        else -> OctoMessage.Event.Unknown(type = type)
    }

    override fun serialize(encoder: Encoder, value: OctoMessage?) {
        throw UnsupportedOperationException("Not implemented")
    }
}