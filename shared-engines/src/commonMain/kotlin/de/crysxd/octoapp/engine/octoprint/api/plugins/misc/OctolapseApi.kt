package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseCommandBody
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseStateAndSettings
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText

class OctolapseApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) {


    suspend fun getSettingsAndState() = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "plugin", OctoPlugins.Octolapse, "loadSettingsAndState")
        }.octolapseBody<OctolapseStateAndSettings>()
    }

    suspend fun cancelPreprocessing(jobId: String?) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "plugin", OctoPlugins.Octolapse, "cancelPreprocessing")
            setJsonBody(OctolapseCommandBody(cancel = true, jobId = jobId))
        }.octolapseBody<OctolapseStateAndSettings>()
    }

    suspend fun acceptSnapshotPlan(jobId: String?) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "plugin", OctoPlugins.Octolapse, "acceptSnapshotPlanPreview")
            setJsonBody(OctolapseCommandBody(jobId = jobId))
        }.octolapseBody<OctolapseStateAndSettings>()
    }

    private suspend inline fun <reified T> HttpResponse.octolapseBody(): T = EngineJson.decodeFromString(bodyAsText())
}