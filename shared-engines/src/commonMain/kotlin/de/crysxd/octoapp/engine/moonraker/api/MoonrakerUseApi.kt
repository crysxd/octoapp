package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.UserApi
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.models.login.User

internal class MoonrakerUseApi(
    private val probeApi: MoonrakerProbeApi
) : UserApi {
    override suspend fun getCurrentUser() = try {
        probeApi.probe(throwException = true)
        User(name = "moonraker")
    } catch (e: InvalidApiKeyException) {
        User(name = "moonraker", groups = listOf("guests"))
    }
}