package de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoFilamentManagerSpool(
    val id: String,
    val name: String? = null,
    val density: Float? = null,
    val profile: Profile = Profile(),
    val weight: Float? = null,
    val used: Float? = null,
) {
    @Serializable
    data class Profile(
        val vendor: String? = null,
        val material: String? = null,
    )
}