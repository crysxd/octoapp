package de.crysxd.octoapp.engine.octoprint.dto.login

import kotlinx.serialization.Serializable

@Serializable
@Suppress("unused")
internal class OctoLoginBody(
    val passive: Boolean = true,
    val user: String? = null,
    val pass: String? = null,
    val remember: Boolean = false
)
