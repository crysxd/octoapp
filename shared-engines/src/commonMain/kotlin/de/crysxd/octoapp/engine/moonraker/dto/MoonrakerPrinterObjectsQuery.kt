package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerPrinterObjectsQueryStatusItemSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerPrinterObjectsQuery(
    @SerialName("eventtime") val eventtime: MoonrakerInstant? = null,
    @SerialName("status") val status: StatusItems? = null
) {
    @Serializable(with = MoonrakerPrinterObjectsQueryStatusItemSerializer::class)
    data class StatusItems(val items: Map<String, MoonrakerStatusItem?> = emptyMap()) : Map<String, MoonrakerStatusItem?> by items
}

internal inline fun <reified T : MoonrakerStatusItem> Map<String, MoonrakerStatusItem?>.getItem(item: String) = this[item] as? T

internal fun MoonrakerPrinterObjectsQuery.StatusItems.backFillWith(previous: MoonrakerPrinterObjectsQuery.StatusItems) = MoonrakerPrinterObjectsQuery.StatusItems(
    items = (items + previous.filterKeys { !items.containsKey(it) }).mapValues { (key, value) ->
        value ?: return@mapValues previous.items[key]
        val previousItem = previous.items[key] ?: return@mapValues value
        value.backFillWithPrevious(previousItem)
    }
)