package de.crysxd.octoapp.engine.models.event

import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

sealed class Event {
    val localTime: Instant = Clock.System.now()

    data class Connected(val connectionType: ConnectionType, val connectionQuality: ConnectionQuality) : Event()
    data class Disconnected(val exception: Throwable? = null, val connectionAttemptCounter: Int) : Event()
    data class MessageReceived(val message: Message, private val isSelfGenerated: Boolean = false) : Event()
}