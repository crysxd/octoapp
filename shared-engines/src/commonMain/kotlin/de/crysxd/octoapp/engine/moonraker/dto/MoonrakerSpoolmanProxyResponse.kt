package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.dto.SpoolmanSpool
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerSpoolmanProxyResponse(
    @SerialName("error") val error: Error? = null,
    @SerialName("response") val response: List<SpoolmanSpool>? = null
) {
    @Serializable
    data class Error(
        @SerialName("status_code") val statusCode: Int? = null,
        @SerialName("message") val message: String? = null,
    )
}