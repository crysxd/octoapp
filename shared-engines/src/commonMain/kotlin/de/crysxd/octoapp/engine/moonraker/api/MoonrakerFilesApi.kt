package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.FilesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerEventHandler
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDirectoryContent
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileActionResult
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileMetadata
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileReferenceList
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerCopyFileParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerCreateDirectoryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerDeleteDirectoryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerDeleteFileParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerFilesMetadataParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerGetDirectoryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerListFilesParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerMoveFileParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerStartPrintParams
import de.crysxd.octoapp.engine.moonraker.mappers.map
import de.crysxd.octoapp.sharedcommon.Constants
import io.ktor.client.HttpClient
import io.ktor.client.plugins.expectSuccess
import io.ktor.client.plugins.onDownload
import io.ktor.client.plugins.onUpload
import io.ktor.client.request.forms.InputProvider
import io.ktor.client.request.forms.formData
import io.ktor.client.request.forms.submitFormWithBinaryData
import io.ktor.client.request.header
import io.ktor.client.request.prepareGet
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.ContentType
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders
import io.ktor.utils.io.ByteReadChannel
import kotlinx.serialization.json.JsonElement

internal class MoonrakerFilesApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val requestHandler: MoonrakerRequestHandler,
    private val eventHandler: MoonrakerEventHandler,
) : FilesApi {

    override suspend fun getAllFiles(
        origin: FileOrigin
    ) = requestHandler.sendRequest<MoonrakerFileReferenceList>(
        method = "server.files.list",
        params = MoonrakerListFilesParams(
            when (origin) {
                FileOrigin.Gcode -> "gcodes"
                is FileOrigin.Other -> origin.name
            }
        )
    ).map(origin)

    override suspend fun getFile(
        origin: FileOrigin,
        path: String,
        isDirectory: Boolean
    ): FileObject = if (isDirectory) {
        requestHandler.sendRequest<MoonrakerDirectoryContent>(
            method = "server.files.get_directory",
            params = MoonrakerGetDirectoryParams(
                path = origin.resolve(path),
                extended = true,
            )
        ).map(path, origin)
    } else {
        requestHandler.sendRequest<MoonrakerFileMetadata>(
            method = "server.files.metadata",
            params = MoonrakerFilesMetadataParams(filename = path)
        ).map(path, origin)
    }

    override suspend fun executeFileCommand(
        file: FileReference,
        command: FileCommand
    ) = requestHandler.sendRequest<JsonElement>(
        method = when (command) {
            is FileCommand.CopyFile -> "server.files.copy"
            is FileCommand.MoveFile -> "server.files.move"
            FileCommand.PrintFile -> "printer.print.start"
        },
        params = when (command) {
            is FileCommand.CopyFile -> MoonrakerCopyFileParams(
                source = file.moonrakerPath,
                dest = file.origin.resolve(command.destination),
            )

            is FileCommand.MoveFile -> MoonrakerMoveFileParams(
                source = file.moonrakerPath,
                dest = file.origin.resolve(command.destination),
            )

            FileCommand.PrintFile -> MoonrakerStartPrintParams(
                filename = file.path
            )
        }
    ).emitFilesChangedEvent(file = file, isStartPrint = command == FileCommand.PrintFile)

    override suspend fun deleteFile(
        fileObject: FileReference
    ) = requestHandler.sendRequest<MoonrakerFileActionResult>(
        method = if (fileObject is FileReference.Folder) {
            "server.files.delete_directory"
        } else {
            "server.files.delete_file"
        },
        params = if (fileObject is FileReference.Folder) {
            MoonrakerDeleteDirectoryParams(
                path = fileObject.moonrakerPath,
                force = true,
            )
        } else {
            MoonrakerDeleteFileParams(
                path = fileObject.moonrakerPath,
            )
        }
    ).emitFilesChangedEvent(file = fileObject, isStartPrint = false)

    override suspend fun createFolder(
        parent: FileReference.Folder,
        name: String
    ) = requestHandler.sendRequest<MoonrakerFileActionResult>(
        method = "server.files.post_directory",
        params = MoonrakerCreateDirectoryParams(
            "${parent.moonrakerPath}/$name"
        )
    ).emitFilesChangedEvent(file = null, isStartPrint = false)

    override suspend fun uploadFile(
        parent: FileReference.Folder,
        input: InputProvider,
        name: String,
        progressUpdate: (Float) -> Unit
    ) = baseUrlRotator.request<Unit> { baseUrl ->
        httpClient.submitFormWithBinaryData(
            formData {
                append(
                    key = "file",
                    value = input,
                    headers = Headers.build {
                        append(HttpHeaders.ContentDisposition, "filename=\"${name}\"")
                        append(HttpHeaders.ContentType, ContentType.Application.OctetStream.contentType)
                    }
                )
                append(key = "root", value = parent.origin.moonrakerRoot)
                append(key = "path", value = parent.path)
            }
        ) {
            urlFromPath(baseUrl = baseUrl, "server", "files", "upload")
            onUpload { bytesSentTotal, contentLength ->
                progressUpdate(if (contentLength > 0) (bytesSentTotal / contentLength.toFloat()) else -1f)
            }
            attributes.put(Constants.SuppressLogging, true)
        }
    }.emitFilesChangedEvent(file = null, isStartPrint = false)

    override suspend fun <T> downloadFile(
        file: FileReference.File,
        progressUpdate: (Float) -> Unit,
        consume: suspend (ByteReadChannel) -> T
    ): T = baseUrlRotator.request { baseUrl ->
        httpClient.prepareGet {
            urlFromPath(baseUrl = baseUrl, "server", "files", *file.moonrakerPath.splitIntoSegments())
            onDownload { bytesSentTotal, contentLength ->
                val total = contentLength.takeIf { it > 0 } ?: file.size ?: return@onDownload progressUpdate(-1f)
                val progressPercent = bytesSentTotal / total.toFloat()
                progressUpdate(progressPercent.coerceIn(0f..1f))
            }

            // There is an issue with KTOR cache as it saves the entire download in memory at first, we can't have this of course
            // with potentially huge files. Also prevent logging on this request as logging will clog up memory as well.
            header(HttpHeaders.CacheControl, "no-store, no-cache")
            attributes.put(Constants.SuppressLogging, true)
            expectSuccess = true
        }.execute { response ->
            // We must use this execute function to prevent the entire body to be read into memory before the function returns
            consume(response.bodyAsChannel())
        }
    }

    private fun String.splitIntoSegments() = split("/").toTypedArray()

    @Suppress("UnusedReceiverParameter")
    private suspend fun Any.emitFilesChangedEvent(
        file: FileReference?,
        isStartPrint: Boolean
    ) = if (isStartPrint && file is FileReference.File) {
        eventHandler.injectInterpolatedEvent { Message.Event.PrintStarted }
        eventHandler.injectInterpolatedPrintStart(file)
    } else {
        eventHandler.emitEvent(Event.MessageReceived(Message.Event.UpdatedFiles))
    }

    private
    val FileReference.moonrakerPath
        get() = "${origin.moonrakerRoot}/${path.removePrefix("/")}"

    private val FileOrigin.moonrakerRoot
        get() = when (this) {
            FileOrigin.Gcode -> "gcodes"
            is FileOrigin.Other -> name
        }

    private fun FileOrigin.resolve(path: String) = "$moonrakerRoot/${path.removePrefix("/")}".removeSuffix("/")
}