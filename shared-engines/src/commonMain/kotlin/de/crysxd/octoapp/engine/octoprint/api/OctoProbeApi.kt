package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.Constants.ApiKeyHeader
import de.crysxd.octoapp.engine.framework.Constants.SuppressApiKey
import de.crysxd.octoapp.engine.framework.Constants.SuppressBrokenSetup
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoConstants.OCTOPRINT_TAG
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoUser
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.headers

internal class OctoProbeApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) {

    suspend fun probe(throwException: Boolean = false) = try {
        baseUrlRotator.request {
            httpClient.get {
                headers {
                    remove(ApiKeyHeader)
                }
                attributes.put(SuppressApiKey, true)
                attributes.put(SuppressBrokenSetup, true)
                urlFromPath(baseUrl = it, "api", "currentuser")
            }.body<OctoUser>().groups == listOf("guests")
        }
    } catch (e: Exception) {
        Napier.w(tag = OCTOPRINT_TAG, message = "Probe failed: ${e::class.qualifiedName}")
        if (throwException) throw e else false
    }
}
