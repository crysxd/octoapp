package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerCopyFileParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerCreateDirectoryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerDeleteDirectoryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerDeleteFileParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerFilesMetadataParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerGetDeviceParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerGetDirectoryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerGetJobListParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerListFilesParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerMachineServicesRestartParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerMoveFileParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPostDeviceParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterGcodeScriptParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectSubscribeParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectsQueryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerRpcParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerConnectionIdentifyParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerDatabaseGetItemParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerDatabasePostItemParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerFilesMetadataParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerGcodeStoreParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerSpoolmanProxyParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerSpoolmanSelectSpoolParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerStartPrintParams
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

internal class MoonrakerRpcParamsSerializer : KSerializer<MoonrakerRpcParams> {
    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerRpcRequest.Params", kind = PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder) = throw UnsupportedOperationException("Can't deserialize")

    override fun serialize(encoder: Encoder, value: MoonrakerRpcParams) = when (value) {
        is MoonrakerPrinterObjectSubscribeParams -> encoder.encodeSerializableValue(MoonrakerPrinterObjectSubscribeParams.serializer(), value)
        is MoonrakerServerFilesMetadataParams -> encoder.encodeSerializableValue(MoonrakerServerFilesMetadataParams.serializer(), value)
        is MoonrakerServerDatabaseGetItemParams -> encoder.encodeSerializableValue(MoonrakerServerDatabaseGetItemParams.serializer(), value)
        is MoonrakerMachineServicesRestartParams -> encoder.encodeSerializableValue(MoonrakerMachineServicesRestartParams.serializer(), value)
        is MoonrakerPrinterObjectsQueryParams -> encoder.encodeSerializableValue(MoonrakerPrinterObjectsQueryParams.serializer(), value)
        is MoonrakerPrinterGcodeScriptParams -> encoder.encodeSerializableValue(MoonrakerPrinterGcodeScriptParams.serializer(), value)
        is MoonrakerServerGcodeStoreParams -> encoder.encodeSerializableValue(MoonrakerServerGcodeStoreParams.serializer(), value)
        is MoonrakerListFilesParams -> encoder.encodeSerializableValue(MoonrakerListFilesParams.serializer(), value)
        is MoonrakerFilesMetadataParams -> encoder.encodeSerializableValue(MoonrakerFilesMetadataParams.serializer(), value)
        is MoonrakerGetJobListParams -> encoder.encodeSerializableValue(MoonrakerGetJobListParams.serializer(), value)
        is MoonrakerCopyFileParams -> encoder.encodeSerializableValue(MoonrakerCopyFileParams.serializer(), value)
        is MoonrakerCreateDirectoryParams -> encoder.encodeSerializableValue(MoonrakerCreateDirectoryParams.serializer(), value)
        is MoonrakerDeleteDirectoryParams -> encoder.encodeSerializableValue(MoonrakerDeleteDirectoryParams.serializer(), value)
        is MoonrakerDeleteFileParams -> encoder.encodeSerializableValue(MoonrakerDeleteFileParams.serializer(), value)
        is MoonrakerMoveFileParams -> encoder.encodeSerializableValue(MoonrakerMoveFileParams.serializer(), value)
        is MoonrakerStartPrintParams -> encoder.encodeSerializableValue(MoonrakerStartPrintParams.serializer(), value)
        is MoonrakerGetDirectoryParams -> encoder.encodeSerializableValue(MoonrakerGetDirectoryParams.serializer(), value)
        is MoonrakerServerDatabasePostItemParams -> encoder.encodeSerializableValue(MoonrakerServerDatabasePostItemParams.serializer(), value)
        is MoonrakerServerConnectionIdentifyParams -> encoder.encodeSerializableValue(MoonrakerServerConnectionIdentifyParams.serializer(), value)
        is MoonrakerGetDeviceParams -> encoder.encodeSerializableValue(MoonrakerGetDeviceParams.serializer(), value)
        is MoonrakerPostDeviceParams -> encoder.encodeSerializableValue(MoonrakerPostDeviceParams.serializer(), value)
        is MoonrakerSpoolmanSelectSpoolParams -> encoder.encodeSerializableValue(MoonrakerSpoolmanSelectSpoolParams.serializer(), value)
        is MoonrakerSpoolmanProxyParams -> encoder.encodeSerializableValue(MoonrakerSpoolmanProxyParams.serializer(), value)
    }
}