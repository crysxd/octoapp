package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExcludeObject
import io.github.aakira.napier.Napier


internal fun MoonrakerExcludeObject.map() = try {
    Message.Current.PrintObjects(
        currentObject = currentObject,
        excluded = excludedObjects ?: emptyList(),
        all = objects?.map { obj ->
            Message.Current.PrintObjects.Object(
                id = requireNotNull(obj.name) { "Missing name" },
                label = obj.name,
                center = obj.center?.toPair(),
            )
        } ?: emptyList()

    )
} catch (e: Exception) {
    Napier.e(tag = "MoonrakerExcludeObjectMapper", message = "Failed to map", throwable = e)
    null
}

private fun List<Float>.toPair(): Pair<Float, Float> {
    require(size == 2) { "Expected exactly 2 items, got $size" }
    return this[0] to this[1]
}