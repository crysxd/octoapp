package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.framework.json.InstantSerializer
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoHistoricTemperatureData
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonObject

class OctoHistoricTemperatureDataSerializer : KSerializer<OctoHistoricTemperatureData> {

    private val instantSerializer = InstantSerializer()
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor(serialName = "HistoricTemperatureData", kind = PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): OctoHistoricTemperatureData {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonObject

        return OctoHistoricTemperatureData(
            time = EngineJson.decodeFromJsonElement(instantSerializer, requireNotNull(tree["time"]) { "Missing time" }),
            components = tree.keys.filter { it != "time" }.associateWith { EngineJson.decodeFromJsonElement(tree[it]!!) }
        )
    }

    override fun serialize(encoder: Encoder, value: OctoHistoricTemperatureData) {
        throw UnsupportedOperationException("Can't serialize")
    }

}