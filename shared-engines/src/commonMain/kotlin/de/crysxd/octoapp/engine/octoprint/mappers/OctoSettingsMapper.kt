package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.framework.guessWebcamUrlType
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.models.macro.Macro
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Extras
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Type
import de.crysxd.octoapp.engine.octoprint.OctoColorSchemes
import de.crysxd.octoapp.engine.octoprint.dto.plugins.camereastreamercontrol.CameraStreamerControlExtras
import de.crysxd.octoapp.engine.octoprint.dto.plugins.go2rtc.Go2RtcExtras
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoWebcamSettings
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import io.github.aakira.napier.Napier
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement
import kotlin.math.roundToLong

private const val toolCount = 5

internal fun OctoSettings.map(hash: String? = null) = Settings(
    hash = hash,
    webcam = WebcamSettings(
        webcamEnabled = webcam.webcamEnabled,
        webcams = when (webcam) {
            is OctoWebcamSettings.OneDotEightAndOlder -> webcam.streamUrl?.guessWebcamUrlType()?.let { (type, url) ->
                listOf(
                    WebcamSettings.Webcam(
                        name = "octoprint",
                        displayName = "OctoPrint",
                        provider = "octoprint",
                        streamRatio = webcam.streamRatio,
                        flipH = webcam.flipH,
                        flipV = webcam.flipV,
                        rotate90 = webcam.rotate90,
                        snapshotUrl = webcam.snapshotUrl?.trim(),
                        streamUrl = url?.trim(),
                        type = type,
                    )
                )
            } ?: emptyList()

            is OctoWebcamSettings.OneDotNineAndNewer -> webcam.webcams.flatMap { webcam ->
                val extrasJson = webcam.extras ?: JsonObject(emptyMap())
                val subWebcams = when (webcam.provider) {
                    // For the camera streamer control, we pick up WebRTC if configured but we still need to support
                    // MJPEG in case a user does not have supporter perks. This allows the user to use the MJPEG webcam instead
                    "camerastreamer_control" -> EngineJson.decodeFromJsonElement<CameraStreamerControlExtras>(extrasJson).let { extras ->
                        listOfNotNull(
                            Triple(
                                Type.WebRtcCameraStreamer,
                                extras.url,
                                Extras.WebRtcExtras(
                                    stunServer = extras.webrtc?.stun,
                                    turnServer = extras.webrtc?.turn,
                                )
                            ).takeIf { extras.mode == CameraStreamerControlExtras.Mode.WebRtc },
                            Triple(
                                Type.Mjpeg,
                                webcam.compat?.stream,
                                Extras.GenericExtras(extrasJson)
                            ).takeUnless { webcam.compat?.stream.isNullOrBlank() }
                        )
                    }

                    "go2rtc" -> listOfNotNull(
                        Triple(
                            Type.WebRtcGoRtc,
                            EngineJson.decodeFromJsonElement<Go2RtcExtras>(extrasJson).stream,
                            Extras.GenericExtras(extrasJson)
                        ),
                    )

                    // Generic webcam. We try to guess the type from the URL but fall back on MJPEG if we can't
                    else -> listOf(webcam.compat?.stream?.guessWebcamUrlType()?.let { (type, url) ->
                        Triple(
                            type,
                            url,
                            Extras.GenericExtras(extrasJson)
                        )
                    } ?: Triple(
                        Type.Mjpeg,
                        null,
                        Extras.GenericExtras(extrasJson)
                    ))
                }

                // Associate all subwebcams with the webcam
                subWebcams.map { webcam to it }
            }.map { (webcam, x) ->
                val (type, url, extras) = x
                WebcamSettings.Webcam(
                    name = webcam.name,
                    displayName = webcam.displayName ?: webcam.name,
                    provider = webcam.provider ?: "unknown",
                    flipH = webcam.flipH,
                    flipV = webcam.flipV,
                    rotate90 = webcam.rotate90,
                    snapshotUrl = webcam.compat?.snapshot?.trim(),
                    streamUrl = url,
                    snapshotDisplay = webcam.snapshotDisplay,
                    extras = extras,
                    canSnapshot = webcam.canSnapshot,
                    type = type,
                )
            }
        }
    ),
    bedMesh = plugins.bedLevelVisualizer?.let { bedLevel ->
        val (graphZMin, graphZMax) = try {
            val (a, b) = bedLevel.graphZLimits?.split(",")?.takeIf { it.size > 1 } ?: listOf("-1", "1")
            a.toFloat() to b.toFloat()
        } catch (e: NumberFormatException) {
            Napier.e(tag = "SettingsMapper", message = "Failed to parse graph min/max due to invlaid input, using default values")
            -1f to 1f
        } catch (e: Exception) {
            Napier.e(tag = "SettingsMapper", throwable = e, message = "Failed to parse graph min/max, using default values")
            -1f to 1f
        }

        fun List<String?>.toFloat(): List<Float>? {
            val res = map { it?.toFloatOrNull() }
            return if (res.any { it == null }) {
                Napier.e(tag = "SettingsMapper", message = "Unable to parse some floats for BedLevelVisualizer: $this")
                null
            } else {
                res.filterNotNull()
            }
        }

        Settings.BedMesh(
            mesh = bedLevel.mesh?.map { x -> x.map { y -> y?.toFloatOrNull() } },
            meshX = bedLevel.meshX?.toFloat(),
            meshY = bedLevel.meshY?.toFloat(),
            refreshCommand = bedLevel.command,
            graphZMin = graphZMin,
            graphZMax = graphZMax,
            colorScale = bedLevel.colorscale?.let { json ->
                try {
                    val scale = EngineJson.decodeFromString<List<List<String>>>(json)
                    scale.map { colorStop ->
                        require(colorStop.size == 2) { "Expected exactly 2 elements, got $colorStop" }
                        val position = requireNotNull(colorStop[0].toFloatOrNull()) { "Epected ${colorStop[0]} to be Float but was not" }
                        val color = HexColor(colorStop[1])

                        // Scale to what we need. Comes as 0..1 but we need -1..1
                        ((position - 0.5f) * 2) to color
                    }
                } catch (e: Exception) {
                    Napier.e(tag = "SettingsMapper", message = "Failed to parse color scale, using default", throwable = e)
                    null
                }
            } ?: Settings.BedMesh.DefaultColorScale
        )
    },
    appearance = Settings.Appearance(
        name = appearance.name,
        colors = Settings.Appearance.Colors(
            themeName = appearance.color,
            light = OctoColorSchemes.Light.fromString(appearance.color),
            dark = OctoColorSchemes.Dark.fromString(appearance.color),
        )
    ),
    temperaturePresets = temperature.profiles.map { profile ->
        Settings.TemperaturePreset(
            name = profile.name,
            components = listOfNotNull(
                profile.bed?.let {
                    listOf(
                        Settings.TemperaturePreset.Component(
                            key = "bed",
                            temperature = it,
                            isBed = true,
                        )
                    )
                },
                profile.chamber?.let {
                    listOf(
                        Settings.TemperaturePreset.Component(
                            key = "chamber",
                            temperature = it,
                            isChamber = true,
                        )
                    )
                },
                profile.extruder?.let {
                    (0..<toolCount).map { index ->
                        Settings.TemperaturePreset.Component(
                            key = "tool$index",
                            temperature = it,
                            isExtruder = true,
                            displayName = "#${index + 1}"
                        )
                    }
                }
            ).flatten()
        )
    },
    coolDownProfile = Settings.TemperaturePreset(
        name = "__cooldown",
        components = listOf(
            Settings.TemperaturePreset.Component(key = "bed", temperature = 0f, isBed = true),
            Settings.TemperaturePreset.Component(key = "chamber", temperature = 0f, isChamber = true),
        ) + (0..<toolCount).map { index ->
            Settings.TemperaturePreset.Component(key = "tool$index", displayName = "#${index + 1}", temperature = 0f, isExtruder = true)
        },
    ),
    terminalFilters = terminalFilters.map {
        Settings.TerminalFilter(
            name = it.name,
            regex = it.regex,
        )
    },
    plugins = Settings.PluginSettingsGroup(
        gcodeViewer = plugins.gcodeViewer?.let {
            Settings.GcodeViewer(
                mobileSizeThreshold = it.mobileSizeThreshold?.roundToLong() ?: 0L,
                sizeThreshold = it.sizeThreshold?.roundToLong() ?: 0L,
            )
        },
        octoEverywhere = plugins.octoEverywhere?.let {
            Settings.OctoEverywhere
        },
        cancelObject = plugins.cancelObject?.let {
            Settings.CancelObject
        },
        discovery = plugins.discovery?.let {
            Settings.Discovery(
                uuid = it.uuid,
            )
        },
        mmu = plugins.mmu2FilamentSelect?.let { mmu ->
            Settings.Mmu(
                plugin = Settings.Mmu.Plugin.Mmu2FilamentSelect,
                filament = listOf(
                    mmu.filament1,
                    mmu.filament2,
                    mmu.filament3,
                    mmu.filament4,
                    mmu.filament5,
                ).mapIndexed { index, name ->
                    Settings.Mmu.Filament(
                        name = name?.takeUnless { it.isBlank() },
                        toolIndex = index,
                        color = null,
                    )
                },
                labelSource = when (mmu.labelSource) {
                    OctoSettings.Mmu2FilamentSelect.LabelSource.Manual -> Settings.Mmu.LabelSource.Manual
                    OctoSettings.Mmu2FilamentSelect.LabelSource.FilamentManager -> Settings.Mmu.LabelSource.FilamentManager
                    OctoSettings.Mmu2FilamentSelect.LabelSource.SpoolManager -> Settings.Mmu.LabelSource.SpoolManager
                }
            )
        } ?: plugins.prusaMmu?.let { mmu ->
            Settings.Mmu(
                plugin = Settings.Mmu.Plugin.PrusaMmu,
                filament = mmu.filament?.map { filament ->
                    Settings.Mmu.Filament(
                        name = filament.name,
                        toolIndex = filament.id - 1,
                        color = filament.color?.takeUnless { it.rawValue.isNullOrBlank() },
                    )
                } ?: emptyList(),
                labelSource = when (mmu.filamentSource) {
                    OctoSettings.PrusaMmu.FilamentSource.PrusaMmu -> Settings.Mmu.LabelSource.Manual
                    OctoSettings.PrusaMmu.FilamentSource.FilamentManager -> Settings.Mmu.LabelSource.FilamentManager
                    OctoSettings.PrusaMmu.FilamentSource.SpoolManager -> Settings.Mmu.LabelSource.SpoolManager
                }
            )
        },
        multiCamSettings = plugins.multiCam?.let {
            Settings.MultiCam(
                webcams = it.profiles.filter { profile ->
                    profile.name != "Default"
                }.mapIndexed { index, webcam ->
                    webcam.streamUrl?.guessWebcamUrlType()?.let { (type, url) ->
                        WebcamSettings.Webcam(
                            name = webcam.name ?: "multicam_$index",
                            displayName = webcam.name?.let { name -> "$name (MultiCam)" } ?: "${index + 1} (MultiCam)",
                            streamRatio = webcam.streamRatio,
                            flipH = webcam.flipH,
                            flipV = webcam.flipV,
                            rotate90 = webcam.rotate90,
                            snapshotUrl = webcam.snapshotUrl?.trim(),
                            streamUrl = url?.trim(),
                            canSnapshot = webcam.snapshotUrl != null,
                            snapshotDisplay = webcam.snapshotUrl,
                            provider = "multicam_legacy",
                            type = type,
                        )
                    }
                }.filterNotNull()
            )
        },
        ngrok = plugins.ngrok?.let {
            Settings.Ngrok(
                authName = it.authName,
                authPassword = it.authPassword,
            )
        },
        octoAppCompanion = plugins.octoAppCompanion?.let {
            Settings.OctoAppCompanion(
                version = it.version,
                encryptionKey = it.encryptionKey,
                running = true,
            )
        },
        obico = plugins.obico?.let {
            Settings.Obico
        },
        uploadAnything = plugins.uploadAnything?.let {
            Settings.UploadAnything(
                allowedExtensions = it.allowedExtensions
            )
        },
        spoolManager = plugins.spoolManager?.let {
            Settings.SpoolManager
        },
        spoolman = plugins.spoolman?.let {
            Settings.Spoolman(
                selectedSpoolIds = it.selectedSpoolIds?.mapValues { (_, selection) ->
                    Settings.Spoolman.SelectedSpool(
                        spoolId = selection.spoolId
                    )
                }
            )
        },
        filamentManager = plugins.filamentManager?.let {
            Settings.FilamentManager
        },
        octoCam = plugins.octoCam?.let {
            Settings.OctoCam
        },
        myStrom = plugins.myStrom?.let {
            Settings.MyStrom
        },
        octoHue = plugins.octoHue?.let {
            Settings.OctoHue
        },
        octoLight = plugins.octoLight?.let {
            Settings.OctoLight
        },
        octoLightHA = plugins.octoLightHA?.let {
            Settings.OctoLightHA
        },
        ophom = plugins.ophom?.let {
            Settings.Ophom
        },
        psuControl = plugins.psuControl?.let {
            Settings.PsuControl
        },
        wled = plugins.wled?.let {
            Settings.Wled
        },
        wS281x = plugins.wS281x?.let {
            Settings.WS281x
        },
        enclosure = plugins.enclosure?.let {
            Settings.Enclosure(
                outputs = it.outputs.map { o ->
                    Settings.Enclosure.Output(
                        indexId = o.indexId,
                        type = o.type,
                        label = o.label
                    )
                }
            )
        },
        gpioControl = plugins.gpioControl?.let {
            Settings.GpioControl(
                devices = it.devices.mapIndexed { index, device ->
                    Settings.GpioControl.Device(
                        index = index,
                        name = device.name
                    )
                }
            )
        },
        usbRelayControl = plugins.usbRelayControl?.let {
            Settings.UsbRelayControl(
                devices = it.devices.mapIndexed { index, device ->
                    Settings.UsbRelayControl.Device(
                        index = index,
                        name = device.name
                    )
                }
            )
        },
        tasmota = plugins.tasmota?.let {
            Settings.Tasmota(
                devices = it.devices.map { device ->
                    Settings.Tasmota.Device(
                        label = device.label,
                        idx = device.idx,
                        ip = device.ip
                    )
                }
            )
        },
        tpLinkSmartPlug = plugins.tpLinkSmartPlug?.let {
            Settings.TpLinkSmartPlug(
                devices = it.devices.map { device ->
                    Settings.TpLinkSmartPlug.Device(
                        label = device.label,
                        ip = device.ip
                    )
                }
            )
        },
        tradfri = plugins.tradfri?.let {
            Settings.Tradfri(
                devices = it.devices.mapNotNull { device ->
                    Settings.Tradfri.Device(
                        name = device.name ?: return@mapNotNull null,
                        id = device.id ?: return@mapNotNull null,
                    )
                }
            )
        },
        tuya = plugins.tuya?.let {
            Settings.Tuya(
                devices = it.devices.map { device ->
                    Settings.Tuya.Device(
                        label = device.label,
                    )
                }
            )
        },
        wemoSwitch = plugins.wemoSwitch?.let {
            Settings.WemoSwitch(
                devices = it.devices.map { device ->
                    Settings.WemoSwitch.Device(
                        label = device.label,
                        ip = device.ip,
                    )
                }
            )
        },
        octoRelay = plugins.octoRelay?.let {
            Settings.OctoRelay(
                devices = it.devices.mapNotNull { d ->
                    if (d.active) {
                        Settings.OctoRelay.Device(
                            id = d.id,
                            displayName = d.labelText
                        )
                    } else {
                        null
                    }
                }
            )
        },
        bedLevelVisualizer = plugins.bedLevelVisualizer?.let { _ ->
            Settings.BedLevelVisualizer
        },
        octoKlipper = plugins.octoKlipper?.let {
            Settings.OctoKlipper(
                macros = it.macros?.map { m ->
                    Macro(
                        id = listOf(m.macro, m.name).joinToString().hashCode().toString(),
                        name = m.name ?: m.macro,
                        inputs = emptyList(),
                    )
                }?.distinctBy { m ->
                    m.id
                } ?: emptyList()
            )
        }
    )
)