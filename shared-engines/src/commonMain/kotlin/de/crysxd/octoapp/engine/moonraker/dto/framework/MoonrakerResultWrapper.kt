package de.crysxd.octoapp.engine.moonraker.dto.framework

import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerResultWrapper<Result>(
    val result: Result
)