package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoChamberCommand {

    @Serializable
    data class SetTargetTemperature(val target: Float) : OctoChamberCommand() {
        val command = "target"
    }

    @Serializable
    data class SetTemperatureOffset(val offset: Float) : OctoChamberCommand() {
        val command = "offset"
    }
}

