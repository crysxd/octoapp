package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerPostDeviceParams(
    @SerialName("device") val device: String,
    @SerialName("action") val action: Action,
) : MoonrakerRpcParams {
    @Serializable
    enum class Action {
        @SerialName("on")
        ON,
        @SerialName("off")
        OFF,
    }
}