package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerSystemInfo(
    @SerialName("system_info") val systemInfo: SystemInfo? = null
) {
    @Serializable
    data class SystemInfo(
        @SerialName("available_services") val availableServices: List<String>? = null,
        @SerialName("canbus") val canbus: Canbus? = null,
        @SerialName("cpu_info") val cpuInfo: CpuInfo? = null,
        @SerialName("distribution") val distribution: Distribution? = null,
        @SerialName("instance_ids") val instanceIds: InstanceIds? = null,
        @SerialName("network") val network: Map<String, NetworkInterface>? = null,
        @SerialName("python") val python: Python? = null,
        @SerialName("sd_info") val sdInfo: SdInfo? = null,
        @SerialName("service_state") val serviceState: Map<String, ServiceState>? = null,
        @SerialName("virtualization") val virtualization: Virtualization? = null
    ) {
        @Serializable
        data class Canbus(
            @SerialName("can0") val can0: Can0? = null,
            @SerialName("can1") val can1: Can1? = null
        ) {
            @Serializable
            data class Can0(
                @SerialName("bitrate") val bitrate: Int? = null,
                @SerialName("driver") val driver: String? = null,
                @SerialName("tx_queue_len") val txQueueLen: Int? = null
            )

            @Serializable
            data class Can1(
                @SerialName("bitrate") val bitrate: Int? = null,
                @SerialName("driver") val driver: String? = null,
                @SerialName("tx_queue_len") val txQueueLen: Int? = null
            )
        }

        @Serializable
        data class CpuInfo(
            @SerialName("bits") val bits: String? = null,
            @SerialName("cpu_count") val cpuCount: Int? = null,
            @SerialName("cpu_desc") val cpuDesc: String? = null,
            @SerialName("hardware_desc") val hardwareDesc: String? = null,
            @SerialName("memory_units") val memoryUnits: String? = null,
            @SerialName("model") val model: String? = null,
            @SerialName("processor") val processor: String? = null,
            @SerialName("serial_number") val serialNumber: String? = null,
            @SerialName("total_memory") val totalMemory: Int? = null
        )

        @Serializable
        data class Distribution(
            @SerialName("codename") val codename: String? = null,
            @SerialName("id") val id: String? = null,
            @SerialName("like") val like: String? = null,
            @SerialName("name") val name: String? = null,
            @SerialName("version") val version: String? = null,
            @SerialName("version_parts") val versionParts: VersionParts? = null
        ) {
            @Serializable
            data class VersionParts(
                @SerialName("build_number") val buildNumber: String? = null,
                @SerialName("major") val major: String? = null,
                @SerialName("minor") val minor: String? = null
            )
        }

        @Serializable
        data class InstanceIds(
            @SerialName("klipper") val klipper: String? = null,
            @SerialName("moonraker") val moonraker: String? = null
        )


        @Serializable
        data class NetworkInterface(
            @SerialName("ip_addresses") val ipAddresses: List<IpAddresse?>? = null,
            @SerialName("mac_address") val macAddress: String? = null
        ) {
            @Serializable
            data class IpAddresse(
                @SerialName("address") val address: String? = null,
                @SerialName("family") val family: String? = null,
                @SerialName("is_link_local") val isLinkLocal: Boolean? = null
            )
        }

        @Serializable
        data class Python(
            @SerialName("version") val version: List<String>? = null,
            @SerialName("version_string") val versionString: String? = null
        )

        @Serializable
        data class SdInfo(
            @SerialName("capacity") val capacity: String? = null,
            @SerialName("manufacturer") val manufacturer: String? = null,
            @SerialName("manufacturer_date") val manufacturerDate: String? = null,
            @SerialName("manufacturer_id") val manufacturerId: String? = null,
            @SerialName("oem_id") val oemId: String? = null,
            @SerialName("product_name") val productName: String? = null,
            @SerialName("product_revision") val productRevision: String? = null,
            @SerialName("serial_number") val serialNumber: String? = null,
            @SerialName("total_bytes") val totalBytes: Long? = null
        )

        @Serializable
        data class ServiceState(
            @SerialName("active_state") val activeState: ActiveState? = null,
            @SerialName("sub_state") val subState: String? = null
        ) {
            @Serializable
            enum class ActiveState {
                @SerialName("active")
                Active,
                @SerialName("inactive")
                Inactive,
                @SerialName("deactivating")
                Deactivating,
            }
        }

        @Serializable
        data class Virtualization(
            @SerialName("virt_identifier") val virtIdentifier: String? = null,
            @SerialName("virt_type") val virtType: String? = null
        )
    }
}