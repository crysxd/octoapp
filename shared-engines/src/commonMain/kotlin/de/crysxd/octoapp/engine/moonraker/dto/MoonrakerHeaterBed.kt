package de.crysxd.octoapp.engine.moonraker.dto


import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericHeater
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerHeaterBed(
    @SerialName("power") override val power: Float? = null,
    @SerialName("target") override val target: Float? = null,
    @SerialName("temperature") override val temperature: Float? = null
) : MoonrakerGenericHeater, MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        power = power ?: (previous as? MoonrakerHeaterBed)?.power,
        target = target ?: (previous as? MoonrakerHeaterBed)?.target,
        temperature = temperature ?: (previous as? MoonrakerHeaterBed)?.temperature,
    )
}