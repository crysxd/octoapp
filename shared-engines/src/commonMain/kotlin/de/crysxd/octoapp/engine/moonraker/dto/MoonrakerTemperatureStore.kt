package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerTemperatureStoreSerializer
import kotlinx.serialization.Serializable

@Serializable(with = MoonrakerTemperatureStoreSerializer::class)
data class MoonrakerTemperatureStore(
    val components: Map<String, Component>
) {
    @Serializable
    data class Component(
        val temperatures: List<Float?>? = null,
        val targets: List<Float?>? = null,
        val powers: List<Float?>? = null,
        val speeds: List<Float?>? = null,
    )
}