package de.crysxd.octoapp.engine.octoprint

import io.github.aakira.napier.Napier

interface CommandBody {
    val command: String

    fun log() {
        Napier.d(tag = OctoConstants.OCTOPRINT_TAG, message = "command=$command")
    }
}