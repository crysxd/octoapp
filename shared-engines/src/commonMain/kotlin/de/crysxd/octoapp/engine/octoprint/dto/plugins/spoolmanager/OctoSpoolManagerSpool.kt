package de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolmanager

import de.crysxd.octoapp.engine.framework.json.SafeFloatSerializer
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoSpoolManagerSpool(
    val color: HexColor? = null,
    val colorName: String? = null,
    val databaseId: String? = null,
    val density: Float? = null,
    val vendor: String? = null,
    val material: String? = null,
    val displayName: String? = null,
    val isActive: Boolean? = null,
    @Serializable(with = SafeFloatSerializer::class) val remainingWeight: Float? = null,
    val isTemplate: Boolean? = null,
)