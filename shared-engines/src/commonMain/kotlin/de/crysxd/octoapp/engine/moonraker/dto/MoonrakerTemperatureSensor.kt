package de.crysxd.octoapp.engine.moonraker.dto


import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericTemperatureSensor
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerTemperatureSensor(
    @SerialName("measured_max_temp") val measuredMaxTemp: Float? = null,
    @SerialName("measured_min_temp") val measuredMinTemp: Float? = null,
    @SerialName("temperature") override val temperature: Float? = null
) : MoonrakerStatusItem, MoonrakerGenericTemperatureSensor {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        measuredMaxTemp = measuredMaxTemp ?: (previous as? MoonrakerTemperatureSensor)?.measuredMaxTemp,
        measuredMinTemp = measuredMinTemp ?: (previous as? MoonrakerTemperatureSensor)?.measuredMinTemp,
        temperature = temperature ?: (previous as? MoonrakerTemperatureSensor)?.temperature,
    )
}