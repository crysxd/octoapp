package de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys

import kotlinx.serialization.Serializable

@Serializable
internal data class ApplicationKeyRequest(
    val app: String,
)