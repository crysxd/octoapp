package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.psucontrol.PsuControlCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.psucontrol.PsuControlState
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.sharedcommon.http.framework.DisabledConnectionEstiablishedTimeout
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class PsuControlApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = if (settings.plugins.psuControl != null) {
        listOf(PowerDevice(this))
    } else {
        emptyList()
    }

    private suspend fun executeCommand(command: String) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.PsuControl)
            setJsonBody(PsuControlCommand(command))

            // PSU Control is known for taking some time to respond, disable SoftTimeout
            attributes.put(DisabledConnectionEstiablishedTimeout, true)
        }
    }

    private suspend fun turnOn() {
        executeCommand("turnPSUOn")
    }

    private suspend fun turnOff() {
        executeCommand("turnPSUOff")
    }

    private suspend fun isOn() = executeCommand("getPSUState").body<PsuControlState>().isPSUOn in listOf("1", "true", "on")

    internal data class PowerDevice(
        private val owner: PsuControlApi,
    ) : IPowerDevice {
        override val id: String = "psu"
        override val pluginId: String = OctoPlugins.PsuControl
        override val displayName: String = "PSU"
        override val pluginDisplayName = "PSU Control"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.turnOn()
        override suspend fun turnOff() = owner.turnOff()
        override suspend fun isOn() = owner.isOn()
    }
}