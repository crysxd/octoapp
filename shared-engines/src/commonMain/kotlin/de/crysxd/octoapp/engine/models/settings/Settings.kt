package de.crysxd.octoapp.engine.models.settings

import de.crysxd.octoapp.engine.models.macro.Macro
import de.crysxd.octoapp.engine.octoprint.OctoColorSchemes
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonNames

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class Settings(
    val webcam: WebcamSettings = WebcamSettings(),
    val plugins: PluginSettingsGroup = PluginSettingsGroup(),
    val bedMesh: BedMesh? = null,
    @JsonNames("temperatureProfiles", "temperaturePresets") val temperaturePresets: List<TemperaturePreset> = emptyList(),
    val coolDownProfile: TemperaturePreset? = null,
    val terminalFilters: List<TerminalFilter> = emptyList(),
    val appearance: Appearance = Appearance(),
    val printerObjects: List<String> = emptyList(),
    val hash: String? = null,
    val babyStepIntervals: List<Float> = listOf(
        0.005f,
        0.01f,
        0.025f,
        0.05f,
    )
) {

    @Serializable
    data class BedMesh(
        val refreshCommand: String? = null,
        val colorScale: List<Pair<Float, HexColor>> = DefaultColorScale,
        val mesh: List<List<Float?>>? = null,
        val meshX: List<Float>? = null,
        val meshY: List<Float>? = null,
        val graphZMin: Float = 0f,
        val graphZMax: Float = 1f,
        val profiles: List<Profile> = emptyList(),
        val activeProfile: String? = null,
    ) : OctoSettings.PluginSettings {
        @Serializable
        data class Profile(
            val id: String,
            val label: String,
            val enableCommand: String
        )

        companion object {
            val DefaultColorScale = listOf(
                -1.0f to HexColor("#663399"),
                -0.100000024f to HexColor("#0000FF"),
                0.0f to HexColor("#008000"),
                0.100000024f to HexColor("#FFFF00"),
                1.0f to HexColor("#FF0000"),
            )
        }
    }

    @Serializable
    data class Appearance(
        val name: String? = null,
        val colors: Colors = Colors(),
    ) {

        @Serializable
        data class Colors(
            val themeName: String? = null,
            val light: ColorScheme = OctoColorSchemes.Light.default,
            val dark: ColorScheme = OctoColorSchemes.Dark.default,
        )

        @Serializable
        data class ColorScheme(
            val main: HexColor,
            val accent: HexColor,
        )
    }

    @Serializable
    data class TerminalFilter(
        val name: String,
        val regex: String
    )

    @Serializable
    data class TemperaturePreset(
        val name: String,
        val gcode: String? = null,
        val components: List<Component> = emptyList(),
    ) {
        @Serializable
        data class Component(
            val key: String,
            val displayName: String = key,
            val temperature: Float,
            val isBed: Boolean = false,
            val isChamber: Boolean = false,
            val isExtruder: Boolean = false,
        )
    }

    @Serializable
    data class PluginSettingsGroup(
        val gcodeViewer: GcodeViewer? = null,
        val octoEverywhere: OctoEverywhere? = null,
        val ngrok: Ngrok? = null,
        val obico: Obico? = null,
        val spoolManager: SpoolManager? = null,
        val spoolman: Spoolman? = null,
        val filamentManager: FilamentManager? = null,
        val octoAppCompanion: OctoAppCompanion? = null,
        val multiCamSettings: MultiCam? = null,
        val discovery: Discovery? = null,
        val uploadAnything: UploadAnything? = null,
        val mmu: Mmu? = null,
        val cancelObject: CancelObject? = null,
        val psuControl: PsuControl? = null,
        val wled: Wled? = null,
        val octoCam: OctoCam? = null,
        val octoLight: OctoLight? = null,
        val octoLightHA: OctoLightHA? = null,
        val ophom: Ophom? = null,
        val octoHue: OctoHue? = null,
        val myStrom: MyStrom? = null,
        val wS281x: WS281x? = null,
        val enclosure: Enclosure? = null,
        val gpioControl: GpioControl? = null,
        val octoRelay: OctoRelay? = null,
        val tasmota: Tasmota? = null,
        val tpLinkSmartPlug: TpLinkSmartPlug? = null,
        val tradfri: Tradfri? = null,
        val tuya: Tuya? = null,
        val usbRelayControl: UsbRelayControl? = null,
        val wemoSwitch: WemoSwitch? = null,
        val bedLevelVisualizer: BedLevelVisualizer? = null,
        val moonrakerPowerDevices: MoonrakerPowerDevices? = null,
        val octoKlipper: OctoKlipper? = null
    )

    interface PluginSettings

    @Serializable
    data class MoonrakerPowerDevices(
        val devices: List<Device>
    ) {
        @Serializable
        data class Device(
            val device: String,
            val type: String
        )
    }

    @Serializable
    data class GcodeViewer(
        val mobileSizeThreshold: Long = 0,
        val sizeThreshold: Long = 0
    ) : PluginSettings

    @Serializable
    data class Tradfri(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val id: String,
            val name: String,
        )
    }

    @Serializable
    data class Tuya(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val label: String,
        )
    }

    @Serializable
    data class TpLinkSmartPlug(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class WemoSwitch(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class Tasmota(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val idx: String,
            val label: String,
        )
    }

    @Serializable
    data class GpioControl(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val name: String,
            val index: Int,
        )
    }

    @Serializable
    data class Enclosure(
        val outputs: List<Output>?
    ) : PluginSettings {
        @Serializable
        data class Output(
            val label: String,
            val type: String,
            val indexId: Int,
        )
    }

    @Serializable
    data class UsbRelayControl(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val name: String,
            val index: Int,
        )
    }

    @Serializable
    data class OctoRelay(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val id: String,
            val displayName: String,
        )
    }

    @Serializable
    data object WS281x : PluginSettings

    @Serializable
    data object Wled : PluginSettings

    @Serializable
    data object OctoCam : PluginSettings

    @Serializable
    data object OctoLight : PluginSettings

    @Serializable
    data object OctoLightHA : PluginSettings

    @Serializable
    data object Ophom : PluginSettings

    @Serializable
    data object OctoHue : PluginSettings

    @Serializable
    data object MyStrom : PluginSettings

    @Serializable
    data object PsuControl : PluginSettings

    @Serializable
    data object CancelObject : PluginSettings

    @Serializable
    data class Ngrok(
        val authName: String? = null,
        val authPassword: String? = null,
    ) : PluginSettings

    @Serializable
    data object Obico : PluginSettings

    @Serializable
    data class OctoAppCompanion(
        val encryptionKey: String? = null,
        val running: Boolean? = null,
        val version: String?,
        val serviceName: String? = null,
    ) : PluginSettings

    @Serializable
    data class MultiCam(
        val webcams: List<WebcamSettings.Webcam> = emptyList()
    ) : PluginSettings

    @Serializable
    data class Discovery(
        val uuid: String?
    ) : PluginSettings

    @Serializable
    data class UploadAnything(
        val allowedExtensions: List<String> = emptyList()
    ) : PluginSettings

    @Serializable
    data object SpoolManager : PluginSettings

    @Serializable
    data class Spoolman(
        val selectedSpoolIds: Map<String, SelectedSpool>? = null
    ) : PluginSettings {
        @Serializable
        data class SelectedSpool(
            val spoolId: String? = null
        )
    }

    @Serializable
    data object FilamentManager : PluginSettings

    @Serializable
    data object OctoEverywhere : PluginSettings

    @Serializable
    data class Mmu(
        val filament: List<Filament> = emptyList(),
        val labelSource: LabelSource = LabelSource.Manual,
        val plugin: Plugin,
    ) : PluginSettings {
        @Serializable
        data class Filament(
            val name: String?,
            val toolIndex: Int,
            val color: HexColor?,
        )

        enum class Plugin {
            Mmu2FilamentSelect,
            PrusaMmu,
        }

        enum class LabelSource {
            Manual,
            FilamentManager,
            SpoolManager,
        }
    }

    @Serializable
    data object BedLevelVisualizer : OctoSettings.PluginSettings

    @Serializable
    data class OctoKlipper(
        val macros: List<Macro>
    ) : PluginSettings
}