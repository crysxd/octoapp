package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.models.macro.Macro
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectsQueryParams
import de.crysxd.octoapp.engine.moonraker.mappers.map

class MoonrakerMacroApi internal constructor(
    private val requestHandler: MoonrakerRequestHandler
) {

    suspend fun getMacros(): List<Macro> {
        val res = requestHandler.sendRequest<MoonrakerPrinterObjectsQuery>(
            method = "printer.objects.query",
            params = MoonrakerPrinterObjectsQueryParams(
                objects = mapOf(
                    Constants.PrinterObjects.ConfigFile to null,
                )
            )
        )

        return res.status?.get(Constants.PrinterObjects.ConfigFile)?.let {
            it as? MoonrakerConfigFile
        }?.map() ?: emptyList()
    }
}