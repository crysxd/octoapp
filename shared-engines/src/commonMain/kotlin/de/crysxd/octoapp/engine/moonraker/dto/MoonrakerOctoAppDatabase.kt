package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.time.Duration.Companion.minutes

@Serializable
data class MoonrakerOctoAppDatabase(
    @SerialName("public") val public: Public? = null,
) {

    @Serializable
    data class Public(
        @SerialName("printerId") val printerId: String? = null,
        @SerialName("pluginVersion") val pluginVersion: String? = null,
        @SerialName("lastSeen") val lastSeenAt: MoonrakerInstant? = null,
        @SerialName("encryptionKey") val encryptionKey: String? = null
    ) {
        val isRunning
            get() : Boolean {
                val lastSeen = lastSeenAt ?: Instant.DISTANT_FUTURE
                val diff = Clock.System.now() - lastSeen
                return diff < 10.minutes
            }
    }

    @Serializable
    data class App(
        @SerialName("appBuild") val appBuild: Long,
        @SerialName("appLanguage") val appLanguage: String,
        @SerialName("appVersion") val appVersion: String,
        @SerialName("displayDescription") val displayDescription: String?,
        @SerialName("displayName") val displayName: String,
        @SerialName("expireAt") val expireAt: Double,
        @SerialName("fcmToken") val fcmToken: String,
        @SerialName("fcmTokenFallback") val fcmTokenFallback: String? = null,
        @SerialName("instanceId") val instanceId: String,
        @SerialName("lastSeenAt") val lastSeenAt: Double,
        @SerialName("model") val model: String,
        @SerialName("excludeNotifications") val excludeNotifications: List<String>?,
    )
}