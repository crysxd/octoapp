package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerOutputPin(
    @SerialName("value") val value: Float? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        value = value ?: (previous as? MoonrakerOutputPin)?.value
    )
}