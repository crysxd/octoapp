package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import de.crysxd.octoapp.sharedcommon.http.OctoRemoteIpAttributeKey
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.url.getConnectionType
import io.github.aakira.napier.Napier
import io.ktor.client.call.HttpClientCall

internal fun HttpClientCall.guessConnectionType(): ConnectionType {
    // OkHTTP interceptor adds this header on Android
    // On iOS we rely on the request URL which will only report "local connection" for IP addresses but not for hostnames resolved to a local IP
    val url = attributes.getOrNull(OctoRemoteIpAttributeKey)?.let { "http://$it".toUrl() }
        ?: request.url

    Napier.v(tag = "GuessConnectionType", message = "Guessed URL: ${attributes.getOrNull(OctoRemoteIpAttributeKey)} -> $url")

    // If the default request URL is "cloud" use the resolved URL
    // This way we don't loose the specific types for OE, Obico, ngrok, ...
    return when (val type = request.url.getConnectionType()) {
        ConnectionType.DefaultCloud -> url.getConnectionType()
        else -> type
    }
}