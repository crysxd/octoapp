package de.crysxd.octoapp.engine.octoprint.dto.plugins.tplinksmartplug

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class TpLinkSmartPlugCommand(
    override val command: String,
    val ip: String
) : CommandBody
