package de.crysxd.octoapp.engine.moonraker.dto


import de.crysxd.octoapp.engine.framework.json.SafeBooleanSerializer
import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerLegacyWebcamDatabaseSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable(with = MoonrakerLegacyWebcamDatabaseSerializer::class)
internal data class MoonrakerLegacyWebcamDatabase(
    private val webcams: Map<String, Webcam?> = emptyMap()
) : Map<String, MoonrakerLegacyWebcamDatabase.Webcam?> by webcams {
    @Serializable
    data class Webcam(
        @Serializable(with = SafeBooleanSerializer::class) @SerialName("enabled") val enabled: Boolean? = null,
        @Serializable(with = SafeBooleanSerializer::class) @SerialName("flipX") val flipX: Boolean? = null,
        @Serializable(with = SafeBooleanSerializer::class) @SerialName("flipY") val flipY: Boolean? = null,
        @SerialName("name") val name: String? = null,
        @SerialName("rotation") val rotation: Float? = null,
        @SerialName("service") val service: String? = null,
        @SerialName("targetFps") val targetFps: Float? = null,
        @SerialName("targetFpsIdle") val targetFpsIdle: Float? = null,
        @SerialName("urlStream") val urlStream: String? = null
    )
}