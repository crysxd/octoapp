package de.crysxd.octoapp.engine.octoprint.event

import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportConfiguration
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportSubscription
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportThrottle

internal var eventSourceInstanceCounter = 0

internal fun List<EventSource.Config>.buildEventTransportConfig() = if (isEmpty()) {
    null
} else {
    OctoEventTransportConfiguration(
        throttle = OctoEventTransportThrottle(minOf { it.throttle }),
        subscription = OctoEventTransportSubscription(
            plugins = if (any { it.requestPlugins }) {
                listOf("octoapp", "octolapse", "ngrok", "cancelobject", "mmu2filamentselect", "prusammu")
            } else {
                listOf("octoapp")
            },
            state = map { it.requestTerminalLogs }.flatten().distinct().let {
                when {
                    it.isEmpty() -> OctoEventTransportSubscription.State.NoLogs()
                    it.contains(EventSource.Config.ALL_LOGS) -> OctoEventTransportSubscription.State.AllLogs()
                    else -> OctoEventTransportSubscription.State.SomeLogs(logs = "(${it.joinToString("|")})")
                }
            }
        )
    )
}
