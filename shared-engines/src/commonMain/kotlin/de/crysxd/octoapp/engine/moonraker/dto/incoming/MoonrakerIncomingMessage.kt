package de.crysxd.octoapp.engine.moonraker.dto.incoming

internal sealed interface MoonrakerIncomingMessage {
    data object Unknown : MoonrakerIncomingMessage
}