package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerGcodeMessage(
    @SerialName("message") val message: String? = null,
    @SerialName("time") val time: MoonrakerInstant? = null,
    @SerialName("type") val type: String? = null
)