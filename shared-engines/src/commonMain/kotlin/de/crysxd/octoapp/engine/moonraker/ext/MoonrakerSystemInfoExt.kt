package de.crysxd.octoapp.engine.moonraker.ext

import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo

internal val Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?.description
    get() = this?.let {
        "ServiceStates(${entries.joinToString { (k, v) -> "$k:${v.activeState}" }})"
    }

internal val Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?.octoapp get() = closeMatch("octoapp")
internal val Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?.obico get() = closeMatch("obico")
internal val Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?.octoEverywhere get() = closeMatch("octoeverywhere")

internal operator fun Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?.plus(other: Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>): Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState> {
    val mutable = this?.toMutableMap() ?: mutableMapOf()
    other.entries.forEach { (k, v) ->
        mutable[k] = v
    }
    return mutable.toMap()
}

private fun Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?.closeMatch(match: String) =
    this?.keys?.firstOrNull { it.contains(match, ignoreCase = true) }?.let { key ->
        ServiceData(
            name = key,
            state = this[key]
        )
    }