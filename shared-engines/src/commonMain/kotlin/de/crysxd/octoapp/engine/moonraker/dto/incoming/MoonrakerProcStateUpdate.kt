package de.crysxd.octoapp.engine.moonraker.dto.incoming

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerProcStateUpdate(
    @SerialName("jsonrpc") val jsonrpc: String? = null,
    @SerialName("method") val method: String? = null,
    @SerialName("params") val params: List<Param?>? = null
) : MoonrakerIncomingMessage {
    @Serializable
    data class Param(
        @SerialName("cpu_temp") val cpuTemp: Float? = null,
        @SerialName("moonraker_stats") val moonrakerStats: MoonrakerStats? = null,
        @SerialName("network") val network: Map<String, NetworkInterface>? = null,
        @SerialName("system_cpu_usage") val systemCpuUsage: Map<String, Float>? = null,
        @SerialName("system_memory") val systemMemory: SystemMemory? = null,
        @SerialName("websocket_connections") val websocketConnections: Int? = null
    ) {
        @Serializable
        data class MoonrakerStats(
            @SerialName("cpu_usage") val cpuUsage: Float? = null,
            @SerialName("mem_units") val memUnits: String? = null,
            @SerialName("memory") val memory: Float? = null,
            @SerialName("time") val time: Float? = null
        )

        @Serializable
        data class NetworkInterface(
            @SerialName("bandwidth") val bandwidth: Float? = null,
            @SerialName("rx_bytes") val rxBytes: Long? = null,
            @SerialName("rx_drop") val rxDrop: Long? = null,
            @SerialName("rx_errs") val rxErrs: Long? = null,
            @SerialName("rx_packets") val rxPackets: Long? = null,
            @SerialName("tx_bytes") val txBytes: Long? = null,
            @SerialName("tx_drop") val txDrop: Long? = null,
            @SerialName("tx_errs") val txErrs: Long? = null,
            @SerialName("tx_packets") val txPackets: Long? = null
        )

        @Serializable
        data class SystemMemory(
            @SerialName("available") val available: Long? = null,
            @SerialName("total") val total: Long? = null,
            @SerialName("used") val used: Long? = null
        )
    }
}