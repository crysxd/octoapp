package de.crysxd.octoapp.engine.moonraker.dto.framework

interface MoonrakerGenericTemperatureSensor {
    val temperature: Float?
}