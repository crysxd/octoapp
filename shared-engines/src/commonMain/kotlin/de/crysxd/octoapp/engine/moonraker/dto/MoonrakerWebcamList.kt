package de.crysxd.octoapp.engine.moonraker.dto


import de.crysxd.octoapp.engine.framework.json.SafeBooleanSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable
internal data class MoonrakerWebcamList(
    @SerialName("webcams") val webcams: List<Webcam?>? = null
) {
    @Serializable
    data class Webcam(
        @SerialName("aspect_ratio") val aspectRatio: String? = null,
        @Serializable(with = SafeBooleanSerializer::class) @SerialName("enabled") val enabled: Boolean? = null,
        @SerialName("extra_data") val extraData: JsonObject? = null,
        @Serializable(with = SafeBooleanSerializer::class) @SerialName("flip_horizontal") val flipHorizontal: Boolean? = null,
        @Serializable(with = SafeBooleanSerializer::class) @SerialName("flip_vertical") val flipVertical: Boolean? = null,
        @SerialName("icon") val icon: String? = null,
        @SerialName("location") val location: String? = null,
        @SerialName("name") val name: String? = null,
        @SerialName("rotation") val rotation: Float? = null,
        @SerialName("service") val service: Service? = null,
        @SerialName("snapshot_url") val snapshotUrl: String? = null,
        @SerialName("source") val source: String? = null,
        @SerialName("stream_url") val streamUrl: String? = null,
        @SerialName("target_fps") val targetFps: Float? = null,
        @SerialName("target_fps_idle") val targetFpsIdle: Float? = null
    ) {
        @Serializable
        enum class Service {
            @SerialName("webrtc-camerastreamer")
            WebRtcCameraStreamer,

            @SerialName("mjpegstreamer")
            MjpegStreamer,


            @SerialName("mjpegstreamer-adaptive")
            MjpegStreamerAdaptive,

            @SerialName("webrtc-janus")
            WebRtcJanus,

            @SerialName("jmuxer-stream")
            JmuxerRaw,

            @SerialName("hlsstream")
            HlsStream,

            @SerialName("webrtc-mediamtx")
            WebRtcMediaMtx,

            @SerialName("webrtc-go2rtc")
            WebRtcGo2Rtc,

            @SerialName("ipstream")
            IpCamera,

            @SerialName("uv4l-mjpeg")
            Uv4lMjpeg,
        }
    }
}