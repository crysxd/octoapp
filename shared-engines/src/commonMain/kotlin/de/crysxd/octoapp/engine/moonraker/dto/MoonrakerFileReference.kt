package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerFileReference(
    @SerialName("modified") val modified: MoonrakerInstant? = null,
    @SerialName("path") val path: String? = null,
    @SerialName("dirname") val dirname: String? = null,
    @SerialName("filename") val filname: String? = null,
    @SerialName("permissions") val permissions: String? = null,
    @SerialName("size") val size: Long? = null
) {
    val isDirectory = dirname != null && filname == null
}

internal fun List<MoonrakerFileReference>.wrap() = MoonrakerFileReferenceList(this)
internal class MoonrakerFileReferenceList(items: List<MoonrakerFileReference>) : List<MoonrakerFileReference> by items