package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerServerDatabaseGetItemParams(
    @SerialName("namespace") val namespace: String,
    @SerialName("key") val key: String? = null,
) : MoonrakerRpcParams