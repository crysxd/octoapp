package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerCopyFileParams(
    @SerialName("source") val source: String,
    @SerialName("dest") val dest: String,
) : MoonrakerRpcParams