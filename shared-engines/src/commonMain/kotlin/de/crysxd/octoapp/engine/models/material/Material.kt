package de.crysxd.octoapp.engine.models.material

import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.utils.HexColor

interface Material : CommonParcelable {
    val id: UniqueId
    val displayName: String
    val vendor: String
    val material: String
    val color: HexColor?
    val colorName: String?
    val density: Float?
    val providerDisplayName: String
    val activeToolIndex: Int?
    val activeExtruderComponent: String?
    val weightGrams: Float?
    val attributes: List<String>

    val isActivated
        get() = activeExtruderComponent != null

    fun copyWithAttributes(vararg attributes: String): Material
}