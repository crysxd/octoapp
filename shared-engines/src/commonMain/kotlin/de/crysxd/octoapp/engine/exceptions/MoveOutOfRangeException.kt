package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException

class MoveOutOfRangeException : SuppressedIllegalStateException("Received negative repsonse to move command"), UserMessageException {
    override val userMessage: CharSequence = "Move reported out of range, homing might be required."
}