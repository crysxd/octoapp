package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.tune.TuneState
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExtruder
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMove
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOutputPin
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerToolhead
import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericFan
import de.crysxd.octoapp.engine.moonraker.ext.convertToMoonrakerComponentLabel

internal data class MoonrakerTuneStateMapperInput(
    val toolhead: MoonrakerToolhead?,
    val extruder: MoonrakerExtruder?,
    val gcodeMove: MoonrakerGcodeMove?,
    val fans: Map<String, MoonrakerGenericFan>,
    val fanSurrogates: Map<String, MoonrakerOutputPin>,
)

internal fun MoonrakerTuneStateMapperInput.map() = TuneState(
    isValuesImprecise = false,
    machineLimits = TuneState.MachineLimits(
        maxVelocity = toolhead?.maxVelocity,
        maxSquareCornerVelocity = toolhead?.squareCornerVelocity,
        maxAcceleration = toolhead?.maxAccel,
        maxAccelerationToDeceleration = toolhead?.maxAccelToDecel
    ),
    pressureAdvance = TuneState.PressureAdvance(
        pressureAdvance = extruder?.pressureAdvance,
        smoothTime = extruder?.smoothTime,
    ),
    fans = fans.map { (key, value) ->
        TuneState.Fan(
            component = key,
            label = key.split(" ").last().convertToMoonrakerComponentLabel(),
            speed = value.speed?.times(100f),
            isPartCoolingFan = key == Constants.PrinterObjects.Fan,
        )
    } + fanSurrogates.map { (key, value) ->
        // Creality K1 default config has the part cooling fan configured as output pin "output_pin fan0", treat as part cooling fan if not explicitly defined
        TuneState.Fan(
            component = key,
            label = when (key) {
                "output_pin fan0" -> "Model fan"
                "output_pin fan1" -> "Back fan"
                "output_pin fan2" -> "Side fan"
                else -> key.split(" ").last().convertToMoonrakerComponentLabel()
            },
            speed = value.value?.times(100f),
            isPartCoolingFan = key == "output_pin fan0" && fans.none { it.key == Constants.PrinterObjects.Fan },
        )
    },
    print = TuneState.Print(
        flowRate = gcodeMove?.extrudeFactor?.times(100f),
        feedRate = gcodeMove?.speedFactor?.times(100f)
    ),
    offsets = TuneState.Offsets(
        z = gcodeMove?.homingOrigin?.get(2)
    )
)