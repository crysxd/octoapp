@file:UseSerializers(InstantSerializer::class)

package de.crysxd.octoapp.engine.octoprint.dto.job

import de.crysxd.octoapp.engine.framework.json.InstantSerializer
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileOrigin
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
data class OctoJobInformation(
    val file: File? = null
) {
    @Serializable
    data class File(
        val name: String? = null,
        val path: String? = null,
        val origin: OctoFileOrigin?,
        val display: String? = null,
        val size: Long? = null,
        val date: Instant? = null,
    )
}