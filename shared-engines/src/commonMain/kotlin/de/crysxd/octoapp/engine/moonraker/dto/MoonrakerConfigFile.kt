package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement

@Serializable
internal data class MoonrakerConfigFile(
    @SerialName("config") val config: Map<String, JsonObject>? = null,
    @SerialName("save_config_pending") val saveConfigPending: Boolean? = null,
) : MoonrakerStatusItem {

    companion object {
        private val configJson = SharedCommonInjector.get().json
    }

    internal inline fun <reified T> getConfigItem(key: String) = config?.get(key)?.let {
        configJson.decodeFromJsonElement<T>(it)
    }

    override fun backFillWithPrevious(
        previous: MoonrakerStatusItem,
    ): MoonrakerStatusItem = copy(
        config = ((previous as? MoonrakerConfigFile)?.config ?: emptyMap()) + (config ?: emptyMap()),
    )

    override fun toString() = "MoonrakerConfigFile(config=<omitted>, saveConfigPending=$saveConfigPending)"

    @Serializable
    data class Extruder(
        @SerialName("filament_diameter") val filamentDiameter: Float? = null,
        @SerialName("nozzle_diameter") val nozzleDiameter: Float? = null,
    )
}