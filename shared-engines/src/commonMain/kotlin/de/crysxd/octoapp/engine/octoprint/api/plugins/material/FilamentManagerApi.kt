package de.crysxd.octoapp.engine.octoprint.api.plugins.material

import de.crysxd.octoapp.engine.api.AggregatorMaterialsApi
import de.crysxd.octoapp.engine.api.MaterialsApi.Companion.NoMaterialId
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager.OctoFilamentManagerList
import de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager.OctoFilamentManagerSelection
import de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager.OctoFilamentManagerSelectionRequest
import de.crysxd.octoapp.engine.octoprint.http.setJsonBodyWithExplicitNulls
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.patch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.currentCoroutineContext
import de.crysxd.octoapp.engine.models.material.Material as IMaterial

class FilamentManagerApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) : AggregatorMaterialsApi.MaterialApiAggregate {

    override val providerId = OctoPlugins.FilamentManager
    private val colorLut = mapOf(
        "White" to "#ffffff",
        "Yellow" to "#ffd500",
        "Green" to "#07e328",
        "Blue" to "#005eff",
        "Gray" to "#8c8c8c",
        "Grey" to "#8c8c8c",
        "Magenta" to "#fc19d7",
        "Purple" to "#ab008f",
        "Teal" to "#04c9c6",
        "Pink" to "#ff00c3",
        "Black" to "#000000",
        "Natural" to "#e5e6d5",
        "Wood" to "#deb887",
        "Metal" to "#a8a8a8",
        "Gold" to "#ffd700",
        "Copper" to "#b87333",
        "Bronze" to "#cd7f32",
        "Cyan" to "#00ffff",
        "Brown" to "#8b4513",
        "Beige" to "#f5f5dc",
        "Navy" to "#000080",
        "Lime" to "#00ff00",
        "Silver" to "#c0c0c0",
        "Marble" to "#eaeaea",
        "Orange" to "#ff8c00",
        "Red" to "#ff0000",
        "Transparent" to "#bfbfbf",
        "Fusili" to "#00ff00",
    )

    override suspend fun isMaterialManagerAvailable(settings: Settings) = settings.plugins.filamentManager != null

    override suspend fun getMaterials(settings: Settings) = with(CoroutineScope(currentCoroutineContext() + Dispatchers.SharedIO)) {
        val selectionDeferred = async {
            baseUrlRotator.request {
                httpClient.get {
                    urlFromPath(baseUrl = it, "plugin", OctoPlugins.FilamentManager, "selections")
                }.body<OctoFilamentManagerSelection>()
            }
        }

        val material = baseUrlRotator.request {
            httpClient.get {
                urlFromPath(baseUrl = it, "plugin", OctoPlugins.FilamentManager, "spools")
            }.body<OctoFilamentManagerList>()
        }

        val selection = selectionDeferred.await()

        material.spools.map { spool ->
            val (colorName, color) = spool.name?.findColor() ?: (null to null)
            Material(
                id = UniqueId(providerId = providerId, id = spool.id),
                displayName = spool.name ?: spool.id,
                color = color,
                colorName = colorName,
                vendor = spool.profile.vendor ?: "Unknown",
                material = spool.profile.material ?: "Unknown",
                providerDisplayName = "FilamentManager",
                activeToolIndex = selection.selections.firstOrNull { it.spool.id == spool.id }?.tool?.takeIf { it >= 0 },
                weightGrams = spool.weight?.let { w -> spool.used?.let { u -> w - u } },
                density = spool.density,
            )
        }
    }

    override suspend fun activateMaterial(uniqueMaterialId: UniqueId, extruderComponent: String) = baseUrlRotator.request<Unit> {
        httpClient.patch {
            val id = if (uniqueMaterialId.id == NoMaterialId.id) null else uniqueMaterialId.id
            val toolIndex = extruderComponent.removePrefix("tool").toIntOrNull()?.toString()
                ?: throw IllegalArgumentException("Expected extruderComponent in format 'tool[0-9]'")

            urlFromPath(baseUrl = it, "plugin", "filamentmanager", "selections", toolIndex)
            setJsonBodyWithExplicitNulls(
                body = OctoFilamentManagerSelectionRequest(
                    selection = OctoFilamentManagerSelectionRequest.Selection(
                        spool = OctoFilamentManagerSelectionRequest.SpoolSelection(
                            id = id
                        )
                    )
                )
            )
        }
    }

    private fun String.findColor() = colorLut.entries.firstOrNull { contains(it.key, ignoreCase = true) }
        ?.let { it.key to HexColor(it.value) }

    @CommonParcelize
    data class Material(
        override val id: UniqueId,
        override val displayName: String,
        override val vendor: String,
        override val material: String,
        override val density: Float?,
        override val color: HexColor?,
        override val colorName: String?,
        override val providerDisplayName: String,
        override val activeToolIndex: Int?,
        override val weightGrams: Float?,
        override val attributes: List<String> = listOf()
    ) : IMaterial {
        override val activeExtruderComponent get() = activeToolIndex?.let { "tool$it" }
        override fun copyWithAttributes(vararg attributes: String) = copy(attributes = attributes.toList())
    }
}
