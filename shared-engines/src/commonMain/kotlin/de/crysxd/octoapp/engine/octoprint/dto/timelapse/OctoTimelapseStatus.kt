package de.crysxd.octoapp.engine.octoprint.dto.timelapse

import kotlinx.serialization.Serializable

@Serializable
data class OctoTimelapseStatus(
    val enabled: Boolean = false,
    val config: OctoTimelapseConfig = OctoTimelapseConfig(),
    val files: List<OctoTimelapseFile> = emptyList(),
    val unrendered: List<OctoTimelapseFile> = emptyList(),
)