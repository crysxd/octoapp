package de.crysxd.octoapp.engine.octoprint.dto.plugins.tasmota

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class TasmotaCommand(
    override val command: String,
    val ip: String,
    val idx: String
) : CommandBody

