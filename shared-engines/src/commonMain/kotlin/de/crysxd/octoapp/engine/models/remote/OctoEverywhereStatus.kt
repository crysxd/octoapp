package de.crysxd.octoapp.engine.models.remote

data class OctoEverywhereStatus(
    val pluginVersion: String? = null,
    val printerId: String? = null,
)