package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.connection.Connection
import de.crysxd.octoapp.engine.models.connection.ConnectionState
import de.crysxd.octoapp.engine.models.printer.PrinterProfileReference
import de.crysxd.octoapp.engine.octoprint.dto.connection.OctoConnectionState
import de.crysxd.octoapp.engine.octoprint.dto.connection.OctoPrintConnection

internal fun OctoConnectionState.map() = ConnectionState(
    current = Connection(
        state = when (current.state) {
            OctoPrintConnection.State.MAYBE_DETECTING_SERIAL_PORT -> Connection.State.MAYBE_DETECTING_SERIAL_PORT
            OctoPrintConnection.State.MAYBE_DETECTING_BAUDRATE -> Connection.State.MAYBE_DETECTING_BAUDRATE
            OctoPrintConnection.State.MAYBE_CONNECTING -> Connection.State.MAYBE_CONNECTING
            OctoPrintConnection.State.MAYBE_OPERATIONAL -> Connection.State.MAYBE_OPERATIONAL
            OctoPrintConnection.State.MAYBE_CLOSED -> Connection.State.MAYBE_CLOSED
            OctoPrintConnection.State.MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT -> Connection.State.MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT
            OctoPrintConnection.State.MAYBE_CONNECTION_ERROR -> Connection.State.MAYBE_CONNECTION_ERROR
            OctoPrintConnection.State.MAYBE_DETECTING_SERIAL_CONNECTION -> Connection.State.MAYBE_DETECTING_SERIAL_CONNECTION
            OctoPrintConnection.State.MAYBE_PRINTING -> Connection.State.MAYBE_PRINTING
            OctoPrintConnection.State.MAYBE_UNKNOWN_ERROR -> Connection.State.MAYBE_UNKNOWN_ERROR
            OctoPrintConnection.State.OTHER -> Connection.State.OTHER
        },
        baudrate = current.baudrate,
        port = current.port,
        printerProfile = current.printerProfile,
    ),
    options = ConnectionState.Options(
        ports = options.ports,
        baudrates = options.baudrates,
        printerProfiles = options.printerProfiles.map {
            PrinterProfileReference(
                name = it.name,
                id = it.id,
            )
        },
        portPreference = options.portPreference,
        baudratePreference = options.baudratePreference,
        autoConnect = options.autoConnect,
        printerProfilePreference = options.printerProfilePreference,
    )
)