package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterProfile
import io.github.aakira.napier.Napier
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement

internal class OctoCustomBoundingBoxSerializer : KSerializer<OctoPrinterProfile.CustomBox> {

    private val nested = OctoPrinterProfile.CustomBox.serializer()

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterProfile.CustomBox", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: OctoPrinterProfile.CustomBox) =
        encoder.encodeSerializableValue(value = value, serializer = nested)

    override fun deserialize(decoder: Decoder): OctoPrinterProfile.CustomBox = when (val e = (decoder as JsonDecoder).decodeJsonElement()) {
        is JsonObject -> decoder.json.decodeFromJsonElement(e)
        else -> OctoPrinterProfile.CustomBox()
    }.also {
        Napier.d(tag = "OctoCustomBoundingBoxSerializer", message = "Deserialized: $it")
    }
}