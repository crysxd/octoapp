package de.crysxd.octoapp.engine.octoprint.dto.plugins.go2rtc

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Go2RtcExtras(
    @SerialName("stream") val stream: String? = null,
    @SerialName("streamRatio") val streamRatio: String? = null,
    @SerialName("streamTimeout") val streamTimeout: Float? = null
)