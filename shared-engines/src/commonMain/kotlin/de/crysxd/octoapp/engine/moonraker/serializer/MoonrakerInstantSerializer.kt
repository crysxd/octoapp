package de.crysxd.octoapp.engine.moonraker.serializer

import kotlinx.datetime.Instant
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

internal class MoonrakerInstantSerializer : KSerializer<Instant> {

    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerInstant", kind = PrimitiveKind.FLOAT)

    override fun deserialize(decoder: Decoder) = Instant.fromEpochMilliseconds((decoder.decodeDouble() * 1000.0).toLong())
    override fun serialize(encoder: Encoder, value: Instant) = encoder.encodeDouble(value.toEpochMilliseconds() / 1000.0)
}