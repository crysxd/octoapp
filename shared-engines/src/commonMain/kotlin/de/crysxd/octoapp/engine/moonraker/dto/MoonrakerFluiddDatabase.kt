package de.crysxd.octoapp.engine.moonraker.dto


import de.crysxd.octoapp.engine.framework.json.SafeBoolean
import de.crysxd.octoapp.engine.framework.json.SafeFloat
import de.crysxd.octoapp.engine.framework.json.SafeInt
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonNames

@Serializable
data class MoonrakerFluiddDatabase(
    @SerialName("cameras") val cameras: Cameras? = null,
    @SerialName("charts") val charts: Charts? = null,
    @SerialName("console") val console: Console? = null,
    @SerialName("layout") val layout: Layout? = null,
    @SerialName("macros") val macros: Macros? = null,
    @SerialName("uiSettings") val uiSettings: UiSettings? = null
) {

    @Serializable
    data class Cameras(
        @SerialName("cameras") val cameras: List<Camera?>? = null
    ) {
        @Serializable
        @OptIn(ExperimentalSerializationApi::class)
        data class Camera(
            @SerialName("enabled") val enabled: SafeBoolean = null,
            @SerialName("flipX") val flipX: SafeBoolean = null,
            @SerialName("flipY") val flipY: SafeBoolean = null,
            @SerialName("fpsidletarget") val fpsidletarget: SafeInt = null,
            @SerialName("fpstarget") val fpstarget: SafeInt = null,
            @SerialName("height") val height: SafeInt = null,
            @SerialName("id") val id: String? = null,
            @SerialName("name") val name: String? = null,
            @SerialName("type") val type: String? = null,
            @SerialName("url") val url: String? = null,
            @SerialName("rotation") @JsonNames("rotation", "rotate") val rotation: SafeInt = null,
        )
    }

    @Serializable
    data object Charts

    @Serializable
    data class Console(
        @SerialName("commandHistory") val commandHistory: List<String?>? = null
    )

    @Serializable
    data object Layout

    @Serializable
    data object Macros

    @Serializable
    data class UiSettings(
        @SerialName("dashboard") val dashboard: Dashboard? = null,
        @SerialName("editor") val editor: Editor? = null,
        @SerialName("general") val general: General? = null,
        @SerialName("theme") val theme: Theme? = null
    ) {
        @Serializable
        data class Dashboard(
            @SerialName("tempPresets") val tempPresets: List<TempPreset>? = null
        ) {
            @Serializable
            data class TempPreset(
                @SerialName("gcode") val gcode: String? = null,
                @SerialName("id") val id: String? = null,
                @SerialName("name") val name: String? = null,
                @SerialName("values") val values: Map<String, Component>? = null
            ) {
                @Serializable
                data class Component(
                    @SerialName("active") val active: Boolean? = null,
                    @SerialName("type") val type: String? = null,
                    @SerialName("value") val value: SafeFloat = null
                )
            }
        }

        @Serializable
        data class Editor(
            @SerialName("autoEditExtensions") val autoEditExtensions: List<String?>? = null,
            @SerialName("codeLens") val codeLens: Boolean? = null,
            @SerialName("confirmDirtyEditorClose") val confirmDirtyEditorClose: Boolean? = null,
            @SerialName("restoreViewState") val restoreViewState: String? = null
        )

        @Serializable
        data class General(
            @SerialName("instanceName") val instanceName: String? = null,
            @SerialName("zAdjustDistances") val zAdjustDistances: List<Float>? = null,
        )

        @Serializable
        data class Theme(
            @SerialName("currentTheme") val currentTheme: CurrentTheme? = null,
            @SerialName("isDark") val isDark: SafeBoolean = null,
            @SerialName("logo") val logo: Logo? = null
        ) {
            @Serializable
            data class CurrentTheme(
                @SerialName("primary") val primary: String? = null
            )

            @Serializable
            data class Logo(
                @SerialName("dynamic") val `dynamic`: SafeBoolean = null,
                @SerialName("src") val src: String? = null
            )
        }
    }
}