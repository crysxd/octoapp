package de.crysxd.octoapp.engine.octoprint.dto.event

import de.crysxd.octoapp.engine.framework.json.EngineJson
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString

@Serializable
internal data class OctoEventTransportThrottle(val throttle: Int = 1) {
    fun toMessageJson() = EngineJson.encodeToString(this)
}
