package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.octoprint.OctoConstants.toolLabels
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileObject
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileOrigin
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsDuration
import de.crysxd.octoapp.sharedcommon.ext.formatAsVolume
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.datetime.Instant

internal fun OctoFileObject.map(): FileObject = when (this) {
    is OctoFileObject.File -> FileObject.File(
        name = name,
        path = path,
        display = display,
        origin = origin.map(),
        size = size,
        type = type,
        date = date ?: Instant.DISTANT_PAST,
        prints = prints?.let { ph ->
            FileObject.PrintHistory(
                last = ph.last?.let { lp ->
                    FileObject.PrintHistory.LastPrint(
                        date = lp.date,
                        success = lp.success,
                    )
                },
                success = ph.success,
                failure = ph.failure,
            )
        },
        gcodeAnalysis = gcodeAnalysis?.let { ga ->
            FileObject.GcodeAnalysis(
                dimensions = ga.dimensions?.map(),
                estimatedPrintTime = ga.estimatedPrintTime,
                filament = ga.filament.toList().associate { (key, value) ->
                    value.map().let { key to it }
                }
            )
        },
        hash = hash,
        thumbnails = listOfNotNull(
            thumbnail?.let { path ->
                FileObject.File.Thumbnail(
                    width = 0,
                    height = 0,
                    path = path,
                )
            }
        ),
        typePath = typePath,
        metadata = listOf(
            FileObject.File.MetadataGroup(
                label = getString("file_manager___file_details___print_info"),
                items = listOf(
                    getString("file_manager___file_details___print_time") to gcodeAnalysis?.estimatedPrintTime?.formatAsSecondsDuration(),
                    getString("file_manager___file_details___model_size") to gcodeAnalysis?.dimensions?.let {
                        listOf(
                            it.width?.format(maxDecimals = 1),
                            it.depth?.format(maxDecimals = 1),
                            it.height?.format(maxDecimals = 1),
                        ).joinToString(" x ") + " mm"
                    },
                ) + (gcodeAnalysis?.filament ?: emptyMap()).map { (tool, filament) ->
                    listOfNotNull(
                        getString("file_manager___file_details___filament_use"),
                        "(${toolLabels[tool]})".takeIf { tool != "tool0" }
                    ).joinToString(" ") to listOf(
                        filament.length?.formatAsLength(onlyMilli = false),
                        filament.volume?.times(1000 /* cm3 -> mm3 */)?.formatAsVolume(),
                    ).joinToString(" / ")
                }
            ),
            FileObject.File.MetadataGroup(
                label = getString("file_manager___file_details___file"),
                items = listOf(
                    getString("location") to when (origin) {
                        OctoFileOrigin.SdCard -> getString("file_manager___file_details___file_location_sd_card")
                        OctoFileOrigin.Local -> getString("file_manager___file_details___file_location_local")
                    },
                    getString("file_manager___file_details___name") to name,
                    getString("file_manager___file_details___path") to (if (path == "/") "/" else "/" + path.removeSuffix(name)),
                    getString("file_manager___file_details___file_size") to size?.formatAsFileSize(),
                    getString("file_manager___file_details___uploaded") to date?.format()
                )
            ),
            FileObject.File.MetadataGroup(
                label = getString("file_manager___file_details___history"),
                items = listOf(
                    getString("file_manager___file_details___last_print") to prints?.last.let { last ->
                        if (last != null) {
                            getString(
                                id = if (last.success) "file_manager___file_details___last_print_at_x_success" else "file_manager___file_details___last_print_at_x_failure",
                                last.date.format()
                            )
                        } else {
                            getString("file_manager___file_details___never")
                        }
                    },
                    getString("file_manager___file_details___completed") to prints?.success.let { times ->
                        times?.let { getString("x_times", it) } ?: getString("file_manager___file_details___never")
                    },
                    getString("file_manager___file_details___failures") to prints?.failure.let { times ->
                        times?.let { getString("x_times", it) } ?: getString("file_manager___file_details___never")
                    },
                )
            )
        ).filter {
            it.items.isNotEmpty()
        }
    )

    is OctoFileObject.Folder -> children.map { it.map() }.let { children ->
        FileObject.Folder(
            name = name,
            path = path,
            display = display,
            size = size,
            origin = origin.map(),
            children = children,
        )
    }
}

private fun OctoFileObject.GcodeAnalysis.Dimensions.map(): FileObject.GcodeAnalysis.Dimensions {
    return FileObject.GcodeAnalysis.Dimensions(
        depth = depth,
        height = height,
        width = width,
    )
}

private fun OctoFileObject.GcodeAnalysis.FilamentUse.map(): FileObject.GcodeAnalysis.FilamentUse {
    return FileObject.GcodeAnalysis.FilamentUse(
        volume = volume,
        length = length,
    )
}