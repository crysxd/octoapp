package de.crysxd.octoapp.engine.moonraker

import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.api.FilesApi
import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.api.MaterialsApi
import de.crysxd.octoapp.engine.api.ObicoApi
import de.crysxd.octoapp.engine.api.OctoAppCompanionApi
import de.crysxd.octoapp.engine.api.OctoEverywhereApi
import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.api.PrinterApi
import de.crysxd.octoapp.engine.api.PrinterProfileApi
import de.crysxd.octoapp.engine.api.SettingsApi
import de.crysxd.octoapp.engine.api.SystemApi
import de.crysxd.octoapp.engine.api.TimelapseApi
import de.crysxd.octoapp.engine.api.UserApi
import de.crysxd.octoapp.engine.api.VersionApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerExtruderApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerFilesApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerJobApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerLoginApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerMacroApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerMaterialApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerObicoApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerOctoAppCompanionApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerOctoEverywhereApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerPowerDevicesApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerPrinterApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerPrinterProfileApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerProbeApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerSettingsApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerSpoolmanApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerSystemApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerTimelapseApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerUseApi
import de.crysxd.octoapp.engine.moonraker.api.MoonrakerVersionApi
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerConnection
import de.crysxd.octoapp.sharedcommon.Platform
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.http.Url
import kotlinx.coroutines.flow.StateFlow

class MoonrakerEngine internal constructor(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: () -> HttpClient,
    settings: MoonrakerEngineBuilder.HttpClientSettings,
    platform: Platform,
    apiKey: String?,
) : PrinterEngine {

    private val tag = "OctoPrintEngine"
    private val connection = MoonrakerConnection(
        baseUrlRotator = baseUrlRotator,
        httpClient = httpClient,
        settings = settings,
        probe = { probe(baseUrlRotator) },
        apiKey = apiKey,
        platform = platform,
    )
    private val genericHttpClient = httpClient()
    private val probeApi = MoonrakerProbeApi(baseUrlRotator = baseUrlRotator, httpClient = httpClient())
    override val eventSource: EventSource = connection.eventHandler
    override val baseUrl: StateFlow<Url> = baseUrlRotator.activeUrl
    override val jobApi = MoonrakerJobApi(requestHandler = connection.requestHandler, eventHandler = connection.eventHandler)
    override val loginApi: LoginApi = MoonrakerLoginApi()
    override val printerApi: PrinterApi = MoonrakerPrinterApi(requestHandler = connection.requestHandler, eventHandler = connection.eventHandler)
    override val printerProfileApi: PrinterProfileApi = MoonrakerPrinterProfileApi(requestHandler = connection.requestHandler)
    override val systemApi: SystemApi = MoonrakerSystemApi(requestHandler = connection.requestHandler, httpClient = httpClient, baseUrlRotator = baseUrlRotator)
    override val settingsApi: SettingsApi = MoonrakerSettingsApi(requestHandler = connection.requestHandler, eventHandler = connection.eventHandler) {
        systemApi.getSystemInfo()
    }
    override val timelapseApi: TimelapseApi = MoonrakerTimelapseApi()
    override val userApi: UserApi = MoonrakerUseApi(probeApi = probeApi)
    override val versionApi: VersionApi = MoonrakerVersionApi(requestHandler = connection.requestHandler)
    override val materialsApi: MaterialsApi = MoonrakerMaterialApi(
        MoonrakerSpoolmanApi(requestHandler = connection.requestHandler, eventHandler = connection.eventHandler)
    )
    override val obicoApi: ObicoApi = MoonrakerObicoApi(httpClient = httpClient(), baseUrlRotator = baseUrlRotator, requestHandler = connection.requestHandler)
    override val octoEverywhereApi: OctoEverywhereApi = MoonrakerOctoEverywhereApi(requestHandler = connection.requestHandler)
    override val octoAppCompanionApi: OctoAppCompanionApi = MoonrakerOctoAppCompanionApi(requestHandler = connection.requestHandler)

    override val powerDevicesApi: PowerDevicesApi = MoonrakerPowerDevicesApi(
        requestHandler = connection.requestHandler,
        eventHandler = connection.eventHandler,
        printerApi = MoonrakerPrinterApi(requestHandler = connection.requestHandler, eventHandler = connection.eventHandler)
    )
    override val filesApi: FilesApi = MoonrakerFilesApi(
        baseUrlRotator = baseUrlRotator,
        httpClient = httpClient(),
        requestHandler = connection.requestHandler,
        eventHandler = connection.eventHandler
    )

    // Moonraker specifics
    val extruderApi = MoonrakerExtruderApi(requestHandler = connection.requestHandler)
    val macroApi = MoonrakerMacroApi(requestHandler = connection.requestHandler)

    init {
        Napier.i(tag = tag, message = "New engine for ${baseUrl.value} (this=$this)")
        baseUrlRotator.consumeActiveFlow(
            active = connection.eventHandler.active,
            probe = ::probe
        ) { connection.reconnect() }
    }


    private suspend fun probe(
        baseUrlRotator: BaseUrlRotator,
        throwException: Boolean = false
    ): Boolean = MoonrakerProbeApi(
        httpClient = httpClient(),
        baseUrlRotator = baseUrlRotator
    ).probe(throwException = throwException)

    override suspend fun notifyConnectionChange() = baseUrlRotator.considerUpgradingConnection(trigger = "notify-connection-change", forced = true)
    override suspend fun considerConnectionChange() = baseUrlRotator.considerUpgradingConnection(trigger = "consider-connection-change", forced = false)
    override suspend fun probe(throwException: Boolean) = probe(baseUrlRotator, throwException = throwException)

    override fun destroy() {
        genericHttpClient.close()
        connection.destroy()
    }

    override suspend fun <T> genericRequest(block: suspend (Url, HttpClient) -> T): T = baseUrlRotator.request {
        block(it, genericHttpClient)
    }
}