package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.AggregatorMaterialsApi
import de.crysxd.octoapp.engine.api.MaterialsApi

class OctoMaterialApi(
    private vararg val aggregates: AggregatorMaterialsApi.MaterialApiAggregate
) : MaterialsApi by AggregatorMaterialsApi(
    aggregates = aggregates.toList()
)