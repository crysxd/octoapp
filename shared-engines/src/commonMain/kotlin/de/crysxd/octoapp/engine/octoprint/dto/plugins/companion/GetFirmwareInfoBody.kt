package de.crysxd.octoapp.engine.octoprint.dto.plugins.companion

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal class GetFirmwareInfoBody : CommandBody {
    override val command: String = "getPrinterFirmware"
}