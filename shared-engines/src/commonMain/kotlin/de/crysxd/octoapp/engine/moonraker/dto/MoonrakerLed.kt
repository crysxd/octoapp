package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerLed(
    @SerialName("color_data") val colorData: List<List<Float?>?>? = null
) : MoonrakerStatusItem {

    override fun toString() = "MoonrakerLed(colorData=${colorData.hashCode()})"

    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        colorData = colorData ?: (previous as? MoonrakerLed)?.colorData
    )
}