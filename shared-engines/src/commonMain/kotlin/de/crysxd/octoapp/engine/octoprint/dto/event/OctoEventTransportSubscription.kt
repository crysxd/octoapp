package de.crysxd.octoapp.engine.octoprint.dto.event

import de.crysxd.octoapp.engine.framework.json.EngineJson
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Serializable
internal data class OctoEventTransportSubscription(
    val state: State = State.NoLogs(),
    val plugins: List<String> = emptyList(),
    val events: List<String> = listOf(
        "PrinterStateChanged",
        "PrinterProfileModified",
        "Connecting",
        "Connected",
        "Disconnected",
        "UpdatedFiles",
        "PrintStarted",
        "FileSelected",
        "PrintCancelling",
        "PrintCancelled",
        "PrintPausing",
        "PrintPaused",
        "PrintFailed",
        "PrintDone",
        "PrintResumed",
        "FirmwareData",
        "SettingsUpdated",
        "MovieRendering",
        "MovieDone",
        "MovieFailed",
    ),
) {

    fun toMessageJson() = EngineJson.encodeToString(mapOf("subscribe" to this))

    @Serializable
    sealed class State {
        abstract val messages: Boolean
        abstract val state: Boolean
        abstract fun toJson(): String

        @Serializable
        data class AllLogs(
            override val messages: Boolean = false,
            override val state: Boolean = true,
        ) : State() {
            val logs: Boolean = true
            override fun toJson() = Json.encodeToString(this)
        }

        @Serializable
        data class NoLogs(
            override val messages: Boolean = false,
            override val state: Boolean = true,
        ) : State() {
            val logs: Boolean = false
            override fun toJson() = Json.encodeToString(this)
        }

        @Serializable
        data class SomeLogs(
            override val messages: Boolean = false,
            override val state: Boolean = true,
            val logs: String
        ) : State() {
            override fun toJson() = Json.encodeToString(this)
        }
    }
}
