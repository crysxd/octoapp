package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonPrimitive

@Serializable
internal data class MoonrakerBedMesh(
    @SerialName("mesh_matrix") val meshMatrix: List<List<Float>>? = null,
    @SerialName("mesh_max") val meshMax: List<Float>? = null,
    @SerialName("mesh_min") val meshMin: List<Float>? = null,
    @SerialName("probed_matrix") val probedMatrix: List<List<Float>>? = null,
    @SerialName("profile_name") val profileName: String? = null,
    @SerialName("profiles") val profiles: Map<String, Profile>? = null
) : MoonrakerStatusItem {

    @Serializable
    data class Profile(
        @SerialName("mesh_params") val meshParams: Map<String, JsonPrimitive>? = null,
        @SerialName("points") val points: List<List<Float>>? = null
    )

    override fun backFillWithPrevious(
        previous: MoonrakerStatusItem,
    ): MoonrakerStatusItem = copy(
        meshMatrix = meshMatrix ?: (previous as? MoonrakerBedMesh)?.meshMatrix,
        meshMax = meshMax ?: (previous as? MoonrakerBedMesh)?.meshMax,
        meshMin = meshMin ?: (previous as? MoonrakerBedMesh)?.meshMin,
        probedMatrix = probedMatrix ?: (previous as? MoonrakerBedMesh)?.probedMatrix,
        profileName = profileName ?: (previous as? MoonrakerBedMesh)?.profileName,
        profiles = profiles ?: (previous as? MoonrakerBedMesh)?.profiles,
    )
}