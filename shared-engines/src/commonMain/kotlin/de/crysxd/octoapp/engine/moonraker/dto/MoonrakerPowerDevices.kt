package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerPowerDevices(
    @SerialName("devices") val devices: List<Device?>? = null
) {
    @Serializable
    data class Device(
        @SerialName("device") val device: String? = null,
        @SerialName("locked_while_printing") val lockedWhilePrinting: Boolean? = null,
        @SerialName("status") val status: Status? = null,
        @SerialName("type") val type: String? = null
    )

    @Serializable
    enum class Status {
        @SerialName("on")
        ON,
        @SerialName("off")
        OFF,
    }
}