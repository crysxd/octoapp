package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.files.FileList
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileReference
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.CommonTypeParceler
import de.crysxd.octoapp.sharedcommon.InstantParceler
import kotlinx.datetime.Instant

internal fun List<MoonrakerFileReference>.map(origin: FileOrigin): FileList {
    val nodes = mapNotNull { ref ->
        if (ref.isDirectory) {
            MoonrakerFolderReference(
                path = ref.path ?: ref.dirname ?: return@mapNotNull null,
                origin = origin,
                children = emptyList(),
            )
        } else {
            MoonrakerFileReference(
                path = ref.path ?: ref.filname ?: return@mapNotNull null,
                date = ref.modified ?: return@mapNotNull null,
                size = ref.size,
                origin = origin,
            )
        }
    }.groupBy { ref ->
        // Group by parent path, "" for root
        ref.path.split("/").dropLast(1).joinToString("/")
    }.map { (nodePath, children) ->
        MoonrakerFolderReference(
            children = children,
            path = nodePath,
            origin = origin,
        )
    }

    val rootNode = fillChildNodes(
        node = nodes.firstOrNull { it.path == "" } ?: MoonrakerFolderReference(
            children = emptyList(),
            path = "",
            origin = origin,
        ),
        allNodes = nodes
    )

    return FileList(
        files = rootNode,
        total = sumOf { it.size ?: 0 },
        free = null,
    )
}

private fun fillChildNodes(
    node: MoonrakerFolderReference,
    allNodes: List<MoonrakerFolderReference>,
    depth: Int = 0
): MoonrakerFolderReference {
    val childNodes = allNodes.filter { potentialChild ->
        val b = isChildPath(parent = node.path, child = potentialChild.path)
        b
    }.map { child ->
        // We had issues with stack overflow for one user here at a depth of 12
        // As this is kinda an edge case let's limit to depth of 10 which should be
        // fine for almost all use cases. If somebody reaches out "my folder is empty"
        // we can investigate. Better to have 10 levels than 0 :)
        if (depth < 10) {
            fillChildNodes(child, allNodes, depth + 1)
        } else {
            child
        }
    }

    return node.copy(
        children = childNodes + node.children
    )
}

private fun isChildPath(parent: String, child: String): Boolean {
    if (parent == child || child == "") return false
    val childPath = child.split("/").dropLast(1).joinToString("/")
    return childPath == parent.removeSuffix("/")
}

@CommonParcelize
private data class MoonrakerFolderReference(
    override val children: List<FileReference>,
    override val path: String,
    override val id: String = path,
    override val origin: FileOrigin,
) : FileReference.Folder {
    override val display: String get() = name
}

@CommonParcelize
private data class MoonrakerFileReference(
    override val path: String,
    override val size: Long?,
    override val id: String = path,
    override val origin: FileOrigin,
    @CommonTypeParceler<Instant, InstantParceler>() override val date: Instant,
) : FileReference.File {
    override val display: String get() = name
}