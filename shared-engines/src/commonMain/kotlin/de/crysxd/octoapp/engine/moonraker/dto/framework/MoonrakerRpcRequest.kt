package de.crysxd.octoapp.engine.moonraker.dto.framework

import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerRpcParams
import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerRpcParamsSerializer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.updateAndGet
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Suppress("SERIALIZER_TYPE_INCOMPATIBLE")
internal data class MoonrakerRpcRequest<Params : MoonrakerRpcParams>(
    @Serializable(with = MoonrakerRpcParamsSerializer::class) val params: Params?,
    val method: String,
) {
    @SerialName("jsonrpc")
    val jsonRpc = "2.0"

    @SerialName("id")
    val id: Int = counter.updateAndGet { it + 1 }

    companion object {
        private var counter = MutableStateFlow(0)
    }
}
