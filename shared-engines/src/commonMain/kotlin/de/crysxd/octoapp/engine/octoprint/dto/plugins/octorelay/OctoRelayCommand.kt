package de.crysxd.octoapp.engine.octoprint.dto.plugins.octorelay

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoRelayCommand(
    override val command: String,
    val pin: String,
    val subject: String = pin
) : CommandBody