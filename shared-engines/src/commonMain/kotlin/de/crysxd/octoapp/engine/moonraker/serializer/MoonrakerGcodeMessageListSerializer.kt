package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMessage
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMessageList
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject

class MoonrakerGcodeMessageListSerializer : KSerializer<MoonrakerGcodeMessageList> {

    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerGcodeMessageList", kind = PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): MoonrakerGcodeMessageList {
        val input = decoder as JsonDecoder
        val items = input.decodeJsonElement().jsonObject["gcode_store"]?.jsonArray
        requireNotNull(items) { "Missing items" }
        return MoonrakerGcodeMessageList(
            messages = EngineJson.decodeFromJsonElement(ListSerializer(MoonrakerGcodeMessage.serializer()), items)
        )
    }

    override fun serialize(encoder: Encoder, value: MoonrakerGcodeMessageList) = throw UnsupportedOperationException("Can't serializer")

}