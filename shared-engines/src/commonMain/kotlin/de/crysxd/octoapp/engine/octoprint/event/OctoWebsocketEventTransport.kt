package de.crysxd.octoapp.engine.octoprint.event

import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.exceptions.WebSocketUnstableException
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.WebsocketExceptionHandler
import de.crysxd.octoapp.engine.framework.WebsocketWatchdog
import de.crysxd.octoapp.engine.framework.guessConnectionType
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportAuthentication
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.mappers.map
import de.crysxd.octoapp.engine.octoprint.serializer.OctoEventMessageSerializer
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.plugins.websocket.DefaultClientWebSocketSession
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.http.URLProtocol
import io.ktor.http.Url
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.time.Duration.Companion.seconds

internal class OctoWebsocketEventTransport(
    parentJob: Job,
    parenTag: String,
    loginApi: () -> LoginApi,
    probe: suspend () -> Unit,
    private val eventSink: OctoEventSource,
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val onCurrentReceived: (Pair<String, OctoMessage.Current>?) -> Unit,
    private val onWebsocketBroken: suspend (OctoWebsocketEventTransport, Throwable?) -> Unit,
) : OctoEventTransport(
    loginApi = loginApi,
    parentJob = parentJob,
    parenTag = parenTag,
) {

    private var compatReAuthDetection = true
    private val noMessageTimeout = if (OctoWebsocketConfig.canUseLongNoMessageTimeout) {
        OctoWebsocketConfig.longNoMessageTimeout
    } else {
        OctoWebsocketConfig.shotNoMessageTimeout
    }

    private var messageCounter = 0L
    private var currentMessageCounter = 0L
    private var noConnectionMessageCounter = 0
    private var destroyed = false
    private var currentBaseUrl: Url? = null
    private var websocketUrl: Url? = null
    private val exceptionHandler = WebsocketExceptionHandler(
        reconnect = ::connect,
        probe = probe,
        onWebsocketBroken = { onWebsocketBroken(this, it) },
        eventSink = eventSink,
        logTag = tag,
        baseUrlRotator = baseUrlRotator,
        connectionNeeded = { eventSink.active.value },
        disconnect = ::disconnect
    )
    private val watchdog = WebsocketWatchdog(
        logTag = { tag },
        eventSink = eventSink,
        onWebsocketBroken = { onWebsocketBroken(this, it) },
        reconnect = ::connect,
        noMessageTimeout = noMessageTimeout,
        connectTimeout = 30.seconds
    )
    override val name get() = "WS"

    override fun connect() {
        recreateScope().launch {
            // Any exception in this block is handled by the scope's exception handler
            messageCounter = 0
            currentMessageCounter = 0
            destroyed = false
            onCurrentReceived(null)

            // Set up watchdog for our obscure websocket bug
            Napier.i(tag = tag, message = "Starting connecting")
            watchdog.notifyConnectionStarted(scope)

            baseUrlRotator.request { baseUrl ->
                if (currentBaseUrl != baseUrl) {
                    messageCounter = 0
                }

                currentBaseUrl = baseUrl
                Napier.i(tag = tag, message = "Connecting using $baseUrl")

                httpClient.webSocket(
                    request = {
                        urlFromPath(baseUrl = baseUrl, "sockjs", "websocket") {
                            protocol = if (baseUrl.protocol == URLProtocol.HTTPS) URLProtocol.WSS else URLProtocol.WS
                        }
                        websocketUrl = url.build()
                    }
                ) {
                    Napier.i(tag = tag, message = "Starting websocket main loop")
                    watchdog.notifyConnectionEstablished(scope)

                    receiveAllMessagedAsync(connectionType = call.guessConnectionType())
                    sendConfigurations()
                    ensureConnected()
                    ensureAuthValid()
                    sendAuthentication()

                    // Wait until we are closed.
                    job?.join()
                }
            }
        }.invokeOnCompletion {
            if (job?.isCancelled != true && job?.isActive == true && it == null) {
                // This happens on iOS when the baseUrl is changed. No biggy, just connect again.
                Napier.d(
                    tag = tag,
                    message = "Main loop exited but job not cancelled, connecting again after delay isCompleted=${job?.isCompleted} isCancelled=${job?.isCancelled} isActive=${job?.isActive}"
                )
                scope.launch {
                    delay(1.seconds)
                    Napier.w(tag = tag, message = "Re-connecting now")
                    connect()
                }
            }
        }
    }

    override fun handleException(e: Throwable) {
        destroyed = true
        scope.launch {
            exceptionHandler.handleException(e)
        }
    }

    private fun ensureAuthValid() = scope.launch {
        // Prior to OctoPrint 1.8.4 the only way to tell if the server declined the auth message
        // is to see if there is nothing send but the "connected" message. After 1.8.4 a "reauth" message is sent
        val minMessageCount = 3
        val time = 6.seconds
        delay(time)

        Napier.i(tag = tag, message = "Setting up authentication watchdog for old OctoPrint versions to ensure auth valid")
        if (minMessageCount > messageCounter && compatReAuthDetection) {
            Napier.w(tag = tag, message = "Did not receive $minMessageCount messages within $time, authentication might be broken. Re-authenticating in new session.")
            reAuthenticate()
        }
    }

    private fun ensureConnected() = scope.launch {
        // We want to receive at least one message (connected)
        val time = 5.seconds
        delay(time)

        Napier.i(tag = tag, message = "Setting up connected message watchdog")
        if (messageCounter == 0L) {
            noConnectionMessageCounter++
            if (noConnectionMessageCounter > 1) {
                Napier.w(tag = tag, message = "Did not receive one message within $time multiple times, considering websocket broken")
                onWebsocketBroken(this@OctoWebsocketEventTransport, null)
            } else {
                Napier.w(tag = tag, message = "Did not receive one message within $time, reconnecting")
                reAuthenticate()
            }
        }
    }

    private suspend fun reAuthenticate() {
        Napier.w(tag = tag, message = "Re-authenticating...")
        logOut()
        delay(1000)
        connect()
    }

    private suspend fun DefaultClientWebSocketSession.sendAuthentication() {
        val auth = logIn()
        Napier.i(tag = tag, message = "Sending auth: ${auth.split(":")[0]}:******")
        outgoing.send(Frame.Text(text = EngineJson.encodeToString(OctoEventTransportAuthentication(auth))))
    }


    private fun DefaultClientWebSocketSession.sendConfigurations() = scope.launch {
        Napier.i(tag = tag, message = "Sending configurations")

        config.collectLatest {
            if (beforeSendingConfig()) {
                Napier.i(tag = tag, message = "Sending throttle: ${it.throttle}")
                outgoing.send(Frame.Text(it.throttle.toMessageJson()))
                Napier.i(tag = tag, message = "Sending subscription: ${it.subscription}")
                outgoing.send(Frame.Text(it.subscription.toMessageJson()))
            }
        }
    }

    private fun DefaultClientWebSocketSession.receiveAllMessagedAsync(connectionType: ConnectionType) = scope.launch {
        Napier.i(tag = tag, message = "Waiting for messages")
        var first = true

        while (isActive) {
            when (val frame = incoming.receive()) {
                is Frame.Binary -> Napier.v(tag = tag, message = "Received binary, ignoring")
                is Frame.Text -> {
                    val text = frame.readText()
                    if (first) {
                        first = false
                        Napier.i(tag = tag, message = "Received first message, ${text.take(100)}")
                    } else {
                        Napier.v(tag = tag, message = "Received message, ${text.take(1000)}")
                    }
                    receiveTextMessage(text, connectionType)
                }

                is Frame.Close -> {
                    Napier.v(tag = tag, message = "Received close")
                    disconnect()
                }

                is Frame.Ping -> Napier.v(tag = tag, message = "Received Ping")
                is Frame.Pong -> Napier.v(tag = tag, message = "Received Pong")
                else -> Napier.v(tag = tag, message = "Received unknown message: $frame")
            }

            watchdog.notifyMessageReceived()
        }
    }

    private suspend fun receiveTextMessage(text: String, connectionType: ConnectionType) {
        handleFirstMessage(connectionType)

        try {
            // Message might be null -> dropped
            val message = Json.decodeFromString(deserializer = OctoEventMessageSerializer(), string = text)

            when (message) {
                is OctoMessage.Connected -> {
                    // Prior to OctoPrint 1.8.4 the only way to tell if the server declined the auth message
                    // is to see if there is nothing send but the "connected" message. After 1.8.4 a "reauth" message is sent
                    onConnected(message)
                    compatReAuthDetection = message.version?.asVersion()?.let { "1.8.4".asVersion() > it } ?: true
                    if (compatReAuthDetection) {
                        Napier.d(tag = tag, message = "Using re-auth compat layer")
                    }
                }

                is OctoMessage.ReAuthRequired -> {
                    // We need to reset the connection and re-authenticate. Do not emit.
                    scope.launch { reAuthenticate() }
                    return
                }

                is OctoMessage.Current -> {
                    // Forward to special handler (in addition to emitting below)
                    onCurrentReceived(text to message)
                }
            }

            if (message !is OctoMessage.UnknownMessage && !destroyed && message != null) {
                eventSink.emitEvent(Event.MessageReceived(message.map()))
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failed to decode message: ${text.take(128)}")
        }
    }

    private suspend fun handleFirstMessage(connectionType: ConnectionType) {
        messageCounter++

        if (messageCounter == 1L) {
            Napier.i("Emitting connected via ${connectionType.name}")
            eventSink.emitEvent(Event.Connected(connectionType = connectionType, connectionQuality = ConnectionQuality.Normal))
            noConnectionMessageCounter = 0
        }
    }

    private fun WebSocketUnstableException(cause: Throwable) = WebSocketUnstableException(
        webSocketUrl = websocketUrl ?: currentBaseUrl ?: baseUrlRotator.activeUrl.value,
        webUrl = currentBaseUrl ?: baseUrlRotator.activeUrl.value,
        cause = cause,
    )
}