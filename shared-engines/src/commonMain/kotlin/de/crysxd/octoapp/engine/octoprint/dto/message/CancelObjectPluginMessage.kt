package de.crysxd.octoapp.engine.octoprint.dto.message

import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

sealed class CancelObjectPluginMessage {

    object Other : CancelObjectPluginMessage(), OctoMessage, Message

    @Serializable
    data class Active(
        @SerialName("ActiveID") val activeId: Int? = null
    ) : CancelObjectPluginMessage(), OctoMessage, Message

    @Serializable
    data class ObjectList(
        val objects: List<Object>? = null,
    ) : CancelObjectPluginMessage(), OctoMessage, Message {

        @Serializable
        data class Object(
            val active: Boolean? = false,
            val id: Int? = null,
            val cancelled: Boolean? = false,
            val ignore: Boolean? = false,
            @SerialName("max_x") val maxX: Float?,
            @SerialName("min_x") val minX: Float?,
            @SerialName("max_y") val maxY: Float?,
            @SerialName("min_y") val minY: Float?,
            @SerialName("object") val label: String? = null,
        )
    }
}