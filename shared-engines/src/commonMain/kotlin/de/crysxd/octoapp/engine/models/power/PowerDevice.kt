package de.crysxd.octoapp.engine.models.power

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import io.github.aakira.napier.Napier
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.retryWhen
import kotlinx.coroutines.isActive
import kotlin.time.Duration.Companion.seconds

interface PowerDevice {
    val id: String
    val pluginId: String
    val displayName: String
    val pluginDisplayName: String
    val capabilities: List<Capability>
    val controlMethods: List<ControlMethod> get() = listOf(ControlMethod.TurnOnOff, ControlMethod.Toggle)
    val uniqueId
        get() = "$pluginId:$id"

    suspend fun toggle() {
        val status = isOn() ?: throw IllegalStateException("Can't determine current state")
        if (status) turnOff() else turnOn()
    }

    // Basic IO
    suspend fun turnOn()
    suspend fun turnOff()
    suspend fun isOn(): Boolean?
    fun isOnFlow() = flow {
        while (currentCoroutineContext().isActive) {
            emit(isOn())
            delay(1.seconds)
        }
    }.retryWhen { cause, attempt ->
        Napier.e(tag = "PowerDevice", message = "Failed to get current state (attempt=$attempt)", throwable = cause)
        if (attempt > 1) emit(null)
        delay(1.seconds)
        true
    }

    // RGBW
    suspend fun setColor(color: RgbwColor): Unit = throw UnsupportedOperationException("Cannot control color")
    fun colorFlow(): Flow<List<RgbwColor>?> = throw UnsupportedOperationException("Cannot control color")

    // PWM
    suspend fun setPwmDutyCycle(dutyCycle: Float): Unit = throw UnsupportedOperationException("Cannot control color")
    fun pwmDutyCycleFlow(): Flow<Float?> = throw UnsupportedOperationException("Cannot control color")

    sealed class Capability {
        data object ControlPrinterPower : Capability()
        data object Illuminate : Capability()
    }

    sealed class ControlMethod {
        data object TurnOnOff : ControlMethod()
        data object Toggle : ControlMethod()
        data object RgbwColor : ControlMethod()
        data object Pwm : ControlMethod()
    }

    @CommonParcelize
    data class RgbwColor(val r: Float, val g: Float, val b: Float, val w: Float) : CommonParcelable

}