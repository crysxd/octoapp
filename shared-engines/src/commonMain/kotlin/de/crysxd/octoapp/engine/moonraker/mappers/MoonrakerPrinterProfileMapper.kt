package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.moonraker.Constants.PrinterObjects.HeaterBed
import de.crysxd.octoapp.engine.moonraker.Constants.PrinterObjects.HeaterChamber
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExtruder
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerHeaters
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerMcu
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerToolhead
import io.github.aakira.napier.Napier

internal data class MoonrakerPrinterProfileMapperInput(
    val toolhead: MoonrakerToolhead?,
    val heaters: MoonrakerHeaters?,
    val extruders: Map<String, MoonrakerExtruder>,
    val mcus: Map<String, MoonrakerMcu>,
    val config: MoonrakerConfigFile?,
)

internal fun MoonrakerPrinterProfileMapperInput.map(): PrinterProfile = PrinterProfile(
    id = mcus.map { (key, value) -> "$key:${value.mcu?.mcuVersion}:${value.mcu?.mcuBuildVersions}" }.joinToString("__"),
    current = true,
    default = true,
    model = "Klipper",
    name = "Default",
    color = "default",
    volume = let {
        val xMax = toolhead?.axisMaximum?.getOrNull(0) ?: 200f
        val yMax = toolhead?.axisMaximum?.getOrNull(1) ?: 200f
        val zMax = toolhead?.axisMaximum?.getOrNull(2) ?: 200f
        val xMin = toolhead?.axisMinimum?.getOrNull(0) ?: 0f
        val yMin = toolhead?.axisMinimum?.getOrNull(1) ?: 0f
        val zMin = toolhead?.axisMinimum?.getOrNull(2) ?: 0f
        PrinterProfile.Volume(
            width = xMax - xMin,
            depth = yMax - yMin,
            height = zMax - zMin,
            boundingBox = PrinterProfile.CustomBox(
                xMax = xMax,
                yMax = yMax,
                zMax = zMax,
                xMin = xMin,
                yMin = yMin,
                zMin = zMin,
            ),
            origin = if (xMin + xMax < 5f) PrinterProfile.Origin.Center else PrinterProfile.Origin.LowerLeft,
            formFactor = PrinterProfile.FormFactor.Rectangular,
        )
    },
    axes = PrinterProfile.Axes(
        e = PrinterProfile.Axis(
            inverted = false,
            speed = null,
        ),
        x = PrinterProfile.Axis(
            inverted = false,
            speed = null,
        ),
        y = PrinterProfile.Axis(
            inverted = false,
            speed = null,
        ),
        z = PrinterProfile.Axis(
            inverted = false,
            speed = null,
        )
    ),
    heatedBed = heaters?.availableHeaters?.contains(HeaterBed) == true,
    heatedChamber = heaters?.availableHeaters?.contains(HeaterChamber) == true,
    extruders = extruders.flatMap { (key, _) ->
        listOf(
            PrinterProfile.Extruder(
                componentName = key,
                nozzleDiameter = try {
                    config?.getConfigItem<MoonrakerConfigFile.Extruder>(toolhead?.extruder ?: "extruder")?.nozzleDiameter
                } catch (e: Exception) {
                    Napier.e(tag = "MoonrakerPrinterProfileMapperInput", message = "Failed to get nozzle diameter", throwable = e)
                    null
                } ?: 0.4f,
                // This doesn't support shared nozzles yet, most likely we need to get the config for this and nozzle diameter. List motors here.
                extruderComponents = listOf(key)
            )
        )
    }
)