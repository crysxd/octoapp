package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.remote.ObicoStatus
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.ObicoCamFrame
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.ObicoDataUsage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.ObicoPrediction
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.http.appendEncodedPathSegments

abstract class ObicoApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) {

    suspend fun getObicoCamFrame(webcamIndex: Int = 0) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "_tsd_", "webcam", "$webcamIndex") {
                // Force trailing /
                appendEncodedPathSegments("")
            }
        }.body<ObicoCamFrame>()
    }

    suspend fun getDataUsage() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "_tsd_", "tunnelusage") {
                // Force trailing /
                appendEncodedPathSegments("")
            }
        }.body<ObicoDataUsage>()
    }

    suspend fun getPrintPrediction() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "_tsd_", "prediction") {
                // Force trailing /
                appendEncodedPathSegments("")
            }
        }.body<ObicoPrediction>()
    }

    abstract suspend fun getPluginStatus(): ObicoStatus
}
