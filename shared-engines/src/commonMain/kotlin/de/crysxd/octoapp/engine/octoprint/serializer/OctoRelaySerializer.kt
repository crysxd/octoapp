package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.boolean
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

internal class OctoRelaySerializer : KSerializer<OctoSettings.OctoRelay> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("OctoRelay", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: OctoSettings.OctoRelay) {
        throw IllegalStateException("Not supported")
    }

    override fun deserialize(decoder: Decoder): OctoSettings.OctoRelay {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonObject

        val devices = tree.entries.map {
            OctoSettings.OctoRelay.Device(
                id = it.key,
                labelText = (it.value.jsonObject["labelText"] ?: it.value.jsonObject["label_text"])?.jsonPrimitive?.content ?: it.key,
                active = it.value.jsonObject["active"]?.jsonPrimitive?.boolean == true,
            )
        }
        return OctoSettings.OctoRelay(devices)
    }
}