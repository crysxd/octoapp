package de.crysxd.octoapp.engine.moonraker.dto


import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericTemperatureSensor
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerTemperatureFan(
    @SerialName("speed") val speed: Float? = null,
    @SerialName("target") val target: Float? = null,
    @SerialName("temperature") override val temperature: Float? = null
) : MoonrakerStatusItem, MoonrakerGenericTemperatureSensor {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        speed = speed ?: (previous as? MoonrakerTemperatureFan)?.speed,
        target = target ?: (previous as? MoonrakerTemperatureFan)?.target,
        temperature = temperature ?: (previous as? MoonrakerTemperatureFan)?.temperature,
    )
}