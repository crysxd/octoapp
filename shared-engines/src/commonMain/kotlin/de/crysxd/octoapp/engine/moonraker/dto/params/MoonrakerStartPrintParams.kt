package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerStartPrintParams(
    @SerialName("filename") val filename: String,
) : MoonrakerRpcParams