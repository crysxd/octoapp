package de.crysxd.octoapp.engine

import de.crysxd.octoapp.engine.api.FilesApi
import de.crysxd.octoapp.engine.api.JobApi
import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.api.MaterialsApi
import de.crysxd.octoapp.engine.api.ObicoApi
import de.crysxd.octoapp.engine.api.OctoAppCompanionApi
import de.crysxd.octoapp.engine.api.OctoEverywhereApi
import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.api.PrinterApi
import de.crysxd.octoapp.engine.api.PrinterProfileApi
import de.crysxd.octoapp.engine.api.SettingsApi
import de.crysxd.octoapp.engine.api.SystemApi
import de.crysxd.octoapp.engine.api.TimelapseApi
import de.crysxd.octoapp.engine.api.UserApi
import de.crysxd.octoapp.engine.api.VersionApi
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.client.HttpClient
import io.ktor.http.Url
import kotlinx.coroutines.flow.StateFlow

interface PrinterEngine {

    val eventSource: EventSource
    suspend fun notifyConnectionChange()
    suspend fun considerConnectionChange()
    suspend fun <T> genericRequest(block: suspend (Url, HttpClient) -> T): T = throw IllegalStateException("Calls for tests not supported")
    suspend fun probe(throwException: Boolean = false): Boolean
    fun destroy()

    val baseUrl: StateFlow<Url>

    val filesApi: FilesApi
    val jobApi: JobApi
    val loginApi: LoginApi
    val printerApi: PrinterApi
    val printerProfileApi: PrinterProfileApi
    val settingsApi: SettingsApi
    val systemApi: SystemApi
    val timelapseApi: TimelapseApi
    val userApi: UserApi
    val versionApi: VersionApi
    val powerDevicesApi: PowerDevicesApi
    val materialsApi: MaterialsApi
    val obicoApi: ObicoApi
    val octoEverywhereApi: OctoEverywhereApi
    val octoAppCompanionApi: OctoAppCompanionApi
}

fun PrinterEngine.asOctoPrint(): OctoPrintEngine = if (this is OctoPrintEngine) {
    this
} else {
    throw NetworkException(
        webUrl = baseUrl.value,
        userFacingMessage = "This feature is only supported with OctoPrint",
        technicalMessage = "${this::class.simpleName} is not a ${OctoPrintEngine::class.simpleName}"
    )
}

fun PrinterEngine.asMoonraker(): MoonrakerEngine = if (this is MoonrakerEngine) {
    this
} else {
    throw NetworkException(
        webUrl = baseUrl.value,
        userFacingMessage = "This feature is only supported with Moonraker",
        technicalMessage = "${this::class.simpleName} is not a ${MoonrakerEngine::class.simpleName}"
    )
}