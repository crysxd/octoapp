package de.crysxd.octoapp.engine.framework.json

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

typealias SafeInt = @Serializable(with = SafeIntSerializer::class) Int?

class SafeIntSerializer : KSerializer<Int?> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("OctoSafeInt", PrimitiveKind.INT)

    override fun deserialize(decoder: Decoder): Int? = try {
        val text = decoder.decodeString()
        when {
            text.toIntOrNull() != null -> text.toInt()
            text.toFloatOrNull() != null -> text.toFloat().toInt()
            else -> null
        }
    } catch (e: Exception) {
        null
    }

    @OptIn(ExperimentalSerializationApi::class)
    override fun serialize(encoder: Encoder, value: Int?) = value?.let { encoder.encodeInt(it) } ?: encoder.encodeNull()
}