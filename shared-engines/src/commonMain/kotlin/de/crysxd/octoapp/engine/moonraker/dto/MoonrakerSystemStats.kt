package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerSystemStats(
    @SerialName("cputime") val cputime: Float? = null,
    @SerialName("memavail") val memavail: Float? = null,
    @SerialName("sysload") val sysload: Float? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        cputime = cputime ?: (previous as? MoonrakerSystemStats)?.cputime,
        memavail = memavail ?: (previous as? MoonrakerSystemStats)?.memavail,
        sysload = sysload ?: (previous as? MoonrakerSystemStats)?.sysload,
    )
}