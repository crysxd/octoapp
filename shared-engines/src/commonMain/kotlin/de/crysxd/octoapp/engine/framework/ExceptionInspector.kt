package de.crysxd.octoapp.engine.framework

fun interface ExceptionInspector {
    fun inspect(e: Throwable)
}