package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerPrinterObjectSubscribeParams(
    @SerialName("objects") val objects: Map<String, List<String>?>? = null
) : MoonrakerRpcParams