package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.dto.SpoolmanSpool
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.material.Material
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.withContext
import de.crysxd.octoapp.engine.models.material.Material as IMaterial

interface MaterialsApi {
    companion object {
        val NoMaterialId = UniqueId(providerId = "all", id = "_____none")
    }

    suspend fun isMaterialManagerAvailable(settings: Settings): Boolean
    suspend fun getMaterials(settings: Settings): List<Material>
    suspend fun activateMaterial(uniqueMaterialId: UniqueId, extruderComponent: String)
}

class AggregatorMaterialsApi(
    private val aggregates: List<MaterialApiAggregate>
) : MaterialsApi {

    override suspend fun isMaterialManagerAvailable(settings: Settings) = aggregates
        .any { it.isMaterialManagerAvailable(settings) }

    override suspend fun getMaterials(settings: Settings) = with(CoroutineScope(currentCoroutineContext() + Dispatchers.SharedIO)) {
        aggregates
            .filter { it.isMaterialManagerAvailable(settings) }
            .map { async { it.getMaterials(settings) } }
            .flatMap { it.await() }
    }

    override suspend fun activateMaterial(uniqueMaterialId: UniqueId, extruderComponent: String) = aggregates
        .firstOrNull { it.providerId == uniqueMaterialId.providerId }
        ?.activateMaterial(uniqueMaterialId, extruderComponent)
        ?: throw IllegalStateException("No one is responsible for material: $uniqueMaterialId")


    interface MaterialApiAggregate : MaterialsApi {
        val providerId: String
    }
}


internal abstract class BaseMoonrakerSpoolmanApi : AggregatorMaterialsApi.MaterialApiAggregate {

    override val providerId = OctoPlugins.Spoolman

    abstract suspend fun loadSpools(): List<SpoolmanSpool>

    abstract suspend fun loadSelection(): Map<String, Int?>

    abstract suspend fun getToolIndex(extruderComponent: String): Int

    override suspend fun isMaterialManagerAvailable(settings: Settings) = settings.plugins.spoolman != null

    override suspend fun getMaterials(settings: Settings) = withContext(Dispatchers.SharedIO) {
        val spools = async { loadSpools() }
        val selection = async { loadSelection() }
        spools.await() to selection.await()
    }.let { (spools, selection) ->
        spools.filter {
            // Hide spools that
            // - Are archived
            // - Empty
            it.archived != true && (it.remainingWeight ?: 1f) > 0
        }.map { spool ->
            val activeExtruderComponent = selection.entries.firstOrNull { (_, spoolId) -> spoolId == spool.id }?.key
            Material(
                id = UniqueId(
                    id = spool.id.toString(),
                    providerId = providerId
                ),
                displayName = spool.filament?.name ?: spool.id.toString(),
                color = spool.filament?.colorHex,
                colorName = spool.filament?.colorHex?.toHexString(),
                vendor = spool.filament?.vendor?.name ?: "Unknown",
                material = spool.filament?.material ?: "Unknown",
                providerDisplayName = "Spoolman",
                activeExtruderComponent = activeExtruderComponent,
                activeToolIndex = activeExtruderComponent?.let { getToolIndex(it) },
                weightGrams = spool.remainingWeight,
                density = spool.filament?.density,
            )
        }
    }

    @CommonParcelize
    data class Material(
        override val id: UniqueId,
        override val displayName: String,
        override val vendor: String,
        override val material: String,
        override val color: HexColor?,
        override val density: Float?,
        override val colorName: String?,
        override val providerDisplayName: String,
        override val activeExtruderComponent: String?,
        override val activeToolIndex: Int?,
        override val weightGrams: Float?,
        override val attributes: List<String> = listOf(),
    ) : IMaterial {
        override fun copyWithAttributes(vararg attributes: String) = copy(attributes = attributes.toList())
    }
}