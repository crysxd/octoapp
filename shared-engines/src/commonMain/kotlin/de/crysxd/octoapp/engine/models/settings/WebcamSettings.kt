package de.crysxd.octoapp.engine.models.settings

import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Extras.Serializer.typeGeneric
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Extras.Serializer.typeWebRtc
import io.github.aakira.napier.Napier
import kotlinx.serialization.KSerializer
import kotlinx.serialization.PolymorphicSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonPrimitive

@Serializable
data class WebcamSettings(
    val webcams: List<Webcam> = emptyList(),
    val webcamEnabled: Boolean = true,
) {
    @Serializable
    data class Webcam(
        val name: String,
        val provider: String,
        val displayName: String = name,
        val streamUrl: String? = null,
        val flipH: Boolean = false,
        val flipV: Boolean = false,
        val rotate90: Boolean = false,
        val streamRatio: String = "16:9",
        val snapshotUrl: String? = null,
        val canSnapshot: Boolean = snapshotUrl != null,
        val snapshotDisplay: String? = null,
        val extras: Extras? = null,
        val type: Type? = null,
    ) {
        enum class Type {
            @SerialName("hls")
            Hls,

            @SerialName("mjpeg")
            Mjpeg,

            @SerialName("rtsp")
            Rtsp,

            @SerialName("jmuxer")
            JmuxerRaw,

            @SerialName("webrtcCameraStreamer")
            WebRtcCameraStreamer,

            @SerialName("webrtcJanus")
            WebRtcJanus,

            @SerialName("webrtcMediaMtx")
            WebRtcMediaMtx,

            @SerialName("webrtcGoRtc")
            WebRtcGoRtc,

            @SerialName("iframe")
            Iframe,

            @SerialName("ipcamera")
            IpCamera,
        }

        @Serializable(with = Extras.Serializer::class)
        sealed class Extras {
            @Serializable
            data class WebRtcExtras(
                val stunServer: String?,
                val turnServer: String?,
            ) : Extras() {
                val type get() = typeWebRtc
            }

            @Serializable
            data class GenericExtras(
                val jsonObject: JsonObject
            ) : Extras() {
                val type get() = typeGeneric
            }

            object Serializer : KSerializer<Extras> {
                private val json = Json { encodeDefaults = true }
                private val default = GenericExtras(JsonObject(emptyMap()))
                internal const val typeWebRtc = "webrtc"
                internal const val typeGeneric = "generic"


                override val descriptor = PolymorphicSerializer(Extras::class).descriptor

                override fun serialize(encoder: Encoder, value: Extras) = when (value) {
                    is GenericExtras -> GenericExtras.serializer().serialize(encoder, value)
                    is WebRtcExtras -> WebRtcExtras.serializer().serialize(encoder, value)
                }

                override fun deserialize(decoder: Decoder): Extras = try {
                    val (json, element) = (decoder as JsonDecoder).let { it.json to it.decodeJsonElement() }

                    when (element) {
                        is JsonObject -> when (element["type"]?.jsonPrimitive?.content) {
                            typeWebRtc -> json.decodeFromJsonElement<WebRtcExtras>(element)
                            typeGeneric -> json.decodeFromJsonElement<GenericExtras>(element)
                            else -> default
                        }

                        else -> default
                    }
                } catch (e: Exception) {
                    Napier.e(tag = "WebcamExtrasSerializer", message = "Failed to deserialize", throwable = e)
                    default
                }
            }
        }
    }
}