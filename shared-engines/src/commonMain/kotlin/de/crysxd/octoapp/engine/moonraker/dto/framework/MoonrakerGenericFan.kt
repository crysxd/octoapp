package de.crysxd.octoapp.engine.moonraker.dto.framework

interface MoonrakerGenericFan {
    val speed: Float?
    val rpm: Float?
}