package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerMcu(
    @SerialName("mcu") val mcu: Mcu? = null
) : MoonrakerStatusItem {
    @Serializable
    data class Mcu(
        @SerialName("mcu_build_versions") val mcuBuildVersions: String? = null,
        @SerialName("mcu_version") val mcuVersion: String? = null
    )

    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        mcu = Mcu(
            mcuBuildVersions = (previous as? MoonrakerMcu)?.mcu?.mcuBuildVersions,
            mcuVersion = (previous as? MoonrakerMcu)?.mcu?.mcuVersion,
        )
    )
}