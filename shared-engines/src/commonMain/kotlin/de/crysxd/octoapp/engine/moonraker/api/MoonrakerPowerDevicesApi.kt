package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerEventHandler
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerLed
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOutputPin
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerGetDeviceParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPostDeviceParams
import de.crysxd.octoapp.engine.moonraker.ext.convertToMoonrakerComponentLabel
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.isActive
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.floatOrNull
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import kotlin.time.Duration.Companion.seconds

private const val logTag = "MoonrakerApiPowerDevice"

internal class MoonrakerPowerDevicesApi(
    private val requestHandler: MoonrakerRequestHandler,
    private val eventHandler: MoonrakerEventHandler,
    private val printerApi: MoonrakerPrinterApi,
) : PowerDevicesApi {


    override suspend fun getDevices(
        settings: Settings,
        availablePlugins: Map<String, String?>?
    ): List<PowerDevice> = listOfNotNull(
        // Klipper devices
        settings.printerObjects.filterSupportedObjects().map {
            MoonrakerMacroPowerDevice(
                id = it,
                printerApi = printerApi,
                requestHandler = requestHandler,
                eventHandler = eventHandler,
            )
        },
        // Moonraker devices
        settings.plugins.moonrakerPowerDevices?.devices?.map { device ->
            MoonrakerApiPowerDevice(
                id = device.device,
                pluginId = listOfNotNull("moonraker", device.type).joinToString("/"),
                printerApi = printerApi,
                eventHandler = eventHandler,
                requestHandler = requestHandler,
            )
        }
    ).flatten()

    private fun List<String>.filterSupportedObjects() = filter {
        it.split(" ")[0] in listOf(
            Constants.PrinterObjects.Led,
            Constants.PrinterObjects.Neopixel,
            Constants.PrinterObjects.Dotstar,
            Constants.PrinterObjects.Pca9533,
            Constants.PrinterObjects.Pca9632,
            Constants.PrinterObjects.OutputPin,
        )
    }

    private data class MoonrakerApiPowerDevice(
        override val id: String,
        private val requestHandler: MoonrakerRequestHandler,
        private val eventHandler: MoonrakerEventHandler,
        private val printerApi: MoonrakerPrinterApi,
        override val pluginId: String,
    ) : PowerDevice {
        private val manualState = MutableStateFlow<Boolean?>(value = null)
        override val pluginDisplayName: String = "Moonraker"
        override val displayName: String = id.convertToMoonrakerComponentLabel()
        override val capabilities = listOf(PowerDevice.Capability.Illuminate)
        override val controlMethods = listOf(
            PowerDevice.ControlMethod.TurnOnOff,
            PowerDevice.ControlMethod.Toggle,
        )

        override suspend fun turnOn() = postState(on = true)

        override suspend fun turnOff() = postState(on = false)

        override suspend fun isOn() = requestHandler.sendRequest<JsonObject>(
            method = "machine.device_power.get_device",
            params = MoonrakerGetDeviceParams(
                device = id,
            )
        ).flushState()

        private suspend fun postState(on: Boolean) {
            requestHandler.sendRequest<JsonObject>(
                method = "machine.device_power.post_device",
                params = MoonrakerPostDeviceParams(
                    device = id,
                    action = if (on) MoonrakerPostDeviceParams.Action.ON else MoonrakerPostDeviceParams.Action.OFF
                )
            ).flushState()
        }

        private fun JsonObject.flushState(): Boolean? {
            val state = this[id]?.jsonPrimitive?.content?.let { it == "on" }
            manualState.value = state
            return state
        }

        @OptIn(ExperimentalCoroutinesApi::class)
        override fun isOnFlow() = flow {
            while (currentCoroutineContext().isActive) {
                emit(Unit)
                isOn()
                delay(1.seconds)
            }
        }.retry { e ->
            Napier.e(tag = logTag, message = "Failed to get power device state", throwable = e)
            delay(3.seconds)
            true
        }.flatMapLatest {
            manualState
        }.distinctUntilChanged().onEach {
            Napier.d(tag = logTag, message = "$id is on=$it")
        }.onStart {
            Napier.i(tag = logTag, message = "Start observing $id")
        }.onCompletion {
            Napier.i(tag = logTag, message = "Stop observing $id")
        }
    }

    private data class MoonrakerMacroPowerDevice(
        override val id: String,
        private val requestHandler: MoonrakerRequestHandler,
        private val eventHandler: MoonrakerEventHandler,
        private val printerApi: MoonrakerPrinterApi,
    ) : PowerDevice {
        override val pluginId: String = "klipper"
        override val pluginDisplayName: String = "Klipper"
        override val displayName: String = id.convertToMoonrakerComponentLabel()
        override val capabilities = listOf(PowerDevice.Capability.Illuminate)
        override val controlMethods = when (id.split(" ")[0]) {
            Constants.PrinterObjects.Led,
            Constants.PrinterObjects.Neopixel,
            Constants.PrinterObjects.Dotstar,
            Constants.PrinterObjects.Pca9533,
            Constants.PrinterObjects.Pca9632 -> listOf(
                PowerDevice.ControlMethod.TurnOnOff,
                PowerDevice.ControlMethod.Toggle,
                PowerDevice.ControlMethod.RgbwColor,
            )

            Constants.PrinterObjects.OutputPin -> listOf(
                PowerDevice.ControlMethod.TurnOnOff,
                PowerDevice.ControlMethod.Toggle,
                PowerDevice.ControlMethod.Pwm,
            )

            else -> listOf(
                PowerDevice.ControlMethod.TurnOnOff
            )
        }

        private fun createCommand(value: Float, r: Float = value, g: Float = value, b: Float = value): String {
            val configName = id.split(" ")[1]
            return when (PowerDevice.ControlMethod.RgbwColor in controlMethods) {
                false -> {
                    val configFile = eventHandler.status.value?.items?.get(Constants.PrinterObjects.ConfigFile) as? MoonrakerConfigFile
                    val componentConfig = configFile?.config?.get(id)?.jsonObject
                    val scale = componentConfig?.get("scale")?.jsonPrimitive?.floatOrNull ?: 1f
                    "SET_PIN PIN=$configName VALUE=${value * scale}"
                }
                true -> "SET_LED LED=$configName RED=$r GREEN=$g BLUE=$b WHITE=$value"
            }
        }

        override suspend fun turnOn() {
            printerApi.executeGcodeCommand(GcodeCommand.Single(createCommand(value = 1f)))
        }

        override suspend fun turnOff() {
            printerApi.executeGcodeCommand(GcodeCommand.Single(createCommand(value = 0f)))
        }

        override suspend fun setColor(color: PowerDevice.RgbwColor) {
            printerApi.executeGcodeCommand(GcodeCommand.Single(createCommand(value = color.w, r = color.r, g = color.g, b = color.b)))
        }

        override suspend fun setPwmDutyCycle(dutyCycle: Float) {
            printerApi.executeGcodeCommand(GcodeCommand.Single(createCommand(value = dutyCycle)))
        }

        override fun colorFlow(): Flow<List<PowerDevice.RgbwColor>?> = eventHandler.eventFlow("power-device-color-$id").map {
            when (val item = eventHandler.status.value?.items?.get(id)) {
                is MoonrakerLed -> item.colorData
                else -> null
            }?.mapNotNull { colorData ->
                PowerDevice.RgbwColor(
                    r = colorData?.getOrNull(0) ?: return@map null,
                    g = colorData.getOrNull(1) ?: return@map null,
                    b = colorData.getOrNull(2) ?: return@map null,
                    w = colorData.getOrNull(3) ?: return@map null,
                )
            }
        }.onStart {
            Napier.i(tag = "PowerDevice/$id", message = "Start observing color")
        }.onCompletion {
            Napier.i(tag = "PowerDevice/$id", message = "Stop observing color")
        }.distinctUntilChanged()

        override fun pwmDutyCycleFlow(): Flow<Float?> = eventHandler.eventFlow("power-device-duty-$id").map {
            when (val item = eventHandler.status.value?.items?.get(id)) {
                is MoonrakerOutputPin -> item.value
                else -> null
            }
        }.onStart {
            Napier.i(tag = "PowerDevice/$id", message = "Start observing duty cycle")
        }.onCompletion {
            Napier.i(tag = "PowerDevice/$id", message = "Stop observing duty cycle")
        }.distinctUntilChanged()

        override fun isOnFlow(): Flow<Boolean?> = eventHandler.eventFlow("power-device-on-$id").map {
            when (val item = eventHandler.status.value?.items?.get(id)) {
                is MoonrakerOutputPin -> item.value ?: 0f
                is MoonrakerLed -> item.colorData?.mapNotNull { stops -> stops?.map { it ?: 0f }?.sum() }?.sum() ?: 0f
                else -> 0f
            } > 0f
        }.onStart {
            Napier.i(tag = "PowerDevice/$id", message = "Start observing on")
        }.onCompletion {
            Napier.i(tag = "PowerDevice/$id", message = "Stop observing on")
        }.distinctUntilChanged()

        override suspend fun isOn() = isOnFlow().first()
    }
}

