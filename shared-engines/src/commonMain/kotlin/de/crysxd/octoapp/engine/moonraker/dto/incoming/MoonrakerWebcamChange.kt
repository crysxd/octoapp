package de.crysxd.octoapp.engine.moonraker.dto.incoming


import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerWebcamChange(
    @SerialName("jsonrpc") val jsonrpc: String? = null,
    @SerialName("method") val method: String? = null,
    @SerialName("params") val params: List<MoonrakerWebcamList>,
) : MoonrakerIncomingMessage