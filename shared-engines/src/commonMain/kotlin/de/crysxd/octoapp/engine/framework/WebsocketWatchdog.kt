package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.exceptions.WebsocketDeadException
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

internal class WebsocketWatchdog(
    private val logTag: () -> String,
    private val eventSink: EventSink,
    private val onWebsocketBroken: suspend (Throwable) -> Unit,
    private val reconnect: () -> Unit,
    private val connectTimeout: Duration,
    private val noMessageTimeout: Duration,
    private val countToConsiderBroken: Int = 2,
) {

    private var watchdogCount = 0
    private var reconnectCounter = 0
    private var reportDisconnectOnStart = false
    private var lastMessageReceivedAt = Instant.DISTANT_PAST
    private var firstMessageJob: Job? = null
    private var reportDisconnectOnStartJob: Job? = null

    fun notifyConnectionStarted(scope: CoroutineScope) = scope.launch {
        reconnectCounter++
        reportDisconnectOnStartJob?.cancel("Reconnected")
        firstMessageJob?.cancel("Reconnected")
        firstMessageJob = scope.launch { awaitFirstMessage() }

        if (reportDisconnectOnStart) {
            reportDisconnectOnStart = false
            reportDisconnectOnStartJob = launch {
                val delay = 5.seconds
                Napier.i(tag = logTag(), message = "Report disconnect at start, will report disconnect if not connected in $delay")
                delay(delay)
                Napier.i(tag = logTag(), message = "Report disconnect at start, $delay passed without connection")
                eventSink.emitEvent(Event.Disconnected(SuppressedIllegalStateException("No messages for $delay"), connectionAttemptCounter = reconnectCounter))
            }
            reportDisconnectOnStartJob?.invokeOnCompletion {
                Napier.i(tag = logTag(), message = "Report disconnect at start completed")
            }
        }
    }

    fun notifyConnectionEstablished(scope: CoroutineScope) = scope.launch {
        Napier.i(tag = logTag(), message = "Ensuring connection stays alive (noMessageTimeout=$noMessageTimeout)")
        lastMessageReceivedAt = Clock.System.now()
        while (isActive) {
            delay(1.seconds)

            // This will also trigger when the app comes from background to foreground in iOS. We do want to reset the connection in this case
            if (Clock.System.now() > lastMessageReceivedAt + noMessageTimeout) {
                Napier.d(tag = logTag(), message = "Missed message timeout, no messages for $noMessageTimeout -> reconnecting")
                reportDisconnectOnStart = true
                reconnect()
            }
        }
    }

    private suspend fun awaitFirstMessage() {
        val timeout = connectTimeout + 2.seconds
        Napier.i(tag = logTag(), message = "Watchdog started for $timeout")
        delay(timeout)
        watchdogCount++

        if (watchdogCount > countToConsiderBroken) {
            val message = "Websocket considered permanently after $watchdogCount watchdog violations"
            val exception = WebsocketDeadException(message)
            Napier.e(tag = logTag(), message = message, throwable = exception)
            Napier.w(tag = logTag(), message = "Watchdog triggered, considering connection permanently broken")
            onWebsocketBroken(exception)
        } else {
            Napier.w(tag = logTag(), message = "Watchdog triggered, resetting")
            throw WebsocketDeadException("Watchdog task not cancelled, reconnecting")
        }
    }


    fun notifyMessageReceived() {
        if (firstMessageJob?.isActive == true || reportDisconnectOnStartJob?.isActive == true) {
            Napier.w(tag = logTag(), message = "First message received, cancelling job")
            firstMessageJob?.cancel("First message received")
            reportDisconnectOnStartJob?.cancel("First message received")
            firstMessageJob = null
            reportDisconnectOnStartJob = null
            reconnectCounter = 0
        }

        lastMessageReceivedAt = Clock.System.now()
    }
}