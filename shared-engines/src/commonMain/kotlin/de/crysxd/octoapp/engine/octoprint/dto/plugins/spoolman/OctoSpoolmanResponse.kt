package de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolman


import de.crysxd.octoapp.engine.dto.SpoolmanSpool
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OctoSpoolmanResponse(
    @SerialName("data") val `data`: Data? = null
) {
    @Serializable
    data class Data(
        @SerialName("spools") val spools: List<SpoolmanSpool>? = null
    )
}