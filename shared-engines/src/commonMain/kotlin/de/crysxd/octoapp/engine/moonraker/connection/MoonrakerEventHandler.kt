package de.crysxd.octoapp.engine.moonraker.connection

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileMetadata
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMessageList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerServerInfo
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSubscriptionResult
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo.SystemInfo.ServiceState
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerTemperatureStore
import de.crysxd.octoapp.engine.moonraker.dto.backFillWith
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyStatus
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectSubscribeParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerFilesMetadataParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerGcodeStoreParams
import de.crysxd.octoapp.engine.moonraker.ext.description
import de.crysxd.octoapp.engine.moonraker.ext.octoapp
import de.crysxd.octoapp.engine.moonraker.ext.plus
import de.crysxd.octoapp.engine.moonraker.mappers.MoonrakerNotifyStatusMapperContext
import de.crysxd.octoapp.engine.moonraker.mappers.map
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.flow.updateAndGet
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.seconds

internal class MoonrakerEventHandler(
    private val connect: () -> Unit,
    private val disconnect: () -> Unit,
    private val logTag: String,
    private val connectionScope: () -> CoroutineScope,
    private val requestHandler: MoonrakerRequestHandler,
) : EventSource, EventSink {

    private val mutableEventSource = MutableSharedFlow<Event>(replay = 16, extraBufferCapacity = 16)
    private val mutableSubscriptions = MutableStateFlow<List<Pair<EventSource.Config, String>>>(emptyList())
    private val mutableConnectionInitializationSkipped = MutableStateFlow(false)
    private val scope = CoroutineScope(Dispatchers.Default)
    private var mutableStatus = MutableStateFlow<MoonrakerNotifyStatus?>(null)
    private val mutableCompanionServiceState = MutableStateFlow<Map<String, ServiceState>?>(null)
    private var activeFile: MoonrakerFileMetadata? = null
    private var activeFileJob: Pair<Job, String>? = null
    private var statusCounter = 0
    private var notifyStatusMapperContext = MoonrakerNotifyStatusMapperContext()
    private val statusAlterations = MutableStateFlow<List<(Message.Current) -> Message.Current>>(emptyList())
    private val mutableActive = MutableStateFlow(false)
    internal val isLightweight get() = mutableSubscriptions.value.isLightweight
    val active = mutableActive.asStateFlow()
    val connectionNeeded get() = mutableSubscriptions.value.isNotEmpty()
    var status = mutableStatus.asStateFlow()
    var companionServiceState = mutableCompanionServiceState.asStateFlow()

    init {
        observeSubscriptionCount()
    }

    private fun observeSubscriptionCount() {
        scope.launch {
            mutableSubscriptions.onEach { configs ->
                mutableActive.value = configs.isNotEmpty()
            }.distinctUntilChanged { old, new ->
                val oldLightweight = old.isLightweight
                val newLightweight = new.isLightweight
                Napier.i(
                    tag = logTag,
                    message = "Flow count changed: ${old.map { it.second }} (lightweight=$oldLightweight)-> ${new.map { it.second }} (lightweight=$newLightweight) active flows"
                )

                when {
                    old.isNotEmpty() && new.isEmpty() -> {
                        Napier.v(tag = logTag, message = "No flow active, disconnecting")
                        disconnect()
                    }

                    old.isEmpty() && new.isNotEmpty() || oldLightweight != newLightweight -> {
                        connect()
                    }
                }

                false
            }.collect()
        }
    }

    fun destroy() {
        scope.cancel("MoonrakerEventHandler destroyed")
    }

    suspend fun handleSettingsChanged() {
        mutableEventSource.emit(Event.MessageReceived(Message.Event.SettingsUpdated(null)))
    }

    suspend fun handleStatusUpdate(status: MoonrakerNotifyStatus) {
        // If we receive some updates we trigger a settings refresh
        val settingsRefreshNeeded = listOf(
            Constants.PrinterObjects.BedMesh
        ).any { key ->
            status.items.contains(key)
        }

        // Merge the new status with the previous as not all status updates have all items
        // If we don't have previous skip, subscription is not completed
        val newStatus = mutableStatus.updateAndGet { previous ->
            previous ?: return@updateAndGet null
            status.copy(
                items = status.items.backFillWith(previous.items),
                eventTime = Clock.System.now()
            )
        }

        if (newStatus != null) {
            newStatus.emitNow()
            ensureFileAvailable(newStatus)
        } else {
            Napier.d(tag = logTag, message = "Skipping received status update, no previous to backfill")
        }

        if (settingsRefreshNeeded) {
            emitEvent(Event.MessageReceived(Message.Event.SettingsUpdated(configHash = uuid4().toString())))
        }

        if (statusCounter++ % 100 == 0) {
            Napier.d(tag = logTag, message = "Status ${statusCounter - 1}: ${mutableStatus.value}")
        }
    }

    private suspend fun MoonrakerNotifyStatus.emitNow(
        gcodeStore: List<String> = emptyList(),
        file: MoonrakerFileMetadata? = activeFile,
    ) {
        var current = items.map(
            eventTime = eventTime,
            activeFile = file,
            gcodeStore = gcodeStore,
            context = notifyStatusMapperContext,
        )

        statusAlterations.value.forEach {
            current = it(current)
        }

        emitEvent(Event.MessageReceived(current))
    }

    suspend fun alterStatusWhile(alter: (Message.Current) -> Message.Current, action: suspend () -> Unit) = try {
        statusAlterations.update { it + alter }
        mutableStatus.value?.emitNow()
        action()
    } finally {
        statusAlterations.update { it - alter }
        mutableStatus.value?.emitNow()
    }

    suspend fun handleGcodeResponses(responses: List<String>) {
        status.value?.emitNow(gcodeStore = responses)
    }

    fun resetConnection() {
        mutableStatus.value = MoonrakerNotifyStatus()
        mutableCompanionServiceState.value = null
        mutableConnectionInitializationSkipped.value = false
        activeFile = null
        statusCounter = 0
        activeFileJob?.first?.cancel()
        activeFileJob = null
    }

    suspend fun initializeConnection(lightweight: Boolean) {
        if (mutableSubscriptions.value.isEmpty()) {
            Napier.i(tag = logTag, message = "Skipping event initialization, no active flows")
            mutableConnectionInitializationSkipped.value = true
            return
        }

        // Request temperature history (async)
        // Request server info (async)
        val temperatureStoreDeferred = scope.async {
            if (isLightweight) null else requestHandler.sendRequest<MoonrakerTemperatureStore>("server.temperature_store")
        }
        val gcodeStoreDeferred = scope.async {
            if (isLightweight) null else requestHandler.sendRequest<MoonrakerGcodeMessageList>("server.gcode_store", MoonrakerServerGcodeStoreParams())
        }
        val serverInfoDeferred = scope.async { requestHandler.sendRequest<MoonrakerServerInfo>("server.info") }
        val systemInfoDeferred = scope.async { requestHandler.sendRequest<MoonrakerSystemInfo>("machine.system_info") }

        // Request printer objects
        Napier.i(tag = logTag, message = "Performing event initialization (lightweight=$lightweight)")
        Napier.d(tag = logTag, message = "Requesting printer objects")
        val printerObjects = requestHandler
            .sendRequest<MoonrakerPrinterObjectList>("printer.objects.list", null)
            .filterSupportedObjects(lightweight)

        // Subscribe to objects we need
        Napier.d(tag = logTag, message = if (lightweight) "Querying once: $printerObjects" else "Subscribing to: $printerObjects")
        val subscription = MoonrakerPrinterObjectSubscribeParams(objects = printerObjects)
        val subscriptionResult = requestHandler.sendRequest<MoonrakerSubscriptionResult>(
            method = if (lightweight) "printer.objects.query" else "printer.objects.subscribe",
            params = subscription
        )

        // Emit connected
        val version = serverInfoDeferred.await().moonrakerVersion
        emitEvent(
            Event.MessageReceived(
                message = Message.Connected(
                    version = version,
                    displayVersion = version,
                    configHash = uuid4().toString(),
                )
            )
        )
        // Emit firmware
        emitEvent(
            Event.MessageReceived(
                message = Message.Event.FirmwareData(
                    extruderCount = printerObjects.keys.count { it.startsWith("extruder") },
                    firmwareName = "klipper",
                    machineType = "klipper",
                )
            )
        )
        // Emit "history message"
        emitEvent(
            Event.MessageReceived(
                message = subscriptionResult.items.map(
                    eventTime = subscriptionResult.eventTime,
                    historyMessage = true,
                    temperatureStore = temperatureStoreDeferred.await(),
                    gcodeStore = gcodeStoreDeferred.await()?.messages?.mapNotNull { it.message } ?: emptyList(),
                    context = notifyStatusMapperContext,
                )
            )
        )


        // Save previous items, from now on updates will be emitted
        val initial = MoonrakerNotifyStatus(items = subscriptionResult.items, eventTime = subscriptionResult.eventTime, jsonrpc = null, method = null)
        mutableStatus.value = initial
        mutableCompanionServiceState.value = systemInfoDeferred.await().systemInfo?.serviceState
        Napier.d(tag = logTag, message = "Subscribing info received, items initialized, making sure file is available")
        Napier.d(tag = logTag, message = "Companion service state: ${mutableCompanionServiceState.value.description}")

        if (!lightweight) {
            ensureFileAvailable(status = initial)
        }
    }

    private fun MoonrakerPrinterObjectList.filterSupportedObjects(lightWeight: Boolean): Map<String, List<String>?> {
        val supported = mutableMapOf<String, List<String>?>(
            Constants.PrinterObjects.PrintStats to null,
            Constants.PrinterObjects.VirtualSdCard to null,
            Constants.PrinterObjects.HeaterBed to null,
            Constants.PrinterObjects.BedMesh to null,
            Constants.PrinterObjects.Fan to null,
            Constants.PrinterObjects.HeaterGenericKind to null,
            Constants.PrinterObjects.FanGenericKind to null,
            Constants.PrinterObjects.TemperatureSensorKind to null,
            Constants.PrinterObjects.TemperatureHost to null,
            Constants.PrinterObjects.Webhooks to null,
            Constants.PrinterObjects.MotionReport to null,
            Constants.PrinterObjects.DisplayStatus to null,
            Constants.PrinterObjects.Toolhead to null,
            Constants.PrinterObjects.Extruder to null,
            Constants.PrinterObjects.GcodeMove to null,
            Constants.PrinterObjects.Led to null,
            Constants.PrinterObjects.Neopixel to null,
            Constants.PrinterObjects.Dotstar to null,
            Constants.PrinterObjects.Pca9533 to null,
            Constants.PrinterObjects.Pca9632 to null,
            Constants.PrinterObjects.OutputPin to null,
            Constants.PrinterObjects.ConfigFile to null,
            Constants.PrinterObjects.StepperEnable to null,
            Constants.PrinterObjects.ExcludeObject to null,
        ).also {
            if (lightWeight) {
                it.remove(Constants.PrinterObjects.MotionReport)
                it.remove(Constants.PrinterObjects.ConfigFile)
            }
        }

        return objects?.filter {
            it.split(" ").first() in supported.keys
        }?.associate {
            it to supported[it]
        } ?: emptyMap()
    }

    private fun ensureFileAvailable(status: MoonrakerNotifyStatus?) {
        val filename = status?.items?.firstNotNullOfOrNull { (_, value) -> value as? MoonrakerPrintStats }?.filename
        if (filename.isNullOrBlank()) {
            activeFile = null
        } else if (activeFile?.filename != filename && (activeFileJob?.second != filename || activeFileJob?.first?.isActive != true)) {
            // The active file loaded is not what we need, the active file job is done or is not for the correct file
            activeFileJob?.first?.cancel()
            activeFileJob = connectionScope().launch {
                Napier.d(tag = logTag, message = "Active file not available, loading='$filename' available='${activeFile?.filename}'")
                activeFile = try {
                    requestHandler.sendRequest("server.files.metadata", MoonrakerServerFilesMetadataParams(filename = filename))
                } catch (e: Exception) {
                    Napier.e(tag = logTag, message = "Unable to load active file, retrying after delay (filename=$filename)", throwable = e)
                    delay(5.seconds)
                    null
                }
                mutableStatus.value?.emitNow(file = activeFile)
                Napier.d(tag = logTag, message = "Active file loaded, updated last state")
            } to filename
        }
    }


    override suspend fun injectInterpolatedEvent(event: (Message.Current?) -> Message.Event) = Unit

    override suspend fun injectInterpolatedPrintStart(file: FileReference.File) = Unit

    override suspend fun injectInterpolatedTemperatureTarget(targets: Map<String, Float>) = Unit

    override suspend fun emitEvent(e: Event) {
        mutableEventSource.emit(e)
    }

    override fun eventFlow(tag: String, config: EventSource.Config) = mutableEventSource
        .onStart {
            mutableSubscriptions.update { it + (config to tag) }
            Napier.d(tag = logTag, message = "onStart for Flow (tag=$tag, eventSource=${this@MoonrakerEventHandler.toString().split("@")[1]}, filters=$config)")
        }
        .onCompletion {
            mutableSubscriptions.update { it - (config to tag) }
            Napier.d(tag = logTag, message = "onCompletion for Flow (tag=$tag, eventSource=${this@MoonrakerEventHandler.toString().split("@")[1]})")
        }

    override fun passiveEventFlow(): Flow<Event> = mutableEventSource.asSharedFlow()

    suspend fun handleServiceStateChange(serviceState: Map<String, ServiceState>) {
        val oldState = mutableCompanionServiceState.value
        val newState = oldState + serviceState
        val wasActive = oldState?.octoapp?.state?.activeState == ServiceState.ActiveState.Active
        val isActive = newState.octoapp?.state?.activeState == ServiceState.ActiveState.Active

        if (wasActive != isActive) {
            Napier.i(tag = logTag, message = "OctoApp service state changed to ${newState.octoapp?.state}")
            emitEvent(Event.MessageReceived(message = Message.Event.SettingsUpdated(configHash = uuid4().toString())))
        }

        mutableCompanionServiceState.value = serviceState
        Napier.i(tag = logTag, message = "Service states are now ${serviceState.description}")
    }

    private val List<Pair<EventSource.Config, String>>.isLightweight get() = all { it.first.throttle > 1 }
}