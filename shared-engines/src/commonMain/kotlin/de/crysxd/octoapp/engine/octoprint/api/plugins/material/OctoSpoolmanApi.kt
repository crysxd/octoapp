package de.crysxd.octoapp.engine.octoprint.api.plugins.material

import de.crysxd.octoapp.engine.api.BaseMoonrakerSpoolmanApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolman.OctoSpoolmanResponse
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolman.OctoSpoolmanSelectionRequest
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post

internal class OctoSpoolmanApi(
    val baseUrlRotator: BaseUrlRotator,
    val httpClient: HttpClient
) : BaseMoonrakerSpoolmanApi() {

    override suspend fun loadSelection() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "settings")
        }.body<OctoSettings>().plugins.spoolman?.selectedSpoolIds?.map { (key, value) ->
            "tool$key" to value.spoolId?.toIntOrNull()
        }?.toMap() ?: emptyMap()
    }

    override suspend fun loadSpools() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "plugin", OctoPlugins.Spoolman, "spoolman", "spools")
        }.body<OctoSpoolmanResponse>().data?.spools ?: emptyList()
    }

    override suspend fun getToolIndex(extruderComponent: String) = extruderComponent.removePrefix("tool").toIntOrNull()
        ?: throw IllegalArgumentException("Expected extruderComponent in format 'tool[0-9]'")

    override suspend fun activateMaterial(uniqueMaterialId: UniqueId, extruderComponent: String) = baseUrlRotator.request<Unit> {
        //region The request
        suspend fun request(extruderComponent: String, spoolId: Int?) = httpClient.post {
            urlFromPath(baseUrl = it, "plugin", OctoPlugins.Spoolman, "self", "spool")
            setJsonBody(
                OctoSpoolmanSelectionRequest(
                    spoolId = spoolId,
                    toolIdx = getToolIndex(extruderComponent),
                )
            )
        }
        //endregion
        //region De-select from old tool
        getMaterials(Settings()).firstOrNull { it.id == uniqueMaterialId }?.activeExtruderComponent?.let { currentActive ->
            request(extruderComponent = currentActive, spoolId = null)
        }
        //endregion
        //region Select on new tool
        request(extruderComponent = extruderComponent, spoolId = uniqueMaterialId.id.toIntOrNull())
        //endregion
    }
}