package de.crysxd.octoapp.engine.moonraker.http

import de.crysxd.octoapp.engine.framework.ExceptionInspector
import de.crysxd.octoapp.engine.framework.installApiKeyInterceptor
import de.crysxd.octoapp.engine.framework.installNgrokHeaderInterceptor
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngineBuilder
import de.crysxd.octoapp.engine.octoprint.CommandBody
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.http.HttpExceptionGenerator
import de.crysxd.octoapp.sharedcommon.http.framework.installBasicAuthInterceptorPlugin
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.plugin
import io.ktor.client.plugins.websocket.WebSockets
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json

private val generator = HttpExceptionGenerator()

internal fun createMoonrakerHttpClient(
    engine: HttpClientEngine? = null,
    settings: MoonrakerEngineBuilder.HttpClientSettings,
): HttpClient {
    val sharedConfig: HttpClientConfig<*>.() -> Unit = {
        expectSuccess = false
        followRedirects = true

        installJsonSerialization()
        installWebSockets()
    }

    return DefaultHttpClient(settings.general, engine, sharedConfig).apply {
        installExceptionInspector(settings.exceptionInspector)
        installBasicAuthInterceptorPlugin()
        installApiKeyInterceptor(settings.apiKey)
        installNgrokHeaderInterceptor()
        installMoonrakerGenerateExceptionInterceptorPlugin()
    }
}

internal fun HttpClient.installMoonrakerGenerateExceptionInterceptorPlugin() = plugin(HttpSend).intercept { request ->
    generator.generateExceptionsForCall(
        request = request,
        sender = this,
    )
}

inline fun <reified T> HttpRequestBuilder.setJsonBody(body: T) {
    setBody(body)
    contentType(ContentType.Application.Json)

    if (body is CommandBody) {
        body.log()
    }
}

internal fun HttpClient.installExceptionInspector(inspector: ExceptionInspector) {
    plugin(HttpSend).intercept { request ->
        try {
            execute(request)
        } catch (e: Exception) {
            inspector.inspect(e)
            throw e
        }
    }
}

internal fun HttpClientConfig<*>.installWebSockets() {
    install(WebSockets)
}

internal fun HttpClientConfig<*>.installJsonSerialization() {
    install(ContentNegotiation) {
        json(EngineJson)
    }
}
