package de.crysxd.octoapp.engine.moonraker.serializer

//internal class MoonrakerDatabaseItemSerializer : KSerializer<MoonrakerDatabaseItem<*>> {
//
//    private val tag = "MoonrakerDatabaseItemSerializer"
//    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerDatabaseItem", kind = PrimitiveKind.STRING)
//
//    override fun deserialize(decoder: Decoder): MoonrakerDatabaseItem<*> {
//        val input = decoder as JsonDecoder
//        val tree = input.decodeJsonElement().jsonObject
//        val key = tree["key"]?.jsonPrimitive?.content
//        val namespace = tree["namespace"]?.jsonPrimitive?.content
//        val value = tree["value"] ?: return MoonrakerDatabaseItem(namespace = namespace, key = key, value = null)
//
//        when (namespace) {
//            null -> MoonrakerDatabaseItem(namespace = namespace, key = key, value = null)
//            "mainsail" -> MoonrakerDatabaseItem(
//                namespace = namespace,
//                key = key,
//                value = EngineJson.decodeFromJsonElement(MoonrakerMainsailDatabase.serializer(), value)
//            )
//        }
//
//        return MoonrakerNotifyStatus.Items(items)
//    }
//
//    override fun serialize(encoder: Encoder, value: MoonrakerNotifyStatus.Items) = throw UnsupportedOperationException("Can't serialize")
//}