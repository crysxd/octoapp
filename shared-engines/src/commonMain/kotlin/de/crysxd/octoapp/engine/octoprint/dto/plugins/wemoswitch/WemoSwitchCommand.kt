package de.crysxd.octoapp.engine.octoprint.dto.plugins.wemoswitch

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class WemoSwitchCommand(
    override val command: String,
    val ip: String
) : CommandBody

