package de.crysxd.octoapp.engine.framework.json

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

typealias SafeBoolean = @Serializable(with = SafeBooleanSerializer::class) Boolean?

internal class SafeBooleanSerializer : KSerializer<Boolean> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("SafeBooleanSerializer", PrimitiveKind.BOOLEAN)

    override fun serialize(encoder: Encoder, value: Boolean) = encoder.encodeBoolean(value)

    override fun deserialize(decoder: Decoder): Boolean {
        val raw = decoder.decodeString().lowercase()
        return when {
            raw.toBooleanStrictOrNull() != null -> raw.toBoolean()
            raw.toIntOrNull() != null -> raw.toInt() == 1
            raw.toFloatOrNull() != null -> raw.toFloat() == 1f
            else -> false
        }
    }
}