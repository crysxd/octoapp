package de.crysxd.octoapp.engine.moonraker.dto.framework

interface MoonrakerGenericHeater : MoonrakerGenericTemperatureSensor {
    val target: Float?
    val power: Float?
}