package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.engine.framework.Constants.ApiKeyHeader
import de.crysxd.octoapp.engine.framework.Constants.SuppressApiKey
import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.client.request.headers

internal fun HttpClient.installApiKeyInterceptor(apiKey: String?) = plugin(HttpSend).intercept { request ->

    if (!request.attributes.contains(SuppressApiKey) && !apiKey.isNullOrBlank()) {
        request.headers {
            append(ApiKeyHeader, apiKey)
        }
    }

    execute(request)
}