package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.octoprint.dto.plugins.companion.AppRegistration

interface OctoAppCompanionApi {
    suspend fun registerApp(registration: AppRegistration)
}