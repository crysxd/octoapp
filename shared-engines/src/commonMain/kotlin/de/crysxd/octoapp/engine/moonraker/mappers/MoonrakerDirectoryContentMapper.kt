package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDirectoryContent

internal fun MoonrakerDirectoryContent.map(path: String, origin: FileOrigin): FileObject.Folder = FileObject.Folder(
    name = path.split("/").last(),
    display = path.split("/").last(),
    path = path,
    origin = FileOrigin.Gcode,
    size = files?.sumOf { it.size ?: 0L },
    children = listOfNotNull(
        dirs?.filter { dir ->
            dir.dirname?.startsWith(".") == false
        }?.mapNotNull { dir ->
            FileObject.Folder(
                name = dir.dirname ?: return@mapNotNull null,
                origin = origin,
                path = "${path.removeSuffix("/")}/${dir.dirname}",
                size = dir.size,
            )
        },
        files?.map { child -> child.map(path = if (path == "/") "${child.filename}" else "${path.removeSuffix("/")}/${child.filename}", origin) },
    ).flatten()
)