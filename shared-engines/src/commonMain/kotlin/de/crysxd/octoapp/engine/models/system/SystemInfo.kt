package de.crysxd.octoapp.engine.models.system

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class SystemInfo(
    val generatedAt: Instant = Clock.System.now(),
    val printerFirmware: String? = null,
    val printerFirmwareFamily: FirmwareFamily = FirmwareFamily.Generic,
    val safeMode: Boolean = false,
    val interfaceType: Interface = Interface.OctoPrint,
    val capabilities: List<Capability> = emptyList()
) {
    enum class Capability {
        Timelapse, PrettyTerminal, Plugins, Macro, AutoNgrok, TemperatureOffset, ResetZOffset, RealTimeStats, AbsoluteToolheadMove
    }

    enum class Interface {
        OctoPrint, MoonrakerFluidd, MoonrakerMainsail, MoonrakerMixed;
    }

    enum class FirmwareFamily {
        Klipper, Generic;
    }
}

val SystemInfo.Interface?.label
    get() = when (this) {
        SystemInfo.Interface.OctoPrint -> "OctoPrint"
        SystemInfo.Interface.MoonrakerFluidd -> "Fluidd"
        SystemInfo.Interface.MoonrakerMainsail -> "Mainsail"
        SystemInfo.Interface.MoonrakerMixed -> "Moonraker"
        null -> "Server"
    }

val SystemInfo.Interface?.isMoonraker
    get() = when (this) {
        SystemInfo.Interface.MoonrakerFluidd,
        SystemInfo.Interface.MoonrakerMainsail,
        SystemInfo.Interface.MoonrakerMixed -> true

        else -> false
    }

val SystemInfo.Interface?.isOctoPrint
    get() = when (this) {
        SystemInfo.Interface.OctoPrint -> true
        else -> false
    }