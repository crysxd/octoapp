package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.job.Job
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJob
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJobInformation
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoProgressInformation
import kotlinx.datetime.Instant

internal fun OctoJob.map() = Job(
    info = job.map(),
    progress = progress.map(),
)

internal fun OctoProgressInformation.map() = ProgressInformation(
    completion = if (printTimeLeftOrigin == ProgressInformation.ORIGIN_GENIUS && printTime != null && printTimeLeft != null) {
        // If the time estimation comes from PrintTimeGenius, the webinterface shows the progress based of time. We need to adapt here to have the same value.
        (printTime / (printTime + printTimeLeft.toFloat())) * 100f
    } else {
        completion
    },
    printTime = printTime,
    printTimeLeft = printTimeLeft,
    filepos = filepos,
    printTimeLeftOrigin = printTimeLeftOrigin,
)

internal fun OctoJobInformation.map() = JobInformation(
    file = file?.map()
)

internal fun OctoJobInformation.File.map() = if (name != null && origin != null && path != null) {
    JobInformation.JobFile(
        name = name,
        origin = origin.map(),
        size = size ?: -1,
        path = path,
        display = display ?: name,
        date = date ?: Instant.DISTANT_PAST,
    )
} else {
    null
}
