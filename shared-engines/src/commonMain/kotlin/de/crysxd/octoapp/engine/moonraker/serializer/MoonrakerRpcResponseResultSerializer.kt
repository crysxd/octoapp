package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileReference
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileReferenceList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMessageList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerTemperatureStore
import de.crysxd.octoapp.engine.moonraker.dto.wrap
import io.github.aakira.napier.Napier
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.serializer
import kotlin.reflect.KClass

class MoonrakerRpcResponseResultSerializer<Result : Any>(private val type: KClass<Result>) : DeserializationStrategy<Result> {

    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerRpcRequest.Params", kind = PrimitiveKind.STRING)

    @OptIn(InternalSerializationApi::class)
    override fun deserialize(decoder: Decoder): Result {
        var didLog = true
        return try {
            val input = decoder as JsonDecoder
            val tree = input.decodeJsonElement().jsonObject
            val result = tree.jsonObject["result"] ?: throw IllegalStateException("Missing field 'result'")

            // For some reason MoonrakerTemperatureStoreSerializer and MoonrakerGcodeMessageListSerializer isn't picked up on Kotlin/Native
            // List<MoonrakerFileReference> struggles a bit, let's just simply help out here
            try {
                @Suppress("UNCHECKED_CAST")
                when (type) {
                    MoonrakerTemperatureStore::class -> EngineJson.decodeFromJsonElement(MoonrakerTemperatureStoreSerializer(), result) as Result
                    MoonrakerGcodeMessageList::class -> EngineJson.decodeFromJsonElement(MoonrakerGcodeMessageListSerializer(), result) as Result
                    MoonrakerFileReferenceList::class -> EngineJson.decodeFromJsonElement(ListSerializer(MoonrakerFileReference.serializer()), result).wrap() as Result
                    else -> EngineJson.decodeFromJsonElement(type.serializer(), result)
                }
            } catch (e: Exception) {
                Napier.e(tag = "MoonrakerRpcResponseResultSerializer", message = "Failed to deserialize: $type from $result", throwable = e)
                didLog = true
                throw e
            }
        } catch (e: Exception) {
            if (!didLog) {
                Napier.e(tag = "MoonrakerRpcResponseResultSerializer", message = "Failed to deserialize: $type", throwable = e)
            }
            throw e
        }
    }
}