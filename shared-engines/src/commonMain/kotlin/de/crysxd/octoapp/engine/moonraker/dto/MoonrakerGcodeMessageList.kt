package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerGcodeMessageListSerializer
import kotlinx.serialization.Serializable

@Serializable(with = MoonrakerGcodeMessageListSerializer::class)
data class MoonrakerGcodeMessageList(
    val messages: List<MoonrakerGcodeMessage>
)