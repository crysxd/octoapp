package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.wemoswitch.WemoSwitchCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.wemoswitch.WemoSwitchResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class WemoSwitchApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = settings.plugins.wemoSwitch?.devices?.map {
        PowerDevice(
            owner = this,
            displayName = it.label,
            ip = it.ip,
        )
    } ?: emptyList()

    private suspend fun sendCommand(ip: String, command: String) = baseUrlRotator.request {
        httpClient.post {
            url(it)
            setJsonBody(WemoSwitchCommand(ip = ip, command = command))
        }
    }

    private suspend fun setOn(ip: String, on: Boolean) {
        sendCommand(ip = ip, command = if (on) "turnOn" else "turnOff")
    }

    private suspend fun isOn(ip: String) =
        sendCommand(ip = ip, command = "checkStatus").body<WemoSwitchResponse>().currentState == "on"

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.WemoSwitch)

    internal data class PowerDevice(
        private val owner: WemoSwitchApi,
        override val displayName: String,
        private val ip: String,
    ) : IPowerDevice {
        override val id: String = ip
        override val pluginId: String = OctoPlugins.WemoSwitch
        override val pluginDisplayName = "Wemo"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(ip = ip, on = true)
        override suspend fun turnOff() = owner.setOn(ip = ip, on = false)
        override suspend fun isOn() = owner.isOn(ip = ip)
    }
}