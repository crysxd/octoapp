package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.enclosure.EnclosureOutputCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.enclosure.EnclosureOutputResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.patch
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class EnclosureApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = settings.plugins.enclosure?.outputs?.filter {
        // We only support regular IO, no PWM etc
        it.type == "regular"
    }?.map {
        PowerDevice(
            owner = this,
            displayName = it.label,
            indexId = it.indexId,
        )
    } ?: emptyList()

    private suspend fun setOn(indexId: Int, on: Boolean) = baseUrlRotator.request<Unit> {
        httpClient.patch {
            url(it, indexId)
            setJsonBody(EnclosureOutputCommand(on))
        }
    }

    private suspend fun isOn(indexId: Int) = baseUrlRotator.request {
        httpClient.get {
            url(it, indexId)
        }.body<EnclosureOutputResponse>().currentValue
    }

    private fun HttpRequestBuilder.url(baseUrl: Url, indexId: Int) = urlFromPath(baseUrl, "plugin", OctoPlugins.Enclosure, "outputs", indexId.toString())

    internal data class PowerDevice(
        private val owner: EnclosureApi,
        override val displayName: String,
        private val indexId: Int,
    ) : IPowerDevice {
        override val id: String = "index-$indexId"
        override val pluginId: String = OctoPlugins.Enclosure
        override val pluginDisplayName = "Enclosure Plugin"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(indexId, true)
        override suspend fun turnOff() = owner.setOn(indexId, false)
        override suspend fun isOn() = owner.isOn(indexId)
    }
}