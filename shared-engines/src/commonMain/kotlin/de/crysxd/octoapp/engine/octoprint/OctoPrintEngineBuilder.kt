package de.crysxd.octoapp.engine.octoprint

import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.PrinterEngineBuilder
import de.crysxd.octoapp.engine.PrinterEngineHttpClientSettings
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.BenchmarkBaseUrlRotator
import de.crysxd.octoapp.engine.framework.ExceptionInspector
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.http.Url
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings as GenericHttpClientSettings

fun OctoPrintEngineBuilder(block: OctoPrintEngineBuilder.Scope.() -> Unit): PrinterEngine {
    val scope = object : OctoPrintEngineBuilder.Scope {
        override var baseUrls: List<String> = emptyList()
        override var apiKey: String? = null
        override var interpolateEvents: Boolean = false
        override var exceptionInspector: ExceptionInspector = ExceptionInspector {}
        override var httpClientSettings: GenericHttpClientSettings? = null
        override var allowWebSocketTransport: Boolean = true
    }

    scope.block()

    require(scope.baseUrls.isNotEmpty()) { "At least one URL required" }
    val apiKey = requireNotNull(scope.apiKey) { "An API key is required" }
    val settings = requireNotNull(scope.httpClientSettings) { "Missing HttpClientSettings" }

    val bur = BenchmarkBaseUrlRotator(baseUrls = scope.baseUrls.map { Url(it) })
    return OctoPrintEngine(
        baseUrlRotator = bur,
        interpolateEvents = scope.interpolateEvents,
        allowWebSocketTransport = scope.allowWebSocketTransport,
        httpClient = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.HttpClientSettings(
                general = settings,
                apiKey = apiKey,
                exceptionInspector = scope.exceptionInspector,
                baseUrlRotator = bur,
            )
        )
    )
}

class OctoPrintEngineBuilder : PrinterEngineBuilder {
    interface Scope : PrinterEngineBuilder.Scope {
        override var baseUrls: List<String>
        override var interpolateEvents: Boolean
        override var exceptionInspector: ExceptionInspector
        override var httpClientSettings: GenericHttpClientSettings?
        override var allowWebSocketTransport: Boolean
        override var apiKey: String?
    }

    internal data class HttpClientSettings(
        override val general: GenericHttpClientSettings = GenericHttpClientSettings(logTag = OctoConstants.OCTOPRINT_TAG),
        override val baseUrlRotator: BaseUrlRotator,
        override val apiKey: String,
        override val exceptionInspector: ExceptionInspector = ExceptionInspector {},
    ) : PrinterEngineHttpClientSettings
}




