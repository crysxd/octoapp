package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

open class PrinterNotOperationalException(webUrl: Url) : NetworkException(
    webUrl = webUrl,
    technicalMessage = "Printer was not operational when accessing $webUrl",
    userFacingMessage = "The printer is not ready to execute this task"
)