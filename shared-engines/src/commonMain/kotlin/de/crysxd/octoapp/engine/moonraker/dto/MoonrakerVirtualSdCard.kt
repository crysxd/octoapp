package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerVirtualSdCard(
    @SerialName("file_path") val filePath: String? = null,
    @SerialName("file_position") val filePosition: Double? = null,
    @SerialName("file_size") val fileSize: Double? = null,
    @SerialName("is_active") val isActive: Boolean? = null,
    @SerialName("progress") val progress: Double? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        filePath = filePath ?: (previous as? MoonrakerVirtualSdCard)?.filePath,
        filePosition = filePosition ?: (previous as? MoonrakerVirtualSdCard)?.filePosition,
        fileSize = fileSize ?: (previous as? MoonrakerVirtualSdCard)?.fileSize,
        isActive = isActive ?: (previous as? MoonrakerVirtualSdCard)?.isActive,
        progress = progress ?: (previous as? MoonrakerVirtualSdCard)?.progress,
    )
}