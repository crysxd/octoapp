package de.crysxd.octoapp.engine.models.job

import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.CommonTypeParceler
import de.crysxd.octoapp.sharedcommon.InstantParceler
import kotlinx.datetime.Instant

data class JobInformation(
    val file: FileReference.File? = null
) {
    @CommonParcelize
    data class JobFile(
        override val name: String,
        override val path: String,
        override val origin: FileOrigin,
        override val display: String,
        override val size: Long,
        override val id: String = path,
        @CommonTypeParceler<Instant, InstantParceler>() override val date: Instant,
    ) : FileReference.File
}