package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerFilesMetadataParams(
    @SerialName("filename") val filename: String,
) : MoonrakerRpcParams