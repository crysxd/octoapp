package de.crysxd.octoapp.engine

import de.crysxd.octoapp.engine.models.event.Event
import kotlinx.coroutines.flow.Flow

interface EventSource {

    fun eventFlow(tag: String, config: Config = Config()): Flow<Event>
    fun passiveEventFlow(): Flow<Event>

    data class Config(
        // Default throttle is 1 -> 500ms target interval
        val throttle: Int = 1,

        // Default logs we always want to receive -> TuneWidget
        // This allows the live notification and other parts of the app to look out for those values so the TuneWidget gets them right away
        val requestTerminalLogs: List<String> = listOf(
            "M106",
            "M107",
            "M220",
            "M221",
            "M290",
            "Probe Offset"
        ),

        // Whether we are interested in 3rd party plugins
        val requestPlugins: Boolean = true,
    ) {
        companion object {
            const val ALL_LOGS = "%%ALL_LOGS%%"
        }

        constructor() : this(
            throttle = 1,
            requestPlugins = true,
            requestTerminalLogs = listOf(
                "M106",
                "M107",
                "M220",
                "M221",
                "M290",
                "Probe Offset"
            ),
        )
    }
}