package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerServerFilesMetadataParams(
    @SerialName("filename") val filename: String
) : MoonrakerRpcParams