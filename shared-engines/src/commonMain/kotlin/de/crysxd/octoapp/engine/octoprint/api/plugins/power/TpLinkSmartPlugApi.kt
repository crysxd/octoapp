package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tplinksmartplug.TpLinkSmartPlugCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tplinksmartplug.TpLinkSmartPlugResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class TpLinkSmartPlugApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = settings.plugins.tpLinkSmartPlug?.devices?.map {
        PowerDevice(
            owner = this,
            ip = it.ip,
            displayName = it.label
        )
    } ?: emptyList()

    private suspend fun sendCommand(ip: String, command: String) = baseUrlRotator.request {
        httpClient.post {
            url(it)
            setJsonBody(TpLinkSmartPlugCommand(ip = ip, command = command))
        }
    }

    private suspend fun setOn(ip: String, on: Boolean) {
        sendCommand(ip = ip, command = if (on) "turnOn" else "turnOff")
    }

    private suspend fun isOn(ip: String) =
        sendCommand(ip = ip, command = "checkStatus").body<TpLinkSmartPlugResponse>().currentState == TpLinkSmartPlugResponse.State.ON

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.TpLinkSmartPlug)

    internal data class PowerDevice(
        private val owner: TpLinkSmartPlugApi,
        private val ip: String,
        override val displayName: String,
    ) : IPowerDevice {
        override val id: String = ip
        override val pluginId: String = OctoPlugins.TpLinkSmartPlug
        override val pluginDisplayName = "TP-Link Plug"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(ip = ip, on = true)
        override suspend fun turnOff() = owner.setOn(ip = ip, on = false)
        override suspend fun isOn() = owner.isOn(ip = ip)
    }
}