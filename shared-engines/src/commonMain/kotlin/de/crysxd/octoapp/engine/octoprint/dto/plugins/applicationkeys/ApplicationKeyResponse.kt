package de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ApplicationKeyResponse(
    @SerialName("app_token") val appToken: String,
    @SerialName("auth_dialog") val authUrl: String? = null,
)