package de.crysxd.octoapp.engine.moonraker.dto.params

import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOctoAppDatabase
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerServerDatabasePostItemParams(
    @SerialName("namespace") val namespace: String,
    @SerialName("key") val key: String? = null,
    @SerialName("value") val value: MoonrakerOctoAppDatabase.App,
) : MoonrakerRpcParams