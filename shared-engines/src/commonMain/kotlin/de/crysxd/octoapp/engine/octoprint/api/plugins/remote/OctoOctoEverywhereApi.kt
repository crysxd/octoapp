package de.crysxd.octoapp.engine.octoprint.api.plugins.remote

import de.crysxd.octoapp.engine.api.OctoEverywhereApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.remote.OctoEverywhereStatus
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octoeverywhere.OctoEverywhereInfo
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

internal class OctoOctoEverywhereApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : OctoEverywhereApi {

    override suspend fun getInfo() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoEverywhere)
        }.body<OctoEverywhereInfo>().let { info ->
            OctoEverywhereStatus(
                pluginVersion = info.version,
                printerId = info.printerId,
            )
        }
    }
}