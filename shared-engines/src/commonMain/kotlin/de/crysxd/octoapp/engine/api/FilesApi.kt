package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.files.FileList
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import io.ktor.client.request.forms.InputProvider
import io.ktor.utils.io.ByteReadChannel

interface FilesApi {

    suspend fun getAllFiles(origin: FileOrigin): FileList

    suspend fun getFile(origin: FileOrigin, path: String, isDirectory: Boolean): FileObject

    suspend fun executeFileCommand(file: FileReference, command: FileCommand)

    suspend fun deleteFile(fileObject: FileReference)

    suspend fun createFolder(parent: FileReference.Folder, name: String)

    suspend fun uploadFile(parent: FileReference.Folder, input: InputProvider, name: String, progressUpdate: (Float) -> Unit)

    suspend fun <T> downloadFile(file: FileReference.File, progressUpdate: (Float) -> Unit = {}, consume: suspend (ByteReadChannel) -> T): T

}