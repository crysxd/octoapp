package de.crysxd.octoapp.engine.models.system

import kotlinx.serialization.Serializable

@Serializable
data class SystemCommand(
    val name: String,
    val action: String,
    val source: String,
    val confirmation: String?,
    val type: Type? = Type.Other,
) {
    enum class Type {
        Other, Reboot, Restart, RestartSafeMode, Shutdown
    }
}