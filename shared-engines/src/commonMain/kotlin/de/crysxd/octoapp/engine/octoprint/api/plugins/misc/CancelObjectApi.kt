package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.cancelobject.CancelObjectCommand
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post

class CancelObjectApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) {

    suspend fun triggerInitialMessage() = baseUrlRotator.request<Unit> {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.CancelObject)
        }
    }

    suspend fun cancelObject(objectId: Int) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.CancelObject)
            setJsonBody(CancelObjectCommand(objectId))
        }
    }
}