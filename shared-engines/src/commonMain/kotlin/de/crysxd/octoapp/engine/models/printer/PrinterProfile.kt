package de.crysxd.octoapp.engine.models.printer

import kotlinx.serialization.Serializable

@Serializable
data class PrinterProfile(
    val id: String = "fallback",
    val current: Boolean = false,
    val default: Boolean = false,
    val model: String = "Unknown",
    val name: String = "Unknown",
    val color: String = "default",
    val volume: Volume = Volume(),
    val axes: Axes = Axes(),
    val extruders: List<Extruder> = listOf(Extruder(componentName = "tool0")),
    val heatedChamber: Boolean = false,
    val heatedBed: Boolean = true,
) {

    val estimatedNozzleDiameter = if (extruders.isEmpty()) 0.4f else extruders.map { it.nozzleDiameter }.average().toFloat()
    val extruderHeatingComponents = extruders.map { it.componentName }
    val extruderMotorComponents = extruders.flatMap { it.extruderComponents }

    @Serializable
    data class Volume(
        val depth: Float = 200f,
        val width: Float = 200f,
        val height: Float = 200f,
        val origin: Origin = Origin.LowerLeft,
        val formFactor: FormFactor = FormFactor.Rectangular,
        val boundingBox: CustomBox = CustomBox(
            xMin = if (origin == Origin.Center) -(width / 2) else 0f,
            xMax = if (origin == Origin.Center) (width / 2) else width,
            yMin = if (origin == Origin.Center) -(depth / 2) else 0f,
            yMax = if (origin == Origin.Center) (depth / 2) else depth,
            zMin = 0f,
            zMax = height,
        ),
    )

    @Serializable
    data class CustomBox(
        val xMax: Float,
        val xMin: Float,
        val yMax: Float,
        val yMin: Float,
        val zMax: Float,
        val zMin: Float,
    )

    @Serializable
    data class Axes(
        val e: Axis = Axis(),
        val x: Axis = Axis(),
        val y: Axis = Axis(),
        val z: Axis = Axis(),
    )

    @Serializable
    data class Axis(
        val inverted: Boolean = false,
        val speed: Float? = null,
    )

    @Serializable
    data class Extruder(
        val nozzleDiameter: Float = 0.4f,
        val componentName: String,
        val extruderComponents: List<String> = listOf(componentName),
    )

    @Serializable
    enum class Origin {
        LowerLeft,
        Center
    }

    @Serializable
    enum class FormFactor {
        Circular,
        Rectangular
    }
}