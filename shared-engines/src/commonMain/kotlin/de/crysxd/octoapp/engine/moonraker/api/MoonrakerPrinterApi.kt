package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.PrinterApi
import de.crysxd.octoapp.engine.exceptions.MoveOutOfRangeException
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerEventHandler
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.connection.sendRequestIgnoringResult
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebhooks
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterGcodeScriptParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectsQueryParams
import de.crysxd.octoapp.engine.moonraker.mappers.MoonrakerPrinterStateMapperInput
import de.crysxd.octoapp.engine.moonraker.mappers.map
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import io.github.aakira.napier.Napier
import kotlinx.serialization.json.floatOrNull
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import kotlin.math.roundToInt

internal class MoonrakerPrinterApi(
    private val requestHandler: MoonrakerRequestHandler,
    private val eventHandler: MoonrakerEventHandler,
) : PrinterApi {

    private val tag = "MoonrakerPrinterApi"

    override suspend fun getPrinterState(): PrinterState {
        //region Query objects
        val status = requestHandler.sendRequest<MoonrakerPrinterObjectsQuery>(
            method = "printer.objects.query",
            params = MoonrakerPrinterObjectsQueryParams(
                objects = mapOf(
                    Constants.PrinterObjects.PrintStats to null,
                    Constants.PrinterObjects.Webhooks to null,
                )
            )
        ).status
        Napier.i("Not printing: $status")
        //endregion
        //region Map
        return PrinterState(
            state = MoonrakerPrinterStateMapperInput(
                stats = status?.get(Constants.PrinterObjects.PrintStats) as? MoonrakerPrintStats,
                webhooks = status?.get(Constants.PrinterObjects.Webhooks) as? MoonrakerWebhooks,
            ).map(),
            temperature = emptyMap(),
        )
        //endregion
    }

    override suspend fun setTemperatureTargets(targets: Map<String, Float>) = targets.forEach { (component, target) ->
        executeGcodeInternal(
            code = "SET_HEATER_TEMPERATURE HEATER=${component.split(" ").last()} TARGET=${target}",
            awaitResponse = true
        )
    }

    override suspend fun setTemperatureOffsets(offsets: Map<String, Float>) = Unit

    override suspend fun extrudeFilament(length: Float, speedMmMin: Float?) = executeGcodeInternal(
        code = """
                SAVE_GCODE_STATE NAME=octoapp
                G91
                G1 E${length} F${speedMmMin ?: 360}
                RESTORE_GCODE_STATE NAME=octoapp
            """.trimIndent(),
        awaitResponse = true
    )

    override suspend fun changeZOffset(changeMm: Float) = executeGcodeInternal(
        code = "SET_GCODE_OFFSET Z_ADJUST=${changeMm}",
        awaitResponse = true
    )

    override suspend fun saveZOffset() = executeGcodeInternal(
        code = "Z_OFFSET_APPLY_PROBE",
        awaitResponse = true
    )

    override suspend fun resetZOffset() = executeGcodeInternal(
        code = "SET_GCODE_OFFSET Z=0",
        awaitResponse = true
    )

    override suspend fun selectExtruder(component: String) = executeGcodeInternal(
        code = "ACTIVATE_EXTRUDER EXTRUDER=${component.split(" ").last()}",
        awaitResponse = true
    )

    override suspend fun executePrintHeadCommand(cmd: PrintHeadCommand) = when (cmd) {
        is PrintHeadCommand.HomePrintHeadCommand -> executeGcodeInternal(
            code = (listOf("G28") + cmd.axes).joinToString(" ").uppercase(),
            awaitResponse = true,
        )

        is PrintHeadCommand.JogPrintHeadCommand -> try {
            executeGcodeInternal(
                code = """
                SAVE_GCODE_STATE NAME=octoapp_extrude
                G91
                G1 X${cmd.x} Y${cmd.y} Z${cmd.z} F${cmd.speed ?: 5000}
                RESTORE_GCODE_STATE NAME=octoapp_extrude
            """.trimIndent(),
                awaitResponse = true
            )
        } catch (e: PrinterApiException) {
            if (e.responseCode == 400) {
                throw MoveOutOfRangeException()
            } else {
                throw e
            }
        }

        is PrintHeadCommand.MovePrintHeadCommand -> try {
            executeGcodeInternal(
                code = """
                SAVE_GCODE_STATE NAME=octoapp_extrude
                G90
                G1 X${cmd.x} Y${cmd.y} Z${cmd.z} F${cmd.speed ?: 5000}
                RESTORE_GCODE_STATE NAME=octoapp_extrude
            """.trimIndent(),
                awaitResponse = true
            )
        } catch (e: PrinterApiException) {
            if (e.responseCode == 400) {
                throw MoveOutOfRangeException()
            } else {
                throw e
            }
        }
    }

    override suspend fun executeGcodeCommand(cmd: GcodeCommand) = when (cmd) {
        is GcodeCommand.Batch -> cmd.commands.joinToString("\n")
        is GcodeCommand.Single -> cmd.command
    }.let { code ->
        executeGcodeInternal(
            code = code,
            awaitResponse = false
        )
    }

    override suspend fun setFanSpeed(component: String, percent: Float) {
        val type = component.split(" ").first()

        when {
            component == Constants.PrinterObjects.Fan -> "M106 S${(percent.coerceIn(0f..100f) * 2.55f).roundToInt()}"
            type == Constants.PrinterObjects.OutputPin -> {
                val configFile = eventHandler.status.value?.items?.get(Constants.PrinterObjects.ConfigFile) as? MoonrakerConfigFile
                val componentConfig = configFile?.config?.get(component)?.jsonObject
                val scale = componentConfig?.get("scale")?.jsonPrimitive?.floatOrNull ?: 1f
                "SET_PIN PIN=${component.split(" ").drop(1).firstOrNull()} VALUE=${percent / 100f * scale}"
            }
            else -> "SET_FAN_SPEED FAN=${component.split(" ").drop(1).firstOrNull()} SPEED=${percent / 100f}"
        }.let { code ->
            executeGcodeInternal(
                code = code,
                awaitResponse = false
            )
        }
    }

    private suspend fun setHeaterTemperature(
        heater: String,
        target: Float
    ) = executeGcodeInternal(
        code = "SET_HEATER_TEMPERATURE HEATER=$heater TARGET=$target",
        awaitResponse = true
    )

    private suspend fun executeGcodeInternal(
        code: String,
        awaitResponse: Boolean
    ) {
        Napier.i(tag = tag, message = "Executing Gcode: ${code.replace("\n", "\\n")}")
        when {
            // M112 (emergency stop) is placed onto the queue for Klipper, request emergency stop manually
            code.uppercase() == "M112" -> requestHandler.sendRequest("printer.emergency_stop")

            // For firmware restart we want to wait for the response, let's use the dedicated endpoint
            code.uppercase() == "FIRMWARE_RESTART" -> requestHandler.sendRequest("printer.firmware_restart")

            // Await response?
            awaitResponse -> requestHandler.sendRequest("printer.gcode.script", MoonrakerPrinterGcodeScriptParams(script = code))

            // Any other Gcode is fire & forget
            else -> requestHandler.sendRequestIgnoringResult("printer.gcode.script", MoonrakerPrinterGcodeScriptParams(script = code))
        }
    }
}