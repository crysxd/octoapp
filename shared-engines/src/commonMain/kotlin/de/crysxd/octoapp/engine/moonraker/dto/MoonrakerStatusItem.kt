package de.crysxd.octoapp.engine.moonraker.dto

internal sealed interface MoonrakerStatusItem {
    fun backFillWithPrevious(previous: MoonrakerStatusItem): MoonrakerStatusItem
}