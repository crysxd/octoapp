package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerLegacyWebcamDatabase
import io.github.aakira.napier.Napier
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonObject

internal class MoonrakerLegacyWebcamDatabaseSerializer : KSerializer<MoonrakerLegacyWebcamDatabase> {

    private val tag = "MoonrakerLegacyWebcamDatabaseSerializer"
    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerLegacyWebcamDatabase", kind = PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): MoonrakerLegacyWebcamDatabase {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonObject
        val webcams = tree.entries.associate { (key, element) ->
            key to try {
                input.json.decodeFromJsonElement<MoonrakerLegacyWebcamDatabase.Webcam>(element)
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to deserialize webcam", throwable = e)
                null
            }
        }


        return MoonrakerLegacyWebcamDatabase(webcams)
    }

    override fun serialize(encoder: Encoder, value: MoonrakerLegacyWebcamDatabase) = throw UnsupportedOperationException("Can't serialize")
}