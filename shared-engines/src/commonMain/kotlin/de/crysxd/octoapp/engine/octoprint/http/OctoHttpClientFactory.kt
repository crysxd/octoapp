package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.framework.Constants.SuppressBrokenSetup
import de.crysxd.octoapp.engine.framework.ExceptionInspector
import de.crysxd.octoapp.engine.framework.installApiKeyInterceptor
import de.crysxd.octoapp.engine.framework.installNgrokHeaderInterceptor
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.octoprint.CommandBody
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder
import de.crysxd.octoapp.engine.octoprint.api.OctoUserApi
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.http.framework.installBasicAuthInterceptorPlugin
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.plugin
import io.ktor.client.plugins.websocket.WebSockets
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@OptIn(ExperimentalSerializationApi::class)
private val EngineJsonWithNulls by lazy {
    Json(EngineJson) {
        explicitNulls = true
    }
}

internal fun createOctoPrintHttpClient(
    engine: HttpClientEngine? = null,
    settings: OctoPrintEngineBuilder.HttpClientSettings,
): HttpClient {
    val sharedConfig: HttpClientConfig<*>.() -> Unit = {
        expectSuccess = false
        followRedirects = true

        installJsonSerialization()
        installWebSockets()
    }

    return DefaultHttpClient(settings.general, engine, sharedConfig).apply {
        installExceptionInspector(settings.exceptionInspector)
        installBasicAuthInterceptorPlugin()
        installApiKeyInterceptor(settings.apiKey)
        installNgrokHeaderInterceptor()
        installOctoPrintGenerateExceptionInterceptorPlugin { OctoUserApi(baseUrlRotator = settings.baseUrlRotator, httpClient = it) }
    }
}

inline fun <reified T> HttpRequestBuilder.setJsonBody(body: T) {
    setBody(body)
    contentType(ContentType.Application.Json)

    if (body is CommandBody) {
        body.log()
    }
}

internal inline fun <reified T> HttpRequestBuilder.setJsonBodyWithExplicitNulls(body: T) {
    setBody(EngineJsonWithNulls.encodeToString(body))
    contentType(ContentType.Application.Json)

    if (body is CommandBody) {
        body.log()
    }
}

internal fun HttpClient.installExceptionInspector(inspector: ExceptionInspector) {
    plugin(HttpSend).intercept { request ->
        try {
            execute(request)
        } catch (e: Exception) {
            if (!request.attributes.contains(SuppressBrokenSetup)) {
                inspector.inspect(e)
            }

            throw e
        }
    }
}

internal fun HttpClientConfig<*>.installWebSockets() {
    install(WebSockets)
}

internal fun HttpClientConfig<*>.installJsonSerialization() {
    install(ContentNegotiation) {
        json(EngineJson)
    }
}