package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerBedMesh
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDisplayStatus
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExcludeObject
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExtruder
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFan
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMove
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerHeaterBed
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerHeaterGeneric
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerHeaters
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerIdleTimeout
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerLed
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerMcu
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerMotionReport
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOutputPin
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerStepperEnable
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerTemperatureFan
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerTemperatureSensor
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerToolhead
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerVirtualSdCard
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebhooks
import io.github.aakira.napier.Napier
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonObject

internal class MoonrakerPrinterObjectsQueryStatusItemSerializer : KSerializer<MoonrakerPrinterObjectsQuery.StatusItems> {

    private val tag = "MoonrakerStatusItemsSerializer"
    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerStatus.Items", kind = PrimitiveKind.STRING)

    companion object {
        private val loggedMissingStatusItems = mutableListOf<String>()
    }

    override fun deserialize(decoder: Decoder): MoonrakerPrinterObjectsQuery.StatusItems {
        val input = decoder as JsonDecoder
        val json = decoder.json
        val tree = input.decodeJsonElement().jsonObject
        val items = tree.entries.associate { (key, item) ->
            try {
                val type = key.split(" ")[0]
                key to when (type) {
                    Constants.PrinterObjects.PrintStats -> json.decodeFromJsonElement(MoonrakerPrintStats.serializer(), item)
                    Constants.PrinterObjects.Toolhead -> json.decodeFromJsonElement(MoonrakerToolhead.serializer(), item)
                    Constants.PrinterObjects.DisplayStatus -> json.decodeFromJsonElement(MoonrakerDisplayStatus.serializer(), item)
                    Constants.PrinterObjects.VirtualSdCard -> json.decodeFromJsonElement(MoonrakerVirtualSdCard.serializer(), item)
                    Constants.PrinterObjects.IdleTimeout -> json.decodeFromJsonElement(MoonrakerIdleTimeout.serializer(), item)
                    Constants.PrinterObjects.SystemStats -> json.decodeFromJsonElement(MoonrakerSystemStats.serializer(), item)
                    Constants.PrinterObjects.MotionReport -> json.decodeFromJsonElement(MoonrakerMotionReport.serializer(), item)
                    Constants.PrinterObjects.Webhooks -> json.decodeFromJsonElement(MoonrakerWebhooks.serializer(), item)
                    Constants.PrinterObjects.HeaterBed -> json.decodeFromJsonElement(MoonrakerHeaterBed.serializer(), item)
                    Constants.PrinterObjects.TemperatureSensorKind -> json.decodeFromJsonElement(MoonrakerTemperatureSensor.serializer(), item)
                    Constants.PrinterObjects.TemperatureFanKind -> json.decodeFromJsonElement(MoonrakerTemperatureFan.serializer(), item)
                    Constants.PrinterObjects.HeaterGenericKind -> json.decodeFromJsonElement(MoonrakerHeaterGeneric.serializer(), item)
                    Constants.PrinterObjects.GcodeMove -> json.decodeFromJsonElement(MoonrakerGcodeMove.serializer(), item)
                    Constants.PrinterObjects.ConfigFile -> json.decodeFromJsonElement(MoonrakerConfigFile.serializer(), item)
                    Constants.PrinterObjects.BedMesh -> json.decodeFromJsonElement(MoonrakerBedMesh.serializer(), item)
                    Constants.PrinterObjects.Heaters -> json.decodeFromJsonElement(MoonrakerHeaters.serializer(), item)
                    Constants.PrinterObjects.OutputPin -> json.decodeFromJsonElement(MoonrakerOutputPin.serializer(), item)
                    Constants.PrinterObjects.Mcu -> json.decodeFromJsonElement(MoonrakerMcu.serializer(), item)
                    Constants.PrinterObjects.HeaterFan -> json.decodeFromJsonElement(MoonrakerFan.serializer(), item)
                    Constants.PrinterObjects.Fan -> json.decodeFromJsonElement(MoonrakerFan.serializer(), item)
                    Constants.PrinterObjects.FanGenericKind -> json.decodeFromJsonElement(MoonrakerFan.serializer(), item)
                    Constants.PrinterObjects.StepperEnable -> json.decodeFromJsonElement(MoonrakerStepperEnable.serializer(), item)
                    Constants.PrinterObjects.ExcludeObject -> json.decodeFromJsonElement(MoonrakerExcludeObject.serializer(), item)
                    Constants.PrinterObjects.Led,
                    Constants.PrinterObjects.Neopixel,
                    Constants.PrinterObjects.Dotstar,
                    Constants.PrinterObjects.Pca9533,
                    Constants.PrinterObjects.Pca9632 -> json.decodeFromJsonElement(MoonrakerLed.serializer(), item)

                    else -> when {
                        // extruder, extruder1, extruder2, ...
                        type.startsWith(Constants.PrinterObjects.Extruder) -> json.decodeFromJsonElement(MoonrakerExtruder.serializer(), item)

                        else -> {
                            if (key !in loggedMissingStatusItems) {
                                Napier.w(tag = tag, message = "Unknown status item $key")
                                loggedMissingStatusItems += key
                            }

                            null
                        }
                    }
                }
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to decode status item $key, falling back to null. Input: $item", throwable = e)
                key to null
            }
        }

        return MoonrakerPrinterObjectsQuery.StatusItems(items)
    }

    override fun serialize(encoder: Encoder, value: MoonrakerPrinterObjectsQuery.StatusItems) = throw UnsupportedOperationException("Can't serialize")
}