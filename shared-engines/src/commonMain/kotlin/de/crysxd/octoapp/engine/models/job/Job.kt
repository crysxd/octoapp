package de.crysxd.octoapp.engine.models.job

import de.crysxd.octoapp.engine.models.printer.PrinterState

data class Job(
    val progress: ProgressInformation = ProgressInformation(),
    val info: JobInformation = JobInformation(),
    val state: PrinterState.State = PrinterState.State()
) {
    val statusText
        get() = state.createStatusText(progress.completion)
}