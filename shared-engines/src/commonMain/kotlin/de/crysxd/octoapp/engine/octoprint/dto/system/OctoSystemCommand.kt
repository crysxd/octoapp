package de.crysxd.octoapp.engine.octoprint.dto.system

import kotlinx.serialization.Serializable

@Serializable
data class OctoSystemCommand(
    val name: String = "Unknown",
    val source: String,
    val action: String,
    val confirm: String? = null,
    val resource: String? = null,
)