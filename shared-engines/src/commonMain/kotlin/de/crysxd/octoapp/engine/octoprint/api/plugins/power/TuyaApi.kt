package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tuya.TuyaCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tuya.TuyaResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class TuyaApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = settings.plugins.tuya?.devices?.map {
        PowerDevice(
            owner = this,
            displayName = it.label,
        )
    } ?: emptyList()

    private suspend fun sendCommand(device: PowerDevice, command: String) = try {
        baseUrlRotator.request {
            httpClient.post {
                url(it)
                setJsonBody(TuyaCommand(label = device.displayName, command = command))
            }
        }
    } catch (e: PrinterApiException) {
        if (e.responseCode == 500 || e.responseCode == 204) {
            // That's basically a 200....
            null
        } else {
            throw e
        }
    }

    private suspend fun setOn(device: PowerDevice, on: Boolean) {
        sendCommand(device = device, command = if (on) "turnOn" else "turnOff")
    }

    private suspend fun isOn(device: PowerDevice) =
        sendCommand(device = device, command = "checkStatus")?.body<TuyaResponse>()?.currentState == TuyaResponse.State.ON

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.Tuya)

    internal data class PowerDevice(
        private val owner: TuyaApi,
        override val displayName: String
    ) : IPowerDevice {
        // Tuya uses display name as id....
        override val id: String = displayName
        override val pluginId: String = OctoPlugins.Tuya
        override val pluginDisplayName = "Tuya"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(device = this, on = true)
        override suspend fun turnOff() = owner.setOn(device = this, on = false)
        override suspend fun isOn() = owner.isOn(device = this)
    }
}