package de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys


sealed class ApplicationKeyStatus {
    data object DeniedOrTimedOut : ApplicationKeyStatus()
    data object Pending : ApplicationKeyStatus()
    data class Granted(val apiKey: String) : ApplicationKeyStatus()
}