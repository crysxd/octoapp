package de.crysxd.octoapp.engine.octoprint.api.plugins.remote

import de.crysxd.octoapp.engine.api.ObicoApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.remote.ObicoStatus
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.ObicoCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.OctoObicoStatus
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post

internal class OctoObicoApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : ObicoApi(
    baseUrlRotator = baseUrlRotator,
    httpClient = httpClient,
) {

    override suspend fun getPluginStatus() = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.Obico)
            setJsonBody(ObicoCommand.GetPluginStatus())
        }.body<OctoObicoStatus>().let { status ->
            ObicoStatus(
                name = status.linkedPrinter?.name,
                printerId = status.linkedPrinter?.id,
                isPro = status.linkedPrinter?.isPro,
            )
        }
    }
}