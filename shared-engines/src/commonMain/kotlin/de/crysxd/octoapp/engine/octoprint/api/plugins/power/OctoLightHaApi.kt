package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolight.OctoLightState
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class OctoLightHaApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = if (settings.plugins.octoLightHA != null) {
        listOf(PowerDevice(this))
    } else {
        emptyList()
    }

    private suspend fun executeCommand(action: String) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoLightHA)
            parameter("action", action)
        }
    }

    private suspend fun turnOn() {
        executeCommand("turnOn")
    }

    private suspend fun turnOff() {
        executeCommand("turnOff")
    }

    private suspend fun toggle() {
        executeCommand("toggle")
    }

    private suspend fun isOn() = executeCommand("getState").body<OctoLightState>().state

    internal data class PowerDevice(
        private val owner: OctoLightHaApi,
    ) : IPowerDevice {
        override val id = "octolight-ha"
        override val pluginId = OctoPlugins.OctoLightHA
        override val displayName = "OctoLight Home Assistant"
        override val pluginDisplayName = "OctoLight Home Assistant"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate, IPowerDevice.Capability.ControlPrinterPower)
        override suspend fun turnOn() = owner.turnOn()
        override suspend fun turnOff() = owner.turnOff()
        override suspend fun isOn() = owner.isOn()
        override suspend fun toggle() = owner.toggle()
    }
}