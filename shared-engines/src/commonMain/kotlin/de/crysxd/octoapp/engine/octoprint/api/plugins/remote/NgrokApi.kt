package de.crysxd.octoapp.engine.octoprint.api.plugins.remote

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.ngrok.NgrokConfig
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

class NgrokApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) {

    suspend fun getActiveTunnel() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.Ngrok)
        }.body<NgrokConfig>()
    }
}