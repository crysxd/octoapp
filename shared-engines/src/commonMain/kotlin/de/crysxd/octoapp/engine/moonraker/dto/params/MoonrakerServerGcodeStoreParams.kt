package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerServerGcodeStoreParams(
    @SerialName("count") val count: Int = 1000
) : MoonrakerRpcParams