package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.models.settings.Settings

interface PowerDevicesApi {
    suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<PowerDevice>
}