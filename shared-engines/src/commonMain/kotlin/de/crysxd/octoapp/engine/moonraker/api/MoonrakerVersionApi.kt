package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.VersionApi
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerServerInfo
import de.crysxd.octoapp.engine.moonraker.mappers.map

internal class MoonrakerVersionApi(
    private val requestHandler: MoonrakerRequestHandler,
) : VersionApi {

    override suspend fun getVersion() = requestHandler.sendRequest<MoonrakerServerInfo>("server.info").map()
}