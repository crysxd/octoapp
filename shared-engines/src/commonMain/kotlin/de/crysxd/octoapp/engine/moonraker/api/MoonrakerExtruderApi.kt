package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExtruder
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectsQueryParams

class MoonrakerExtruderApi internal constructor(
    private val requestHandler: MoonrakerRequestHandler
) {
    suspend fun checkExtruderCanExtrude(component: String): Boolean {
        val extruder = requestHandler.sendRequest<MoonrakerPrinterObjectsQuery>(
            method = "printer.objects.query",
            params = MoonrakerPrinterObjectsQueryParams(
                objects = mapOf(component to null)
            )
        ).status?.get(Constants.PrinterObjects.Extruder) as? MoonrakerExtruder
        return extruder?.canExtrude == true
    }
}