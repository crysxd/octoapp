package de.crysxd.octoapp.engine.moonraker.dto.params


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerServerConnectionIdentifyParams(
    @SerialName("access_token") val accessToken: String? = null,
    @SerialName("api_key") val apiKey: String? = null,
    @SerialName("client_name") val clientName: String? = null,
    @SerialName("type") val type: String? = null,
    @SerialName("url") val url: String? = null,
    @SerialName("version") val version: String? = null
) : MoonrakerRpcParams