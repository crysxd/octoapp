package de.crysxd.octoapp.engine.octoprint.event

import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.models.login.LoginResponse
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportConfiguration
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.minutes

internal abstract class OctoEventTransport(
    private val loginApi: () -> LoginApi,
    private val parentJob: Job,
    private val parenTag: String,
) {
    private var connectionCounter = 0
    private var reconfigureCompat = false
    private var didSendConfig = false
    private val loginMutex = Mutex()
    protected val tag get() = "$parenTag/$name/$connectionCounter"
    private var loginResponse: LoginResponse? = null
    private var loginResponseValidUntil = Instant.fromEpochSeconds(0)
    private val sessionDuration = 10.minutes
    protected val config = MutableStateFlow(OctoEventTransportConfiguration())
    protected abstract val name: String

    protected var job: Job? = null
        private set
    protected lateinit var scope: CoroutineScope
        private set

    protected fun recreateScope(): CoroutineScope {
        connectionCounter++
        Napier.i(tag = tag, message = "Recreating scope")
        job?.cancel()
        if (::scope.isInitialized) {
            scope.cancel()
        }
        val job = SupervisorJob(parentJob).also { job = it }
        scope = CoroutineScope(job + Dispatchers.SharedIO + CoroutineExceptionHandler { _, throwable -> handleException(throwable) })

        // Login in background already to speed up connection
        scope.launch {
            Napier.d(tag = tag, message = "Triggering eager login")
            logIn()
        }
        didSendConfig = false
        return scope
    }

    protected suspend fun logIn(): String = loginMutex.withLock {
        val lr = loginResponse
        return if (lr?.session != null && loginResponseValidUntil + sessionDuration > Clock.System.now()) {
            Napier.d(tag = tag, message = "Reusing login, valid until $loginResponseValidUntil")
            "${lr.name}:${lr.session}"
        } else {
            Napier.d(tag = tag, message = "Performing login")
            loginApi().passiveLogin().let {
                loginResponse = it
                loginResponseValidUntil = Clock.System.now() + sessionDuration
                Napier.d(tag = tag, message = "Login completed, valid until $loginResponseValidUntil")
                "${it.name}:${it.session}"
            }
        }
    }

    protected fun onConnected(message: OctoMessage.Connected) {
        // There is a bug in all OctoPrint up to 1.9.0 causing a reconfigured connection to not send the
        // history message again
        Napier.i(tag = tag, message = "Connected to OctoPrint ${message.version}")
        reconfigureCompat = message.version?.asVersion()?.let { "1.10.0".asVersion() > it } ?: true
        if (reconfigureCompat) {
            Napier.d(tag = tag, message = "Using reconfiguration compat layer")
        }
    }

    protected fun beforeSendingConfig(): Boolean = if (reconfigureCompat && didSendConfig) {
        // This version of OctoPrint can't reconfigure, we need to reconnect
        Napier.d(tag = tag, message = "Configuration changed but already connected, reconnecting... (behaviour for old OctoPrint versions)")
        connect()
        didSendConfig = false
        false
    } else {
        // We can reconfigure the existing connection
        didSendConfig = true
        true
    }

    protected fun logOut() {
        loginResponse = null
        loginResponseValidUntil = Instant.DISTANT_PAST
    }

    fun configure(configuration: OctoEventTransportConfiguration) {
        config.value = configuration
    }

    fun disconnect() {
        if (::scope.isInitialized) {
            scope.cancel()
        }
        Napier.i(tag = tag, message = "Disconnecting")
    }

    abstract fun connect()
    protected abstract fun handleException(e: Throwable)

}