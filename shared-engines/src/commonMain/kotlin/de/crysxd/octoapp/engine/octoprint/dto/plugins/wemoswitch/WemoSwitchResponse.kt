package de.crysxd.octoapp.engine.octoprint.dto.plugins.wemoswitch

import kotlinx.serialization.Serializable

@Serializable
internal data class WemoSwitchResponse(
    val currentState: String? = null
)