package de.crysxd.octoapp.engine.octoprint.api.plugins.material

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPut
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class SpoolManagerApiTest {

    @Test
    fun WHEN_spools_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/SpoolManager/loadSpoolsByQuery?from=0&to=1000&sortColumn=displayName&sortOrder=desc&filterName=hideEmptySpools%2ChideInactiveSpools"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                       {"allSpools":[{"bedTemperature":null,"code":null,"color":"#ee82ee","colorName":"violet","cost":null,"costUnit":"\u20ac","created":"14.09.2021 06:28","databaseId":6,"density":null,"diameter":null,"diameterTolerance":null,"displayName":"Template","enclosureTemperature":null,"firstUse":"","flowRateCompensation":null,"isActive":true,"isTemplate":true,"labels":"[]","lastUse":"","material":"","materialCharacteristic":null,"noteDeltaFormat":"{\"ops\": [{\"insert\": \"\\n\"}]}","noteHtml":"<p><br></p>","noteText":"\n","offsetBedTemperature":null,"offsetEnclosureTemperature":null,"offsetTemperature":null,"originator":null,"purchasedFrom":null,"purchasedOn":"","remainingLength":"0","remainingLengthPercentage":"","remainingPercentage":"100.0","remainingWeight":"100.0","spoolWeight":"","temperature":null,"totalLength":0,"totalWeight":"100.0","updated":"14.09.2021 06:28","usedLength":0,"usedLengthPercentage":"","usedPercentage":"0.0","usedWeight":"0.0","vendor":"","version":2},{"bedTemperature":null,"code":null,"color":"#ffff00","colorName":"yellow","cost":null,"costUnit":"\u20ac","created":"03.08.2021 19:00","databaseId":3,"density":1.24,"diameter":null,"diameterTolerance":null,"displayName":"SM Sp\u00e4tzle","enclosureTemperature":null,"firstUse":"20.08.2021 19:40","flowRateCompensation":null,"isActive":true,"isTemplate":false,"labels":"[]","lastUse":"29.09.2022 15:46","material":"PLA","materialCharacteristic":null,"noteDeltaFormat":"{\"ops\": [{\"insert\": \"\\n\"}]}","noteHtml":"<p><br></p>","noteText":"\n","offsetBedTemperature":null,"offsetEnclosureTemperature":null,"offsetTemperature":null,"originator":null,"purchasedFrom":null,"purchasedOn":"","remainingLength":"-61236","remainingLengthPercentage":"","remainingPercentage":"99.0","remainingWeight":"99.0","spoolWeight":"","temperature":null,"totalLength":0,"totalWeight":"100.0","updated":"03.08.2021 19:00","usedLength":61236,"usedLengthPercentage":"","usedPercentage":"1.0","usedWeight":"1.0","vendor":"Germany","version":5229},{"bedTemperature":null,"code":null,"color":"#ff0000","colorName":"red","cost":null,"costUnit":"\u20ac","created":"03.08.2021 19:00","databaseId":1,"density":1.24,"diameter":null,"diameterTolerance":null,"displayName":"SM Spaghetti","enclosureTemperature":null,"firstUse":"20.08.2021 19:40","flowRateCompensation":null,"isActive":true,"isTemplate":false,"labels":"[]","lastUse":"06.02.2022 08:39","material":"PLA","materialCharacteristic":null,"noteDeltaFormat":"{\"ops\": [{\"insert\": \"\\n\"}]}","noteHtml":"<p><br></p>","noteText":"\n","offsetBedTemperature":null,"offsetEnclosureTemperature":null,"offsetTemperature":null,"originator":null,"purchasedFrom":null,"purchasedOn":"","remainingLength":"0","remainingLengthPercentage":"","remainingPercentage":"90.0","remainingWeight":"90.0","spoolWeight":"","temperature":null,"totalLength":0,"totalWeight":"100.0","updated":"03.08.2021 19:00","usedLength":0,"usedLengthPercentage":"","usedPercentage":"10.0","usedWeight":"10.0","vendor":"Italy","version":4},{"bedTemperature":null,"code":null,"color":"#ffffff","colorName":"white","cost":null,"costUnit":"\u20ac","created":"03.08.2021 19:00","databaseId":2,"density":1.24,"diameter":null,"diameterTolerance":null,"displayName":"SM Ramen","enclosureTemperature":null,"firstUse":"20.08.2021 19:40","flowRateCompensation":null,"isActive":true,"isTemplate":false,"labels":"[]","lastUse":"06.02.2022 08:39","material":"PLA","materialCharacteristic":null,"noteDeltaFormat":"{\"ops\": [{\"insert\": \"\\n\"}]}","noteHtml":"<p><br></p>","noteText":"\n","offsetBedTemperature":null,"offsetEnclosureTemperature":null,"offsetTemperature":null,"originator":null,"purchasedFrom":null,"purchasedOn":"","remainingLength":"0","remainingLengthPercentage":"","remainingPercentage":"85.0","remainingWeight":"170.0","spoolWeight":"","temperature":null,"totalLength":0,"totalWeight":"200.0","updated":"03.08.2021 19:00","usedLength":0,"usedLengthPercentage":"","usedPercentage":"15.0","usedWeight":"30.0","vendor":"Japan","version":5},{"bedTemperature":null,"code":null,"color":"#ffa500","colorName":"orange","cost":null,"costUnit":"\u20ac","created":"03.08.2021 19:01","databaseId":4,"density":1.24,"diameter":null,"diameterTolerance":null,"displayName":"SM Ramen","enclosureTemperature":null,"firstUse":"20.08.2021 19:40","flowRateCompensation":null,"isActive":true,"isTemplate":false,"labels":"[]","lastUse":"06.02.2022 08:39","material":"PLA","materialCharacteristic":null,"noteDeltaFormat":"{\"ops\": [{\"insert\": \"\\n\"}]}","noteHtml":"<p><br></p>","noteText":"\n","offsetBedTemperature":null,"offsetEnclosureTemperature":null,"offsetTemperature":null,"originator":null,"purchasedFrom":null,"purchasedOn":"","remainingLength":"0","remainingLengthPercentage":"","remainingPercentage":"100.0","remainingWeight":"1000.0","spoolWeight":"","temperature":null,"totalLength":0,"totalWeight":"1000.0","updated":"03.08.2021 19:01","usedLength":0,"usedLengthPercentage":"","usedPercentage":"0.0","usedWeight":"0.0","vendor":"Japan","version":5}],"catalogs":{"colors":[{"color":"#ff0000","colorId":"#ff0000;red","colorName":"red"},{"color":"#ffffff","colorId":"#ffffff;white","colorName":"white"},{"color":"#ffff00","colorId":"#ffff00;yellow","colorName":"yellow"},{"color":"#ffa500","colorId":"#ffa500;orange","colorName":"orange"},{"color":"#ee82ee","colorId":"#ee82ee;violet","colorName":"violet"}],"labels":[],"materials":["","PLA","PLA_plus","ABS","PETG","NYLON","TPU","PC","Wood","Carbon Fiber","PC_ABS","HIPS","PVA","ASA","PP","POM","PMMA","FPE"],"vendors":["","Germany","Italy","Japan"]},"selectedSpools":[{"bedTemperature":null,"code":null,"color":"#ffff00","colorName":"yellow","cost":null,"costUnit":"\u20ac","created":"03.08.2021 19:00","databaseId":3,"density":1.24,"diameter":null,"diameterTolerance":null,"displayName":"SM Sp\u00e4tzle","enclosureTemperature":null,"firstUse":"20.08.2021 19:40","flowRateCompensation":null,"isActive":true,"isTemplate":false,"labels":"[]","lastUse":"29.09.2022 15:46","material":"PLA","materialCharacteristic":null,"noteDeltaFormat":"{\"ops\": [{\"insert\": \"\\n\"}]}","noteHtml":"<p><br></p>","noteText":"\n","offsetBedTemperature":null,"offsetEnclosureTemperature":null,"offsetTemperature":null,"originator":null,"purchasedFrom":null,"purchasedOn":"","remainingLength":"-61236","remainingLengthPercentage":"","remainingPercentage":"99.0","remainingWeight":"99.0","spoolWeight":"","temperature":null,"totalLength":0,"totalWeight":"100.0","updated":"03.08.2021 19:00","usedLength":61236,"usedLengthPercentage":"","usedPercentage":"1.0","usedWeight":"1.0","vendor":"Germany","version":5229}, null, null],"templateSpools":[{"bedTemperature":null,"code":null,"color":"#ee82ee","colorName":"violet","cost":null,"costUnit":"\u20ac","created":"14.09.2021 06:28","databaseId":6,"density":null,"diameter":null,"diameterTolerance":null,"displayName":"Template","enclosureTemperature":null,"firstUse":"","flowRateCompensation":null,"isActive":true,"isTemplate":true,"labels":"[]","lastUse":"","material":"","materialCharacteristic":null,"noteDeltaFormat":"{\"ops\": [{\"insert\": \"\\n\"}]}","noteHtml":"<p><br></p>","noteText":"\n","offsetBedTemperature":null,"offsetEnclosureTemperature":null,"offsetTemperature":null,"originator":null,"purchasedFrom":null,"purchasedOn":"","remainingLength":"0","remainingLengthPercentage":"","remainingPercentage":"100.0","remainingWeight":"100.0","spoolWeight":"","temperature":null,"totalLength":0,"totalWeight":"100.0","updated":"14.09.2021 06:28","usedLength":0,"usedLengthPercentage":"","usedPercentage":"0.0","usedWeight":"0.0","vendor":"","version":2}],"totalItemCount":7}
                    """.trimIndent()
                )
            }
        )
        val target = SpoolManagerApi(rotator, http)
        //endregion
        //region WHEN
        val materials = target.getMaterials(Settings())
        //endregion
        //region THEN
        assertEquals(
            expected = listOf(
                SpoolManagerApi.Material(
                    id = UniqueId("SpoolManager", "3"),
                    displayName = "SM Spätzle",
                    vendor = "Germany",
                    material = "PLA",
                    color = HexColor("#ffff00"),
                    colorName = "yellow",
                    providerDisplayName = "SpoolManager",
                    activeToolIndex = 0,
                    weightGrams = 99.0f,
                    density = 1.24f,
                ), SpoolManagerApi.Material(
                    id = UniqueId("SpoolManager", "1"),
                    displayName = "SM Spaghetti",
                    vendor = "Italy",
                    material = "PLA",
                    color = HexColor("#ff0000"),
                    colorName = "red",
                    providerDisplayName = "SpoolManager",
                    activeToolIndex = null,
                    weightGrams = 90.0f,
                    density = 1.24f,
                ),
                SpoolManagerApi.Material(
                    id = UniqueId("SpoolManager", "2"),
                    displayName = "SM Ramen",
                    vendor = "Japan",
                    material = "PLA",
                    color = HexColor("#ffffff"),
                    colorName = "white",
                    providerDisplayName = "SpoolManager",
                    activeToolIndex = null,
                    weightGrams = 170.0f,
                    density = 1.24f,
                ),
                SpoolManagerApi.Material(
                    id = UniqueId("SpoolManager", "4"),
                    displayName = "SM Ramen",
                    vendor = "Japan",
                    material = "PLA",
                    color = HexColor("#ffa500"),
                    colorName = "orange",
                    providerDisplayName = "SpoolManager",
                    activeToolIndex = null,
                    weightGrams = 1000.0f,
                    density = 1.24f,
                )
            ),
            actual = materials,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_spoolsis_activated_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPut()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/SpoolManager/selectSpool"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"databaseId\":4,\"toolIndex\":1}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )

                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = ""
                )
            }
        )
        val target = SpoolManagerApi(rotator, http)
        //endregion
        //region WHEN
        target.activateMaterial(UniqueId(providerId = OctoPlugins.SpoolManager, "4"), "tool1")
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_checked_if_available_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine {
                throw IllegalStateException("No request expected")
            }
        )
        val target = SpoolManagerApi(rotator, http)
        //endregion
        //region WHEN
        val available = target.isMaterialManagerAvailable(Settings(plugins = Settings.PluginSettingsGroup(spoolManager = Settings.SpoolManager)))
        //endregion
        //region THEN
        assertTrue(
            actual = available,
            message = "Expected available"
        )
        //endregion
    }

    @Test
    fun WHEN_checked_if_available_but_not_available_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine {
                throw IllegalStateException("No request expected")
            }
        )
        val target = SpoolManagerApi(rotator, http)
        //endregion
        //region WHEN
        val available = target.isMaterialManagerAvailable(Settings())
        //endregion
        //region THEN
        assertFalse(
            actual = available,
            message = "Expected not available"
        )
        //endregion
    }
}