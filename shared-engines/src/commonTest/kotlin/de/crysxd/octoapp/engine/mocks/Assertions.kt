package de.crysxd.octoapp.engine.mocks

import io.ktor.client.request.HttpRequestData
import io.ktor.http.HttpMethod
import kotlin.test.assertEquals

fun HttpRequestData.assertGet() = assertEquals(
    expected = HttpMethod.Get,
    actual = method,
    message = "Expected GET"
)

fun HttpRequestData.assertPost() = assertEquals(
    expected = HttpMethod.Post,
    actual = method,
    message = "Expected POST"
)

fun HttpRequestData.assertPut() = assertEquals(
    expected = HttpMethod.Put,
    actual = method,
    message = "Expected PUT"
)

fun HttpRequestData.assertPatch() = assertEquals(
    expected = HttpMethod.Patch,
    actual = method,
    message = "Expected PATCH"
)

fun HttpRequestData.assertHead() = assertEquals(
    expected = HttpMethod.Head,
    actual = method,
    message = "Expected HEAD"
)

fun HttpRequestData.assertDelete() = assertEquals(
    expected = HttpMethod.Delete,
    actual = method,
    message = "Expected DELETE"
)