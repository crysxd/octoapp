package de.crysxd.octoapp.engine.mocks

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileReference

object MockEventSink : EventSink {
    override suspend fun injectInterpolatedEvent(event: (Message.Current?) -> Message.Event) = Unit
    override suspend fun injectInterpolatedPrintStart(file: FileReference.File) = Unit
    override suspend fun injectInterpolatedTemperatureTarget(targets: Map<String, Float>) = Unit
    override suspend fun emitEvent(e: Event) = Unit
}