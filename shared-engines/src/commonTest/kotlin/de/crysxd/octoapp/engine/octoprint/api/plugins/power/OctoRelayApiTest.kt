package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.mocks.TestApiBuilder
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.decodeFromString
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoRelayApiTest {

    @Test
    fun WHEN_settings_are_decoded_THEN_devices_are_present() = runBlocking {
        //region GIVEN
        val json = """
            {
                "plugins": {
                    "octorelay": {
                        "1": {
                            "active": true,
                            "labelText": "First"
                        },
                        "2": {
                            "active": false,
                            "labelText": "Second"
                        },
                        "3": {
                        }
                    }
                }
            }
        """.trimIndent()
        //endregion
        //region WHEN
        val o = EngineJson.decodeFromString<OctoSettings>(json).plugins.octoRelay
        //endregion
        //region THEN
        assertEquals(
            expected = OctoSettings.OctoRelay(
                devices = listOf(
                    OctoSettings.OctoRelay.Device(
                        id = "1",
                        labelText = "First",
                        active = true,
                    ),
                    OctoSettings.OctoRelay.Device(
                        id = "2",
                        labelText = "Second",
                        active = false,
                    ),
                    OctoSettings.OctoRelay.Device(
                        id = "3",
                        labelText = "3",
                        active = false,
                    )
                )
            ),
            actual = o,
            message = "Expected devices to match"
        )
        //endregion
    }

    @Test
    fun WHEN_devices_are_listed_THEN_devices_are_returned() = runBlocking {
        //region GIVEN
        val settings = OctoSettings(
            plugins = OctoSettings.PluginSettingsGroup(
                octoRelay = OctoSettings.OctoRelay(
                    devices = listOf(
                        OctoSettings.OctoRelay.Device(
                            id = "1",
                            labelText = "First",
                            active = true,
                        ),
                        OctoSettings.OctoRelay.Device(
                            id = "2",
                            labelText = "Second",
                            active = false,
                        ),
                    )
                )
            )
        )
        val target = TestApiBuilder(
            MockEngine {
                throw IllegalStateException("No request expected")
            }
        ) { rotator, httpClient ->
            OctoRelayApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val devices = target.getDevices(settings.map(), emptyMap())
        //endregion
        //region THEN
        assertEquals(
            expected = 1,
            actual = devices.size,
            message = "Expected one device"
        )
        assertEquals(
            expected = OctoRelayApi.PowerDevice(owner = target, id = "1", displayName = "First"),
            actual = devices[0],
            message = "Expected device to match"
        )
        //endregion
    }

    @Test
    fun WHEN_device_is_on_and_is_turned_on_THEN_request_is_made() = testSet(
        updateRequest = "////",
        getRequest = "{\"command\":\"getStatus\",\"pin\":\"3\",\"subject\":\"3\"}",
        action = { turnOn() },
        isOnBefore = true,
        expectUpdate = false
    )

    @Test
    fun WHEN_device_is_off_and_is_turned_on_THEN_request_is_made() = testSet(
        updateRequest = "{\"command\":\"update\",\"pin\":\"3\",\"subject\":\"3\"}",
        getRequest = "{\"command\":\"getStatus\",\"pin\":\"3\",\"subject\":\"3\"}",
        action = { turnOn() },
        isOnBefore = false,
        expectUpdate = true
    )

    @Test
    fun WHEN_device_is_on_and_turned_off_THEN_request_is_made() = testSet(
        updateRequest = "{\"command\":\"update\",\"pin\":\"3\",\"subject\":\"3\"}",
        getRequest = "{\"command\":\"getStatus\",\"pin\":\"3\",\"subject\":\"3\"}",
        action = { turnOff() },
        isOnBefore = true,
        expectUpdate = true
    )

    @Test
    fun WHEN_device_is_off_and_turned_off_THEN_request_is_made() = testSet(
        updateRequest = "{\"command\":\"update\",\"pin\":\"3\",\"subject\":\"3\"}",
        getRequest = "{\"command\":\"getStatus\",\"pin\":\"3\",\"subject\":\"3\"}",
        action = { turnOff() },
        isOnBefore = false,
        expectUpdate = false
    )

    @Test
    fun WHEN_device_status_is_checked_and_off_THEN_request_is_made() = testGet(
        response = "{\"status\":false}",
        expectedOn = false,
    )

    @Test
    fun WHEN_device_status_is_checked_and_on_THEN_request_is_made() = testGet(
        response = "{\"status\":true}",
        expectedOn = true
    )

    @Test
    fun WHEN_device_status_is_checked_and_broken_THEN_request_is_made() = testGet(
        response = "{}",
        expectedOn = false
    )

    private fun testGet(response: String, expectedOn: Boolean) = runBlocking {
        //region GIVEN
        val target = TestApiBuilder(
            MockEngine {
                it.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octorelay"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                respond(
                    content = response,
                    headers = headersOf("Content-Type" to listOf("application/json")),
                    status = HttpStatusCode.OK
                )
            }
        ) { rotator, httpClient ->
            OctoRelayApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = OctoRelayApi.PowerDevice(
            owner = target,
            id = "2",
            displayName = "Label"
        )
        //endregion
        //region WHEN
        val isOn = device.isOn()
        //endregion
        //region THEN
        assertEquals(
            expected = expectedOn,
            actual = isOn,
            message = "Expected device to be on: $expectedOn"
        )
        //endregion
    }

    private fun testSet(getRequest: String, updateRequest: String, isOnBefore: Boolean, expectUpdate: Boolean, action: suspend PowerDevice.() -> Unit) = runBlocking {
        //region GIVEN
        var requestMade = false
        val target = TestApiBuilder(
            MockEngine {
                it.assertPost()
                val body = it.body.toByteReadPacket().readText()

                when {
                    body.contains("getStatus") -> {
                        assertEquals(
                            expected = Url("http://gstatic.com/api/plugin/octorelay"),
                            actual = it.url,
                            message = "Expected URL to match"
                        )
                        assertEquals(
                            expected = getRequest,
                            actual = it.body.toByteReadPacket().readText(),
                            message = "Expected body to match"
                        )
                        respond(
                            content = "{\"status\":$isOnBefore}",
                            headers = headersOf("Content-Type" to listOf("application/json")),
                            status = HttpStatusCode.OK
                        )
                    }

                    body.contains("update") -> {
                        assertEquals(
                            expected = Url("http://gstatic.com/api/plugin/octorelay"),
                            actual = it.url,
                            message = "Expected URL to match"
                        )
                        assertEquals(
                            expected = updateRequest,
                            actual = it.body.toByteReadPacket().readText(),
                            message = "Expected body to match"
                        )
                        requestMade = true
                        respondOk()
                    }

                    else -> throw IllegalStateException("Unexpected request: $body")
                }

            }
        ) { rotator, httpClient ->
            OctoRelayApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = OctoRelayApi.PowerDevice(
            owner = target,
            id = "3",
            displayName = "Label"
        )
        //endregion
        //region WHEN
        device.action()
        //endregion
        //region THEN
        assertEquals(
            actual = requestMade,
            expected = expectUpdate,
            message = "Expected request to be made: $expectUpdate"
        )
        //endregion
    }
}