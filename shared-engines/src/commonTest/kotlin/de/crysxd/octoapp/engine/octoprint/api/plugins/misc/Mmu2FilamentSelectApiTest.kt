package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class Mmu2FilamentSelectApiTest {

    @Test
    fun WHEN_initial_message_is_triggered_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/mmu2filamentselect"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"choice\":3,\"command\":\"select\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = Mmu2FilamentSelectApi(rotator, http)
        //endregion
        //region WHEN
        target.selectChoice(choice = 3)
        //endregion
        //region THEN
        //endregion
    }
}