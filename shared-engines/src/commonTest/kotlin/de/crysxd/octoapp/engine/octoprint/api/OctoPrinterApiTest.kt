package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.engine.mock.toByteArray
import io.ktor.client.request.HttpRequestData
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class OctoPrinterApiTest {

    var request: HttpRequestData? = null
    var eventInjected: Boolean = false
    private val rotator = NoopBaseUrlRotator("http://gstatic.com")
    private val eventSink = object : EventSink {
        override suspend fun injectInterpolatedEvent(event: (Message.Current?) -> Message.Event) {
            eventInjected = true
        }

        override suspend fun injectInterpolatedPrintStart(file: FileReference.File) {
            eventInjected = true
        }

        override suspend fun injectInterpolatedTemperatureTarget(targets: Map<String, Float>) {
            eventInjected = true
        }

        override suspend fun emitEvent(e: Event) {
            throw IllegalStateException("Should not be accessed")
        }
    }
    private val http = createOctoPrintHttpClient(
        settings = HttpClientSettings(
            apiKey = "key",
            baseUrlRotator = rotator,
        ),
        engine = MockEngine { r ->
            request = r
            respondOk()
        }
    )

    @BeforeTest
    fun setUp() {
        eventInjected = false
        request = null
    }

    @Test
    fun WHEN_a_printer_state_request_is_made_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = Url("${rotator.activeUrl.value}/api/printer"),
                    actual = request.url,
                    message = "Expected URL to be correct"
                )

                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "sd": {
                                "ready": true
                            },
                            "state": {
                                "error": "",
                                "flags": {
                                    "cancelling": false,
                                    "closedOrError": false,
                                    "error": false,
                                    "finishing": false,
                                    "operational": true,
                                    "paused": false,
                                    "pausing": false,
                                    "printing": false,
                                    "ready": true,
                                    "resuming": false,
                                    "sdReady": true
                                },
                                "text": "Operational"
                            },
                            "temperature": {
                                "bed": {
                                    "actual": 21.3,
                                    "offset": 0,
                                    "target": 0.0
                                },
                                "tool0": {
                                    "actual": 21.3,
                                    "offset": 0,
                                    "target": 0.0
                                }
                            }
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        val connection = target.getPrinterState()
        //endregion
        //region THEN
        assertEquals(
            expected = PrinterState(
                state = PrinterState.State(
                    flags = PrinterState.Flags(
                        cancelling = false,
                        closedOrError = false,
                        error = false,
                        finishing = false,
                        operational = true,
                        pausing = false,
                        paused = false,
                        printing = false,
                        ready = true,
                        sdReady = true,
                        resuming = false,
                    ),
                    text = "Operational",
                ),
                temperature = mapOf(
                    "bed" to ComponentTemperature(actual = 21.3f, target = 0f),
                    "tool0" to ComponentTemperature(actual = 21.3f, target = 0f),
                )
            ),
            actual = connection,
            message = "Expected response to match"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_chamber_target_is_set_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        target.setTemperatureTargets(mapOf("chamber" to 42.5f))
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/chamber"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"target\":42.5,\"command\":\"target\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertTrue(
            message = "Expected event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_chamber_offset_is_set_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        target.setTemperatureOffsets(mapOf("chamber" to 42.5f))
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/chamber"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"offset\":42.5,\"command\":\"offset\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_bed_target_is_set_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        target.setTemperatureTargets(mapOf("bed" to 42.5f))
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/bed"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"target\":42.5,\"command\":\"target\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertTrue(
            message = "Expected event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_bed_offset_is_set_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        target.setTemperatureOffsets(mapOf("bed" to 42.5f))
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/bed"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"offset\":42.5,\"command\":\"offset\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_tool_target_is_set_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        target.setTemperatureTargets(mapOf("tool0" to 42.5f, "tool1" to 133.7f))
        //endregions
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/tool"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"targets\":{\"tool0\":42.5,\"tool1\":133.7},\"command\":\"target\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertTrue(
            message = "Expected event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_tool_offset_is_set_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        target.setTemperatureOffsets(mapOf("tool0" to 42.5f, "tool1" to 133.7f))
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/tool"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"offsets\":{\"tool0\":42.5,\"tool1\":133.7},\"command\":\"offset\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_filament_is_extruded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        //endregion
        //region WHEN
        target.extrudeFilament(length = 42f, speedMmMin = null)
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/tool"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"amount\":42,\"command\":\"extrude\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_x_y_is_homed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        val cmd = PrintHeadCommand.HomeXYAxisPrintHeadCommand
        //endregion
        //region WHEN
        target.executePrintHeadCommand(cmd)
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/printhead"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"axes\":[\"x\",\"y\"],\"command\":\"home\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_z_is_homed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        val cmd = PrintHeadCommand.HomeZAxisPrintHeadCommand
        //endregion
        //region WHEN
        target.executePrintHeadCommand(cmd)
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/printhead"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"axes\":[\"z\"],\"command\":\"home\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_x_y_z_is_homed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        val cmd = PrintHeadCommand.HomeAllAxisPrintHeadCommand
        //endregion
        //region WHEN
        target.executePrintHeadCommand(cmd)
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/printhead"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"axes\":[\"x\",\"y\",\"z\"],\"command\":\"home\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_jog_command_is_send_is_homed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        val cmd = PrintHeadCommand.JogPrintHeadCommand(x = 13.37f, y = 42f, z = 34f, speed = 13)
        //endregion
        //region WHEN
        target.executePrintHeadCommand(cmd)
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/printhead"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"x\":13.37,\"y\":42.0,\"z\":34.0,\"speed\":13,\"command\":\"jog\"}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_gcode_batch_command_is_send_is_homed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        val cmd = GcodeCommand.Batch(listOf("G28", "G29"))
        //endregion
        //region WHEN
        target.executeGcodeCommand(cmd)
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/command"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"commands\":[\"G28\",\"G29\"]}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }

    @Test
    fun WHEN_gcode_single_command_is_send_is_homed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val target = OctoPrinterApi(rotator, http, eventSink)
        val cmd = GcodeCommand.Single("G28")
        //endregion
        //region WHEN
        target.executeGcodeCommand(cmd)
        //endregion
        //region THEN
        val r = request
        assertNotNull(
            actual = r,
            message = "Expected request was made"
        )
        assertEquals(
            expected = Url("${rotator.activeUrl.value}/api/printer/command"),
            actual = r.url,
            message = "Expected URL to be correct"
        )
        assertEquals(
            expected = "{\"commands\":[\"G28\"]}",
            actual = io.ktor.utils.io.core.String(r.body.toByteArray()),
            message = "Expected body to be correct"
        )
        assertFalse(
            message = "Expected no event to be injected",
            actual = eventInjected
        )
        //endregion
    }
}