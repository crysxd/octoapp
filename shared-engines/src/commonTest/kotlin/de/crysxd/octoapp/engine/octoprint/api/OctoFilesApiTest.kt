package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.MockEventSink
import de.crysxd.octoapp.engine.mocks.assertDelete
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.files.FileList
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileObject.File.MetadataGroup
import de.crysxd.octoapp.engine.models.files.FileObject.File.MetadataItem
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import de.crysxd.octoapp.sharedcommon.utils.DefaultDataFormatter
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.engine.mock.toByteArray
import io.ktor.client.request.forms.InputProvider
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLBuilder
import io.ktor.http.appendEncodedPathSegments
import io.ktor.http.content.TextContent
import io.ktor.http.headersOf
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.core.ByteReadPacket
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Instant
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame
import kotlin.test.assertTrue


internal class OctoFilesApiTest {

    @BeforeTest
    fun setUp() {
        DefaultDataFormatter.enableTestingMode("en")
    }

    @Test
    fun WHEN_all_files_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).apply {
                        appendEncodedPathSegments("api", "files", "local")
                        parameters.append("recursive", "true")
                    }.build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                       {
                            "files": [
                                {
                                    "date": 1663496195,
                                    "display": "CE3PRO_100_objects-2.gcode",
                                    "gcodeAnalysis": {
                                        "dimensions": {
                                            "depth": 212.89999999999998,
                                            "height": 1.0,
                                            "width": 212.89999999999998
                                        },
                                        "estimatedPrintTime": 6,
                                        "filament": {
                                            "tool0": {
                                                "length": 16677.632499999847,
                                                "volume": 0.0
                                            }
                                        },
                                        "printingArea": {
                                            "maxX": 223.95,
                                            "maxY": 223.95,
                                            "maxZ": 1.0,
                                            "minX": 11.05,
                                            "minY": 11.05,
                                            "minZ": 0.0
                                        }
                                    },
                                    "hash": "f391ac1eb033b933983100dc2d3556e6f604e366",
                                    "name": "CE3PRO_100_objects-2.gcode",
                                    "origin": "local",
                                    "path": "CE3PRO_100_objects-2.gcode",
                                    "prints": {
                                        "failure": 6,
                                        "last": {
                                            "date": 1663595836.309785,
                                            "success": false
                                        },
                                        "success": 0
                                    },
                                    "refs": {
                                        "download": "http://doggy-daycare:5006/downloads/files/local/CE3PRO_100_objects-2.gcode",
                                        "resource": "http://doggy-daycare:5006/api/files/local/CE3PRO_100_objects-2.gcode"
                                    },
                                    "size": 1865108,
                                    "statistics": {
                                        "averagePrintTime": {},
                                        "lastPrintTime": {}
                                    },
                                    "type": "machinecode",
                                    "typePath": [
                                        "machinecode",
                                        "gcode"
                                    ]
                                },
                                {
                                    "children": [
                                        {
                                            "date": 1664089979,
                                            "display": "Calibration_Cube_Single_Filament_MMU-2.gcode",
                                            "gcodeAnalysis": {
                                                "dimensions": {
                                                    "depth": 120.754,
                                                    "height": 20.0,
                                                    "width": 235.0
                                                },
                                                "estimatedPrintTime": 6,
                                                "filament": {
                                                    "tool0": {
                                                        "length": 1546.8173099999847,
                                                        "volume": 3.7205316403051936
                                                    }
                                                },
                                                "printingArea": {
                                                    "maxX": 240.0,
                                                    "maxY": 117.754,
                                                    "maxZ": 20.0,
                                                    "minX": 5.0,
                                                    "minY": -3.0,
                                                    "minZ": 0.0
                                                }
                                            },
                                            "hash": "faea2cefa05eec72960279319c6d691170dcf909",
                                            "name": "Calibration_Cube_Single_Filament_MMU-2.gcode",
                                            "origin": "local",
                                            "path": "Test/Calibration_Cube_Single_Filament_MMU-2.gcode",
                                            "refs": {
                                                "download": "http://doggy-daycare:5006/downloads/files/local/Test/Calibration_Cube_Single_Filament_MMU-2.gcode",
                                                "resource": "http://doggy-daycare:5006/api/files/local/Test/Calibration_Cube_Single_Filament_MMU-2.gcode"
                                            },
                                            "size": 812895,
                                            "type": "machinecode",
                                            "typePath": [
                                                "machinecode",
                                                "gcode"
                                            ]
                                        }
                                    ],
                                    "display": "Test",
                                    "name": "Test",
                                    "origin": "local",
                                    "path": "Test",
                                    "refs": {
                                        "resource": "http://doggy-daycare:5006/api/files/local/Test"
                                    },
                                    "size": 812895,
                                    "type": "folder",
                                    "typePath": [
                                        "folder"
                                    ]
                                }
                            ],
                            "free": 22801358848,
                            "total": 121018208256
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        val files = target.getAllFiles(origin = FileOrigin.Gcode)
        //endregion
        //region THEN
        assertEquals(
            //region expected = ...
            expected = FileList(
                files = FileObject.Folder(
                    name = "/",
                    path = "/",
                    size = 121018208256,
                    origin = FileOrigin.Gcode,
                    children = listOf(
                        FileObject.File(
                            date = Instant.fromEpochMilliseconds(1663496195000),
                            display = "CE3PRO_100_objects-2.gcode",
                            gcodeAnalysis = FileObject.GcodeAnalysis(
                                dimensions = FileObject.GcodeAnalysis.Dimensions(
                                    depth = 212.9f,
                                    height = 1.0f,
                                    width = 212.9f
                                ),
                                estimatedPrintTime = 6f,
                                filament = mapOf(
                                    "tool0" to FileObject.GcodeAnalysis.FilamentUse(
                                        length = 16677.633f,
                                        volume = 0.0f
                                    )
                                ),
                            ),
                            hash = "f391ac1eb033b933983100dc2d3556e6f604e366",
                            name = "CE3PRO_100_objects-2.gcode",
                            origin = FileOrigin.Gcode,
                            path = "CE3PRO_100_objects-2.gcode",
                            prints = FileObject.PrintHistory(
                                failure = 6,
                                last = FileObject.PrintHistory.LastPrint(
                                    date = Instant.fromEpochMilliseconds(1663595836310),
                                    success = false
                                ),
                                success = 0,
                            ),
                            size = 1865108,
                            type = "machinecode",
                            typePath = listOf(
                                "machinecode",
                                "gcode"
                            ),
                            metadata = listOf(
                                MetadataGroup(
                                    label = "file_manager___file_details___print_info",
                                    id = "file_manager___file_details___print_info",
                                    items = listOf(
                                        MetadataItem(
                                            label = "file_manager___file_details___print_time",
                                            id = "file_manager___file_details___print_time",
                                            value = "less_than_a_minute"
                                        ), MetadataItem(
                                            label = "file_manager___file_details___model_size",
                                            id = "file_manager___file_details___model_size",
                                            value = "212.9 x 212.9 x 1 mm"
                                        ), MetadataItem(
                                            label = "file_manager___file_details___filament_use",
                                            id = "file_manager___file_details___filament_use",
                                            value = "16.68 m / 0 mm³"
                                        )
                                    )
                                ),
                                MetadataGroup(
                                    label = "file_manager___file_details___file",
                                    id = "file_manager___file_details___file",
                                    items = listOf(
                                        MetadataItem(
                                            label = "location",
                                            id = "location",
                                            value = "file_manager___file_details___file_location_local"
                                        ), MetadataItem(
                                            label = "file_manager___file_details___name",
                                            id = "file_manager___file_details___name",
                                            value = "CE3PRO_100_objects-2.gcode"
                                        ), MetadataItem(
                                            label = "file_manager___file_details___path",
                                            id = "file_manager___file_details___path",
                                            value = "/"
                                        ),
                                        MetadataItem(
                                            label = "file_manager___file_details___file_size",
                                            id = "file_manager___file_details___file_size", value = "1.8 MiB"
                                        ),
                                        MetadataItem(
                                            label = "file_manager___file_details___uploaded",
                                            id = "file_manager___file_details___uploaded",
                                            value = "2022-09-18T10:16:35Z",
                                        )
                                    )
                                ),
                                MetadataGroup(
                                    label = "file_manager___file_details___history",
                                    id = "file_manager___file_details___history",
                                    items = listOf(
                                        MetadataItem(
                                            label = "file_manager___file_details___last_print",
                                            id = "file_manager___file_details___last_print",
                                            value = "file_manager___file_details___last_print_at_x_failure"
                                        ),
                                        MetadataItem(
                                            label = "file_manager___file_details___completed",
                                            id = "file_manager___file_details___completed",
                                            value = "x_times"
                                        ),
                                        MetadataItem(
                                            label = "file_manager___file_details___failures",
                                            id = "file_manager___file_details___failures",
                                            value = "x_times"
                                        )
                                    )
                                )
                            )
                        ),
                        FileObject.Folder(
                            name = "Test",
                            path = "Test",
                            origin = FileOrigin.Gcode,
                            display = "Test",
                            size = 812895,
                            children = listOf(
                                FileObject.File(
                                    date = Instant.fromEpochMilliseconds(1664089979000),
                                    display = "Calibration_Cube_Single_Filament_MMU-2.gcode",
                                    gcodeAnalysis = FileObject.GcodeAnalysis(
                                        dimensions = FileObject.GcodeAnalysis.Dimensions(
                                            depth = 120.754f,
                                            height = 20.0f,
                                            width = 235.0f,
                                        ),
                                        estimatedPrintTime = 6f,
                                        filament = mapOf(
                                            "tool0" to FileObject.GcodeAnalysis.FilamentUse(
                                                length = 1546.8173f,
                                                volume = 3.7205317f,
                                            )
                                        ),
                                    ),
                                    hash = "faea2cefa05eec72960279319c6d691170dcf909",
                                    name = "Calibration_Cube_Single_Filament_MMU-2.gcode",
                                    origin = FileOrigin.Gcode,
                                    path = "Test/Calibration_Cube_Single_Filament_MMU-2.gcode",
                                    prints = null,
                                    size = 812895,
                                    type = "machinecode",
                                    typePath = listOf(
                                        "machinecode",
                                        "gcode"
                                    ),
                                    metadata = listOf(
                                        MetadataGroup(
                                            label = "file_manager___file_details___print_info",
                                            id = "file_manager___file_details___print_info",
                                            items = listOf(
                                                MetadataItem(
                                                    label = "file_manager___file_details___print_time",
                                                    id = "file_manager___file_details___print_time",
                                                    value = "less_than_a_minute"
                                                ), MetadataItem(
                                                    label = "file_manager___file_details___model_size",
                                                    id = "file_manager___file_details___model_size",
                                                    value = "235 x 120.8 x 20 mm"
                                                ), MetadataItem(
                                                    label = "file_manager___file_details___filament_use",
                                                    id = "file_manager___file_details___filament_use",
                                                    value = "1.55 m / 3.72 cm³"
                                                )
                                            )
                                        ),
                                        MetadataGroup(
                                            label = "file_manager___file_details___file",
                                            id = "file_manager___file_details___file",
                                            items = listOf(
                                                MetadataItem(
                                                    label = "location",
                                                    id = "location",
                                                    value = "file_manager___file_details___file_location_local"
                                                ), MetadataItem(
                                                    label = "file_manager___file_details___name",
                                                    id = "file_manager___file_details___name",
                                                    value = "Calibration_Cube_Single_Filament_MMU-2.gcode"
                                                ), MetadataItem(
                                                    label = "file_manager___file_details___path",
                                                    id = "file_manager___file_details___path",
                                                    value = "/Test/"
                                                ),
                                                MetadataItem(
                                                    label = "file_manager___file_details___file_size",
                                                    id = "file_manager___file_details___file_size", value = "794 kiB"
                                                ),
                                                MetadataItem(
                                                    label = "file_manager___file_details___uploaded",
                                                    id = "file_manager___file_details___uploaded",
                                                    value = "2022-09-25T07:12:59Z",
                                                )
                                            )
                                        ),
                                        MetadataGroup(
                                            label = "file_manager___file_details___history",
                                            id = "file_manager___file_details___history",
                                            items = listOf(
                                                MetadataItem(
                                                    label = "file_manager___file_details___last_print",
                                                    id = "file_manager___file_details___last_print",
                                                    value = "file_manager___file_details___never"
                                                ),
                                                MetadataItem(
                                                    label = "file_manager___file_details___completed",
                                                    id = "file_manager___file_details___completed",
                                                    value = "file_manager___file_details___never"
                                                ),
                                                MetadataItem(
                                                    label = "file_manager___file_details___failures",
                                                    id = "file_manager___file_details___failures",
                                                    value = "file_manager___file_details___never"
                                                )
                                            )
                                        )
                                    )
                                ),
                            )
                        )
                    )
                ),
                free = 22801358848,
                total = 121018208256,
            ),
            //endregion
            actual = files,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_file_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "api",
                        "files",
                        "local",
                        "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2",
                        "test-%F0%9F%98%8E%20%F0%9F%A4%AF%20%F0%9F%98%8D-%C3%BC-%C3%B1--%23-%25-@.gcode"
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                        {
                            "date": 1651038592,
                            "display": "test-😎 🤯 😍-ü-ñ--#-%-@.gcode",
                            "hash": "858b775eecd72ca3c43f02d3b1bfa5f4bab28ee4",
                            "name": "test-😎 🤯 😍-ü-ñ--#-%-@.gcode",
                            "origin": "local",
                            "path": "test-#-ü-ñ--% 🥲/test-😎 🤯 😍-ü-ñ--#-%-@.gcode",
                            "size": 5182346,
                            "type": "machinecode",
                            "typePath": [
                                "machinecode",
                                "gcode"
                            ]
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        val files = target.getFile(
            origin = FileOrigin.Gcode,
            path = "test-#-ü-ñ--% \uD83E\uDD72/test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D-ü-ñ--#-%-@.gcode",
            isDirectory = false,
        )
        //endregion
        //region THEN
        assertEquals(
            expected = FileObject.File(
                name = "test-😎 🤯 😍-ü-ñ--#-%-@.gcode",
                display = "test-😎 🤯 😍-ü-ñ--#-%-@.gcode",
                typePath = listOf("machinecode", "gcode"),
                type = "machinecode",
                size = 5182346,
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% 🥲/test-😎 🤯 😍-ü-ñ--#-%-@.gcode",
                date = Instant.fromEpochMilliseconds(1651038592000),
                hash = "858b775eecd72ca3c43f02d3b1bfa5f4bab28ee4",
                metadata = listOf(
                    MetadataGroup(
                        label = "file_manager___file_details___file",
                        id = "file_manager___file_details___file",
                        items = listOf(
                            MetadataItem(
                                label = "location",
                                id = "location",
                                value = "file_manager___file_details___file_location_local"
                            ),
                            MetadataItem(
                                label = "file_manager___file_details___name",
                                id = "file_manager___file_details___name",
                                value = "test-😎 🤯 😍-ü-ñ--#-%-@.gcode"
                            ),
                            MetadataItem(
                                label = "file_manager___file_details___path",
                                id = "file_manager___file_details___path",
                                value = "/test-#-ü-ñ--% 🥲/"
                            ), MetadataItem(
                                label = "file_manager___file_details___file_size",
                                id = "file_manager___file_details___file_size", value = "4.9 MiB"
                            ),
                            MetadataItem(
                                label = "file_manager___file_details___uploaded",
                                id = "file_manager___file_details___uploaded",
                                value = "2022-04-27T05:49:52Z"
                            )
                        )
                    ),
                    MetadataGroup(
                        label = "file_manager___file_details___history",
                        id = "file_manager___file_details___history",
                        items = listOf(
                            MetadataItem(
                                label = "file_manager___file_details___last_print",
                                id = "file_manager___file_details___last_print",
                                value = "file_manager___file_details___never"
                            ),
                            MetadataItem(
                                label = "file_manager___file_details___completed",
                                id = "file_manager___file_details___completed",
                                value = "file_manager___file_details___never"
                            ),
                            MetadataItem(
                                label = "file_manager___file_details___failures",
                                id = "file_manager___file_details___failures",
                                value = "file_manager___file_details___never"
                            )
                        )
                    )
                )
            ),
            actual = files,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_file_is_deleted_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertDelete()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "api",
                        "files",
                        "local",
                        "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2",
                        "test-%F0%9F%98%8E%20%F0%9F%A4%AF%20%F0%9F%98%8D-%C3%BC-%C3%B1--%23-%25-@.gcode"
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        target.deleteFile(
            FileObject.File(
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72/test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D-ü-ñ--#-%-@.gcode",
                name = "somehting"
            )
        )
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_folder_is_created_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertTrue(
                    actual = request.body is MultiPartFormDataContent,
                    message = "Expected multipart form"
                )
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "api",
                        "files",
                        "local",
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        target.createFolder(
            parent = FileObject.Folder(
                name = "notused",
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72",
            ),
            name = "test"
        )
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_copy_command_is_executed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "api",
                        "files",
                        "local",
                        "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2",
                        "some.gcode"
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = "{\"command\":\"copy\",\"destination\":\"Test \uD83E\uDD23/test-#-ü-ñ--% \uD83E\uDD72\"}",
                    actual = (request.body as? TextContent)?.text,
                    message = "Expected body to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        target.executeFileCommand(
            file = FileObject.Folder(
                name = "notused",
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72/some.gcode"
            ),
            command = FileCommand.CopyFile(destination = "Test \uD83E\uDD23/test-#-ü-ñ--% \uD83E\uDD72")
        )
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_move_command_is_executed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "api",
                        "files",
                        "local",
                        "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2",
                        "some.gcode"
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = "{\"command\":\"move\",\"destination\":\"Test \uD83E\uDD23/test-#-ü-ñ--% \uD83E\uDD72\"}",
                    actual = (request.body as? TextContent)?.text,
                    message = "Expected body to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        target.executeFileCommand(
            file = FileObject.Folder(
                name = "notused",
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72/some.gcode",
            ),
            command = FileCommand.MoveFile(destination = "Test \uD83E\uDD23/test-#-ü-ñ--% \uD83E\uDD72")
        )
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_select_command_is_executed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "api",
                        "files",
                        "local",
                        "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2",
                        "some.gcode"
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = "{\"command\":\"select\",\"print\":true}",
                    actual = (request.body as? TextContent)?.text,
                    message = "Expected body to match"
                )
                respond(status = HttpStatusCode.NoContent, content = "")
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        target.executeFileCommand(
            file = FileObject.File(
                name = "notused",
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72/some.gcode"
            ),
            command = FileCommand.PrintFile
        )
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_file_is_downloaded_with_no_content_length_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "downloads",
                        "files",
                        "local",
                        "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2",
                        "some.gcode"
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                val bytes = ByteArray(1000 * 1000)
                respond(
                    status = HttpStatusCode.OK,
                    content = bytes
                )
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        val progressUpdates = mutableListOf<Float>()
        target.downloadFile(
            file = FileObject.File(
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72/some.gcode",
                name = "something"
            ),
            progressUpdate = { progressUpdates += it }
        ) {
            it.readIntoNirvana()
        }
        //endregion
        //region THEN
        assertTrue(
            actual = progressUpdates.size > 100,
            message = "Expected at least 100 progress updates: $progressUpdates"
        )
        assertTrue(
            actual = progressUpdates.all { it == -1f },
            message = "Expected all progress updates to be -1: $progressUpdates"
        )
        //endregion
    }

    @Test
    fun WHEN_file_is_downloaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "downloads",
                        "files",
                        "local",
                        "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2",
                        "some.gcode"
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                val bytes = ByteArray(1000 * 1000)
                respond(
                    status = HttpStatusCode.OK,
                    content = bytes,
                    headers = headersOf("Content-Length", "${bytes.size}")
                )
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        val progressUpdates = mutableListOf<Float>()
        val expected = "result"
        val result = target.downloadFile(
            file = FileObject.File(
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72/some.gcode",
                name = "something",
            ),
            progressUpdate = { progressUpdates += it }
        ) {
            it.readIntoNirvana()
            expected
        }
        //endregion
        //region THEN
        assertTrue(
            actual = progressUpdates.size > 100,
            message = "Expected at least 100 progress updates: $progressUpdates"
        )
        assertTrue(
            actual = progressUpdates.all { it in 0f..1f },
            message = "Expected all progress updates to be in 0..1: $progressUpdates"
        )
        assertSame(
            expected = expected,
            actual = result,
            message = "Expected correct result"
        )
        //endregion
    }

    @Test
    fun WHEN_file_is_uploaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments(
                        "api",
                        "files",
                        "local",
                    ).build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                assertTrue(
                    actual = request.body.contentType.toString().startsWith("multipart/form-data; boundary="),
                    message = "Expected multipart form: ${request.body.contentType.toString()}"
                )
                assertTrue(
                    actual = request.body.contentLength == 1000469L,
                    message = "Expected positive content length form: ${request.body.contentLength}"
                )
                // Perform "upload"
                request.body.toByteArray()
                respondOk(content = "")
            }
        )
        val target = OctoFilesApi(rotator, http, MockEventSink)
        //endregion
        //region WHEN
        val progressUpdates = mutableListOf<Float>()
        val bytes = ByteArray(1000 * 1000)
        target.uploadFile(
            parent = FileObject.Folder(
                name = "notused",
                origin = FileOrigin.Gcode,
                path = "test-#-ü-ñ--% \uD83E\uDD72",
            ),
            progressUpdate = { progressUpdates += it },
            name = "test-%23-%C3%BC-%C3%B1--%25%20%F0%9F%A5%B2.gcode",
            input = InputProvider(bytes.size.toLong()) { ByteReadPacket(bytes) }
        )
        //endregion
        //region THEN
        assertTrue(
            actual = progressUpdates.size > 100,
            message = "Expected at least 3 progress updates: $progressUpdates"
        )
        assertTrue(
            actual = progressUpdates.all { it in 0f..1f },
            message = "Expected all progress updates to be in 0..1: $progressUpdates"
        )
        //endregion
    }

    private suspend fun ByteReadChannel.readIntoNirvana() {
        do {
            awaitContent()
            while (availableForRead > 0) {
                readByte()
            }
        } while (!isClosedForRead)
    }
}
