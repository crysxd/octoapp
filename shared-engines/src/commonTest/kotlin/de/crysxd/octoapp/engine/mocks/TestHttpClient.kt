package de.crysxd.octoapp.engine.mocks

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.http.Url

fun <T> TestApiBuilder(engine: HttpClientEngine?, builder: (BaseUrlRotator, HttpClient) -> T): T {
    val rotator = NoopBaseUrlRotator(baseUrl = Url("http://gstatic.com"))
    return builder(
        rotator,
        createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.HttpClientSettings(apiKey = "dfds", baseUrlRotator = rotator),
            engine = engine,
        )
    )
}