package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.framework.ClassicBaseUrlRotator
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngineBuilder
import de.crysxd.octoapp.engine.moonraker.http.createMoonrakerHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class MoonrakerProbeApiTest {

    @Test
    fun WHEN_the_connection_is_probed_with_moonraker_THEN_success_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ClassicBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createMoonrakerHttpClient(
            settings = MoonrakerEngineBuilder.HttpClientSettings(
                baseUrlRotator = rotator,
                apiKey = null,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = Url("${rotator.activeUrl.value}/server/info"),
                    actual = request.url,
                    message = "Expected URL to be correct"
                )

                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                       {
                        "result": {
                            "klippy_connected": true,
                            "klippy_state": "ready",
                            "components": [
                                "klippy_connection",
                                "application",
                                "websockets",
                                "internal_transport",
                                "dbus_manager",
                                "database",
                                "file_manager",
                                "klippy_apis",
                                "secrets",
                                "template",
                                "shell_command",
                                "machine",
                                "data_store",
                                "proc_stats",
                                "job_state",
                                "job_queue",
                                "http_client",
                                "announcements",
                                "webcam",
                                "extensions",
                                "update_manager",
                                "history",
                                "octoprint_compat",
                                "authorization"
                            ],
                            "failed_components": [],
                            "registered_directories": [
                                "config",
                                "logs",
                                "gcodes",
                                "config_examples",
                                "docs"
                            ],
                            "warnings": [],
                            "websocket_count": 1,
                            "moonraker_version": "v0.8.0-83-g5d8422e",
                            "missing_klippy_requirements": [],
                            "api_version": [
                                1,
                                2,
                                1
                            ],
                            "api_version_string": "1.2.1"
                        }
                    }
                    """.trimIndent(),
                )
            }
        )
        val target = MoonrakerProbeApi(rotator, http)
        //endregion
        //region WHEN
        val success = target.probe()
        //endregion
        //region THEN
        assertTrue(success, "Expected success")
        //endregion
    }

    @Test
    fun WHEN_the_connection_is_probed_with_something_else_THEN_failure_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ClassicBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createMoonrakerHttpClient(
            settings = MoonrakerEngineBuilder.HttpClientSettings(
                baseUrlRotator = rotator,
                apiKey = null,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = Url("${rotator.activeUrl.value}/server/info"),
                    actual = request.url,
                    message = "Expected URL to be correct"
                )

                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                       {
                          "success": true
                       }
                    """.trimIndent(),
                )
            }
        )
        val target = MoonrakerProbeApi(rotator, http)
        //endregion
        //region WHEN
        val success = target.probe()
        //endregion
        //region THEN
        assertFalse(success, "Expected success")
        //endregion
    }

    @Test
    fun WHEN_the_connection_is_probed_with_404_THEN_failure_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ClassicBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createMoonrakerHttpClient(
            settings = MoonrakerEngineBuilder.HttpClientSettings(
                baseUrlRotator = rotator,
                apiKey = null,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = Url("${rotator.activeUrl.value}/server/info"),
                    actual = request.url,
                    message = "Expected URL to be correct"
                )

                respond(
                    status = HttpStatusCode.NotFound,
                    headers = headersOf("Content-Type", "application/json"),
                    content = "",
                )
            }
        )
        val target = MoonrakerProbeApi(rotator, http)
        //endregion
        //region WHEN
        val success = target.probe()
        //endregion
        //region THEN
        assertFalse(success, "Expected success")
        //endregion
    }
}