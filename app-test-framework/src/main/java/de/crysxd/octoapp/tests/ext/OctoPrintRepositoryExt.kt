package de.crysxd.octoapp.tests.ext

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository

fun PrinterConfigurationRepository.setActive(instanceInformationV3: PrinterConfigurationV3) =
    setActive(instanceInformationV3, trigger = "TestFramework")