package de.crysxd.octoapp.tests.utils

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.ConnectionCommand
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.github.aakira.napier.Napier
import io.ktor.client.request.post
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.util.concurrent.TimeoutException

object VirtualPrinterUtils {
    private val provider get() = BaseInjector.get().octoPrintProvider()

    fun PrinterConfigurationV3.setVirtualPrinterEnabled(enabled: Boolean) = runBlocking {
        Napier.i("Setting virtual printer enabled=$enabled")
        BaseInjector.get().octoPrintProvider()
            .createAdHocPrinter(this@setVirtualPrinterEnabled, logTag = "VirtualPrinterUtils")
            .genericRequest { baseUrl, httpClient ->
                httpClient.post {
                    urlFromPath(baseUrl = baseUrl, "api", "settings")
                    setJsonBody("{  \"plugins\": {   \"virtual_printer\": {     \"enabled\": $enabled } }}")
                }
            }
    }

    fun PrinterConfigurationV3.connectPrinter() = runBlocking {
        Napier.i("Connecting printer to $webUrl")
        val octoprint = provider.createAdHocPrinter(this@connectPrinter, logTag = "VirtualPrinterUtils").asOctoPrint()
        octoprint.connectionApi.executeConnectionCommand(ConnectionCommand.Connect(port = "VIRTUAL"))
        val end = System.currentTimeMillis() + 30_000
        do {
            delay(500)
            if (System.currentTimeMillis() > end) {
                throw TimeoutException("Unable to connect printer within 30s")
            }
        } while (octoprint.connectionApi.getConnection().current.port == null)
        Napier.i("Connected printer to $webUrl")
    }

    fun PrinterConfigurationV3.disconnectPrinter() = runBlocking {
        Napier.i("Disconnecting printer from $webUrl")
        val octoprint = provider.createAdHocPrinter(this@disconnectPrinter, logTag = "VirtualPrinterUtils").asOctoPrint()
        octoprint.connectionApi.executeConnectionCommand(ConnectionCommand.Disconnect)

        val end = System.currentTimeMillis() + 30_000
        do {
            delay(500)
            if (System.currentTimeMillis() > end) {
                throw TimeoutException("Unable to disconnect printer within 30s")
            }
        } while (octoprint.connectionApi.getConnection().current.port != null)
        Napier.i("Disconnected printer from $webUrl")
    }
}