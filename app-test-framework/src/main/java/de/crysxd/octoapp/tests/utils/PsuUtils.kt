package de.crysxd.octoapp.tests.utils

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.coroutines.runBlocking

object PsuUtils {
    fun PrinterConfigurationV3.turnAllOff() = runBlocking {
        val octoPrint = BaseInjector.get().octoPrintProvider().createAdHocPrinter(this@turnAllOff, logTag = "PsuUtils")
        val settings = octoPrint.settingsApi.getSettings()
        octoPrint.powerDevicesApi.getDevices(settings, emptyMap()).forEach { it.turnOff() }
    }
}