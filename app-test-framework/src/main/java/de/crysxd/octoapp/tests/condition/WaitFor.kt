package de.crysxd.octoapp.tests.condition

import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

fun waitFor(description: String, timeout: Duration = 15.seconds, condition: () -> Boolean) {
    val instruction = WaitInstructions(description, condition)
    ConditionWatcher.waitForCondition(instruction, timeout.inWholeMilliseconds.toInt())
}

fun waitForPass(description: String, timeout: Duration = 15.seconds, condition: () -> Unit) {
    var lastException: Throwable? = null
    val instruction = WaitInstructions(description) {
        try {
            condition()
            true
        } catch (e: Throwable) {
            lastException = e
            false
        }
    }
    try {
        ConditionWatcher.waitForCondition(instruction, timeout.inWholeMilliseconds.toInt())
    } catch (e: Exception) {
        throw Exception(lastException?.message ?: e.message, lastException)
    }
}

private class WaitInstructions(val desc: String, val condition: () -> Boolean) : Instruction() {
    override fun getDescription() = desc
    override fun checkCondition() = condition()
}