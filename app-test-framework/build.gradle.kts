import de.crysxd.octoapp.buildscript.localProperties
import de.crysxd.octoapp.buildscript.octoAppAndroidLibrary

plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinAndroid)
}

octoAppAndroidLibrary(
    name = "tests",
    usesCompose = false,
    usesDagger = false,
    usesTestFramework = false,
) {
    defaultConfig {
        buildConfigField("String", "TEST_ENV_DOMAIN", "\"${localProperties.getProperty("integrationTests.testEnvDoamin", "100.67.24.114")}\"")
        buildConfigField("String", "TEST_OCTOEVERYWHERE_PASSWORD", "\"${localProperties.getProperty("integrationTests.octoEevrywherePassword") ?: ""}\"")
        buildConfigField("String", "TEST_OCTOEVERYWHERE_USER", "\"${localProperties.getProperty("integrationTests.octoEverywhereUser", "")}\"")
        buildConfigField("String", "TEST_OCTOEVERYWHERE_URL", "\"${localProperties.getProperty("integrationTests.octoEverywhereUrl", "")}\"")
    }
}

dependencies {
    implementation(projects.base)
    implementation(projects.sharedBase)
    implementation(projects.sharedEngines)

    // For OctoEverywhere tests
    implementation(libs.removesoon.retrofit)
    implementation(libs.removesoon.retrofit.converter)

    api(libs.test.junit)
    api(libs.test.truth)
    api(libs.test.mockito)
    api(libs.test.core)
    api(libs.test.espressoCore)
    api(libs.test.androidxJunit)
    api(libs.test.dexmakerMockitoInline)
    api(libs.test.runner)
    api(libs.test.rules)
    api(libs.test.uiautomator)
    api(libs.test.barista) {
        exclude(group = "org.jetbrains.kotlin")
        exclude(group = "org.checkerframework")
    }
}