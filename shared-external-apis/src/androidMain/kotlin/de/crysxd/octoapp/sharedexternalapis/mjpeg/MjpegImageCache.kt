package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.sharedcommon.exceptions.ImageByteCacheOverflowException
import io.github.aakira.napier.Napier


internal class MjpegImageCache(maxImageSize: Int?, private val logTag: String) {
    private val maxSize = 1024 * 1024 * 10L
    private val array = ByteArrayOutputStream2()
    private val buffer = ByteArrayOutputStream2()
    private val decoder = JpegCoder(usePool = true, logTag = logTag, maxImageSize = maxImageSize)
    private var sameOffsetCounter = 0

    fun reset() {
        array.reset()
    }

    fun push(bytes: ByteArray, length: Int) {
        if ((array.size() + length) > maxSize) {
            throw ImageByteCacheOverflowException(array.toByteArray().copyOf())
        }
        array.write(bytes, 0, length)
    }

    suspend fun readImage(length: Int, boundaryLength: Int): Image? {
        val bitmap = if (length > 10) {
            try {
                decoder.decode(array.toByteArray(), length)
            } catch (e: Exception) {
                Napier.w(tag = logTag, message = "Failed to decode frame: $e")
                null
            }
        } else {
            null
        }
        dropUntil(boundaryLength)
        return bitmap
    }

    private fun dropUntil(until: Int) {
        val length = array.size() - until
        if (length >= 0) {
            buffer.reset()
            buffer.write(array.toByteArray(), until, array.size() - until)
            array.reset()
            array.write(buffer.toByteArray(), 0, buffer.size())
        } else {
            // This shouldn't happen, length is negative. This indicates an issue in index search...resetting the array
            // will cause next frame to fail to decode but after that we should be back on track
            array.reset()
        }
    }

    fun indexOf(offset: Int, boundaryStart: String): IndexResult {
        // Search start
        var startIndex = 0
        var startFound = false
        val length = array.size()
        val array = array.toByteArray()
        for (i in offset until length) {
            if (i < 0 || startIndex < 0) {
                throw IllegalStateException("Index is negative: i=$i startIndex=$startIndex offset=$offset boundaryStart=$boundaryStart length=$length")
            }

            if (array[i].toInt().toChar() == boundaryStart[startIndex]) {
                // Continued streak of the match
                startIndex++
            } else if (array[i].toInt().toChar() == boundaryStart[0]) {
                // Streak is broken but the current char is matching the start, so let's start over
                startIndex = 1
            } else {
                // Streak is broken but also no match for start, reset
                startIndex = 0
            }

            if (startIndex == boundaryStart.length) {
                startFound = true
                startIndex = i - boundaryStart.length + 1
                break
            }
        }

        if (!startFound) {
            val nextOffset = length - boundaryStart.length
            return if (sameOffsetCounter > 10) {
                throw IllegalStateException("Returned same offset 10 times: length=${array.size} offset=${offset} boundaryStart=${boundaryStart}")
            } else if (nextOffset < 0) {
                Napier.w(tag = logTag, message = "Available data less than boundary, returning same offset")
                sameOffsetCounter++
                IndexResult(nextOffset = offset)
            } else {
                sameOffsetCounter = 0
                IndexResult(nextOffset = length - boundaryStart.length)
            }
        } else {
            // Reset counter, we found something
            sameOffsetCounter = 0
        }

        var endIndex = -1
        // Search end
        for (i in startIndex until length) {
            val c1 = array.getOrNull(i + 0)?.toInt()?.toChar()
            val c2 = array.getOrNull(i + 1)?.toInt()?.toChar()
            val c3 = array.getOrNull(i + 2)?.toInt()?.toChar()
            val c4 = array.getOrNull(i + 3)?.toInt()?.toChar()
            if (c1 == '\n' && c2 == '\n' && i < length - 2) {
                endIndex = i + 2
                break
            }
            if (c1 == '\r' && c2 == '\n' && c3 == '\r' && c4 == '\n' && i < length - 4) {
                endIndex = i + 4
                break
            }
        }

        if (startIndex < 0) {
            throw IllegalStateException("Start offset can't be negative: length=$length boundaryStart=$boundaryStart startIndex=$startIndex endIndex=$endIndex")
        }

        return if (endIndex < 0) {
            IndexResult(nextOffset = startIndex)
        } else {
            IndexResult(start = startIndex, end = endIndex, nextOffset = startIndex)
        }
    }

    data class IndexResult(
        val start: Int? = null,
        val end: Int? = null,
        val nextOffset: Int,
    )
}