package de.crysxd.octoapp.sharedexternalapis.mjpeg

expect class JpegCoder(
    usePool: Boolean,
    logTag: String,
    maxImageSize: Int?,
) {

    suspend fun decode(byteArray: ByteArray, length: Int): Image
    suspend fun encode(image: Image, quality: Float = 0.7f): ByteArray
    fun reset()

}