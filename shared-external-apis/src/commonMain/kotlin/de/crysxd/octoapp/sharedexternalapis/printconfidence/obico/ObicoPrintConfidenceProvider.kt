package de.crysxd.octoapp.sharedexternalapis.printconfidence.obico

import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidenceService

class ObicoPrintConfidenceProvider {

    fun create(
        confidence: suspend () -> Float,
        shouldRetry: (Throwable) -> Boolean,
    ): PrintConfidenceService = ObicoPrintConfidenceService(
        confidence = confidence,
        shouldRetry = shouldRetry,
    )
}