package de.crysxd.octoapp.sharedexternalapis.printconfidence

data class PrintConfidence(
    val description: String,
    val confidence: Float?,
    val origin: Origin,
    val level: Level,
) {
    override fun equals(other: Any?) = other is PrintConfidence &&
            other.description == description &&
            other.origin == origin &&
            other.level == level

    override fun hashCode() = "$description$origin$level".hashCode()

    enum class Origin {
        OctoEverywhere, Obico
    }

    enum class Level {
        High, Medium, Low, Unknown
    }
}