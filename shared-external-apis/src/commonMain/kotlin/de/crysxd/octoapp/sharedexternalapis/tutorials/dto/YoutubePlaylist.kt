package de.crysxd.octoapp.sharedexternalapis.tutorials.dto

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
internal data class YoutubePlaylist(
    val items: List<PlaylistItem> = emptyList()
) {
    @Serializable
    data class PlaylistItem(
        val snippet: Snippet? = null,
        val contentDetails: ContentDetails?,
    ) {

        @Serializable
        data class Snippet(
            val title: String? = null,
            val description: String? = null,
            val thumbnails: Map<String, Thumbnail>?,
        )

        @Serializable
        data class Thumbnail(
            val url: String? = null,
            val width: Int,
            val height: Int,
        )

        @Serializable
        data class ContentDetails(
            val videoId: String? = null,
            val videoPublishedAt: Instant? = null,
        )
    }
}