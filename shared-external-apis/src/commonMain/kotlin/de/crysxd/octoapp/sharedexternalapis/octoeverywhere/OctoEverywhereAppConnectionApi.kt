package de.crysxd.octoapp.sharedexternalapis.octoeverywhere

import de.crysxd.octoapp.sharedexternalapis.http.ExternalApiHttpClient
import de.crysxd.octoapp.sharedexternalapis.octoeverywhere.dto.OctoEverywhereConnectionInfoResponse
import de.crysxd.octoapp.sharedexternalapis.octoeverywhere.dto.OctoEverywhereCreateLiveLinkRequest
import de.crysxd.octoapp.sharedexternalapis.octoeverywhere.dto.OctoEverywhereCreateLiveLinkResponse
import de.crysxd.octoapp.sharedexternalapis.octoeverywhere.dto.OctoEverywherePrintConfidenceResponse
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere.OctoEverywhereDisconnectedException
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ResponseException
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import kotlin.time.Duration

class OctoEverywhereAppConnectionApi(
    private val appToken: String,
    private val httpClient: HttpClient = ExternalApiHttpClient()
) {
    suspend fun createLiveLink(
        validity: Duration
    ) = httpClient.post("https://octoeverywhere.com/api/live/createfromappconnection") {
        configure()
        setBody(OctoEverywhereCreateLiveLinkRequest(appToken = appToken, expirationHours = validity.inWholeHours.toInt().coerceAtLeast(1)))
    }.body<OctoEverywhereCreateLiveLinkResponse>().let {
        it.result.shortUrl ?: it.result.url
    }

    suspend fun getWebUrl() = httpClient.get("https://octoeverywhere.com/api/appconnection/info") {
        configure(hasBody = false)
    }.body<OctoEverywhereConnectionInfoResponse>().result.userFacingPrinterUrl

    suspend fun checkConfidence() = try {
        httpClient.post("https://octoeverywhere.com/api/gadget/GetStatusFromAppConnection") {
            configure()
        }.body<OctoEverywherePrintConfidenceResponse>().result.let {
            PrintConfidence(
                description = when (it.state) {
                    0, 1 -> throw return@let null
                    else -> it.status ?: "Unknown"
                },
                confidence = it.rating?.let { r -> r / 10f },
                origin = PrintConfidence.Origin.OctoEverywhere,
                level = when {
                    // Rely on color
                    it.statusColor == "g" -> PrintConfidence.Level.High
                    it.statusColor == "y" -> PrintConfidence.Level.Medium
                    it.statusColor == "r" -> PrintConfidence.Level.Low

                    // Fallback with confidence
                    (it.rating ?: 0) > 7 -> PrintConfidence.Level.High
                    (it.rating ?: 0) > 4 -> PrintConfidence.Level.Medium
                    else -> PrintConfidence.Level.High
                }
            )
        }
    } catch (e: ResponseException) {
        when (e.response.status.value) {
            // Temporary Error - PrinterOffline
            // Temporary Error - PrinterConnectionTimeOut
            601,
            602 -> PrintConfidence(
                description = "Can't see your print",
                confidence = null,
                origin = PrintConfidence.Origin.OctoEverywhere,
                level = PrintConfidence.Level.Low
            )
            // App Connection Not Found
            // App Connection Expired or Revoked
            603,
            604 -> throw OctoEverywhereDisconnectedException()
            // PluginUpdateRequired
            610 -> PrintConfidence(
                description = "OctoEverywhere plugin outdated",
                confidence = null,
                origin = PrintConfidence.Origin.OctoEverywhere,
                level = PrintConfidence.Level.Low
            )
            // Not part of Gadget beta
            611 -> throw OctoEverywhereDisconnectedException()
            else -> null
        }
    }

    private fun HttpRequestBuilder.configure(hasBody: Boolean = true) {
        header("AppToken", appToken)
        if (hasBody) {
            setBody("{}")
            header("Content-Type", "application/json")
        }
    }
}