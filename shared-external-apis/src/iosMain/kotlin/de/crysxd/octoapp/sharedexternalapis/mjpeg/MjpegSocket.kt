package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.sharedcommon.NSErrorException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.http.HttpExceptionGenerator
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3.MjpegSnapshot.Frame
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import platform.Foundation.NSError


interface MjpegSocketCore {

    companion object {
        lateinit var factory: () -> MjpegSocketCore
    }

    suspend fun startSession(
        url: String,
        extraHeaders: Map<String, String>,
        onFrame: (Frame) -> Unit,
        onCompletion: (Any?) -> Unit,
    )

    fun stopSession()
}

internal actual class MjpegSocket actual constructor(
    maxImageSize: Int?,
    private val url: Url,
    private val httpClientSettings: HttpClientSettings,
    private val logTag: String,
) {

    private val tag = "MjpegSocket"

    actual fun connect(): Flow<MjpegConnection3.MjpegSnapshot> {
        var core: MjpegSocketCore? = null
        Napier.i(tag = tag, message = "Using url $url")
        val generator = HttpExceptionGenerator()

        return flow {
            val internalFlow = MutableStateFlow<Any>(Unit)
            val closeSignal = "close"

            core = MjpegSocketCore.factory().apply {
                Napier.i(tag = logTag, message = "Starting session, connecting to $url")
                startSession(
                    url = url.withoutBasicAuth().toString(),
                    extraHeaders = httpClientSettings.extraHeaders,
                    onFrame = { internalFlow.value = it },
                    onCompletion = { internalFlow.value = it ?: closeSignal },
                )
            }

            val mappedFlow = internalFlow.mapNotNull {
                when (it) {
                    is PrinterApiException -> {
                        // Let generator have a crack at it. If this is e.g. a OctoEverywhere 6xx error we will get a specific error out of it
                        generator.handleResponseCode(code = it.responseCode, url = it.webUrl, bodyAsText = { it.body }, headers = emptyMap())
                        throw it
                    }

                    is Frame -> it
                    is Throwable -> throw it
                    is NSError -> throw MjpegSocketIOException(it)
                    closeSignal -> throw CancellationException("Connection closed")
                    Unit -> null
                    else -> throw UnsupportedOperationException("Can't handle $it")
                }
            }

            Napier.v(tag = logTag, message = "Emitting all")
            emitAll(mappedFlow)
        }.onCompletion {
            Napier.i(tag = logTag, message = "Closing session")
            core?.stopSession()
        }.catch {
            generator.rethrowException(exception = it, url = url)
        }
    }
}

class MjpegSocketIOException(error: NSError) : NetworkException(
    userFacingMessage = error.localizedDescription,
    webUrl = error.failingUrl ?: Url("http://error"),
    originalCause = NSErrorException(error, "${error.description} (${error.failingUrl ?: error.userInfo})"),
    technicalMessage = "${error.localizedDescription}: ${error.failingUrl ?: error.userInfo}"
)

private val NSError.failingUrl get() = (userInfo["NSErrorFailingURLStringKey"] as? String)?.toUrlOrNull()