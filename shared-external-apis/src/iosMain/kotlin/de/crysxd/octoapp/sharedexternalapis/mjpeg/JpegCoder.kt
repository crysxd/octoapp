package de.crysxd.octoapp.sharedexternalapis.mjpeg

import kotlinx.cinterop.addressOf
import kotlinx.cinterop.convert
import kotlinx.cinterop.usePinned
import platform.Foundation.NSData
import platform.Foundation.create
import platform.UIKit.UIImage
import platform.UIKit.UIImageJPEGRepresentation
import platform.posix.memcpy

@OptIn(kotlinx.cinterop.ExperimentalForeignApi::class, kotlinx.cinterop.BetaInteropApi::class)
actual class JpegCoder actual constructor(
    usePool: Boolean,
    logTag: String,
    maxImageSize: Int?,
) {

    actual suspend fun decode(byteArray: ByteArray, length: Int): Image =
        UIImage(data = byteArray.toNSData())

    actual suspend fun encode(image: Image, quality: Float): ByteArray =
        requireNotNull(UIImageJPEGRepresentation(image, quality.toDouble())) { "JPEG encode failed" }.toByteArray()

    private fun ByteArray.toNSData(): NSData = usePinned { array ->
        NSData.create(bytes = array.addressOf(0), length = this.size.convert())
    }

    private fun NSData.toByteArray(): ByteArray = ByteArray(this@toByteArray.length.toInt()).apply {
        usePinned {
            memcpy(it.addressOf(0), this@toByteArray.bytes, this@toByteArray.length)
        }
    }

    actual fun reset() = Unit
}