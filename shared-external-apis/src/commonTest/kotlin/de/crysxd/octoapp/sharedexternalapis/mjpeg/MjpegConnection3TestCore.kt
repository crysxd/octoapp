package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail
import kotlin.time.Duration.Companion.seconds


abstract class MjpegConnection3TestCore {

    private lateinit var server: MockJpegServer
    protected abstract val settings: HttpClientSettings

    @BeforeTest
    fun setUp() {
        Napier.base(DebugAntilog())
    }

    @AfterTest
    fun tearDown() {
        if (mjpegTestsEnabled) {
            Napier.takeLogarithm()
            server.stop()
        }
    }

    @Test
    fun WHEN_images_are_loaded_THEN_images_are_emitted() = runBlocking {
        if (!mjpegTestsEnabled) return@runBlocking

        //region GIVEN
        server = MockJpegServer(useBasicAuth = false).also { it.start() }
        //endregion
        //region WHEN
        val connection = MjpegConnection3(
            streamUrl = server.url(MockJpegServer.Mode.Mjpeg),
            name = "Test",
            maxSize = null,
            httpSettings = settings,
            throwExceptions = true
        )
        //endregion
        //region THEN
        // Collect 10 frames in 2s to pass
        withTimeout(2.seconds) {
            connection.load().filter { it is MjpegConnection3.MjpegSnapshot.Frame }.take(10).collect()
        }
        //endregion
    }

    @Test
    fun WHEN_basic_auth_used_THEN_images_are_emitted() = runBlocking {
        if (!mjpegTestsEnabled) return@runBlocking
        //region GIVEN
        server = MockJpegServer(useBasicAuth = true).also { it.start() }
        //endregion
        //region WHEN
        val connection = MjpegConnection3(
            streamUrl = server.url(MockJpegServer.Mode.Mjpeg),
            name = "Test",
            maxSize = null,
            httpSettings = settings,
            throwExceptions = true
        )
        //endregion
        //region THEN
        // Collect 10 frames in 2s to pass
        withTimeout(2.seconds) {
            connection.load().filter { it is MjpegConnection3.MjpegSnapshot.Frame }.take(10).collect()
        }
        //endregion
    }

    @Test
    fun WHEN_basic_auth_required_THEN_exception_thrown() = runBlocking {
        if (!mjpegTestsEnabled) return@runBlocking
        //region GIVEN
        server = MockJpegServer(useBasicAuth = true).also { it.start() }
        //endregion
        //region WHEN
        val connection = MjpegConnection3(
            streamUrl = server.url(MockJpegServer.Mode.Mjpeg).withoutBasicAuth(),
            name = "Test",
            maxSize = null,
            httpSettings = settings,
            throwExceptions = true
        )
        //endregion
        //region THEN
        try {
            connection.load().filter { it is MjpegConnection3.MjpegSnapshot.Frame }.take(10).collect()
            fail("Expected exception")
        } catch (e: BasicAuthRequiredException) {
            Napier.e(throwable = e, message = "Expected exception")
            assertEquals(
                expected = server.realm,
                actual = e.userRealm,
                message = "Expected realm to match"
            )
        } catch (e: Exception) {
            fail("Expected ${PrinterApiException::class.qualifiedName}")
        }
        //endregion
    }

    @Test
    fun WHEN_unexpected_code_is_returned_THEN_exception_thrown() = runBlocking {
        if (!mjpegTestsEnabled) return@runBlocking
        //region GIVEN
        server = MockJpegServer(useBasicAuth = true).also { it.start() }
        //endregion
        //region WHEN
        val connection = MjpegConnection3(
            streamUrl = server.url(MockJpegServer.Mode.InternalServerError),
            name = "Test",
            maxSize = null,
            httpSettings = settings,
            throwExceptions = true
        )
        //endregion
        //region THEN
        try {
            connection.load().filter { it is MjpegConnection3.MjpegSnapshot.Frame }.take(10).collect()
            fail("Expected exception")
        } catch (e: PrinterApiException) {
            Napier.e(throwable = e, message = "Expected exception")
            assertEquals(
                expected = 500,
                actual = e.responseCode,
                message = "Expected response code to match",
            )
        } catch (e: Exception) {
            fail("Expected ${PrinterApiException::class.qualifiedName}")
        }
        //endregion
    }
}