package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MockJpegServer.Mode.InternalServerError
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MockJpegServer.Mode.Mjpeg
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MockJpegServer.Mode.MjpegInvalid
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MockJpegServer.Mode.Single
import io.github.aakira.napier.Napier
import io.ktor.http.ContentType
import io.ktor.http.HeaderValueParam
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLProtocol
import io.ktor.http.appendEncodedPathSegments
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.server.cio.CIO
import io.ktor.server.engine.embeddedServer
import io.ktor.server.request.uri
import io.ktor.server.response.respond
import io.ktor.server.response.respondBytesWriter
import io.ktor.server.response.respondText
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import io.ktor.server.util.url
import io.ktor.util.encodeBase64
import io.ktor.util.pipeline.PipelineContext
import io.ktor.utils.io.charsets.Charsets
import io.ktor.utils.io.core.toByteArray
import io.ktor.utils.io.writeAvailable
import io.ktor.utils.io.writeStringUtf8
import kotlinx.coroutines.delay

class MockJpegServer(private val useBasicAuth: Boolean) {

    companion object {
        private var serverPort = (10000..15000).random()
    }

    private val myPort = serverPort++
    private val BOUNDARY = "BOUNDARYDONOTCROSS"
    private val NL = "\r\n"
    private val tag = "MockJpegServer"
    private val user = "someuser"
    private val password = "somepass"
    private val param = "something"
    private val paramInvalid = "invalid"
    private val paramValue = "value"
    val realm = "Mock MJPEG server"
    val serverErrorBody = "Server error"

    private val server = embeddedServer(CIO, port = myPort) {
        routing {
            get("/error") {
                call.respondText(contentType = ContentType.Text.Html, status = HttpStatusCode.InternalServerError) { serverErrorBody }
            }

            get("/stream") {
                Napier.i(tag = tag, message = "Accepting request to ${call.request.uri}")
                if (!checkAuthorization()) return@get
                if (!checkParam()) return@get


                val invalid = call.request.queryParameters.contains(paramInvalid)
                val type = ContentType("multipart", "x-mixed-replace", parameters = listOf(HeaderValueParam("boundary", BOUNDARY)))
                val size = if (invalid) SampleJpeg.size / 2 else SampleJpeg.size

                var frameCounter = 0
                call.respondBytesWriter(type) {
                    while (true) {
                        writeStringUtf8("$NL--$BOUNDARY$NL")
                        writeStringUtf8("Content-Type: image/jpeg$NL")
                        writeStringUtf8("Content-Length: ${size}$NL$NL")
                        writeFully(SampleJpeg, 0, size)
                        flush()
                        if (frameCounter % 100 == 0) {
                            Napier.i(tag = tag, message = "Sent $frameCounter frames")
                        }
                        frameCounter++

                        // 20 FPS
                        delay(50)
                    }
                }
            }

            get("/snapshot") {
                Napier.i(tag = tag, message = "Accepting request to ${call.request.uri}")
                if (!checkAuthorization()) return@get
                if (!checkParam()) return@get

                call.respondBytesWriter(ContentType.parse("image/jpeg")) {
                    writeAvailable(SampleJpeg)
                }
            }
        }
    }

    fun url(mode: Mode) = url {
        port = myPort
        host = "127.0.0.1"
        protocol = URLProtocol.HTTP
        parameters.append(param, paramValue)
        if (mode == MjpegInvalid) {
            parameters.append(paramInvalid, "true")
        }
        appendEncodedPathSegments(
            when (mode) {
                Single -> "snapshot"
                Mjpeg, MjpegInvalid -> "stream"
                InternalServerError -> "error"
            }
        )
    }.toUrl().withBasicAuth(
        user = if (useBasicAuth) this.user else null,
        password = if (useBasicAuth) this.password else null,
    )

    fun start() = server.start(wait = false)

    fun stop() = server.stop(gracePeriodMillis = 0, timeoutMillis = 0)

    private suspend fun PipelineContext<Unit, ApplicationCall>.checkParam(): Boolean = call.request.queryParameters[param].let { value ->
        if (value != paramValue) {
            call.respond(HttpStatusCode.BadRequest)
            Napier.i(tag = tag, message = "Parameter does not match expected: `$value` != `$paramValue`")
            false
        } else {
            true
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.checkAuthorization(): Boolean = if (useBasicAuth) {
        val auth = call.request.headers[HttpHeaders.Authorization]
        val usernameAndPassword = listOfNotNull(user, password).joinToString(":")
        val expectedAuth = "Basic ${io.ktor.utils.io.core.String(usernameAndPassword.toByteArray(charset = Charsets.ISO_8859_1)).encodeBase64()}"
        val correct = auth == expectedAuth
        if (!correct) {
            call.response.headers.append("WWW-Authenticate", "Basic realm=\"$realm\", charset=\"UTF-8\"")
            call.respond(HttpStatusCode.Unauthorized)
            Napier.i(tag = tag, message = "Authorization does not match expected: `$auth` != `$expectedAuth`")
        }
        correct
    } else {
        true
    }

    enum class Mode {
        Single,
        Mjpeg,
        MjpegInvalid,
        InternalServerError
    }
}