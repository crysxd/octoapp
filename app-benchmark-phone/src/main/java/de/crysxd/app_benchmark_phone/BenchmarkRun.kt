package de.crysxd.app_benchmark_phone

import android.content.Intent
import android.widget.EditText
import androidx.benchmark.macro.MacrobenchmarkScope
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Direction
import androidx.test.uiautomator.Until
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.connectPrinter


fun MacrobenchmarkScope.launchApp(instance: PrinterConfigurationV3) {
    instance.connectPrinter()

    val intent = Intent().apply {
        setClassName("de.crysxd.octoapp", "de.crysxd.octoapp.MainActivity")
        putExtra("benchmark_reset", true)
        putExtra("benchmark_all_features", true)
        putExtra("benchmark_suppress_discover", true)
    }

    pressHome()
    startActivityAndWait(intent)
}

fun MacrobenchmarkScope.runBenchmark(instance: PrinterConfigurationV3) = with(device) {
    wait(Until.hasObject(By.text("Connect manually")), 15_000)
    findObject(By.text("Connect manually")).click()
    findObject(By.clazz(EditText::class.java)).text = instance.webUrl.toString()
    findObject(By.text("Continue")).click()
    wait(Until.hasObject(By.text("Open the web interface and allow access")), 15_000)
    wait(Until.hasObject(By.text("…or enter an API key")), 2_000)
    findObject(By.text("…or enter an API key")).click()
    wait(Until.hasObject(By.text("Enter your OctoPrint's API key")), 1_000)
    findObject(By.clazz(EditText::class.java)).text = instance.apiKey
    findObject(By.text("Continue")).click()
    wait(Until.hasObject(By.text("Everything is ready!")), 15_000)
    wait(Until.hasObject(By.text("Continue")), 15_000)
    findObject(By.text("Continue")).click()
//        wait(Until.hasObject(By.text("Connect when you are ready")), 5_000)
//        findObject(By.text("Connect Printer")).click()
//        wait(Until.hasObject(By.text("Connect automatically")), 1_000)
//        findObject(By.text("Connect manually")).click()
//        wait(Until.hasObject(By.text("Cancel")), 1_000)
//        findObject(By.text("Connect Printer")).click()
    wait(Until.hasObject(By.text("Temperature")), 5_000)
    repeat(3) {
        findObject(By.res("controls:scroller")).scroll(Direction.DOWN, 1f)
    }
    findObject(By.desc("Main menu")).click()
    wait(Until.hasObject(By.text("Printer")), 5_000)
    findObject(By.text("Printer")).click()
    Thread.sleep(1000)
    click(displayHeight / 4, displayWidth / 2)
    Thread.sleep(1000)
    findObject(By.desc("Main menu")).click()
    wait(Until.hasObject(By.text("Switch printer")), 5_000)
    findObject(By.text("Switch printer"))?.click()
    Thread.sleep(1000)
    pressHome()
}