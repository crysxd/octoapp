package de.crysxd.octoapp.base.ext

import androidx.core.text.HtmlCompat

fun String.toHtml() = HtmlCompat.fromHtml(this.replace("\n", "<br>"), HtmlCompat.FROM_HTML_MODE_COMPACT)