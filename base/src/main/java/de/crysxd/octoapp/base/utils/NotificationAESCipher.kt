package de.crysxd.octoapp.base.utils

import android.util.Base64
import de.crysxd.octoapp.base.data.models.FcmPrintEvent
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import kotlinx.serialization.json.Json
import java.security.MessageDigest
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class NotificationAESCipher {

    private val tag = "NotificationAESCipher"
    private val repository = BaseInjector.get().octorPrintRepository()
    private val json = Json {
        ignoreUnknownKeys = true
    }

    fun parseRawData(instanceId: String, raw: String): FcmPrintEvent? {
        val decrypted = if (raw.startsWith("{")) {
            // Not encrypted
            raw
        } else {
            // Decrypt and decode data
            val key = repository.get(instanceId)?.settings?.plugins?.octoAppCompanion?.encryptionKey ?: let {
                Napier.w(tag = tag, message = "No encryption key present")
                return null
            }
            String(AESCipher(key).decrypt(raw))
        }
        val data = json.decodeFromString<FcmPrintEvent>(decrypted)
        Napier.i(tag = tag, message = "Data: ${data.type} => $data")
        return data
    }

    private class AESCipher(private val key: String) {

        private fun createDecryptCipher(ivBytes: ByteArray): Cipher {
            val c = Cipher.getInstance("AES/CBC/PKCS7Padding")
            val sk = SecretKeySpec(key.getSha256(), "AES")
            val iv = IvParameterSpec(ivBytes)
            c.init(Cipher.DECRYPT_MODE, sk, iv)
            return c
        }

        fun decrypt(data: String): ByteArray = try {
            val bytes = Base64.decode(data, Base64.DEFAULT)
            val ivBytes = bytes.take(16).toByteArray()
            val rawDataBytes = bytes.drop(16).toByteArray()
            val cipher = createDecryptCipher(ivBytes)
            cipher.doFinal(rawDataBytes)
        } catch (e: BadPaddingException) {
            throw SuppressedIllegalStateException("Unable to decrypt, key mismatch", e)
        }

        private fun String.getSha256(): ByteArray {
            val digest = MessageDigest.getInstance("SHA-256").also { it.reset() }
            return digest.digest(this.toByteArray())
        }
    }
}