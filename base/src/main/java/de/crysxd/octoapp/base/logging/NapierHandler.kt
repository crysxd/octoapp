package de.crysxd.octoapp.base.logging

import io.github.aakira.napier.Napier
import java.util.logging.Handler
import java.util.logging.Level
import java.util.logging.LogRecord

class NapierHandler : Handler() {

    override fun publish(record: LogRecord) = when (record.level) {
        Level.OFF -> Unit
        Level.FINEST -> Napier.v(tag = record.loggerName, message = record.message, throwable = record.thrown)
        Level.FINER, Level.FINE -> Napier.d(tag = record.loggerName, message = record.message, throwable = record.thrown)
        Level.INFO -> Napier.i(tag = record.loggerName, message = record.message, throwable = record.thrown)
        Level.WARNING -> Napier.w(tag = record.loggerName, message = record.message, throwable = record.thrown)
        else -> Napier.e(tag = record.loggerName, message = record.message, throwable = record.thrown)
    }

    override fun flush() = Unit

    override fun close() = Unit
}