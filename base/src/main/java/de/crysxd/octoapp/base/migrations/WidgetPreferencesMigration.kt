package de.crysxd.octoapp.base.migrations

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import de.crysxd.octoapp.base.data.models.ControlType as NewType
import de.crysxd.octoapp.base.data.models.ControlsPreferences as New

class WidgetPreferencesMigration(private val context: Context) {

    private val tag = "WidgetPreferencesMigration"
    fun migrate() {
        listOf(
            ControlsPreferencesRepository.LIST_PRINT,
            ControlsPreferencesRepository.LIST_CONNECT,
            ControlsPreferencesRepository.LIST_PREPARE,
        ).forEach { listId ->
            val ds = WidgetPreferencesDataSource(context = context, listId = listId)
            if (ds.hasAny()) {
                Napier.i(tag = tag, message = "WidgetPreferencesMigration/$listId running...")
                ds.loadOrder()?.let { old ->
                    val new = New(
                        listId = old.listId,
                        hidden = old.hidden.map { it.map() },
                        items = old.items.map { it.map() },
                    )
                    Napier.i(tag = tag, message = "Mapped $listId")
                    SharedBaseInjector.get().controlsPreferencesRepository.setWidgetOrder(listId, new)
                    ds.delete()
                }
            }
            Napier.i(tag = tag, message = "WidgetPreferencesMigration/$listId is done")
        }

    }

    private class WidgetPreferencesDataSource(
        private val context: Context,
        private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context),
        private val gson: Gson = Gson(),
        listId: String
    ) {
        private val key = "widget_prefs_2_$listId"

        fun hasAny(): Boolean {
            return sharedPreferences.getString(key, "[]") != "[]"
        }

        fun delete() {
            sharedPreferences.edit { remove(key) }
        }

        fun loadOrder(): WidgetPreferences? {
            val string = sharedPreferences.getString(key, null)
            string ?: return null
            return gson.fromJson(string, WidgetPreferences::class.java)
        }
    }


    private data class WidgetPreferences(
        val listId: String,
        val items: List<WidgetType>,
        val hidden: List<WidgetType>,
    )

    private enum class WidgetType {
        @SerializedName("AnnouncementWidget")
        AnnouncementWidget,

        @SerializedName("ControlTemperatureWidget")
        ControlTemperatureWidget,

        @SerializedName("ExtrudeWidget")
        ExtrudeWidget,

        @SerializedName("GcodePreviewWidget")
        GcodePreviewWidget,

        @SerializedName("MoveToolWidget")
        MoveToolWidget,

        @SerializedName("PrePrintQuickAccessWidget")
        PrePrintQuickAccessWidget,

        @SerializedName("PrintQuickAccessWidget")
        PrintQuickAccessWidget,

        @SerializedName("ProgressWidget")
        ProgressWidget,

        @SerializedName("QuickAccessWidget")
        QuickAccessWidget,

        @SerializedName("SendGcodeWidget")
        SendGcodeWidget,

        @SerializedName("TuneWidget")
        TuneWidget,

        @SerializedName("WebcamWidget")
        WebcamWidget,

        @SerializedName("QuickPrint")
        QuickPrint,

        @SerializedName("CancelObject")
        CancelObject,
    }

    private fun WidgetType.map(): NewType = when (this) {
        WidgetType.AnnouncementWidget -> NewType.AnnouncementWidget
        WidgetType.ControlTemperatureWidget -> NewType.ControlTemperatureWidget
        WidgetType.ExtrudeWidget -> NewType.ExtrudeWidget
        WidgetType.GcodePreviewWidget -> NewType.GcodePreviewWidget
        WidgetType.MoveToolWidget -> NewType.MoveToolWidget
        WidgetType.PrePrintQuickAccessWidget -> NewType.QuickAccessWidget
        WidgetType.PrintQuickAccessWidget -> NewType.QuickAccessWidget
        WidgetType.ProgressWidget -> NewType.ProgressWidget
        WidgetType.QuickAccessWidget -> NewType.QuickAccessWidget
        WidgetType.SendGcodeWidget -> NewType.SendGcodeWidget
        WidgetType.TuneWidget -> NewType.TuneWidget
        WidgetType.WebcamWidget -> NewType.WebcamWidget
        WidgetType.QuickPrint -> NewType.QuickPrint
        WidgetType.CancelObject -> NewType.CancelObject
    }
}