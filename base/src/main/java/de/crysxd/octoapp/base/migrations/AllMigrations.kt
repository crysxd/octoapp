package de.crysxd.octoapp.base.migrations

import android.content.Context
import io.github.aakira.napier.Napier

class AllMigrations(val context: Context) {

    fun migrate() {
        Napier.i(tag = "AllMigrations", message = "Running migrations...")
        OctoPreferencesMigration(context).migrate()
        ExtrusionHistoryMigration(context).migrate()
        GcodeHistoryMigration(context).migrate()
        PrinterConfigurationMigration(context).migrate()
        DnsCacheMigration(context).migrate()
        WidgetPreferencesMigration(context).migrate()
        PinnedMenuItemMigration(context).migrate()
        HttpCacheMigration(context).migrate()
        GcodeCacheMigration(context).migrate()
        MediaFileRepositoryMigration(context).migrate()
        ObicoMigration().migrate()
        ReviewFlowConditionMigration().migrate()
    }
}