package de.crysxd.octoapp.base.usecase

import android.content.Context
import android.content.Intent
import android.net.Uri
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.sharedcommon.http.config.hostNameOrIp
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import de.crysxd.octoapp.sharedcommon.url.isUpnPUrl
import okhttp3.HttpUrl
import javax.inject.Inject

class OpenOctoprintWebUseCase @Inject constructor(
    private val getActiveHttpUrlUseCase: GetActiveHttpUrlUseCase,
    private val localDnsResolver: CachedDns,
    private val context: Context
) : UseCase2<OpenOctoprintWebUseCase.Params, Uri?>() {

    override suspend fun doExecute(param: Params, logger: Logger): Uri? {
        val webUrl = param.octoPrintWebUrl?.toString()?.toUrl()?.let { url ->
            if (url.isUpnPUrl()) {
                val resolvedHost = localDnsResolver.lookup(url.host).first().hostNameOrIp()
                url.withHost(resolvedHost)
            } else {
                url
            }
        } ?: SharedBaseInjector.get().getOctoPrintWebUrlUseCase().execute(
            param = GetOctoPrintWebUrlUseCase.Params(
                instanceId = param.instanceId
            )
        )

        val uri = Uri.parse(webUrl.toString())

        if (!param.returnUriOnly) {
            val intent = Intent(Intent.ACTION_VIEW, uri).also { it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) }
            context.startActivity(intent)
        }

        return uri
    }

    data class Params(
        val octoPrintWebUrl: HttpUrl? = null,
        val instanceId: String? = null,
        val returnUriOnly: Boolean = false,
    )
}