package de.crysxd.octoapp.base.migrations

import android.content.Context
import io.github.aakira.napier.Napier
import java.io.File

class HttpCacheMigration(private val context: Context) {

    private val tag = "HttpCacheMigration"

    fun migrate() {
        try {
            val dir = File(context.externalCacheDir, "http-cache")
            if (dir.exists()) {
                dir.deleteRecursively()
            }
            Napier.i(tag = tag, message = "HttpCacheMigration is done")
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to delete old HTTP cache", throwable = e)
        }
    }
}