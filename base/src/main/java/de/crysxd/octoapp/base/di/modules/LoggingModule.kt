package de.crysxd.octoapp.base.di.modules

import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.di.BaseScope
import de.crysxd.octoapp.base.logging.NapierHandler

@Module
open class LoggingModule {

    @Provides
    @BaseScope
    open fun provideNapierHandler(): NapierHandler = NapierHandler()

}