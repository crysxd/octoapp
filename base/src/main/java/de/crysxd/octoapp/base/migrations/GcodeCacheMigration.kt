package de.crysxd.octoapp.base.migrations

import android.content.Context
import io.github.aakira.napier.Napier
import java.io.File

class GcodeCacheMigration(
    private val context: Context
) {

    private val tag = "GcodeCacheMigration"

    fun migrate() {
        (1 .. 8).map {
            listOf(
                File(File(context.cacheDir.parentFile, "shared_prefs"), "gcode_cache_index_$it.xml"),
                File(context.cacheDir, "gcode${it.takeIf { it > 1 } ?: ""}"),
                File(context.externalCacheDir, "gcode${it.takeIf { it > 1 } ?: ""}"),
            )
        }.flatten().filter { it.exists() }.forEach {
            try {
                it.deleteRecursively()
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Migration failed", throwable = e)
            }
        }

        Napier.i(tag = tag, message = "GcodeCacheMigration is done")
    }
}