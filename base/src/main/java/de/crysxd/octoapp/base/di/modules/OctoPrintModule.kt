package de.crysxd.octoapp.base.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.data.repository.AndroidMediaFileHelper
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository
import de.crysxd.octoapp.base.data.repository.MediaFileRepository
import de.crysxd.octoapp.base.data.repository.NotificationIdRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.BaseScope
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.SslKeyStoreHandler

@Module
open class OctoPrintModule {

    @Provides
    open fun provideOctoPrintRepository(): PrinterConfigurationRepository = SharedBaseInjector.get().printerConfigRepository

    @Provides
    open fun provideWidgetPreferencesRepository(): ControlsPreferencesRepository = SharedBaseInjector.get().controlsPreferencesRepository

    @Provides
    open fun provideOctoPrintProvider() = SharedBaseInjector.get().printerEngineProvider

    @Provides
    open fun provideGcodeHistoryRepository() = SharedBaseInjector.get().gcodeHistoryRepository

    @Provides
    open fun provideExtrusionHistoryRepository() = SharedBaseInjector.get().extrusionHistoryRepository

    @Provides
    open fun providePinnedMenuItemRepository() = SharedBaseInjector.get().pinnedMenuItemRepository

    @Provides
    open fun provideGcodeFileRepository() = SharedBaseInjector.get().gcodeFileRepository

    @Provides
    open fun provideFileListRepository() = SharedBaseInjector.get().fileListRepository

    @Provides
    open fun provideSerialCommunicationLogsRepository() = SharedBaseInjector.get().serialCommunicationLogsRepository

    @Provides
    open fun provideTemperatureDataRepository() = SharedBaseInjector.get().temperatureDataRepository

    @Provides
    open fun provideTimelapseRepository() = SharedBaseInjector.get().timelapseRepository

    @Provides
    open fun provideMediaFileRepository() = SharedBaseInjector.get().mediaFileRepository

    @BaseScope
    @Provides
    open fun provideNotificationIdRepository(
        printerConfigurationRepository: PrinterConfigurationRepository,
        context: Context,
    ) = NotificationIdRepository(
        printerConfigurationRepository = printerConfigurationRepository,
        sharedPreferences = context.getSharedPreferences("notification_id_cache", Context.MODE_PRIVATE)
    )

    @Provides
    open fun provideTutorialsRepository() = SharedBaseInjector.get().tutorialsRepository

    @Provides
    open fun provideLocalDnsResolver() = SharedBaseInjector.get().dnsResolver

    @Provides
    fun provideSslKeyStoreHandler() = SharedBaseInjector.get().keyStoreProvider as SslKeyStoreHandler

    @BaseScope
    @Provides
    open fun provideAndroidMediaFileHelper(
        mediaFileRepository: MediaFileRepository,
    ) = AndroidMediaFileHelper(
        mediaFileRepository = mediaFileRepository
    )
}