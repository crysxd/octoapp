package de.crysxd.octoapp.base.migrations

import android.content.Context
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier
import java.io.File

class MediaFileRepositoryMigration(val context: Context) {

    private val tag = "MediaFileRepositoryMigration"

    fun migrate() = try {
        Napier.i(tag = tag, message = "Running MediaFileRepositoryMigration")
        val newCache = SharedCommonInjector.get().fileManager.forNameSpace("eternal-media-files")
        File(context.externalCacheDir, "media").listFiles()?.forEach { oldFile ->
            val newFile = File(newCache.getPath(oldFile.name).toString())
            newFile.parentFile?.mkdirs()
            if (oldFile.renameTo(newFile)) {
                Napier.i(tag = tag, message = "Migrated ${oldFile.name}")
            } else {
                Napier.w(tag = tag, message = "Failed to migrate ${oldFile.name}")
            }
        }

        listOf(
            File(context.cacheDir, "purgable_media"),
            File(context.cacheDir, "media"),
            File(context.externalCacheDir, "purgable_media"),
            File(context.externalCacheDir, "media")
        ).forEach {
            it.deleteRecursively()
        }
        Napier.i(tag = tag, message = "MediaFileRepositoryMigration is done")
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to delete old media cache", throwable = e)
    }
}