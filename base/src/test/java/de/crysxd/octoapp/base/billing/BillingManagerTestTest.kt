package de.crysxd.octoapp.base.billing

import org.junit.Assert.assertNull
import org.junit.Test

class BillingManagerTestTest {

    @Test
    fun `WHEN Billing is initialised THEN debug flag is null`() {
        assertNull(BillingManagerTest.enabledForTest)
    }
}