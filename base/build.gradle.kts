import de.crysxd.octoapp.buildscript.octoAppAndroidLibrary

plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinAndroid)
    alias(libs.plugins.kotlinSerialization)
}

octoAppAndroidLibrary(
    name = "base",
    usesCompose = false,
    usesTestFramework = false
)

dependencies {
    api(projects.sharedCommon)
    api(projects.sharedEngines)
    api(projects.sharedMenus)
    api(projects.sharedExternalApis)
    api(projects.sharedBase)
    api(projects.sharedViewmodels)

    // Remove soon
    api(libs.removesoon.gson)

    // KTOR
    api(libs.ktor.client.logging)
    api(libs.ktor.client.auth)
    api(libs.ktor.client.content.negotiation)
    api(libs.ktor.client.okhttp)

    // General
    api(libs.androidx.core.ktx)
    api(libs.androidx.preferences.ktx)
    api(libs.androidx.lifecycle.livedata)
    api(libs.androidx.lifecycle.viewmodel)

    // Testing
    testImplementation(libs.test.junit)
}