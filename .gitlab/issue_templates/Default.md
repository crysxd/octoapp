Please do not open tickets with bug reports!
Send a bug report from main menu > Settings > FAQ & Help report a bug. If this does not work send an email to hello@octoapp.eu.