package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.MenuTargetPlatform
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

internal actual fun createMenuItems() = listOf(
    ChangelogMenuItem(),
    RotationMenuItem(),
    NotificationBatterySaver(),
    NoSwipeConfirmationsMenuItem(),
    SuppressM115Request(),
    AllowTerminalDuringPrint(),
    SuppressRemoteNotificationInitialization(),
    DebugNetworkLogging(),
    EnforceIPv4(),
    RecordWebcam(),
    CompactLayout(),
    WebsocketSubscription(),
    ActivePluginIntegrations(),
    LongTimeouts(),
    DnsSdType(),
    LayersFromSlicer(),
    StartArZero(),
    ShowAllMaterialPropertiesMenuItem(),
    TestCrash(),
    TestError()
)

private class ChangelogMenuItem : MenuItem {
    override val itemId = "changelog"
    override var groupId = ""
    override val canBePinned = false
    override val order = 0
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.History
    override val title = getString("lab_menu___changelog_title")

    override suspend fun onClicked(host: MenuHost?) {
        SettingsMenuHelper.showChangelogDialog(host)
    }
}

private class NotificationBatterySaver : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.allowNotificationBatterySaver }
    override val itemId = "notification_battery_saver"
    override var groupId = "notification"
    override val canBePinned = false
    override val order = 1
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Battery
    override val title = getString("lab_menu___allow_battery_saver_title")
    override val description = getString("lab_menu___allow_battery_saver_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.allowNotificationBatterySaver = enabled
    }
}

private class SuppressRemoteNotificationInitialization : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.suppressRemoteMessageInitialization }
    override val itemId = "suppress_remote"
    override var groupId = "notification"
    override val canBePinned = false
    override val order = 2
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.NotificationsOff
    override val title = getString("lab_menu___suppress_remote_notification_init")
    override val description = getString("lab_menu___suppress_remote_notification_init_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.suppressRemoteMessageInitialization = enabled
    }
}

private class RotationMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.allowAppRotation }
    override val itemId = "rotate_app"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 100
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Rotation
    override val title = getString("lab_menu___allow_to_rotate_title")
    override val description = getString("lab_menu___allow_to_rotate_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.allowAppRotation = enabled
    }
}


private class EnforceIPv4 : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.enforceIPv4 }
    override val itemId = "user_custom_dns"
    override var groupId = "network"
    override val canBePinned = false
    override val order = 51
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Server
    override val title = getString("lab_menu___enforce_ip_v4")
    override val description = getString("lab_menu___enforce_ip_v4_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.enforceIPv4 = enabled
    }
}

private class RecordWebcam : MenuItem {
    override val itemId = "record_webcam"
    override var groupId = "webcam"
    override val canBePinned = false
    override val order = 52
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Share
    override val title = getString("lab_menu___record_webcam_traffic_for_debug")
    override val description = getString("lab_menu___record_webcam_traffic_for_debug_description")

    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().preferences.recordWebcamForDebug = true
    }
}

private class CompactLayout : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.compactLayout }
    override val itemId = "compact_layout"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 101
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Layout
    override val title = getString("lab_menu___compact_layout")
    override val description = getString("lab_menu___compact_layout_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.compactLayout = enabled
    }
}

private class NoSwipeConfirmationsMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.swipeConfirmations }
    override val itemId = "no_swipe_confirmations"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 102
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Finger
    override val supportedTargets = listOf(MenuTargetPlatform.Android)
    override val title = getString("lab_menu___swipe_confirmations_title")
    override val description = getString("lab_menu___swipe_confirmations_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.swipeConfirmations = enabled
    }
}

private class DnsSdType : RevolvingOptionsMenuItem {
    override val activeValue get() = preferences.updatedFlow2.map { it.mDnsSystem ?: DnsSd.Type.Auto.toString() }
    override val itemId = "mdsn_system"
    override var groupId = "debug"
    override val canBePinned = false
    override val order = 203
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Network
    override val title = getString("lab_menu___mdns_type")
    override val description = getString("lab_menu___mdns_type_description")
    override val options = listOf(
        RevolvingOptionsMenuItem.Option(
            label = "Google Play Services",
            value = DnsSd.Type.Android.toString(),
        ),
        RevolvingOptionsMenuItem.Option(
            label = "Apple Bonjour",
            value = DnsSd.Type.Legacy.toString(),
        ),
        RevolvingOptionsMenuItem.Option(
            label = "Automatic",
            value = DnsSd.Type.Auto.toString(),
        )
    )

    override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
        SharedBaseInjector.get().preferences.mDnsSystem = option.value
    }
}