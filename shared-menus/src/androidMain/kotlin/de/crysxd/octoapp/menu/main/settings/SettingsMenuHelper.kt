package de.crysxd.octoapp.menu.main.settings

import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Settings
import com.jakewharton.processphoenix.ProcessPhoenix
import de.crysxd.octoapp.base.data.models.AppTheme
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector

actual object SettingsMenuHelper {

    actual fun startLivePrintNotification(menuHost: MenuHost?) = try {
        (menuHost as? AndroidMenuHost)?.startLiveNotificationService()
        Unit
    } catch (e: IllegalStateException) {
        // User might have closed app just in time so we can't start the service
    }

    actual fun openNotificationSettings(menuHost: MenuHost?) {
        SharedCommonInjector.get().platform.context.openNotificationSettings()
    }

    private fun Context.openNotificationSettings() {
        val settingsIntent = Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
        startActivity(settingsIntent)
    }

    actual suspend fun customizeControls(menuHost: MenuHost?) {
        (menuHost as? AndroidMenuHost)?.customizeControls()
        menuHost?.closeMenu()
    }

    actual suspend fun transferPurchase(menuHost: MenuHost?) {
        (menuHost as? AndroidMenuHost)?.transferPurchase()
    }

    actual fun getSupportedAppThemes() = listOfNotNull(
        AppTheme.AUTO.takeIf { Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q },
        AppTheme.DARK,
        AppTheme.LIGHT,
    )

    fun showChangelogDialog(menuHost: MenuHost?) {
        throw IllegalStateException("Not implemented $menuHost")
    }

    actual fun restartApp() {
        ProcessPhoenix.triggerRebirth(SharedCommonInjector.get().platform.context)
    }
}