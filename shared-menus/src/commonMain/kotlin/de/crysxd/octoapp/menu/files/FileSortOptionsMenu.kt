package de.crysxd.octoapp.menu.files

import de.crysxd.octoapp.base.data.models.FileManagerSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class FileSortOptionsMenu : SimpleMenu {

    override val id get() = "file-sort-options"
    override val title get() = ""
    override val bottomTextFlow
        get() = SharedBaseInjector.get().preferences.updatedFlow2.map {
            if (it.fileManagerSettings.sortBy == FileManagerSettings.SortBy.PrintTime) getString("file_manager___sorting_menu___klipper_print_time_disclaimer") else ""
        }

    override suspend fun getMenuItems() = listOf(
        SortByMenuItem(),
        SortDirectionMenuItem(),
        HidePrintedMenuItem()
    )

    class SortByMenuItem : RevolvingOptionsMenuItem {
        override val activeValue get() = preferences.updatedFlow2.map { it.fileManagerSettings.sortBy.name }
        override val canBePinned = false
        override val itemId = "sort_by"
        override var groupId = "none"
        override val order = 1
        override val style = MenuItemStyle.Settings
        override val icon: MenuIcon = MenuIcon.SortBy
        override val title = getString("file_manager___sorting_menu___sort_by")
        override val options = listOf(
            RevolvingOptionsMenuItem.Option(
                label = getString("file_manager___sorting_menu___sort_by_upload_time"),
                value = FileManagerSettings.SortBy.UploadTime.name
            ),
            RevolvingOptionsMenuItem.Option(
                label = getString("file_manager___sorting_menu___sort_by_last_print_time"),
                value = FileManagerSettings.SortBy.PrintTime.name
            ),
            RevolvingOptionsMenuItem.Option(
                label = getString("file_manager___sorting_menu___sort_by_name"),
                value = FileManagerSettings.SortBy.Name.name
            ),
            RevolvingOptionsMenuItem.Option(
                label = getString("file_manager___sorting_menu___sort_by_file_size"),
                value = FileManagerSettings.SortBy.FileSize.name
            ),
        )

        override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
            preferences.fileManagerSettings = preferences.fileManagerSettings.copy(
                sortBy = FileManagerSettings.SortBy.valueOf(option.value)
            )
        }
    }

    class SortDirectionMenuItem : RevolvingOptionsMenuItem {
        override val activeValue get() = preferences.updatedFlow2.map { it.fileManagerSettings.sortDirection.name }
        override val canBePinned = false
        override val itemId = "sort_directon"
        override var groupId = "none"
        override val order = 2
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.SortOrder
        override val title = getString("file_manager___sorting_menu___direction")
        override val options = listOf(
            RevolvingOptionsMenuItem.Option(
                label = getString("file_manager___sorting_menu___direction_ascending"),
                value = FileManagerSettings.SortDirection.Ascending.name
            ),
            RevolvingOptionsMenuItem.Option(
                label = getString("file_manager___sorting_menu___direction_decending"),
                value = FileManagerSettings.SortDirection.Descending.name
            ),
        )

        override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
            preferences.fileManagerSettings = preferences.fileManagerSettings.copy(
                sortDirection = FileManagerSettings.SortDirection.valueOf(option.value)
            )
        }
    }

    class HidePrintedMenuItem : ToggleMenuItem {
        override val state get() = preferences.updatedFlow2.map { it.fileManagerSettings.hidePrintedFiles }
        override val itemId = "hide_printed"
        override var groupId = "none"
        override val order = 3
        override val style = MenuItemStyle.Settings
        override val canBePinned = false
        override val icon = MenuIcon.HideCompleted
        override val title = getString("file_manager___sorting_menu___hide_printed")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.fileManagerSettings = preferences.fileManagerSettings.copy(
                hidePrintedFiles = enabled
            )
        }
    }
}