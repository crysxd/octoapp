package de.crysxd.octoapp.menu.companion

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.ExecuteSystemCommandUseCase
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class MoonrakerCompanionMenu(val instanceId: String) : SimpleMenu {
    override val id get() = "moonraker-companion"
    override val title get() = getString("moonraker_companion___menu___title")

    override val subtitleFlow
        get() = SharedBaseInjector.get().printerConfigRepository.instanceInformationFlow(instanceId).map {
            when (it?.settings?.plugins?.octoAppCompanion?.running) {
                true -> getString("moonraker_companion___menu___subtitle_running")
                false -> getString("moonraker_companion___menu___subtitle_not_running")
                null -> getString("moonraker_companion___menu___subtitle_not_installed")
            }
        }

    override val bottomText get() = getString("moonraker_companion___menu___bottom")

    override suspend fun getMenuItems(): List<MenuItem> = listOf(
        RestartCompanionMenuItem(instanceId)
    )

    private class RestartCompanionMenuItem(
        private val instanceId: String,
    ) : MenuItem {
        private val octoPrint get() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)
        override val itemId = "restart-companion"
        override var groupId = "system"
        override val order = 210
        override val canBePinned = false
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.ActionRestart
        override val title = getString("moonraker_companion___menu___restart")

        override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = octoPrint?.hasPlugin(OctoPlugins.OctoApp) == true

        override suspend fun onClicked(host: MenuHost?) {
            val serviceName = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.settings?.plugins?.octoAppCompanion?.serviceName ?: "octoapp"
            SharedBaseInjector.get().executeSystemCommandUseCase().execute(
                ExecuteSystemCommandUseCase.Params(
                    instanceId = instanceId,
                    systemCommand = SystemCommand(
                        name = getString("moonraker_companion___menu___restart"),
                        action = "machine.services.restart/$serviceName",
                        confirmation = null,
                        type = null,
                        source = "generated"
                    ),
                )
            )
        }
    }
}