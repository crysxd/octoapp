package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

@CommonParcelize
class TemperatureControlsSettingsMenu(val instanceId: String) : SimpleMenu {

    override val id get() = "TemperatureControlsSettingsMenu/$instanceId"
    override val title get() = getString("temperature_settings_menu___title")
    override val subtitle get() = getString("temperature_settings_menu___description")

    override suspend fun getMenuItems(): List<MenuItem> = SharedBaseInjector.get().temperatureDataRepository.flow(instanceId = instanceId)
        .first()
        .mapIndexed { index, it ->
            AdditionalTemperatureMenuItem(
                component = it.component,
                instanceId = instanceId,
                canControl = it.canControl,
                order = index,
                componentLabel = it.componentLabel,
            )
        }

    private class AdditionalTemperatureMenuItem(
        val component: String,
        val instanceId: String,
        val componentLabel: String,
        canControl: Boolean,
        override val order: Int,
    ) : ToggleMenuItem {

        override val state = configRepository.instanceInformationFlow(instanceId).map {
            it?.appSettings?.hiddenTemperatureComponents?.contains(component) != true
        }

        override val itemId = "additional_temperature/$component"
        override var groupId = if (canControl) "primary" else "additional"
        override val style = MenuItemStyle.Settings
        override val canBePinned = false
        override val icon = MenuIcon.Fire
        override val title: String = getString("temperature_settings_menu___show_x", componentLabel)

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            configRepository.updateAppSettings(instanceId) {
                it.copy(
                    hiddenTemperatureComponents = if (enabled) {
                        it.hiddenTemperatureComponents - component
                    } else {
                        it.hiddenTemperatureComponents + component
                    }
                )
            }
        }
    }
}