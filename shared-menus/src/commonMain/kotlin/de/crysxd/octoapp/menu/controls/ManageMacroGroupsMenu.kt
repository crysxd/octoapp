package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.data.repository.MacroGroupsRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf

@CommonParcelize
class ManageMacroGroupsMenu : SimpleMenu {
    override val id: String get() = "manage-groups"
    override val title: String get() = getString("widget_gcode_send___manage_groups")
    override val bottomText get() = getString("widget_gcode_send___manage_groups_description")

    override suspend fun getMenuItems(): List<MenuItem> = SharedBaseInjector.get().macroGroupsRepository.run {
        listOf(
            AddMacroGroupMenuItem(),
        ) + getGroups().filter { group ->
            !isReservedGroup(group)
        }.mapIndexed { index, group ->
            MacroGroupMenuItem(
                groupName = group,
                order = index + 100
            )
        }
    }

    private open class AddMacroGroupMenuItem : InputMenuItem {
        private val repository = SharedBaseInjector.get().macroGroupsRepository

        override val order = Int.MIN_VALUE
        override val itemId = "add-macro-group"
        override var groupId = "add-group"
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Add
        override val title = getString("widget_gcode_send___add_new_group")
        override val currentValue: Flow<String> = flowOf("")
        override val rightDetail = flowOf("")
        override val inputHint: String = getString("widget_gcode_send___group_name")

        override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = if (input.isBlank() && !repository.isReservedGroup(input.trim())) {
            getString("error_please_enter_a_value")
        } else {
            null
        }

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            val current = repository.getGroups()
            repository.setGroups(current + input.trim())
            menuHost?.reloadMenu()
        }
    }

    private class MacroGroupMenuItem(
        private val groupName: String,
        override val order: Int,
    ) : AddMacroGroupMenuItem() {
        private val repository = SharedBaseInjector.get().macroGroupsRepository

        override val itemId = "macro-group/$groupName"
        override var groupId = "groups"
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Label
        override val title = getString("widget_gcode_send___edit_group_x", repository.getLabelFor(groupName).second)
        override val rightDetail = flowOf(repository.getLabelFor(groupName).first.takeIf { it != Int.MAX_VALUE }?.let { "#$it" } ?: "")
        override val secondaryButtonIcon = MenuIcon.Delete
        override val currentValue = MutableStateFlow(groupName)

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            val newName = input.trim()
            val groupContent = repository.getAllMacros().filter { (_, group) -> group == groupName }.map { (macro, _) -> macro }
            val current = repository.getGroups()

            Napier.i(tag = "MacroGroupMenuItem", message = "Renaming group from $groupName to $newName, ${groupContent.size} items to move")
            repository.setGroups(current - groupName + newName)
            groupContent.forEach { macro ->
                repository.setGroup(macroId = macro, group = newName)
            }

            currentValue.value = newName
            menuHost?.reloadMenu()
        }

        override suspend fun onSecondaryClicked(host: MenuHost?) {
            val current = repository.getGroups()
            val groupContent = repository.getAllMacros().filter { (_, group) -> group == groupName }.map { (macro, _) -> macro }
            Napier.i(tag = "MacroGroupMenuItem", message = "Deleting group $groupName, moving ${groupContent.size} items to default")
            repository.setGroups(current - groupName)
            groupContent.forEach { macro ->
                repository.setGroup(macroId = macro, group = MacroGroupsRepository.GroupDefault)
            }

            host?.reloadMenu()
        }
    }
}