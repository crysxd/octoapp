package de.crysxd.octoapp.menu.controlcenter

import de.crysxd.octoapp.base.data.models.ControlCenterSettings
import de.crysxd.octoapp.base.data.models.ControlCenterSettings.SortBy.Color
import de.crysxd.octoapp.base.data.models.ControlCenterSettings.SortBy.Label
import de.crysxd.octoapp.base.data.models.ControlCenterSettings.SortBy.valueOf
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.menu.ConfirmedMenuItem
import de.crysxd.octoapp.menu.EditUrlMenuItem
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.controlcenter.ManageOctoPrintInstancesMenu.Companion.AddNewInstanceResult
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlin.time.Duration.Companion.seconds

@CommonParcelize
class ManageOctoPrintInstancesMenu : SimpleMenu {
    override val id get() = "manage-instances"
    override val title get() = getString("control_center___manage_menu___title")

    companion object {
        const val AddNewInstanceResult = "add-new"
    }

    override suspend fun getMenuItems() = SharedBaseInjector.get()
        .printerConfigRepository
        .getAll()
        .mapIndexed { index, instance ->
            EditInstanceMenuItem(
                instanceId = instance.id,
                order = index,
                label = instance.label
            )
        } + listOf(
        AddInstanceMenuItem(),
        SortOrderMenuItem()
    )
}

private class EditInstanceMenuItem(
    private val instanceId: String,
    label: String,
    override val order: Int,
) : EditUrlMenuItem(), ConfirmedMenuItem, InputMenuItem {
    private val typeLabel = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.systemInfo?.interfaceType.label
    override val itemId = "switch-$instanceId"
    override var groupId = ""
    override val style = MenuItemStyle.Settings
    override val canBePinned = false
    override val title = getString("control_center___manage_menu___edit", label)
    override val icon = MenuIcon.Edit
    override val secondaryButtonIcon = MenuIcon.Delete
    override val showSuccess = true
    override val secondaryConfirmationMessage = getString("sign_in___discovery___delete_printer_message", label)
    override val secondaryConfirmationPositiveAction = getString("sign_in___discovery___delete_printer_confirmation", label)
    override val inputHint = getString("sign_in___discover___web_url_hint", typeLabel)
    override val rightDetail = flowOf("")
    override val keyboardType = InputMenuItem.KeyboardType.Url
    override val currentValue = SharedBaseInjector.get().printerConfigRepository.instanceInformationFlow(instanceId).map {
        it?.webUrl?.toString() ?: ""
    }

    override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = validateUrlInput(input)
    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) = handleUrlInput(input = input, instanceId = instanceId)

    override suspend fun onSecondaryClicked(host: MenuHost?) = with(SharedBaseInjector.get().printerConfigRepository) {
        val isActive = getActiveInstanceSnapshot()?.id == instanceId
        remove(instanceId)

        val any = getAll().firstOrNull()
        if (isActive && any != null) {
            setActive(instance = any, trigger = "other-deleted")
            delay(1.seconds)
            host?.reloadMenu()
        } else {
            host?.closeMenu()
        }

        Unit
    }
}

private class AddInstanceMenuItem : MenuItem {
    override val itemId = "add-instance"
    override var groupId = "general"
    override val order = Int.MAX_VALUE - 1
    override val style = MenuItemStyle.Settings
    override val canBePinned = false
    override val icon = MenuIcon.Add
    override val title = getString("main_menu___item_add_instance")

    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().printerConfigRepository.clearActive()
        host?.closeMenu(AddNewInstanceResult)
    }
}

private class SortOrderMenuItem : RevolvingOptionsMenuItem {
    override val itemId = "sort-order"
    override var groupId = "general"
    override val order = Int.MAX_VALUE
    override val style = MenuItemStyle.Settings
    override val canBePinned = false
    override val icon = MenuIcon.SortBy
    override val title = getString("control_center___manage_menu___sort_by")
    override val activeValue: Flow<String> = preferences.updatedFlow2.map { it.controlCenterSettings.sortBy.name }
    override val description get() = getString("control_center___manage_menu___bottom_text")

    override val options = ControlCenterSettings.SortBy.entries.map {
        RevolvingOptionsMenuItem.Option(
            label = when (it) {
                Color -> getString("control_center___manage_menu___sort_by_color")
                Label -> getString("control_center___manage_menu___sort_by_name")
            },
            value = it.name,
        )
    }

    override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
        preferences.controlCenterSettings = preferences.controlCenterSettings.copy(
            sortBy = valueOf(option.value)
        )
    }
}
