package de.crysxd.octoapp.menu.main.printer

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice.ControlMethod.RgbwColor
import de.crysxd.octoapp.engine.models.power.PowerDevice.ControlMethod.Toggle
import de.crysxd.octoapp.engine.models.power.PowerDevice.ControlMethod.TurnOnOff
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.ConfirmedMenuItem
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuAnimation
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import io.ktor.http.decodeURLPart
import io.ktor.http.encodeURLPathPart
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withTimeout
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.seconds

private fun createItemId(baseItemId: String, uniqueDeviceId: String, name: String, pluginName: String) =
    "$baseItemId/${uniqueDeviceId.encodeURLPathPart()}/${name.encodeURLPathPart()}/${pluginName.encodeURLPathPart()}"

private fun splitItemId(itemId: String) = itemId.split("/").mapIndexed { index, s ->
    if (index == 0) s else s.decodeURLPart()
}

private suspend fun findDevice(uniqueDeviceId: String, instanceId: String) = try {
    SharedBaseInjector.get().getPowerDevicesUseCase().execute(
        GetPowerDevicesUseCase.Params(onlyGetDeviceWithUniqueId = uniqueDeviceId, allowNetwork = false, instanceId = instanceId)
    ).results.firstOrNull()
} catch (e: Exception) {
    Napier.e(tag = "PowerControlsMenu", message = "Failed to get device, assuming null", throwable = e)
    null
}

private suspend fun isDeviceAvailable(uniqueDeviceId: String, instanceId: String) = findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId) != null

@CommonParcelize
internal class PowerControlsMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "power/$instanceId"
    override val emptyStateAction get() = { host: MenuHost? -> host?.openUrl(UriLibrary.getPluginLibraryUri(category = "power")) }
    override val emptyStateActionText get() = getString("power_menu___empty_state_action")
    override val emptyStateAnimation get() = MenuAnimation.PowerDevices
    override val emptyStateSubtitle get() = getString("power_menu___empty_state_subtitle")
    override val title get() = getString("power_menu___title_neutral")
    override val bottomText get() = getString("power_menu___bottom_text")

    override suspend fun getMenuItems() = SharedBaseInjector.get().getPowerDevicesUseCase().execute(
        GetPowerDevicesUseCase.Params(instanceId = instanceId)
    ).results.map { (device, _) ->
        ShowPowerDeviceActionsMenuItem(
            uniqueDeviceId = device.uniqueId,
            name = device.displayName,
            pluginName = device.pluginDisplayName,
            instanceId = instanceId
        )
    }
}

@CommonParcelize
class PowerControlsDeviceMenu(
    private val instanceId: String,
    private val uniqueDeviceId: String,
    private val displayName: String,
    private val pluginName: String,
    private val hasRgbw: Boolean
) : SimpleMenu {

    companion object {
        const val PowerControlsDeviceMenuRgbwView = "powerDeviceRgbw"
    }

    override val id get() = "power/$instanceId/$uniqueDeviceId"
    override val title get() = displayName
    override val bottomText get() = getString("power_menu___device_provided_by_x", pluginName)
    override val subtitleFlow: Flow<String>?
        get() = if (hasRgbw) {
            null
        } else {
            flow {
                emit(getString("loading"))

                val stateFlow = SharedBaseInjector.get().getPowerDevicesUseCase().execute(
                    GetPowerDevicesUseCase.Params(instanceId = instanceId, onlyGetDeviceWithUniqueId = uniqueDeviceId)
                ).results.firstOrNull()?.state?.invoke()?.map { state ->
                    when (state) {
                        is GetPowerDevicesUseCase.PowerState.Color -> ""
                        is GetPowerDevicesUseCase.PowerState.Percentage -> if (state.percent > 0f) {
                            state.percent.times(100).formatAsPercent()
                        } else {
                            getString("power_menu___device_is_off")
                        }

                        GetPowerDevicesUseCase.PowerState.Off -> getString("power_menu___device_is_off")
                        GetPowerDevicesUseCase.PowerState.On -> getString("power_menu___device_is_on")
                        GetPowerDevicesUseCase.PowerState.Unknown -> getString("unknown")
                    }
                } ?: emptyFlow()


                emitAll(stateFlow)
            }
        }

    override val customViewType: String?
        get() = if (hasRgbw) "$PowerControlsDeviceMenuRgbwView/${uniqueDeviceId}" else null

    override suspend fun getMenuItems(): List<MenuItem> {
        val (device, _) = requireNotNull(findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId))

        return listOfNotNull(
            CyclePowerDeviceMenuItem(uniqueDeviceId = uniqueDeviceId, pluginName = pluginName, name = displayName, showName = false, instanceId = instanceId)
                .takeIf { device.controlMethods.contains(TurnOnOff) && !device.controlMethods.contains(RgbwColor) },
            TurnPowerDeviceOffMenuItem(uniqueDeviceId = uniqueDeviceId, pluginName = pluginName, name = displayName, showName = false, instanceId = instanceId)
                .takeIf { device.controlMethods.contains(TurnOnOff) },
            SetRgbwColorMenuItem(uniqueDeviceId = uniqueDeviceId, pluginName = pluginName, name = displayName, showName = false, instanceId = instanceId)
                .takeIf { device.controlMethods.contains(RgbwColor) },
            TurnPowerDeviceOnMenuItem(uniqueDeviceId = uniqueDeviceId, pluginName = pluginName, name = displayName, showName = false, instanceId = instanceId)
                .takeIf { device.controlMethods.contains(TurnOnOff) },
            TogglePowerDeviceMenuItem(uniqueDeviceId = uniqueDeviceId, pluginName = pluginName, name = displayName, showName = false, instanceId = instanceId)
                .takeIf { device.controlMethods.contains(Toggle) },
        )
    }
}

class ShowPowerDeviceActionsMenuItem(
    val uniqueDeviceId: String,
    val name: String,
    val pluginName: String,
    val instanceId: String,
) : MenuItem {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = splitItemId(itemId).let {
            ShowPowerDeviceActionsMenuItem(
                uniqueDeviceId = it[1],
                name = it[2],
                pluginName = it[3],
                instanceId = instanceId
            )
        }
    }

    override val itemId = createItemId(MenuItems.MENU_ITEM_SHOW_POWER_DEVICE_ACTIONS, uniqueDeviceId = uniqueDeviceId, name = name, pluginName = pluginName)
    override var groupId = "devices"
    override val order = 332
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.PowerOutlet
    override val showAsSubMenu = true
    override val title = name

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = isDeviceAvailable(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)
    override suspend fun onClicked(host: MenuHost?) {
        val controlMethods = findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)?.device?.controlMethods ?: emptyList()

        host?.pushMenu(
            PowerControlsDeviceMenu(
                instanceId = instanceId,
                uniqueDeviceId = uniqueDeviceId,
                pluginName = pluginName,
                displayName = name,
                hasRgbw = RgbwColor in controlMethods,
            )
        )
    }
}

class TurnPowerDeviceOffMenuItem(
    val uniqueDeviceId: String,
    val name: String,
    val pluginName: String,
    val instanceId: String,
    val showName: Boolean = true
) : ConfirmedMenuItem {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = splitItemId(itemId).let {
            TurnPowerDeviceOffMenuItem(
                uniqueDeviceId = it[1],
                name = it[2],
                pluginName = it[3],
                instanceId = instanceId
            )
        }
    }

    override val itemId = createItemId(MenuItems.MENU_ITEM_POWER_DEVICE_OFF, uniqueDeviceId = uniqueDeviceId, name = name, pluginName = pluginName)
    override var groupId = ""
    override val order = 333
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.PowerDeviceOff
    override val canSkipConfirmation = !SharedBaseInjector.get().preferences.confirmPowerOffDevices.contains(uniqueDeviceId)
    override val title = if (showName) getString("power_menu___turn_x_off", name) else getString("power_menu___turn_off")
    override val confirmationMessage = getString("power_menu___confirm_turn_x_off", name)
    override val confirmationPositiveAction = getString("power_menu___turn_x_off", name)

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = isDeviceAvailable(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)
    override suspend fun onClicked(host: MenuHost?) {
        findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)?.device?.turnOff()
        host?.reloadMenu()
    }
}

class TurnPowerDeviceOnMenuItem(
    val uniqueDeviceId: String,
    val name: String,
    val pluginName: String,
    val instanceId: String,
    val showName: Boolean = true
) : MenuItem {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = splitItemId(itemId).let {
            TurnPowerDeviceOnMenuItem(
                uniqueDeviceId = it[1],
                name = it[2],
                pluginName = it[3],
                instanceId = instanceId
            )
        }
    }

    override val itemId = createItemId(MenuItems.MENU_ITEM_POWER_DEVICE_ON, uniqueDeviceId = uniqueDeviceId, name = name, pluginName = pluginName)
    override var groupId = ""
    override val order = 334
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.PowerDeviceOn
    override val title = if (showName) getString("power_menu___turn_x_on", name) else getString("power_menu___turn_on")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = isDeviceAvailable(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)
    override suspend fun onClicked(host: MenuHost?) {
        findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)?.device?.turnOn()
        host?.reloadMenu()
    }
}

class TogglePowerDeviceMenuItem(
    val uniqueDeviceId: String,
    val name: String,
    val pluginName: String,
    val instanceId: String,
    val showName: Boolean = true,
) : ConfirmedMenuItem {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = splitItemId(itemId).let {
            TogglePowerDeviceMenuItem(
                uniqueDeviceId = it[1],
                name = it[2],
                pluginName = it[3],
                instanceId = instanceId
            )
        }
    }

    override val itemId = createItemId(MenuItems.MENU_ITEM_POWER_DEVICE_TOGGLE, uniqueDeviceId = uniqueDeviceId, name = name, pluginName = pluginName)
    override var groupId = ""
    override val order = 337
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.PowerDeviceToggle
    override val canSkipConfirmation = !SharedBaseInjector.get().preferences.confirmPowerOffDevices.contains(uniqueDeviceId)
    override val title = if (showName) getString("power_menu___toggle_x", name) else getString("power_menu___toggle")
    override val confirmationMessage = getString("power_menu___confirm_toggle_x", name)
    override val confirmationPositiveAction = getString("power_menu___toggle_x", name)

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = isDeviceAvailable(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)
    override suspend fun onClicked(host: MenuHost?) {
        findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)?.device?.toggle()
        host?.reloadMenu()
    }
}


class CyclePowerDeviceMenuItem(
    val uniqueDeviceId: String,
    val name: String,
    pluginName: String,
    val instanceId: String,
    showName: Boolean = true,
) : ConfirmedMenuItem {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = splitItemId(itemId).let {
            TogglePowerDeviceMenuItem(
                uniqueDeviceId = it[1],
                name = it[2],
                pluginName = it[3],
                instanceId = instanceId
            )
        }
    }

    override val itemId = createItemId(MenuItems.MENU_ITEM_POWER_DEVICE_CYCLE, uniqueDeviceId = uniqueDeviceId, name = name, pluginName = pluginName)
    override var groupId = ""
    override val order = 336
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.PowerDeviceCycle
    override val canSkipConfirmation = !SharedBaseInjector.get().preferences.confirmPowerOffDevices.contains(uniqueDeviceId)
    override val title = if (showName) getString("power_menu___cycle_x", name) else getString("power_menu___cycle")
    override val confirmationMessage = getString("power_menu___confirm_cycle_x", name)
    override val confirmationPositiveAction = getString("power_menu___cycle_x", name)

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = isDeviceAvailable(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)
    override suspend fun onClicked(host: MenuHost?) {
        val (device, _) = requireNotNull(findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId))
        SharedBaseInjector.get().cyclePsuUseCase().execute(device)
        host?.reloadMenu()
    }
}

@CommonParcelize
class PowerControlsRgbwDeviceMenu(
    private val instanceId: String,
    private val uniqueDeviceId: String,
    private val displayName: String,
    private val pluginName: String,
) : SimpleMenu {

    override val id get() = "power/$instanceId/$uniqueDeviceId/color"
    override val title get() = displayName
    override val bottomText get() = getString("power_menu___device_provided_by_x", pluginName)
    override val customViewType get() = "${PowerControlsDeviceMenu.PowerControlsDeviceMenuRgbwView}/${uniqueDeviceId}"

    override suspend fun getMenuItems(): List<MenuItem> = (0..3).map {
        SetColorComponentMenuItem(
            uniqueDeviceId = uniqueDeviceId,
            instanceId = instanceId,
            componentIndex = it
        )
    } + listOf(
        TurnPowerDeviceOnMenuItem(uniqueDeviceId = uniqueDeviceId, pluginName = pluginName, name = displayName, showName = false, instanceId = instanceId),
        TurnPowerDeviceOffMenuItem(uniqueDeviceId = uniqueDeviceId, pluginName = pluginName, name = displayName, showName = false, instanceId = instanceId),
    )
}

private class SetColorComponentMenuItem(
    val uniqueDeviceId: String,
    val instanceId: String,
    val componentIndex: Int
) : InputMenuItem {

    init {
        require(componentIndex in 0..3) { "Invalid index: $componentIndex" }
    }

    override val itemId = "color-componet-$componentIndex"
    override var groupId = "color"
    override val order = componentIndex
    override val canBePinned = false
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.Color
    override val inputHint = "0 - 255"
    override val keyboardType = InputMenuItem.KeyboardType.Numbers
    override val title = when (componentIndex) {
        0 -> getString("power_menu___color_r")
        1 -> getString("power_menu___color_g")
        2 -> getString("power_menu___color_b")
        3 -> getString("power_menu___color_w")
        else -> ""
    }
    override val currentValue = flow {
        val (_, flowFactory) = requireNotNull(findDevice(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId))
        val nested = flowFactory().map {
            (it as GetPowerDevicesUseCase.PowerState.Color).colors.first().let { color ->
                when (componentIndex) {
                    0 -> color.r
                    1 -> color.g
                    2 -> color.b
                    3 -> color.w
                    else -> 0f
                }.times(255).roundToInt().toString()
            }
        }

        emitAll(nested)
    }.catch { e ->
        Napier.e(tag = "SetColorComponentMenuItem", message = "Failure in flow", throwable = e)
        emit("")
    }

    override val rightDetail = currentValue.map { "$it / 255" }

    override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = if (inputAsNumber == null || inputAsNumber !in 0f..255f) {
        getString("error_please_enter_a_value")
    } else {
        null
    }

    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        val (device, _) = requireNotNull(findDevice(instanceId = instanceId, uniqueDeviceId = uniqueDeviceId))
        val newValue = requireNotNull(inputAsNumber) / 255f
        val currentColor = withTimeout(5.seconds) { device.colorFlow().filterNotNull().first() }.first()
        val newColor = currentColor.copy(
            r = if (componentIndex == 0) newValue else currentColor.r,
            g = if (componentIndex == 1) newValue else currentColor.g,
            b = if (componentIndex == 2) newValue else currentColor.b,
            w = if (componentIndex == 3) newValue else currentColor.w,
        )
        device.setColor(newColor)
    }
}

class SetRgbwColorMenuItem(
    val uniqueDeviceId: String,
    val name: String,
    val pluginName: String,
    val instanceId: String,
    showName: Boolean = true,
) : MenuItem {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = splitItemId(itemId).let {
            SetRgbwColorMenuItem(
                uniqueDeviceId = it[1],
                name = it[2],
                pluginName = it[3],
                instanceId = instanceId
            )
        }
    }

    override val itemId = createItemId(MenuItems.MENU_ITEM_POWER_DEVICE_RGBW, uniqueDeviceId = uniqueDeviceId, name = name, pluginName = pluginName)
    override var groupId = ""
    override val order = 335
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.Color
    override val showAsSubMenu = true
    override val title = if (showName) getString("power_menu___color_x", name) else getString("power_menu___color")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = isDeviceAvailable(uniqueDeviceId = uniqueDeviceId, instanceId = instanceId)
    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(
            PowerControlsRgbwDeviceMenu(
                instanceId = instanceId,
                uniqueDeviceId = uniqueDeviceId,
                displayName = name,
                pluginName = pluginName,
            )
        )
    }
}


