package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.SupporterPerkMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.isDarwin
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
class SwitchOctoPrintDisabledMenu : SimpleMenu {

    override val id get() = "switch-disabled"
    override val title get() = getString("supporter_perk___title")
    override val subtitle get() = getString("main_menu___subtitle_quick_switch_disabled", getString("supporter_perk___description", getString("control_center___title")))
    override val customViewType get() = if (SharedCommonInjector.get().platform.isDarwin()) SupporterPerkMenu.SupporterPerkView else null

    override suspend fun getMenuItems() = listOf(
        SignOutMenuItem(),
        EnableQuickSwitchMenuItem()
    )
}

private class SignOutMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SIGN_OUT
    override var groupId = ""
    override val order = 151
    override val canBePinned = false
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.LogOut
    override val title = getString("main_menu___item_sign_out")

    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().printerConfigRepository.clearActive()
        host?.closeMenu()
    }
}

private class EnableQuickSwitchMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_ENABLE_QUICK_SWITCH
    override var groupId = ""
    override val order = 149
    override val canBePinned = false
    override val style = MenuItemStyle.Support
    override val icon =  MenuIcon.Support
    override val title = getString("main_menu___enable_quick_switch")

    override suspend fun onClicked(host: MenuHost?) {
        OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenOpen, mapOf("trigger" to "switch_menu"))
        host?.openUrl(UriLibrary.getPurchaseUri())
    }
}