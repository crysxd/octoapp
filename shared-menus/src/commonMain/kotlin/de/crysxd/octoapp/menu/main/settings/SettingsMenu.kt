package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.menu.EditUrlMenuItem
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.main.settings.SettingsMenu.Companion.ResultShowControlCenter
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedcommon.url.isUpnPUrl
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking

@CommonParcelize
class SettingsMenu : SimpleMenu {

    companion object {
        const val ResultShowControlCenter = "show-control-center"
        const val ResultCustomizeControls = "customize-controls"
    }

    override val id get() = "settings"
    override val title get() = getString("main_menu___item_show_settings")
    override val subtitle get() = getString("main_menu___submenu_subtitle")
    override val bottomText get() = getString("main_menu___about_text", "", SharedCommonInjector.getOrNull()?.platform?.appVersion ?: "???")

    override suspend fun getMenuItems() = listOf(
        HelpMenuItem(),
        PrintNotificationMenuItem(),
        ShowAutoConnectPrinterMenu(),
        ChangeInstanceMenuItem(),
        ShowOctoAppLabMenuItem(),
        ShowUiSettingsMenuItem(),
        ShowPowerDeviceSettingsMenuItem(),
        CachesMenuItem(),
        EditInstanceMenuItem(),
        ManageBillingMenuItem(),
    )

    override fun onUrlOpened(menuHost: MenuHost?, url: String) = if (url == "privacy" && menuHost != null) runBlocking {
        menuHost.pushMenu(PrivacyMenu())
        true
    } else {
        false
    }
}

class HelpMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_HELP
    override var groupId = "help"
    override val order = 101
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val canBePinned = true
    override val icon = MenuIcon.Help
    override val showAsSubMenu = true
    override val title = getString("main_menu___item_help_faq_and_feedback")

    override suspend fun onClicked(host: MenuHost?) {
        host?.openUrl(UriLibrary.getHelpUri())
    }
}

class PrintNotificationMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_PRINT_NOTIFICATIONS
    override var groupId = "default"
    override val order = 117
    override val enforceSingleLine = false
    override val canBePinned = true
    override val canRunWithAppInBackground = false
    override val style = MenuItemStyle.Settings
    override val showAsSubMenu = true
    override val icon = MenuIcon.Notifications
    override val title = getString("main_menu___item_print_notifications")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(PrintNotificationsMenu())
    }
}

class ShowUiSettingsMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SHOW_UI_SETTINGS_MENU
    override var groupId = "default"
    override val order = 118
    override val style = MenuItemStyle.Settings
    override val canRunWithAppInBackground = false
    override val canBePinned = true
    override val enforceSingleLine = false
    override val icon = MenuIcon.UserInterface
    override val showAsSubMenu = true
    override val title = getString("main_menu___menu_ui_settings_title")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(UiSettingsMenu())
    }
}

class ShowPowerDeviceSettingsMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SHOW_POWER_SETTINGS_MENU
    override var groupId = "default"
    override val order = 119
    override val canBePinned = true
    override val style = MenuItemStyle.Settings
    override val canRunWithAppInBackground = false
    override val enforceSingleLine = false
    override val icon = MenuIcon.PowerStrip
    override val showAsSubMenu = true
    override val title = getString("main_menu___menu_power_settings_title")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(PowerSettingsMenu())
    }
}

class ShowAutoConnectPrinterMenu : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SHOW_AUTO_CONNECT_PRINTER_MENU
    override var groupId = "default"
    override val order = 120
    override val style = MenuItemStyle.Settings
    override val canRunWithAppInBackground = false
    override val enforceSingleLine = false
    override val icon = MenuIcon.Automatic
    override val canBePinned = true
    override val showAsSubMenu = true
    override val title = getString("main_menu___item_auto_connect_printer")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(AutoConnectPrinterMenu())
    }
}

open class CachesMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_CACHES_MENU
    override var groupId = "default"
    override val order = 131
    override val canRunWithAppInBackground = false
    override val style: MenuItemStyle = MenuItemStyle.Settings
    override val icon = MenuIcon.Storage
    override val canBePinned = true
    override val showAsSubMenu = true
    override val title = getString("caches_menu___title")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(CachesMenu())
    }
}

class ShowOctoAppLabMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SHOW_OCTOAPP_LAB
    override var groupId = "default"
    override val order = 132
    override val canRunWithAppInBackground = false
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Lab
    override val showAsSubMenu = true
    override val canBePinned = true
    override val title = getString("main_menu___show_octoapp_lab")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(OctoAppLabMenu())
    }
}

class ManageBillingMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_MANAGE_BILLING
    override var groupId = "default"
    override val order = 133
    override val canRunWithAppInBackground = false
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Support
    override val showAsSubMenu = true
    override val canBePinned = true
    override val title = getString("main_menu___manage_billing")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = BillingManager.state.value.purchases.isNotEmpty()

    override suspend fun onClicked(host: MenuHost?) {
        if (SharedCommonInjector.get().platform.isAndroid()) {
            host?.pushMenu(AndroidBillingMenu())
        } else {
            host?.openUrl(UriLibrary.getPurchaseUri())
        }
    }
}

open class EditInstanceMenuItem : EditUrlMenuItem(), InputMenuItem {
    private val instance
        get() = repository?.getActiveInstanceSnapshot()

    override val itemId = MenuItems.MENU_ITEM_EDIT_INSTANCE_URL
    override var groupId = "change"
    override val order = 152
    override val style: MenuItemStyle = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Edit
    override val canRunWithAppInBackground = false
    override val canBePinned = true
    override val title = getString("control_center___manage_menu___edit", instance?.label ?: "???")
    override val inputHint = getString("sign_in___discover___web_url_hint", instance?.systemInfo?.interfaceType.label)
    override val rightDetail = flowOf("")
    override val keyboardType = InputMenuItem.KeyboardType.Url
    override val showSuccess = true
    override val description get() = instance?.webUrl?.toString()
    override val currentValue = repository?.instanceInformationFlow(instance?.id)?.map {
        val webUrl = it?.webUrl
        if (webUrl?.isUpnPUrl() == true) "UPnP" else (webUrl?.toString() ?: "")
    } ?: flowOf("")

    override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = validateUrlInput(input)
    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = instance != null
    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        handleUrlInput(
            input = input,
            instanceId = requireNotNull(instance?.id) { "Missing instanceId" }
        )

        menuHost?.reloadMenu()
    }
}

open class ChangeInstanceMenuItem : MenuItem {
    private val printerCount = SharedBaseInjector.getOrNull()?.printerConfigRepository?.getAll()?.size ?: 1
    private val isSwitchItem = printerCount > 1 && BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)
    private val isDisabledItem = !BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)

    override val itemId = MenuItems.MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
    override var groupId = "change"
    override val order = 151
    override val style: MenuItemStyle = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Switch
    override val canRunWithAppInBackground = false
    override val canBePinned = true
    override val showAsSubMenu = true
    override val title = getString(if (isSwitchItem) "main_menu___item_change_octoprint_instance" else "main_menu___item_add_octoprint_instance")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) =
        !BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH) || SharedCommonInjector.get().platform.platformName != "android"

    override suspend fun onClicked(host: MenuHost?) {
        if (isDisabledItem) {
            // User had no quick switch, push disabled menu
            host?.pushMenu(SwitchOctoPrintDisabledMenu())
        } else if (isSwitchItem) {
            // User has quick switch and multiple printer, show control center
            host?.closeMenu(ResultShowControlCenter)
        } else {
            // User has quick switch but only one printer, go to log in
            SharedBaseInjector.get().printerConfigRepository.clearActive()
            host?.closeMenu()
        }
    }
}