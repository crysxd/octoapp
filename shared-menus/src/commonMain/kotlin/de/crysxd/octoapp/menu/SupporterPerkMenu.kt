package de.crysxd.octoapp.menu

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
class SupporterPerkMenu(private val featureName: String) : SimpleMenu {

    companion object {
        const val SupporterPerkView = "supporter_perk"
    }

    override val id get() = "feature-disabled-${featureName.hashCode()}"
    override val title get() = getString("supporter_perk___title")
    override val subtitle get() = getString("supporter_perk___description", featureName)
    override val customViewType get() = SupporterPerkView

    private val logFeatureName: String get() = featureName.lowercase().replace(" ", "_").take(16)

    override suspend fun getMenuItems() = listOf(
        SupportMenuItem(logFeatureName),
        LearnMoreMenuItem(logFeatureName),
    )

    private class SupportMenuItem(val featureName: String) : MenuItem {

        override val itemId: String = "support_octoapp"
        override var groupId: String = ""
        override val order = 1
        override val style = MenuItemStyle.Support
        override val canBePinned = false
        override val icon = MenuIcon.Support
        override val title = getString("support_octoapp")

        override suspend fun onClicked(host: MenuHost?) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenOpen, mapOf("trigger" to "perk_menu_support/$featureName"))
            host?.openUrl(UriLibrary.getPurchaseUri())
        }
    }

    private class LearnMoreMenuItem(val featureName: String) : MenuItem {
        override val itemId: String = "learn_more"
        override var groupId: String = ""
        override val order = 0
        override val style = MenuItemStyle.Neutral
        override val canBePinned = false
        override val icon = MenuIcon.Info
        override val title = getString("learn_more")

        override suspend fun onClicked(host: MenuHost?) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenOpen, mapOf("trigger" to "perk_menu_info/$featureName"))
            host?.openUrl(UriLibrary.getPurchaseUri())
        }
    }
}