package de.crysxd.octoapp.menu.plugins

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.GetMaterialsUseCase
import de.crysxd.octoapp.base.usecase.SelectMmu2FilamentUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonIgnoreOnParcel
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch

@CommonParcelize
class Mmu2FilamentSelectMenu(
    val source: Settings.Mmu.LabelSource,
    val instanceId: String,
) : SimpleMenu {

    override val id get() = "mmu2-selection"
    override val title get() = getString("material_menu___title_select_material")
    override val subtitle get() = getString("material_menu___subtitle_select_material")

    @CommonIgnoreOnParcel
    private var choiceMade = false

    @CommonIgnoreOnParcel
    private var plugin: Settings.Mmu.Plugin? = null

    override suspend fun getMenuItems(): List<MenuItem> {
        val settings = requireNotNull(SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.settings?.plugins?.mmu) { "No MMU settings for $instanceId" }
        plugin = settings.plugin

        return when (settings.labelSource) {
            Settings.Mmu.LabelSource.Manual -> manualMaterialSelection(settings)
            Settings.Mmu.LabelSource.FilamentManager -> filamentManagerSelection(instanceId = instanceId, plugin = settings.plugin)
            Settings.Mmu.LabelSource.SpoolManager -> spoolManagerSelection(instanceId = instanceId, plugin = settings.plugin)
        }
    }

    private suspend fun makeChoice(choice: SelectMmu2FilamentUseCase.Params) {
        choiceMade = true
        SharedBaseInjector.get().selectMmu2FilamentUseCase().execute(choice)
    }

    override fun onDestroy() {
        // If no choice was made, we attempt to cancel.
        // This means if the user made a choice somewhere else this call will fail
        if (!choiceMade) plugin?.let { plugin ->
            AppScope.launch(Dispatchers.Default) {
                try {
                    Napier.i(tag = "Mmu2FilamentSelectMenu", message = "No choice made, cancelling...")
                    makeChoice(SelectMmu2FilamentUseCase.Params.Cancel(instanceId = instanceId, plugin = plugin))
                } catch (e: Exception) {
                    Napier.w(tag = "Mmu2FilamentSelectMenu", message = "Failed to cancel MMU2 selection: ${e::class.simpleName}: ${e.message}")
                }
            }
        }
    }

    private fun manualMaterialSelection(settings: Settings.Mmu) = settings.filament.map { filament ->
        SelectMaterialMenuItem(
            choice = SelectMmu2FilamentUseCase.Params.ToolIndex(toolIndex = filament.toolIndex, instanceId = instanceId, plugin = settings.plugin),
            displayName = filament.name,
            colorHex = filament.color,
            menu = this,
        )
    } + CancelMenuItem(
        menu = this,
        choice = SelectMmu2FilamentUseCase.Params.Cancel(
            plugin = settings.plugin,
            instanceId = instanceId
        )
    )

    private suspend fun filamentManagerSelection(instanceId: String, plugin: Settings.Mmu.Plugin) =
        materialPluginSelection(filamentPlugin = OctoPlugins.FilamentManager, instanceId = instanceId, mmuPlugin = plugin)

    private suspend fun spoolManagerSelection(instanceId: String, plugin: Settings.Mmu.Plugin) =
        materialPluginSelection(filamentPlugin = OctoPlugins.SpoolManager, instanceId = instanceId, mmuPlugin = plugin)

    private suspend fun materialPluginSelection(filamentPlugin: String, mmuPlugin: Settings.Mmu.Plugin, instanceId: String): List<MenuItem> {
        val materials = SharedBaseInjector.get().getMaterialsUseCase().execute(
            param = GetMaterialsUseCase.Params(instanceId = instanceId)
        ).filter { it.id.providerId.equals(filamentPlugin, ignoreCase = true) }

        val tools = (0..4).map { toolIndex ->
            toolIndex to materials.firstOrNull { it.activeToolIndex == toolIndex }
        }.map { (toolIndex, material) ->
            SelectMaterialMenuItem(
                choice = SelectMmu2FilamentUseCase.Params.ToolIndex(toolIndex = toolIndex, instanceId = instanceId, plugin = mmuPlugin),
                displayName = material?.displayName,
                weightGrams = material?.weightGrams,
                colorHex = material?.color,
                menu = this
            )
        }

        return tools + CancelMenuItem(
            menu = this,
            choice = SelectMmu2FilamentUseCase.Params.Cancel(
                plugin = mmuPlugin,
                instanceId = instanceId
            )
        )
    }


    private class SelectMaterialMenuItem(
        private val menu: Mmu2FilamentSelectMenu,
        private val choice: SelectMmu2FilamentUseCase.Params,
        displayName: String?,
        weightGrams: Float? = null,
        colorHex: HexColor? = null,
    ) : MenuItem {
        override val itemId = "mmuMaterials/${choice.choice}"
        override var groupId = ""
        override val order = choice.choice
        override val canBePinned = false
        override val style = MenuItemStyle.Printer
        override val icon = MenuIcon.MaterialInactive
        override val title = choice.toolIndexLabel.toString() + (displayName?.takeIf { it.isNotBlank() }?.let { ": $it" } ?: "")
        override val rightDetail = weightGrams?.let { flowOf(getString("x_grams", it.toInt())) }
        override val iconColorOverwrite = colorHex

        override suspend fun onClicked(host: MenuHost?) {
            menu.makeChoice(choice)
        }
    }

    private class CancelMenuItem(
        private val menu: Mmu2FilamentSelectMenu,
        private val choice: SelectMmu2FilamentUseCase.Params
    ) : MenuItem {
        override val itemId = "mmuMaterials/${choice.choice}"
        override val canBePinned = false
        override var groupId = "cancel"
        override val order = choice.choice
        override val style = MenuItemStyle.Printer
        override val icon = MenuIcon.Stop
        override val title = getString("cancel_print")

        // Prusa MMU plugin doesn't support cancel
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = choice.plugin != Settings.Mmu.Plugin.PrusaMmu
        override suspend fun onClicked(host: MenuHost?) = menu.makeChoice(choice)
    }
}