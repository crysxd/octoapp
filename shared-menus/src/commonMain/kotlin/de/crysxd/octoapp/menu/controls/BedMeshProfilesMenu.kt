package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.seconds

@CommonParcelize
class BedMeshProfilesMenu(private val instanceId: String) : SimpleMenu {
    override val id get() = "meshprofiles"
    override val title get() = getString("widget_bed_mesh___profiles")

    override suspend fun getMenuItems(): List<MenuItem> = SharedBaseInjector.get().printerConfigRepository.get(instanceId)
        ?.settings
        ?.bedMesh
        ?.let { mesh ->
            mesh.profiles.sortedBy { it.label }.mapIndexed { index, profile ->
                ActivateMeshProfileMenuItem(
                    profile = profile,
                    order = index,
                    active = mesh.activeProfile == profile.id
                )
            }
        } ?: emptyList()

    private class ActivateMeshProfileMenuItem(
        val profile: Settings.BedMesh.Profile,
        val active: Boolean,
        override val order: Int,
    ) : MenuItem {
        override val itemId = "profile/${profile.id}"
        override var groupId = "profiles"
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = if (active) MenuIcon.CheckActive else MenuIcon.CheckInactive
        override val title = getString("widget_bed_mesh___activate_profile_x", profile.label)

        override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = !active

        override suspend fun onClicked(host: MenuHost?) {
            SharedBaseInjector.get().executeGcodeCommandUseCase().execute(
                param = ExecuteGcodeCommandUseCase.Param(
                    command = GcodeCommand.Batch(commands = profile.enableCommand.split("\n")),
                    fromUser = false,
                )
            )

            // Let profile refresh, then close
            delay(1.seconds)
            host?.closeMenu()
        }
    }
}