package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

@CommonParcelize
internal class OctoAppLabMenu : SimpleMenu {

    override val id get() = "octolab"
    override val title get() = getString("lab_menu___title")
    override val subtitle get() = getString("lab_menu___subtitle")

    override suspend fun getMenuItems() = createMenuItems()
}

internal expect fun createMenuItems(): List<MenuItem>

internal class SuppressM115Request : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.suppressM115Request }
    override val itemId = "suppress_m115"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 101
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Block
    override val title = getString("lab_menu___suppress_m115_request_title")
    override val description = getString("lab_menu___suppress_m115_request_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.suppressM115Request = enabled
    }
}

internal class AllowTerminalDuringPrint : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.allowTerminalDuringPrint }
    override val itemId = "allow_terminal_during_print"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 102
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.CodeOff
    override val title = getString("lab_menu___allow_terminal_during_print_title")
    override val description = getString("lab_menu___allow_terminal_during_print_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.allowTerminalDuringPrint = enabled
    }
}

internal class DebugNetworkLogging : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.debugNetworkLogging }
    override val itemId = "debug_network_logging"
    override var groupId = "network"
    override val canBePinned = false
    override val order = 50
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Bug
    override val title = getString("lab_menu___debug_network_logging")
    override val description = getString("lab_menu___debug_network_logging_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.debugNetworkLogging = enabled
    }
}

internal class WebsocketSubscription : RevolvingOptionsMenuItem {
    private val valueHttp = "HTTP"
    private val valueWsHttp = "WS + HTTP"
    override val activeValue = preferences.updatedFlow2.map { if (it.websocketTransportAllowed) valueWsHttp else valueHttp }
    override val options = listOf(
        RevolvingOptionsMenuItem.Option(valueWsHttp, valueWsHttp),
        RevolvingOptionsMenuItem.Option(valueHttp, valueHttp),
    )
    override val itemId = "websocket_transport_allowed"
    override var groupId = "debug"
    override val canBePinned = false
    override val order = 200
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Ethernet
    override val title: String = getString("lab_menu___event_transport")
    override val description = getString("lab_menu___event_transport_description")

    override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
        preferences.websocketTransportAllowed = option.value == valueWsHttp
    }
}

internal class ActivePluginIntegrations : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.activePluginIntegrations }
    override val itemId = "no_active_plugin_integrations"
    override var groupId = "debug"
    override val canBePinned = false
    override val order = 201
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.PluginsOff
    override val title = getString("lab_menu___active_plugins")
    override val description = getString("lab_menu___active_plugins_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.activePluginIntegrations = enabled
    }
}

internal class LongTimeouts : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.longTimeouts }
    override val itemId = "long_timeouts"
    override var groupId = "debug"
    override val canBePinned = false
    override val order = 202
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.TimeLeft
    override val title: String = getString("lab_menu___long_timeouts")
    override val description: String = getString("lab_menu___long_timeouts_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.longTimeouts = enabled
    }
}

internal class ShowAllMaterialPropertiesMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.showAllMaterialProperties }
    override val itemId = "show_all_material_properties"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 103
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Text
    override val title = getString("lab_menu___show_all_material_properties")
    override val description = getString("lab_menu___show_all_material_properties_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.showAllMaterialProperties = enabled
    }
}

internal class LayersFromSlicer : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.detectLayersFromSlicer }
    override val itemId = "layers_from_slicer"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 104
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Layers
    override val title: String = getString("lab_menu___layers_from_slicer")
    override val description: String = getString("lab_menu___layers_from_slicer_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.detectLayersFromSlicer = enabled
        SharedBaseInjector.get().localGcodeFileDataSource.clear()
    }
}

class StartArZero : InputMenuItem {
    private val preferences: OctoPreferences get() = SharedBaseInjector.get().preferences
    override val currentValue = preferences.updatedFlow2.map { it.gcodePreviewSettings.layerOffset.toString() }
    override val inputHint = getString("gcode_preview_settings___start_at_layer_zero")
    override val itemId = "firstLayer"
    override var groupId = "misc"
    override val canBePinned = false
    override val order = 105
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Zero
    override val title = getString("gcode_preview_settings___start_at_layer_zero")
    override val keyboardType = InputMenuItem.KeyboardType.NumbersNegative

    override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = if (input.toIntOrNull() == null) {
        getString("error_please_enter_a_value")
    } else {
        null
    }

    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        preferences.gcodePreviewSettings = preferences.gcodePreviewSettings.copy(layerOffset = input.toInt())
    }
}


internal class TestCrash : MenuItem {
    override val itemId = "test_crash"
    override var groupId = "crash"
    override val canBePinned = false
    override val order = 1000
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Bug
    override val title = "Crash app"
    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) =
        SharedCommonInjector.get().platform.betaBuild || SharedCommonInjector.get().platform.debugBuild

    @OptIn(DelicateCoroutinesApi::class)
    override suspend fun onClicked(host: MenuHost?) {
        GlobalScope.launch { throw Exception("This is a test crash") }
    }
}

internal class TestError : MenuItem {
    override val itemId = "test_error"
    override var groupId = "crash"
    override val canBePinned = false
    override val order = 1001
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Bug
    override val title = "Report test error"
    override val description = "Just for my testing :)"
    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) =
        SharedCommonInjector.get().platform.betaBuild || SharedCommonInjector.get().platform.debugBuild

    override suspend fun onClicked(host: MenuHost?) {
        Napier.e(tag = "Test", message = "Test", throwable = IllegalStateException("This is a test error! (2)"))
    }
}

