package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class MoveControlsSettingsMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "move_settings"
    override val title get() = getString("move_settings")
    override val subtitle get() = getString("move_settings_description")

    companion object {
        private const val DEFAULT_FEED_RATE = 8000
    }

    override suspend fun getMenuItems() = listOf(
        XFeedrateMenuItem(instanceId = instanceId),
        YFeedrateMenuItem(instanceId = instanceId),
        ZFeedrateMenuItem(instanceId = instanceId),
    )

    abstract class FeedrateMenuItem(private val instanceId: String) : InputMenuItem {
        private val repository get() = SharedBaseInjector.get().printerConfigRepository
        override val currentValue = repository.instanceInformationFlow(instanceId).map { it?.appSettings.getFeedrate().toString() }
        override val inputHint = getString("gcode_preview_settings___start_at_layer_zero")
        override var groupId = "feedrate"
        override val canBePinned = false
        override val order = 2
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Speed
        override val keyboardType = InputMenuItem.KeyboardType.Numbers

        abstract fun AppSettings.setFeedratre(feedrate: Int): AppSettings
        abstract fun AppSettings?.getFeedrate(): Int

        override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = if (inputAsNumber == null) {
            getString("error_please_enter_a_value")
        } else {
            null
        }

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            repository.updateAppSettings(instanceId) { appSettings ->
                appSettings.setFeedratre(feedrate = requireNotNull(inputAsNumber?.toInt()) { "Missing float" })
            }
        }
    }

    private class XFeedrateMenuItem(instanceId: String) : FeedrateMenuItem(instanceId) {
        override val itemId = "x_feedrate"
        override val title = getString("x_feedrate_mm_min")


        override fun AppSettings.setFeedratre(feedrate: Int) = copy(moveXFeedRate = feedrate)
        override fun AppSettings?.getFeedrate() = this?.moveXFeedRate ?: DEFAULT_FEED_RATE
    }

    private class YFeedrateMenuItem(instanceId: String) : FeedrateMenuItem(instanceId) {
        override val itemId = "y_feedrate"
        override val title = getString("y_feedrate_mm_min")


        override fun AppSettings.setFeedratre(feedrate: Int) = copy(moveYFeedRate = feedrate)
        override fun AppSettings?.getFeedrate() = this?.moveYFeedRate ?: DEFAULT_FEED_RATE
    }

    private class ZFeedrateMenuItem(instanceId: String) : FeedrateMenuItem(instanceId) {
        override val itemId = "z_feedrate"
        override val title = getString("z_feedrate_mm_min")


        override fun AppSettings.setFeedratre(feedrate: Int) = copy(moveZFeedRate = feedrate)
        override fun AppSettings?.getFeedrate() = this?.moveZFeedRate ?: DEFAULT_FEED_RATE
    }
}