package de.crysxd.octoapp.menu

import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.menu.controls.WebcamSettingsMenu
import de.crysxd.octoapp.menu.main.octoprint.ConfigureRemoteAccessMenuItem
import de.crysxd.octoapp.menu.main.octoprint.ExecuteSystemCommandMenuItem
import de.crysxd.octoapp.menu.main.octoprint.OpenOctoPrintMenuItem
import de.crysxd.octoapp.menu.main.octoprint.ShowFilesMenuItem
import de.crysxd.octoapp.menu.main.octoprint.ShowPluginLibraryOctoPrintMenuItem
import de.crysxd.octoapp.menu.main.octoprint.TimelapseArchiveMenuItem
import de.crysxd.octoapp.menu.main.octoprint.TimelapseConfigMenuItem
import de.crysxd.octoapp.menu.main.printer.ApplyTemperaturePresetForAllMenuItem
import de.crysxd.octoapp.menu.main.printer.ApplyTemperaturePresetForBedMenuItem
import de.crysxd.octoapp.menu.main.printer.ApplyTemperaturePresetForChamberMenuItem
import de.crysxd.octoapp.menu.main.printer.ApplyTemperaturePresetForHotendMenuItem
import de.crysxd.octoapp.menu.main.printer.ApplyTemperaturePresetMenuItem
import de.crysxd.octoapp.menu.main.printer.CancelPrintKeepTemperaturesMenuItem
import de.crysxd.octoapp.menu.main.printer.CancelPrintMenuItem
import de.crysxd.octoapp.menu.main.printer.CoolDownMenuItem
import de.crysxd.octoapp.menu.main.printer.CyclePowerDeviceMenuItem
import de.crysxd.octoapp.menu.main.printer.DisconnectMenuItem
import de.crysxd.octoapp.menu.main.printer.EmergencyStopMenuItem
import de.crysxd.octoapp.menu.main.printer.ExecuteGcodeForTemperaturePresetMenuItem
import de.crysxd.octoapp.menu.main.printer.OpenPowerControlsMenuItem
import de.crysxd.octoapp.menu.main.printer.OpenTerminalMenuItem
import de.crysxd.octoapp.menu.main.printer.SetRgbwColorMenuItem
import de.crysxd.octoapp.menu.main.printer.ShowMaterialPluginMenuItem
import de.crysxd.octoapp.menu.main.printer.ShowPowerDeviceActionsMenuItem
import de.crysxd.octoapp.menu.main.printer.ShowTemperatureMenuItem
import de.crysxd.octoapp.menu.main.printer.ShowWebcamMenuItem
import de.crysxd.octoapp.menu.main.printer.TogglePowerDeviceMenuItem
import de.crysxd.octoapp.menu.main.printer.TurnPowerDeviceOffMenuItem
import de.crysxd.octoapp.menu.main.printer.TurnPowerDeviceOnMenuItem
import de.crysxd.octoapp.menu.main.printer.TurnPsuOffMenuItem
import de.crysxd.octoapp.menu.main.settings.AllowMoveWhilePrinting
import de.crysxd.octoapp.menu.main.settings.AppThemeMenuItem
import de.crysxd.octoapp.menu.main.settings.AutoConnectPrinterMenuItem
import de.crysxd.octoapp.menu.main.settings.CachesMenuItem
import de.crysxd.octoapp.menu.main.settings.ChangeInstanceMenuItem
import de.crysxd.octoapp.menu.main.settings.ChangeLanguageMenuItem
import de.crysxd.octoapp.menu.main.settings.CustomizeWidgetsMenuItem
import de.crysxd.octoapp.menu.main.settings.HelpMenuItem
import de.crysxd.octoapp.menu.main.settings.KeepScreenOnDuringPrintMenuItem
import de.crysxd.octoapp.menu.main.settings.LeftHandModeMenuItem
import de.crysxd.octoapp.menu.main.settings.LiveNotificationMenuItem
import de.crysxd.octoapp.menu.main.settings.ManageBillingMenuItem
import de.crysxd.octoapp.menu.main.settings.NotifyLayerCompletion
import de.crysxd.octoapp.menu.main.settings.NotifyPrinterBeep
import de.crysxd.octoapp.menu.main.settings.NotifyUserInteraction
import de.crysxd.octoapp.menu.main.settings.PrintNotificationMenuItem
import de.crysxd.octoapp.menu.main.settings.ResetDefaultPowerDevicesMenuItem
import de.crysxd.octoapp.menu.main.settings.ShowAutoConnectPrinterMenu
import de.crysxd.octoapp.menu.main.settings.ShowEmergencyStopInBottomBar
import de.crysxd.octoapp.menu.main.settings.ShowInstanceInStatusBarMenuItem
import de.crysxd.octoapp.menu.main.settings.ShowOctoAppLabMenuItem
import de.crysxd.octoapp.menu.main.settings.ShowPowerDeviceSettingsMenuItem
import de.crysxd.octoapp.menu.main.settings.ShowUiSettingsMenuItem
import de.crysxd.octoapp.menu.main.settings.SystemNotificationSettings
import io.github.aakira.napier.Napier

class MenuItemLibrary {

    private val map = mapOf<String, (String) -> MenuItem>(
        MenuItems.MENU_ITEM_CHANGE_LANGUAGE to { ChangeLanguageMenuItem() },
        MenuItems.MENU_ITEM_OPEN_OCTOPRINT to { OpenOctoPrintMenuItem(instanceId = it) },
        MenuItems.MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU to { ChangeInstanceMenuItem() },
        MenuItems.MENU_ITEM_CANCEL_PRINT to { CancelPrintMenuItem(it) },
        MenuItems.MENU_ITEM_EMERGENCY_STOP to { EmergencyStopMenuItem(it) },
        MenuItems.MENU_ITEM_TURN_PSU_OFF to { TurnPsuOffMenuItem(it) },
        MenuItems.MENU_ITEM_POWER_CONTROLS to { OpenPowerControlsMenuItem(it) },
        MenuItems.MENU_ITEM_NIGHT_THEME to { AppThemeMenuItem() },
        MenuItems.MENU_ITEM_PRINT_NOTIFICATION_SETTINGS to { PrintNotificationMenuItem() },
        MenuItems.MENU_ITEM_LIVE_NOTIFICATION to { LiveNotificationMenuItem() },
        MenuItems.MENU_ITEM_PRINTER_BEEP to { NotifyPrinterBeep() },
        MenuItems.MENU_ITEM_NOTIFY_LAYER_COMPLETION to { NotifyLayerCompletion() },
        MenuItems.MENU_ITEM_NOTIFY_USER_INTERACTION to { NotifyUserInteraction() },
        MenuItems.MENU_ITEM_SYSTEM_NOTIFICATION_SETTINGS to { SystemNotificationSettings() },
        MenuItems.MENU_ITEM_SCREEN_ON_DURING_PRINT to { KeepScreenOnDuringPrintMenuItem() },
        MenuItems.MENU_ITEM_SHOW_WEBCAM to { ShowWebcamMenuItem(it) },
        MenuItems.MENU_ITEM_CANCEL_PRINT_KEEP_TEMPS to { CancelPrintKeepTemperaturesMenuItem(it) },
        MenuItems.MENU_ITEM_TEMPERATURE_MENU to { ShowTemperatureMenuItem(it) },
        MenuItems.MENU_ITEM_DISCONNECT to { DisconnectMenuItem(it) },
        MenuItems.MENU_ITEM_AUTO_CONNECT_PRINTER to { AutoConnectPrinterMenuItem() },
        MenuItems.MENU_ITEM_MATERIAL_MENU to { ShowMaterialPluginMenuItem(it) },
        MenuItems.MENU_ITEM_HELP to { HelpMenuItem() },
        MenuItems.MENU_PRINT_NOTIFICATIONS to { PrintNotificationMenuItem() },
        MenuItems.MENU_ITEM_CUSTOMIZE_WIDGETS to { CustomizeWidgetsMenuItem() },
        MenuItems.MENU_ITEM_OPEN_TERMINAL to { OpenTerminalMenuItem(it) },
        MenuItems.MENU_ITEM_SHOW_OCTOAPP_LAB to { ShowOctoAppLabMenuItem() },
        MenuItems.MENU_ITEM_MANAGE_BILLING to { ManageBillingMenuItem() },
        MenuItems.MENU_ITEM_CONFIGURE_REMOTE_ACCESS to { ConfigureRemoteAccessMenuItem() },
        MenuItems.MENU_ITEM_SHOW_FILES to { ShowFilesMenuItem(instanceId = it) },
        MenuItems.MENU_ITEM_SHOW_WEBCAM_RESOLUTION to { WebcamSettingsMenu.ShowResolutionMenuItem() },
        MenuItems.MENU_ITEM_SHOW_WEBCAM_NAME to { WebcamSettingsMenu.ShowNameMenuItem() },
        MenuItems.MENU_ITEM_WEBCAM_ASPECT_RATIO_SOURCE to { WebcamSettingsMenu.AspectRatioMenuItem(instanceId = it) },
        MenuItems.MENU_ITEM_WEBCAM_DATA_SAVER_INTERVAL to { WebcamSettingsMenu.DataSaverIntervalMenuItem() },
        MenuItems.MENU_ITEM_WEBCAM_DATA_SAVER_CONDITIONS to { WebcamSettingsMenu.DataSaverConditionsMenuItem() },
        MenuItems.MENU_ITEM_LEFT_HAND_MODE to { LeftHandModeMenuItem() },
        MenuItems.MENU_ITEM_PLUGINS to { ShowPluginLibraryOctoPrintMenuItem(instanceId = it) },
        MenuItems.MENU_ITEM_COOL_DOWN to { CoolDownMenuItem(it) },
        MenuItems.MENU_ITEM_TIMELAPSE_CONFIG to { TimelapseConfigMenuItem(instanceId = it) },
        MenuItems.MENU_ITEM_TIMELAPSE_ARCHIVE to { TimelapseArchiveMenuItem(instanceId = it) },
        MenuItems.MENU_ITEM_RESET_DEFAULT_POWER_DEVICES to { ResetDefaultPowerDevicesMenuItem() },
        MenuItems.MENU_ITEM_SHOW_INSTNACE_IN_STATUS_BAR to { ShowInstanceInStatusBarMenuItem() },
        MenuItems.MENU_ITEM_MOVE_DURING_PRINT to { AllowMoveWhilePrinting() },
        MenuItems.MENU_ITEM_EMERGENCY_STOP_IN_BOTTOM_BAR to { ShowEmergencyStopInBottomBar() },
        MenuItems.MENU_ITEM_CACHES_MENU to { CachesMenuItem() },
        MenuItems.MENU_ITEM_SHOW_POWER_SETTINGS_MENU to { ShowPowerDeviceSettingsMenuItem() },
        MenuItems.MENU_ITEM_SHOW_UI_SETTINGS_MENU to { ShowUiSettingsMenuItem() },
        MenuItems.MENU_ITEM_SHOW_AUTO_CONNECT_PRINTER_MENU to { ShowAutoConnectPrinterMenu() },
    )

    operator fun get(itemId: String, instanceId: String): MenuItem? = with(MenuItems) {
        when {
            map.containsKey(itemId) -> try {
                map[itemId]?.invoke(instanceId)
            } catch (e: Exception) {
                Napier.e(tag = "MenuItemLibrary", message = "Unable to inflate menu item with itemId=$itemId", throwable = e)
                null
            }

            itemId.startsWith(MENU_ITEM_APPLY_TEMPERATURE_PRESET_ALL) -> ApplyTemperaturePresetForAllMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_ITEM_APPLY_TEMPERATURE_PRESET_HOTEND) -> ApplyTemperaturePresetForHotendMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_ITEM_APPLY_TEMPERATURE_PRESET_BED) -> ApplyTemperaturePresetForBedMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_ITEM_APPLY_TEMPERATURE_PRESET_CHAMBER) -> ApplyTemperaturePresetForChamberMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_ITEM_APPLY_TEMPERATURE_PRESET_GCODE) -> ExecuteGcodeForTemperaturePresetMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_ITEM_APPLY_TEMPERATURE_PRESET) -> ApplyTemperaturePresetMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_EXECUTE_SYSTEM_COMMAND) -> ExecuteSystemCommandMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_ITEM_SHOW_POWER_DEVICE_ACTIONS) -> ShowPowerDeviceActionsMenuItem.forItemId(
                itemId,
                instanceId = instanceId
            )

            itemId.startsWith(MENU_ITEM_POWER_DEVICE_OFF) -> TurnPowerDeviceOffMenuItem.forItemId(itemId, instanceId = instanceId)
            itemId.startsWith(MENU_ITEM_POWER_DEVICE_ON) -> TurnPowerDeviceOnMenuItem.forItemId(itemId, instanceId = instanceId)
            itemId.startsWith(MENU_ITEM_POWER_DEVICE_CYCLE) -> CyclePowerDeviceMenuItem.forItemId(itemId, instanceId = instanceId)
            itemId.startsWith(MENU_ITEM_POWER_DEVICE_TOGGLE) -> TogglePowerDeviceMenuItem.forItemId(itemId, instanceId = instanceId)
            itemId.startsWith(MENU_ITEM_POWER_DEVICE_RGBW) -> SetRgbwColorMenuItem.forItemId(itemId, instanceId = instanceId)
            else -> null
        }
    }
}