package de.crysxd.octoapp.menu.main.printer

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.ActivateMaterialUseCase
import de.crysxd.octoapp.engine.api.MaterialsApi.Companion.NoMaterialId
import de.crysxd.octoapp.engine.models.material.Material
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.moonraker.ext.convertToMoonrakerComponentLabel
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
class MaterialExtruderSelectionMenu(
    private val instanceId: String,
    private val offerStartPrint: Boolean,
    private val extruderComponents: List<String>,
    private val material: Material,
) : SimpleMenu {

    override val id get() = "materials/$instanceId/extruders/$offerStartPrint"
    override val title get() = getString("material_menu_extruder___title")
    override val subtitle get() = getString("material_menu_extruder___subtitle", material.displayName)

    override suspend fun getMenuItems(): List<MenuItem> = extruderComponents.mapIndexed { index, extruderComponent ->
        SelectExtruderMenuItem(
            material = material,
            offerStartPrint = offerStartPrint,
            instanceId = instanceId,
            extruderComponent = extruderComponent,
            index = index,
        )
    } + ClearSelectionMenuItem(
        material = material,
        instanceId = instanceId,
        extruderComponents = extruderComponents,
    )

    private class SelectExtruderMenuItem(
        private val material: Material,
        private val offerStartPrint: Boolean = false,
        private val instanceId: String,
        private val extruderComponent: String,
        private val index: Int,
    ) : MenuItem {
        companion object {
            private val toolRegex = Regex("tool\\d+")
        }

        private val preferences = SharedBaseInjector.get().preferences
        override val itemId = "material/${material.id}/extruder/$extruderComponent"
        override var groupId = "tools"
        override val order = index
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val showSuccess = true
        override val icon
            get() = when (index) {
                0 -> MenuIcon.MaterialActive0
                1 -> MenuIcon.MaterialActive1
                2 -> MenuIcon.MaterialActive2
                3 -> MenuIcon.MaterialActive3
                4 -> MenuIcon.MaterialActive4
                5 -> MenuIcon.MaterialActive5
                6 -> MenuIcon.MaterialActive6
                7 -> MenuIcon.MaterialActive7
                8 -> MenuIcon.MaterialActive8
                9 -> MenuIcon.MaterialActive9
                else -> MenuIcon.MaterialInactive
            }
        override val title = if (toolRegex.matches(extruderComponent)) {
            "#${extruderComponent.removePrefix("tool")}"
        } else {
            extruderComponent.convertToMoonrakerComponentLabel()
        }
        override val iconColorOverwrite: HexColor? = try {
            material.color
        } catch (e: Exception) {
            null
        }

        override suspend fun onClicked(host: MenuHost?) {
            SharedBaseInjector.get().activateMaterialUseCase().execute(
                ActivateMaterialUseCase.Params(instanceId = instanceId, uniqueMaterialId = material.id, extruderComponent = extruderComponent)
            )

            if (offerStartPrint) {
                host?.closeMenu(MaterialMenu.ResultStartPrint)
            } else {
                host?.popMenu()
            }
        }
    }

    private class ClearSelectionMenuItem(
        private val material: Material,
        private val instanceId: String,
        private val extruderComponents: List<String>,
    ) : MenuItem {
        private val preferences = SharedBaseInjector.get().preferences
        override val itemId = "material/${material.id}/deactivate"
        override var groupId = "other"
        override val order = Int.MAX_VALUE
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val showSuccess = true
        override val icon = MenuIcon.MaterialReset
        override val title = getString("material_menu___deactivate", material.displayName)

        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = material.isActivated

        override suspend fun onClicked(host: MenuHost?) {
            material.activeExtruderComponent?.let { component ->
                SharedBaseInjector.get().activateMaterialUseCase().execute(
                    ActivateMaterialUseCase.Params(
                        instanceId = instanceId,
                        uniqueMaterialId = NoMaterialId.copy(providerId = material.id.providerId),
                        extruderComponent = component
                    )
                )
            }

            host?.popMenu()
        }
    }
}