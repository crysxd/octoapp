package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
internal class ConfirmPowerOffSettingsMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "ConfirmPowerOffSettingsMenu/$instanceId"
    override val title get() = getString("main_menu___title_confirm_power_off")
    override val subtitle get() = getString("main_menu___subtitle_confirm_power_off")

    override suspend fun getMenuItems() = SharedBaseInjector.get().getPowerDevicesUseCase().execute(
        GetPowerDevicesUseCase.Params(instanceId = instanceId)
    ).results.map {
        ConfirmPowerOffMenuItem(it.device)
    }

}

internal class ConfirmPowerOffMenuItem(private val device: PowerDevice) : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.confirmPowerOffDevices.contains(device.uniqueId) }
    override val itemId = "confirm_power_off/${device.uniqueId}"
    override var groupId = "devices"
    override val order = 100
    override val canBePinned = true
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.PowerOutlet
    override val title = device.displayName

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        val base = preferences.confirmPowerOffDevices.toMutableSet()
        if (enabled) base.add(device.uniqueId) else base.remove(device.uniqueId)
        preferences.confirmPowerOffDevices = base
    }
}
