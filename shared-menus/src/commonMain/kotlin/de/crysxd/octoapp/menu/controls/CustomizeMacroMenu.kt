package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.macro.Macro
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.MutableStateFlow

@CommonParcelize
class CustomizeMacroMenu(private val instanceId: String, private val macroId: String?) : SimpleMenu {
    private val allMacros get() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.macros ?: emptyList()
    private val macros get() = if (macroId != null) allMacros.filter { it.id == macroId } else allMacros

    override val id get() = "CustomizeMacroMenu"
    override val title get() = if (macros.size == 1) macros[0].name else getString("send_gcode")

    override suspend fun getMenuItems(): List<MenuItem> = mutableListOf<MenuItem>().also { list ->
        list += ManageGroupsMenuItem(groupId = if (macros.size == 1) "single" else "groups")
        list += macros.mapIndexed { index, macro ->
            MacroMenuItem(
                macro = macro,
                showName = macros.size > 1,
                groupId = if (macros.size == 1) "single" else "macros",
                order = 100 + index,
            )
        }
    }

    private class MacroMenuItem(
        private val macro: Macro,
        showName: Boolean,
        override var groupId: String,
        override val order: Int = 1,
    ) : RevolvingOptionsMenuItem {
        private val repository = SharedBaseInjector.get().macroGroupsRepository

        override val itemId: String = "macro/${macro.id}"
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Label
        override val title = if (showName) macro.name else getString("widget_gcode_send___group")
        override val activeValue = MutableStateFlow(repository.getGroup(macro.id))
        override val options get() = repository.getGroups().map { RevolvingOptionsMenuItem.Option(value = it, label = repository.getLabelFor(it).second) }

        override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
            repository.setGroup(macroId = macro.id, group = option.value)
            activeValue.value = option.value
        }
    }

    private class ManageGroupsMenuItem(
        override var groupId: String,
    ) : MenuItem {
        override val order: Int = 0
        override val itemId: String = "macro-manage-groups"
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Label
        override val title = getString("widget_gcode_send___manage_groups")
        override val showAsSubMenu = true

        override suspend fun onClicked(host: MenuHost?) {
            host?.pushMenu(ManageMacroGroupsMenu())
        }
    }
}