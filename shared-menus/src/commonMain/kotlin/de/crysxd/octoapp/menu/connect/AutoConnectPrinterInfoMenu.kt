package de.crysxd.octoapp.menu.connect

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
class AutoConnectPrinterInfoMenu : SimpleMenu {

    companion object {
        const val TriggerConnectResult = "triggerConnect"
    }

    override val id get() = "AutoConnectPrinterInfoMenu"
    override val title get() = getString("connect_printer___auto_menu___title")
    override val subtitle get() = getString("connect_printer___auto_menu___subtitle")

    override suspend fun getMenuItems() = listOf(
        ConnectAutomaticallyMenuItem(),
        ConnectManuallyMenuItem(),
    )

    private class ConnectAutomaticallyMenuItem : MenuItem {
        override val itemId = "auto_connect"
        override var groupId = ""
        override val order = 1
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Automatic
        override val title: String get() = getString("connect_printer___auto_menu___auto_option")

        override suspend fun onClicked(host: MenuHost?) {
            SharedBaseInjector.get().preferences.apply {
                wasAutoConnectPrinterInfoShown = true
                isAutoConnectPrinter = true
            }

            host?.closeMenu(TriggerConnectResult)
        }
    }

    private class ConnectManuallyMenuItem : MenuItem {
        override val itemId = "manual_connect"
        override var groupId = ""
        override val order = 2
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Manual
        override val title: String get() = getString("connect_printer___auto_menu___manual_option")

        override suspend fun onClicked(host: MenuHost?) {
            SharedBaseInjector.get().preferences.apply {
                wasAutoConnectPrinterInfoShown = true
                isAutoConnectPrinter = false
            }

            host?.closeMenu(TriggerConnectResult)
        }
    }
}