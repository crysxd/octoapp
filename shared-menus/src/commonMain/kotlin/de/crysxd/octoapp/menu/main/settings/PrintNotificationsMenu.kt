package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.MenuAnimation
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.MenuTargetPlatform
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.menu.main.settings.SettingsMenuHelper.openNotificationSettings
import de.crysxd.octoapp.menu.main.settings.SettingsMenuHelper.startLivePrintNotification
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedcommon.utils.NotificationUtils.canUseNotifications
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

@CommonParcelize
class PrintNotificationsMenu : SimpleMenu {

    companion object {
        const val NotificationStatusView = "notificationStatus"
    }

    override val id get() = "PrintNotificationsMenu"
    override val title get() = getString("main_menu___item_print_notifications")
    override val subtitle get() = getString("print_notifications_menu___subtitle").takeIf { SharedCommonInjector.get().platform.isAndroid() }
    override val emptyStateSubtitle get() = getString("notifications_permission___rationale")
    override val emptyStateActionText get() = getString("notifications_permission___action_enable")
    override val customViewType get() = NotificationStatusView
    override val emptyStateAnimation get() = MenuAnimation.Notificiations
    override val emptyStateAction get() = { host: MenuHost? -> openNotificationSettings(host) }
    override val bottomText
        get() = getString(
            "print_notifications_menu___bottom",
            UriLibrary.getFaqUri("print_notifications").toString()
        ).takeIf { SharedCommonInjector.get().platform.isAndroid() }

    override suspend fun getMenuItems() = if (canUseNotifications()) {
        listOf(
            LiveNotificationMenuItem(),
            SystemNotificationSettings(),
            NotifyPrinterBeep(),
            NotifyLayerCompletion(),
            NotifyUserInteraction(),
            NotificationLayout(),
        )
    } else {
        emptyList()
    }
}

internal class LiveNotificationMenuItem : ToggleMenuItem {
    override val state get() = SharedBaseInjector.get().preferences.updatedFlow2.map { it.isLivePrintNotificationsEnabled }
    override val itemId = MenuItems.MENU_ITEM_LIVE_NOTIFICATION
    override var groupId = ""
    override val order = 105
    override val canBePinned = true
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val supportedTargets = listOf(MenuTargetPlatform.Android)
    override val icon = MenuIcon.Notifications
    override val title = getString("print_notifications_menu___item_live_notification_on")
    override val description = getString("print_notifications_menu___item_live_notification_on_description")

    override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = canUseNotifications()

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        SharedBaseInjector.get().preferences.isLivePrintNotificationsEnabled = enabled

        try {
            if (enabled) {
                Napier.i(tag = "LiveNotificationMenuItem", message = "Service enabled, starting service")
                startLivePrintNotification(host)
            }
        } catch (e: IllegalStateException) {
            // User might have closed app just in time so we can't start the service
        }
    }
}

internal class NotifyPrinterBeep : ToggleMenuItem {
    override val state get() = SharedBaseInjector.get().preferences.updatedFlow2.map { it.notifyPrinterBeep }
    override val itemId = MenuItems.MENU_ITEM_PRINTER_BEEP
    override var groupId = "types"
    override val order = 107
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.VolumeUp
    override val canBePinned = true
    override val title = getString("notification_channel___beep")

    override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = canUseNotifications()
    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        SharedBaseInjector.get().preferences.notifyPrinterBeep = enabled
    }
}

internal class NotifyLayerCompletion : RevolvingOptionsMenuItem {
    override val activeValue = preferences.updatedFlow2.map { it.notifyForLayers.joinToString(",") }
    override val options = listOf(
        RevolvingOptionsMenuItem.Option(
            label = getString("print_notifications_menu___item_layer_completion_option_1_and_3"),
            value = "1,3"
        ),
        RevolvingOptionsMenuItem.Option(
            label = getString("print_notifications_menu___item_layer_completion_option_1"),
            value = "1"
        ),
        RevolvingOptionsMenuItem.Option(
            label = getString("print_notifications_menu___item_layer_completion_option_none"),
            value = "-1"
        )
    )

    override val itemId = MenuItems.MENU_ITEM_NOTIFY_LAYER_COMPLETION
    override var groupId = "types"
    override val order = 110
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Layers
    override val canBePinned = true
    override val title = getString("print_notifications_menu___item_layer_completion")
    override val description = getString("print_notifications_menu___item_layer_completion_description")

    override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = canUseNotifications()
    override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
        preferences.notifyForLayers = option.value.split(",").map { it.toInt() }

        // Update the capabilities for currently active. This will flush the new settings to the plugin
        SharedBaseInjector.get().updateInstanceCapabilitiesUseCase().execute(
            UpdateInstanceCapabilitiesUseCase.Params(
                updateVersion = false,
                updateSettings = false,
                updateCommands = false,
                updateProfile = false,
                updateSystemInfo = false,
                updatePlugins = false,
                instanceId = null
            )
        )
    }
}

internal class NotifyUserInteraction : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.notifyForUserInteraction }
    override val itemId = MenuItems.MENU_ITEM_NOTIFY_USER_INTERACTION
    override var groupId = "types"
    override val order = 108
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Pause
    override val canBePinned = true
    override val title = getString("print_notifications_menu___item_user_interaction")

    override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = canUseNotifications()
    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.notifyForUserInteraction = enabled

        // Update the capabilities for currently active. This will flush the new settings to the plugin
        SharedBaseInjector.get().updateInstanceCapabilitiesUseCase().execute(
            UpdateInstanceCapabilitiesUseCase.Params(
                updateVersion = false,
                updateSettings = false,
                updateCommands = false,
                updateProfile = false,
                updateSystemInfo = false,
                updatePlugins = false,
                instanceId = null
            )
        )
    }
}

internal class SystemNotificationSettings : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SYSTEM_NOTIFICATION_SETTINGS
    override var groupId = "system"
    override val order = 120
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val canBePinned = true
    override val icon = MenuIcon.Settings
    override val title = getString("print_notifications_menu___item_system_settings")
    override val description = if (SharedCommonInjector.get().platform.isAndroid()) getString("print_notifications_menu___item_system_settings_description") else null

    override suspend fun onClicked(host: MenuHost?) {
        openNotificationSettings(host)
    }
}

internal class NotificationLayout : RevolvingOptionsMenuItem {
    override val itemId = MenuItems.MENU_ITEM_NOTIFICATION_LAYOUT
    override var groupId = "types"
    override val order = 106
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val canBePinned = true
    override val icon = MenuIcon.Settings
    override val title = "Notification layout*"
    override val options = listOf(
        RevolvingOptionsMenuItem.Option("Snapshot + progress", OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_PROGRESS),
        RevolvingOptionsMenuItem.Option("Snapshot + details", OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_SUBTITLE),
        RevolvingOptionsMenuItem.Option("Details + progress", OctoPreferences.VALUE_NOTIFICATIONS_LAYOUT_SUBTITLE_PROGRESS),
    )
    override val activeValue: Flow<String>
        get() = preferences.updatedFlow2.map { it.notificationsLayout }

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) =
        SharedCommonInjector.get().platform.isAndroid()

    override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
        preferences.notificationsLayout = option.value
    }
}