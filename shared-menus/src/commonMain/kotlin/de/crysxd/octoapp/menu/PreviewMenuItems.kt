package de.crysxd.octoapp.menu

import de.crysxd.octoapp.base.billing.FEATURE_AUTOMATIC_LIGHTS
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map

object PreviewMenuItems {

    @CommonParcelize
    private class SubMenu(override val title: String) : SimpleMenu {
        override val id get() = title
        private val count get() = (2..8).random()

        override suspend fun getMenuItems(): List<MenuItem> = (0..count).mapIndexed { index, _ ->
            SubMenuTrigger(listOf(title, "$index").joinToString(" > "))
        }
    }

    private class SubMenuTrigger(override val title: String) : MenuItem {
        override val itemId = title
        override var groupId = ""
        override val order = 1
        override val canBePinned = true
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Fire

        override suspend fun onClicked(host: MenuHost?) {
            host?.pushMenu(SubMenu(title))
        }
    }

    val MenuWithNavigation: Menu
        get() = SubMenu("Root")

    val Normal: MenuItem
        get() = object : MenuItem {
            override val itemId = "test1"
            override var groupId = "1"
            override val order = 1
            override val canBePinned = false
            override val style = MenuItemStyle.Settings
            override val icon = MenuIcon.Settings
            override val title: String = getString("main_menu___item_show_settings")

            override suspend fun onClicked(host: MenuHost?) {
                delay(1000)
            }
        }

    val Confirmed: MenuItem
        get() = object : ConfirmedMenuItem {
            override val confirmationPositiveAction = "Do it!"
            override val confirmationMessage = "This is dangerous!"
            override val itemId = "test1"
            override val canBePinned = true
            override var groupId = "1"
            override val order = 1
            override val style = MenuItemStyle.Settings
            override val icon = MenuIcon.Settings
            override val title: String = getString("Turn PSU off")

            override suspend fun onClicked(host: MenuHost?) {
                delay(1000)
            }
        }

    val FeatureDisabled: MenuItem
        get() = object : ConfirmedMenuItem {
            override val confirmationPositiveAction = "Do it!"
            override val confirmationMessage = "This is dangerous!"
            override val itemId = "test1"
            override val canBePinned = true
            override var groupId = "1"
            override val order = 1
            override val style = MenuItemStyle.Settings
            override val icon = MenuIcon.Settings
            override val title: String = getString("Automatic lights")
            override val billingManagerFeature = FEATURE_AUTOMATIC_LIGHTS

            override suspend fun onClicked(host: MenuHost?) {
                delay(1000)
            }
        }

    val Input: MenuItem
        get() = object : InputMenuItem {
            override val currentValue = MutableStateFlow("15")
            override val rightDetail = currentValue.map { "$it *" }
            override val inputHint = "A number"
            override val canBePinned = true
            override val itemId = "test1"
            override var groupId = "1"
            override val order = 1
            override val style = MenuItemStyle.Settings
            override val icon = MenuIcon.Settings
            override val title: String = getString("Automatic lights")
            override val billingManagerFeature = FEATURE_AUTOMATIC_LIGHTS

            override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? =
                "Invalid! Must be a number".takeIf { inputAsNumber == null }
                    ?: "Invalid! Must be less than 20".takeIf { inputAsNumber!!.toInt() !in (0 until 20) }

            override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
                delay(1000)
                currentValue.value = input
            }
        }

    val Toggle: ToggleMenuItem
        get() = object : ToggleMenuItem {
            override val state = MutableStateFlow(false)
            override val itemId = "test2"
            override var groupId = "1"
            override val canBePinned = true
            override val order = 1
            override val style = MenuItemStyle.Settings
            override val icon = MenuIcon.Fire
            override val title: String = getString("temperature_settings_menu___show_x", "Hotend")

            override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
                delay(300)
                state.value = enabled
            }
        }

    val RevolvingOptions: RevolvingOptionsMenuItem
        get() = object : RevolvingOptionsMenuItem {
            override val activeValue = MutableStateFlow("0")
            override val options = listOf(
                RevolvingOptionsMenuItem.Option(label = "First", value = "0"),
                RevolvingOptionsMenuItem.Option(label = "Second", value = "1"),
                RevolvingOptionsMenuItem.Option(label = "Third", value = "2"),
            )
            override val itemId = "test2"
            override var groupId = "1"
            override val order = 1
            override val canBePinned = true
            override val style = MenuItemStyle.Settings
            override val icon = MenuIcon.Fire
            override val title: String = getString("temperature_settings_menu___show_x", "Hotend")

            override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
                activeValue.value = option.value
            }
        }


    val Secondary: MenuItem
        get() = object : MenuItem {
            override val itemId = "test2"
            override var groupId = ""
            override val order = 1
            override val canBePinned = true
            override val style = MenuItemStyle.Settings
            override val icon = MenuIcon.Fire
            override val title: String = "Preheat PLA"
            override val secondaryButtonIcon = MenuIcon.More

            override suspend fun onClicked(host: MenuHost?) {
                delay(1500)
                throw IllegalStateException()
            }

            override suspend fun onSecondaryClicked(host: MenuHost?) {
                delay(1500)
                throw IllegalStateException()
            }
        }
}