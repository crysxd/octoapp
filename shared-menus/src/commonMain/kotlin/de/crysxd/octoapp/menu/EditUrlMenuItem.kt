package de.crysxd.octoapp.menu

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.TestNewUrlUseCase
import de.crysxd.octoapp.base.utils.urlFromStringInput
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class EditUrlMenuItem : MenuItem {
    protected val repository = SharedBaseInjector.getOrNull()?.printerConfigRepository

    protected fun validateUrlInput(input: String): String? {
        val url = urlFromStringInput(input)
        return getString("sign_in___discovery___error_invalid_url").takeIf { url == null }
    }

    protected suspend fun handleUrlInput(input: String, instanceId: String) = withContext(Dispatchers.SharedIO) {
        //region Test
        SharedBaseInjector.get().testNewUrlUseCase().execute(
            TestNewUrlUseCase.Params(
                instanceId = instanceId,
                newUrl = requireNotNull(urlFromStringInput(input)?.toUrl()) { "Url conversion failed" }
            )
        )
        //endregion
        //region Update
        SharedBaseInjector.get().printerConfigRepository.update(id = instanceId) {
            it.copy(webUrl = input.toUrl())
        }
        //endregion
    }
}