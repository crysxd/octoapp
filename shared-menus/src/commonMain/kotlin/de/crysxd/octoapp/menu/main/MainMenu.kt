package de.crysxd.octoapp.menu.main

import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuItemLibrary
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize

@CommonParcelize
class MainMenu(private val instanceId: String) : SimpleMenu {

    companion object {
        const val MainMenuHeaderView = "mainMenu"
        const val ResultOpenControlCenter = "openControlCenter"
        private val sharedLibrary by lazy { MenuItemLibrary() }
    }

    override val id get() = "main/$instanceId"
    override val title get() = ""
    override val customHeaderViewType get() = MainMenuHeaderView
    private val library get() = sharedLibrary
    override val suppressGrouping get() = true

    override suspend fun getMenuItems() = SharedBaseInjector.get().pinnedMenuItemRepository.getPinnedMenuItems(MenuId.MainMenu).mapNotNull {
        library[it, instanceId]
    }
}