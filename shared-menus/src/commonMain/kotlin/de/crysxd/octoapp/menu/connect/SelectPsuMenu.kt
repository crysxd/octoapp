package de.crysxd.octoapp.menu.connect

import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice.Capability.ControlPrinterPower
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
class SelectPsuMenu(val instanceId: String) : SimpleMenu {

    override val id get() = "SelectPsuMenu/$instanceId"
    override val title get() = getString("power_menu___title_select_device")

    override suspend fun getMenuItems(): List<MenuItem> {
        val useCase = SharedBaseInjector.get().getPowerDevicesUseCase()
        val params = GetPowerDevicesUseCase.Params(instanceId = instanceId, requiredCapabilities = listOf(ControlPrinterPower))
        val devices = useCase.execute(params)
        return devices.results.map { (device, _) ->
            SelectPsuMenuItem(
                instanceId = instanceId,
                uniqueId = device.uniqueId,
                displayName = device.displayName,
            )
        }
    }

    private class SelectPsuMenuItem(
        val instanceId: String,
        val uniqueId: String,
        val displayName: String
    ) : MenuItem {
        override val itemId = uniqueId
        override var groupId = ""
        override val order = 0
        override val style = MenuItemStyle.Printer
        override val icon = MenuIcon.PowerOutlet
        override val title = displayName
        override val canBePinned = false

        override suspend fun onClicked(host: MenuHost?) {
            // Set default
            SharedBaseInjector.get().printerConfigRepository.update(id = instanceId) {
                val devices = (it.appSettings?.defaultPowerDevices ?: emptyMap()).toMutableMap().also { map ->
                    map[AppSettings.DEFAULT_POWER_DEVICE_PSU] = uniqueId
                }

                it.copy(appSettings = (it.appSettings ?: AppSettings()).copy(defaultPowerDevices = devices))
            }

            // Close menu
            host?.closeMenu(uniqueId)
        }
    }
}