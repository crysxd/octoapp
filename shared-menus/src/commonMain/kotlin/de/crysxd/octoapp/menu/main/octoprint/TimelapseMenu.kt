package de.crysxd.octoapp.menu.main.octoprint

import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem.Option
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedcommon.utils.runForAtLeast
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlin.time.Duration.Companion.seconds

@CommonParcelize
class TimelapseMenu(
    private val instanceId: String,
    private val offerStartPrint: Boolean,
) : SimpleMenu {

    companion object {
        const val ResultStartPrint = "start_print"
    }

    override val id get() = "TimelapseMenu/$instanceId"
    override val title get() = getString("timelapse_config___title")

    override suspend fun getMenuItems(): List<MenuItem> {
        val repo = SharedBaseInjector.get().timelapseRepository
        val current = repo.getState(instanceId).first()?.config ?: runForAtLeast(1.seconds) { repo.fetchLatest(instanceId).config }

        return listOfNotNull(
            TimelapseModeMenuItem(instanceId = instanceId, repo = repo),
            TimelapseMinimumIntervalMenuItem(instanceId = instanceId, repo = repo, currentType = current.type),
            TimelapseZHopMenuItem(instanceId = instanceId, repo = repo, currentType = current.type),
            TimelapseFrameRateMenuItem(instanceId = instanceId, repo = repo, currentType = current.type),
            TimelapsePostRollMenuItem(instanceId = instanceId, repo = repo, currentType = current.type),
            TimelapseIntervalItem(instanceId = instanceId, repo = repo, currentType = current.type),

            AskForTimelapseBeforePrintingMenuItem(),
            PersistTimelapseConfigMenuItem(instanceId = instanceId, repo = repo),
            StartPrintMenuItem(ResultStartPrint).takeIf { offerStartPrint }
        )
    }
}

private fun validateInput(input: Float?, canBeZero: Boolean = true) = when {
    input == null -> getString("error_please_enter_a_value")
    canBeZero && input < 0 -> getString("error_please_enter_a_value")
    !canBeZero && input <= 0 -> getString("error_please_enter_a_value")
    else -> null
}

private class TimelapseModeMenuItem(
    private val repo: TimelapseRepository,
    private val instanceId: String
) : RevolvingOptionsMenuItem {
    override val itemId = "timelapse_mode"
    override var groupId = ""
    override val order = 262
    override val canBePinned = false
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.VideoCameraType
    override val title = getString("timelapse_config___mode")
    override val activeValue = repo.getState(instanceId).mapNotNull { it?.config?.type?.name }
    override val options = listOf(
        Option(label = getString("timelapse_config___mode___off"), value = TimelapseConfig.Type.Off.name),
        Option(label = getString("timelapse_config___mode___timed"), value = TimelapseConfig.Type.Timed.name),
        Option(label = getString("timelapse_config___mode___zchange"), value = TimelapseConfig.Type.ZChange.name),
    )

    override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
        repo.updateConfig(instanceId = instanceId) {
            copy(type = TimelapseConfig.Type.valueOf(option.value))
        }

        // Might change which items are shown
        host?.reloadMenu()
    }
}

private class TimelapseMinimumIntervalMenuItem(
    private val repo: TimelapseRepository,
    private val instanceId: String,
    private val currentType: TimelapseConfig.Type
) : InputMenuItem {
    override val itemId = "timelapse_minimum_interval"
    override var groupId = ""
    override val order = 263
    override val canBePinned = false
    override val keyboardType = InputMenuItem.KeyboardType.Numbers
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.TimeInterval
    override val title = getString("timelapse_config___min_interval")
    override val inputHint = title
    override val currentValue = repo.getState(instanceId).mapNotNull { it?.config?.minDelay?.format() }
    override val rightDetail = repo.getState(instanceId).mapNotNull { it?.config?.minDelay?.format() }

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = currentType == TimelapseConfig.Type.ZChange
    override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? = validateInput(inputAsNumber)
    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        repo.updateConfig(instanceId = instanceId) {
            copy(minDelay = requireNotNull(inputAsNumber) { "Number required" })
        }
    }
}

class TimelapseIntervalItem(
    private val repo: TimelapseRepository,
    private val instanceId: String,
    private val currentType: TimelapseConfig.Type
) : InputMenuItem {
    override val itemId = "timelapse_interval"
    override var groupId = ""
    override val order = 263
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.TimeInterval
    override val keyboardType = InputMenuItem.KeyboardType.Numbers
    override val title = getString("timelapse_config___interval")
    override val inputHint = title
    override val canBePinned = false
    override val currentValue = repo.getState(instanceId).mapNotNull { it?.config?.interval?.toString() }
    override val rightDetail = repo.getState(instanceId).mapNotNull { it?.config?.interval?.let { sec -> getString("x_secs", sec) } }

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = currentType == TimelapseConfig.Type.Timed
    override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? = validateInput(inputAsNumber, canBeZero = false)
    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        repo.updateConfig(instanceId = instanceId) {
            copy(interval = requireNotNull(inputAsNumber) { "Number required" }.toInt())
        }
    }
}

class TimelapseZHopMenuItem(
    private val repo: TimelapseRepository,
    private val instanceId: String,
    private val currentType: TimelapseConfig.Type
) : InputMenuItem {
    override val itemId = "timelapse_z_hop"
    override var groupId = ""
    override val order = 264
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.Height
    override val keyboardType = InputMenuItem.KeyboardType.Numbers
    override val title = getString("timelapse_config___retraction_z_hop")
    override val inputHint = title
    override val canBePinned = false
    override val currentValue = repo.getState(instanceId).mapNotNull { it?.config?.retractionZHop?.format() }
    override val rightDetail = repo.getState(instanceId).mapNotNull { it?.config?.retractionZHop?.formatAsLength() }

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = currentType == TimelapseConfig.Type.ZChange
    override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? = validateInput(inputAsNumber)
    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        repo.updateConfig(instanceId = instanceId) {
            copy(retractionZHop = requireNotNull(inputAsNumber) { "Number required" })
        }
    }
}

class TimelapseFrameRateMenuItem(
    private val repo: TimelapseRepository,
    private val instanceId: String,
    private val currentType: TimelapseConfig.Type
) : InputMenuItem {
    override val itemId = "timelapse_frame_rate"
    override var groupId = ""
    override val order = 265
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.ImageSequnce
    override val canBePinned = false
    override val keyboardType = InputMenuItem.KeyboardType.Numbers
    override val title = getString("timelapse_config___frame_rate")
    override val inputHint = title
    override val currentValue = repo.getState(instanceId).mapNotNull { it?.config?.fps?.toString() }
    override val rightDetail = repo.getState(instanceId).mapNotNull { it?.config?.fps?.let { fps -> getString("x_fps", fps) } }

    override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = currentType != TimelapseConfig.Type.Off
    override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? = validateInput(inputAsNumber, canBeZero = false)
    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        repo.updateConfig(instanceId = instanceId) {
            copy(fps = requireNotNull(inputAsNumber) { "Number required" }.toInt())
        }
    }
}

class TimelapsePostRollMenuItem(
    private val repo: TimelapseRepository,
    private val instanceId: String,
    private val currentType: TimelapseConfig.Type
) : InputMenuItem {
    override val itemId = "timelapse_post_roll"
    override var groupId = ""
    override val order = 266
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.PostRoll
    override val canBePinned = false
    override val keyboardType = InputMenuItem.KeyboardType.Numbers
    override val title = getString("timelapse_config___post_roll")
    override val inputHint = title
    override val currentValue = repo.getState(instanceId).mapNotNull { it?.config?.postRoll?.toString() }
    override val rightDetail = repo.getState(instanceId).mapNotNull { it?.config?.postRoll?.let { secs -> getString("x_secs", secs) } }

    override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = currentType != TimelapseConfig.Type.Off
    override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? = validateInput(inputAsNumber)
    override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
        repo.updateConfig(instanceId = instanceId) {
            copy(postRoll = requireNotNull(inputAsNumber) { "Number required" }.toInt())
        }
    }
}

class AskForTimelapseBeforePrintingMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.askForTimelapseBeforePrinting }
    override val itemId = "timelapse_ask_before_printing"
    override var groupId = "settings"
    override val order = 270
    override val canBePinned = false
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.Start
    override val title = getString("timelapse_config___ask_before_printing")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.askForTimelapseBeforePrinting = enabled
    }
}

class PersistTimelapseConfigMenuItem(
    private val repo: TimelapseRepository,
    private val instanceId: String,
) : MenuItem {
    override val itemId = "timelapse_persist"
    override var groupId = "settings"
    override val order = 271
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.Save
    override val canBePinned = false
    override val title = getString("timelapse_config___save_as_default")
    override val description = getString("timelapse_config___save_as_default___description")

    override suspend fun onClicked(host: MenuHost?) {
        repo.updateConfig(instanceId) {
            copy(save = true)
        }
    }
}

class StartPrintMenuItem(
    private val result: String
) : MenuItem {
    override val itemId = "timelapse_start_print"
    override var groupId = "print"
    override val order = 272
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.Send
    override val canBePinned = false
    override val title = getString("timelapse_config___start_print")

    override suspend fun onClicked(host: MenuHost?) {
        host?.closeMenu(result = result)
    }
}
