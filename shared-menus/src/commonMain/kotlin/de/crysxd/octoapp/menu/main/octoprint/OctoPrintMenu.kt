package de.crysxd.octoapp.menu.main.octoprint

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.ExecuteSystemCommandUseCase
import de.crysxd.octoapp.base.usecase.GetOctoPrintWebUrlUseCase
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.menu.ConfirmedMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.main.settings.ChangeInstanceMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.http.framework.isLocalResolvable
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedcommon.utils.parseHtml
import io.ktor.http.Url

@CommonParcelize
class OctoPrintMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "octoprint"
    override val title get() = getSystemInfo().interfaceType.label
    override val subtitle get() = getString("main_menu___submenu_subtitle")

    override suspend fun getMenuItems() = getBaseItems() + getSystemCommandItems()

    private fun getSystemInfo() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.systemInfo ?: SystemInfo()

    private fun getBaseItems(): List<MenuItem> {
        return listOfNotNull(
            ChangeOctoPrintInstanceMenuItem2(),
            ShowPluginLibraryOctoPrintMenuItem(instanceId = instanceId),
            OpenOctoPrintMenuItem(instanceId = instanceId),
            ConfigureRemoteAccessMenuItem(),
            ShowFilesMenuItem(instanceId = instanceId),
            TimelapseConfigMenuItem(instanceId = instanceId),
            TimelapseArchiveMenuItem(instanceId = instanceId),
        )
    }

    private fun getSystemCommandItems() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)
        ?.systemCommands
        ?.map { ExecuteSystemCommandMenuItem(source = it.source, action = it.action, instanceId = instanceId) }
        ?: listOf(
            ExecuteSystemCommandMenuItem(
                source = "dummy",
                action = "reboot",
                dummyName = "Reboot system", /* Same as "real", do not localise */
                instanceId = instanceId,
            ),
            ExecuteSystemCommandMenuItem(
                source = "dummy",
                action = "shutdown",
                showDummyDescription = true,
                dummyName = "Shutdown system", /* Same as "real", do not localise */
                instanceId = instanceId,
            ),
        )
}

internal class ChangeOctoPrintInstanceMenuItem2 : ChangeInstanceMenuItem() {
    override val itemId = MenuItems.MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
    override var groupId = "change"
    override val order = 252
    override val canBePinned = true
    override val style = MenuItemStyle.OctoPrint
}

internal class OpenOctoPrintMenuItem(
    private val instanceId: String,
    val webUrl: Url? = null
) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_OPEN_OCTOPRINT
    override var groupId = ""
    override val order = 200
    override val enforceSingleLine = false
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.OpenExternal
    override val canBePinned = true
    override val title = webUrl?.toString() ?: getString("main_menu___item_open_octoprint")

    override val secondaryButtonIcon
        get() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.let {
            val hasAlternativeUrl = it.alternativeWebUrl != null
            val hasLocalUrl = it.webUrl.isLocalResolvable()
            MenuIcon.More.takeIf { (hasLocalUrl || hasAlternativeUrl) && webUrl == null }
        }

    override suspend fun onClicked(host: MenuHost?) {
        val url = webUrl ?: SharedBaseInjector.get().getOctoPrintWebUrlUseCase().execute(GetOctoPrintWebUrlUseCase.Params(instanceId = instanceId))
        host?.openUrl(url)
    }

    override suspend fun onSecondaryClicked(host: MenuHost?) {
        host?.pushMenu(OpenOctoPrintMenu(instanceId))
    }
}

internal class ConfigureRemoteAccessMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_CONFIGURE_REMOTE_ACCESS
    override var groupId = "config"
    override val order = 251
    override val showAsSubMenu = true
    override val canBePinned = true
    override val enforceSingleLine = false
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.Cloud
    override val title = getString("main_menu___configure_remote_access")

    override suspend fun onClicked(host: MenuHost?) {
        host?.openUrl(UriLibrary.getConfigureRemoteAccessUri())
    }
}

internal class ShowFilesMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SHOW_FILES
    override var groupId = ""
    override val order = 202
    override val showAsSubMenu = true
    override val canBePinned = true
    override val enforceSingleLine = false
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.Files
    override val title = getString("main_menu___show_files")

    override suspend fun onClicked(host: MenuHost?) {
        val label = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.label
        host?.openUrl(UriLibrary.getFileManagerUri(instanceId = instanceId, label = label))
    }
}

internal class ExecuteSystemCommandMenuItem(
    private val instanceId: String,
    val source: String,
    val action: String,
    val dummyName: String? = null,
    val showDummyDescription: Boolean = false
) : ConfirmedMenuItem {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = itemId.split("/").let {
            ExecuteSystemCommandMenuItem(source = it[1], action = it[2], instanceId = instanceId)
        }
    }

    override val itemId = "${MenuItems.MENU_EXECUTE_SYSTEM_COMMAND}/$source/$action"
    override var groupId = "system"
    override val order = 210
    override val canBePinned = dummyName == null
    override val style = MenuItemStyle.OctoPrint
    override val icon = when (systemCommand?.type) {
        SystemCommand.Type.Reboot -> MenuIcon.ActionReboot
        SystemCommand.Type.Restart -> MenuIcon.ActionRestart
        SystemCommand.Type.RestartSafeMode -> MenuIcon.ActionRestartSafe
        SystemCommand.Type.Shutdown -> MenuIcon.ActionShutDown
        else -> MenuIcon.ActionGeneric
    }
    override val description = getString("main_menu___system_permission_missing").takeIf { showDummyDescription }
    override val title = systemCommand?.name ?: dummyName ?: "Unknown system command"
    override val confirmationMessage = systemCommand?.confirmation?.parseHtml()?.toString() ?: "Execute?"
    override val confirmationPositiveAction = systemCommand?.name ?: "OK"

    private val systemCommand
        get() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)
            ?.systemCommands
            ?.firstOrNull { it.source == source && it.action == action }

    override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = dummyName == null
    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = systemCommand != null || dummyName != null
    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().executeSystemCommandUseCase().execute(
            ExecuteSystemCommandUseCase.Params(systemCommand = systemCommand!!, instanceId = instanceId)
        )
    }
}

internal class ShowPluginLibraryOctoPrintMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_PLUGINS
    override var groupId = "config"
    override val order = 250
    override val showAsSubMenu = true
    override val canBePinned = true
    override val icon = MenuIcon.Plugins
    override val style = MenuItemStyle.OctoPrint
    override val title = getString("main_menu___explore_support_plugins")
    override val badgeCount
        get() = if (SharedBaseInjector.get().printerConfigRepository.getActiveInstanceSnapshot()?.hasPlugin(OctoPlugins.OctoApp) == false) {
            1
        } else {
            0
        }

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = SystemInfo.Capability.PrettyTerminal in systemInfo.capabilities

    override suspend fun onClicked(host: MenuHost?) {
        host?.openUrl(UriLibrary.getPluginLibraryUri())
    }
}

internal class TimelapseConfigMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_TIMELAPSE_CONFIG
    override var groupId = ""
    override val order = 203
    override val showAsSubMenu = true
    override val canBePinned = true
    override val icon = MenuIcon.TimelapseConfig
    override val style = MenuItemStyle.OctoPrint
    override val title = getString("main_menu___timelapse_config")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = SystemInfo.Capability.Timelapse in systemInfo.capabilities
    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(TimelapseMenu(instanceId = instanceId, offerStartPrint = false))
    }
}

internal class TimelapseArchiveMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_TIMELAPSE_ARCHIVE
    override var groupId = ""
    override val order = 204
    override val showAsSubMenu = true
    override val canBePinned = true
    override val style = MenuItemStyle.OctoPrint
    override val icon = MenuIcon.TimelapseArchive
    override val title = getString("main_menu___timelapse_archive")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = SystemInfo.Capability.Timelapse in systemInfo.capabilities
    override suspend fun onClicked(host: MenuHost?) {
        host?.openUrl(UriLibrary.getTimelapseArchiveUri())
    }
}