package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

@CommonParcelize
class CustomizeGcodeShortcutMenu(
    private val gcode: String,
    private val offerInsert: Boolean = false
) : Menu {

    private val repository get() = SharedBaseInjector.get().gcodeHistoryRepository
    private val gcodeHistoryItem get() = repository.getItem(gcode)
    override val id get() = "customize/${gcodeHistoryItem?.command}/$offerInsert"
    override val title get() = gcodeHistoryItem?.name ?: "???"
    override val subtitleFlow
        get() = repository.observeItem(gcode).map {
            if (it?.name == it?.label) {
                gcode
            } else {
                ""
            }
        }

    override suspend fun getMenuItems(): List<MenuItem> {
        val refreshedItem = repository.observeItem(gcode).first() ?: let {
            Napier.e(tag = "CustomizeGcodeShortcutMenu", message = "Not found", throwable = IllegalStateException("Launched for $gcode but was not found"))
            return emptyList()
        }

        return listOfNotNull(
            TogglePinnedMenuItem(repository, refreshedItem),
            InsertMenuItem(refreshedItem).takeIf { offerInsert },
            SetLabelMenuItem(repository, refreshedItem),
            ClearLabelMenuItem(repository, refreshedItem),
            RemoveMenuItem(repository, refreshedItem)
        )
    }

    private class TogglePinnedMenuItem(
        private val repo: GcodeHistoryRepository,
        private val command: GcodeHistoryItem
    ) : MenuItem {
        override val itemId = "pinned"
        override var groupId = ""
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val order = 1
        override val icon = if (command.isFavorite) MenuIcon.Unpinned else MenuIcon.Pinned
        override val title = getString("toggle_favorite")

        override suspend fun onClicked(host: MenuHost?) {
            repo.setFavorite(command.command, !command.isFavorite)
            host?.reloadMenu()
        }
    }

    private class InsertMenuItem(
        private val command: GcodeHistoryItem
    ) : MenuItem {
        override val itemId = "insert"
        override var groupId = ""
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val order = 2
        override val icon = MenuIcon.Paste
        override val title = getString("insert")

        override suspend fun onClicked(host: MenuHost?) {
            host?.closeMenu(result = command.command)
        }
    }

    private class SetLabelMenuItem(
        private val repo: GcodeHistoryRepository,
        private val command: GcodeHistoryItem
    ) : InputMenuItem {
        override val itemId = "label"
        override var groupId = ""
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val order = 3
        override val icon = MenuIcon.Label
        override val title = getString("enter_label")
        override val currentValue = repo.observeItem(command.command).map { it?.label ?: "" }
        override val inputHint = getString("enter_label")

        override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? = null

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            repo.setLabelForGcode(command = command.command, label = input.trim().takeUnless { it.isBlank() })
            menuHost?.reloadMenu()
        }
    }

    private class ClearLabelMenuItem(
        private val repo: GcodeHistoryRepository,
        private val command: GcodeHistoryItem
    ) : MenuItem {
        override val itemId = "clearLabel"
        override var groupId = ""
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val order = 4
        override val icon = MenuIcon.LabelOff
        override val title = getString("clear_lebel")

        override suspend fun onClicked(host: MenuHost?) {
            repo.setLabelForGcode(command.command, null)
        }
    }

    private class RemoveMenuItem(
        private val repo: GcodeHistoryRepository,
        private val command: GcodeHistoryItem
    ) : MenuItem {
        override val itemId = "remove"
        override var groupId = ""
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val order = 5
        override val icon = MenuIcon.Delete
        override val title = getString("remove_shortcut")

        override suspend fun onClicked(host: MenuHost?) {
            repo.removeEntry(command.command)
            host?.closeMenu()
        }
    }
}