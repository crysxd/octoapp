package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.data.models.AppTheme
import de.crysxd.octoapp.menu.MenuHost

expect object SettingsMenuHelper {
    suspend fun customizeControls(menuHost: MenuHost?)
    suspend fun transferPurchase(menuHost: MenuHost?)
    fun startLivePrintNotification(menuHost: MenuHost?)
    fun openNotificationSettings(menuHost: MenuHost?)
    fun getSupportedAppThemes(): List<AppTheme>
    fun restartApp()
}