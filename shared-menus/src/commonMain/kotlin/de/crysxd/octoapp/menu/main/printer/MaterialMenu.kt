package de.crysxd.octoapp.menu.main.printer

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.ActivateMaterialUseCase
import de.crysxd.octoapp.base.usecase.GetMaterialsUseCase
import de.crysxd.octoapp.base.utils.awaitItem
import de.crysxd.octoapp.engine.api.MaterialsApi.Companion.NoMaterialId
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.material.Material
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.isMoonraker
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.menu.MenuAnimation
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.main.printer.MaterialMenu.Companion.ResultStartPrint
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.flow

@CommonParcelize
class MaterialMenu(
    private val instanceId: String,
    private val startPrintAfterSelectionForPath: String?,
) : SimpleMenu {

    companion object {
        const val ResultStartPrint = "start_print"
    }

    private val isMoonraker get() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.systemInfo?.interfaceType?.isMoonraker == true
    private val startPrintAfterSelection get() = startPrintAfterSelectionForPath != null

    override val id get() = "materials/$instanceId/$startPrintAfterSelection"
    override val title get() = getString(if (startPrintAfterSelection) "material_menu___title_select_material" else "material_menu___title_neutral")
    override val subtitle get() = getString(if (startPrintAfterSelection) "material_menu___subtitle_select_material" else "material_menu___subtitle_neutral")
    override val bottomText get() = if (isMoonraker) getString("material_menu___bottom_text_moonraker") else getString("material_menu___bottom_text_octoprint")
    override val emptyStateActionText get() = getString("material_menu___empty_state_action")
    override val emptyStateSubtitle get() = getString("material_menu___empty_state")
    override val emptyStateAnimation get() = MenuAnimation.Materials
    override val emptyStateAction: (menuHost: MenuHost?) -> Unit?
        get() = {
            it?.openUrl(if (isMoonraker) "https://github.com/Donkie/Spoolman".toUrl() else UriLibrary.getPluginLibraryUri(category = "materials"))
        }

    override suspend fun getMenuItems(): List<MenuItem> {
        // Only start the print after selection if we are told so and if there is only one extruder. If there
        // are multiple extruders we need to wait until the user selected all
        val extruderCount = getExtruderComponents(instanceId).size
        val doStartPrintAfterSelection = extruderCount == 1 && startPrintAfterSelection
        val materials = SharedBaseInjector.get().getMaterialsUseCase().execute(
            GetMaterialsUseCase.Params(instanceId = instanceId)
        )

        val file = startPrintAfterSelectionForPath?.let { path ->
            try {
                SharedBaseInjector.get().fileListRepository
                    .observePath(instanceId = instanceId, path = path, fileOrigin = FileOrigin.Gcode)
                    .awaitItem() as? FileObject.File
            } catch (e: Exception) {
                Napier.e(tag = "MaterialMenu", message = "Failed to load file", throwable = e)
                null
            }
        }

        return materials.map { material ->
            ActivateMaterialMenuItem(
                material = material,
                startPrintAfterSelection = doStartPrintAfterSelection,
                instanceId = instanceId,
                requiredFilamentVolumes = file?.gcodeAnalysis?.filament?.map { it.value.volume }
            )
        } + listOfNotNull(
            ClearMaterialSelectionMenuItem(
                allMaterials = materials,
                instanceId = instanceId
            ),
            PrintWithoutMaterialSelection(
                allMaterials = materials,
                instanceId = instanceId
            ).takeIf { startPrintAfterSelection },
            PrintWithCurrentSelection().takeIf { startPrintAfterSelection }
        )
    }
}

private class ActivateMaterialMenuItem(
    private val material: Material,
    private val startPrintAfterSelection: Boolean = false,
    private val instanceId: String,
    private val requiredFilamentVolumes: List<Float?>? = null,
) : MenuItem {
    private val preferences = SharedBaseInjector.get().preferences
    private val extruders get() = getExtruderComponents(instanceId)
    private val activeExtruderIndex get() = material.activeExtruderComponent?.let { extruders.indexOf(it) }?.takeIf { it >= 0 }

    override val itemId = "material/${material.id}"
    override var groupId = if (material.isActivated && activeExtruderIndex != null) "active" else "other"
    override val order = activeExtruderIndex ?: 300
    override val style = MenuItemStyle.Printer
    override val canBePinned = false
    override val showSuccess = true
    override val icon
        get() = material.activeExtruderComponent?.let { active ->
            if (extruders.size == 1) {
                MenuIcon.MaterialActive
            } else when (extruders.indexOf(active)) {
                0 -> MenuIcon.MaterialActive0
                1 -> MenuIcon.MaterialActive1
                2 -> MenuIcon.MaterialActive2
                3 -> MenuIcon.MaterialActive3
                4 -> MenuIcon.MaterialActive4
                5 -> MenuIcon.MaterialActive5
                6 -> MenuIcon.MaterialActive6
                7 -> MenuIcon.MaterialActive7
                8 -> MenuIcon.MaterialActive8
                9 -> MenuIcon.MaterialActive9
                else -> MenuIcon.MaterialActive
            }
        } ?: MenuIcon.MaterialInactive

    override val title = material.displayName
    override val rightDetail = flow {
        // If we show all attributes, ignore the one coming from the material to ensure consistency
        val weight = material.weightGrams
        val requiredWeight = material.activeExtruderComponent?.let { activeExtruder ->
            requiredFilamentVolumes?.getOrNull(extruders.indexOf(activeExtruder))?.let { requiredFilamentVolume ->
                material.density?.let { density ->
                    requiredFilamentVolume * density
                }
            }
        }
        val weightLabel = listOf(requiredWeight, weight).mapNotNull { it?.format(maxDecimals = 0)?.plus("g") }.joinToString(" / ")
        val attributes = if (preferences.showAllMaterialProperties) {
            listOf(material.vendor, material.material)
        } else if (material.attributes.isNotEmpty()) {
            material.attributes
        } else {
            listOf(material.material)
        }

        emit("${attributes.joinToString()}\n$weightLabel".trim())
    }
    override val iconColorOverwrite: HexColor? = try {
        material.color
    } catch (e: Exception) {
        null
    }

    override suspend fun onClicked(host: MenuHost?) {
        when (extruders.size) {
            1 -> {
                SharedBaseInjector.get().activateMaterialUseCase().execute(
                    ActivateMaterialUseCase.Params(instanceId = instanceId, uniqueMaterialId = material.id, extruderComponent = extruders[0])
                )

                if (startPrintAfterSelection) {
                    host?.closeMenu(ResultStartPrint)
                } else {
                    host?.reloadMenu()
                }
            }

            else -> host?.pushMenu(
                MaterialExtruderSelectionMenu(
                    instanceId = instanceId,
                    extruderComponents = extruders,
                    offerStartPrint = startPrintAfterSelection,
                    material = material
                )
            )
        }
    }
}

private open class ClearMaterialSelectionMenuItem(
    private val instanceId: String,
    private val allMaterials: List<Material>,
) : MenuItem {
    override val itemId = "print-no-selection"
    override var groupId = "top"
    override val order = -1
    override val canBePinned = false
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.MaterialReset
    override val title = getString("material_menu___clear_selection")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = allMaterials.any { it.isActivated }

    override suspend fun onClicked(host: MenuHost?) {
        val extruders = getExtruderComponents(instanceId)

        allMaterials
            .map { it.id.providerId }
            .distinct()
            .forEach { providerId ->
                extruders.forEach { extruderComponent ->
                    SharedBaseInjector.get().activateMaterialUseCase().execute(
                        ActivateMaterialUseCase.Params(
                            instanceId = instanceId,
                            uniqueMaterialId = NoMaterialId.copy(providerId = providerId),
                            extruderComponent = extruderComponent,
                        )
                    )
                }
            }

        Napier.i(tag = "ClearMaterialSelectionMenuItem", message = "All materials removed")
        onAfterClicked(host)
    }

    protected open suspend fun onAfterClicked(host: MenuHost?) {
        host?.reloadMenu()
    }
}

private class PrintWithoutMaterialSelection(
    instanceId: String,
    allMaterials: List<Material>,
) : ClearMaterialSelectionMenuItem(
    instanceId = instanceId,
    allMaterials = allMaterials,
) {
    private val hasMmu = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.let {
        it.hasPlugin(OctoPlugins.Mmu2FilamentSelect) || it.hasPlugin(OctoPlugins.PrusaMmu)
    } == true

    override val itemId = "clear-selection"
    override val order = -3
    override var groupId = "top"
    override val icon = MenuIcon.Automatic
    override val title = getString(if (hasMmu) "material_menu___print_without_selection_for_mmu" else "material_menu___print_without_selection")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = true

    override suspend fun onAfterClicked(host: MenuHost?) {
        super.onAfterClicked(host)
        host?.closeMenu(ResultStartPrint)
    }
}

private class PrintWithCurrentSelection : MenuItem {
    override val itemId = "current-selection"
    override val order = -2
    override val style = MenuItemStyle.Printer
    override val canBePinned = false
    override val icon = MenuIcon.Printer
    override var groupId = "top"
    override val title = getString("material_menu___print_with_current_selection")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = true

    override suspend fun onClicked(host: MenuHost?) {
        host?.closeMenu(ResultStartPrint)
    }
}

private fun getExtruderComponents(
    instanceId: String
): List<String> = requireNotNull(SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.activeProfile?.extruders?.flatMap { it.extruderComponents }) {
    "Missing extruders for $instanceId"
}

