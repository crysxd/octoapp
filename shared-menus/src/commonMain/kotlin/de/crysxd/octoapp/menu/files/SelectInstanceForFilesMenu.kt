package de.crysxd.octoapp.menu.files

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.controlcenter.ManageOctoPrintInstancesMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
class SelectInstanceForFilesMenu(val activeInstanceId: String) : SimpleMenu {

    override val id get() = "select-instance-for-files"
    override val title get() = "Show files for printer"

    override suspend fun getMenuItems() = SharedBaseInjector.get().printerConfigRepository.getAll().mapIndexed { index, config ->
        InstanceMenuItem(
            index = index,
            config = config,
            active = config.id == activeInstanceId
        )
    } + ManagePrintFarmMenuItem()

    private class InstanceMenuItem(index: Int, config: PrinterConfigurationV3, active: Boolean) : MenuItem {
        override val itemId = config.id
        override var groupId = ""
        override val order = index
        override val style = MenuItemStyle.OctoPrint
        override val canBePinned = false
        override val icon = if (active) MenuIcon.CheckActive else MenuIcon.Switch
        override val title = config.label

        override suspend fun onClicked(host: MenuHost?) {
            host?.closeMenu(itemId)
        }
    }

    private class ManagePrintFarmMenuItem() : MenuItem {
        override val itemId = "manage"
        override var groupId = "manage"
        override val order = Int.MAX_VALUE
        override val style = MenuItemStyle.Settings
        override val canBePinned = false
        override val icon = MenuIcon.Printer
        override val title = getString("control_center___manage_menu___title")

        override suspend fun onClicked(host: MenuHost?) {
            host?.pushMenu(ManageOctoPrintInstancesMenu())
        }
    }
}