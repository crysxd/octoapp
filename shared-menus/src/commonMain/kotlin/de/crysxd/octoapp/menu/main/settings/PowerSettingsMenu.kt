package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_POWER_INSTANCE
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_RESET_DEFAULT_POWER_DEVICES
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
internal class PowerSettingsMenu : SimpleMenu {

    override val id get() = "PowerSettingsMenu"
    override val title get() = getString("main_menu___menu_settings_title")
    override val subtitle get() = getString("main_menu___submenu_subtitle")

    override suspend fun getMenuItems(): List<MenuItem> {
        val instances = SharedBaseInjector.get().printerConfigRepository.getAll()

        return if (instances.size == 1) {
            listOf(
                AutomaticLightsSettingsMenuItem(instanceId = instances[0].id),
                ConfirmPowerOffSettingsMenuItem(instanceId = instances[0].id),
                ResetDefaultPowerDevicesMenuItem(),
            )
        } else {
            instances.map {
                PowerSettingsInstanceMenuItem(instanceId = it.id, instanceLabel = it.label)
            } + ResetDefaultPowerDevicesMenuItem()
        }
    }
}

internal class ResetDefaultPowerDevicesMenuItem : MenuItem {
    override val itemId = MENU_ITEM_RESET_DEFAULT_POWER_DEVICES
    override var groupId = "top"
    override val order = 104
    override val canBePinned = true
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Reset
    override val title = getString("main_menu___item_reset_default_power_devices")

    override suspend fun onClicked(host: MenuHost?) {
        val repo = SharedBaseInjector.get().printerConfigRepository
        repo.getAll().forEach { instance ->
            repo.updateAppSettings(id = instance.id) { it.copy(defaultPowerDevices = null) }
        }
    }
}

internal class PowerSettingsInstanceMenuItem(private val instanceId: String, private val instanceLabel: String) : MenuItem {
    override val itemId = "$MENU_ITEM_POWER_INSTANCE/$instanceId"
    override var groupId = ""
    override val order = 118
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Lights
    override val showAsSubMenu = true
    override val canRunWithAppInBackground = false
    override val canBePinned = true
    override val title = instanceLabel

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(PowerSettingsInstanceMenu(instanceId = instanceId, instanceLabel = instanceLabel))
    }
}
