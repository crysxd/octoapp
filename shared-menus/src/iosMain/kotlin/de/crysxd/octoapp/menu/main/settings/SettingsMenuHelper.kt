package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.data.models.AppTheme
import de.crysxd.octoapp.menu.MenuHost
import platform.Foundation.NSURL
import platform.UIKit.UIApplication
import platform.UIKit.UIApplicationOpenSettingsURLString
import platform.UserNotifications.UNAuthorizationOptionAlert
import platform.UserNotifications.UNAuthorizationOptionSound
import platform.UserNotifications.UNAuthorizationStatusNotDetermined
import platform.UserNotifications.UNUserNotificationCenter
import platform.posix.exit

actual object SettingsMenuHelper {

    private const val tag = "SettingsMenuHelper"

    actual fun startLivePrintNotification(menuHost: MenuHost?) = Unit

    actual fun openNotificationSettings(menuHost: MenuHost?) {
        fun doOpen() {
            val url = NSURL(string = UIApplicationOpenSettingsURLString)
            UIApplication.sharedApplication.openURL(url)
        }

        UNUserNotificationCenter.currentNotificationCenter().getNotificationSettingsWithCompletionHandler { settings ->
            when (settings?.authorizationStatus) {
                // We did not yet ask, the settings wont' be shown
                UNAuthorizationStatusNotDetermined -> UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions(
                    options = UNAuthorizationOptionAlert or UNAuthorizationOptionSound
                ) { _, _ ->
                    // No longer undetermined, now we can open settings
                    doOpen()
                }

                // State is determined, we asked before for notifications and settings are shown
                else -> doOpen()
            }
        }
    }

    actual suspend fun customizeControls(menuHost: MenuHost?) {
        menuHost?.closeMenu(SettingsMenu.ResultCustomizeControls)
    }

    actual fun getSupportedAppThemes(): List<AppTheme> = AppTheme.entries

    actual suspend fun transferPurchase(menuHost: MenuHost?) {
        throw UnsupportedOperationException("Not available on iOS")
    }

    actual fun restartApp() {
        exit(0)
    }
}