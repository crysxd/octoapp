import de.crysxd.octoapp.buildscript.octoAppAndroidLibrary

plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinAndroid)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinComposeCompiler)
    alias(libs.plugins.androidxSafeArgs)
}

octoAppAndroidLibrary("baseui") {
    namespace = "de.crysxd.baseui"
}

dependencies {
    api(projects.base)

    // General
    api(libs.androidx.browser)
    api(libs.lottie)
    api(libs.lottie.compose)
    api(libs.zxing)
    api(libs.accompanist.flowlayout)
    api(libs.picasso)

    // Compose UI
    api(project.dependencies.platform(libs.compose.bom))
    api(libs.compose.ui)
    api(libs.compose.foundation)
    api(libs.compose.material)
    api(libs.compose.navigation)
    api(libs.compose.runtime)
    api(libs.compose.animation)
    api(libs.compose.constraintlayout)
    api(libs.compose.ui.tooling.preview)
    api(libs.androidx.activity.compose)
    api(libs.androidx.lifecycle.viewmodel.compose)
    api(libs.reorderable)

    // XML UI
    api(libs.androidx.appcompat)
    api(libs.androidx.constraintlayout)
    api(libs.androidx.navigation.fragment.ktx)
    api(libs.androidx.navigation.ui.ktx)
    api(libs.androidx.material)
    api(libs.androidx.swiperefreshlayout)
    api(libs.removesoon.transitionseverywhere)

    // Markdown
    api(libs.markwon.core)
    api(libs.markwon.image)
    api(libs.markwon.linkify)

    // Tooling / Test
    debugApi(libs.compose.ui.test)
    debugApi(libs.compose.ui.tooling)
//    debugApi "androidx.customview:customview:1.2.0-alpha02"
//    debugApi "androidx.customview:customview-poolingcontainer:1.0.0"
}