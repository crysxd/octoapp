package de.crysxd.baseui.utils

import androidx.lifecycle.ViewModel
import de.crysxd.baseui.ext.connectCore
import de.crysxd.octoapp.viewmodels.BaseViewModelCore

abstract class ViewModelWithCore : ViewModel() {
    protected abstract val core: BaseViewModelCore

    init {
        connectCore { core }
    }
}