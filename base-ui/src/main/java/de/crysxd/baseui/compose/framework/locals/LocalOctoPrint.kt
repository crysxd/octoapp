package de.crysxd.baseui.compose.framework.locals

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import de.crysxd.baseui.compose.theme.LocalOctoAppColors
import de.crysxd.baseui.compose.theme.OctoAppColors
import de.crysxd.baseui.ext.toCompose
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.seconds

val LocalOctoPrint = compositionLocalOf<PrinterConfigurationV3> { throw IllegalStateException("No OctoPrint active") }

@Composable
fun WithOctoPrint(instanceId: String?, content: @Composable () -> Unit) {
    val repo = remember { BaseInjector.get().octorPrintRepository() }
    val octoPrintFlow = remember(instanceId) { repo.instanceInformationFlow(instanceId) }
    val octoPrint by octoPrintFlow.collectAsState(repo.get(instanceId))
    val tag = "WithOctoPrint"

    octoPrint?.let {
        CompositionLocalProvider(
            LocalOctoPrint provides it,
            LocalOctoAppColors provides LocalOctoAppColors.current.forOctoPrint(octoPrint)
        ) {
            content()
        }
    } ?: Unit.let {
        val (activeRepo, activePrefs, broken) = remember(instanceId) {
            val activeRepo = repo.getActiveInstanceSnapshot()?.id
            val activePrefs = SharedBaseInjector.get().preferences.activeInstanceId
            val broken = (activeRepo != null || activePrefs != null) && repo.getAll().isNotEmpty()
            Napier.w(tag = tag, message = "activeRepo=$activeRepo, activePrefs=$activePrefs")
            Triple(activeRepo, activePrefs, broken)
        }

        LaunchedEffect(broken) {
            if (broken && instanceId != null) {
                delay(1.seconds)
                val all = repo.getAll().map { it.id }
                Napier.e(
                    tag = tag,
                    throwable = IllegalStateException("No octoprint $instanceId in $all (activeRepo=$activeRepo, activePrefs=$activePrefs)"),
                    message = "Broken"
                )
            }
        }

        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            if (broken) {
                Text(text = "...")
            } else {
                Text(text = ":::")
            }
        }
    }
}

private fun OctoAppColors.forOctoPrint(instance: PrinterConfigurationV3?) = object : OctoAppColors by this {

    override val activeColorScheme: Color
        @Composable get() = (if (isSystemInDarkTheme()) instance.colors.dark.main else instance.colors.light.main).toCompose()

    override val activeColorSchemeLight: Color
        @Composable get() = (if (isSystemInDarkTheme()) instance.colors.dark.accent else instance.colors.light.accent).toCompose()

}