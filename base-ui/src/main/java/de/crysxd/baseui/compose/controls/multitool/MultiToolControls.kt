package de.crysxd.baseui.compose.controls.multitool

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview

@Composable
fun MultiToolControls(
    state: MultiToolControlsState,
    editState: EditState,
) = AnimatedVisibility(
    visible = state.toolCount > 1,
    enter = fadeIn() + expandVertically(),
    exit = fadeOut() + shrinkVertically()
) {
    MultiToolControlsScaffold(
        state = state,
        editState = editState,
    )
}

//region Internals
@Composable
private fun MultiToolControlsScaffold(
    state: MultiToolControlsState,
    editState: EditState,
) = ControlsScaffold(
    title = stringResource(id = R.string.widget_multi_tool___title),
    editState = editState,
) {
    Box(
        modifier = Modifier
            .clip(MaterialTheme.shapes.small)
            .background(OctoAppTheme.colors.inputBackground)
            .height(IntrinsicSize.Min)
    ) {
        var sliderPosition by remember { mutableStateOf(Rect.Zero) }

        Slider(position = sliderPosition)

        Row {
            Options(state = state) { activePosition ->
                sliderPosition = activePosition
            }
        }
    }

    Text(
        text = stringResource(R.string.widget_multi_tool___explainer),
        style = OctoAppTheme.typography.labelSmall,
        textAlign = TextAlign.Center,
        color = OctoAppTheme.colors.lightText,
        modifier = Modifier.padding(top = OctoAppTheme.dimens.margin01)
    )
}

@Composable
private fun RowScope.Options(
    state: MultiToolControlsState,
    reportSize: (Rect) -> Unit,
) = (-1 until state.toolCount).forEach { toolIndex ->
    val toolIndexFixed = toolIndex.takeIf { it >= 0 }
    Option(
        toolIndex = toolIndexFixed,
        activeIndex = state.activeToolIndex,
        activate = { state.activateTool(toolIndexFixed) },
        reportPosition = reportSize,
    )

    val isLast = toolIndex == state.toolCount - 1
    if (!isLast) {
        val borderingActive = state.activeToolIndex == toolIndexFixed || state.activeToolIndex == (toolIndex + 1)
        Separator(visible = !borderingActive)
    }
}

@Composable
private fun Slider(position: Rect) {
    if (position == Rect.Zero) return

    val animatedOffset by animateFloatAsState(targetValue = position.topLeft.x, animationSpec = spring())
    val animatedWidth by animateFloatAsState(targetValue = position.size.width, animationSpec = spring())
    val color = OctoAppTheme.colors.accent
    val cornerRadius = OctoAppTheme.dimens.cornerRadiusSmall

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .drawBehind {
                drawRoundRect(
                    color = color,
                    size = Size(width = animatedWidth, height = this.size.height),
                    topLeft = Offset(x = animatedOffset, y = 0f),
                    cornerRadius = CornerRadius(cornerRadius.toPx(), cornerRadius.toPx())
                )
            }
            .fillMaxHeight()
            .padding(vertical = OctoAppTheme.dimens.margin01)
            .background(OctoAppTheme.colors.accent.copy(alpha = 0.2f))
    )
}

@Composable
private fun Separator(visible: Boolean) {
    val animatedAlpha by animateFloatAsState(targetValue = if (visible) 1f else 0f)
    Box(
        modifier = Modifier
            .width(1.dp)
            .fillMaxHeight()
            .graphicsLayer { alpha = animatedAlpha }
            .padding(vertical = OctoAppTheme.dimens.margin01)
            .background(OctoAppTheme.colors.accent.copy(alpha = 0.2f))
    )
}


@Composable
private fun RowScope.Option(
    toolIndex: Int?,
    activeIndex: Int?,
    activate: () -> Unit,
    reportPosition: (Rect) -> Unit,
) {
    val active = toolIndex == activeIndex
    val foreground = if (active) OctoAppTheme.colors.textColoredBackground else OctoAppTheme.colors.accent
    val text by animateColorAsState(targetValue = foreground)
    var position by remember { mutableStateOf(Rect.Zero) }


    LaunchedEffect(active, position) {
        if (active) {
            reportPosition(position)
        }
    }

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .height(48.dp)
            .weight(1f)
            .fillMaxWidth()
            .onGloballyPositioned { position = Rect(offset = it.positionInParent(), size = it.size.toSize()) }
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                onClick = activate,
                indication = rememberRipple(color = foreground, bounded = false)
            )
    ) {
        Text(
            text = toolIndex?.let { "${toolIndex + 1}" } ?: stringResource(id = R.string.target_off),
            color = text,
            style = OctoAppTheme.typography.buttonSmall,
        )
    }
}

//endregion
//region State
@Composable
fun rememberMultiToolControlState(): MultiToolControlsState {
    val vmFactory = MultiToolControlsViewModel.Factory(LocalOctoPrint.current.id)
    val context = LocalContext.current
    val vm = viewModel(
        modelClass = MultiToolControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    val state = vm.state.collectAsState()

    return remember(context, vm) {
        object : MultiToolControlsState {
            override val activeToolIndex by derivedStateOf { state.value.activeToolIndex }
            override val toolCount by derivedStateOf { state.value.toolCount }
            override fun activateTool(toolIndex: Int?) = vm.activateTool(toolIndex)
        }
    }
}

interface MultiToolControlsState {
    val activeToolIndex: Int?
    val toolCount: Int
    fun activateTool(toolIndex: Int?)
}

//endregion
//region Previews
@Preview
@Composable
private fun PreviewHidden() = OctoAppThemeForPreview {
    MultiToolControls(
        editState = EditState.ForPreview,
        state = object : MultiToolControlsState {
            override val activeToolIndex = 1
            override val toolCount = 1
            override fun activateTool(toolIndex: Int?) = Unit
        }
    )
}

@Preview
@Composable
private fun PreviewVisible() = OctoAppThemeForPreview {
    MultiToolControls(
        editState = EditState.ForPreview,
        state = object : MultiToolControlsState {
            override val activeToolIndex = 1
            override val toolCount: Int = 2
            override fun activateTool(toolIndex: Int?) = Unit
        }
    )
}

@Preview
@Composable
private fun PreviewMany() = OctoAppThemeForPreview {
    var active by remember { mutableStateOf<Int?>(2) }
    MultiToolControls(
        editState = EditState.ForPreview,
        state = object : MultiToolControlsState {
            override val activeToolIndex = active
            override val toolCount: Int = 4
            override fun activateTool(toolIndex: Int?) {
                active = toolIndex
            }
        }
    )
}
//endregion