package de.crysxd.baseui.compose.controls.tune

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.expandIn
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.common.controls.ControlsFragmentDirections
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.engine.models.tune.TuneState
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

@Composable
fun TuneControls(
    state: TuneControlsState,
    editState: EditState
) = ControlsScaffold(
    title = null,
    editState = editState,
) {
    TuneControlsInternals(state = state.state) {
        state.openFullscreen()
    }
}

interface TuneControlsState {
    val state: TuneState
    fun openFullscreen()
}

@Composable
fun rememberTuneControlsState(): TuneControlsState {
    val instanceId = LocalOctoPrint.current.id
    val vmFactory = TuneControlsViewModel.Factory(instanceId)
    val vm = viewModel(
        modelClass = TuneControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = LocalOctoActivity.current ?: requireNotNull(LocalViewModelStoreOwner.current)
    )

    val controller = OctoAppTheme.navController
    val state = vm.state.collectAsStateWhileActive(key = "tune", initial = TuneState())
    return object : TuneControlsState {
        override val state by state
        override fun openFullscreen() {
            controller().navigate(ControlsFragmentDirections.actionTune(instanceId))
        }
    }
}

//region Internals
@Composable
private fun TuneControlsInternals(
    state: TuneState,
    onClick: () -> Unit = {},
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier
        .fillMaxWidth()
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.inputBackground)
        .clickable(onClick = onClick)
        .padding(OctoAppTheme.dimens.margin2)
) {
    Text(
        text = stringResource(id = R.string.tune),
        style = OctoAppTheme.typography.sectionHeader,
        modifier = Modifier.weight(1f),
        color = OctoAppTheme.colors.darkText
    )

    Component(
        icon = R.drawable.ic_fan_20px,
        value = state.fans.firstOrNull { it.isPartCoolingFan }?.speed,
        contentDescription = R.string.cd_fan_speed
    )
    Component(
        icon = R.drawable.ic_flow_20px,
        value = state.print?.flowRate,
        contentDescription = R.string.cd_flow_rate
    )
    Component(
        icon = R.drawable.ic_speed_20px,
        value = state.print?.feedRate,
        contentDescription = R.string.cd_feed_rate
    )

    Icon(
        tint = OctoAppTheme.colors.darkText,
        painter = painterResource(id = R.drawable.ic_round_chevron_right_24),
        contentDescription = null
    )
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun RowScope.Component(
    @DrawableRes icon: Int,
    @StringRes contentDescription: Int,
    value: Float?,
) = AnimatedVisibility(
    visible = value != null,
    enter = fadeIn() + slideInVertically { it / 2 } + expandIn(),
    exit = fadeOut() + slideOutVertically { it / 2 } + shrinkOut(),
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(start = OctoAppTheme.dimens.margin1)
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = stringResource(id = contentDescription),
            tint = OctoAppTheme.colors.normalText
        )
        AnimatedContent(value) { target ->
            Text(
                text = (target ?: 0).formatAsPercent(),
                style = OctoAppTheme.typography.label,
                color = OctoAppTheme.colors.normalText,
                modifier = Modifier.padding(start = OctoAppTheme.dimens.margin0)
            )
        }
    }
}
//endregion

//region Previews
@Composable
@Preview
private fun Preview() = OctoAppThemeForPreview {
    val fan = TuneState.Fan(
        component = "fan",
        label = "Fan",
        speed = 100f,
        isPartCoolingFan = true,
    )

    var state by remember {
        mutableStateOf(
            TuneState(
                print = TuneState.Print(
                    flowRate = 100f,
                    feedRate = 100f
                ),
                fans = listOf(fan.copy(speed = 100f)),
                offsets = TuneState.Offsets(
                    z = -0.015f
                ),
                isValuesImprecise = true,
            )
        )
    }

    LaunchedEffect(Unit) {
        while (isActive) {
            state = TuneState(print = TuneState.Print(), fans = emptyList(), isValuesImprecise = true)
            delay(500)
            state = state.copy(print = state.print?.copy(flowRate = 100f))
            delay(1000)
            state = state.copy(print = state.print?.copy(feedRate = 100f))
            delay(1000)
            state = state.copy(fans = listOf(fan.copy(speed = 30f)))
            delay(1000)
            state = state.copy(fans = listOf(fan.copy(speed = 100f)))
            delay(1000)
        }
    }

    TuneControlsInternals(state)
}
//endregion