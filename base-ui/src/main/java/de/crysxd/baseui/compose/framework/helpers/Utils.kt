package de.crysxd.baseui.compose.framework.helpers

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.text.Spanned
import android.text.style.StyleSpan
import android.text.style.URLSpan
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow

@SuppressLint("UnnecessaryComposedModifier")
fun Modifier.thenIf(condition: Boolean, block: @Composable Modifier.() -> Modifier) = composed {
    if (condition) {
        block()
    } else {
        this
    }
}

@Composable
fun CharSequence.asAnnotatedString() = if (this is AnnotatedString) this else buildAnnotatedString {
    append(this@asAnnotatedString.toString())

    if (this@asAnnotatedString is Spanned) {
        getSpans(0, this@asAnnotatedString.length, StyleSpan::class.java).forEach {
            addStyle(
                start = getSpanStart(it),
                end = getSpanEnd(it),
                style = when (it.style) {
                    Typeface.BOLD -> SpanStyle(fontWeight = FontWeight.Bold)
                    Typeface.ITALIC -> SpanStyle(fontStyle = FontStyle.Italic)
                    Typeface.BOLD_ITALIC -> SpanStyle(fontStyle = FontStyle.Italic, fontWeight = FontWeight.Bold)
                    else -> SpanStyle()
                },
            )
        }

        getSpans(0, this@asAnnotatedString.length, URLSpan::class.java).forEach {
            addStringAnnotation(
                tag = "URL",
                annotation = it.url,
                start = getSpanStart(it),
                end = getSpanEnd(it)
            )
            addStyle(
                start = getSpanStart(it),
                end = getSpanEnd(it),
                style = SpanStyle(
                    color = OctoAppTheme.colors.accent,
                    textDecoration = TextDecoration.Underline
                ),
            )
        }
    }
}

@Suppress("UNCHECKED_CAST")
private object SavedData {

    private val data = mutableMapOf<String, Any?>()
    fun <T : Any?> save(instanceId: String, key: String, item: T) = data.set("$instanceId/$key", item)
    fun <T : Any?> get(instanceId: String, key: String, default: T) = data.getOrElse("$instanceId/$key") { default } as T

}

@Composable
fun rememberAndroidLifecycleActive(
    logTag: String? = null,
): Boolean {
    var active by remember { mutableStateOf(false) }
    val lifecycle = LocalLifecycleOwner.current.lifecycle
    DisposableEffect(Unit) {
        logTag?.let { Napier.i(tag = it, message = "Setting up lifecycle observer") }
        val observer = object : DefaultLifecycleObserver {
            override fun onResume(owner: LifecycleOwner) {
                super.onResume(owner)
                logTag?.let { Napier.i(tag = it, message = "Lifecycle active") }
                active = true
            }

            override fun onPause(owner: LifecycleOwner) {
                super.onPause(owner)
                logTag?.let { Napier.i(tag = it, message = "Lifecycle inactive") }
                active = false
            }
        }

        lifecycle.addObserver(observer)

        onDispose {
            lifecycle.removeObserver(observer)
        }
    }

    return active
}

@Composable
fun <T : Any?> Flow<T>.collectAsStateWhileActive(
    initial: T,
    logTag: String? = null,
    key: String,
): State<T> {
    val instanceId = LocalOctoPrint.current.id
    val state = remember { mutableStateOf(SavedData.get(instanceId = instanceId, key = key, default = initial)) }
    val active = rememberAndroidLifecycleActive(logTag = logTag)

    LaunchedEffect(active, key, instanceId) {
        if (active) {
            logTag?.let { Napier.i(tag = it, message = "Collection active") }
            state.value = SavedData.get(instanceId = instanceId, key = key, default = initial)
            collect {
                state.value = it
                SavedData.save(instanceId = instanceId, key = key, item = it)
            }
        } else {
            logTag?.let { Napier.i(tag = it, message = "Collection stopped") }
        }
    }

    return state
}

@Composable
fun rememberPrinterConfig(instanceId: String): State<PrinterConfigurationV3> {
    val config = remember {
        mutableStateOf(
            requireNotNull(SharedBaseInjector.get().printerConfigRepository.get(instanceId)) { "Missing instance $instanceId" }
        )
    }

    LaunchedEffect(instanceId) {
        SharedBaseInjector.get().printerConfigRepository
            .instanceInformationFlow(instanceId)
            .collect { config.value = it ?: config.value }
    }

    return config
}