package de.crysxd.baseui.compose.controls.cancelobject

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOcto
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.viewmodels.ext.printBed
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn

class CancelObjectRenderViewModel(
    instanceId: String,
    octoPreferences: OctoPreferences,
    printerConfigurationRepository: PrinterConfigurationRepository,
) : ViewModel() {

    val state = combine(
        octoPreferences.updatedFlow2.distinctUntilChangedBy { it.gcodePreviewSettings },
        printerConfigurationRepository.instanceInformationFlow(instanceId).distinctUntilChangedBy { it?.activeProfile },
    ) { prefs, instance ->
        @Suppress("USELESS_CAST")
        State.Ready(
            settings = prefs.gcodePreviewSettings,
            printerProfile = requireNotNull(instance?.activeProfile) { "Missing printer profile" },
            printBed = instance.printBed,
        ) as State
    }.onStart {
        emit(State.Loading)
    }.shareIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribedOcto,
        replay = 1
    )

    sealed class State {
        data object Loading : State()
        data class Error(val error: Throwable) : State()
        data class Ready(
            val settings: GcodePreviewSettings,
            val printBed: GcodeNativeCanvas.Image,
            val printerProfile: PrinterProfile,
        ) : State()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = CancelObjectRenderViewModel(
            instanceId = instanceId,
            printerConfigurationRepository = BaseInjector.get().octorPrintRepository(),
            octoPreferences = BaseInjector.get().octoPreferences()
        ) as T
    }
}