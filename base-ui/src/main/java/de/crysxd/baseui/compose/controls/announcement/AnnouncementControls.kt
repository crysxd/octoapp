package de.crysxd.baseui.compose.controls.announcement

import android.net.Uri
import android.os.Build
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.components.AnnouncementId
import de.crysxd.baseui.compose.framework.components.OctoAnnouncementController
import de.crysxd.baseui.compose.framework.components.SmallAnnouncement
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.screens.companion.CompanionAnnouncementBottomSheet
import de.crysxd.baseui.compose.screens.notifications.RequestNotificationBottomSheet
import de.crysxd.baseui.compose.theme.LocalFragmentManager
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.viewmodels.AnnouncementControlsViewModelCore

@Composable
fun AnnouncementControls(
    editState: EditState,
) = AnimatedVisibility(
    visible = !editState.editing,
    enter = expandVertically(expandFrom = Alignment.Top) + fadeIn(),
    exit = shrinkVertically(shrinkTowards = Alignment.Top) + fadeOut(),
) {
    Column(
        modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin2),
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
    ) {
        val factory = AnnouncementControlsViewModel.Factory(LocalOctoPrint.current.id)
        val viewModel = viewModel(
            modelClass = AnnouncementControlsViewModel::class.java,
            factory = factory,
            key = factory.id,
        )
        val fragmentManager = LocalFragmentManager.current
        val state by viewModel.state.collectAsState(initial = AnnouncementControlsViewModelCore.State())
        var showCompanionMenu by remember { mutableStateOf(false) }

        state.announcements.forEach { announcement ->
            OctoAnnouncementController(announcementId = AnnouncementId.Default(announcement.id)) {
                SmallAnnouncement(
                    text = announcement.text.toHtml(),
                    learnMoreButton = announcement.learnMoreText,
                    foregroundColor = if (announcement.redColor) OctoAppTheme.colors.red else OctoAppTheme.colors.primaryButtonBackground,
                    backgroundColor = if (announcement.redColor) OctoAppTheme.colors.redTranslucent else OctoAppTheme.colors.inputBackground,
                    hideButton = if (announcement.canHide) stringResource(id = R.string.hide) else null,
                    onLearnMore = {
                        when (announcement.learnMoreLink) {
                            null -> Unit
                            AnnouncementControlsViewModelCore.LinkCompanionInstallSheet -> CompanionAnnouncementBottomSheet().show(fragmentManager(), "companion")
                            AnnouncementControlsViewModelCore.LinkCompanionRunningMenu -> showCompanionMenu = true
                            AnnouncementControlsViewModelCore.LinkNotificationSheet -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                                RequestNotificationBottomSheet().show(fragmentManager(), "companion")
                            }

                            else -> Uri.parse(announcement.learnMoreLink).open()
                        }
                    },
                )
            }
        }
    }
}

//region Preview
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    AnnouncementControls(editState = EditState.ForPreview)
}
//endregion
