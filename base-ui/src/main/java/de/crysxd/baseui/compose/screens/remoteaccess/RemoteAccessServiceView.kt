package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.viewmodels.RemoteAccessBaseViewModelCore.State

@Composable
fun RemoteAccessServiceCard(
    modifier: Modifier = Modifier,
    backgroundColor: Color,
    content: @Composable ColumnScope.() -> Unit
) = Column(
    modifier = modifier
        .shadow(elevation = 2.dp, shape = MaterialTheme.shapes.large)
        .background(backgroundColor, shape = MaterialTheme.shapes.large)
        .clip(MaterialTheme.shapes.large)
        .fillMaxSize()
        .verticalScroll(rememberScrollState())
        .nestedScroll(LocalNestedScrollConnection.current)
        .fillMaxSize()
) {
    content()
}

@Composable
fun RemoteAccessServiceView(
    controller: ConfigureRemoteAccessViewController,
    backgroundColor: Color,
    modifier: Modifier = Modifier,
    titleBarBackgroundColor: Color,
    titleBarElevation: Dp = 0.dp,
    titleBarTagText: String? = null,
    titleBarTagColor: Color = Color.Magenta,
    boxBackgroundColor: Color,
    foregroundColor: Color,
    titleLogo: Painter,
    descriptionText: String,
    bottomText: String? = null,
    usps: List<RemoteAccessUsp>,
    content: @Composable (State) -> Unit,
) = Crossfade(controller.state, modifier = modifier) { s ->
    RemoteAccessServiceCard(
        backgroundColor = backgroundColor,
    ) {
        val failure = s.failure

        TitleBar(
            backgroundColor = titleBarBackgroundColor,
            logo = titleLogo,
            titleBarElevation = titleBarElevation,
            tagText = titleBarTagText,
            tagColor = titleBarTagColor,
        )

        Description(
            text = descriptionText,
            color = foregroundColor
        )

        content(s)

        if (failure != null) {
            RemoteAccessFailureView(
                failure = failure,
                backgroundColor = boxBackgroundColor,
                foregroundColor = foregroundColor,
                modifier = Modifier.padding(OctoAppTheme.dimens.margin2),
            )
        } else if (!s.connected) {
            RemoteAccessUspView(
                backgroundColor = boxBackgroundColor,
                foregroundColor = foregroundColor,
                usps = usps,
                modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
            )
        }

        BottomText(
            text = bottomText,
            color = foregroundColor,
        )
    }
}

@Composable
private fun TitleBar(
    backgroundColor: Color,
    logo: Painter,
    titleBarElevation: Dp,
    tagColor: Color,
    tagText: String?
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier
        .shadow(elevation = titleBarElevation)
        .background(backgroundColor)
        .fillMaxWidth()
        .padding(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin12)
        .height(32.dp)
) {
    Image(
        painter = logo,
        contentDescription = null,
    )

    Spacer(modifier = Modifier.weight(1f))

    tagText?.let { text ->
        Text(
            text = text,
            style = OctoAppTheme.typography.base,
            color = backgroundColor,
            modifier = Modifier
                .background(tagColor, shape = RoundedCornerShape(percent = 50))
                .padding(horizontal = OctoAppTheme.dimens.margin1, vertical = OctoAppTheme.dimens.margin0)
        )
    }
}

@Composable
private fun Description(
    text: String,
    color: Color,
) = Text(
    text = text,
    style = OctoAppTheme.typography.base,
    textAlign = TextAlign.Center,
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2),
    color = color,
)

@Composable
private fun ColumnScope.BottomText(
    text: String?,
    color: Color,
) = text?.let {
    Spacer(modifier = Modifier.weight(1f))

    Text(
        text = text,
        style = OctoAppTheme.typography.labelSmall,
        textAlign = TextAlign.Center,
        modifier = Modifier.padding(OctoAppTheme.dimens.margin2),
        color = color.copy(alpha = 0.75f),
    )
}


//region State
interface ConfigureRemoteAccessViewController {
    val state: State
    suspend fun disconnect(): Boolean
}

//endregion
//region Preview
@Composable
private fun PreviewBase(connected: Boolean, failure: RemoteConnectionFailure?) = OctoAppThemeForPreview {
    RemoteAccessServiceView(
        controller = object : ConfigureRemoteAccessViewController {
            override val state = State(connected = connected, loading = false, failure = failure, url = null)
            override suspend fun disconnect() = false
        },
        backgroundColor = Color(0xFF25272a),
        titleBarBackgroundColor = colorResource(id = R.color.octoeverywhere),
        boxBackgroundColor = colorResource(id = R.color.white_translucent_3),
        foregroundColor = OctoAppTheme.colors.textColoredBackground,
        titleLogo = painterResource(R.drawable.octoeverywhere_logo),
        descriptionText = stringResource(R.string.configure_remote_acces___octoeverywhere___description_1),
        content = { _ ->
            Box(
                modifier = Modifier
                    .background(Color.Red)
                    .fillMaxWidth()
                    .height(72.dp)
            )
        },
        modifier = Modifier,
        usps = listOf(
            RemoteAccessUsp(
                icon = R.drawable.ic_round_network_check_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_1)
            ),
            RemoteAccessUsp(
                icon = R.drawable.ic_round_videocam_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_2)
            ),
            RemoteAccessUsp(
                icon = R.drawable.ic_round_star_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_3)
            ),
            RemoteAccessUsp(
                icon = R.drawable.ic_round_lock_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_4)
            )
        )
    )
}

@Composable
@Preview(showSystemUi = true)
private fun PreviewIdle() = PreviewBase(connected = false, failure = null)

@Composable
@Preview(showSystemUi = true)
private fun PreviewFailure() = PreviewBase(
    connected = false,
    failure = RemoteConnectionFailure(
        errorMessage = "Some",
        errorMessageStack = "",
        stackTrace = "",
        remoteServiceName = "",
        dateMillis = 0
    )
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewConnected() = PreviewBase(connected = true, failure = null)
//endregion