package de.crysxd.baseui.compose.controls.quickaccess

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.spring
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.with
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.net.toUri
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.components.AnnouncementId
import de.crysxd.baseui.compose.framework.components.LargeAnnouncement
import de.crysxd.baseui.compose.framework.components.OctoAnnouncementController
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.menu.EmptyState
import de.crysxd.baseui.compose.menu.LocalAndroidMenuHost
import de.crysxd.baseui.compose.menu.MenuList
import de.crysxd.baseui.compose.menu.PreparedMenu
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.controls.QuickAccessMenu
import de.crysxd.octoapp.menu.main.settings.AndroidMenuHost
import de.crysxd.octoapp.viewmodels.QuickAccessControlsViewModelCore
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun QuickAccessControls(
    editState: EditState,
    state: QuickAccessState,
) = AnimatedVisibility(
    visible = state.hasItems || editState.editing,
    enter = fadeIn() + expandVertically(),
    exit = fadeOut() + shrinkVertically(),
) {
    ControlsScaffold(
        title = stringResource(id = R.string.widget_quick_access),
        editState = editState,
        actionIcon = painterResource(id = R.drawable.ic_round_info_24),
        onAction = { _, _ ->
            //region Explainer dialog
            OctoActivity.instance?.showDialog(
                OctoActivity.Message.DialogMessage(
                    title = { getString(R.string.widget_quick_access___tutorial_title) },
                    text = { getString(R.string.widget_quick_access___tutorial_detail) },
                    neutralButton = { getString(R.string.widget_quick_access___tutorial_learn_more) },
                    neutralAction = { UriLibrary.getCustomizeTutorialUri().open() }
                )
            )
            //endregion
        }
    ) {
        //region Tutorial
        OctoAnnouncementController(announcementId = AnnouncementId.Default("customization_tutorial_quick_access")) {
            LargeAnnouncement(
                title = stringResource(id = R.string.widget_quick_access___tutorial_title),
                text = stringResource(id = R.string.widget_quick_access___tutorial_detail),
                learnMoreButton = stringResource(id = R.string.widget_quick_access___tutorial_learn_more),
                onLearnMore = { UriLibrary.getCustomizeTutorialUri().open() },
                modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin2)
            )
        }
        //endregion
        //region Menu
        AnimatedContent(
            targetState = state,
            transitionSpec = { fadeIn(spring()) with fadeOut(spring()) }
        ) { s ->
            MenuList(
                menu = s.menu,
                menuHost = s.menuHost,
                horizontalMargin = 0.dp,
                menuId = s.menuId,
                useGrouping = false,
                useDescriptions = false,
                useEmptyTitle = false,
            )
        }
        //endregion
    }
}

data class QuickAccessState(
    val menuHost: MenuHost,
    val menu: PreparedMenu,
    val menuId: MenuId,
    val hasItems: Boolean,
)

@Composable
fun rememberQuickAccessState(): QuickAccessState {
    val instanceId = LocalOctoPrint.current.id
    val androidMenuHost = LocalAndroidMenuHost.current
    val vmFactory = QuickAccessControlsViewModel.Factory(instanceId)
    val vm = viewModel(modelClass = QuickAccessControlsViewModel::class.java, factory = vmFactory, key = vmFactory.id)
    val state by vm.state.collectAsStateWhileActive(initial = QuickAccessControlsViewModelCore.State(MenuId.Other, emptySet()), key = vmFactory.id)
    var reloadId by remember { mutableStateOf(1) }
    val menu = remember(reloadId, state.itemIds) { QuickAccessMenu(instanceId = instanceId, itemIds = state.itemIds) }
    var subMenu by remember { mutableStateOf<Menu?>(null) }

    subMenu?.let {
        MenuSheet(it) {
            subMenu = null
        }
    }

    return QuickAccessState(
        menuHost = remember(instanceId) {
            QuickAccessMenuHost(
                androidMenuHost = androidMenuHost,
                onShowMenu = { subMenu = it },
                onReload = { reloadId++ },
                instanceId = instanceId
            )
        },
        hasItems = state.itemIds.isNotEmpty(),
        menuId = state.menuId,
        menu = PreparedMenu(
            raw = menu,
            subtitle = null,
            items = runBlocking { menu.getMenuItems() },
            bottomText = null,
            title = null,
            emptyState = EmptyState(null, null) {},
            customHeaderViewType = null,
            customViewType = null,
        ),
    )
}

private class QuickAccessMenuHost(
    androidMenuHost: AndroidMenuHost,
    private val onShowMenu: (Menu) -> Unit,
    private val onReload: () -> Unit,
    private val instanceId: String,
) : MenuHost, AndroidMenuHost by androidMenuHost {
    override suspend fun pushMenu(subMenu: Menu) = onShowMenu(subMenu)
    override suspend fun popMenu() = Unit
    override suspend fun closeMenu(result: String?) = Unit
    override suspend fun reloadMenu() = onReload()
    override fun openUrl(url: Url) = url.toString().toUri().open()
}
