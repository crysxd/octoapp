package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.net.toUri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.EnterValueDialogInputConfig
import de.crysxd.baseui.compose.framework.components.LocalEnterValueProvider
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.connectCore
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.base.utils.urlFromStringInput
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.ObicoDataUsage
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.viewmodels.RemoteAccessBaseViewModelCore.State
import de.crysxd.octoapp.viewmodels.RemoteAccessObicoViewModelCore
import kotlinx.coroutines.delay
import java.text.DateFormat
import kotlin.time.Duration.Companion.seconds

private val buttonColor
    @Composable get() = OctoAppTheme.colors.spaghettiDetective

private val boxBackgroundColor
    @Composable get() = colorResource(id = R.color.white_translucent)

private val foregroundColor
    @Composable get() = OctoAppTheme.colors.black

@Composable
fun RemoteAccessObicoView(
    controller: ConfigureObicoViewController = rememberObicoController()
) = RemoteAccessServiceView(
    controller = controller,
    backgroundColor = Color(0xFFEFF3FB),
    titleBarBackgroundColor = Color(0xFFEFF3FB),
    boxBackgroundColor = boxBackgroundColor,
    foregroundColor = foregroundColor,
    titleBarTagColor = buttonColor,
    titleBarTagText = stringResource(id = R.string.configure_remote_acces___free_tier_available),
    titleLogo = painterResource(R.drawable.spaghetti_detective_logo),
    descriptionText = stringResource(R.string.configure_remote_acces___spaghetti_detective___description),
    modifier = Modifier,
    usps = listOf(
        RemoteAccessUsp(
            icon = R.drawable.ic_round_network_check_24,
            description = stringResource(R.string.configure_remote_acces___spaghetti_detective___usp_1, 300, "$4")
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_videocam_24,
            description = stringResource(R.string.configure_remote_acces___spaghetti_detective___usp_2)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_remove_red_eye_24,
            description = stringResource(R.string.configure_remote_acces___spaghetti_detective___usp_3)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_lock_24,
            description = stringResource(R.string.configure_remote_acces___spaghetti_detective___usp_4)
        )
    )
) { state ->
    Column(
        modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin2)
    ) {
        if (state.connected) {
            Connected(controller)
        } else {
            Disconnected(controller)
        }
    }
}

@Composable
private fun Connected(
    controller: ConfigureObicoViewController,
) = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin2),
    horizontalAlignment = Alignment.CenterHorizontally,
) {
    RemoteAccessConnected(
        text = stringResource(id = R.string.configure_remote_acces___spaghetti_detective___connected),
    )

    OctoButton(
        text = stringResource(id = R.string.configure_remote_acces___spaghetti_detective___disconnect_button),
        onClick = { controller.disconnect() },
        loading = controller.state.loading,
        type = OctoButtonType.Special(
            background = { Color.White },
            foreground = { buttonColor },
            shape = { RoundedCornerShape(percent = 50) },
            stroke = { buttonColor },
            suppressShadow = false,
        )
    )

    DataUsage(controller = controller)
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun DataUsage(controller: ConfigureObicoViewController) = AnimatedContent(
    targetState = controller.dataUsage,
    transitionSpec = { fadeIn() with fadeOut() }
) { state ->
    if (!state.shouldBeShown) return@AnimatedContent
    val dataUsage = (state as? FlowState.Ready)?.data

    Row(
        modifier = Modifier
            .background(boxBackgroundColor, MaterialTheme.shapes.large)
            .padding(vertical = OctoAppTheme.dimens.margin1)
            .padding(start = OctoAppTheme.dimens.margin2, end = OctoAppTheme.dimens.margin1),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    ) {
        Column(
            verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
            modifier = Modifier.weight(1f)
        ) {
            Text(
                text = stringResource(id = R.string.configure_remote_acces___spaghetti_detective___data_usafe),
                style = OctoAppTheme.typography.sectionHeader,
                color = foregroundColor,
            )

            LinearProgressIndicator(
                modifier = Modifier.fillMaxWidth(),
                color = buttonColor,
                progress = dataUsage?.run { totalBytes / monthlyCapBytes }?.toFloat() ?: 0f
            )

            Text(
                text = when (state) {
                    is FlowState.Error -> stringResource(id = R.string.configure_remote_acces___spaghetti_detective___data_usage_failed)
                    is FlowState.Loading -> stringResource(id = R.string.loading)
                    is FlowState.Ready -> stringResource(
                        id = R.string.configure_remote_acces___spaghetti_detective___data_usage_limited,
                        state.data.totalBytes.toLong().formatAsFileSize(),
                        state.data.monthlyCapBytes.toLong().formatAsFileSize(),
                        DateFormat.getDateInstance(DateFormat.SHORT).format(System.currentTimeMillis() + state.data.resetInSeconds * 1000)
                    )
                },
                style = OctoAppTheme.typography.labelSmall,
                color = foregroundColor.copy(alpha = 0.75f),
            )
        }

        OctoIconButton(
            painter = painterResource(id = R.drawable.ic_round_refresh_24),
            contentDescription = null,
            onClick = controller::reloadDataUsage,
            loading = state is FlowState.Loading
        )
    }
}

private val FlowState<ObicoDataUsage>.shouldBeShown get() = this !is FlowState.Ready || this.data.hasDataCap

@Composable
private fun Disconnected(controller: ConfigureObicoViewController) {
    val enterValueProvider = LocalEnterValueProvider.current
    val enterValueTitle = stringResource(R.string.configure_remote_acces___spaghetti_detective___custom_title)
    val enterValueConfirm = stringResource(R.string.configure_remote_acces___spaghetti_detective___custom_action)
    val enterValueHint = stringResource(R.string.configure_remote_acces___spaghetti_detective___custom_hint)
    val enterValueError = stringResource(R.string.configure_remote_acces___spaghetti_detective___custom_error)
    val activity = OctoAppTheme.octoActivity

    OctoButton(
        text = stringResource(id = R.string.configure_remote_acces___spaghetti_detective___connect_button),
        onClick = {
            val uri = controller.getLoginUrl(null)?.toUri() ?: return@OctoButton
            activity?.showDialog(
                OctoActivity.Message.DialogMessage(
                    title = { getString(R.string.configure_remote_acces___leaving_app___title) },
                    text = { getString(R.string.configure_remote_acces___leaving_app___text, "Obico") },
                    positiveButton = { getString(R.string.sign_in___continue) },
                    positiveAction = { uri.open() },

                    )
            )
        },
        loading = controller.state.loading,
        type = OctoButtonType.Special(
            background = { buttonColor },
            foreground = { Color.White },
            shape = { RoundedCornerShape(percent = 50) },
            stroke = { Color.Transparent },
            suppressShadow = false
        )
    )

    OctoButton(
        text = stringResource(id = R.string.configure_remote_acces___spaghetti_detective___custom_button),
        onClick = {
            enterValueProvider.requestEnterValues(
                title = enterValueTitle,
                confirmButton = enterValueConfirm,
                inputs = listOf(
                    EnterValueDialogInputConfig(
                        label = "",
                        value = "",
                        labelActive = enterValueHint,
                        keyboard = InputMenuItem.KeyboardType.Url,
                        validator = { input ->
                            enterValueError.takeIf { urlFromStringInput(input) == null }
                        },
                    )
                )
            )?.first()?.let(::urlFromStringInput)?.let { baseUrl ->
                controller.getLoginUrl(baseUrl)
            }?.toUri()?.open()
        },
        small = true,
        modifier = Modifier.padding(top = OctoAppTheme.dimens.margin01),
        type = OctoButtonType.Special(
            background = { Color.Transparent },
            foreground = { buttonColor },
            shape = { RoundedCornerShape(percent = 50) },
            stroke = { Color.Transparent },
            suppressShadow = true
        )
    )
}

//region State
@Composable
private fun rememberObicoController(): ConfigureObicoViewController {
    val instanceId = LocalOctoPrint.current.id
    val factory = ConfigureObicoViewModel.Factory(instanceId)
    val viewModel = viewModel<ConfigureObicoViewModel>(
        factory = factory,
        key = factory.id,
    )
    return object : ConfigureObicoViewController {
        override val state by viewModel.connectedState.collectAsState(
            initial = State(
                connected = false,
                loading = false,
                failure = null,
                url = null
            )
        )
        override val dataUsage by viewModel.dataUsage.collectAsState(initial = FlowState.Loading())

        override suspend fun getLoginUrl(baseUrl: String?) = viewModel.getLoginUrl(baseUrl)
        override suspend fun disconnect() = viewModel.disconnect()
        override suspend fun reloadDataUsage() = viewModel.reloadDataUsage()
    }
}

interface ConfigureObicoViewController : ConfigureRemoteAccessViewController {
    val dataUsage: FlowState<ObicoDataUsage>
    suspend fun getLoginUrl(baseUrl: String?): String?
    suspend fun reloadDataUsage()
}

private class ConfigureObicoViewModel(instanceId: String) : ViewModel() {
    val core = RemoteAccessObicoViewModelCore(instanceId)
    val connectedState = core.connectedState
    val dataUsage = core.dataUsage

    init {
        connectCore { core }
    }

    suspend fun getLoginUrl(baseUrl: String?) = core.getLoginUrl(baseUrl)
    suspend fun disconnect() = core.disconnect()
    suspend fun reloadDataUsage() = core.reloadDataUsage()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ConfigureObicoViewModel(
            instanceId = instanceId,
        ) as T
    }
}

//endregion
//region Preview
@Composable
private fun PreviewBase(
    connected: Boolean,
    failure: RemoteConnectionFailure?,
    dataUsage: FlowState<ObicoDataUsage> = FlowState.Loading(),
) = OctoAppThemeForPreview {
    RemoteAccessObicoView(
        controller = object : ConfigureObicoViewController {
            override val state = State(connected = connected, loading = false, failure = failure, url = null)
            override val dataUsage = dataUsage

            override suspend fun getLoginUrl(baseUrl: String?): String? = null
            override suspend fun reloadDataUsage() = delay(2.seconds)
            override suspend fun disconnect() = false
        }
    )
}

@Composable
@Preview(showSystemUi = true)
private fun PreviewIdle() = PreviewBase(connected = false, failure = null)

@Composable
@Preview(showSystemUi = true)
private fun PreviewFailure() = PreviewBase(
    connected = false,
    failure = RemoteConnectionFailure(
        errorMessage = "Some",
        errorMessageStack = "",
        stackTrace = "",
        remoteServiceName = "",
        dateMillis = 0
    )
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewConnectedDataUsageLoading() = PreviewBase(
    connected = true,
    failure = null,
    dataUsage = FlowState.Loading()
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewConnectedDataUsageReady() = PreviewBase(
    connected = true,
    failure = null,
    dataUsage = FlowState.Ready(ObicoDataUsage(monthlyCapBytes = 500_000_000.0, totalBytes = 123_000_000.0, resetInSeconds = 1373.0))
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewConnectedDataUsageNoCap() = PreviewBase(
    connected = true,
    failure = null,
    dataUsage = FlowState.Ready(ObicoDataUsage(monthlyCapBytes = -1.0, totalBytes = 123_000_000.0, resetInSeconds = 1373.0))
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewConnectedDataUsageFailed() = PreviewBase(
    connected = true,
    failure = null,
    dataUsage = FlowState.Error(IllegalStateException("Some"))
)
//endregion