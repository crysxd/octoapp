package de.crysxd.baseui.compose.framework.helpers

import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.State
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import de.crysxd.baseui.compose.framework.locals.LocalOctoAppInsets
import de.crysxd.baseui.compose.framework.locals.WithOctoPrint
import de.crysxd.baseui.compose.menu.DefaultAndroidMenuHost
import de.crysxd.baseui.compose.menu.LocalAndroidMenuHost
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.requireOctoActivity

@Suppress("FunctionName")
fun Fragment.ComposeContentWithoutOctoPrint(
    insets: State<Rect> = staticStateOf(Rect()),
    block: @Composable () -> Unit
): View {
    val view = ComposeView(requireContext())
    view.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
    view.setContent {
        OctoAppTheme(
            viewModelStoreOwner = requireActivity(),
            octoActivity = requireOctoActivity(),
            fragmentManager = { childFragmentManager },
        ) {
            val androidMenuHost = remember { DefaultAndroidMenuHost(this) }

            CompositionLocalProvider(
                LocalOctoAppInsets provides insets.value,
                LocalAndroidMenuHost provides androidMenuHost
            ) {
                block()
            }
        }
    }
    return view
}

@Suppress("FunctionName")
fun Fragment.ComposeContent(
    instanceId: String?,
    insets: State<Rect> = staticStateOf(Rect()),
    block: @Composable () -> Unit
) = ComposeContentWithoutOctoPrint(insets) {
    WithOctoPrint(instanceId = instanceId) {
        block()
    }
}