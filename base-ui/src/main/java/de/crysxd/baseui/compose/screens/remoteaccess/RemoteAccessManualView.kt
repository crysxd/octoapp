package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoInputField
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.connectCore
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import de.crysxd.octoapp.viewmodels.RemoteAccessBaseViewModelCore
import de.crysxd.octoapp.viewmodels.RemoteAccessManualViewModelCore
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

@Composable
fun RemoteAccessManualView(
    controller: ConfigureManualRemoteAccessViewController = rememberManualRemoteAccessController()
) = RemoteAccessServiceCard(
    backgroundColor = OctoAppTheme.colors.inputBackground,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
    ) {
        Info(controller)
        Form(controller)
    }
}

@Composable
private fun showDetails(controller: ConfigureManualRemoteAccessViewController): State<Boolean> {
    val isImeVisible = WindowInsets.ime.getBottom(LocalDensity.current) > 0
    val hasData = controller.url.isNotEmpty()
    return rememberUpdatedState(isImeVisible || hasData)
}

@Composable
private fun ColumnScope.Info(controller: ConfigureManualRemoteAccessViewController) = AnimatedVisibility(
    visible = !showDetails(controller).value
) {
    Text(
        text = stringResource(id = R.string.configure_remote_acces___manual___description),
        style = OctoAppTheme.typography.base,
        textAlign = TextAlign.Center,
        color = OctoAppTheme.colors.darkText,
        modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin2)
    )
}

@Composable
@OptIn(ExperimentalComposeUiApi::class)
private fun ColumnScope.Form(controller: ConfigureManualRemoteAccessViewController) {
    val verticalPadding = OctoAppTheme.dimens.margin1
    val keyboard = LocalSoftwareKeyboardController.current
    val scope = rememberCoroutineScope()
    var saved by remember { mutableStateOf(false) }
    val showInfo by showDetails(controller)
    suspend fun save(skipTest: Boolean) {
        keyboard?.hide()
        if (controller.setManual(skipTest)) scope.launch {
            saved = true
            delay(2.seconds)
            saved = false
        }
    }

    val typeLabel = LocalOctoPrint.current.systemInfo?.interfaceType.label
    OctoInputField(
        value = controller.url,
        onValueChanged = { controller.url = it },
        label = stringResource(id = R.string.sign_in___discover___web_url_hint, typeLabel),
        labelActive = stringResource(id = R.string.sign_in___discover___web_url_hint_active),
        placeholder = stringResource(id = R.string.sign_in___discover___web_url_hint, typeLabel),
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Uri, autoCorrect = false, capitalization = KeyboardCapitalization.None),
        keyboardActions = KeyboardActions(onAny = { keyboard?.hide() }),
        alternativeBackground = true,
        modifier = Modifier.padding(bottom = verticalPadding)
    )

    AnimatedVisibility(visible = showInfo) {
        Column(
            verticalArrangement = Arrangement.spacedBy(verticalPadding),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            OctoInputField(
                value = controller.user,
                onValueChanged = { controller.user = it },
                label = stringResource(id = R.string.configure_remote_access___manual___username_hint),
                labelActive = stringResource(id = R.string.configure_remote_access___manual___username_hint_active),
                placeholder = stringResource(id = R.string.configure_remote_access___manual___username_hint),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Uri, autoCorrect = false, capitalization = KeyboardCapitalization.None),
                keyboardActions = KeyboardActions(onAny = { keyboard?.hide() }),
                alternativeBackground = true,
            )

            OctoInputField(
                value = controller.password,
                onValueChanged = { controller.password = it },
                label = stringResource(id = R.string.configure_remote_access___manual___password_hint),
                labelActive = stringResource(id = R.string.configure_remote_access___manual___password_hint_active),
                placeholder = stringResource(id = R.string.configure_remote_access___manual___password_hint),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password, autoCorrect = false, capitalization = KeyboardCapitalization.None),
                keyboardActions = KeyboardActions(onAny = { keyboard?.hide() }),
                alternativeBackground = true,
            )
        }
    }

    OctoButton(
        text = stringResource(id = R.string.configure_remote_acces___manual___button),
        loading = controller.state.loading,
        onClick = { save(skipTest = false) },
        modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2)
    )

    AnimatedVisibility(visible = showInfo) {
        OctoButton(
            text = stringResource(id = R.string.configure_remote_acces___manual___button_no_tests),
            loading = controller.state.loading,
            onClick = { save(skipTest = true) },
            small = true,
            type = OctoButtonType.Link,
            modifier = Modifier.padding(top = OctoAppTheme.dimens.margin1, bottom = OctoAppTheme.dimens.margin1)
        )
    }

    AnimatedVisibility(saved) {
        RemoteAccessConnected(text = stringResource(id = R.string.configure_remote_acces___remote_access_configured))
    }
}

//region State
@Composable
private fun rememberManualRemoteAccessController(): ConfigureManualRemoteAccessViewController {
    val instanceId = LocalOctoPrint.current.id
    val factory = ConfigureManualRemoteAccessViewModel.Factory(instanceId)
    val viewModel = viewModel<ConfigureManualRemoteAccessViewModel>(factory = factory, key = factory.id)
    return object : ConfigureManualRemoteAccessViewController {
        override val state by viewModel.connectedState.collectAsState(
            initial = RemoteAccessBaseViewModelCore.State(
                connected = false,
                loading = false,
                failure = null,
                url = null
            )
        )

        override var url by viewModel.url
        override var user by viewModel.user
        override var password by viewModel.password

        override suspend fun disconnect() = viewModel.disconnect()
        override suspend fun setManual(skipTest: Boolean) = viewModel.setManual(skipTest)
    }
}

interface ConfigureManualRemoteAccessViewController : ConfigureRemoteAccessViewController {
    var url: String
    var user: String
    var password: String

    suspend fun setManual(skipTest: Boolean): Boolean
}

private class ConfigureManualRemoteAccessViewModel(instanceId: String) : BaseViewModel() {
    val core = connectCore(RemoteAccessManualViewModelCore(instanceId))
    val url = mutableStateOf("")
    val user = mutableStateOf("")
    val password = mutableStateOf("")

    val connectedState = core.connectedState.distinctUntilChanged().onEach { state ->
        val stateUrl = state.url ?: ""
        if (stateUrl == "") {
            url.value = ""
            user.value = ""
            password.value = ""
        } else {
            val u = stateUrl.toUrl()
            url.value = u.withoutBasicAuth().toString()
            user.value = u.user ?: ""
            password.value = u.password ?: ""
        }
    }

    suspend fun disconnect() = core.disconnect()
    suspend fun setManual(skipTest: Boolean) = core.setManual(
        url = url.value,
        user = user.value,
        password = password.value,
        skipTest = skipTest,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ConfigureManualRemoteAccessViewModel(
            instanceId = instanceId,
        ) as T
    }
}

//endregion
//region Preview
@Composable
@Preview(showSystemUi = true)
private fun PreviewConnected() = OctoAppThemeForPreview {
    RemoteAccessManualView(
        controller = remember {
            object : ConfigureManualRemoteAccessViewController {
                override val state = RemoteAccessBaseViewModelCore.State(connected = true, loading = false, failure = null, url = "http://some.com")
                override var url by mutableStateOf("http://some.com")
                override var user by mutableStateOf("")
                override var password by mutableStateOf("")
                override suspend fun setManual(skipTest: Boolean): Boolean {
                    delay(1.seconds)
                    return true
                }

                override suspend fun disconnect() = false
            }
        }
    )
}
//endregion