package de.crysxd.baseui.compose.screens.filelist

import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.viewmodels.FileActionsViewModelCore

class FileActionsViewModel : ViewModelWithCore() {
    override val core by lazy { FileActionsViewModelCore() }

    val state = core.state

    fun copy(instanceId: String, path: String, display: String) = core.copy(instanceId = instanceId, path = path, display = display)

    fun cut(instanceId: String, path: String, display: String) = core.cut(instanceId = instanceId, path = path, display = display)

    fun paste(instanceId: String, path: String) = core.paste(instanceId = instanceId, path = path)

    fun cancel() = core.cancel()
}