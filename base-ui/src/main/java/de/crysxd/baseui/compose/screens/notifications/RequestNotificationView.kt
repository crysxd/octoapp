package de.crysxd.baseui.compose.screens.notifications

import androidx.compose.animation.graphics.ExperimentalAnimationGraphicsApi
import androidx.compose.animation.graphics.res.animatedVectorResource
import androidx.compose.animation.graphics.res.rememberAnimatedVectorPainter
import androidx.compose.animation.graphics.vector.AnimatedImageVector
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.delay

@Composable
fun RequestNotificationView(
    onRequest: () -> Unit,
    onDismiss: () -> Unit,
    isRationale: Boolean
) {
    Column {
        Animation()
        Title(isRationale = isRationale)
        Reasons()
        Actions(onRequest = onRequest, onDismiss = onDismiss, isRationale = isRationale)
    }
}

@Composable
@OptIn(ExperimentalAnimationGraphicsApi::class)
private fun Animation() {
    val vector = AnimatedImageVector.animatedVectorResource(R.drawable.octo_notification)
    var atEnd by remember { mutableStateOf(false) }
    LaunchedEffect(Unit) {
        delay(1000)
        atEnd = true
    }

    Icon(
        painter = rememberAnimatedVectorPainter(vector, atEnd = atEnd),
        contentDescription = null,
        tint = Color.Unspecified,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = OctoAppTheme.dimens.margin2),
    )
}

@Composable
private fun Title(isRationale: Boolean) = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    Text(
        text = if (isRationale) {
            stringResource(R.string.notifications_permission___title_rationale)
        } else {
            stringResource(R.string.notifications_permission___title)
        },
        style = OctoAppTheme.typography.titleBig,
        textAlign = TextAlign.Center,
        modifier = Modifier.fillMaxWidth()
    )
    if (isRationale) {
        Text(
            text = stringResource(id = R.string.notifications_permission___rationale),
            style = OctoAppTheme.typography.base,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
private fun Reasons() = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    Reason(text = stringResource(R.string.notifications_permission___reason_1))
    Reason(text = stringResource(R.string.notifications_permission___reason_2))
    Reason(text = stringResource(R.string.notifications_permission___reason_3))
}

@Composable
private fun Reason(text: String) = Row(
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    verticalAlignment = Alignment.CenterVertically,
) {
    Icon(
        painter = painterResource(id = R.drawable.ic_round_check_24),
        tint = OctoAppTheme.colors.green,
        contentDescription = null
    )
    Text(
        text = text,
        style = OctoAppTheme.typography.base
    )
}

@Composable
private fun Actions(
    onRequest: () -> Unit,
    onDismiss: () -> Unit,
    isRationale: Boolean
) = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    OctoButton(
        text = stringResource(R.string.notifications_permission___action_enable),
        onClick = onRequest
    )
    if (isRationale) {
        OctoButton(
            type = OctoButtonType.Link,
            text = stringResource(R.string.notifications_permission___action_later),
            onClick = onDismiss,
            small = true,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

//region Preview
@Preview
@Composable
private fun PreviewNormal() = OctoAppThemeForPreview {
    RequestNotificationView(onRequest = { }, onDismiss = { }, isRationale = false)
}

@Preview
@Composable
private fun PreviewRationale() = OctoAppThemeForPreview {
    RequestNotificationView(onRequest = { }, onDismiss = { }, isRationale = true)
}
//endregion