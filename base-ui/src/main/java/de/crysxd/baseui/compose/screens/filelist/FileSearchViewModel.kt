package de.crysxd.baseui.compose.screens.filelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.FileSearchViewModelCore
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn

class FileSearchViewModel(instanceId: String) : ViewModelWithCore() {
    override val core by lazy { FileSearchViewModelCore(instanceId) }

    val state = core.searchState.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribedOctoDelay,
        initialValue = FlowState.Loading()
    )

    fun search(term: String, skipCache: Boolean) = core.search(term, skipCache)

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = FileSearchViewModel(
            instanceId = instanceId,
        ) as T
    }
}