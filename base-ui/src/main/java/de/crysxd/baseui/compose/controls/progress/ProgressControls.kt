package de.crysxd.baseui.compose.controls.progress

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.flowlayout.FlowCrossAxisAlignment
import com.google.accompanist.flowlayout.FlowMainAxisAlignment
import com.google.accompanist.flowlayout.FlowRow
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoPicassoImage
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.FontSize.Large
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.FontSize.Normal
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.FontSize.Small
import de.crysxd.octoapp.base.models.ProgressControlsRole
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.menu.controls.ProgressControlsSettingsMenu
import de.crysxd.octoapp.viewmodels.ProgressControlsViewModelCore
import de.crysxd.octoapp.viewmodels.ProgressControlsViewModelCore.Companion.UPDATE_INTERVAL
import de.crysxd.octoapp.viewmodels.ProgressControlsViewModelCore.State.Icon.Circle
import de.crysxd.octoapp.viewmodels.ProgressControlsViewModelCore.State.Icon.Star
import kotlin.math.roundToInt

@Composable
fun ProgressControls(
    state: () -> ProgressControlsState,
    editState: EditState,
    modifier: Modifier = Modifier
) {
    var showSettings by remember { mutableStateOf(false) }
    if (showSettings) {
        MenuSheet(
            menu = ProgressControlsSettingsMenu(role = ProgressControlsRole.ForNormal),
            onDismiss = { showSettings = false }
        )
    }

    ControlsScaffold(
        editState = editState,
        title = stringResource(id = R.string.progress_widget___title),
        actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
        actionContentDescription = stringResource(id = R.string.cd_settings),
        onAction = { _, _ -> showSettings = true },
        modifier = modifier,
    ) {
        ProgressControlsInternal(state = state)
    }
}

interface ProgressControlsState {
    val state: ProgressControlsViewModelCore.State
}

@Composable
fun rememberProgressControlsState(defaultPrinting: Boolean = true): ProgressControlsState {
    val vmFactory = ProgressControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = ProgressControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = OctoAppTheme.octoActivity as ViewModelStoreOwner
    )

    val state = vm.state.collectAsStateWhileActive(key = "progress", initial = vm.stateCache.copy(printing = defaultPrinting))
    return object : ProgressControlsState {
        override val state get() = state.value
    }
}

//region Internals
private val LocalFontSize = compositionLocalOf { ProgressWidgetSettings.FontSize.Normal }

@Composable
private fun ProgressControlsInternal(
    state: () -> ProgressControlsState,
) {
    ProgressBar(
        completion = { state().state.completion },
        status = { state().state.status }
    )

    val showThumbnail by remember(state) {
        derivedStateOf {
            state().state.settings.showThumbnail
        }
    }

    Row(
        modifier = Modifier.padding(
            top = OctoAppTheme.dimens.margin2,
        )
    ) {
        //region Preview
        if (showThumbnail) {
            PreviewImage(horizontalSpacing = OctoAppTheme.dimens.margin1) {
                (state().state.activeFile as? FlowState.Ready)?.data?.largeThumbnail?.path
            }
        }
        //endregion
        //region Values
        ProgressValues(
            state = state,
            role = ProgressControlsRole.ForNormal,
        )
        //endregion
    }
}

@Composable
private fun PreviewImage(
    horizontalSpacing: Dp,
    path: () -> String?,
) {
    var showDialog by remember { mutableStateOf(false) }
    OctoPicassoImage(
        modifier = Modifier
            .padding(end = horizontalSpacing)
            .clip(MaterialTheme.shapes.medium)
            .clickable(enabled = path() != null) { showDialog = true }
            .background(MaterialTheme.colors.surface)
            .fillMaxWidth(fraction = 0.25f)
            .aspectRatio(ratio = 1f),
        paddingIfAlpha = OctoAppTheme.dimens.margin1,
        path = path(),
    )

    if (showDialog) {
        AlertDialog(
            onDismissRequest = { showDialog = false },
            text = {
                OctoPicassoImage(
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(1f)
                        .widthIn(max = 600.dp),
                    path = path(),
                )
            },
            confirmButton = {
                OctoButton(
                    text = stringResource(id = android.R.string.ok),
                    type = OctoButtonType.Link,
                    small = true,
                    onClick = { showDialog = false }
                )
            }
        )
    }
}


@Composable
fun ProgressValues(
    modifier: Modifier = Modifier,
    state: () -> ProgressControlsState,
    role: ProgressControlsRole,
) {
    val fontSize by remember(state) {
        derivedStateOf { state().state.settingsForRole(role).fontSize }
    }

    CompositionLocalProvider(LocalFontSize provides fontSize) {
        FlowRow(
            modifier = modifier.fillMaxWidth(),
            mainAxisSpacing = if (fontSize == Small) OctoAppTheme.dimens.margin0 else OctoAppTheme.dimens.margin01,
            lastLineMainAxisAlignment = FlowMainAxisAlignment.Start,
            crossAxisAlignment = FlowCrossAxisAlignment.Start,
            crossAxisSpacing = if (fontSize == Small) OctoAppTheme.dimens.margin0 else OctoAppTheme.dimens.margin01,
        ) {
            state().state.dataItemsForRole(role).forEach { item ->
                Value(
                    label = item.label,
                    value = item.value,
                    placeholderValue = item.valueLayoutClue,
                    largeValue = item.largeValue,
                    maxLines = item.maxLines,
                    icon = when (item.icon) {
                        Circle -> painterResource(id = R.drawable.eta_ball)
                        Star -> painterResource(id = R.drawable.ic_round_star_18)
                        null -> null
                    },
                    iconTint = when (item.iconColor) {
                        ProgressControlsViewModelCore.State.Color.Red -> colorResource(id = R.color.analysis_bad)
                        ProgressControlsViewModelCore.State.Color.Orange -> colorResource(id = R.color.analysis_normal)
                        ProgressControlsViewModelCore.State.Color.Yellow -> colorResource(id = R.color.analysis_good)
                        ProgressControlsViewModelCore.State.Color.Green -> colorResource(id = R.color.yellow)
                        null -> null
                    }
                )
            }
        }
    }
}

@Composable
private fun Value(
    label: String,
    value: String,
    placeholderValue: String? = null,
    icon: Painter? = null,
    iconTint: Color? = null,
    largeValue: Boolean,
    maxLines: Int = 1,
) = Column(
    modifier = (if (largeValue) Modifier.fillMaxWidth() else Modifier)
        .background(OctoAppTheme.colors.inputBackground.copy(alpha = 0.45f), MaterialTheme.shapes.medium)
        .padding(horizontal = OctoAppTheme.dimens.margin01, vertical = OctoAppTheme.dimens.margin0)
) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Text(
            text = label,
            style = when (LocalFontSize.current) {
                Small -> OctoAppTheme.typography.labelSmall
                Normal -> OctoAppTheme.typography.label
                Large -> OctoAppTheme.typography.base
            }
        )

        icon?.let {
            Icon(
                painter = it,
                contentDescription = null,
                tint = iconTint ?: Color.Unspecified,
                modifier = Modifier
                    .size(14.dp)
                    .padding(start = OctoAppTheme.dimens.margin0)
            )
        }

    }

    Box {
        val style = when (LocalFontSize.current) {
            Small -> OctoAppTheme.typography.label
            Normal -> OctoAppTheme.typography.base
            Large -> OctoAppTheme.typography.data
        }

        Text(
            text = value,
            style = style,
            maxLines = maxLines,
            overflow = TextOverflow.Ellipsis,
        )

        // Used to have a stable width
        placeholderValue?.let {
            Text(
                text = placeholderValue,
                style = style,
                modifier = Modifier.alpha(0f),
                maxLines = maxLines,
                overflow = TextOverflow.Ellipsis,
            )
        }
    }
}


@Composable
private fun ProgressBar(
    completion: () -> Float,
    status: () -> String,
) = Box(
    modifier = Modifier
        .clip(MaterialTheme.shapes.small)
        .background(MaterialTheme.colors.surface)
        .height(IntrinsicSize.Min)
) {
    val roundedCompletion by remember(completion) {
        derivedStateOf {
            completion().let { (it * 100).roundToInt() / 100f }
        }
    }

    val statusText by remember(status) {
        derivedStateOf {
            status()
        }
    }

    val animatedCompletion by animateFloatAsState(
        targetValue = roundedCompletion,
        animationSpec = tween(durationMillis = UPDATE_INTERVAL.inWholeMilliseconds.toInt())
    )

    val color = OctoAppTheme.colors.activeColorScheme
    val textColorInside = OctoAppTheme.colors.textColoredBackground
    val textColorOutside = OctoAppTheme.colors.darkText
    val textInside = { animatedCompletion < 0.7 }
    val textColor by remember {
        derivedStateOf {
            if (textInside()) textColorOutside else textColorInside
        }
    }
    val textAlign by remember {
        derivedStateOf {
            if (textInside()) TextAlign.Start else TextAlign.End
        }
    }
    var fullWidth by remember { mutableStateOf(0) }
    val textWidth = 150.dp

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .onGloballyPositioned { fullWidth = it.size.width }
            .drawBehind {
                drawRect(
                    color = color,
                    size = Size(width = size.width * animatedCompletion, height = size.height)
                )
            }
    ) {
        Text(
            style = OctoAppTheme.typography.sectionHeader,
            color = textColor,
            text = statusText,
            maxLines = 1,
            textAlign = textAlign,
            overflow = TextOverflow.Visible,
            modifier = Modifier
                .width(textWidth)
                .padding(horizontal = OctoAppTheme.dimens.margin01, vertical = OctoAppTheme.dimens.margin1)
                .offset {
                    val barEnd = (fullWidth * animatedCompletion).roundToInt()
                    if (textAlign == TextAlign.Start) {
                        IntOffset(barEnd, 0)
                    } else {
                        IntOffset(barEnd - textWidth.roundToPx(), 0)
                    }
                }
        )
    }
}

//endregion

//region Preview
@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewInitial() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val state = ProgressControlsViewModelCore.State(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),
                    activeFile = FlowState.Loading(),
                    fullscreenDataItems = emptyList(),
                    dataItems = emptyList(),
                    status = "0%",
                    completion = 0f,
                    printing = true,
                )
            }
        }
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewEarly() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val state = ProgressControlsViewModelCore.State(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),
                    status = "10%",
                    completion = .133f,
                    activeFile = FlowState.Loading(),
                    fullscreenDataItems = emptyList(),
                    dataItems = emptyList(),
                    printing = true,
                )
            }
        }
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewLate() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val state = ProgressControlsViewModelCore.State(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),
                    fullscreenDataItems = emptyList(),
                    dataItems = emptyList(),
                    status = "80%",
                    completion = .8033f,
                    activeFile = FlowState.Loading(),
                    printing = true,
                )
            }
        }
    )
}

@Composable
@Preview(backgroundColor = 0xFFFFFF)
private fun PreviewPaused() = OctoAppThemeForPreview {
    ProgressControlsInternal(
        state = {
            object : ProgressControlsState {
                override val state = ProgressControlsViewModelCore.State(
                    settings = ProgressWidgetSettings(),
                    fullscreenSettings = ProgressWidgetSettings(),
                    fullscreenDataItems = emptyList(),
                    dataItems = emptyList(),
                    status = "Paused",
                    completion = .4033f,
                    activeFile = FlowState.Loading(),
                    printing = true,
                )
            }
        }
    )
}
//endregion