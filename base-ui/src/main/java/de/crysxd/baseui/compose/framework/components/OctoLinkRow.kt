package de.crysxd.baseui.compose.framework.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import de.crysxd.baseui.compose.theme.OctoAppTheme

@Composable
fun OctoLinkButtonRow(
    texts: List<String>,
    onClicks: List<suspend () -> Unit>,
    modifier: Modifier = Modifier,
    alignment: Alignment.Horizontal = Alignment.End,
    small: Boolean = true,
    color: Color? = null,
) = Row(
    modifier = modifier,
    horizontalArrangement = Arrangement.spacedBy(-OctoAppTheme.dimens.margin12, alignment)
) {
    require(texts.size == onClicks.size) { "Need equal amount of texts and click listeners, is ${texts.size} <> ${onClicks.size}" }
    texts.indices.forEach { index ->
        OctoButton(
            type = color?.let { OctoButtonType.LinkWithCustomColor(it) } ?: OctoButtonType.Link,
            small = small,
            text = texts[index],
            onClick = onClicks[index]
        )
    }
}