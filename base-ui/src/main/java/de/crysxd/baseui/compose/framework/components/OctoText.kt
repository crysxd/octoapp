package de.crysxd.baseui.compose.framework.components

import android.text.Spanned
import androidx.compose.foundation.text.ClickableText
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.core.net.toUri
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch

@Composable
fun OctoText(
    text: CharSequence,
    modifier: Modifier = Modifier,
    style: TextStyle = OctoAppTheme.typography.base,
    textAlign: TextAlign? = null,
    color: Color = OctoAppTheme.colors.normalText,
    onUrlOpened: suspend (String) -> Boolean? = { false },
) {
    val scope = rememberCoroutineScope()
    val activity = OctoAppTheme.octoActivity
    val annotated = when (text) {
        is Spanned -> text.asAnnotatedString()
        else -> AnnotatedString(text.toString())
    }

    ClickableText(
        text = annotated,
        modifier = modifier,
        style = style.copy(
            color = color,
            textAlign = textAlign ?: TextAlign.Start,
        ),
    ) { position ->
        annotated.getStringAnnotations(tag = "URL", position, position).firstOrNull()?.let { url ->
            scope.launch {
                try {
                    Napier.i(tag = "ClickableText", message = "Opening URL: ${url.item}")
                    if (onUrlOpened(url.item) != true) {
                        url.item.toUri().open()
                    }
                } catch (e: Exception) {
                    Napier.e(tag = "ClickableText", message = "Failed to handle URL", throwable = e)
                    activity?.showDialog(e)
                }
            }
        }
    }
}