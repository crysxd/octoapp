package de.crysxd.baseui.compose.controls.movetool

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.viewmodels.MoveControlsViewModelCore

class MoveToolControlsViewModel(
    private val instanceId: String,
) : ViewModelWithCore() {

    override val core = MoveControlsViewModelCore(instanceId)
    val state = core.state

    fun getState() = core.getState()
    suspend fun homeXYAxis() = core.homeXYAxis()
    suspend fun homeZAxis() = core.homeZAxis()
    fun move(axis: MoveControlsViewModelCore.Axis, direction: MoveControlsViewModelCore.Direction, distance: Double) = core.moveAxis(axis, direction, distance)
    suspend fun moveToPosition(x: Float, y: Float, z: Float) = core.moveToPosition(x = x, y = y, z = z)
    suspend fun turnMotorsOff() = core.turnMotorsOff()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = MoveToolControlsViewModel(
            instanceId = instanceId,
        ) as T
    }
}