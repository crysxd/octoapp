package de.crysxd.baseui.compose.screens.filelist

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoInputField
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.layout.StateBox
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.FileSearchViewModelCore
import kotlinx.coroutines.flow.first

@Composable
fun FileSearchScreen(
    controller: FileSearchController = rememberFileSearchController(),
) = CollapsibleHeaderScreenScaffold(
    onRefresh = controller::reload,
    header = {},
    tabs = {
        SearchInput(
            modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin1),
            term = controller.searchTerm,
            onChanged = controller::search
        )
    },
    content = {
        SearchContent(
            state = controller.state,
            onRetry = controller::reload
        )
    },
)

@Composable
private fun SearchInput(
    modifier: Modifier,
    term: String,
    onChanged: (String) -> Unit,
) = OctoInputField(
    value = term,
    onValueChanged = onChanged,
    modifier = modifier,
    alternativeBackground = true,
    focusInitially = term.isEmpty(),
    placeholder = "",
    label = stringResource(R.string.file_manager___file_search___hint),
    labelActive = stringResource(R.string.file_manager___file_search___hint),
)

@Composable
fun SearchContent(
    modifier: Modifier = Modifier,
    state: FlowState<FileSearchViewModelCore.SearchResult>,
    onRetry: suspend () -> Unit,
) = StateBox(
    modifier = modifier,
    state = state,
    onRetry = onRetry,
    isEmpty = { it.files.isEmpty() && it.folders.isEmpty() },
    emptyText = stringResource(R.string.file_manager___file_search___no_files_found),
) {
    val keyboard = LocalSoftwareKeyboardController.current
    val listState = rememberLazyListState()
    val nestedScroll = LocalNestedScrollConnection.current

    LazyColumn(
        state = listState,
        modifier = Modifier.nestedScroll(
            object : NestedScrollConnection by nestedScroll {
                override fun onPreScroll(
                    available: Offset,
                    source: androidx.compose.ui.input.nestedscroll.NestedScrollSource
                ): Offset {
                    keyboard?.hide()
                    return nestedScroll.onPreScroll(available, source)
                }
            }
        )
    ) {
        items(
            items = it.folders + it.files,
            key = { it.path }
        ) {
            FileListItem(
                file = it,
            )
        }
    }
}


@Composable
fun rememberFileSearchController(): FileSearchController {
    val factory = FileSearchViewModel.Factory(instanceId = LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = FileSearchViewModel::class,
        factory = factory,
        key = factory.id,
    )

    val state = vm.state.collectAsState()
    val searchTerm = rememberSaveable { mutableStateOf("") }

    LaunchedEffect(searchTerm.value) {
        vm.search(term = searchTerm.value, skipCache = false)
    }

    return remember(state, searchTerm, vm) {
        object : FileSearchController {
            override val state by state
            override val searchTerm by searchTerm

            override fun search(term: String) {
                searchTerm.value = term
            }

            override suspend fun reload() {
                vm.search(term = searchTerm.value, skipCache = true)
                vm.state.first { it !is FlowState.Loading }
            }
        }
    }
}

interface FileSearchController {
    val state: FlowState<FileSearchViewModelCore.SearchResult>
    val searchTerm: String
    fun search(term: String)
    suspend fun reload()
}