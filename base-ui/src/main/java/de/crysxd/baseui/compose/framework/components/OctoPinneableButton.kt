package de.crysxd.baseui.compose.framework.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview

@Composable
fun OctoPinneableButton(
    text: String,
    modifier: Modifier = Modifier,
    type: OctoButtonType = OctoButtonType.Primary,
    enabled: Boolean = true,
    pinned: Boolean,
    successAnimation: Boolean = false,
    alternativeBackground: Boolean = false,
    onLongClick: suspend () -> Unit = {},
    onClick: suspend () -> Unit,
) = Row(
    modifier = modifier
        .clip(type.shape())
        .background(if (alternativeBackground) OctoAppTheme.colors.inputBackgroundAlternative else OctoAppTheme.colors.inputBackground),
    verticalAlignment = Alignment.CenterVertically,
) {
    OctoButton(
        text = text,
        onClick = onClick,
        type = type,
        small = true,
        successAnimation = successAnimation,
        enabled = enabled,
        onLongClick = onLongClick,
        loading = false,
    )

    AnimatedVisibility(visible = pinned) {
        Icon(
            painter = painterResource(id = R.drawable.ic_round_push_pin_10),
            contentDescription = null,
            tint = OctoAppTheme.colors.primaryButtonBackground,
            modifier = Modifier.padding(end = OctoAppTheme.dimens.margin0, start = OctoAppTheme.dimens.margin01)
        )
    }
}

//region Preview
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    var pinned by remember { mutableStateOf(true) }
    OctoPinneableButton(
        text = "Some button",
        onClick = { pinned = !pinned },
        onLongClick = { pinned = !pinned },
        pinned = pinned,
    )
}
//endregion