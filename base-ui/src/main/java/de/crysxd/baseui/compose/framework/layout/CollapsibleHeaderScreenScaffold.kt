package de.crysxd.baseui.compose.framework.layout

import android.annotation.SuppressLint
import android.graphics.Rect
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.PullRefreshState
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.MeasureResult
import androidx.compose.ui.layout.MeasureScope
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import de.crysxd.baseui.compose.framework.helpers.Holder
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.framework.locals.LocalOctoAppInsets
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsBottomPadding
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsTopPadding
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlin.math.absoluteValue
import kotlin.time.Duration.Companion.seconds

private const val headerLayoutId = "header"
private const val tabLayoutId = "header"
private const val bodyLayoutId = "body"

@Composable
fun CollapsibleHeaderScreenScaffold(
    @SuppressLint("ModifierParameter") headerModifier: Modifier = Modifier,
    headerBackground: Color = OctoAppTheme.colors.inputBackground,
    title: AnnotatedString,
    subtitle: AnnotatedString,
    onRefresh: (suspend () -> Unit)? = null,
    tabs: @Composable CollapsibleHeaderScreenScope.() -> Unit = {},
    content: @Composable CollapsibleHeaderScreenScope.() -> Unit,
) = CollapsibleHeaderScreenScaffold(
    headerBackground = headerBackground,
    tabs = tabs,
    content = content,
    onRefresh = onRefresh,
    header = {
        Column(
            modifier = headerModifier.padding(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin5),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = title,
                textAlign = TextAlign.Center,
                style = OctoAppTheme.typography.titleBig,
                color = OctoAppTheme.colors.darkText,
                modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin1),
            )

            Text(
                text = subtitle,
                textAlign = TextAlign.Center,
                style = OctoAppTheme.typography.base,
                color = OctoAppTheme.colors.lightText,
            )
        }
    }
)

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CollapsibleHeaderScreenScaffold(
    headerBackground: Color = OctoAppTheme.colors.inputBackground,
    header: @Composable () -> Unit,
    onRefresh: (suspend () -> Unit)? = null,
    tabs: @Composable CollapsibleHeaderScreenScope.() -> Unit = {},
    content: @Composable CollapsibleHeaderScreenScope.() -> Unit,
) = Box {
    var keepCollapsed by remember { mutableStateOf(false) }
    val nested = rememberNestedScroll(keepAlwaysCollapsed = keepCollapsed)
    val scope = remember {
        object : CollapsibleHeaderScreenScope {
            override fun collapse(lock: Boolean) {
                nested.collapse()
                keepCollapsed = true
            }

            override fun expand() {
                nested.expand()
                keepCollapsed = false
            }
        }
    }

    var refreshing by remember { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope()
    val activity = LocalOctoActivity.current
    val pullRefreshState = rememberPullRefreshState(
        refreshing = refreshing,
        onRefresh = {
            coroutineScope.launch {
                refreshing = true
                val start = Clock.System.now()
                try {
                    onRefresh?.invoke()
                } catch (e: Exception) {
                    activity?.showDialog(e)
                    Napier.e(tag = "CollapsibleHeaderScreenScaffold", message = "Failed to refresh", throwable = e)
                }
                delay((Clock.System.now() - start).coerceAtLeast(1.seconds))
            }.invokeOnCompletion {
                refreshing = false
            }
        }
    )

    val octoInsets = LocalOctoAppInsets.current
    Layout(
        modifier = Modifier
            .background(OctoAppTheme.colors.windowBackground)
            .octoAppInsetsBottomPadding()
            .clip(RectangleShape)
            .pullRefresh(pullRefreshState, enabled = onRefresh != null)
            .scrollable(
                orientation = Orientation.Vertical,
                state = rememberScrollableState(nested::consumeScroll),
            )
            .offset { IntOffset(x = 0, y = nested.offset.toInt()) },
        content = {
            CompositionLocalProvider(LocalNestedScrollConnection provides nested.connection) {
                Content(
                    headerBackground = headerBackground,
                    pullRefreshState = pullRefreshState,
                    refreshing = { refreshing },
                    header = header,
                    tabs = { scope.tabs() },
                    content = { scope.content() },
                    canScrollBack = { -10 > nested.offset },
                )
            }
        },
        measurePolicy = { measureables, constraints ->
            measurePolicy(measureables, constraints, octoInsets) { nested.maxOffset = it.toFloat() }
        }
    )

//    Box(
//        modifier = Modifier
//            .background(OctoAppTheme.colors.windowBackground)
//            .octoAppInsetsBottomPadding()
//            .align(Alignment.BottomCenter)
//            .fillMaxWidth()
//    )
}

interface CollapsibleHeaderScreenScope {
    fun collapse(lock: Boolean = false)
    fun expand()
}

@Composable
@OptIn(ExperimentalMaterialApi::class)
private fun Content(
    headerBackground: Color = OctoAppTheme.colors.inputBackground,
    pullRefreshState: PullRefreshState,
    refreshing: () -> Boolean,
    header: @Composable () -> Unit,
    tabs: @Composable () -> Unit,
    content: @Composable () -> Unit,
    canScrollBack: () -> Boolean,
) {
    val expanded by remember { derivedStateOf { !canScrollBack() } }
    val headerAlpha by animateFloatAsState(targetValue = if (expanded) 1f else 0f)

    Box(
        modifier = Modifier
            .background(headerBackground)
            .graphicsLayer { alpha = headerAlpha }
            .zIndex(100f)
            .octoAppInsetsTopPadding()
            .layoutId(headerLayoutId),
        contentAlignment = Alignment.Center,
    ) {
        header()
    }

    Box(
        modifier = Modifier
            .background(headerBackground)
            .layoutId(tabLayoutId)
            .zIndex(50f)
    ) {
        tabs()
    }

    Box(
        modifier = Modifier
            .background(OctoAppTheme.colors.windowBackground)
            .fillMaxSize()
            .zIndex(10f)
            .layoutId(bodyLayoutId)
    ) {
        Box {
            content()
            PullRefreshIndicator(
                modifier = Modifier.align(Alignment.TopCenter),
                refreshing = refreshing(),
                state = pullRefreshState,
                backgroundColor = OctoAppTheme.colors.windowBackground,
                contentColor = OctoAppTheme.colors.accent,
            )
        }

        ScrollEdge(
            visible = { canScrollBack() },
            alignment = Alignment.TopCenter
        )
    }
}

private fun MeasureScope.measurePolicy(measurables: List<Measurable>, constraints: Constraints, insets: Rect, setMaxOffset: (Int) -> Unit): MeasureResult {
    var tabHeight = 0
    val placeables = measurables.map { measurable ->
        val minHeight = when (measurable.layoutId) {
            bodyLayoutId -> constraints.maxHeight - tabHeight - insets.top
            else -> 0
        }

        val maxHeight = when (measurable.layoutId) {
            bodyLayoutId -> minHeight
            else -> constraints.maxHeight
        }

        val placeable = measurable.measure(constraints.copy(minWidth = constraints.maxWidth, minHeight = minHeight, maxHeight = maxHeight))

        when (measurable.layoutId) {
            tabLayoutId -> tabHeight = placeable.height
        }

        placeable
    }

    val headerIndex = measurables.indexOfFirst { it.layoutId == headerLayoutId }
    setMaxOffset(placeables[headerIndex].height - insets.top)

    return layout(width = constraints.maxWidth, height = constraints.maxHeight) {
        var y = 0
        placeables.forEach { placeable ->
            placeable.place(x = 0, y = y)
            y += placeable.height
        }
    }
}

@Composable
fun rememberNestedScroll(
    keepAlwaysCollapsed: Boolean,
): NestedScrollScope {
    val lastPreFling = remember { Holder(0L) }
    val offset = rememberSaveable { mutableFloatStateOf(0f) }
    val maxOffset = rememberSaveable { mutableFloatStateOf(1f) }
    val connection = object : NestedScrollConnection {
        override fun onPreScroll(
            available: Offset,
            source: NestedScrollSource
        ) = if (available.y < 0f) {
            val consume = (maxOffset.floatValue - offset.floatValue.absoluteValue)
                .coerceAtMost(available.y.absoluteValue)
            offset.floatValue -= consume
            lastPreFling.value = System.currentTimeMillis()
            Offset(x = 0f, y = -consume)
        } else {
            Offset.Zero
        }

        override suspend fun onPreFling(
            available: Velocity
        ): Velocity = if (available.y < 0f && offset.floatValue > -maxOffset.floatValue) {
            val consumed = animateOffsetWithVelocity(
                available = available,
                target = -maxOffset.floatValue
            )
            consumed
        } else {
            super.onPreFling(available)
        }

        override fun onPostScroll(
            consumed: Offset,
            available: Offset,
            source: NestedScrollSource
        ) = if (available.y > 0f) {
            val o = offset.floatValue
            val consume = o.absoluteValue.coerceAtMost(available.y.absoluteValue)
            offset.floatValue += consume
            Offset(x = 0f, y = consume)
        } else {
            Offset.Zero
        }

        override suspend fun onPostFling(consumed: Velocity, available: Velocity) =
            if (available.y > 0f) {
                val consumedByUs = animateOffsetWithVelocity(available = available, target = 0f)
                consumedByUs
            } else {
                super.onPostFling(consumed, available)
            }

        private suspend fun animateOffsetWithVelocity(
            available: Velocity,
            target: Float
        ): Velocity = Animatable(
            initialValue = offset.floatValue,
            typeConverter = Float.VectorConverter,
        ).run {
            updateBounds(-maxOffset.floatValue, 0f)
            val left = animateTo(
                targetValue = target,
                initialVelocity = available.y.absoluteValue,
                block = { offset.floatValue = value },
                animationSpec = spring(),
            ).endState.velocity

            val consumed = if (available.y > 0) {
                available.copy(y = available.y - left)
            } else {
                available.copy(y = available.y + left)
            }
            consumed
        }
    }

    // Keep offset at max if always collapsed
    LaunchedEffect(keepAlwaysCollapsed, maxOffset.floatValue) {
        if (keepAlwaysCollapsed) {
            offset.floatValue = -maxOffset.floatValue
        }
    }

    val scope = rememberCoroutineScope()
    return remember(keepAlwaysCollapsed) {
        object : NestedScrollScope {
            override var offset by offset
            override var maxOffset by maxOffset
            override val connection = connection

            override fun consumeScroll(delta: Float): Float = when {
                keepAlwaysCollapsed -> delta
                delta > 0 -> {
                    connection.onPostScroll(
                        consumed = Offset.Zero,
                        available = Offset(x = 0f, y = delta),
                        source = NestedScrollSource.Drag
                    ).y
                }

                else -> {
                    connection.onPreScroll(
                        available = Offset(x = 0f, y = delta),
                        source = NestedScrollSource.Drag
                    ).y
                }
            }

            override fun collapse(animationSpec: AnimationSpec<Float>?) =
                animateToTarget(-maxOffset.floatValue, animationSpec)

            override fun expand(animationSpec: AnimationSpec<Float>?) =
                animateToTarget(0f, animationSpec)

            private fun animateToTarget(target: Float, animationSpec: AnimationSpec<Float>?) {
                if (keepAlwaysCollapsed) return

                scope.launch {
                    Animatable(initialValue = offset.floatValue).animateTo(
                        targetValue = target,
                        animationSpec = animationSpec ?: spring(stiffness = Spring.StiffnessLow)
                    ) {
                        offset.floatValue = value
                    }
                }
            }
        }
    }
}


interface NestedScrollScope {
    var offset: Float
    var maxOffset: Float
    val connection: NestedScrollConnection
    fun collapse(animationSpec: AnimationSpec<Float>? = null)
    fun expand(animationSpec: AnimationSpec<Float>? = null)
    fun consumeScroll(delta: Float): Float
}

//region Preview
@Preview(showSystemUi = true)
@Composable
private fun Preview() = OctoAppThemeForPreview {
    CollapsibleHeaderScreenScaffold(
        header = {
            Box(
                modifier = Modifier
                    .background(Color.Blue)
                    .fillMaxWidth()
                    .aspectRatio(1f)
            )
        },
        tabs = {
            Box(
                modifier = Modifier
                    .background(Color.Red)
                    .height(40.dp)
                    .fillMaxWidth()
            )
        }
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxSize()
                .nestedScroll(LocalNestedScrollConnection.current)
                .verticalScroll(rememberScrollState())
        ) {
            Text(text = "Content ".repeat(1000))
        }
    }
}
//endregion
