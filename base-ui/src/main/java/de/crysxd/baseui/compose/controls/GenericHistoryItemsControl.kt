package de.crysxd.baseui.compose.controls

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.drawText
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Constraints
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import de.crysxd.baseui.compose.framework.layout.AnimatedFlowRow
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoPinneableButton
import de.crysxd.baseui.compose.theme.OctoAppTheme

@Composable
fun <T> GenericHistoryItemsControl(
    editState: EditState,
    state: GenericHistoryItemsControlState<T>,
    title: String?,
    announcement: (@Composable () -> Unit)? = null,
    actionIcon: Painter? = null,
    actionContentDescription: String? = null,
    onAction: suspend (FragmentManager, NavController) -> Unit = { _, _ -> },
) = AnimatedVisibility(
    visible = state.visible || editState.editing,
    enter = fadeIn() + expandVertically(),
    exit = fadeOut() + shrinkVertically()
) {
    ControlsScaffold(
        title = title,
        editState = editState,
        actionIcon = actionIcon,
        actionContentDescription = actionContentDescription,
        onAction = onAction,
    ) {
        announcement?.invoke()

        Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12)) {
            state.items.forEach { (sectionTitle, items) ->
                Section(
                    state = state,
                    sectionTitle = sectionTitle,
                    items = items
                )
            }
        }
    }
}

@OptIn(ExperimentalTextApi::class)
private fun Modifier.sectionTitle(text: String?) = composed {
    val textMeasurer = rememberTextMeasurer()
    val titleStyle = OctoAppTheme.typography.focus
    if (text == null) return@composed this

    this
        .padding(start = OctoAppTheme.dimens.margin01)
        .drawBehind {
            rotate(degrees = -90f, pivot = size.center) {
                val textLayout = textMeasurer.measure(
                    text = text,
                    style = titleStyle,
                    softWrap = false,
                    constraints = Constraints(maxWidth = size.height.toInt()),
                    overflow = TextOverflow.Ellipsis,
                )

                drawText(
                    textLayoutResult = textLayout,
                    topLeft = Offset(
                        x = ((size.width - textLayout.size.width) / 2),
                        y = (size.height - textLayout.size.height) / 2 - (size.width / 2),
                    )
                )
            }
        }
        .padding(start = with(LocalDensity.current) {
            OctoAppTheme.typography.focus.fontSize.value.toDp() + OctoAppTheme.dimens.margin1
        })
}

@Composable
private fun <T> Section(
    state: GenericHistoryItemsControlState<T>,
    sectionTitle: String?,
    items: List<T>,
) = AnimatedFlowRow(
    mainAxisSpacing = OctoAppTheme.dimens.margin01,
    crossAxisSpacing = OctoAppTheme.dimens.margin01,
    modifier = if (sectionTitle != null) {
        Modifier
            .background(OctoAppTheme.colors.inputBackground, MaterialTheme.shapes.large)
            .padding(OctoAppTheme.dimens.margin1)
            .sectionTitle(text = sectionTitle)
    } else {
        Modifier
    }
) {
    items.forEach { item ->
        item(key = state.key(item)) {
            OctoPinneableButton(
                alternativeBackground = sectionTitle != null,
                pinned = state.isPinned(item),
                text = state.label(item),
                successAnimation = true,
                onClick = { state.onClick(item) },
                onLongClick = { state.onLongClick(item) },
            )
        }
    }

    if (sectionTitle == null) {
        item(key = state.otherOptionText) {
            OctoButton(
                small = true,
                type = OctoButtonType.Secondary,
                text = state.otherOptionText,
                onClick = state::onOtherOptionClicked,
            )
        }
    }
}

interface GenericHistoryItemsControlState<T> {

    val visible: Boolean
    val items: List<Pair<String?, List<T>>>
    val otherOptionText: String

    fun isPinned(item: T): Boolean
    fun label(item: T): String
    fun key(item: T): String

    suspend fun onOtherOptionClicked()
    suspend fun onClick(item: T)
    suspend fun onLongClick(item: T)
}
