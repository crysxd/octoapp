package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme

@Composable
fun RemoteAccessConnected(
    text: String,
    modifier: Modifier = Modifier
) = Row(
    modifier = modifier,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
) {
    Icon(
        painter = painterResource(R.drawable.ic_baseline_cloud_done_24),
        tint = OctoAppTheme.colors.green,
        contentDescription = text,
    )

    Text(
        text = text,
        style = OctoAppTheme.typography.sectionHeader,
        color = OctoAppTheme.colors.green
    )
}