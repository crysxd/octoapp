package de.crysxd.baseui.compose.controls.gcodepreview

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.with
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntSize
import androidx.core.content.ContextCompat
import androidx.core.graphics.applyCanvas
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.common.controls.ControlsFragmentDirections
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.PrintBedData
import de.crysxd.baseui.compose.controls.PrintBedLayout
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.helpers.staticStateOf
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.menu.SimpleMenuItem
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.ControlType
import de.crysxd.octoapp.base.data.models.ControlsPreferences
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository.Companion.LIST_PRINT
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.drawableRes
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.menu.controls.GcodeSettingsMenu
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.datetime.Instant
import kotlin.math.roundToInt

private const val TAG = "GcodeControls"

@Composable
@OptIn(ExperimentalAnimationApi::class)
fun GcodeControls(
    state: GcodeControlsState,
    editState: EditState,
    modifier: Modifier = Modifier,
) {
    val instanceId = LocalOctoPrint.current.id
    val gbrl by remember { hasGbrl(instanceId) }.collectAsStateWhileActive(initial = false, key = "previewGbrl")

    SideEffect {
        if (gbrl) {
            Napier.i(tag = TAG, message = "[GCD] GBRL active, hiding Gcode")
        } else {
            Napier.i(tag = TAG, message = "[GCD] Gcode active")
        }
    }

    AnimatedVisibility(!gbrl, modifier = modifier) {
        GcodeControlsBox(editState) {
            val transition = updateTransition(targetState = state.current, label = "AnimatedContent")
            transition.AnimatedContent(
                transitionSpec = { fadeIn(animationSpec = tween(220, delayMillis = 90)) with fadeOut(animationSpec = tween(90)) },
                contentKey = { it::class },
                contentAlignment = Alignment.Center,
            ) { target ->
                Napier.i(tag = TAG, message = "Moving to state ${target.toString().take(150)}")
                when (target) {
                    is GcodePreviewControlsViewModel.ViewState.Error -> GcodeControlsError(
                        state = target,
                        onRetry = state::retry
                    )

                    is GcodePreviewControlsViewModel.ViewState.LargeFileDownloadRequired -> GcodeControlsReadyLargeFileDownloadRequired(
                        state = target,
                        onDownload = state::allowLargeFileDownloads
                    )

                    is GcodePreviewControlsViewModel.ViewState.DataReady -> GcodeControlsReady(
                        state = { target },
                        onFullscreen = { state.openFullscreen() }
                    )

                    is GcodePreviewControlsViewModel.ViewState.Loading -> GcodeControlsLoading(
                        state = target
                    )

                    is GcodePreviewControlsViewModel.ViewState.FeatureDisabled -> GcodeControlsDisabled(
                        state = target,
                        onHideWhenDisabled = {
                            BaseInjector.get().widgetPreferencesRepository().apply {
                                (getWidgetOrder(LIST_PRINT) ?: ControlsPreferences(LIST_PRINT, emptyList(), emptyList())).let { old ->
                                    setWidgetOrder(LIST_PRINT, old.copy(hidden = old.hidden + ControlType.GcodePreviewWidget))
                                }
                            }
                        }
                    )

                    is GcodePreviewControlsViewModel.ViewState.PrintingFromSdCard -> GcodeControlsPrintingFromSdCard(
                        state = target,
                    )
                }
            }
        }
    }
}

//region State
interface GcodeControlsState {
    val current: GcodePreviewControlsViewModel.ViewState
    fun retry()
    fun allowLargeFileDownloads()
    fun openFullscreen()
}

@Composable
fun rememberGcodeControlsState(): GcodeControlsState {
    val instanceId = LocalOctoPrint.current.id
    val controller = OctoAppTheme.navController
    val vmFactory = GcodePreviewControlsViewModel.Factory(instanceId)
    val vm = viewModel(
        modelClass = GcodePreviewControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )
    vm.useLive()

    val state = vm.state.collectAsStateWhileActive(initial = GcodePreviewControlsViewModel.ViewState.Loading(), key = "preivew")
    LaunchedEffect(state) {
        Napier.i(tag = TAG, message = "[GCD] Received to state ${state.value.toString().take(150)}")
    }
    return object : GcodeControlsState {
        override val current get() = state.value
        override fun retry() = vm.retry()
        override fun allowLargeFileDownloads() = vm.allowLargeDownloads()
        override fun openFullscreen() = controller().navigate(ControlsFragmentDirections.actionShowGcodePreviewFullscreen(instanceId))
    }
}

//endregion
//region Internals
internal const val GcodeNotLiveIfNoUpdateForMs = 5000L

private fun hasGbrl(instanceId: String) = BaseInjector.get().octorPrintRepository().instanceInformationFlow(instanceId = instanceId)
    .map { it?.hasPlugin(OctoPlugins.BetterGrblSupport) == true }
    .distinctUntilChanged()

@Composable
private fun GcodeControlsBox(editState: EditState? = null, content: @Composable BoxScope.() -> Unit) {
    var showSettings by remember { mutableStateOf(false) }
    if (showSettings) {
        MenuSheet(
            menu = GcodeSettingsMenu(),
            onDismiss = { showSettings = false }
        )
    }

    ControlsScaffold(
        title = stringResource(id = R.string.gcode_preview),
        actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
        actionContentDescription = stringResource(id = R.string.cd_settings),
        editState = editState,
        onAction = { fm, _ -> showSettings = true }
    ) {
        Box(
            contentAlignment = Alignment.Center,
            content = content,
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min)
                .clip(MaterialTheme.shapes.large)
                .background(OctoAppTheme.colors.inputBackground)
        )
    }
}

@Composable
private fun GcodeControlsDisabled(
    state: GcodePreviewControlsViewModel.ViewState.FeatureDisabled,
    onHideWhenDisabled: () -> Unit,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier.aspectRatio(16 / 9f),
) {
    //region Render Preview
    var previewSize by remember { mutableStateOf(IntSize(0, 0)) }
    var preview by remember { mutableStateOf<ImageBitmap?>(null) }
    val context = LocalContext.current

    LaunchedEffect(state, previewSize) {
        if (previewSize.height == 0) return@LaunchedEffect
        val bitmap = Bitmap.createBitmap(previewSize.width, previewSize.height, Bitmap.Config.ARGB_8888)
        val background = ContextCompat.getDrawable(context, state.printBed.drawableRes)
        val foreground = ContextCompat.getDrawable(context, R.drawable.gcode_preview)
        preview = bitmap.applyCanvas {
            fun drawImage(image: Drawable?) = image?.let {
                val backgroundScale = height / it.intrinsicHeight.toFloat()
                it.setBounds(
                    (width - it.intrinsicWidth * backgroundScale).toInt(),
                    0,
                    width,
                    height
                )
                it.draw(this)
            }

            drawImage(background)
            drawImage(foreground)
        }.asImageBitmap()
    }
    //endregion
    //region Preview
    Box(
        Modifier
            .fillMaxHeight()
            .padding(vertical = OctoAppTheme.dimens.margin12)
            .onGloballyPositioned { previewSize = it.size }
            .fillMaxWidth(0.25f),
        contentAlignment = Alignment.CenterEnd,
    ) {
        preview?.let {
            Image(
                bitmap = it,
                contentDescription = null,
            )
        }
    }
    //endregion
    //region Content
    Column(
        modifier = Modifier
            .padding(end = OctoAppTheme.dimens.margin2, start = OctoAppTheme.dimens.margin12)
            .weight(1f),
        verticalArrangement = Arrangement.Center,
    ) {
        Spacer(modifier = Modifier.weight(1f))
        Text(
            text = stringResource(id = R.string.supporter_perk___title),
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.darkText,
        )
        Text(
            text = stringResource(id = R.string.supporter_perk___description, stringResource(id = R.string.widget_gcode_preview)),
            style = OctoAppTheme.typography.labelSmall,
            modifier = Modifier.padding(top = OctoAppTheme.dimens.margin0)
        )
        Spacer(modifier = Modifier.weight(1f))
        SimpleMenuItem(
            title = stringResource(id = R.string.support_octoapp),
            icon = painterResource(R.drawable.ic_round_favorite_24),
            style = de.crysxd.octoapp.menu.MenuItemStyle.Support,
            slim = true,
            modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin01)
        ) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenOpen, mapOf("trigger" to "gcode_live"))
            UriLibrary.getPurchaseUri().open()
        }
        SimpleMenuItem(
            title = stringResource(id = R.string.hide),
            icon = painterResource(R.drawable.ic_round_visibility_off_24),
            style = de.crysxd.octoapp.menu.MenuItemStyle.Neutral,
            slim = true
        ) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.DisabledFeatureHidden, mapOf("feature" to "gcode_preview"))
            onHideWhenDisabled()
        }
        Spacer(modifier = Modifier.weight(1f))
    }
    //endregion
}

@Composable
internal fun GcodeControlsLoading(
    state: GcodePreviewControlsViewModel.ViewState.Loading
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier.aspectRatio(16 / 9f),
    verticalArrangement = Arrangement.Center,
) {
    Text(
        text = stringResource(id = R.string.loading),
        style = OctoAppTheme.typography.subtitle,
        modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin1)
    )

    if (state.progress > 0) {
        LinearProgressIndicator(
            backgroundColor = OctoAppTheme.colors.inputBackgroundAlternative,
            modifier = Modifier.fillMaxWidth(0.5f),
            progress = state.progress
        )
    } else {
        LinearProgressIndicator(
            backgroundColor = OctoAppTheme.colors.inputBackgroundAlternative,
            modifier = Modifier.fillMaxWidth(0.5f),
        )
    }
}

@Composable
private fun GcodeControlsReady(
    state: () -> GcodePreviewControlsViewModel.ViewState.DataReady,
    onFullscreen: () -> Unit,
) = PrintBedLayout(
    showLive = true,
    data = {
        val currentState = state()
        PrintBedData(
            renderContext = currentState.state.value.renderContext,
            printBed = currentState.printBed,
            isTrackingLive = currentState.state.value.isTrackingPrintProgress,
            plugin = {},
            printerProfile = currentState.printerProfile,
            settings = currentState.settings,
        )
    },
    bottomEndDecoration = {
        OctoIconButton(
            painter = painterResource(id = R.drawable.ic_round_fullscreen_24),
            onClick = onFullscreen,
            contentDescription = stringResource(id = R.string.cd_fullscreen)
        )
    },
    legend = { modifier ->
        GcodeControlsReadyData(
            modifier = modifier,
            state = state
        )
    }
)

@Composable
private fun GcodeControlsReadyData(
    modifier: Modifier,
    state: () -> GcodePreviewControlsViewModel.ViewState.DataReady,
) = Column(
    modifier = modifier
        .padding(end = OctoAppTheme.dimens.margin2)
) {
    val layerNumber by remember(state) {
        derivedStateOf {
            state().state.value.renderContext?.let { it.layerNumberDisplay(it.layerNumber) } ?: 0
        }
    }
    val layerCount by remember(state) {
        derivedStateOf {
            state().state.value.renderContext?.let { it.layerCountDisplay(it.layerCount) } ?: 0
        }
    }
    val layerProgress by remember(state) {
        derivedStateOf {
            state().state.value.renderContext?.layerProgress?.takeUnless { it.isNaN() }?.times(1000)?.roundToInt()?.div(10f) ?: 0f
        }
    }

    LaunchedEffect(layerCount) {
        Napier.i(tag = TAG, message = "[GCD] Received $layerCount layers")
    }

    Text(
        text = stringResource(id = R.string.gcode_preview___layer),
        style = OctoAppTheme.typography.subtitle,
        color = OctoAppTheme.colors.darkText,
        overflow = TextOverflow.Ellipsis,
    )
    Text(
        text = stringResource(
            id = R.string.x_of_y,
            layerNumber,
            layerCount
        ),
        style = OctoAppTheme.typography.label,
        textAlign = TextAlign.Center,
    )
    Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin1))
    Text(
        text = stringResource(id = R.string.gcode_preview___layer_progress),
        style = OctoAppTheme.typography.subtitle,
        color = OctoAppTheme.colors.darkText,
        overflow = TextOverflow.Ellipsis,
    )
    Text(
        text = layerProgress.formatAsPercent(),
        style = OctoAppTheme.typography.label,
        textAlign = TextAlign.Center,
    )
}


@Composable
internal fun GcodeControlsReadyLargeFileDownloadRequired(
    state: GcodePreviewControlsViewModel.ViewState.LargeFileDownloadRequired,
    onDownload: () -> Unit
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center,
    modifier = Modifier
        .aspectRatio(16 / 9f)
        .padding(OctoAppTheme.dimens.margin2)
) {
    Text(
        text = stringResource(id = R.string.gcode_preview___download_size_exceeded_title),
        style = OctoAppTheme.typography.subtitle,
        color = OctoAppTheme.colors.darkText,
        overflow = TextOverflow.Ellipsis,
        textAlign = TextAlign.Center,
    )
    Text(
        text = stringResource(id = R.string.gcode_preview___download_size_exceeded_description),
        style = OctoAppTheme.typography.labelSmall,
        textAlign = TextAlign.Center,
    )
    OctoButton(
        small = true,
        text = stringResource(id = R.string.download_x, state.downloadSize.formatAsFileSize()),
        modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
        onClick = onDownload
    )
}

@Composable
internal fun GcodeControlsError(
    state: GcodePreviewControlsViewModel.ViewState.Error,
    onRetry: () -> Unit
) = Box(
    Modifier
        .fillMaxSize()
        .aspectRatio(16 / 9f),
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(OctoAppTheme.dimens.margin2)
            .align(Alignment.Center)
    ) {
        Text(
            text = stringResource(id = R.string.error_general),
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.darkText,
            textAlign = TextAlign.Center,
            maxLines = 4,
            overflow = TextOverflow.Ellipsis,
        )
        OctoButton(
            small = true,
            text = stringResource(id = R.string.retry_general),
            modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
            onClick = onRetry
        )
    }

    val activity = OctoAppTheme.octoActivity
    OctoIconButton(
        painter = painterResource(id = R.drawable.ic_round_info_24),
        contentDescription = stringResource(id = R.string.cd_details),
        modifier = Modifier.align(Alignment.BottomEnd)
    ) {
        activity?.showDialog(state.exception)
    }
}

@Composable
internal fun GcodeControlsPrintingFromSdCard(
    state: GcodePreviewControlsViewModel.ViewState.PrintingFromSdCard,
) = Box(Modifier.fillMaxSize()) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .padding(OctoAppTheme.dimens.margin2)
            .aspectRatio(16 / 9f)
            .align(Alignment.Center)
    ) {
        Text(
            text = stringResource(R.string.gcode_preview___printing_from_sd_card, state.file.display),
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.darkText,
            textAlign = TextAlign.Center,
            maxLines = 4,
            overflow = TextOverflow.Ellipsis,
        )
    }
}

//endregion
//region Previews
@Composable
private fun OctoAppThemeForGcodeControlsPreview(content: @Composable BoxScope.() -> Unit) = OctoAppThemeForPreview(leftyMode = true) {
    GcodeControlsBox(null, content)
}

@Composable
@Preview
private fun PreviewLoadingIndeterminate() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsLoading(GcodePreviewControlsViewModel.ViewState.Loading())
}

@Composable
@Preview
private fun PreviewPrintingFromSdCard() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsPrintingFromSdCard(
        GcodePreviewControlsViewModel.ViewState.PrintingFromSdCard(
            FileObject.File(
                name = "Somefile.gcode",
                date = Instant.DISTANT_PAST,
                type = "type",
                size = 0,
                path = "path",
                gcodeAnalysis = null,
                hash = "hash",
                origin = FileOrigin.Other("sdcard"),
                prints = null,
                typePath = emptyList(),
            )
        )
    )
}

@Composable
@Preview
private fun PreviewLoadingDeterminate() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsLoading(GcodePreviewControlsViewModel.ViewState.Loading(0.66f))
}

@Composable
@Preview
private fun PreviewDisabled() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsDisabled(
        state = GcodePreviewControlsViewModel.ViewState.FeatureDisabled(printBed = GcodeNativeCanvas.Image.PrintBedEnder),
        onHideWhenDisabled = {}
    )
}

@Composable
@Preview
private fun PreviewError() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsError(
        state = GcodePreviewControlsViewModel.ViewState.Error(
            exception = object : Exception(), UserMessageException {
                override val userMessage = "This is a <b>crucial<i> error</i></b><br><a href=\"https://google.com\">Test</a>"

            }
        ),
        onRetry = {},
    )
}

@Composable
@Preview
private fun PreviewLargeFile() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsReadyLargeFileDownloadRequired(
        state = GcodePreviewControlsViewModel.ViewState.LargeFileDownloadRequired(384354742L),
        onDownload = {},
    )
}

@Composable
@Preview
private fun PreviewReady() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsReady(
        state = {
            GcodePreviewControlsViewModel.ViewState.DataReady(
                settings = GcodePreviewSettings(),
                printerProfile = PrinterProfile(
                    volume = PrinterProfile.Volume(
                        depth = 220f,
                        width = 220f,
                        height = 220f,
                        origin = PrinterProfile.Origin.LowerLeft
                    )
                ),
                printBed = GcodeNativeCanvas.Image.PrintBedEnder,
                state = staticStateOf(
                    GcodePreviewControlsViewModel.EnrichedRenderContext(
                        renderContext = GcodeRenderContext(
                            previousLayerPaths = emptyList(),
                            completedLayerPaths = emptyList(),
                            remainingLayerPaths = emptyList(),
                            layerCount = 42,
                            layerNumber = 0,
                            layerProgress = 13.37f,
                            layerZHeight = 0.4f,
                            gcodeBounds = null,
                            printHeadPosition = null,
                            layerNumberDisplay = { it * 10 },
                            layerCountDisplay = { it * 10 },
                        ),
                        exceedsPrintArea = true,
                        unsupportedGcode = true,
                        isTrackingPrintProgress = true,
                    )
                )
            )
        },
        onFullscreen = {}
    )
}

@Composable
@Preview
private fun PreviewReadyOddShape() = OctoAppThemeForGcodeControlsPreview {
    GcodeControlsReady(
        state = {
            GcodePreviewControlsViewModel.ViewState.DataReady(
                settings = GcodePreviewSettings(),
                printerProfile = PrinterProfile(
                    volume = PrinterProfile.Volume(
                        depth = 300f,
                        width = 220f,
                        height = 220f,
                        origin = PrinterProfile.Origin.LowerLeft
                    )
                ),
                printBed = GcodeNativeCanvas.Image.PrintBedGeneric,
                state = staticStateOf(
                    GcodePreviewControlsViewModel.EnrichedRenderContext(
                        renderContext = GcodeRenderContext(
                            previousLayerPaths = emptyList(),
                            completedLayerPaths = emptyList(),
                            remainingLayerPaths = emptyList(),
                            layerCount = 42,
                            layerNumber = 0,
                            layerProgress = 13.37f,
                            layerZHeight = 0.4f,
                            gcodeBounds = null,
                            printHeadPosition = null,
                            layerNumberDisplay = { it * 10 },
                            layerCountDisplay = { it * 10 },
                        ),
                        exceedsPrintArea = true,
                        unsupportedGcode = true,
                        isTrackingPrintProgress = true,
                    )
                )
            )
        },
        onFullscreen = {}
    )
}
//endregion

