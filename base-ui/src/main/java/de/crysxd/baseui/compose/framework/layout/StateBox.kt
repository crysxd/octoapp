package de.crysxd.baseui.compose.framework.layout

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoErrorDialog
import de.crysxd.baseui.compose.framework.components.OctoErrorDisplay
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.utils.FlowState
import kotlinx.coroutines.launch

@Composable
fun <T> StateBox(
    state: FlowState<T>,
    modifier: Modifier = Modifier,
    onRetry: (suspend () -> Unit)? = null,
    isEmpty: (T) -> Boolean,
    emptyText: String,
    content: @Composable (T) -> Unit,
) = StateBox(
    state = state,
    modifier = modifier,
    onRetry = onRetry,
    isEmpty = isEmpty,
    content = content,
    emptyContent = {
        Text(
            text = emptyText,
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.darkText,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(
                horizontal = OctoAppTheme.dimens.margin2,
                vertical = OctoAppTheme.dimens.margin5,
            )
        )
    },
)

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun <T> StateBox(
    state: FlowState<T>,
    modifier: Modifier = Modifier,
    onRetry: (suspend () -> Unit)? = null,
    isEmpty: (T) -> Boolean,
    emptyContent: @Composable () -> Unit,
    content: @Composable (T) -> Unit,
) {
    var displayedState by remember { mutableStateOf(state) }
    var loading by remember { mutableStateOf(false) }
    var showError by remember { mutableStateOf<Throwable?>(null) }

    LaunchedEffect(key1 = state) {
        loading = false
        when (state) {
            is FlowState.Ready -> displayedState = state
            is FlowState.Error -> displayedState = state
            is FlowState.Loading -> if (displayedState is FlowState.Error) {
                loading = true
            } else {
                displayedState = state
            }
        }
    }

    showError?.let { error ->
        OctoErrorDialog(throwable = error) {
            showError = null
        }
    }

    Crossfade(targetState = displayedState, modifier = modifier) { ds ->
        var refreshing by remember { mutableStateOf(false) }
        val scope = rememberCoroutineScope()
        val pullRefreshState = rememberPullRefreshState(
            refreshing = refreshing,
            onRefresh = {
                if (onRetry != null) {
                    scope.launch {
                        refreshing = true
                        onRetry()
                    }.invokeOnCompletion {
                        refreshing = false
                    }
                }
            }
        )

        Box(
            modifier = Modifier
                .fillMaxSize()
                .pullRefresh(pullRefreshState),
            contentAlignment = Alignment.TopCenter
        ) {
            when (ds) {
                is FlowState.Error -> OctoErrorDisplay(
                    throwable = ds.throwable,
                    onRetry = onRetry,
                    onAction = { showError = ds.throwable },
                    actionLabel = stringResource(id = R.string.show_details),
                    modifier = Modifier
                        .padding(top = OctoAppTheme.dimens.margin5)
                        .padding(horizontal = OctoAppTheme.dimens.margin2)
                )

                is FlowState.Loading -> CircularProgressIndicator(
                    modifier = Modifier.padding(OctoAppTheme.dimens.margin5)
                )

                is FlowState.Ready -> Crossfade(targetState = isEmpty(ds.data)) { empty ->
                    Box(modifier = modifier.fillMaxSize(), contentAlignment = Alignment.TopCenter) {
                        if (empty) {
                            emptyContent()
                        } else {
                            content(ds.data)
                        }
                    }
                }
            }

            PullRefreshIndicator(
                refreshing = refreshing,
                state = pullRefreshState,
                modifier = Modifier.align(Alignment.TopCenter)
            )
        }
    }
}