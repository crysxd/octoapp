package de.crysxd.baseui.compose.controls.quickaccess

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.octoapp.viewmodels.QuickAccessControlsViewModelCore

class QuickAccessControlsViewModel(
    private val instanceId: String,
) : ViewModel() {

    private val core = QuickAccessControlsViewModelCore(instanceId)
    val state get() = core.state


    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = QuickAccessControlsViewModel(
            instanceId = instanceId,
        ) as T
    }
}