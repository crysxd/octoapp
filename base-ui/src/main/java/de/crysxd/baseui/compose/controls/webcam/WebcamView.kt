package de.crysxd.baseui.compose.controls.webcam

import android.app.ActionBar
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.view.TextureView
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.VisibleForTesting
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.AnimatedVisibilityScope
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.animateOffsetAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.rememberTransformableState
import androidx.compose.foundation.gestures.transformable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.viewinterop.AndroidView
import androidx.media3.common.PlaybackException
import androidx.media3.common.Player
import androidx.media3.common.VideoSize
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.analytics.AnalyticsListener
import com.benasher44.uuid.uuid4
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoBadge
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.components.OctoLiveBadge
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.sharedcommon.utils.parseHtml
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import io.github.aakira.napier.Napier
import io.ktor.util.decodeBase64Bytes
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import java.util.concurrent.atomic.AtomicInteger
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.milliseconds
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State as WebcamState

@VisibleForTesting
var frame1Callback: () -> Unit = {}

@VisibleForTesting
var frame200Callback: () -> Unit = {}

@VisibleForTesting
val frameCounter = AtomicInteger(0)

private val Tag = "WebcamView"

@Composable
fun BoxScope.WebcamView(
    state: WebcamControlsState,
    boxState: AspectRatioBoxScope,
    allowTouch: Boolean,
    insets: PaddingValues = PaddingValues(),
) {
    WebcamState(
        controlsState = state,
        webcamState = state.current,
        boxState = boxState,
        allowTouch = allowTouch,
        insets = insets,
    )
}

val WebcamOverlayBrush = Brush.verticalGradient(
    0.0f to Color.Black.copy(alpha = 0f),
    0.8f to Color.Black.copy(alpha = 0.4f)
)

//region Controls
@Composable
fun WebcamActions(
    webcamState: WebcamControlsState,
    horizontal: Boolean = true,
    additionalActions: @Composable () -> Unit,
) {
    val canSwitchWebcam = webcamState.current.webcamCount > 1
    val canGrabImage = webcamState.grabImage != null
    val instanceId = LocalOctoPrint.current.id
    val exception = (webcamState.current as? WebcamState.Error)?.exception

    //region Switch
    AnimatedActionVisibility(horizontal = horizontal, visible = canSwitchWebcam) {
        OctoIconButton(
            onClick = webcamState::onSwitchWebcam,
            tint = Color.White,
            painter = painterResource(id = R.drawable.ic_round_switch_camera_24dp),
            contentDescription = stringResource(id = R.string.cd_switch_camera),
        )
    }
    //endregion
    //region Share
    AnimatedActionVisibility(horizontal = horizontal, visible = canGrabImage) {
        OctoIconButton(
            onClick = {
                webcamState.grabImage?.let { img ->
                    webcamState.onShareImage(instanceId = instanceId, img = img)
                }
            },
            tint = Color.White,
            painter = painterResource(id = R.drawable.ic_round_share_24),
            contentDescription = stringResource(id = R.string.cd_share),
        )
    }
    //endregion
    additionalActions()
    //region Fullscreen
    AnimatedActionVisibility(horizontal = horizontal, visible = webcamState.isFullscreenSupported) {
        OctoIconButton(
            onClick = webcamState::onFullscreenClicked,
            tint = Color.White,
            painter = painterResource(id = if (webcamState.isFullscreen) R.drawable.ic_round_fullscreen_exit_24 else R.drawable.ic_round_fullscreen_24),
            contentDescription = stringResource(id = R.string.cd_fullscreen),
        )
    }
    //endregion
    //region Error info
    val octoActivity = OctoAppTheme.octoActivity
    AnimatedActionVisibility(horizontal = horizontal, visible = exception != null) {
        OctoIconButton(
            onClick = { exception?.let { octoActivity?.showErrorDetailsDialog(it, offerSupport = false) } },
            tint = Color.White,
            painter = painterResource(id = R.drawable.ic_round_info_24),
            contentDescription = stringResource(id = R.string.cd_details),
        )
    }
    //endregion
}

@Composable
private fun AnimatedActionVisibility(visible: Boolean, horizontal: Boolean, content: @Composable AnimatedVisibilityScope.() -> Unit) = AnimatedVisibility(
    visible = visible,
    content = content,
    enter = fadeIn() + if (horizontal) expandHorizontally() else expandVertically(),
    exit = fadeOut() + if (horizontal) shrinkHorizontally() else shrinkVertically(),
)

//endregion
//region State machine
@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun BoxScope.WebcamState(
    controlsState: WebcamControlsState,
    webcamState: WebcamState,
    boxState: AspectRatioBoxScope,
    insets: PaddingValues,
    allowTouch: Boolean,
) {
    var isLive by remember { mutableStateOf(false) }
    var liveCountDownTo by remember { mutableStateOf<Instant?>(null) }
    var frameHeight by remember { mutableStateOf<Int?>(null) }

    AnimatedContent(targetState = webcamState) { state ->
        when (state) {
            is WebcamState.Hidden -> Unit

            is WebcamState.Loading -> {
                LaunchedEffect(Unit) {
                    controlsState.grabImage = null
                    boxState.aspectRatioOverride = boxState.measuredAspectRatio
                    boxState.wrapContentIgnoreAspectRatio = false
                    isLive = false
                    frameHeight = null
                }
                WebcamLoading()
            }

            is WebcamState.MjpegReady -> WebcamMjpeg(
                state = state,
                webcamState = controlsState,
                webcamBoxState = boxState,
                getInitialScaleToFillType = controlsState::getInitialScaleType,
                updateScaleToFillType = controlsState::updateScaleToFillType,
                allowTouch = allowTouch,
                onLiveChanged = { isLive = it },
                onAnimateLiveCountDownToChanged = { liveCountDownTo = it },
                onFrameHeightChanged = { frameHeight = it },
            )

            is WebcamState.VideoReady -> WebcamVideo(
                state = state,
                webcamState = controlsState,
                webcamBoxState = boxState,
                getInitialScaleToFillType = controlsState::getInitialScaleType,
                updateScaleToFillType = controlsState::updateScaleToFillType,
                onRetry = controlsState::onRetry,
                onLiveChanged = {
                    isLive = it
                    liveCountDownTo = null
                },
                allowTouch = allowTouch,
                onFrameHeightChanged = { frameHeight = it },
            )

            is WebcamState.WebpageReady -> WebcamWebpage(
                state = state,
                webcamState = controlsState,
                webcamBoxState = boxState,
                getInitialScaleToFillType = controlsState::getInitialScaleType,
                updateScaleToFillType = controlsState::updateScaleToFillType,
                onRetry = controlsState::onRetry,
                onLiveChanged = {
                    isLive = it
                    liveCountDownTo = null
                },
                allowTouch = allowTouch,
                onFrameHeightChanged = { frameHeight = it },
            )

            is WebcamState.Error -> {
                LaunchedEffect(Unit) {
                    controlsState.grabImage = null
                    boxState.wrapContentIgnoreAspectRatio = true
                    frameHeight = null
                    isLive = false
                }

                WebcamError(
                    error = state,
                    onRetry = controlsState::onRetry.takeIf { state.canRetry },
                    action = controlsState::onTroubleshoot,
                    actionLabel = stringResource(id = R.string.trouble_shooting).takeIf { state.canTroubleshoot }
                )
            }

            is WebcamState.FeatureNotAvailable -> {
                LaunchedEffect(Unit) {
                    controlsState.grabImage = null
                    boxState.wrapContentIgnoreAspectRatio = true
                    frameHeight = null
                    isLive = false
                }

                FeatureNotAvailable(
                    featureNotAvailable = state
                )
            }
        }

        LaunchedEffect(controlsState.current) {
            Napier.i(tag = "Webcam", message = "Webcam state: ${controlsState.current}")
        }
    }

    Indicators(
        insets = insets,
        isLive = isLive,
        frameHeight = frameHeight,
        showResolution = controlsState.showResolution,
        displayName = webcamState.displayName,
        showName = controlsState.showName,
        animateCountDownTo = liveCountDownTo,
    )
}

//endregion
//region Rich stream disabled
@Composable
private fun RichStreamDisabled() = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    Title(text = stringResource(id = R.string.supporter_perk___title))
    Detail(text = stringResource(id = R.string.supporter_perk___description, stringResource(id = R.string.rich_stream_disabled_title)))
    Action(text = stringResource(id = R.string.support_octoapp), onClick = {
        UriLibrary.getPurchaseUri().open()
    })
}

//endregion
//region Mjpeg stream
@Composable
private fun WebcamMjpeg(
    webcamState: WebcamControlsState,
    state: WebcamControlsViewModelCore.State.MjpegReady,
    allowTouch: Boolean,
    updateScaleToFillType: (ImageView.ScaleType) -> Unit,
    getInitialScaleToFillType: () -> ImageView.ScaleType,
    onLiveChanged: (Boolean) -> Unit,
    onFrameHeightChanged: (Int) -> Unit,
    onAnimateLiveCountDownToChanged: (Instant?) -> Unit,
    webcamBoxState: AspectRatioBoxScope,
) {
    //region State
    val scope = rememberCoroutineScope()
    var viewSize by remember { mutableStateOf(IntSize.Zero) }
    var image by remember { mutableStateOf(state.frames.currentData.image.asImageBitmap()) }
    var isScaleToFill by remember(getInitialScaleToFillType()) { mutableStateOf(getInitialScaleToFillType()) }
    val scaleCalculations by remember {
        derivedStateOf {
            val width = viewSize.width
            val height = viewSize.height
            val imageWidth = if (state.rotate90) state.frameHeight else state.frameWidth
            val imageHeight = if (state.rotate90) state.frameWidth else state.frameHeight

            val scaleToFill = max(width / imageWidth.toFloat(), height / imageHeight.toFloat())
            val scaleToFit = min(width / imageWidth.toFloat(), height / imageHeight.toFloat())
            val scale = if (isScaleToFill == ImageView.ScaleType.CENTER_CROP) scaleToFill else scaleToFit
            val size = IntSize((state.frameWidth * scale).roundToInt(), (state.frameHeight * scale).roundToInt())
            ScaleCalculationResult(
                outSize = size,
                inSize = IntSize(state.frameWidth, state.frameHeight),
                scaleToFill = scaleToFill,
                scaleToFit = scaleToFit,
                outSizeRotated = if (state.rotate90) IntSize(width = size.height, height = size.width) else size,
                scale = scale,
            )
        }
    }
    var zoom by remember { mutableStateOf(1f) }
    var pan by remember { mutableStateOf(Offset.Zero) }
    val animatedZoom by animateFloatAsState(zoom * scaleCalculations.scale, spring(dampingRatio = Spring.DampingRatioLowBouncy))
    val animatedPan by animateOffsetAsState(pan, spring(dampingRatio = Spring.DampingRatioLowBouncy))
    val transformState = rememberTransformableState { zoomChange, panChange, _ ->
        if (allowTouch) {
            zoom = (zoom * zoomChange).coerceIn(1f, 5f)
            pan = (pan + panChange).let { (x, y) ->
                val xLimit = (((scaleCalculations.outSizeRotated.width * zoom) - viewSize.width) / 2f).coerceAtLeast(0f)
                val yLimit = (((scaleCalculations.outSizeRotated.height * zoom) - viewSize.height) / 2f).coerceAtLeast(0f)
                Offset(x.coerceIn(-xLimit, xLimit), y.coerceIn(-yLimit, yLimit))
            }
        }
    }
    //endregion
    //region Collect frames
    DisposableEffect(Unit) {
        val sinkId = uuid4().toString()
        var liveJob: Job? = null
        state.frames.pushSink(sinkId) { frame ->
            //region Testing
            val fc = frameCounter.getAndIncrement()
            if (fc == 0) {
                frame1Callback()
            }
            if (fc == 200) {
                frame200Callback()
            }
            //endregion
            //region Live indicator
            onLiveChanged(true)
            if (state.frameInterval > 0) {
                onAnimateLiveCountDownToChanged(Clock.System.now() + state.frameInterval.milliseconds)
            } else {
                onAnimateLiveCountDownToChanged(null)
            }
            liveJob?.cancel("recomposed")
            liveJob = scope.launch {
                delay(frame.frameInterval + 1000)
                onLiveChanged(false)
            }
            //endregion
            image = state.frames.currentData.image.asImageBitmap()
        }

        onDispose {
            liveJob?.cancel("Disposed")
            state.frames.popSink(sinkId)
        }
    }
    //endregion
    //region Draw frames
    Box(
        modifier = Modifier
            .fillMaxSize()
            .pointerInput(Unit) {
                detectTapGestures(
                    onDoubleTap = {
                        val newType = when (isScaleToFill) {
                            ImageView.ScaleType.CENTER_CROP -> ImageView.ScaleType.FIT_CENTER
                            else -> ImageView.ScaleType.CENTER_CROP
                        }

                        zoom = 1f
                        pan = Offset.Zero
                        isScaleToFill = newType
                        updateScaleToFillType(newType)
                    },
                    onTap = {
                        // Open fullscreen with single tab
                        if (!webcamState.isFullscreen && webcamState.isFullscreenSupported) {
                            webcamState.onFullscreenClicked()
                        }
                    }
                )
            }
            .transformable(transformState, enabled = allowTouch)
            .graphicsLayer {
                scaleX = animatedZoom
                scaleY = animatedZoom
                translationX = animatedPan.x
                translationY = animatedPan.y
            }
            .onSizeChanged {
                viewSize = it
            }
            .drawBehind {
                rotate(if (state.rotate90) -90f else 0f) {
                    scale(if (state.flipH) -1f else 1f, if (state.flipV) -1f else 1f) {
                        drawImage(
                            image = image,
                            srcOffset = IntOffset.Zero,
                            dstOffset = IntOffset(
                                x = ((size.width - scaleCalculations.inSize.width) / 2).roundToInt(),
                                y = ((size.height - scaleCalculations.inSize.height) / 2).roundToInt()
                            ),
                            srcSize = scaleCalculations.inSize,
                            dstSize = scaleCalculations.inSize,
                        )
                    }
                }
            }
    )
    //endregion

    LaunchedEffect(webcamState.enforcedAspectRatio, state.frameHeight, state.rotate90) {
        //region Update size of outer box
        onFrameHeightChanged(state.frameHeight)
        webcamBoxState.updateVideoSize(
            nativeHeight = state.frameHeight,
            nativeWidth = state.frameWidth,
            rotate90 = state.rotate90,
            enforcedAspectRatio = webcamState.enforcedAspectRatio?.float
        )
        //endregion
        //region Actions
        webcamState.grabImage = {
            val matrix = Matrix()
            if (state.flipV) matrix.postScale(1f, -1f)
            if (state.flipH) matrix.postScale(-1f, 1f)
            if (state.rotate90) matrix.postRotate(-90f)
            val frame = state.frames.currentData.image
            Bitmap.createBitmap(frame, 0, 0, frame.width, frame.height, matrix, true)
        }
        //endregion
    }
}

private data class ScaleCalculationResult(
    val inSize: IntSize,
    val outSize: IntSize,
    val outSizeRotated: IntSize,
    val scaleToFill: Float,
    val scaleToFit: Float,
    val scale: Float,
)


@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun BoxScope.Indicators(
    insets: PaddingValues,
    isLive: Boolean,
    frameHeight: Int?,
    showResolution: Boolean,
    displayName: String?,
    showName: Boolean,
    animateCountDownTo: Instant? = null,
) = Row(
    modifier = Modifier
        .align(Alignment.TopStart)
        .padding(insets)
        .padding(OctoAppTheme.dimens.margin1),
) {
    //region Resolution
    AnimatedContent(targetState = frameHeight?.takeIf { showResolution }, modifier = Modifier.animateContentSize()) { height ->
        if (height != null) {
            OctoBadge(
                text = frameHeight?.let { "${it}p" } ?: "",
                visible = true,
                modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1)
            )
        }
    }
    //endregion
    //region Name
    AnimatedContent(targetState = displayName?.takeIf { showName && it.isNotBlank() }, modifier = Modifier.animateContentSize()) { name ->
        if (name != null) {
            OctoBadge(
                text = name,
                visible = true,
                modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1),
            )
        }
    }
    //endregion
    //region Live
    Spacer(modifier = Modifier.weight(1f))
    Crossfade(targetState = isLive) {
        OctoLiveBadge(
            visible = it,
            animateCountDownTo = animateCountDownTo,
        )
    }
    //endregion
}

//endregion
//region Video
@Composable
@androidx.annotation.OptIn(UnstableApi::class)
private fun WebcamVideo(
    webcamState: WebcamControlsState,
    state: WebcamControlsViewModelCore.State.VideoReady,
    allowTouch: Boolean,
    onLiveChanged: (Boolean) -> Unit,
    onFrameHeightChanged: (Int) -> Unit,
    updateScaleToFillType: (ImageView.ScaleType) -> Unit,
    getInitialScaleToFillType: () -> ImageView.ScaleType,
    onRetry: () -> Unit,
    webcamBoxState: AspectRatioBoxScope,
) = Box {
    //region Update size of outer box
    var videoSize by remember { mutableStateOf<IntSize?>(null) }
    DisposableEffect(webcamState.enforcedAspectRatio) {
        val listener = object : Player.Listener {
            override fun onVideoSizeChanged(vs: VideoSize) {
                super.onVideoSizeChanged(vs)
                val size = IntSize(vs.width, vs.height)
                onFrameHeightChanged(size.height)
                size.let {
                    webcamBoxState.updateVideoSize(
                        nativeHeight = it.height,
                        nativeWidth = it.width,
                        rotate90 = state.rotate90,
                        enforcedAspectRatio = webcamState.enforcedAspectRatio?.float
                    )
                }
                videoSize = size

            }
        }

        state.player.addListener(listener)
        onDispose { state.player.removeListener(listener) }
    }
    //endregion
    //region Analytics
    var loading by remember { mutableStateOf(!state.player.isPlaying) }
    var exception by remember { mutableStateOf<Exception?>(null) }
    DisposableEffect(Unit) {
        val listener = object : AnalyticsListener {
            override fun onIsPlayingChanged(eventTime: AnalyticsListener.EventTime, isPlaying: Boolean) {
                super.onIsPlayingChanged(eventTime, isPlaying)
                loading = !isPlaying

                if (isPlaying) {
                    exception = null
                }
            }

            override fun onPlayerError(eventTime: AnalyticsListener.EventTime, error: PlaybackException) {
                super.onPlayerError(eventTime, error)
                Napier.i(tag = "Webcam", message = "ExoPlayer playback error: ${error.cause} (${error.errorCodeName}/${error.errorCode})", throwable = error.cause)
                loading = false
                exception = error
            }
        }

        state.player.addAnalyticsListener(listener)
        state.player.play()
        onDispose { state.player.removeAnalyticsListener(listener) }
    }
    //endregion
    //region Live
    LaunchedEffect(exception) {
        while (isActive) {
            val live = state.player.isCurrentMediaItemLive
            val delay = state.player.currentLiveOffset
            val playing = state.player.isPlaying
            onLiveChanged(live && delay < LiveDelayThresholdMs && exception == null && playing)
            delay(1000)
        }
    }
    //endregion
    //region View
    AndroidView(
        factory = {
            val mv = MatrixView(it)
            val tv = TextureView(it)
            mv.allowTouch = allowTouch
            state.player.setVideoTextureView(tv)
            webcamState.grabImage = { requireNotNull(tv.bitmap) { "Texture bitmap was null" } }

            mv.addView(tv, ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
            mv.onScaleToFillChanged = { fill ->
                updateScaleToFillType(if (fill) ImageView.ScaleType.CENTER_CROP else ImageView.ScaleType.FIT_CENTER)
            }
            mv.scaleToFill = getInitialScaleToFillType() == ImageView.ScaleType.CENTER_CROP
            mv
        },
        update = {
            videoSize?.let { vs ->
                it.matrixInput = MatrixView.MatrixInput(
                    flipH = state.flipH,
                    flipV = state.flipV,
                    rotate90 = state.rotate90,
                    contentHeight = vs.height,
                    contentWidth = vs.width,
                )
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
    //endregion
    //region Nested states & Indicator
    NestedLoadingAndError(
        loading = loading,
        throwable = exception,
        state = state,
        webcamUrl = state.uri,
        onRetry = onRetry,
        webcamBoxState = webcamBoxState,
    )
    //endregion
}

//endregion
//region Web
@Composable
private fun WebcamWebpage(
    webcamState: WebcamControlsState,
    state: WebcamControlsViewModelCore.State.WebpageReady,
    allowTouch: Boolean,
    onLiveChanged: (Boolean) -> Unit,
    onFrameHeightChanged: (Int) -> Unit,
    updateScaleToFillType: (ImageView.ScaleType) -> Unit,
    getInitialScaleToFillType: () -> ImageView.ScaleType,
    onRetry: () -> Unit,
    webcamBoxState: AspectRatioBoxScope,
) = Box {
    //region State
    val internalState by state.content.state.collectAsState(initial = FlowState.Loading())
    LaunchedEffect(internalState) {
        onLiveChanged(internalState is FlowState.Ready)
        when (val s = internalState) {
            is FlowState.Ready -> {
                webcamBoxState.updateVideoSize(
                    nativeHeight = s.data.height,
                    nativeWidth = s.data.width,
                    rotate90 = state.rotate90,
                    enforcedAspectRatio = webcamState.enforcedAspectRatio?.float
                )
            }

            else -> Unit
        }
    }
    //endregion
    //region View
    AndroidView(
        factory = {
            val mv = MatrixView(it)
            mv.allowTouch = allowTouch
            webcamState.grabImage = {
                suspendCoroutine { suspense ->
                    state.content.webView.evaluateJavascript("takeSnapshot()") { dataUri ->
                        val bytes = dataUri.split(",").last().decodeBase64Bytes()
                        suspense.resume(BitmapFactory.decodeByteArray(bytes, 0, bytes.size))
                    }
                }
            }

            (state.content.webView.parent as? ViewGroup)?.removeView(state.content.webView)
            mv.addView(state.content.webView, ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
            mv.onScaleToFillChanged = { fill ->
                updateScaleToFillType(if (fill) ImageView.ScaleType.CENTER_CROP else ImageView.ScaleType.FIT_CENTER)
            }
            mv.scaleToFill = getInitialScaleToFillType() == ImageView.ScaleType.CENTER_CROP
            mv
        },
        update = {
            it.visibility = if (internalState is FlowState.Ready) VISIBLE else INVISIBLE
            (internalState as? FlowState.Ready)?.data?.let { s ->
                it.matrixInput = MatrixView.MatrixInput(
                    flipH = state.flipH,
                    flipV = state.flipV,
                    rotate90 = state.rotate90,
                    contentHeight = s.height,
                    contentWidth = s.width,
                )
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
    //endregion
    //region Nested states & Indicator
    NestedLoadingAndError(
        loading = internalState is FlowState.Loading,
        throwable = (internalState as? FlowState.Error)?.throwable,
        state = state,
        webcamUrl = state.webcamUrl,
        onRetry = onRetry,
        webcamBoxState = webcamBoxState,
    )
    //endregion
}

//endregion
//region Loading and Error
@Composable
private fun NestedLoadingAndError(
    state: WebcamControlsViewModelCore.State,
    webcamUrl: String?,
    loading: Boolean,
    throwable: Throwable?,
    onRetry: () -> Unit,
    webcamBoxState: AspectRatioBoxScope,
) {
    AnimatedContent(targetState = loading to throwable) { (loading, exception) ->
        if (exception != null) {
            //region Error
            val host = OctoAppTheme.octoActivity
            WebcamError(
                onRetry = onRetry,
                error = WebcamState.Error(
                    canRetry = true,
                    webcamCount = state.webcamCount,
                    exception = exception,
                    webcamUrl = webcamUrl,
                    activeWebcam = state.activeWebcam,
                    warning = state.warning,
                ),
                action = { host?.showErrorDetailsDialog(exception, offerSupport = false) },
                actionLabel = stringResource(id = R.string.show_details)
            )
            //endregion
        } else if (loading) {
            WebcamLoading()
        }
    }

    SideEffect {
        webcamBoxState.aspectRatioOverride = if (loading) webcamBoxState.measuredAspectRatio else null
        webcamBoxState.wrapContentIgnoreAspectRatio = false
    }
}

@Composable
private fun WebcamError(
    error: WebcamControlsViewModelCore.State.Error,
    action: () -> Unit = {},
    actionLabel: String? = null,
    onRetry: (() -> Unit)?,
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center,
    modifier = Modifier
        .fillMaxSize()
        .aspectRatio(16 / 9f)
        .background(OctoAppTheme.colors.blackTranslucent)
        .padding(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin3)
) {
    Title(text = stringResource(id = R.string.connection_failed))
    Detail(text = error.description ?: error.exception.composeErrorMessage())
    error.webcamUrl?.let {
        Detail(text = it)
    }
    Row(
        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
    ) {
        if (actionLabel != null) {
            OctoButton(
                type = OctoButtonType.Secondary,
                small = true,
                text = actionLabel,
                modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
                onClick = action,
            )
        }

        if (onRetry != null) {
            Action(
                text = stringResource(id = R.string.retry_general),
                onClick = onRetry
            )
        }
    }
}

@Composable
private fun FeatureNotAvailable(
    featureNotAvailable: WebcamState.FeatureNotAvailable,
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center,
    modifier = Modifier
        .fillMaxSize()
        .background(OctoAppTheme.colors.blackTranslucent)
        .padding(OctoAppTheme.dimens.margin2)
        .padding(vertical = OctoAppTheme.dimens.margin3)
) {
    Title(text = stringResource(id = R.string.supporter_perk___title))
    Detail(text = stringResource(id = R.string.supporter_perk___description, featureNotAvailable.featureName))
    Row(
        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
    ) {
        OctoButton(
            type = OctoButtonType.Primary,
            small = true,
            text = stringResource(id = R.string.support_octoapp),
            modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
            onClick = { UriLibrary.getPurchaseUri().open() },
        )
    }
}

@Composable
private fun WebcamLoading() = Box(
    modifier = Modifier
        .background(OctoAppTheme.colors.blackTranslucent)
        .fillMaxSize(),
    contentAlignment = Alignment.Center,
) {
    CircularProgressIndicator(
        color = Color.White
    )
}

//endregion
//region Internals
@Composable
private fun Title(
    text: String,
    maxLines: Int = 3,
) = Text(
    text = text,
    style = OctoAppTheme.typography.subtitle,
    color = OctoAppTheme.colors.white,
    textAlign = TextAlign.Center,
    maxLines = maxLines,
    overflow = TextOverflow.Ellipsis,
)

@Composable
private fun Detail(text: CharSequence) = Text(
    text = text.parseHtml().asAnnotatedString(),
    style = OctoAppTheme.typography.label,
    color = OctoAppTheme.colors.whiteTranslucent,
    textAlign = TextAlign.Center,
    maxLines = 5,
    modifier = Modifier.padding(top = OctoAppTheme.dimens.margin01),
    overflow = TextOverflow.Ellipsis,
)

@Composable
private fun Action(
    text: String,
    onClick: () -> Unit
) = OctoButton(
    small = true,
    text = text,
    modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
    onClick = onClick
)

private fun AspectRatioBoxScope.updateVideoSize(
    enforcedAspectRatio: Float?,
    nativeWidth: Int,
    nativeHeight: Int,
    rotate90: Boolean,
) {
    Napier.i(tag = Tag, message = "New video size: ${nativeWidth}x${nativeHeight} rotate90=$rotate90")
    // Skip for invalid sizes
    if (nativeHeight == 0 || nativeWidth == 0) return

    val nativeAspectRatio = if (rotate90) {
        nativeHeight / nativeWidth.toFloat()
    } else {
        nativeWidth / nativeHeight.toFloat()
    }

    aspectRatioOverride = null
    aspectRatio = enforcedAspectRatio ?: nativeAspectRatio
    wrapContentIgnoreAspectRatio = false
}
//endregion