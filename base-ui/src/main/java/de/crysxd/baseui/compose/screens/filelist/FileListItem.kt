package de.crysxd.baseui.compose.screens.filelist

import android.content.res.Configuration
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoListItem
import de.crysxd.baseui.compose.framework.components.OctoPicassoImage
import de.crysxd.baseui.compose.framework.helpers.rememberMenuSheetState
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalNavController
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.ShareFileUseCase
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.menu.files.FileActionsMenu
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import java.io.File

@Composable
fun FileListItem(
    modifier: Modifier = Modifier,
    file: FileReference,
) {
    val navController = LocalNavController.current
    val instanceId = LocalOctoPrint.current.id
    val context = LocalContext.current
    val actionsController = LocalFileActionsController.current
    val fileMenu = rememberMenuSheetState(
        menu = {
            FileActionsMenu(
                instanceId = instanceId,
                display = file.display,
                path = file.path,
                size = (file as? FileObject)?.size,
                isDirectory = file is FileObject.Folder,
            )
        },
        onResult = { result ->
            result ?: return@rememberMenuSheetState

            //region Share
            FileActionsMenu.getSharePath(result)?.let { sharePath ->
                BaseInjector.get().shareFileUseCase().execute(
                    ShareFileUseCase.Params(
                        context = context,
                        file = File(sharePath),
                    )
                )
            }
            //endregion
            //region Copy
            FileActionsMenu.isCopy(result).takeIf { it }?.let { _ ->
                actionsController.copy(instanceId = instanceId, path = file.path, display = file.display)
            }
            //endregion
            //region Cut
            FileActionsMenu.isCut(result).takeIf { it }?.let { _ ->
                actionsController.cut(instanceId = instanceId, path = file.path, display = file.display)
            }
            //endregion
        }
    )

    OctoListItem(
        modifier = modifier,
        thumbnail = {
            OctoPicassoImage(
                modifier = Modifier.size(if (isLargeThumbnails()) 100.dp else 64.dp),
                path = (file as? FileObject.File)?.let { if (isLargeThumbnails()) it.mediumThumbnail else it.smallThumbnail }?.path,
                fallback = when (file) {
                    is FileObject.Folder -> R.drawable.ic_round_folder_24
                    else -> R.drawable.ic_round_print_24
                },
            )
        },
        thumbnailBadge = when {
            file !is FileObject.File -> null
            file.prints?.last?.success == true -> painterResource(R.drawable.ic_round_check_24)
            file.prints?.last?.success == false -> painterResource(R.drawable.ic_round_close_24)
            else -> null
        }?.let { painter ->
            @Composable {
                Icon(
                    painter = painter,
                    contentDescription = null,
                    modifier = Modifier.size(16.dp)
                )
            }
        },
        title = {
            Text(
                text = file.display,
                maxLines = 2,
                overflow = androidx.compose.ui.text.style.TextOverflow.Ellipsis,
            )
        },
        detail = {
            (file as? FileObject.File)?.let { file ->
                Text(
                    text = stringResource(
                        R.string.x_y,
                        file.date.format(),
                        file.size?.formatAsFileSize() ?: ""
                    )
                )
            }
        },
        onClick = {
            navController().navigate(
                UriLibrary.getFileManagerUri(
                    instanceId = instanceId,
                    path = file.path,
                    label = file.display,
                    folder = file is FileObject.Folder
                ).encodedPathAndQuery
            )
        },
        onLongClick = {
            fileMenu.show()
        }
    )
}

@Composable
private fun isLargeThumbnails(): Boolean {
    val configuration = LocalConfiguration.current
    return if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
        configuration.screenWidthDp > 840
    } else {
        configuration.screenWidthDp > 600
    }
}
