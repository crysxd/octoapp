package de.crysxd.baseui.compose.menu

import android.content.pm.PackageManager
import android.os.Build
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.BuildConfig
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.helpers.staticStateOf
import de.crysxd.baseui.compose.framework.helpers.thenIf
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.PurchaseOffers
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.collectSaveWithRetry
import de.crysxd.octoapp.base.ext.purchaseOffers
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerFluidd
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerMainsail
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerMixed
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.OctoPrint
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.main.MainMenu
import de.crysxd.octoapp.menu.main.octoprint.OctoPrintMenu
import de.crysxd.octoapp.menu.main.printer.PowerControlsDeviceMenu
import de.crysxd.octoapp.menu.main.printer.PrinterMenu
import de.crysxd.octoapp.menu.main.settings.PrintNotificationsMenu
import de.crysxd.octoapp.menu.main.settings.SettingsMenu
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlin.time.Duration.Companion.seconds

@Composable
fun CustomMenuHeader(
    customMenuHeaderType: String,
    menuHost: MenuHost,
) = when {
    customMenuHeaderType.startsWith(MainMenu.MainMenuHeaderView) -> MainMenuHeader(menuHost)
    customMenuHeaderType.startsWith(PrintNotificationsMenu.NotificationStatusView) -> NotificationStatus()
    customMenuHeaderType.startsWith(PowerControlsDeviceMenu.PowerControlsDeviceMenuRgbwView) -> RgbwView(type = customMenuHeaderType)
    BuildConfig.DEBUG -> Text(
        text = customMenuHeaderType,
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.Red)
            .padding(vertical = 40.dp)
    )

    else -> Unit
}

@Composable
private fun NotificationStatus(
    hasPlugin: Boolean = LocalOctoPrint.current.settings?.plugins?.octoAppCompanion?.running == true,
    hasPermission: Boolean? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        LocalContext.current.checkSelfPermission(android.Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED
    } else {
        null
    },
) = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = OctoAppTheme.dimens.margin2)
        .padding(top = OctoAppTheme.dimens.margin2)
) {
    hasPermission?.let {
        NotificationStatusItem(
            text = stringResource(id = if (it) R.string.print_notifications_menu___has_permission else R.string.print_notifications_menu___missing_permission),
            checked = it
        )
    }

    NotificationStatusItem(
        text = stringResource(id = if (hasPlugin) R.string.print_notifications_menu___has_plugin else R.string.print_notifications_menu___missing_plugin_optional),
        checked = hasPlugin.takeIf { it } // false -> null -> warning
    )
}

@Composable
private fun NotificationStatusItem(text: String, checked: Boolean?) = Row(
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)
) {
    Icon(
        contentDescription = null,
        painter = painterResource(
            id = when (checked) {
                true -> R.drawable.ic_round_check_24
                false -> R.drawable.ic_round_close_24
                null -> R.drawable.ic_round_warning_amber_24
            }
        ),
        tint = OctoAppTheme.colors.white,
        modifier = Modifier
            .size(20.dp)
            .background(
                color = when (checked) {
                    true -> OctoAppTheme.colors.green
                    false -> OctoAppTheme.colors.red
                    null -> OctoAppTheme.colors.orange
                },
                shape = CircleShape
            )
            .padding(1.dp)
            // For warning to visually center
            .thenIf(checked == null) { padding(bottom = 1.dp) }

    )

    Text(
        text = text,
        color = OctoAppTheme.colors.normalText,
        style = OctoAppTheme.typography.label
    )
}

@Composable
private fun RgbwView(
    type: String
) = Box(
    contentAlignment = Alignment.Center,
    modifier = Modifier.fillMaxWidth()
) {
    var colors by remember { mutableStateOf<List<PowerDevice.RgbwColor?>>(emptyList()) }
    val size = 16.dp
    LaunchedEffect(key1 = type) {
        try {
            SharedBaseInjector.getOrNull()?.getPowerDevicesUseCase()?.execute(
                param = GetPowerDevicesUseCase.Params(onlyGetDeviceWithUniqueId = type.split("/").last())
            )?.results?.firstOrNull()?.state?.invoke()?.collectSaveWithRetry { state ->
                colors = ((state as? GetPowerDevicesUseCase.PowerState.Color)?.colors ?: emptyList()).take(50)
            }
        } catch (e: Exception) {
            Napier.e(tag = "RgbwView", message = "Failure in observation", throwable = e)
        }
    }

    Crossfade(colors, label = "rgb") { target ->
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(bottom = OctoAppTheme.dimens.margin12, top = OctoAppTheme.dimens.margin01)
                .padding(horizontal = OctoAppTheme.dimens.margin2)
                .background(shape = RoundedCornerShape(50), color = OctoAppTheme.colors.veryLightGrey)
                .padding(OctoAppTheme.dimens.margin0)
                .clip(shape = RoundedCornerShape(50))
                .height(size)
                .horizontalScroll(rememberScrollState())
        ) {
            target.forEachIndexed { index, color ->
                if (index > 0) {
                    Box(
                        modifier = Modifier
                            .size(width = size / 3, height = 2.dp)
                            .background(color = OctoAppTheme.colors.lightGrey)
                    )
                }

                Box(
                    modifier = Modifier
                        .size(size)
                        .drawBehind {
                            drawCircle(color = Color(red = color?.r ?: 0f, green = color?.g ?: 0f, blue = color?.b ?: 0f))
                            drawCircle(radius = this.size.minDimension * 0.2f, color = Color.White.copy(alpha = color?.w ?: 0f))
                        }
                )
            }
        }
    }
}

@Composable
private fun MainMenuHeader(
    menuHost: MenuHost,
    config: PurchaseOffers.PurchaseConfig = Firebase.remoteConfig.purchaseOffers.activeConfig,
    hasPremium: State<Boolean> = remember { BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH) }.collectAsState(
        initial = BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)
    ),
    shouldAdvertisePremium: State<Boolean> = remember { BillingManager.shouldAdvertisePremiumFlow() }.collectAsState(
        initial = BillingManager.shouldAdvertisePremium()
    )
) {
    val texts = config.textsWithData
    val hasBanner = !texts.launchPurchaseScreenHighlight.isNullOrBlank() && shouldAdvertisePremium.value
    val background by animateColorAsState(targetValue = if (hasBanner) OctoAppTheme.colors.menuStyleSupportBackground else Color.Transparent)
    val cta = texts.launchPurchaseScreenCta.takeUnless { it.isBlank() } ?: stringResource(id = R.string.support_octoapp)

    Column(
        modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin2)
    ) {
        if (shouldAdvertisePremium.value || hasPremium.value) {
            Column(
                modifier = Modifier
                    .animateContentSize()
                    .padding(bottom = OctoAppTheme.dimens.margin1)
                    .drawBehind { drawRect(background) }
                    .padding(horizontal = OctoAppTheme.dimens.margin2)
                    .padding(bottom = OctoAppTheme.dimens.margin1, top = OctoAppTheme.dimens.margin2),
                verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
            ) {
                SaleBanner(shouldAdvertisePremium, config)
                SwitchInstanceButton(hasPremium, menuHost)
                SupportOctoAppButton(shouldAdvertisePremium, menuHost, cta)
            }
        } else {
            Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin3))
        }

        MainMenuButtons(menuHost)
    }
}

@Composable
private fun MainMenuButtons(menuHost: MenuHost) = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
    modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin2)
) {
    val instanceId = LocalOctoPrint.current.id

    MainMenuHeaderRow {
        SimpleMenuItem(
            title = stringResource(id = R.string.main_menu___item_show_settings),
            icon = painterResource(id = R.drawable.ic_round_settings_24),
            style = MenuItemStyle.Settings,
            onClick = { menuHost.pushMenu(SettingsMenu()) },
            showAsOutline = true,
            showAsSubMenu = true,
            modifier = Modifier.weight(1f),
        )
        SimpleMenuItem(
            title = stringResource(id = R.string.main_menu___item_show_tutorials),
            icon = painterResource(id = R.drawable.ic_round_school_24),
            style = MenuItemStyle.Neutral,
            onClick = { menuHost.openUrl(UriLibrary.getTutorialsUri()) },
            showAsOutline = true,
            showAsSubMenu = true,
            badgeCount = if (OctoAppTheme.isPreview) 1 else BaseInjector.get().tutorialsRepository().getNewTutorialsCount(),
            modifier = Modifier.weight(1f),
        )
    }

    MainMenuHeaderRow {
        SimpleMenuItem(
            title = LocalOctoPrint.current.systemInfo?.interfaceType.label,
            icon = painterResource(
                id = when (LocalOctoPrint.current.systemInfo?.interfaceType) {
                    OctoPrint -> R.drawable.ic_octoprint_24px
                    MoonrakerFluidd -> R.drawable.ic_fluidd_24px
                    MoonrakerMainsail -> R.drawable.ic_mainsail_24px
                    MoonrakerMixed -> R.drawable.ic_klipper_24px
                    null -> R.drawable.baseline_device_hub_24
                }
            ),
            style = MenuItemStyle.OctoPrint,
            onClick = { menuHost.pushMenu(OctoPrintMenu(instanceId)) },
            showAsOutline = true,
            showAsSubMenu = true,
            modifier = Modifier.weight(1f),
        )
        SimpleMenuItem(
            title = stringResource(id = R.string.main_menu___item_show_printer),
            icon = painterResource(id = R.drawable.ic_round_print_24),
            style = MenuItemStyle.Printer,
            onClick = { menuHost.pushMenu(PrinterMenu(instanceId)) },
            showAsOutline = true,
            showAsSubMenu = true,
            modifier = Modifier.weight(1f),
        )
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun SaleBanner(
    shouldAdvertisePremium: State<Boolean>,
    config: PurchaseOffers.PurchaseConfig
) {
    fun getText() = config.textsWithData.launchPurchaseScreenHighlight
    var text by remember { mutableStateOf(getText()) }
    LaunchedEffect(Unit) {
        while (isActive) {
            delay(1.seconds)
            text = getText()
        }
    }

    AnimatedContent(
        targetState = text.takeIf { shouldAdvertisePremium.value },
        transitionSpec = { fadeIn() with fadeOut() }
    ) {
        if (!it.isNullOrBlank()) {
            Text(
                text = it.toHtml().asAnnotatedString(),
                style = OctoAppTheme.typography.base,
                color = OctoAppTheme.colors.darkText,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}


@Composable
private fun ColumnScope.SwitchInstanceButton(
    hasPremium: State<Boolean>,
    menuHost: MenuHost,
) {
    AnimatedVisibility(visible = hasPremium.value) {
        SimpleMenuItem(
            title = stringResource(id = R.string.main_menu___item_change_octoprint_instance),
            icon = painterResource(id = R.drawable.ic_round_swap_horiz_24),
            style = MenuItemStyle.Neutral,
            onClick = {
                menuHost.closeMenu(MainMenu.ResultOpenControlCenter)
            },
        )
    }
}

@Composable
private fun ColumnScope.SupportOctoAppButton(
    shouldAdvertisePremium: State<Boolean>,
    menuHost: MenuHost,
    ctaText: String
) {
    AnimatedVisibility(visible = shouldAdvertisePremium.value) {
        SimpleMenuItem(
            title = ctaText,
            icon = painterResource(id = R.drawable.ic_round_favorite_24),
            style = MenuItemStyle.Support,
            onClick = { menuHost.openUrl(UriLibrary.getPurchaseUri()) },
        )
    }
}

@Composable
private fun MainMenuHeaderRow(content: @Composable RowScope.() -> Unit) = Row(
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    content = content
)

//region Preview
@Preview("Main")
@Composable
fun PreviewMainMenuHeaderSale() = OctoAppThemeForPreview {
    MainMenuHeader(
        menuHost = previewHost,
        config = PurchaseOffers.Default.activeConfig.copy(
            texts = PurchaseOffers.Default.activeConfig.texts.copy(
                launchPurchaseScreenHighlight = "This is a banner with <b>HTML</b>",
                launchPurchaseScreenCta = "Some text"
            )
        ),
        hasPremium = staticStateOf(false),
        shouldAdvertisePremium = staticStateOf(true),
    )
}


@Preview("Main")
@Composable
fun PreviewMainMenuHeaderNoQuickSwitch() = OctoAppThemeForPreview {
    MainMenuHeader(
        menuHost = previewHost,
        config = PurchaseOffers.Default.activeConfig,
        hasPremium = staticStateOf(false),
        shouldAdvertisePremium = staticStateOf(true),
    )
}

@Preview("Main")
@Composable
fun PreviewMainMenuHeaderNoPremium() = OctoAppThemeForPreview {
    MainMenuHeader(
        menuHost = previewHost,
        config = PurchaseOffers.Default.activeConfig,
        hasPremium = staticStateOf(false),
        shouldAdvertisePremium = staticStateOf(false),
    )
}

@Preview("Main")
@Composable
fun PreviewMainMenuHeaderQuickSwitch() = OctoAppThemeForPreview {
    MainMenuHeader(
        menuHost = previewHost,
        config = PurchaseOffers.Default.activeConfig,
        hasPremium = staticStateOf(true),
        shouldAdvertisePremium = staticStateOf(false),
    )
}

@Preview(group = "Notification")
@Composable
fun PreviewMainMenuHeaderNotificationsAllGood() = OctoAppThemeForPreview {
    NotificationStatus(
        hasPermission = true,
        hasPlugin = true,
    )
}

@Preview(group = "Notification")
@Composable
fun PreviewMainMenuHeaderNotificationsNoPermissionNeeded() = OctoAppThemeForPreview {
    NotificationStatus(
        hasPermission = null,
        hasPlugin = true,
    )
}

@Preview(group = "Notification")
@Composable
fun PreviewMainMenuHeaderNotificationsNoPermission() = OctoAppThemeForPreview {
    NotificationStatus(
        hasPermission = false,
        hasPlugin = true,
    )
}


@Preview(group = "Notification")
@Composable
fun PreviewMainMenuHeaderNotificationsNoPlugin() = OctoAppThemeForPreview {
    NotificationStatus(
        hasPermission = true,
        hasPlugin = false,
    )
}

private
val previewHost by lazy {
    object : MenuHost {
        override suspend fun pushMenu(subMenu: Menu) = throw NotImplementedError()
        override suspend fun popMenu() = throw NotImplementedError()
        override suspend fun closeMenu(result: String?) = throw NotImplementedError()
        override fun openUrl(url: Url) = throw NotImplementedError()
        override suspend fun reloadMenu() = throw NotImplementedError()
    }
}
//endregion