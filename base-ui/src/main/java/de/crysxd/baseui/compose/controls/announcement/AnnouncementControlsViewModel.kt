package de.crysxd.baseui.compose.controls.announcement

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.ext.connectCore
import de.crysxd.octoapp.viewmodels.AnnouncementControlsViewModelCore
import kotlinx.coroutines.launch

class AnnouncementControlsViewModel(instanceId: String) : BaseViewModel() {

    private val core = connectCore(AnnouncementControlsViewModelCore(instanceId = instanceId))
    val state = core.state

    fun checkNotificationNow() = viewModelScope.launch {
        core.checkNotificationNow()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = AnnouncementControlsViewModel(instanceId = instanceId) as T
    }
}