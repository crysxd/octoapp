package de.crysxd.baseui.compose.screens.terminal

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.viewmodels.TerminalViewModelCore

@Composable
internal fun StyledTerminalLine(
    modifier: Modifier = Modifier,
    onExecute: suspend () -> Unit,
    canExecute: Boolean,
    line: TerminalViewModelCore.TerminalLogLine,
) {
    val background = OctoAppTheme.colors.inputBackground
    val startMarginInset = OctoAppTheme.dimens.margin2
    val lineInset = OctoAppTheme.dimens.margin1

    val shape = remember(line.isEnd, line.isStart) {
        StyledTerminalShape(
            isStart = line.isStart,
            isEnd = line.isEnd,
            isMiddle = line.isMiddle,
            lineInset = lineInset,
            startMarginInset = startMarginInset,
        )
    }


    Row(
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .background(background, shape)
            .padding(shape.padding)
    ) {
        Text(
            text = line.textStyled,
            style = if (line.isStart) OctoAppTheme.typography.focus else OctoAppTheme.typography.base,
            color = if (line.isStart) OctoAppTheme.colors.darkText else OctoAppTheme.colors.normalText,
            modifier = Modifier
                .weight(1f)
                .padding(vertical = OctoAppTheme.dimens.margin0),
        )

        AnimatedVisibility(
            visible = canExecute && line.isStart,
            modifier = Modifier.height(24.dp)
        ) {
            OctoIconButton(
                usePadding = false,
                painter = painterResource(R.drawable.ic_round_send_24),
                contentDescription = stringResource(R.string.cd_execute),
                onClick = onExecute,
            )
        }
    }
}

private class StyledTerminalShape(
    private val isStart: Boolean,
    private val isEnd: Boolean,
    private val isMiddle: Boolean,
    private val lineInset: Dp,
    private val startMarginInset: Dp,
) : Shape {

    val padding
        @Composable get() = when {
            isStart -> PaddingValues(
                start = OctoAppTheme.dimens.margin3,
                end = OctoAppTheme.dimens.margin2,
                top = OctoAppTheme.dimens.margin12 + lineInset,
                bottom = OctoAppTheme.dimens.margin12 + lineInset,
            )

            isEnd -> PaddingValues(
                start = OctoAppTheme.dimens.margin3,
                end = OctoAppTheme.dimens.margin2,
                top = 0.dp,
                bottom = lineInset * 3,
            )

            else -> PaddingValues(
                start = OctoAppTheme.dimens.margin3,
                end = OctoAppTheme.dimens.margin2,
                top = 0.dp,
                bottom = 0.dp,
            )
        }

    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline = with(density) {
        val p = Path()
        when {
            isStart -> topShape(p, size)
            isEnd -> bottomShape(p, size)
            isMiddle -> middleShape(p, size)
        }
        p.close()
        Outline.Generic(p)
    }

    private fun Density.topShape(path: Path, size: Size) {
        val bottomLineY = size.height - lineInset.toPx()
        val top = lineInset.toPx()
        path.moveTo(size.width, top)
        path.lineTo(size.width, bottomLineY)
        path.arcTo(
            rect = Rect(lineInset.toPx() + startMarginInset.toPx(), bottomLineY, lineInset.toPx() * 2 + startMarginInset.toPx(), size.height),
            startAngleDegrees = 270f,
            sweepAngleDegrees = -90f,
            forceMoveTo = false
        )
        path.lineTo(lineInset.toPx() + startMarginInset.toPx(), size.height)
        path.lineTo(startMarginInset.toPx(), size.height)
        path.arcTo(
            rect = Rect(startMarginInset.toPx(), top, lineInset.toPx() * 4 + startMarginInset.toPx(), lineInset.toPx() * 4),
            startAngleDegrees = 180f,
            sweepAngleDegrees = 90f,
            forceMoveTo = false
        )
    }

    private fun Density.middleShape(path: Path, size: Size) {
        path.moveTo(startMarginInset.toPx(), 0f)
        path.lineTo(startMarginInset.toPx() + lineInset.toPx(), 0f)
        path.lineTo(startMarginInset.toPx() + lineInset.toPx(), size.height)
        path.lineTo(startMarginInset.toPx(), size.height)
    }

    private fun Density.bottomShape(path: Path, size: Size) {
        val topLineY = size.height - lineInset.toPx() * 2
        val bottom = size.height - lineInset.toPx()
        path.moveTo(startMarginInset.toPx(), 0f)
        path.lineTo(startMarginInset.toPx() + lineInset.toPx(), 0f)
        path.arcTo(
            rect = Rect(lineInset.toPx() + startMarginInset.toPx(), topLineY - lineInset.toPx(), lineInset.toPx() * 2 + startMarginInset.toPx(), topLineY),
            startAngleDegrees = 180f,
            sweepAngleDegrees = -90f,
            forceMoveTo = false
        )
        path.lineTo(size.width, topLineY)
        path.lineTo(size.width, bottom)
        path.arcTo(
            rect = Rect(startMarginInset.toPx(), topLineY - lineInset.toPx(), lineInset.toPx() * 2 + startMarginInset.toPx(), bottom),
            startAngleDegrees = 90f,
            sweepAngleDegrees = 90f,
            forceMoveTo = false
        )
    }
}