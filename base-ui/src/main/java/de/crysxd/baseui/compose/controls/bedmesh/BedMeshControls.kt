package de.crysxd.baseui.compose.controls.bedmesh

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ActionButton
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.ControlsScaffoldActionAreaScope
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.PrintBedData
import de.crysxd.baseui.compose.controls.PrintBedLayout
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalCompactLayout
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.toCompose
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.menu.controls.BedMeshProfilesMenu
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import de.crysxd.octoapp.viewmodels.BedMeshControlsViewModelCore.State
import de.crysxd.octoapp.viewmodels.helper.bedmesh.BedMeshParser
import de.crysxd.octoapp.viewmodels.helper.bedmesh.BedMeshRenderPlugin
import kotlinx.coroutines.launch

@Composable
fun BedMeshControls(
    state: BedMeshControlsState,
    editState: EditState,
    modifier: Modifier = Modifier,
) = AnimatedVisibility(
    visible = state.data !is State.Hidden,
    modifier = modifier,
) {
    ControlsScaffold(
        title = stringResource(id = R.string.widget_bed_mesh),
        actionArea = { ActionArea(state = state) },
        editState = editState,
    ) {
        InternalState(state = state)
    }
}


//region Internals
@Composable
private fun ControlsScaffoldActionAreaScope.ActionArea(
    state: BedMeshControlsState
) = ActionsArea(
    state = state,
) { painter, loading, onClick ->
    ActionButton(
        painter = painter,
        loading = loading,
        onClick = onClick,
    )
}

@Composable
private fun ActionsArea(
    state: BedMeshControlsState,
    button: @Composable (Painter, Boolean, () -> Unit) -> Unit,
) {
    val activity = OctoAppTheme.octoActivity
    val scope = rememberCoroutineScope()
    var loading by remember { mutableStateOf(false) }
    var showMenu by remember { mutableStateOf(false) }
    val instanceId = LocalOctoPrint.current.id

    button(
        painterResource(id = R.drawable.ic_round_refresh_24),
        loading,
    ) {
        activity?.showDialog(
            OctoActivity.Message.DialogMessage(
                text = { getString(R.string.widget_bed_mesh___confirmation_message) },
                positiveButton = { getString(R.string.widget_bed_mesh___confirmation_action) },
                negativeButton = { getString(R.string.cancel) },
                positiveAction = {
                    scope.launch {
                        try {
                            loading = true
                            state.refreshMesh()
                        } catch (e: Exception) {
                            activity.showDialog(e)
                        } finally {
                            loading = false
                        }
                    }
                }
            )
        )
    }

    if ((state.data as? State.Available)?.hasProfiles == true) {
        button(
            painterResource(id = R.drawable.ic_round_swap_horiz_24),
            false
        ) {
            showMenu = true
        }
    }

    if (showMenu) {
        MenuSheet(
            menu = BedMeshProfilesMenu(instanceId = instanceId),
            onDismiss = { showMenu = false }
        )
    }
}

@Composable
private fun InternalState(state: BedMeshControlsState) = Box(
    contentAlignment = Alignment.Center,
    modifier = Modifier
        .fillMaxWidth()
        .aspectRatio(16 / 9f)
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.inputBackground)
) {
    val renderer = remember { BedMeshRenderPlugin() }
    PrintBedLayout(
        showLive = false,
        data = {
            PrintBedData(
                renderContext = GcodeRenderContext.Empty,
                printBed = state.data.printBed,
                isTrackingLive = false,
                plugin = (state.data as? State.Available)?.mesh?.let { mesh ->
                    { renderer.render(canvas = this, mesh = mesh) }
                },
                printerProfile = state.data.printerProfile,
                settings = state.data.settings
            )
        },
        legend = { modifier ->
            NotAvailable(state = state, modifier = modifier)
            Available(state = state.data, modifier = modifier)
        },
        bottomEndDecoration = {
            if (LocalCompactLayout.current) {
                Row {
                    ActionsArea(
                        state = state,
                    ) { painter, loading, onClick ->
                        OctoIconButton(
                            painter = painter,
                            loading = loading,
                            onClick = onClick,
                            contentDescription = null,
                        )
                    }
                }
            }
        },
    )
}

@Composable
private fun Available(
    state: State,
    modifier: Modifier
) {
    if (state !is State.Available) return

    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12),
        modifier = modifier.padding(OctoAppTheme.dimens.margin12),
    ) {
        state.mesh.legend.forEach { (value, color) ->
            LegendItem(color = color.toCompose(), value = value)
        }
    }
}

@Composable
private fun LegendItem(color: Color, value: Float) = Row(
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
    verticalAlignment = Alignment.CenterVertically,
) {
    Box(
        modifier = Modifier
            .background(color = color, shape = CircleShape)
            .size(10.dp, 10.dp)
    )

    Text(
        text = value.formatAsLength(onlyMilli = true, minDecimals = 3, maxDecimals = 3),
        style = OctoAppTheme.typography.label,
        color = OctoAppTheme.colors.normalText,
    )
}

@Composable
private fun NotAvailable(
    state: BedMeshControlsState,
    modifier: Modifier
) {
    if (state.data !is State.NotAvailable) return

    Text(
        text = stringResource(id = R.string.no_data_available),
        style = OctoAppTheme.typography.subtitle,
        color = OctoAppTheme.colors.normalText,
        modifier = modifier.padding(OctoAppTheme.dimens.margin12),
    )
}

//endregion
//region State
@Composable
fun rememberBedMeshControlsState(): BedMeshControlsState {
    val vmFactory = BedMeshControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = BedMeshControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )
    val data = vm.state.collectAsState(initial = State.Hidden)

    return remember(vm, data) {
        object : BedMeshControlsState {
            override val data by data
            override suspend fun refreshMesh() = vm.refresh()
        }
    }
}

interface BedMeshControlsState {
    val data: State
    suspend fun refreshMesh()
}

//endregion

@Preview
@Composable
private fun PreviewAvailable2() = OctoAppThemeForPreview {
    BedMeshControls(
        editState = EditState.ForPreview,
        state = object : BedMeshControlsState {
            override val data = State.Available(
                printBed = GcodeNativeCanvas.Image.PrintBedEnder,
                printerProfile = PrinterProfile(
                    volume = PrinterProfile.Volume(
                        width = 235f,
                        depth = 235f,
                        height = 250f,
                    )
                ),
                hasProfiles = true,
                mesh = BedMeshParser().parse(
                    meshX = listOf(0f, 78f, 157f, 235f),
                    meshY = listOf(0f, 78f, 157f, 235f),
                    mesh = listOf(
                        listOf(+0.022f, +0.002f, -0.000f, -0.028f),
                        listOf(-0.030f, -0.028f, +0.010f, +0.012f),
                        listOf(-0.025f, -0.053f, -0.003f, +0.017f),
                        listOf(-0.010f, -0.038f, -0.015f, +0.012f),
                    ),
                    graphZMin = -0.2f,
                    graphZMax = 0.2f,
                    colorScale = Settings.BedMesh.DefaultColorScale,
                    bedMinX = 0f,
                    bedMinY = 0f,
                    bedMaxX = 235f,
                    bedMaxY = 235f,
                ),
            )

            override suspend fun refreshMesh() = Unit
        },
    )
}

@Preview
@Composable
private fun PreviewNotAvailable() = OctoAppThemeForPreview {
    BedMeshControls(
        editState = EditState.ForPreview,
        state = object : BedMeshControlsState {
            override val data = State.NotAvailable(
                printBed = GcodeNativeCanvas.Image.PrintBedEnder,
                printerProfile = PrinterProfile(),
            )

            override suspend fun refreshMesh() = Unit
        },
    )
}
//endregion