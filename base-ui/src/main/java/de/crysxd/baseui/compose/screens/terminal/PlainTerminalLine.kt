package de.crysxd.baseui.compose.screens.terminal

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.viewmodels.TerminalViewModelCore

@Composable
internal fun PlainTerminalLine(
    modifier: Modifier = Modifier,
    line: TerminalViewModelCore.TerminalLogLine,
) {
    Text(
        text = line.text,
        style = OctoAppTheme.typography.base,
        modifier = modifier
            .padding(vertical = OctoAppTheme.dimens.margin0, horizontal = OctoAppTheme.dimens.margin2)
    )
}