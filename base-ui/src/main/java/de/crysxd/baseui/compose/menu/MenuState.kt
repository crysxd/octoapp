package de.crysxd.baseui.compose.menu

import android.os.Parcel
import androidx.compose.runtime.Composable
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuAnimation
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuItem
import kotlinx.coroutines.flow.Flow


data class EmptyState(
    val animation: MenuAnimation?,
    val actionText: String?,
    val action: suspend () -> Unit,
) {
    override fun hashCode() = listOf(
        animation.hashCode() + 1,
        actionText.hashCode() + 2,
    ).sum() + 14

    override fun equals(other: Any?) = other is EmptyState && hashCode() == other.hashCode()
}

suspend fun Menu.prepare(
    menuHost: MenuHost?,
    menuContext: MenuContext,
    systemInfo: SystemInfo
) = PreparedMenu(
    raw = this,
    subtitle = subtitleFlow,
    bottomText = bottomTextFlow,
    items = getMenuItems().filter(menuContext, systemInfo),
    customHeaderViewType = customHeaderViewType,
    customViewType = customViewType,
    emptyState = createEmptyState(menuHost),
    title = title.takeIf { it.isNotBlank() },
)

suspend fun List<MenuItem>.filter(context: MenuContext, systemInfo: SystemInfo) = sortedBy {
    it.order
}.filter {
    it.isVisible(context, systemInfo)
}

fun Menu.createEmptyState(menuHost: MenuHost?) = EmptyState(
    animation = emptyStateAnimation,
    actionText = emptyStateActionText,
    action = { emptyStateAction(menuHost) },
)

data class AdhocMenu(
    private val items: List<MenuItem>
) : Menu {
    constructor(vararg items: MenuItem) : this(items.toList())

    override val id get() = items.joinToString { it.itemId }
    override val title: String = ""
    override suspend fun getMenuItems() = items
    override fun describeContents() = throw UnsupportedOperationException("Can't parcelize")
    override fun writeToParcel(p0: Parcel, p1: Int) = throw UnsupportedOperationException("Can't parcelize")
}

interface MenuHostState {
    @get:Composable
    val menu: PreparedMenu?
    val canNavigateUp: Boolean
    val menuId: MenuId
    val host: MenuHost
    suspend fun navigateUp()
}

data class PreparedMenu(
    val raw: Menu?,
    val subtitle: Flow<String?>?,
    val items: List<MenuItem>,
    val bottomText: Flow<String?>?,
    val title: String?,
    val emptyState: EmptyState,
    val customHeaderViewType: String?,
    val customViewType: String?,
) {
    override fun hashCode() = listOf(
        raw.hashCode() + 1,
        items.map { it.itemId }.hashCode() + 2,
        title.hashCode() + 3,
        emptyState.hashCode() + 4,
        customHeaderViewType.hashCode() + 5,
        customViewType.hashCode() + 6,
    ).sum() + 11

    override fun equals(other: Any?) = other is PreparedMenu && hashCode() == other.hashCode()
}