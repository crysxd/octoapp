package de.crysxd.baseui.compose.controls

import androidx.lifecycle.ViewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first

abstract class GenericHistoryItemsControlsViewModel<T>(
    private val instanceId: String,
    private val printerEngineProvider: PrinterEngineProvider,
    private val octoPreferences: OctoPreferences,
    printerConfigurationRepository: PrinterConfigurationRepository,
) : ViewModel() {

    abstract val items: Flow<List<T>>

    abstract val OctoPreferences.isAlwaysAllowedToShow: Boolean
    abstract val PrinterConfigurationV3.isNeverAllowedToShow: Boolean

    val visible = combine(
        printerEngineProvider.passiveCurrentMessageFlow(tag = "execute-gcode-controls", instanceId = instanceId),
        octoPreferences.updatedFlow2,
        printerConfigurationRepository.instanceInformationFlow(instanceId = instanceId)
    ) { current, _, instance ->
        // Widget is visible if we are not printing (printing, pausing, paused, cancelling) or we are paused or we are allowed to always show
        val printing = current.state.flags.isPrinting()
        val paused = current.state.flags.paused
        val alwaysShown = octoPreferences.isAlwaysAllowedToShow
        val neverShown = instance?.isNeverAllowedToShow == true
        !neverShown && (!printing || paused || alwaysShown)
    }.distinctUntilChanged()


    suspend fun needsConfirmation() = printerEngineProvider.passiveCurrentMessageFlow(tag = "execute-gcode-controls-check", instanceId = instanceId)
        .first().state.flags.isPrinting()

    protected fun postMessage(message: OctoActivity.Message) = when (message) {
        is OctoActivity.Message.DialogMessage -> OctoActivity.instance?.showDialog(message)
        is OctoActivity.Message.SnackbarMessage -> OctoActivity.instance?.showSnackbar(message)
    }
}