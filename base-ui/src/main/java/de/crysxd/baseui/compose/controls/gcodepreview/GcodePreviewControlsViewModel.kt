package de.crysxd.baseui.compose.controls.gcodepreview

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.compose.framework.helpers.staticStateOf
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOcto
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.gcode.GcodeRenderContextFactory
import de.crysxd.octoapp.base.models.GcodeMove
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.viewmodels.ext.printBed
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn

@OptIn(ExperimentalCoroutinesApi::class)
class GcodePreviewControlsViewModel(
    private val instanceId: String,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
    private val octoPreferences: OctoPreferences,
    private val gcodeFileRepository: GcodeFileRepository,
) : ViewModel() {

    private val tag = "GcodePreviewControlsViewModel"

    init {
        Napier.i(tag = tag, message = "New instance: $instanceId")
    }

    //region Inputs
    private val retryFlow = MutableStateFlow(0L)
    private val triggerFlow = MutableStateFlow<Trigger>(Trigger.Live())
    private val largeDownloadsAllowedUntilFlow = MutableStateFlow(0L)

    //endregion
    //region Data sources
    // We already filter the current message here with minimal impact. If the trigger won't create a different outcome with the new message, no need to refresh.
    private val currentMessageFlow = printerEngineProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "gcode-preview-1")
        .combine(triggerFlow) { x, y -> x to y }
        .distinctUntilChangedBy { (current, trigger) -> trigger.createResultId(current) }
        .map { (current, _) -> current }
    private val preferencesFlow = octoPreferences.updatedFlow.map { octoPreferences.gcodePreviewSettings }
    private val printerProfileFlow = printerConfigurationRepository.instanceInformationFlow(instanceId = instanceId).map { it?.activeProfile ?: PrinterProfile() }
    private val printBedFlow = printerConfigurationRepository.instanceInformationFlow(instanceId = instanceId).map { it.printBed }.distinctUntilChanged()
    private val featureEnabledFlow = BillingManager.isFeatureEnabledFlow(FEATURE_GCODE_PREVIEW)
        .distinctUntilChanged()
        .onEach { Napier.i(tag = tag, message = "[GCD] Enabled: $it") }

    //endregion
    //region State computation
    private val activeFileFlow = printerEngineProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "gcode-preview-2")
        .map { it.job?.file }
        .distinctUntilChangedBy { it?.path }
        .onEach { Napier.i(tag = tag, message = "[GCD] Active file: ${it?.path} (origin=${it?.origin})") }
        .combine(triggerFlow) { active, trigger -> (trigger as? Trigger.Manual)?.file ?: active }
        .mapNotNull { it }
        .distinctUntilChangedBy { it.path }
        .onEach { Napier.i(tag = tag, message = "[GCD] Selected file: ${it.path} (origin=${it.origin})") }

    private val gcodeFlow = combine(activeFileFlow, featureEnabledFlow, largeDownloadsAllowedUntilFlow) { file, enabled, largeDownloadsAllowedUntil ->
        Napier.i(tag = tag, message = "[GCD] Trigger load: file=${file.path} enabled=$enabled largeDownloadsAllowedUntil=$largeDownloadsAllowedUntil")
        flow {
            when {
                !enabled -> emit(GcodeFileDataSource.LoadState.Failed(exception = FeatureDisabledException()))
                file.origin is FileOrigin.Other -> emit(GcodeFileDataSource.LoadState.Failed(exception = PrintingFromSdCardException(file)))
                else -> {
                    emit(GcodeFileDataSource.LoadState.Loading(0f))
                    emitAll(gcodeFileRepository.loadFile(file, largeDownloadsAllowedUntil > System.currentTimeMillis()))
                }
            }
        }
    }.flatMapLatest { it }.onEach {
        if (it is GcodeFileDataSource.LoadState.Ready) {
            Napier.i(
                tag = tag,
                message = "[GCD] Loaded Gcode: layerCount=${it.gcode.layers.size} maxX=${it.gcode.maxX} minX=${it.gcode.minX} maxY=${it.gcode.maxY} minY=${it.gcode.minY}"
            )
        }
    }

    private val renderContextStateFlow: Flow<ViewState> =
        combine(gcodeFlow, preferencesFlow, currentMessageFlow, triggerFlow, activeFileFlow) { gcode, preferences, currentMessage, trigger, file ->
            when (gcode) {
                is GcodeFileDataSource.LoadState.Loading -> ViewState.Loading(gcode.progress)
                is GcodeFileDataSource.LoadState.FailedLargeFileDownloadRequired -> ViewState.LargeFileDownloadRequired(gcode.downloadSize)
                is GcodeFileDataSource.LoadState.Failed -> gcode.exception.toViewState()
                is GcodeFileDataSource.LoadState.Ready -> {
                    val (factoryInstance, fromUser) = trigger.toRenderContextFactory(currentMessage)
                    Napier.v(
                        tag = tag,
                        message = "[GCD] Trigger: $trigger"
                    )
                    ViewState.DataReady(
                        // This state is below replaced with a mutableStateOf to reduce refreshes in UI
                        state = staticStateOf(
                            EnrichedRenderContext(
                                renderContext = factoryInstance.extractMoves(
                                    gcode = gcode.gcode,
                                    includePreviousLayer = preferences.showPreviousLayer,
                                    includeRemainingCurrentLayer = preferences.showCurrentLayer,
                                ),
                                isTrackingPrintProgress = !fromUser,
                                exceedsPrintArea = null,
                                unsupportedGcode = null,
                            )
                        ),
                        printBed = GcodeNativeCanvas.Image.PrintBedGeneric,
                        file = file,
                        settings = preferences,
                    )
                }
            }
        }

    private val gcodeViewStateFlowFactory = {
        val renderContextState = mutableStateOf(EnrichedRenderContext())
        var previousState: ViewState? = null

        combine(renderContextStateFlow, printBedFlow, printerProfileFlow) { state, printBed, printerProfile ->
            when (state) {
                is ViewState.DataReady -> {
                    renderContextState.value = state.state.value.copy(
                        exceedsPrintArea = exceedsPrintArea(printerProfile, state.state.value.renderContext),
                        unsupportedGcode = containsUnsupportedGcode(state.state.value.renderContext),
                    )

                    val ps = previousState
                    if (ps !is ViewState.DataReady || ps.printerProfile == null) {
                        previousState = state
                        state.copy(
                            printBed = printBed,
                            printerProfile = printerProfile,
                            state = renderContextState,
                        )
                    } else {
                        null
                    }
                }

                else -> state
            }
        }.retry(3) {
            Napier.e(tag = tag, message = "Failed", throwable = it)
            it !is FeatureDisabledException && it !is PrintingFromSdCardException
        }.filterNotNull()
    }

    val state: Flow<ViewState> = retryFlow.flatMapLatest {
        combine(featureEnabledFlow, printBedFlow) { enabled, printBed ->
            when {
                !enabled -> flowOf(ViewState.FeatureDisabled(printBed = printBed))
                else -> gcodeViewStateFlowFactory()
            }
        }.flatMapLatest {
            it
        }.catch {
            Napier.e(tag = tag, message = "Failed", throwable = it)
            emit(it.toViewState())
        }.flowOn(
            Dispatchers.Default
        ).onStart {
            Napier.i(tag = tag, message = "[GCD] Starting shared Gcode flow")
        }.onCompletion {
            Napier.i(tag = tag, message = "[GCD] Stopping shared Gcode flow")
        }
    }.shareIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribedOcto,
        replay = 1
    ).onStart {
        Napier.i(tag = tag, message = "[GCD] Starting Gcode flow")
    }.onCompletion {
        Napier.i(tag = tag, message = "[GCD] Stopping Gcode flow")
    }

    //endregion
    //region Unsupported Gcode
    private fun containsUnsupportedGcode(context: GcodeRenderContext?) = listOfNotNull(
        context?.completedLayerPaths,
        context?.previousLayerPaths,
        context?.remainingLayerPaths
    ).any {
        it.any { path -> path.type == GcodeMove.Type.Unsupported && path.moveCount > 0 }
    }

    //endregion
    //region Exceeds print area
    private fun exceedsPrintArea(
        printerProfile: PrinterProfile?,
        context: GcodeRenderContext?,
    ): Boolean {
        context ?: return false
        printerProfile ?: return false
        val bounds = context.gcodeBounds
        bounds ?: return false
        val w = printerProfile.volume.width
        val h = printerProfile.volume.depth

        // Give 5% slack to make sure we don't count homing positions as out of bounds
        val graceDistance = minOf(w, h) * 0.05f

        val minX = printerProfile.volume.boundingBox.xMin - graceDistance
        val minY = printerProfile.volume.boundingBox.yMin - graceDistance
        val maxX = printerProfile.volume.boundingBox.xMax + graceDistance
        val maxY = printerProfile.volume.boundingBox.yMax + graceDistance

        return bounds.left < minX || bounds.top > maxY || bounds.right > maxX || bounds.bottom < minY
    }

    //endregion

    fun useLive() {
        triggerFlow.value = Trigger.Live()
    }

    fun useManual(layerIndex: Int, layerProgress: Float, file: FileReference.File? = null) {
        triggerFlow.value = Trigger.Manual(layerIndex = layerIndex, layerProgress = layerProgress, file = file)
    }

    fun allowLargeDownloads() {
        largeDownloadsAllowedUntilFlow.value = System.currentTimeMillis() + 10_000
    }

    fun retry() {
        retryFlow.value = System.currentTimeMillis()
    }

    private suspend fun Throwable.toViewState() = when (this) {
        is FeatureDisabledException -> ViewState.FeatureDisabled(printBed = printBedFlow.first())
        is PrintingFromSdCardException -> ViewState.PrintingFromSdCard(file)
        else -> ViewState.Error(this)
    }

    //region Internal classes
    private class FeatureDisabledException : SuppressedIllegalStateException("Feature disabled")
    private class PrintingFromSdCardException(val file: FileReference) : SuppressedIllegalStateException("Printing from SD card")

    private sealed class Trigger {

        abstract fun toRenderContextFactory(currentMessage: Message.Current): Pair<GcodeRenderContextFactory, Boolean>

        abstract fun createResultId(currentMessage: Message.Current): Int

        data class Live(private val seed: Double = Math.random()) : Trigger() {
            override fun toRenderContextFactory(currentMessage: Message.Current) =
                GcodeRenderContextFactory.ForFileLocation(currentMessage.progress?.filepos?.toInt() ?: Int.MAX_VALUE) to false

            override fun createResultId(currentMessage: Message.Current) =
                currentMessage.progress?.filepos?.toInt() ?: -1
        }

        data class Manual(private val layerIndex: Int, private val layerProgress: Float, internal val file: FileReference.File?) : Trigger() {
            override fun toRenderContextFactory(currentMessage: Message.Current) =
                GcodeRenderContextFactory.ForLayerProgress(layerIndex = layerIndex, progress = layerProgress) to true

            override fun createResultId(currentMessage: Message.Current) =
                "$layerIndex:$layerProgress".hashCode()
        }
    }
    //endregion

    sealed class ViewState {
        data class Loading(val progress: Float = 0f) : ViewState()
        data class FeatureDisabled(val printBed: GcodeNativeCanvas.Image) : ViewState()
        data class LargeFileDownloadRequired(val downloadSize: Long) : ViewState()
        data class Error(val exception: Throwable) : ViewState()
        data class PrintingFromSdCard(val file: FileReference) : ViewState()
        data class DataReady(
            val printBed: GcodeNativeCanvas.Image,
            val file: FileReference.File? = null,
            val printerProfile: PrinterProfile? = null,
            val settings: GcodePreviewSettings,
            val state: State<EnrichedRenderContext>,
        ) : ViewState()
    }

    data class EnrichedRenderContext(
        val renderContext: GcodeRenderContext? = null,
        val exceedsPrintArea: Boolean? = null,
        val unsupportedGcode: Boolean? = null,
        val isTrackingPrintProgress: Boolean = false,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = GcodePreviewControlsViewModel(
            instanceId = instanceId,
            printerEngineProvider = BaseInjector.get().octoPrintProvider(),
            printerConfigurationRepository = BaseInjector.get().octorPrintRepository(),
            gcodeFileRepository = BaseInjector.get().gcodeFileRepository(),
            octoPreferences = BaseInjector.get().octoPreferences()
        ) as T
    }
}
