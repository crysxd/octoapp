package de.crysxd.baseui.compose.screens.help

import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.viewmodels.HelpTutorialViewModelCore

class HelpTutorialViewModel : ViewModelWithCore() {

    override val core by lazy { HelpTutorialViewModelCore() }
    val state get() = core.state

    suspend fun reload() = core.reloadAndWait()


}