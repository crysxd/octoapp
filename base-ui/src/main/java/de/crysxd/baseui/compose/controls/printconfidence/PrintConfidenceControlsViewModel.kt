package de.crysxd.baseui.compose.controls.printconfidence

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.octoapp.viewmodels.PrintConfidenceViewModelCore
import kotlinx.coroutines.flow.map

class PrintConfidenceControlsViewModel(
    private val instanceId: String
) : ViewModel() {

    private val core = PrintConfidenceViewModelCore(instanceId)

    val state = core.state.map { it.confidence }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = PrintConfidenceControlsViewModel(
            instanceId = instanceId,
        ) as T
    }
}