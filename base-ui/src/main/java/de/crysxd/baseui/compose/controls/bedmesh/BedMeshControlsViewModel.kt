package de.crysxd.baseui.compose.controls.bedmesh

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.octoapp.viewmodels.BedMeshControlsViewModelCore

class BedMeshControlsViewModel(
    private val instanceId: String,
) : ViewModel() {

    private val core = BedMeshControlsViewModelCore(instanceId)

    val state = core.state

    suspend fun refresh() = core.refresh()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = BedMeshControlsViewModel(
            instanceId = instanceId,
        ) as T
    }
}
