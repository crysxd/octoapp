package de.crysxd.baseui.compose.controls.extrude

import android.content.Context
import android.text.InputType
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.common.enter_value.EnterValueFragmentArgs
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControlsViewModel
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.ExtrusionHistoryItem
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.BaseChangeTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.ExtrudeFilamentUseCase
import de.crysxd.octoapp.base.usecase.GetExtrusionShortcutsUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.execute
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ExtrudeControlsViewModel(
    private val instanceId: String,
    private val extrudeFilamentUseCase: ExtrudeFilamentUseCase,
    private val setTargetTemperatureUseCase: SetTargetTemperaturesUseCase,
    private val extrusionHistoryRepository: ExtrusionHistoryRepository,
    getExtrusionShortcutsUseCase: GetExtrusionShortcutsUseCase,
    printerEngineProvider: PrinterEngineProvider,
    octoPreferences: OctoPreferences,
    printerConfigurationRepository: PrinterConfigurationRepository,
) : GenericHistoryItemsControlsViewModel<ExtrusionHistoryItem>(
    printerEngineProvider = printerEngineProvider,
    octoPreferences = octoPreferences,
    instanceId = instanceId,
    printerConfigurationRepository = printerConfigurationRepository,
) {

    private val tag = "ExtrudeControlsViewModel"
    @OptIn(ExperimentalCoroutinesApi::class)
    override val items = flow { emit(getExtrusionShortcutsUseCase.execute()) }
        .flatMapLatest { it }
        .distinctUntilChangedBy { it.map { it.isFavorite to it.distanceMm } }

    override val OctoPreferences.isAlwaysAllowedToShow get() = false

    override val PrinterConfigurationV3.isNeverAllowedToShow get() = hasPlugin(OctoPlugins.BetterGrblSupport)

    fun toggleFavourite(distanceMm: Int) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        extrusionHistoryRepository.toggleFavourite(distanceMm)
    }

    fun extrudeOther(context: Context, navContoller: NavController) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        val result = NavigationResultMediator.registerResultCallback<String?>()

        navContoller.navigate(
            R.id.action_enter_value,
            EnterValueFragmentArgs(
                title = context.getString(R.string.extrude_retract),
                hint = context.getString(R.string.distance_in_mm_negative_for_retract),
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                resultId = result.first
            ).toBundle()
        )

        withContext(Dispatchers.Default) {
            result.second.asFlow().first()
        }?.let {
            extrude(it.toInt())
        }
    }

    fun extrude(distanceMm: Int) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        if (needsConfirmation()) {
            postMessage(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.widget_extrude___confirmation_message, distanceMm) },
                    positiveAction = { doExtrude(distanceMm = distanceMm) },
                    negativeButton = { getString(R.string.cancel) },
                    positiveButton = { getString(R.string.widget_extrude___confirmation_action) }
                )
            )
        } else {
            doExtrude(distanceMm = distanceMm)
        }
    }

    private fun doExtrude(distanceMm: Int) = AppScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        try {
            postMessage(OctoActivity.Message.SnackbarMessage { it.getString(R.string.extruding_x_mm, distanceMm) })
            extrudeFilamentUseCase.execute(ExtrudeFilamentUseCase.Param(extrudeLengthMm = distanceMm, instanceId = instanceId))
        } catch (e: ExtrudeFilamentUseCase.ColdExtrusionException) {
            val minTemp = e.minTemp

            postMessage(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.error_cold_extrusion) },
                    neutralButton = { if (minTemp != null) getString(R.string.heat_hotend) else null },
                    neutralAction = {
                        if (minTemp == null) return@DialogMessage

                        AppScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
                            Napier.i(tag = tag, message = "Heating to ${e.minTemp} before extrusion")
                            setTargetTemperatureUseCase.execute(
                                BaseChangeTemperaturesUseCase.Params(
                                    BaseChangeTemperaturesUseCase.Temperature(
                                        component = "tool0",
                                        temperature = minTemp + 5f,
                                    )
                                )
                            )
                            postMessage(OctoActivity.Message.SnackbarMessage { it.getString(R.string.heating_hotend, e.minTemp) })
                        }
                    }
                )
            )
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = with(BaseInjector.get()) {
            ExtrudeControlsViewModel(
                instanceId = instanceId,
                octoPreferences = octoPreferences(),
                getExtrusionShortcutsUseCase = getExtrusionShortcutsUseCase(),
                printerEngineProvider = octoPrintProvider(),
                extrudeFilamentUseCase = extrudeFilamentUseCase(),
                setTargetTemperatureUseCase = setTargetTemperatureUseCase(),
                extrusionHistoryRepository = extrusionHistoryRepository(),
                printerConfigurationRepository = octorPrintRepository()
            ) as T
        }
    }
}