package de.crysxd.baseui.compose.controls.temperature

import android.content.Context
import android.text.InputType
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.common.enter_value.EnterValueFragmentArgs
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.BaseChangeTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTemperatureOffsetUseCase
import de.crysxd.octoapp.base.utils.AnimationTestUtils
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.system.SystemInfo.Capability.TemperatureOffset
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TemperatureControlsViewModel(
    private val instanceId: String,
    temperatureDataRepository: TemperatureDataRepository,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val setTemperatureOffsetUseCase: SetTemperatureOffsetUseCase,
    private val setTargetTemperaturesUseCase: SetTargetTemperaturesUseCase,
) : ViewModel() {

    private val tag = "TemperatureControlsViewModel"
    val temperature = temperatureDataRepository.flow(instanceId = instanceId).let {
        // Slow down update rate for test
        if (AnimationTestUtils.animationsDisabled) {
            it.rateLimit(2000)
        } else {
            it.rateLimit(1000)
        }
    }.map { components ->
        components.filter { !it.isHidden }
    }.retry {
        Napier.e(tag = tag, message = "Failed", throwable = it)
        delay(1000)
        true
    }

    fun getInitialComponentCount() = printerConfigurationRepository.getActiveInstanceSnapshot()?.activeProfile?.let { profile ->
        var counter = profile.extruders.size
        if (profile.heatedBed) counter++
        if (profile.heatedChamber) counter++
        Napier.i(tag = tag, message = "Using $counter temperature controls for initial setup ($profile)")
        counter
    } ?: 2

    private suspend fun setTemperature(temp: Int, component: String) {
        setTargetTemperaturesUseCase.execute(
            BaseChangeTemperaturesUseCase.Params(
                instanceId = instanceId,
                temp = BaseChangeTemperaturesUseCase.Temperature(temperature = temp.toFloat(), component = component)
            )
        )
    }

    private suspend fun setOffset(offset: Int, component: String) {
        setTemperatureOffsetUseCase.execute(
            BaseChangeTemperaturesUseCase.Params(
                instanceId = instanceId,
                temp = BaseChangeTemperaturesUseCase.Temperature(temperature = offset.toFloat(), component = component)
            )
        )
    }

    fun changeTemperature(
        context: Context,
        navController: NavController,
        component: TemperatureDataRepository.TemperatureSnapshot
    ) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        val targetResult = NavigationResultMediator.registerResultCallback<String?>()
        val offsetResult = NavigationResultMediator.registerResultCallback<String?>()
        val target = component.current.target?.toInt()
        val offset = component.offset?.toInt()
        val hasOffsets = printerConfigurationRepository.get(instanceId)?.systemInfo?.capabilities?.contains(TemperatureOffset) == true

        navController.navigate(
            R.id.action_enter_value,
            EnterValueFragmentArgs(
                title = context.getString(R.string.x_temperature, component.componentLabel),
                action = context.getString(R.string.set_temperature),
                resultId = targetResult.first,
                hint = context.getString(R.string.target_temperature),
                value = null,
                inputType = InputType.TYPE_CLASS_NUMBER,
                resultId2 = offsetResult.first.takeIf { hasOffsets } ?: -1,
                hint2 = context.getString(R.string.temperature_widget___change_offset),
                value2 = offset?.toString(),
                inputType2 = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                selectAll = true
            ).toBundle()
        )

        withContext(Dispatchers.Default) {
            targetResult.second.asFlow().first()
        }?.let { temp ->
            val tempInt = temp.toIntOrNull()
            if (tempInt != null && tempInt != target) {
                setTemperature(tempInt, component.component)
            } else {
                Napier.i(tag = tag, message = "No change, dropping target change to $temp")
            }
        }

        withContext(Dispatchers.Default) {
            offsetResult.second.asFlow().first()
        }?.let { temp ->
            val tempInt = temp.toIntOrNull()
            if (tempInt != null && tempInt != target) {
                setOffset(tempInt, component.component)
            } else {
                Napier.i(tag = tag, message = "No change, dropping offset change to $temp")
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = TemperatureControlsViewModel(
            instanceId = instanceId,
            printerConfigurationRepository = BaseInjector.get().octorPrintRepository(),
            setTargetTemperaturesUseCase = BaseInjector.get().setTargetTemperatureUseCase(),
            setTemperatureOffsetUseCase = BaseInjector.get().setTemperatureOffsetUseCase(),
            temperatureDataRepository = BaseInjector.get().temperatureDataRepository(),
        ) as T
    }
}
