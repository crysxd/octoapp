package de.crysxd.baseui.compose.controls.cancelobject

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel
import de.crysxd.baseui.compose.controls.gcodepreview.GcodePreviewControlsViewModel
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.helpers.GcodeRenderCanvas
import de.crysxd.baseui.compose.framework.layout.ComposeBottomSheetFragment
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.gcode.GcodeRenderer
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.viewmodels.helper.cancelobject.CancelObjectGcodeRendererPlugin
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CancelObjectBottomSheet : ComposeBottomSheetFragment() {

    override val instanceId
        get() = navArgs<CancelObjectBottomSheetArgs>().value.instanceId

    @Composable
    override fun Content() {
        //region State
        val context = LocalContext.current
        val padding = OctoAppTheme.dimens.margin2
        val gcodeState by rememberGcodePreviewControlsViewModel().state.collectAsState(initial = GcodePreviewControlsViewModel.ViewState.Loading())
        val cancelObjectViewModel = rememberCancelObjectViewModel()
        val renderState by rememberRenderViewModel().state.collectAsState(initial = CancelObjectRenderViewModel.State.Loading)
        val cancelObjectState by cancelObjectViewModel.state.collectAsState(initial = GenericLoadingControlsViewModel.ViewState.Loading)
        var selectedObject by remember { mutableStateOf<CancelObjectViewModel.PrintObject?>(null) }
        val idleTitle = stringResource(id = R.string.widget_cancel_object)
        val title by remember { derivedStateOf { selectedObject?.label ?: idleTitle } }

        LaunchedEffect(cancelObjectState::class) {
            Napier.i(tag = "CancelObjectDebug", message = "State now ${cancelObjectState::class.simpleName}")

            val cos = cancelObjectState
            if (cos is GenericLoadingControlsViewModel.ViewState.Error) {
                requireOctoActivity().showDialog(cos.exception)
                dismissAllowingStateLoss()
            }

            if (cos is GenericLoadingControlsViewModel.ViewState.Data) {
                Napier.i(tag = "CancelObjectDebug", message = "Loaded ${cos.data.all.size} objects")
                cos.data.all.forEach {
                    Napier.i(tag = "CancelObjectDebug", message = "${it.objectId}: ${it.center}")
                }
            }
        }
        //endregion

        Column {
            //region Title
            Text(
                text = title,
                style = OctoAppTheme.typography.title,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = OctoAppTheme.dimens.margin1),
            )
            //endregion
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .padding(OctoAppTheme.dimens.margin2)
                    .clip(MaterialTheme.shapes.large)
                    .background(OctoAppTheme.colors.inputBackground)
            ) {
                //region Loading
                CircularProgressIndicator(
                    modifier = Modifier.graphicsLayer { this.alpha = (1f - alpha) },
                    color = OctoAppTheme.colors.accent
                )
                //endregion
                //region Render
                val scope = rememberCoroutineScope()
                val dotRadius = 8f
                GcodeRenderCanvas(
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(ratio = 1f),
                    contentPadding = PaddingValues(padding),
                    acceptTouchInput = true,
                    asyncRender = false,
                    onTap = { x, y ->
                        //region Handle dot tap
                        Napier.i(tag = "CancelObjectDebug", message = "Click at x=$x, y=$y")
                        // Search clicked offset async
                        scope.launch(Dispatchers.Default) {
                            val objects = (cancelObjectState as? GenericLoadingControlsViewModel.ViewState.Data)?.data?.all ?: emptyList()
                            selectedObject = objects.filter { !it.cancelled }.firstOrNull {
                                val (cx, cy) = it.center ?: return@firstOrNull false
                                val box = Rect(center = Offset(cx, cy), dotRadius)
                                box.contains(Offset(x, y))
                            }
                        }
                        //endregion
                    },
                    nativeOverlay = {
                        val objects = (cancelObjectState as? GenericLoadingControlsViewModel.ViewState.Data)?.data?.all?.map { it.core } ?: emptyList()
                            val plugin = CancelObjectGcodeRendererPlugin(objects)
                        plugin.selectedId = selectedObject?.objectId
                        plugin.render(this)
                    },
                    renderParams = {
                        //region Create params
                        val readyState = (renderState as? CancelObjectRenderViewModel.State.Ready) ?: return@GcodeRenderCanvas null
                        val pb = readyState.printBed
                        val pp = readyState.printerProfile
                        val s = readyState.settings

                        GcodeRenderer.RenderParams(
                            renderContext = (gcodeState as? GcodePreviewControlsViewModel.ViewState.DataReady)?.state?.value?.renderContext
                                ?: GcodeRenderContext.Empty,
                            originInCenter = pp.volume.origin == PrinterProfile.Origin.Center,
                            printBedHeightMm = pp.volume.depth,
                            printBedWidthMm = pp.volume.width,
                            extrusionWidthMm = pp.estimatedNozzleDiameter,
                            quality = s.quality,
                            printBed = pb,
                            minPrintHeadDiameterPx = context.resources.getDimension(de.crysxd.octoapp.base.R.dimen.gcode_render_view_print_head_size),
                        )
                        //endregion
                    }
                )
            }
            //endregion
            //region Options
            AnimatedVisibility(
                visible = selectedObject != null,
                enter = fadeIn() + expandVertically(expandFrom = Alignment.Top),
                exit = fadeOut() + shrinkVertically(shrinkTowards = Alignment.Top),
            ) {
                OctoButton(
                    text = stringResource(id = R.string.widget_cancel_object),
                    modifier = Modifier
                        .padding(horizontal = OctoAppTheme.dimens.margin2)
                        .padding(top = OctoAppTheme.dimens.margin1, bottom = OctoAppTheme.dimens.margin2),
                    onClick = {
                        selectedObject?.let {
                            cancelAfterConfirmation(it, cancelObjectViewModel) {
                                selectedObject = null
                            }
                        }
                    }
                )
            }
            //endregion
        }
    }

    private fun cancelAfterConfirmation(
        obj: CancelObjectViewModel.PrintObject,
        viewModel: CancelObjectViewModel,
        onConfirmed: () -> Unit,
    ) = requireOctoActivity().showDialog(
        message = OctoActivity.Message.DialogMessage(
            text = { getString(R.string.cancel_object___confirmation_message, obj.label) },
            positiveButton = { getString(R.string.cancel_object___confirmation_positive) },
            negativeButton = { getString(R.string.cancel_object___confirmation_negative) },
            positiveAction = {
                onConfirmed()
                viewModel.cancelObject(obj.objectId)
            }
        )
    )

    @Composable
    private fun rememberCancelObjectViewModel(): CancelObjectViewModel {
        val vmFactory = CancelObjectViewModel.Factory(LocalOctoPrint.current.id)
        return viewModel(
            modelClass = CancelObjectViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory,
            viewModelStoreOwner = requireNotNull(OctoAppTheme.octoActivity)
        )
    }

    @Composable
    private fun rememberRenderViewModel(): CancelObjectRenderViewModel {
        val vmFactory = CancelObjectRenderViewModel.Factory(LocalOctoPrint.current.id)
        return viewModel(
            modelClass = CancelObjectRenderViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory,
        )
    }

    @Composable
    private fun rememberGcodePreviewControlsViewModel(): GcodePreviewControlsViewModel {
        val vmFactory = GcodePreviewControlsViewModel.Factory(LocalOctoPrint.current.id)
        return viewModel(
            modelClass = GcodePreviewControlsViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory,
        ).also {
            it.useLive()
        }
    }
}
