package de.crysxd.baseui.compose.controls.progress

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.ProgressControlsViewModelCore

class ProgressControlsViewModel(
    private val instanceId: String,
) : ViewModelWithCore() {

    override val core by lazy { ProgressControlsViewModelCore(instanceId) }
    private val preferences = SharedBaseInjector.get().preferences

    val state get() = core.state
    val stateCache
        get() = ProgressControlsViewModelCore.State(
            settings = preferences.progressWidgetSettings,
            fullscreenSettings = preferences.webcamFullscreenProgressWidgetSettings,
            activeFile = FlowState.Loading(),
            status = "",
            completion = 0f,
            dataItems = core.createDataItems(
                current = null,
                settings = preferences.progressWidgetSettings,
                gcode = null,
                displayMessage = null
            ),
            fullscreenDataItems = core.createDataItems(
                current = null,
                settings = preferences.webcamFullscreenProgressWidgetSettings,
                gcode = null,
                displayMessage = null
            ),
            printing = true,
        )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ProgressControlsViewModel(
            instanceId = instanceId,
        ) as T
    }
}