package de.crysxd.baseui.compose.framework.locals

import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection

internal val LocalNestedScrollConnection = compositionLocalOf<NestedScrollConnection> { object : NestedScrollConnection {} }
