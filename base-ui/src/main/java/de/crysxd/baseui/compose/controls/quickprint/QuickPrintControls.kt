package de.crysxd.baseui.compose.controls.quickprint

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.GenericLoadingControls
import de.crysxd.baseui.compose.controls.GenericLoadingControlsState
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel.ViewState
import de.crysxd.baseui.compose.framework.components.OctoDataButton
import de.crysxd.baseui.compose.framework.components.OctoPicassoImage
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.screens.filelist.FileListFragmentArgs
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.viewmodels.QuickPrintControlsViewModelCore
import de.crysxd.octoapp.viewmodels.helper.startprint.StartPrintHelper
import de.crysxd.octoapp.viewmodels.helper.startprint.StartPrintHelper.Result.Started
import kotlinx.datetime.Instant
import java.io.IOException

@Composable
fun QuickPrintControls(
    editState: EditState,
    state: QuickPrintControlsState,
) = GenericLoadingControls(
    editState = editState,
    state = state,
    title = stringResource(id = R.string.widget_quick_print),
) { files ->
    Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)) {
        files.forEach {
            Entry(
                fileLink = it,
                onSelected = { state.onFileSelected(it.fileObject) },
                onStartPrint = { materialConfirmed, timelapseConfirmed -> state.onStartPrint(it.fileObject, materialConfirmed, timelapseConfirmed) }
            )
        }
    }
}

//region Internals
@Composable
private fun Entry(
    fileLink: QuickPrintControlsViewModelCore.FileLink,
    onSelected: () -> Unit,
    onStartPrint: suspend (Boolean, Boolean) -> StartPrintHelper.Result
) {
    val startPrintState = rememberStartPrintState(
        startPrint = onStartPrint,
        file = fileLink.fileObject
    )

    OctoDataButton(
        icon = painterResource(id = R.drawable.ic_round_print_24),
        contentDescription = stringResource(id = R.string.cd_print),
        onContentClicked = onSelected,
        onClick = { startPrintState.startPrint() },
        content = {
            //region Main button
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)
            ) {
                OctoPicassoImage(
                    path = fileLink.fileObject.smallThumbnail?.path,
                    fallback = R.drawable.ic_round_insert_drive_file_24,
                    modifier = Modifier
                        .size(32.dp)
                        .clip(CircleShape)
                )
                Column(modifier = Modifier.weight(1f)) {
                    val lastPrint = fileLink.fileObject.prints?.last
                    val lastPrintDate = lastPrint?.date?.format()

                    Text(
                        text = fileLink.fileObject.display,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis,
                        style = OctoAppTheme.typography.base,
                        color = OctoAppTheme.colors.darkText
                    )
                    Text(
                        text = when {
                            fileLink.selected -> stringResource(id = R.string.file_manager___file_list___currently_selected)
                            lastPrintDate != null -> when (lastPrint.success) {
                                true -> stringResource(id = R.string.file_manager___file_details___last_print_at_x_success, lastPrintDate)
                                false -> stringResource(id = R.string.file_manager___file_details___last_print_at_x_failure, lastPrintDate)
                            }

                            else -> stringResource(id = R.string.file_manager___file_list___just_uploaded)
                        },
                        overflow = TextOverflow.Ellipsis,
                        style = OctoAppTheme.typography.labelSmall,
                        color = OctoAppTheme.colors.normalText
                    )
                }
                Icon(
                    painter = painterResource(id = R.drawable.ic_round_chevron_right_24),
                    contentDescription = null,
                    tint = OctoAppTheme.colors.accent,
                )
            }
            //endregion
        }
    )
}


//endregion
//region State
@Composable
fun rememberQuickPrintState(): QuickPrintControlsState {
    val vmFactory = QuickPrintViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = QuickPrintViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = OctoAppTheme.octoActivity as ViewModelStoreOwner
    )

    val controller = OctoAppTheme.navController
    val state = vm.state.collectAsStateWhileActive(key = "quickprint", initial = vm.stateCache)
    return object : QuickPrintControlsState {
        override val wasError = false
        override val current get() = state.value
        override fun onRetry() = vm.retry()

        override suspend fun onStartPrint(fileObject: FileObject.File, materialConfirmed: Boolean, timelapseConfirmed: Boolean) = vm.startPrint(
            path = fileObject.path,
            materialConfirmed = materialConfirmed,
            timelapseConfirmed = timelapseConfirmed
        )

        override fun onFileSelected(fileObject: FileObject.File) = controller().navigate(
            R.id.action_show_file_details,
            FileListFragmentArgs(fileObject).toBundle()
        )
    }
}

interface QuickPrintControlsState : GenericLoadingControlsState<List<QuickPrintControlsViewModelCore.FileLink>> {
    fun onFileSelected(fileObject: FileObject.File)
    suspend fun onStartPrint(fileObject: FileObject.File, materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result
}

//endregion
//region Previews
@Preview
@Composable
private fun PreviewLoadingInitial() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override val current = ViewState.Loading
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override suspend fun onStartPrint(fileObject: FileObject.File, materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result = Started
        override fun onRetry() = Unit
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}

@Preview
@Composable
private fun PreviewLoadingAfterError() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = true
        override val current = ViewState.Loading
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override suspend fun onStartPrint(fileObject: FileObject.File, materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result = Started
        override fun onRetry() = Unit
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}


@Preview
@Composable
private fun PreviewError() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override val current = ViewState.Error(IOException())
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override suspend fun onStartPrint(fileObject: FileObject.File, materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result = Started
        override fun onRetry() = Unit
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}

@Preview
@Composable
private fun PreviewEmpty() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override suspend fun onStartPrint(fileObject: FileObject.File, materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result = Started
        override fun onRetry() = Unit
        override val current = ViewState.Data(
            contentKey = "",
            data = emptyList<QuickPrintControlsViewModelCore.FileLink>()
        )
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}

@Preview
@Composable
private fun PreviewData() = OctoAppThemeForPreview {
    val state = object : QuickPrintControlsState {
        override val wasError = false
        override fun onFileSelected(fileObject: FileObject.File) = Unit
        override suspend fun onStartPrint(fileObject: FileObject.File, materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result = Started
        override fun onRetry() = Unit
        override val current = ViewState.Data(
            contentKey = "",
            data = listOf(
                QuickPrintControlsViewModelCore.FileLink(
                    selected = true,
                    fileObject = FileObject.File(
                        display = "File A",
                        name = "FileA",
                        origin = FileOrigin.Gcode,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        size = 0,
                        date = Instant.DISTANT_PAST,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(Instant.DISTANT_PAST, success = true))
                    )
                ),
                QuickPrintControlsViewModelCore.FileLink(
                    selected = false,
                    fileObject = FileObject.File(
                        display = "File B",
                        name = "FileB",
                        origin = FileOrigin.Gcode,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        size = 0,
                        date = Instant.DISTANT_PAST,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(Instant.DISTANT_PAST, success = true))
                    )
                ),
                QuickPrintControlsViewModelCore.FileLink(
                    selected = false,
                    fileObject = FileObject.File(
                        display = "File B",
                        name = "FileB",
                        origin = FileOrigin.Gcode,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        size = 0,
                        date = Instant.DISTANT_PAST,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(Instant.DISTANT_PAST, success = false))
                    )
                ),
                QuickPrintControlsViewModelCore.FileLink(
                    selected = false,
                    fileObject = FileObject.File(
                        display = "File B",
                        name = "FileB",
                        origin = FileOrigin.Gcode,
                        path = "/",
                        type = "machinecode",
                        typePath = listOf("machinecode"),
                        size = 0,
                        date = Instant.DISTANT_PAST,
                        hash = "",
                        gcodeAnalysis = null,
                        prints = FileObject.PrintHistory(failure = 1, success = 0, last = FileObject.PrintHistory.LastPrint(Instant.DISTANT_PAST, success = true))
                    )
                )
            ),
        )
    }

    QuickPrintControls(editState = EditState.ForPreview, state = state)
}
//endregion
