package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.core.net.toUri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoScrollableTabRow
import de.crysxd.baseui.compose.framework.components.OctoText
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScope
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.sharedcommon.utils.parseHtml
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.Manual
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.Ngrok
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.Obico
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.OctoEverywhere

@Composable
fun RemoteAccessScreen(
    controller: RemoteAccessController = rememberRemoteAccessController()
) = CollapsibleHeaderScreenScaffold(
    header = { Header() },
    tabs = { Tabs(controller) },
    content = { Services(controller) }
)

@Composable
private fun Header() = Column(
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2),
    horizontalAlignment = Alignment.CenterHorizontally,
) {
    Text(
        text = stringResource(id = R.string.configure_remote_access___title),
        style = OctoAppTheme.typography.title,
        textAlign = TextAlign.Center,
        color = OctoAppTheme.colors.darkText,
        modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2)
    )

    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.octo_vacation))
    LottieAnimation(
        composition = composition,
        modifier = Modifier
            .aspectRatio(16 / 9f)
            .padding(vertical = OctoAppTheme.dimens.margin2)
            .fillMaxWidth()
    )

    OctoText(
        text = stringResource(id = R.string.configure_remote_acces___description, UriLibrary.getFaqUri("user_remotely")).parseHtml(),
        style = OctoAppTheme.typography.base,
        textAlign = TextAlign.Center,
        color = OctoAppTheme.colors.normalText,
        onUrlOpened = { it.toUri().open(); true }
    )

}

@Composable
private fun Tabs(controller: RemoteAccessController) = OctoScrollableTabRow(
    selectedIndex = controller.selectedIndex,
    onSelection = { controller.selectedIndex = it },
    options = controller.services.map { it.title },
    modifier = Modifier.padding(vertical = OctoAppTheme.dimens.margin1)
)


private val RemoteAccessViewModelCore.Service.title
    @Composable get() = when (this) {
        OctoEverywhere -> stringResource(id = R.string.configure_remote_acces___octoeverywhere___title)
        Obico -> stringResource(id = R.string.configure_remote_acces___spaghetti_detective___title)
        Ngrok -> stringResource(id = R.string.configure_remote_acces___ngrok___title)
        Manual -> stringResource(id = R.string.configure_remote_acces___manual___title)
    }

@Composable
@OptIn(ExperimentalFoundationApi::class)
private fun CollapsibleHeaderScreenScope.Services(controller: RemoteAccessController) {
    if (controller.services.isEmpty()) return

    var initialChange by remember { mutableStateOf(true) }
    val state = rememberPagerState(initialPage = controller.selectedIndex) { controller.services.size }
    val keyboard = LocalSoftwareKeyboardController.current

    LaunchedEffect(Unit) {
        OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigScreenOpened)

        val service = controller.services[0]
        val serviceEvents = mapOf(
            OctoEverywhere to OctoAnalytics.Event.OctoEverywhereServiceAtPosition1,
            Obico to OctoAnalytics.Event.ObicoServiceAtPosition1,
        )

        if (service !in controller.installedServices && service !in controller.connectedServices) {
            serviceEvents[service]?.let(OctoAnalytics::logEvent)
        }
    }

    LaunchedEffect(controller.selectedIndex) {
        if (initialChange) {
            initialChange = false
        } else {
            collapse()
            keyboard?.hide()
        }

        state.animateScrollToPage(page = controller.selectedIndex)
    }

    LaunchedEffect(state.settledPage) {
        controller.selectedIndex = state.settledPage

        val service = controller.services[state.settledPage]
        val serviceEvents = mapOf(
            Obico to OctoAnalytics.Event.ObicoServicePresented,
            OctoEverywhere to OctoAnalytics.Event.OctoEverywhereServicePresented,
            Ngrok to OctoAnalytics.Event.NgrokServicePresented,
        )

        if (service !in controller.installedServices && service !in controller.connectedServices) {
            serviceEvents[service]?.let(OctoAnalytics::logEvent)
        }
    }

    HorizontalPager(
        state = state,
        contentPadding = PaddingValues(OctoAppTheme.dimens.margin2),
        pageSpacing = OctoAppTheme.dimens.margin12,
        modifier = Modifier.fillMaxSize(),
    ) { index ->
        when (controller.services[index]) {
            OctoEverywhere -> RemoteAccessOctoEverywhereView()
            Obico -> RemoteAccessObicoView()
            Ngrok -> RemoteAccessNgrokView()
            Manual -> RemoteAccessManualView()
        }
    }
}

//region State
@Composable
private fun rememberRemoteAccessController(): RemoteAccessController {
    val instanceId = LocalOctoPrint.current.id
    val factory = RemoteAccessViewModel.Factory(instanceId)
    val viewModel = viewModel<RemoteAccessViewModel>(
        factory = factory,
        key = factory.id,
    )
    var selectedService by remember { mutableStateOf<RemoteAccessViewModelCore.Service?>(null) }
    val state = viewModel.state.collectAsState(
        initial = RemoteAccessViewModelCore.State(
            services = emptyList(),
            loadingServices = emptyList(),
            installedServices = emptyList(),
            connectedServices = emptyList()
        )
    ).value

    return object : RemoteAccessController {
        override val services get() = state.services
        override val installedServices get() = state.installedServices
        override val connectedServices get() = state.connectedServices
        override var selectedIndex
            get() = services.indexOf(selectedService).coerceAtLeast(0)
            set(value) {
                selectedService = services.getOrNull(value)
            }
    }
}

interface RemoteAccessController {
    val services: List<RemoteAccessViewModelCore.Service>
    val installedServices: List<RemoteAccessViewModelCore.Service>
    val connectedServices: List<RemoteAccessViewModelCore.Service>
    var selectedIndex: Int
}

private class RemoteAccessViewModel(instanceId: String) : BaseViewModel() {
    val core = RemoteAccessViewModelCore(instanceId)
    val state = core.state

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = RemoteAccessViewModel(
            instanceId = instanceId,
        ) as T
    }
}

//endregion
