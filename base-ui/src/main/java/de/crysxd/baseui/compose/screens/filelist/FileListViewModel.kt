package de.crysxd.baseui.compose.screens.filelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.FileListViewModelCore
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.stateIn

class FileListViewModel(instanceId: String) : ViewModelWithCore() {

    override val core by lazy { FileListViewModelCore(instanceId) }

    val state = core.fileListState.distinctUntilChanged().stateIn(
        scope = viewModelScope,
        initialValue = FileListViewModelCore.State(canStartPrint = false, state = FlowState.Loading()),
        started = SharingStarted.WhileSubscribedOctoDelay
    )
    val informAboutThumbnails = core.informAboutThumbnails.stateIn(
        scope = viewModelScope,
        initialValue = false,
        started = SharingStarted.WhileSubscribedOctoDelay
    )

    fun loadFiles(path: String, skipCache: Boolean) = core.loadFiles(path, skipCache)
    fun dismissThumbnailInformation() = core.dismissThumbnailInformation()
    suspend fun startPrint(materialConfirmed: Boolean, timelapseConfirmed: Boolean) = core.startPrint(materialConfirmed, timelapseConfirmed)

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = FileListViewModel(
            instanceId = instanceId,
        ) as T
    }
}