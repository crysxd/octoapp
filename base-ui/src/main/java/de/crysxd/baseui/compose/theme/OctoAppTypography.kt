package de.crysxd.baseui.compose.theme

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.sp
import de.crysxd.baseui.R

@Suppress("Unused")
interface OctoAppTypography {

    val base: TextStyle
        @Composable get() = TextStyle(
            fontSize = 14.sp,
            letterSpacing = 0.sp,
            color = colorResource(R.color.normal_text),
            fontFamily = FontFamily(Font(R.font.roboto_regular))
        )

    val label: TextStyle
        @Composable get() = base.copy(
            fontSize = 14.sp,
            fontFamily = FontFamily(Font(R.font.roboto_light))
        )

    val labelSmall: TextStyle
        @Composable get() = label.copy(
            fontSize = 12.sp,
            fontFamily = FontFamily(Font(R.font.roboto_regular))
        )

    val focus: TextStyle
        @Composable get() = base.copy(
            fontFamily = FontFamily(Font(R.font.roboto_medium))
        )

    val input: TextStyle
        @Composable get() = base.copy(
            fontFamily = FontFamily(Font(R.font.ubuntu_light)),
            fontSize = 20.sp
        )

    val data: TextStyle
        @Composable get() = base.copy(
            fontFamily = FontFamily(Font(R.font.ubuntu_light)),
            fontSize = 20.sp
        )

    val dataLarge: TextStyle
        @Composable get() = data.copy(
            fontSize = 30.sp
        )

    val title: TextStyle
        @Composable get() = base.copy(
            fontFamily = FontFamily(Font(R.font.ubuntu_regular)),
            fontSize = 22.sp,
            color = colorResource(R.color.dark_text),
        )

    val titleBig: TextStyle
        @Composable get() = title.copy(
            fontSize = 28.sp,
        )

    val titleLarge: TextStyle
        @Composable get() = title.copy(
            fontSize = 40.sp,
        )

    val subtitle: TextStyle
        @Composable get() = base.copy(
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.ubuntu_light)),
            color = colorResource(R.color.dark_text),
        )

    val button: TextStyle
        @Composable get() = base.copy(
            fontFamily = FontFamily(Font(R.font.ubuntu_light)),
            fontSize = 20.sp,
            color = colorResource(R.color.text_colored_background)
        )

    val buttonSmall: TextStyle
        @Composable get() = button.copy(
            fontSize = 17.sp,
        )

    val buttonLink: TextStyle
        @Composable get() = buttonSmall.copy(
            fontFamily = FontFamily(Font(R.font.roboto_medium)),
            fontSize = 16.sp,
        )

    @Deprecated("Use buttonSmall", ReplaceWith("buttonSmall"))
    val buttonMenu: TextStyle
        @Composable get() = buttonSmall

    @Deprecated("Use buttonSmall", ReplaceWith("buttonSmall"))
    val buttonMenuBorderless: TextStyle
        @Composable get() = buttonSmall

    val sectionHeader: TextStyle
        @Composable get() = button.copy(
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.roboto_medium)),
            color = colorResource(R.color.dark_text),
        )
}