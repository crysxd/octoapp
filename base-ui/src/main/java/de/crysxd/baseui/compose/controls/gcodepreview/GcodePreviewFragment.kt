package de.crysxd.baseui.compose.controls.gcodepreview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.menu.MenuBottomSheetFragment
import de.crysxd.octoapp.menu.controls.GcodeSettingsMenu

class GcodePreviewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(navArgs<GcodePreviewFragmentArgs>().value.instanceId) {
        val instanceId = LocalOctoPrint.current.id
        val vmFactory = GcodePreviewControlsViewModel.Factory(instanceId)
        val vm = viewModel(
            modelClass = GcodePreviewControlsViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory
        )

        val scope = remember(vm) {
            object : GcodePreviewScope {
                override fun setManualProgress(layerIndex: Int, layerProgress: Float) = vm.useManual(layerIndex = layerIndex, layerProgress = layerProgress)
                override fun onRetry() = vm.retry()
                override fun onDownload() = vm.allowLargeDownloads()
                override fun syncWithLive() = vm.useLive()
                override fun showSettings() = MenuBottomSheetFragment.createForMenu(
                    menu = GcodeSettingsMenu(),
                    instanceId = instanceId,
                ).show(childFragmentManager)
            }
        }

        val state by vm.state.collectAsState(GcodePreviewControlsViewModel.ViewState.Loading())
        scope.GcodePreviewView(state = state)
    }
}