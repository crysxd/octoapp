package de.crysxd.baseui.compose.framework.helpers

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import androidx.compose.animation.core.Animatable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.detectTransformGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Canvas
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.ImageBitmapConfig
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.PaintingStyle
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.drawscope.CanvasDrawScope
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.core.content.ContextCompat
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.ext.drawableRes
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.gcode.GcodeRenderer
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okio.withLock
import java.util.concurrent.locks.ReentrantLock
import kotlin.math.roundToInt


@Composable
fun GcodeRenderCanvas(
    modifier: Modifier,
    acceptTouchInput: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    asyncRender: Boolean = !acceptTouchInput,
    renderParams: () -> GcodeRenderer.RenderParams?,
    nativeOverlay: (GcodeNativeCanvas.() -> Unit)? = null,
    onTap: ((Float, Float) -> Unit)? = null,
) {
    val density = LocalDensity.current
    val layoutDirection = LocalLayoutDirection.current
    val context = LocalContext.current
    var size = IntSize.Zero
    val scope = rememberCoroutineScope()
    val alphaEffect = remember { Animatable(0f) }
    var asyncResult by remember { mutableStateOf<Pair<ImageBitmap, ImageBitmap>?>(null) }
    var renderId by remember { mutableIntStateOf(0) }
    val adapter = remember(context) {
        DrawScopeAdapter(
            context = context,
            size = { size },
            invalidate = {
                if (!asyncRender) renderId++
            }
        )
    }
    val renderer = remember(adapter) { GcodeRenderer(adapter) }

    LaunchedEffect(renderParams(), size, asyncRender, nativeOverlay, density) {
        val plugin: GcodeNativeCanvas.() -> Unit = nativeOverlay ?: {}
        renderer.plugin = plugin
        renderer.submitRenderParams(renderParams())

        if (asyncRender && size != IntSize.Zero) withContext(Dispatchers.Default) {
            val makeBitmap = { ImageBitmap(size.width, size.height, ImageBitmapConfig.Argb8888) }
            val bitmaps = asyncResult?.takeIf { it.first.size == size } ?: (makeBitmap() to makeBitmap())

            bitmaps.get(renderId + 1).renderGcode(
                renderer = renderer,
                layoutDirection = layoutDirection,
                density = density,
                adapter = adapter,
            )
            asyncResult = bitmaps
            renderId++
        } else {
            // Rendered will increment renderId
            asyncResult = null
        }

        alphaEffect.animateTo(1f)
    }

    Box(
        modifier = modifier
            .clip(RectangleShape)
            .padding(contentPadding)
            .onSizeChanged { size = it }
            .graphicsLayer { alpha = alphaEffect.value }
            .drawWithContent {
                // Important: We need to touch renderId here so a change always causes a redraw
                val r = renderId

                val ar = asyncResult
                    ?.takeIf { size == it.first.size }
                    ?.get(r)

                if (ar != null) {
                    drawImage(ar)
                } else {
                    if (asyncRender) Napier.v(tag = "GcodeRenderView", message = "Async not available yet, drawing sync")
                    renderGcode(renderer, adapter)
                }
            }
            .pointerInput(acceptTouchInput) {
                if (!acceptTouchInput) return@pointerInput

                detectTransformGestures { centroid, pan, zoomChange, _ ->
                    renderer.zoomBy(
                        scaleFactor = zoomChange,
                        focusX = centroid.x,
                        focusY = centroid.y
                    )

                    renderer.moveBy(
                        x = -pan.x,
                        y = -pan.y
                    )
                }
            }
            .pointerInput(acceptTouchInput) {
                if (!acceptTouchInput) return@pointerInput

                detectTapGestures(
                    onDoubleTap = { focus ->
                        val oldZoom = renderer.zoom
                        val newZoom = if (renderer.zoom == renderer.doubleTapZoom) 1f else renderer.doubleTapZoom
                        Animatable(0f).apply {
                            scope.launch {
                                animateTo(1f) {
                                    renderer.zoom(
                                        newZoom = oldZoom + (newZoom - oldZoom) * value,
                                        focusX = focus.x,
                                        focusY = focus.y
                                    )
                                }
                            }
                        }
                    },
                    onTap = { offset ->
                        val (x, y) = renderer.getPrintBedCoordinateFromViewPortPosition(x = offset.x, y = offset.y)
                        onTap?.invoke(x, y)
                    }
                )
            }
    )
}

private fun ImageBitmap.renderGcode(
    renderer: GcodeRenderer,
    adapter: DrawScopeAdapter,
    density: Density,
    layoutDirection: LayoutDirection,
) = try {
    CanvasDrawScope().draw(
        density = density,
        layoutDirection = layoutDirection,
        canvas = Canvas(this),
        size = size.toSize()
    ) {
        try {
            renderGcode(renderer, adapter)
        } catch (e: Exception) {
            Napier.w(tag = "GcodeRenderCanvas", message = "Rendering failed (2)", throwable = e)
        }
    }
} catch (e: Exception) {
    Napier.w(tag = "GcodeRenderCanvas", message = "Rendering failed (1)", throwable = e)
}

private fun DrawScope.renderGcode(
    renderer: GcodeRenderer,
    adapter: DrawScopeAdapter,
) = adapter.withScope(this) {
    renderer.draw()
}

private val ImageBitmap.size get() = IntSize(width = width, height = height)
private fun Pair<ImageBitmap, ImageBitmap>.get(renderId: Int) = if (renderId % 2 == 0) first else second

private class DrawScopeAdapter(
    private val context: Context,
    private val size: () -> IntSize,
    private val invalidate: () -> Unit,
) : GcodeNativeCanvas {

    private var canvas: Canvas? = null

    private val lock = ReentrantLock()
    private var paint = Paint()
    private val colorMoveExtrusion = Color(ContextCompat.getColor(context, R.color.gcode_extrusion))
    private val colorMoveTravel = Color(ContextCompat.getColor(context, R.color.gcode_travel))
    private val colorMovePreviousLayer = Color(ContextCompat.getColor(context, R.color.gcode_previous))
    private val colorMoveRemainingLayer = Color(ContextCompat.getColor(context, R.color.gcode_remaining))
    private val colorMoveUnsupported = Color(ContextCompat.getColor(context, R.color.gcode_unsupported))
    private val colorPrintHead = Color(ContextCompat.getColor(context, R.color.gcode_print_head))
    private val red = Color(ContextCompat.getColor(context, R.color.red))
    private val green = Color(ContextCompat.getColor(context, R.color.green))
    private val colorYellow = Color(ContextCompat.getColor(context, R.color.yellow))
    private val colorLightGrey = Color(ContextCompat.getColor(context, R.color.light_grey))
    private val accent = Color(ContextCompat.getColor(context, R.color.accent))
    private val imageCache = mutableMapOf<GcodeNativeCanvas.Image, ImageBitmap>()

    override val viewPortWidth get() = size().width.toFloat()
    override val viewPortHeight get() = size().height.toFloat()
    override val paddingTop get() = 0f
    override val paddingLeft get() = 0f
    override val paddingRight get() = 0f
    override val paddingBottom get() = 0f

    init {
        setQuality(GcodePreviewSettings.Quality.Medium)
    }

    // We lock this operation as we've seen crashes with restore under count and this function
    // might be accessed async in rare cases
    fun withScope(scope: DrawScope, block: () -> Unit) = lock.withLock {
        scope.drawIntoCanvas {
            canvas = it
            block()
            canvas = null
        }
    }

    fun setQuality(quality: GcodePreviewSettings.Quality) {
        paint.isAntiAlias = quality >= GcodePreviewSettings.Quality.Ultra
        paint.strokeCap = if (quality >= GcodePreviewSettings.Quality.Medium) StrokeCap.Round else StrokeCap.Butt
    }

    private fun GcodeNativeCanvas.Image.load() = imageCache.getOrPut(this) {
        (ContextCompat.getDrawable(context, drawableRes) as BitmapDrawable).bitmap.asImageBitmap()
    }

    private fun GcodeNativeCanvas.Paint.map(): Paint = paint.also {
        it.strokeWidth = strokeWidth
        it.style = when (mode) {
            GcodeNativeCanvas.Paint.Mode.Fill -> PaintingStyle.Fill
            GcodeNativeCanvas.Paint.Mode.Stroke -> PaintingStyle.Stroke
        }
        it.color = when (val c = color) {
            GcodeNativeCanvas.Color.MoveExtrusion -> colorMoveExtrusion
            GcodeNativeCanvas.Color.MovePreviousLayer -> colorMovePreviousLayer
            GcodeNativeCanvas.Color.MoveRemainingLayer -> colorMoveRemainingLayer
            GcodeNativeCanvas.Color.MoveTravel -> colorMoveTravel
            GcodeNativeCanvas.Color.MoveUnsupported -> colorMoveUnsupported
            GcodeNativeCanvas.Color.PrintHead -> colorPrintHead
            GcodeNativeCanvas.Color.Yellow -> colorYellow
            GcodeNativeCanvas.Color.LightGrey -> colorLightGrey
            GcodeNativeCanvas.Color.Accent -> accent
            GcodeNativeCanvas.Color.Red -> red
            GcodeNativeCanvas.Color.Green -> green
            is GcodeNativeCanvas.Color.Custom -> Color(red = (c.red * 255).roundToInt(), green = (c.green * 255).roundToInt(), blue = (c.blue * 255).roundToInt())
        }
        it.alpha = alpha
    }

    override fun invalidate() =
        invalidate.invoke()

    override fun saveState() {
        canvas?.save()
    }

    override fun restoreState() {
        try {
            canvas?.restore()
        } catch (e: Exception) {
            Napier.w(tag = "GcodeRenderCanvas", message = "State restore failed", throwable = e)
        }
    }

    override fun scale(x: Float, y: Float) {
        canvas?.scale(x, y)
    }

    override fun translate(x: Float, y: Float) {
        canvas?.translate(x, y)
    }

    override fun rotate(degrees: Float) {
        canvas?.rotate(degrees)
    }

    override fun drawLine(xStart: Float, yStart: Float, xEnd: Float, yEnd: Float, paint: GcodeNativeCanvas.Paint) {
        canvas?.drawLine(Offset(xStart, yStart), Offset(xEnd, yEnd), paint.map())
    }

    override fun drawLines(points: FloatArray, offset: Int, count: Int, paint: GcodeNativeCanvas.Paint) {
        canvas?.nativeCanvas?.drawLines(points, offset, count, paint.map().asFrameworkPaint())
    }

    override fun drawRect(left: Float, top: Float, right: Float, bottom: Float, paint: GcodeNativeCanvas.Paint) {
        canvas?.drawRect(left, top, right, bottom, paint.map())
    }

    override fun intrinsicWidth(image: GcodeNativeCanvas.Image) =
        image.load().width.toFloat()

    override fun intrinsicHeight(image: GcodeNativeCanvas.Image) =
        image.load().height.toFloat()

    override fun drawImage(image: GcodeNativeCanvas.Image, x: Float, y: Float, width: Float, height: Float) {
        val bitmap = image.load()
        canvas?.drawImageRect(
            bitmap,
            srcOffset = IntOffset.Zero,
            srcSize = bitmap.size,
            dstOffset = IntOffset(x.toInt(), y.toInt()),
            dstSize = IntSize(width.toInt(), height.toInt()),
            paint
        )
    }

    override fun drawArc(
        centerX: Float,
        centerY: Float,
        radius: Float,
        startDegrees: Float,
        sweepAngle: Float,
        paint: GcodeNativeCanvas.Paint
    ) {
        canvas?.drawArc(
            left = centerX - radius, centerY - radius, centerX + radius, centerY + radius, startDegrees, sweepAngle, false, paint.map()
        )
    }
}