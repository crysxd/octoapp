package de.crysxd.baseui.compose.controls.tune

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.octoapp.engine.models.tune.TuneState

class TuneControlsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = navArgs<TuneControlsFragmentArgs>().value.instanceId) {
        val vmFactory = TuneControlsViewModel.Factory(LocalOctoPrint.current.id)
        val vm = viewModel(
            modelClass = TuneControlsViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory,
            viewModelStoreOwner = LocalOctoActivity.current ?: requireNotNull(LocalViewModelStoreOwner.current)

        )

        DisposableEffect(Unit) {
            vm.setActivePoll(activePoll = true)
            onDispose { vm.setActivePoll(activePoll = false) }
        }

        val tuneState by vm.state.collectAsStateWhileActive(
            initial = TuneState(),
            key = "tune-controls-details"
        )

        // Automatic lights. We want the lights to be on while tuning the print
        vm.fullscreenState.collectAsStateWhileActive(
            initial = Unit,
            key = "tune-controls-fullscreen"
        )

        TuneControlsView(
            tuneState = tuneState,
            onChangeOffset = vm::changeOffset,
            onApplyValues = vm::applyValues,
            onResetOffset = vm::resetOffset,
            onSaveOffsetToConfig = vm::saveOffsetToConfig,
        )
    }
}