package de.crysxd.baseui.compose.screens.companion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import de.crysxd.baseui.BaseBottomSheetDialogFragment
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.octoapp.base.di.BaseInjector

class CompanionAnnouncementBottomSheet : BaseBottomSheetDialogFragment() {

    private val preferences by lazy { BaseInjector.get().octoPreferences() }
    override val viewModel = ViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = null) {
        CompanionAnnouncementView {
            dismiss()
        }
    }

    class ViewModel : BaseViewModel()
}