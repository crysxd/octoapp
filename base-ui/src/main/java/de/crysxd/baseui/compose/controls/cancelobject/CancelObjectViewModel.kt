package de.crysxd.baseui.compose.controls.cancelobject

import android.graphics.RectF
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.CancelObjectViewModelCore
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class CancelObjectViewModel(
    private val instanceId: String,
) : GenericLoadingControlsViewModel<List<CancelObjectViewModel.ObjectList>>() {

    private val core = de.crysxd.octoapp.viewmodels.CancelObjectViewModelCore(instanceId = instanceId)
    private val loadingObjects = MutableStateFlow(emptySet<String>())
    val stateCache get() = core.stateCache.map()
    val state = core.state.map { state -> state.map() }

    fun cancelObject(objectId: String) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        loadingObjects.update { it + objectId }
        core.cancelObject(objectId)
    }.invokeOnCompletion {
        loadingObjects.update { it - objectId }
    }

    override fun retry() = core.retry()

    private fun FlowState<de.crysxd.octoapp.viewmodels.CancelObjectViewModelCore.ObjectList>.map() = when (this) {
        is FlowState.Error -> ViewState.Error(throwable)
        is FlowState.Loading -> ViewState.Loading
        is FlowState.Ready -> if (data.all.isEmpty()) {
            ViewState.Hidden
        } else {
            ViewState.Data(
                data = ObjectList(
                    all = data.all.map(),
                    subset = data.subset.map()
                ),
                contentKey = data.all.joinToString { it.label + it.objectId }
            )
        }
    }

    private fun List<CancelObjectViewModelCore.PrintObject>.map() = map {
        PrintObject(
            objectId = it.objectId,
            cancelled = it.cancelled,
            active = it.active,
            label = it.label,
            loading = it.objectId in loadingObjects.value,
            center = it.center,
            core = it,
        )
    }

    private fun CancelObjectViewModelCore.Box.map() = RectF(minX, minY, maxX, maxY)

    data class PrintObject(
        val objectId: String,
        val cancelled: Boolean,
        val loading: Boolean,
        val active: Boolean,
        val label: String,
        val core: CancelObjectViewModelCore.PrintObject,
        val center: Pair<Float, Float>?,
    )

    data class ObjectList(
        val all: List<PrintObject>,
        val subset: List<PrintObject>,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = CancelObjectViewModel(instanceId = instanceId) as T
    }
}