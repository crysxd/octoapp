package de.crysxd.baseui.compose.framework.helpers

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import de.crysxd.baseui.compose.framework.components.OctoErrorDialog
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException


@Composable
fun rememberErrorDialog(): ErrorDialogScope {
    var error by remember { mutableStateOf<Throwable?>(null) }
    error?.let {
        OctoErrorDialog(it) {
            error = null
        }
    }


    return remember {
        object : ErrorDialogScope {
            override suspend fun runWithErrorDialog(block: suspend () -> Unit) {
                try {
                    block()
                } catch (e: CancellationException) {
                    // ...
                } catch (e: Exception) {
                    Napier.e(tag = "ErrorDialogScope", message = "Executing failed", throwable = e)
                    error = e
                }
            }
        }
    }
}

interface ErrorDialogScope {
    suspend fun runWithErrorDialog(block: suspend () -> Unit)
}