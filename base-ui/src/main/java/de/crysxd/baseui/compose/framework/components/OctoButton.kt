package de.crysxd.baseui.compose.framework.components

import androidx.annotation.DrawableRes
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.rememberErrorDialog
import de.crysxd.baseui.compose.framework.helpers.thenIf
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

@Composable
fun OctoButton(
    modifier: Modifier = Modifier,
    text: String,
    @DrawableRes icon: Int? = null,
    type: OctoButtonType = OctoButtonType.Primary,
    small: Boolean = false,
    successAnimation: Boolean = false,
    loading: Boolean = false,
    enabled: Boolean = true,
    horizontalPadding: Boolean = true,
    onLongClick: (suspend () -> Unit)? = null,
    onClick: suspend () -> Unit
) = OctoButton(
    modifier = modifier,
    content = {
        Row(
            horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(horizontal = if (horizontalPadding) OctoAppTheme.dimens.margin2 else 0.dp)
        ) {
            if (icon != null) {
                Icon(
                    painter = painterResource(id = icon),
                    contentDescription = null,
                )
            }

            Text(
                text = text,
                style = LocalTextStyle.current,
                color = LocalContentColor.current,
                textAlign = TextAlign.Center,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
        }
    },
    type = type,
    small = small,
    successAnimation = successAnimation,
    loading = loading,
    enabled = enabled,
    onLongClick = onLongClick,
    onClick = onClick
)

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun OctoButton(
    modifier: Modifier = Modifier,
    type: OctoButtonType = OctoButtonType.Primary,
    small: Boolean = false,
    successAnimation: Boolean = false,
    loading: Boolean = false,
    enabled: Boolean = true,
    onLongClick: (suspend () -> Unit)? = null,
    onClick: suspend () -> Unit,
    content: @Composable () -> Unit,
) {
    //region State
    val scope = rememberCoroutineScope()
    val errorDialog = rememberErrorDialog()
    var internalLoading by remember { mutableStateOf(false) }
    val finalLoading = internalLoading || loading
    val targetBackground = when {
        !enabled || finalLoading -> type.backgroundDisabled()
        else -> type.background()
    }
    val targetForeground = when {
        !enabled || finalLoading -> type.foreground().copy(alpha = 0.5f)
        else -> type.foreground()
    }
    val targetElevation = when {
        targetBackground.alpha == 0f -> 0.dp
        type.suppressShadow -> 0.dp
        !enabled || finalLoading || small -> 0.dp
        else -> 3.dp
    }
    var targetBackgroundOverride by remember { mutableStateOf<Color?>(null) }

    val background by animateColorAsState(targetValue = targetBackgroundOverride ?: targetBackground, label = "background")
    val foreground by animateColorAsState(targetValue = targetForeground, label = "foreground")
    val elevation by animateDpAsState(targetValue = targetElevation, label = "elevation")
    //endregion
    //region Success
    var successTrigger by remember { mutableIntStateOf(0) }
    val successColor = type.success()
    LaunchedEffect(successTrigger) {
        if (successTrigger == 0 || !successAnimation) return@LaunchedEffect
        targetBackgroundOverride = successColor
        delay(1.seconds)
        targetBackgroundOverride = null
    }
    //endregion

    Surface(
        modifier = modifier.thenIf(!small || type.suppressShadow) { fillMaxWidth() },
        border = type.stroke?.let { BorderStroke(1.dp, if (enabled) it() else OctoAppTheme.colors.lightText) },
        elevation = elevation,
        contentColor = foreground,
        color = background,
        shape = type.shape(),
    ) {
        fun saveCall(b: suspend () -> Unit) = scope.launch {
            try {
                internalLoading = true
                errorDialog.runWithErrorDialog { b() }
                successTrigger++
            } finally {
                internalLoading = false
            }
        }

        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.combinedClickable(
                role = Role.Button,
                onClick = { saveCall(onClick) },
                onLongClick = onLongClick?.let { { saveCall(onLongClick) } },
                enabled = enabled,
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = targetForeground)
            )
        ) {
            //region Loading
            val style = if (small) OctoAppTheme.typography.buttonSmall else OctoAppTheme.typography.button
            val fontSize = with(LocalDensity.current) { style.fontSize.value.sp.toDp() }
            val verticalMargin = if (small) OctoAppTheme.dimens.margin12 else OctoAppTheme.dimens.margin2
            val height = fontSize + verticalMargin * 1.5f
            val textAlpha by animateFloatAsState(targetValue = if (finalLoading) 0f else 1f, label = "textAlpha")
            if (textAlpha < 1f) {
                CircularProgressIndicator(
                    color = LocalContentColor.current,
                    modifier = Modifier
                        .heightIn(max = height)
                        .scale(if (small) 0.5f else 0.66f)
                        .graphicsLayer { alpha = 1 - textAlpha }
                )
            }
            //endregion
            //region Text and Icon
            Row(
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .graphicsLayer { alpha = textAlpha }
                    .height(height)
            ) {
                CompositionLocalProvider(LocalTextStyle provides style) {
                    content()
                }
            }
            //endregion
        }
    }
}

sealed class OctoButtonType(
    open val background: @Composable () -> Color,
    open val backgroundDisabled: @Composable () -> Color = { OctoAppTheme.colors.lightGrey },
    open val foreground: @Composable () -> Color,
    open val stroke: (@Composable () -> Color)? = null,
    open val success: @Composable () -> Color = { OctoAppTheme.colors.green },
    open val shape: @Composable () -> Shape = { MaterialTheme.shapes.small },
    open val suppressShadow: Boolean = false
) {

    data object Primary : OctoButtonType(
        background = { OctoAppTheme.colors.accent },
        foreground = { OctoAppTheme.colors.textColoredBackground },
    )

    data object PrimaryUnselected : OctoButtonType(
        background = { Color.Transparent },
        foreground = { OctoAppTheme.colors.accent },
    )

    data object Secondary : OctoButtonType(
        background = { OctoAppTheme.colors.inputBackground },
        foreground = { OctoAppTheme.colors.accent },
        stroke = { OctoAppTheme.colors.accent },
        success = { OctoAppTheme.colors.greenTranslucent },
        backgroundDisabled = { OctoAppTheme.colors.veryLightGrey }
    )

    data object Link : OctoButtonType(
        background = { Color.Transparent },
        foreground = { OctoAppTheme.colors.accent },
        backgroundDisabled = { Color.Transparent },
        success = { OctoAppTheme.colors.greenTranslucent }
    )

    data object Surface : OctoButtonType(
        background = { OctoAppTheme.colors.accent },
        foreground = { OctoAppTheme.colors.textColoredBackground },
        shape = { RectangleShape },
        suppressShadow = true,
        success = { OctoAppTheme.colors.greenTranslucent }
    )

    data object LinkColoredBackground : OctoButtonType(
        background = { Color.Transparent },
        foreground = { OctoAppTheme.colors.textColoredBackground },
        backgroundDisabled = { Color.Transparent },
        success = { OctoAppTheme.colors.greenTranslucent }
    )

    data class LinkWithCustomColor(
        val color: Color,
    ) : OctoButtonType(
        background = { Color.Transparent },
        foreground = { color },
        backgroundDisabled = { Color.Transparent },
    )

    data class Special(
        override val background: @Composable () -> Color,
        override val foreground: @Composable () -> Color,
        override val stroke: (@Composable () -> Color)? = null,
        override val shape: @Composable () -> Shape = { MaterialTheme.shapes.small },
        override val suppressShadow: Boolean,
    ) : OctoButtonType(
        background = background,
        foreground = foreground,
        stroke = stroke,
        shape = shape,
        suppressShadow = suppressShadow,
    )
}

//region Previews
@Preview
@Composable
private fun OctoButtonPreviews() = OctoAppThemeForPreview {
    val onClick: suspend () -> Unit = { delay(1000) }
    val types = listOf(
        OctoButtonType.Primary,
        OctoButtonType.Secondary,
        OctoButtonType.Surface,
        OctoButtonType.Link,
        OctoButtonType.LinkWithCustomColor(Color.Red)
    )

    Column(
        verticalArrangement = Arrangement.spacedBy(20.dp),
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        types.forEach { type ->
            Column(
                verticalArrangement = Arrangement.spacedBy(5.dp),
                modifier = Modifier
                    .background(Color.Gray.copy(alpha = 0.2f))
                    .padding(5.dp),
            ) {
                Text(type.toString())
                OctoButton(text = "Primary", type = type, onClick = onClick)
                OctoButton(text = "Primary", type = type, onClick = onClick, icon = R.drawable.ic_round_animation_24)
                OctoButton(text = "Primary", type = type, onClick = onClick, enabled = false)
                OctoButton(text = "Primary", type = type, onClick = onClick, small = true)
                OctoButton(text = "Primary", type = type, onClick = onClick, loading = true)
            }
        }
    }
}

//endregion