package de.crysxd.baseui.compose.framework.components

import android.net.Uri
import androidx.annotation.OptIn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.media3.common.MediaItem
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.ui.PlayerView

@Composable
@OptIn(UnstableApi::class)
@kotlin.OptIn(ExperimentalComposeUiApi::class)
fun OctoVideoPlayer(
    videoUri: Uri,
    modifier: Modifier = Modifier,
) {
    var playbackPosition by rememberSaveable(videoUri) { mutableStateOf(0L) }

    AndroidView(
        modifier = modifier,
        factory = { context ->
            PlayerView(context).apply {
                player = ExoPlayer.Builder(context).build()
            }
        },
        update = { view ->
            view.player?.apply {
                playWhenReady = true
                setMediaItem(MediaItem.fromUri(videoUri))
                prepare()
                seekTo(playbackPosition)
            }
            view.showController()
        },
        onReset = {},
        onRelease = { view ->
            playbackPosition = view.player?.currentPosition ?: 0
            view.player?.release()
        }
    )
}