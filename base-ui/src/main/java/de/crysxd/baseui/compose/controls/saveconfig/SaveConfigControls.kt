package de.crysxd.baseui.compose.controls.saveconfig

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.components.AnnouncementId
import de.crysxd.baseui.compose.framework.components.OctoAnnouncementController
import de.crysxd.baseui.compose.framework.components.SmallAnnouncement
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import kotlinx.coroutines.delay
import kotlin.time.Duration.Companion.seconds

@Composable
fun SaveConfigControls(
    state: SaveConfigControlsState,
    editState: EditState,
) = AnimatedVisibility(
    visible = state.saveConfigPending && !editState.editing,
    enter = expandVertically(expandFrom = Alignment.Top) + fadeIn(),
    exit = shrinkVertically(shrinkTowards = Alignment.Top) + fadeOut(),
) {
    OctoAnnouncementController(announcementId = AnnouncementId.Default("save-config")) {
        SmallAnnouncement(
            text = stringResource(id = R.string.save_config___description),
            learnMoreButton = stringResource(id = R.string.save_config___action),
            onLearnMore = {
                // Trigger config change
                state.saveConfig()

                // Let Klipper restart. The app will not immediately navigate away
                delay(30.seconds)
            },
            hideButton = null,
            modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
        )
    }
}

@Composable
fun rememberSaveConfigState(): SaveConfigControlsState {
    val vmFactory = SaveConfigViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = SaveConfigViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = OctoAppTheme.octoActivity as ViewModelStoreOwner
    )

    return remember(vm) {
        object : SaveConfigControlsState {
            override val saveConfigPending
                @Composable get() = vm.saveConfigPending.collectAsState(initial = false).value

            override suspend fun saveConfig() =
                vm.saveConfig()
        }
    }
}

interface SaveConfigControlsState {
    val saveConfigPending: Boolean @Composable get
    suspend fun saveConfig()
}