package de.crysxd.baseui.compose.controls.connect

import androidx.annotation.DrawableRes
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.common.OctoView
import de.crysxd.baseui.common.controls.ControlsBottomBarAction
import de.crysxd.baseui.common.controls.LocalControlCenter
import de.crysxd.baseui.common.controls.LocalControlsBottomBarScope
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.helpers.thenIf
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase.ActionType.AutoConnectTutorial
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase.ActionType.BeginConnect
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase.ActionType.ConfigurePsu
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.connect.AutoConnectPrinterInfoMenu
import de.crysxd.octoapp.menu.connect.SelectPsuMenu
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.viewmodels.ConnectionControlsViewModelCore
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

@Composable
fun ConnectionControls(
    state: ConnectControlsState,
) = Box(
    modifier = Modifier.fillMaxWidth(),
    contentAlignment = Alignment.Center,
) {
    //region Menu
    var menu by remember { mutableStateOf<Menu?>(null) }
    LaunchedEffect(state.menuShownEvent) {
        state.menuShownEvent?.let { (_, m) ->
            menu = m
        }
    }
    menu?.let { m ->
        MenuSheet(
            menu = m,
            onResult = { _ ->
                when (m) {
                    is AutoConnectPrinterInfoMenu -> state.executeAction(BeginConnect, confirmed = true)
                }
            },
            onDismiss = { menu = null },
        )
    }
    //endregion

    Column(
        modifier = Modifier
            .padding(top = OctoAppTheme.dimens.margin4)
            .widthIn(max = 500.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        ConnectionAvatar(avatarState = state.data.uiState.avatarState)

        Column(
            modifier = Modifier
                .offset(y = -OctoAppTheme.dimens.margin3)
                .fillMaxWidth()
                .padding(horizontal = OctoAppTheme.dimens.margin2),
            verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            StateDescription(uiState = state.data.uiState)
            Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin2))
            Options(state = state)
            BottomBarActions(state = state)
        }
    }
}

//region Internals
@Composable
private fun ConnectionAvatar(
    avatarState: ConnectPrinterUseCase.AvatarState,
) = Box(
    contentAlignment = Alignment.Center,
    modifier = Modifier
        .aspectRatio(16 / 9f)
        .widthIn(max = 400.dp)
        .fillMaxWidth()
) {
    //region State
    val colors = LocalOctoPrint.current.colors
    var lastState by remember { mutableStateOf<ConnectPrinterUseCase.AvatarState?>(null) }
    var lastColors by remember { mutableStateOf<Settings.Appearance.Colors?>(null) }
    val lifecycle = LocalLifecycleOwner.current.lifecycle
    //endregion
    //region Background
    Image(
        painter = painterResource(id = R.drawable.octo_background),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize()
            .scale(1.5f)
    )
    //endregion
    //region Foreground
    AndroidView(
        factory = { context ->
            OctoView(context)
        },
        update = { view ->
            if (lastColors != colors) {
                lastColors = colors
                view.loadForColor(colors)
            }

            if (lastState != avatarState) {
                lastState = avatarState
                view.doAfterAnimation(lifecycle = lifecycle) {
                    when (avatarState) {
                        ConnectPrinterUseCase.AvatarState.Idle -> view.idle()
                        ConnectPrinterUseCase.AvatarState.Swim -> view.swim()
                        ConnectPrinterUseCase.AvatarState.Party -> view.party()
                    }
                }
            }
        }
    )
    //endregion
}

@Composable
private fun BottomBarActions(
    state: ConnectControlsState,
) = LocalControlsBottomBarScope.current?.let { scope ->
    val actionType = state.data.uiState.actionType
    val bottomBarAction = ControlsBottomBarAction(
        text = state.data.uiState.action ?: "",
        buttonType = if (state.data.uiState.primaryAction) OctoButtonType.Primary else OctoButtonType.Secondary,
        onClick = { actionType?.let { state.executeAction(it) } }
    ).takeIf { it.text.isNotBlank() && actionType != null }

    DisposableEffect(bottomBarAction) {
        scope.customAction = bottomBarAction
        onDispose { scope.customAction = null }
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun Options(
    state: ConnectControlsState,
) = AnimatedContent(
    targetState = Triple(
        state.data.uiState.exception != null,
        state.data.advertise,
        state.data.service,
    )
) { (_, advertise, service) ->
    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
    ) {
        ErrorDisplay(exception = state.data.uiState.exception)
        RemoteAdvertisementView(advertise = advertise, service = service)
    }
}

@Composable
private fun ErrorDisplay(
    exception: Throwable?,
) = exception?.let { e ->
    val host = OctoAppTheme.octoActivity
    val octoPrint = if (OctoAppTheme.isPreview) null else LocalOctoPrint.current
    val controlCenter = LocalControlCenter.current

    InformationalButton(
        icon = R.drawable.ic_round_network_check_24,
        text = stringResource(id = R.string.connect_printer___action_learn_more),
        onClick = { host?.showErrorDetailsDialog(e, offerSupport = false) },
    )

    InformationalButton(
        icon = R.drawable.ic_round_swap_horiz_24,
        text = stringResource(id = R.string.sign_in___connect_to_other_octoprint),
        onClick = {
            if (BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
                controlCenter?.show()
            } else {
                BaseInjector.get().octorPrintRepository().clearActive()
            }
        },
    )

    InformationalButton(
        icon = R.drawable.ic_round_auto_fix_high_24,
        text = stringResource(id = R.string.connect_printer___action_troubleshooting),
        onClick = {
            octoPrint ?: return@InformationalButton
            host ?: return@InformationalButton
            UriLibrary.getFixOctoPrintConnectionUri(baseUrl = octoPrint.webUrl, instanceId = octoPrint.id).open(host)
        },
    )
}

@Composable
private fun RemoteAdvertisementView(
    advertise: Boolean,
    service: String?,
) {
    if (advertise) {
        InformationalButton(
            icon = when (service) {
                OctoPlugins.Obico -> R.drawable.ic_obico_24px
                OctoPlugins.OctoEverywhere -> R.drawable.ic_octoeverywhere_24px
                OctoPlugins.Ngrok -> R.drawable.ic_ngrok_24px
                else -> R.drawable.ic_round_cloud_24
            },
            accentColor = when (service) {
                OctoPlugins.Obico -> OctoAppTheme.colors.textColoredBackground
                OctoPlugins.OctoEverywhere -> OctoAppTheme.colors.textColoredBackground
                OctoPlugins.Ngrok -> OctoAppTheme.colors.textColoredBackground
                else -> OctoAppTheme.colors.accent
            },
            backgroundColor = when (service) {
                OctoPlugins.Obico -> OctoAppTheme.colors.spaghettiDetective
                OctoPlugins.OctoEverywhere -> OctoAppTheme.colors.octoeverywhere
                OctoPlugins.Ngrok -> OctoAppTheme.colors.ngrok
                else -> OctoAppTheme.colors.inputBackground
            },
            textColor = when (service) {
                OctoPlugins.Obico -> OctoAppTheme.colors.textColoredBackground
                OctoPlugins.OctoEverywhere -> OctoAppTheme.colors.textColoredBackground
                OctoPlugins.Ngrok -> OctoAppTheme.colors.textColoredBackground
                else -> OctoAppTheme.colors.darkText
            },
            iconColor = when (service) {
                null -> OctoAppTheme.colors.accent
                else -> Color.Unspecified
            },
            iconBackground = when (service) {
                null -> OctoAppTheme.colors.accent.copy(alpha = 0.1f)
                OctoPlugins.Ngrok -> OctoAppTheme.colors.textColoredBackground.copy(alpha = 0.1f)
                else -> Color.Transparent
            },
            text = stringResource(id = R.string.connect_printer___action_configure_remote_access),
            onClick = { UriLibrary.getConfigureRemoteAccessUri().open() }
        )
    }
}

@Composable
private fun InformationalButton(
    @DrawableRes icon: Int,
    text: String,
    textColor: Color = OctoAppTheme.colors.darkText,
    accentColor: Color = OctoAppTheme.colors.accent,
    backgroundColor: Color = OctoAppTheme.colors.inputBackground,
    iconColor: Color = accentColor,
    iconBackground: Color = iconColor.copy(alpha = 0.1f),
    onClick: () -> Unit,
) = Row(
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier
        .fillMaxWidth()
        .clip(MaterialTheme.shapes.large)
        .background(backgroundColor)
        .clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = rememberRipple(color = accentColor),
            onClick = onClick,
        )
        .padding(OctoAppTheme.dimens.margin12)
) {
    Icon(
        painter = painterResource(id = icon),
        contentDescription = null,
        tint = iconColor,
        modifier = Modifier
            .size(width = 28.dp, height = 28.dp)
            .background(iconBackground, CircleShape)
            .thenIf(iconBackground.alpha > 0) { padding(OctoAppTheme.dimens.margin01) }
    )
    Text(
        text = text,
        color = textColor,
        style = OctoAppTheme.typography.base,
        modifier = Modifier.weight(1f)
    )
    Icon(
        painter = painterResource(id = R.drawable.ic_round_chevron_right_24),
        contentDescription = null,
        tint = accentColor,
    )
}

@Composable
private fun StateDescription(
    uiState: ConnectPrinterUseCase.UiState,
) = Crossfade(
    targetState = uiState,
    modifier = Modifier.animateContentSize(spring())
) { target ->
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth(),
    ) {
        Text(
            text = target.title,
            style = OctoAppTheme.typography.title,
            color = OctoAppTheme.colors.darkText,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin01)
        )
        Text(
            text = target.detail ?: "",
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.normalText,
            textAlign = TextAlign.Center,
        )
    }
}

//endregion
//region State
@Composable
fun rememberConnectionControlsState(): ConnectControlsState {
    val instanceId = LocalOctoPrint.current.id
    val octoActivity = OctoAppTheme.octoActivity
    val scope = rememberCoroutineScope()
    val vmFactory = ConnectionControlsViewModel.Factory(instanceId)
    val vm = viewModel(
        modelClass = ConnectionControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = requireNotNull(OctoAppTheme.octoActivity)
    )
    val menuShown = remember { mutableStateOf<Pair<Instant, Menu>?>(null) }
    val data = vm.state.collectAsStateWhileActive(
        key = "connection-controls",
        initial = ConnectionControlsViewModelCore.State(
            advertise = false,
            service = null,
            uiState = ConnectPrinterUseCase.UiState.Initializing
        )
    )

    return object : ConnectControlsState {
        override val data by data
        override val menuShownEvent by menuShown
        override suspend fun executeAction(action: ConnectPrinterUseCase.ActionType, confirmed: Boolean) = when (action) {
            AutoConnectTutorial -> menuShown.value = Clock.System.now() to AutoConnectPrinterInfoMenu()

            ConfigurePsu -> menuShown.value = Clock.System.now() to SelectPsuMenu(
                instanceId = instanceId
            )

            BeginConnect -> if (confirmed) {
                vm.executeAction(action)
            } else {
                octoActivity?.showDialog(
                    OctoActivity.Message.DialogMessage(
                        text = { getString(R.string.connect_printer___begin_connection_confirmation_message) },
                        positiveButton = { getString(R.string.connect_printer___begin_connection_cofirmation_positive) },
                        positiveAction = { scope.launch { vm.executeAction(action) } },
                        neutralButton = { getString(R.string.connect_printer___begin_connection_cofirmation_negative) },
                    )
                ) ?: Unit
            }

            else -> vm.executeAction(action)
        }
    }
}

interface ConnectControlsState {
    val data: ConnectionControlsViewModelCore.State
    val menuShownEvent: Pair<Instant, Menu>?
    suspend fun executeAction(action: ConnectPrinterUseCase.ActionType, confirmed: Boolean = false)
}

//endregion
//region Preview
@Composable
private fun PreviewBase(
    uiState: ConnectPrinterUseCase.UiState,
    advertise: Boolean = false,
    service: String? = null,
) = OctoAppThemeForPreview {
    ConnectionControls(
        state = object : ConnectControlsState {
            override val menuShownEvent = null
            override val data = ConnectionControlsViewModelCore.State(
                advertise = advertise,
                service = service,
                uiState = uiState,
            )

            override suspend fun executeAction(action: ConnectPrinterUseCase.ActionType, confirmed: Boolean) = Unit
        }
    )
}

@Preview
@Composable
private fun PreviewInitializing() = PreviewBase(
    uiState = ConnectPrinterUseCase.UiState.Initializing,
)

@Preview
@Composable
private fun PreviewNotAvailable() = PreviewBase(
    uiState = ConnectPrinterUseCase.UiState(
        title = getString("connect_printer___octoprint_not_available_title"),
        detail = "http://test.local\n\n" + getString("connect_printer___octoprint_not_available_detail"),
        avatarState = ConnectPrinterUseCase.AvatarState.Idle,
        exception = IllegalStateException("Something"),
        isNotAvailable = true,
    ),
    advertise = true,
    service = null,
)

@Preview
@Composable
private fun PreviewOctoPrintStarting() = PreviewBase(
    uiState = ConnectPrinterUseCase.UiState(
        title = getString("connect_printer___octoprint_starting_title"),
        detail = null,
        avatarState = ConnectPrinterUseCase.AvatarState.Swim,
    ),
    advertise = false,
    service = null,
)

@Preview
@Composable
private fun PreviewWaitingForUser() = PreviewBase(
    uiState = ConnectPrinterUseCase.UiState(
        title = getString("connect_printer___waiting_for_user_title"),
        detail = getString("connect_printer___waiting_for_user_subtitle"),
        avatarState = ConnectPrinterUseCase.AvatarState.Idle,
        action = getString("connect_printer___begin_connection"),
        actionType = BeginConnect
    ),
    advertise = false,
    service = null,
)
//endregion