package de.crysxd.baseui.compose.screens.help

interface AppUpdater {
    fun triggerUpdateIfAvailable(): Boolean
}