package de.crysxd.baseui.terminal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.baseui.compose.screens.terminal.TerminalScreen
import de.crysxd.baseui.compose.screens.terminal.rememberTerminalController


class TerminalFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = null) {
        TerminalScreen(rememberTerminalController())
    }
}