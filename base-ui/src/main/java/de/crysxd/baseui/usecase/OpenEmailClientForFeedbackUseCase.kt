package de.crysxd.baseui.usecase

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.octoapp.base.data.repository.NotificationIdRepository
import de.crysxd.octoapp.base.di.modules.FileModule
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.UseCase2
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.inject.Inject


class OpenEmailClientForFeedbackUseCase @Inject constructor(
    private val publicFileFactory: FileModule.PublicFileFactory,
    private val notificationIdRepository: NotificationIdRepository,
) : UseCase2<OpenEmailClientForFeedbackUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) = if (param.useShareSheet) {
        createShareIntent(param)
    } else {
        createEmailIntent(param)
    }.let { intent ->
        // Activity running? Launch directly
        if (OctoActivity.instance?.takeIf { it.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) }?.startActivity(intent) != null) {
            return@let
        }

        // Activity not running, post notification
        val notificationId = notificationIdRepository.getWearOsBugReportNotificationId()
        val manager = param.context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = param.context.getString(R.string.updates_notification_channel)
        val notification = NotificationCompat.Builder(param.context, channelId)
            .setContentText(param.context.getString(R.string.wear_bug_report___notification_description))
            .setContentTitle(param.context.getString(R.string.wear_bug_report___notification_title))
            .setSmallIcon(R.drawable.ic_notification_default)
            .setAutoCancel(true)
            .setColorized(true)
            .setLocalOnly(true)
            .setColor(ContextCompat.getColor(param.context, R.color.primary_dark))
            .setContentIntent(PendingIntent.getActivity(param.context, notificationId, intent, PendingIntent.FLAG_IMMUTABLE))
            .build()

        manager.notify(notificationId, notification)
    }

    private fun createEmailIntent(param: Params) = Intent(Intent.ACTION_SEND).also {
        it.type = "message/rfc822"
        it.putExtra(Intent.EXTRA_SUBJECT, param.subject)
        it.putExtra(Intent.EXTRA_TEXT, param.message)
        it.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        createFileUri(param)?.let { uri ->
            it.putExtra(Intent.EXTRA_STREAM, uri)
        }
    }

    private fun createShareIntent(param: Params) = ShareCompat.IntentBuilder(param.context)
        .setStream(createFileUri(param))
        .setChooserTitle("Feedback OctoApp for Android")
        .setEmailTo(arrayOf(email))
        .setSubject(param.subject)
        .setText(param.message)
        .setType("application/octet-stream")
        .createChooserIntent()

    private fun createFileUri(param: Params) = if (param.bugReport != null) {
        val (zipFile, zipFileUri) = publicFileFactory.createPublicFile("data.zip")
        ZipOutputStream(zipFile.outputStream()).use { zip ->
            param.bugReport.files.forEach {
                zip.putNextEntry(ZipEntry(it.name))
                zip.write(it.bytes)
                zip.flush()
            }
        }
        zipFileUri
    } else {
        null
    }

    private val email get() = Firebase.remoteConfig.getString("contact_email")
    private val Params.subject get() = "Feedback OctoApp for Android $version"
    private val Params.version get() = appVersion ?: SharedCommonInjector.get().platform.appVersion

    data class Params(
        val context: Context,
        val bugReport: CreateBugReportUseCase.BugReport?,
        val message: String,
        val appVersion: String? = null,
        val useShareSheet: Boolean,
    )
}
