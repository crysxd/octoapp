package de.crysxd.baseui.purchase

import android.graphics.Rect
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.PurchaseScreenshotFragmentBinding
import de.crysxd.baseui.di.BaseUiInjector

class PurchaseScreenshotFragment : Fragment(), InsetAwareScreen {

    private lateinit var binding: PurchaseScreenshotFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.base_ui_screenshot_transition)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(R.transition.base_ui_screenshot_transition)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        PurchaseScreenshotFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        BaseUiInjector.get().picasso()
            .load(navArgs<PurchaseScreenshotFragmentArgs>().value.url)
            .error(R.drawable.ic_round_image_not_supported_24)
            .into(binding.screenshot)

        binding.root.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun handleInsets(insets: Rect) {
        binding.root.updatePadding(top = insets.top, left = insets.left, bottom = insets.bottom, right = insets.right)
    }
}
