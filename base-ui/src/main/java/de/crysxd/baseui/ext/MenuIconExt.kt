package de.crysxd.baseui.ext

import androidx.annotation.DrawableRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import de.crysxd.baseui.R
import de.crysxd.octoapp.menu.MenuIcon

val MenuIcon.painter
    @Composable get() = painterResource(
        id = drawableRes
    )

val MenuIcon.drawableRes
    @DrawableRes get() = when (this) {
        MenuIcon.Automatic -> R.drawable.ic_round_auto_fix_high_24
        MenuIcon.Manual -> R.drawable.ic_baseline_auto_fix_off_24
        MenuIcon.Edit -> R.drawable.ic_round_edit_24
        MenuIcon.PowerOutlet -> R.drawable.ic_round_power_24
        MenuIcon.Fire -> R.drawable.ic_round_local_fire_department_24
        MenuIcon.Settings -> R.drawable.ic_round_settings_24
        MenuIcon.More -> R.drawable.ic_round_more_vert_24
        MenuIcon.Help -> R.drawable.ic_round_help_outline_24
        MenuIcon.Cloud -> R.drawable.ic_round_cloud_24
        MenuIcon.Tutorials -> R.drawable.ic_round_school_24
        MenuIcon.OctoPrint -> R.drawable.ic_octoprint_24px
        MenuIcon.Printer -> R.drawable.ic_round_print_24
        MenuIcon.Message -> R.drawable.ic_round_chat_bubble_24
        MenuIcon.Image -> R.drawable.ic_round_image_24
        MenuIcon.Layers -> R.drawable.ic_round_layers_24
        MenuIcon.TimeLeft -> R.drawable.ic_round_update_24
        MenuIcon.TimeSpent -> R.drawable.ic_round_restore_24
        MenuIcon.Height -> R.drawable.ic_round_height_24
        MenuIcon.FontSize -> R.drawable.ic_round_format_size_24
        MenuIcon.Label -> R.drawable.ic_round_label_24
        MenuIcon.Calendar -> R.drawable.ic_round_calendar_today_24
        MenuIcon.Notifications -> R.drawable.ic_round_notifications_active_24
        MenuIcon.UserInterface -> R.drawable.ic_round_phonelink_setup_24
        MenuIcon.Storage -> R.drawable.ic_round_sd_storage_24
        MenuIcon.Lab -> R.drawable.ic_round_science_24px
        MenuIcon.Switch -> R.drawable.ic_round_swap_horiz_24
        MenuIcon.VolumeUp -> R.drawable.ic_round_volume_up_24
        MenuIcon.Language -> R.drawable.ic_round_translate_24
        MenuIcon.Customize -> R.drawable.ic_round_person_pin_24
        MenuIcon.AppTheme -> R.drawable.ic_round_dark_mode_24
        MenuIcon.ScreenOn -> R.drawable.ic_round_brightness_high_24
        MenuIcon.LeftHand -> R.drawable.ic_round_left_back_hand_24
        MenuIcon.Reset -> R.drawable.ic_round_power_24
        MenuIcon.Lights -> R.drawable.ic_round_wb_incandescent_24
        MenuIcon.Secure -> R.drawable.ic_round_power_24
        MenuIcon.Light -> R.drawable.ic_round_wb_incandescent_24
        MenuIcon.Widget -> R.drawable.ic_round_widgets_24
        MenuIcon.OpenExternal -> R.drawable.ic_round_open_in_browser_24
        MenuIcon.Files -> R.drawable.ic_round_folder_24
        MenuIcon.Plugins -> R.drawable.ic_round_extension_24
        MenuIcon.TimelapseConfig -> R.drawable.ic_round_video_settings_24
        MenuIcon.TimelapseArchive -> R.drawable.ic_round_video_library_24
        MenuIcon.ActionReboot -> R.drawable.ic_command_restart_24px
        MenuIcon.ActionRestart -> R.drawable.ic_command_restart_24px
        MenuIcon.ActionRestartSafe -> R.drawable.ic_command_restart_24px
        MenuIcon.ActionShutDown -> R.drawable.ic_command_shutdown_24px
        MenuIcon.ActionGeneric -> R.drawable.ic_command_generic_24px
        MenuIcon.VideoCameraType -> R.drawable.ic_round_videocam_24
        MenuIcon.TimeInterval -> R.drawable.ic_round_access_time_24
        MenuIcon.ImageSequnce -> R.drawable.ic_round_burst_mode_24
        MenuIcon.Start -> R.drawable.ic_round_control_camera_24
        MenuIcon.Save -> R.drawable.ic_round_save_24
        MenuIcon.Send -> R.drawable.ic_round_send_24
        MenuIcon.PostRoll -> R.drawable.ic_round_update_24
        MenuIcon.Materials -> R.drawable.ic_round_layers_24
        MenuIcon.Webcam -> R.drawable.ic_round_videocam_24
        MenuIcon.PowerOff -> R.drawable.ic_round_power_off_24
        MenuIcon.Terminal -> R.drawable.ic_round_code_24
        MenuIcon.EmergencyStop -> R.drawable.ic_round_report_24px
        MenuIcon.Stop -> R.drawable.ic_round_stop_24
        MenuIcon.CoolDown -> R.drawable.ic_round_ac_unit_24
        MenuIcon.MaterialActive -> R.drawable.ic_round_hdr_auto_24px
        MenuIcon.MaterialActive0 -> R.drawable.counter_0_24dp
        MenuIcon.MaterialActive1 -> R.drawable.counter_1_24dp
        MenuIcon.MaterialActive2 -> R.drawable.counter_2_24dp
        MenuIcon.MaterialActive3 -> R.drawable.counter_3_24dp
        MenuIcon.MaterialActive4 -> R.drawable.counter_4_24dp
        MenuIcon.MaterialActive5 -> R.drawable.counter_5_24dp
        MenuIcon.MaterialActive6 -> R.drawable.counter_6_24dp
        MenuIcon.MaterialActive7 -> R.drawable.counter_7_24dp
        MenuIcon.MaterialActive8 -> R.drawable.counter_8_24dp
        MenuIcon.MaterialActive9 -> R.drawable.counter_9_24dp
        MenuIcon.MaterialInactive -> R.drawable.circle_24dp
        MenuIcon.MaterialReset -> R.drawable.restart_alt_24dp
        MenuIcon.CheckActive -> R.drawable.round_check_circle_24
        MenuIcon.Pause -> R.drawable.pause_black_24
        MenuIcon.CheckInactive -> R.drawable.round_check_circle_outline_24
        MenuIcon.PowerStrip -> R.drawable.ic_round_power_settings_new_24
        MenuIcon.PowerDeviceOff -> R.drawable.ic_round_power_off_24
        MenuIcon.PowerDeviceOn -> R.drawable.ic_round_power_24
        MenuIcon.PowerDeviceToggle -> R.drawable.ic_round_power_cycle_24px
        MenuIcon.PowerDeviceCycle -> R.drawable.ic_round_power_cycle_24px
        MenuIcon.Pinned -> R.drawable.ic_round_push_pin_24
        MenuIcon.Unpinned -> R.drawable.ic_round_push_pin_24
        MenuIcon.LabelOff -> R.drawable.ic_round_label_off_24
        MenuIcon.Delete -> R.drawable.ic_round_delete_24
        MenuIcon.Paste -> R.drawable.ic_round_content_paste_24
        MenuIcon.Ruler -> R.drawable.ic_round_photo_size_select_large_24
        MenuIcon.AspectRatio -> R.drawable.ic_round_image_aspect_ratio_24
        MenuIcon.Antenna -> R.drawable.ic_round_signal_cellular_alt_24
        MenuIcon.History -> R.drawable.ic_round_change_history_24
        MenuIcon.Block -> R.drawable.ic_round_block_24
        MenuIcon.CodeOff -> R.drawable.ic_round_code_off_24
        MenuIcon.Flow -> R.drawable.ic_flow_20px
        MenuIcon.Length -> R.drawable.ic_round_straighten_24
        MenuIcon.Bug -> R.drawable.ic_round_bug_report_24
        MenuIcon.Ethernet -> R.drawable.ic_round_settings_ethernet_24
        MenuIcon.PluginsOff -> R.drawable.ic_round_extension_off_24
        MenuIcon.Text -> R.drawable.ic_round_short_text_24
        MenuIcon.Battery -> R.drawable.ic_round_battery_charging_full_24
        MenuIcon.NotificationsOff -> R.drawable.ic_round_notifications_off_24
        MenuIcon.Rotation -> R.drawable.ic_round_screen_rotation_24
        MenuIcon.Network -> R.drawable.ic_round_settings_ethernet_24
        MenuIcon.UsbOff -> R.drawable.ic_usb_off_24
        MenuIcon.Share -> R.drawable.ic_round_share_24
        MenuIcon.Layout -> R.drawable.ic_round_view_compact_24
        MenuIcon.Server -> R.drawable.ic_round_dns_24
        MenuIcon.Play -> R.drawable.ic_round_play_arrow_24
        MenuIcon.ImageStack -> R.drawable.ic_round_image_24
        MenuIcon.Finger -> R.drawable.ic_round_swipe_right_24
        MenuIcon.Add -> R.drawable.ic_round_add_24
        MenuIcon.LogOut -> R.drawable.ic_round_login_24
        MenuIcon.Support -> R.drawable.ic_round_favorite_24
        MenuIcon.Info -> R.drawable.ic_round_info_24
        MenuIcon.LayersBelow -> R.drawable.ic_round_layers_24
        MenuIcon.PaintBrush -> R.drawable.ic_round_speed_24
        MenuIcon.Zero -> R.drawable.ic_round_exposure_zero_24
        MenuIcon.Link -> R.drawable.ic_round_cloud_24
        MenuIcon.Clock -> R.drawable.ic_round_access_time_24
        MenuIcon.SortOrder -> R.drawable.ic_round_swap_vert_24
        MenuIcon.SortBy -> R.drawable.ic_round_sort_24
        MenuIcon.HideCompleted -> R.drawable.ic_round_remove_done_24
        MenuIcon.CreateFolder -> R.drawable.ic_round_create_new_folder_24
        MenuIcon.UploadFile -> R.drawable.ic_round_upload_file_24
        MenuIcon.Copy -> R.drawable.ic_round_content_copy_24
        MenuIcon.Cut -> R.drawable.ic_round_content_cut_24
        MenuIcon.Speed -> R.drawable.ic_round_speed_24
        MenuIcon.Move -> R.drawable.round_open_with_24
        MenuIcon.Color -> R.drawable.round_color_lens_24
    }