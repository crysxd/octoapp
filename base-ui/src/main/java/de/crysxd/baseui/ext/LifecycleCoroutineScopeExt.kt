@file:Suppress("DEPRECATION")

package de.crysxd.baseui.ext

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.CoroutineScope

// We retire the old UI soon, patch this until then....
fun LifecycleCoroutineScope.launchWhenCreatedFixed(block: suspend CoroutineScope.() -> Unit) = launchWhenCreated(block)
fun LifecycleCoroutineScope.launchWhenStartedFixed(block: suspend CoroutineScope.() -> Unit) = launchWhenStarted(block)
fun LifecycleCoroutineScope.launchWhenResumedFixed(block: suspend CoroutineScope.() -> Unit) = launchWhenResumed(block)