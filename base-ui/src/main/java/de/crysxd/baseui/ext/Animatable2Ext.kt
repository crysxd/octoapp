package de.crysxd.baseui.ext

import android.graphics.drawable.Animatable2
import android.graphics.drawable.Drawable

fun Animatable2.oneOffEndAction(action: () -> Unit) {
    registerAnimationCallback(object : Animatable2.AnimationCallback() {
        override fun onAnimationEnd(drawable: Drawable?) {
            super.onAnimationEnd(drawable)
            unregisterAnimationCallback(this)
            action()
        }
    })
}