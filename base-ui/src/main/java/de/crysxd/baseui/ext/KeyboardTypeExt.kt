package de.crysxd.baseui.ext

import android.text.InputType
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import de.crysxd.octoapp.menu.InputMenuItem

val InputMenuItem.KeyboardType.inputType
    get() = when (this) {
        InputMenuItem.KeyboardType.Normal -> InputType.TYPE_CLASS_TEXT
        InputMenuItem.KeyboardType.Numbers -> InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        InputMenuItem.KeyboardType.NumbersNegative -> InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        InputMenuItem.KeyboardType.Url -> InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_URI
    }

val InputMenuItem.KeyboardType.options
    get() = KeyboardOptions(
        capitalization = if (this == InputMenuItem.KeyboardType.Normal) KeyboardCapitalization.Sentences else KeyboardCapitalization.None,
        autoCorrect = this == InputMenuItem.KeyboardType.Normal,
        keyboardType = when (this) {
            InputMenuItem.KeyboardType.Normal -> KeyboardType.Text
            InputMenuItem.KeyboardType.Numbers -> KeyboardType.Decimal
            InputMenuItem.KeyboardType.NumbersNegative -> KeyboardType.Decimal
            InputMenuItem.KeyboardType.Url -> KeyboardType.Uri
        },
    )