package de.crysxd.baseui

import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.CallSuper
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.doOnLayout
import androidx.core.view.updatePadding
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.crysxd.baseui.ext.findParent
import de.crysxd.baseui.ext.requireOctoActivity


abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    protected abstract val viewModel: BaseViewModel

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.navContoller = findNavController()
        requireOctoActivity().observeErrorEvents(viewModel.errorLiveData)
        requireOctoActivity().observerMessageEvents(viewModel.messages)
        view.setBackgroundResource(R.drawable.bg_bottom_sheet)


    }

    override fun onStart() {
        super.onStart()

        // Fix bottom sheet not fully shown on tablet in landscape
        forceResizeBottomSheet()

        // Limit bottom sheet width to get a pleasant look on tablets
        withTouchOutsideView {
            it.doOnLayout { _ ->
                val maxWidth = requireContext().resources.getDimension(R.dimen.max_bottom_sheet_width).toInt()
                dialog?.window?.setLayout(it.width.coerceAtMost(maxWidth), ViewGroup.LayoutParams.MATCH_PARENT)
            }
        }

    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = super.onCreateDialog(savedInstanceState).also {
        it.window?.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                attributes.blurBehindRadius = 10
                addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND)
            }
        }
    }

    fun forceResizeBottomSheet() {
        // This is a fix for tablets in landscape mode not fully showing a bottom sheet
        // We manually set the peek height to make sure everything is shown
        withBottomSheet { coordinator, bottomSheet ->
            val behaviour = BottomSheetBehavior.from(bottomSheet)

            bottomSheet.doOnLayout { _ ->
                // If the height of the bottom sheet matches or exceeds the height of the dimmed background view,
                // we need to manually set the top padding to accompany the status bar. By default, the content
                // goes behind the status bar at first and then jumps down when scrolled
                withTouchOutsideView { container ->
                    when {
                        container.height == 0 -> Unit
                        bottomSheet.height >= container.height -> requireOctoActivity().applyInsetsToView(bottomSheet)
                        else -> bottomSheet.updatePadding(top = 0)
                    }
                }

                // Enforce peek height®
                behaviour.peekHeight = bottomSheet.height
                coordinator.parent.requestLayout()
            }
        }
    }

    private fun <T> withBottomSheet(block: (CoordinatorLayout, View) -> T) =
        requireView().findParent<CoordinatorLayout>()?.let { coordinatorLayout ->
            coordinatorLayout.findViewById<View>(R.id.design_bottom_sheet)?.let {
                block(coordinatorLayout, it)
            }
        }

    private fun <T> withTouchOutsideView(block: (View) -> T) =
        requireView().findParent<CoordinatorLayout>()?.findViewById<View>(R.id.touch_outside)?.let(block)

    fun getScreenCenter(): Int = withBottomSheet { _, view ->
        (view.x + view.width / 2).toInt()
    } ?: 0
}