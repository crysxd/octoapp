package de.crysxd.baseui.common.controls

import androidx.activity.OnBackPressedCallback
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.announcement.AnnouncementControls
import de.crysxd.baseui.compose.controls.bedmesh.BedMeshControls
import de.crysxd.baseui.compose.controls.bedmesh.rememberBedMeshControlsState
import de.crysxd.baseui.compose.controls.cancelobject.CancelObjectControls
import de.crysxd.baseui.compose.controls.cancelobject.rememberCancelObjectState
import de.crysxd.baseui.compose.controls.connect.ConnectionControls
import de.crysxd.baseui.compose.controls.connect.rememberConnectionControlsState
import de.crysxd.baseui.compose.controls.executegcode.ExecuteGcodeControls
import de.crysxd.baseui.compose.controls.executegcode.rememberExecuteGcodeControlsState
import de.crysxd.baseui.compose.controls.extrude.ExtrudeControls
import de.crysxd.baseui.compose.controls.extrude.rememberExtrudeControlsState
import de.crysxd.baseui.compose.controls.gcodepreview.GcodeControls
import de.crysxd.baseui.compose.controls.gcodepreview.rememberGcodeControlsState
import de.crysxd.baseui.compose.controls.movetool.MoveToolControls
import de.crysxd.baseui.compose.controls.movetool.rememberMoveToolControlState
import de.crysxd.baseui.compose.controls.multitool.MultiToolControls
import de.crysxd.baseui.compose.controls.multitool.rememberMultiToolControlState
import de.crysxd.baseui.compose.controls.printconfidence.rememberPrintConfidenceState
import de.crysxd.baseui.compose.controls.progress.ProgressControls
import de.crysxd.baseui.compose.controls.progress.rememberProgressControlsState
import de.crysxd.baseui.compose.controls.quickaccess.QuickAccessControls
import de.crysxd.baseui.compose.controls.quickaccess.rememberQuickAccessState
import de.crysxd.baseui.compose.controls.quickprint.QuickPrintControls
import de.crysxd.baseui.compose.controls.quickprint.rememberQuickPrintState
import de.crysxd.baseui.compose.controls.saveconfig.SaveConfigControls
import de.crysxd.baseui.compose.controls.saveconfig.rememberSaveConfigState
import de.crysxd.baseui.compose.controls.temperature.TemperatureControls
import de.crysxd.baseui.compose.controls.temperature.rememberTemperatureControlsState
import de.crysxd.baseui.compose.controls.tune.TuneControls
import de.crysxd.baseui.compose.controls.tune.rememberTuneControlsState
import de.crysxd.baseui.compose.controls.webcam.WebcamActions
import de.crysxd.baseui.compose.controls.webcam.rememberWebcamControlsState
import de.crysxd.baseui.compose.framework.components.OctoDialog
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.data.models.ControlType
import de.crysxd.octoapp.base.data.models.ControlsPreferences
import de.crysxd.octoapp.base.data.repository.asDestinationId
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.engine.models.printer.PrinterState
import java.util.Collections

@Composable
fun ControlsView(
    state: ControlsState,
) = ControlsBottomBarColumn {
    var showExplainerDialog by remember { mutableStateOf(false) }
    val destination = state.flags.asDestinationId()
    val types = remember(destination, state.preferences) {
        val p = state.preferences[destination] ?: ControlsPreferences(destination)
        p.prepare(controlTypes[state.flags.asState()] ?: emptyList()).toList()
    }

    if (showExplainerDialog) {
        OctoDialog(
            title = null,
            message = stringResource(R.string.workspace___explainer).toHtml().asAnnotatedString(),
            onDismissRequest = { showExplainerDialog = false },
        )
    }

    EditToolbar(
        editing = state.editing,
        onStopEditing = {
            state.editing = false
        }
    )

    Box(contentAlignment = Alignment.TopCenter) {
        val connection = remember {
            object : ControlsToolbarConnection {
                override var shouldShow: Boolean by mutableStateOf(true)
            }
        }

        CompositionLocalProvider(LocalControlsToolbarConnection provides connection) {
            ControlsContent(
                editing = state.editing,
                onTypesChanged = state::onTypesChanged,
                types = types,
            )

            ControlsToolbar(
                activeState = state.flags.asState(),
                modifier = Modifier
                    .padding(OctoAppTheme.dimens.margin12)
                    .clip(RoundedCornerShape(50))
                    .clickable { showExplainerDialog = true }
                    .padding(OctoAppTheme.dimens.margin01)
            )
        }
    }
}

//region State
@Composable
fun rememberControlsState(): ControlsState {
    val editing = remember { mutableStateOf(false) }
    var editedControlsSettings by remember { mutableStateOf<List<Pair<ControlType, Boolean>>?>(null) }

    val vmFactory = ControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(factory = vmFactory, modelClass = ControlsViewModel::class.java, key = vmFactory.id)
    val flags = vm.flags.collectAsStateWhileActive(initial = PrinterState.Flags(), key = "controls-flags")
    val destinationId = flags.value.asDestinationId()
    val preferences = vm.getControlsSettings().collectAsState(initial = emptyMap())
    val activity = LocalOctoActivity.current
    val lifecycle = LocalLifecycleOwner.current
    val backPressCallback = remember {
        object : OnBackPressedCallback(false) {
            override fun handleOnBackPressed() {
                // Cancel editing
                editedControlsSettings = null
                editing.value = false
            }
        }
    }

    SideEffect {
        activity?.onBackPressedDispatcher?.addCallback(lifecycle, backPressCallback)
    }

    LaunchedEffect(destinationId) {
        // Cancel editing when destination is changed
        editing.value = false
        editedControlsSettings = null
    }

    LaunchedEffect(editing.value) {
        backPressCallback.isEnabled = editing.value

        // Save if editing ended with changes
        editedControlsSettings.takeUnless { editing.value }?.let { result ->
            vm.saveControlsState(flags.value.asDestinationId(), result)
        }
    }

    return remember {
        object : ControlsState {
            override var editing by editing
            override val preferences by preferences
            override val flags by flags

            override fun onTypesChanged(types: List<Pair<ControlType, Boolean>>) {
                editedControlsSettings = types
            }
        }
    }
}

interface ControlsState {
    var editing: Boolean
    val preferences: Map<String, ControlsPreferences?>
    val flags: PrinterState.Flags

    fun onTypesChanged(types: List<Pair<ControlType, Boolean>>)
}

interface ControlsToolbarConnection {
    var shouldShow: Boolean
}

val LocalControlsToolbarConnection = compositionLocalOf<ControlsToolbarConnection> {
    object : ControlsToolbarConnection {
        override var shouldShow: Boolean = false
    }
}
//endregion

//region Internals
private sealed class State {
    data object Connect : State()
    data object Prepare : State()
    data object Print : State()
}

private fun PrinterState.Flags.asState() = when {
    isPrinting() -> State.Print
    isOperational() -> State.Prepare
    else -> State.Connect
}

private val controlTypes = mapOf(
    State.Connect to listOf(
        ControlType.Connect,
        ControlType.AnnouncementWidget,
        ControlType.WebcamWidget,
        ControlType.QuickAccessWidget,
    ),
    State.Prepare to listOf(
        ControlType.AnnouncementWidget,
        ControlType.SaveConfig,
        ControlType.ControlTemperatureWidget,
        ControlType.WebcamWidget,
        ControlType.MultiTool,
        ControlType.MoveToolWidget,
        ControlType.ExtrudeWidget,
        ControlType.QuickPrint,
        ControlType.SendGcodeWidget,
        ControlType.BedMesh,
        ControlType.QuickAccessWidget,
    ),
    State.Print to listOf(
        ControlType.AnnouncementWidget,
        ControlType.ProgressWidget,
        ControlType.ControlTemperatureWidget,
        ControlType.WebcamWidget,
        ControlType.MoveToolWidget,
        ControlType.GcodePreviewWidget,
        ControlType.CancelObject,
        ControlType.TuneWidget,
        ControlType.MultiTool,
        ControlType.ExtrudeWidget,
        ControlType.SendGcodeWidget,
        ControlType.QuickAccessWidget
    ),
)

@Composable
private fun ControlsToolbar(
    modifier: Modifier = Modifier,
    activeState: State
) = Crossfade(LocalControlsToolbarConnection.current.shouldShow) { shouldShow ->
    if (!shouldShow) return@Crossfade

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        val foreground by animateColorAsState(OctoAppTheme.colors.activeColorScheme)
        val background by animateColorAsState(OctoAppTheme.colors.activeColorSchemeLight)
        val strokeWidth = 2.dp

        ControlsToolbarStep(
            state = State.Connect,
            strokeWidth = strokeWidth,
            activeState = { activeState },
            foreground = { foreground },
            background = { background },
            testTag = TestTags.Controls.StepConnect,
        )

        ControlsToolbarConnector(
            strokeWidth = strokeWidth,
            color = { background }
        )

        ControlsToolbarStep(
            state = State.Prepare,
            strokeWidth = strokeWidth,
            activeState = { activeState },
            foreground = { foreground },
            background = { background },
            testTag = TestTags.Controls.StepPrepare,
        )

        ControlsToolbarConnector(
            strokeWidth = strokeWidth,
            color = { background }
        )

        ControlsToolbarStep(
            state = State.Print,
            strokeWidth = strokeWidth,
            activeState = { activeState },
            foreground = { foreground },
            background = { background },
            testTag = TestTags.Controls.StepPrint,
        )
    }
}

@Composable
private fun ControlsToolbarStep(
    modifier: Modifier = Modifier,
    state: State,
    testTag: String,
    strokeWidth: Dp,
    activeState: () -> State,
    background: () -> Color,
    foreground: () -> Color,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = modifier
        .animateContentSize()
        .drawBehind {
            drawRoundRect(
                color = background(),
                cornerRadius = CornerRadius(size.height / 2, size.height / 2),
                style = Stroke(width = 2.dp.toPx()),
                size = Size(size.width - strokeWidth.toPx(), size.height - strokeWidth.toPx()),
                topLeft = Offset(strokeWidth.toPx() / 2, strokeWidth.toPx() / 2)
            )
        }
        .height(32.dp)
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .drawBehind {
                drawCircle(
                    color = foreground(),
                    style = Stroke(width = 2.dp.toPx()),
                    radius = size.height / 2 - strokeWidth.toPx() / 2,
                )
                drawCircle(
                    color = foreground(),
                    radius = size.height / 2 - strokeWidth.toPx() / 2
                )
            }
            .aspectRatio(1f)
            .fillMaxHeight(),
    ) {
        Text(
            text = when (state) {
                State.Connect -> "1"
                State.Prepare -> "2"
                State.Print -> "3"
            },

            color = OctoAppTheme.colors.textColoredBackground,
            style = OctoAppTheme.typography.base,
        )
    }

    AnimatedVisibility(activeState() == state) {
        Text(
            text = when (state) {
                State.Connect -> stringResource(R.string.workspace___connect)
                State.Prepare -> stringResource(R.string.workspace___prepare)
                State.Print -> stringResource(R.string.workspace___print)
            },
            modifier = Modifier
                .padding(start = OctoAppTheme.dimens.margin01, end = OctoAppTheme.dimens.margin1)
                .testTag(testTag),
            color = OctoAppTheme.colors.normalText,
            style = OctoAppTheme.typography.base
        )
    }
}

@Composable
private fun ControlsToolbarConnector(
    modifier: Modifier = Modifier,
    strokeWidth: Dp,
    color: () -> Color,
) = Box(
    modifier = modifier
        .drawBehind { drawRect(color()) }
        .size(OctoAppTheme.dimens.margin1, strokeWidth)
)


@Composable
private fun ColumnScope.EditToolbar(
    editing: Boolean,
    onStopEditing: () -> Unit,
) = AnimatedVisibility(
    visible = editing,
    modifier = Modifier
        .zIndex(100f)
        .fillMaxWidth(),
) {
    Row(
        horizontalArrangement = Arrangement.End,
        modifier = Modifier
            .shadow(OctoAppTheme.dimens.elementShadow)
            .background(OctoAppTheme.colors.windowBackground)
    ) {
        IconButton(onClick = onStopEditing) {
            Icon(
                painter = painterResource(id = R.drawable.ic_round_check_24),
                contentDescription = stringResource(id = R.string.cd_done),
                tint = OctoAppTheme.colors.primaryButtonBackground
            )
        }
    }
}

@Composable
private fun ControlsContent(
    editing: Boolean,
    types: List<Pair<ControlType, Boolean>>,
    onTypesChanged: (List<Pair<ControlType, Boolean>>) -> Unit,
) = Column {
    var localTypes by remember { mutableStateOf(types) }
    LaunchedEffect(types, editing) { localTypes = types.filter { (_, hidden) -> !hidden || editing } }

    val connectionState = if (types.has(ControlType.Connect, editing = editing)) rememberConnectionControlsState() else null
    val multiToolState = rememberMultiToolControlState()
    val moveToolState = rememberMoveToolControlState()
    val sendGcodeState = if (types.has(ControlType.SendGcodeWidget, editing = editing)) rememberExecuteGcodeControlsState() else null
    val extrudeState = if (types.has(ControlType.ExtrudeWidget, editing = editing)) rememberExtrudeControlsState() else null
    val printConfidenceState = if (types.has(ControlType.WebcamWidget, editing = editing)) rememberPrintConfidenceState() else null
    val progressState = if (types.has(ControlType.ProgressWidget, editing = editing)) rememberProgressControlsState() else null
    val temperatureState = if (types.has(ControlType.ControlTemperatureWidget, editing = editing)) rememberTemperatureControlsState() else null
    val bedMeshState = if (types.has(ControlType.ControlTemperatureWidget, editing = editing)) rememberBedMeshControlsState() else null
    val gcodePreviewState = if (types.has(ControlType.GcodePreviewWidget, editing = editing) || types.has(ControlType.ProgressWidget, editing = editing)) {
        rememberGcodeControlsState()
    } else {
        null
    }
    val webcamState = if (types.has(ControlType.WebcamWidget, editing = editing)) {
        rememberWebcamControlsState(isFullscreen = false)
    } else {
        null
    }
    val tuneState = if (types.has(ControlType.TuneWidget, editing = editing)) {
        rememberTuneControlsState()
    } else {
        null
    }
    val quickAccessState = rememberQuickAccessState()
    val quickPrintState = if (types.has(ControlType.QuickPrint, editing = editing)) {
        rememberQuickPrintState()
    } else {
        null
    }
    val saveConfigState = if (types.has(ControlType.SaveConfig, editing = editing)) {
        rememberSaveConfigState()
    } else {
        null
    }
    val cancelObjectState = if (types.has(ControlType.CancelObject, editing = editing)) {
        rememberCancelObjectState()
    } else {
        null
    }

    ControlsLayout(
        editing = editing,
        onMove = { from, to ->
            // Find index in our list as AnnouncementControls throw the from.index and to.index off
            val fromIndex = localTypes.indexOfFirst { it.first == from.key }.takeIf { it >= 0 } ?: return@ControlsLayout
            val toIndex = localTypes.indexOfFirst { it.first == to.key }.takeIf { it >= 0 } ?: return@ControlsLayout
            localTypes = localTypes.toMutableList().also { Collections.swap(it, fromIndex, toIndex) }
            onTypesChanged(localTypes)
        },
        items = localTypes,
        toggleHidden = { type ->
            localTypes = localTypes.map { it.first to (if (it.first == type) !it.second else it.second) }
            onTypesChanged(localTypes)
        },
        headerView = {
            when {
                editing -> Unit
                connectionState != null -> ConnectionControls(state = connectionState)
                else -> Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin5))
            }
        }
    ) { type, editState ->
        when (type) {
            ControlType.ProgressWidget -> progressState?.let { s1 -> ProgressControls(state = { s1 }, editState = editState) }
            ControlType.GcodePreviewWidget -> gcodePreviewState?.let { s -> GcodeControls(state = s, editState = editState) }
            ControlType.WebcamWidget -> webcamState?.let { s -> WebcamActions(state = s, editState = editState, confidenceState = printConfidenceState) }
            ControlType.ControlTemperatureWidget -> temperatureState?.let { s -> TemperatureControls(state = s, editState = editState) }
            ControlType.TuneWidget -> tuneState?.let { s -> TuneControls(state = tuneState, editState = editState) }
            ControlType.SendGcodeWidget -> sendGcodeState?.let { s -> ExecuteGcodeControls(state = s, editState = editState) }
            ControlType.ExtrudeWidget -> extrudeState?.let { s -> ExtrudeControls(state = s, editState = editState) }
            ControlType.MoveToolWidget -> MoveToolControls(state = moveToolState, editState = editState)
            ControlType.QuickAccessWidget -> QuickAccessControls(state = quickAccessState, editState = editState)
            ControlType.AnnouncementWidget -> AnnouncementControls(editState = editState)
            ControlType.QuickPrint -> quickPrintState?.let { s -> QuickPrintControls(state = s, editState = editState) }
            ControlType.CancelObject -> cancelObjectState?.let { s -> CancelObjectControls(state = s, editState = editState) }
            ControlType.MultiTool -> MultiToolControls(state = multiToolState, editState = editState)
            ControlType.BedMesh -> bedMeshState?.let { BedMeshControls(state = bedMeshState, editState = editState) }
            ControlType.SaveConfig -> saveConfigState?.let { SaveConfigControls(state = saveConfigState, editState = editState) }
            else -> Unit
        }
    }
}

private fun List<Pair<ControlType, Boolean>>.has(t: ControlType, editing: Boolean) = any { (type, hidden) -> type == t && (editing || !hidden) }
//endregion

