package de.crysxd.baseui.common.controls

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.ControlType
import de.crysxd.octoapp.base.data.models.ControlsPreferences
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.EmergencyStopUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class ControlsViewModel(
    printerEngineProvider: PrinterEngineProvider,
    private val togglePausePrintJobUseCase: TogglePausePrintJobUseCase,
    private val cancelPrintJobUseCase: CancelPrintJobUseCase,
    private val emergencyStopUseCase: EmergencyStopUseCase,
    private val controlsPreferencesRepository: ControlsPreferencesRepository,
    private val instanceId: String,
    private val octoPreferences: OctoPreferences,
) : ViewModel() {

    val currentMessage = printerEngineProvider
        .passiveCurrentMessageFlow(tag = "controls-current-message", instanceId = instanceId)
//        .rateLimit(rateMs = 500)

    val flags = printerEngineProvider
        .passiveCurrentMessageFlow(tag = "controls-flags", instanceId = instanceId)
        .map { it.state.flags }
        .distinctUntilChanged()

    val emergencyStopInBottomBar = octoPreferences.updatedFlow2.map {
        it.emergencyStopInBottomBar
    }.distinctUntilChanged()

    val swipeConfirmations = octoPreferences.updatedFlow2.map {
        it.swipeConfirmations
    }.distinctUntilChanged()

    fun getControlsSettings() = combine(
        controlsPreferencesRepository.getWidgetOrderFlow(ControlsPreferencesRepository.LIST_CONNECT),
        controlsPreferencesRepository.getWidgetOrderFlow(ControlsPreferencesRepository.LIST_PREPARE),
        controlsPreferencesRepository.getWidgetOrderFlow(ControlsPreferencesRepository.LIST_PRINT),
    ) { connect, prepare, print ->
        mapOf(
            ControlsPreferencesRepository.LIST_CONNECT to connect,
            ControlsPreferencesRepository.LIST_PREPARE to prepare,
            ControlsPreferencesRepository.LIST_PRINT to print,
        )
    }

    fun togglePausePrint() = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        togglePausePrintJobUseCase.execute(TogglePausePrintJobUseCase.Params(instanceId = instanceId))
    }

    fun cancelPrint() = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        cancelPrintJobUseCase.execute(CancelPrintJobUseCase.Params(restoreTemperatures = false, instanceId = instanceId))
    }

    fun emergencyStop() = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        emergencyStopUseCase.execute(EmergencyStopUseCase.Params(instanceId = instanceId))
    }

    fun saveControlsState(listId: String, result: List<Pair<ControlType, Boolean>>) {
        val new = ControlsPreferences(listId, result)
        controlsPreferencesRepository.setWidgetOrder(listId, new)
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ControlsViewModel(
            instanceId = instanceId,
            printerEngineProvider = BaseInjector.get().octoPrintProvider(),
            togglePausePrintJobUseCase = BaseInjector.get().togglePausePrintJobUseCase(),
            cancelPrintJobUseCase = BaseInjector.get().cancelPrintJobUseCase(),
            octoPreferences = BaseInjector.get().octoPreferences(),
            controlsPreferencesRepository = BaseInjector.get().widgetPreferencesRepository(),
            emergencyStopUseCase = BaseInjector.get().emergencyStopUseCase(),
        ) as T
    }
}