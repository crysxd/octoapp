package de.crysxd.baseui.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import de.crysxd.octoapp.base.ext.WhileSubscribedOcto
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn

class NetworkStateProvider(coroutineScope: CoroutineScope, context: Context) {

    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            updateNetworkState()
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            updateNetworkState()
        }
    }

    private val mutableNetworkState = MutableStateFlow<NetworkState>(NetworkState.Unknown)
    val networkState = mutableNetworkState.onStart {
        connectivityManager.registerDefaultNetworkCallback(networkCallback)
        updateNetworkState()
    }.onCompletion {
        connectivityManager.unregisterNetworkCallback(networkCallback)
    }.shareIn(coroutineScope, SharingStarted.WhileSubscribedOcto)

    private fun updateNetworkState() {
        val network = connectivityManager.activeNetwork
        val wifiConnected = if (network != null) {
            val capabilities = connectivityManager.getNetworkCapabilities(network)
            capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
        } else {
            false
        }
        Napier.i(tag = "NetworkStateProvider", message = "Update network state: $wifiConnected")
        mutableNetworkState.value = if (wifiConnected) NetworkState.WifiConnected else NetworkState.WifiNotConnected
    }

    sealed class NetworkState {
        data object Unknown : NetworkState()
        data object WifiConnected : NetworkState()
        data object WifiNotConnected : NetworkState()
    }
}