package de.crysxd.baseui.common.gcode

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.gcode.GcodeRenderContextFactory
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.viewmodels.ext.printBed
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlin.math.min

@Suppress("EXPERIMENTAL_API_USAGE")
class GcodePreviewViewModel(
    printerEngineProvider: PrinterEngineProvider,
    printerConfigurationRepository: PrinterConfigurationRepository,
    octoPreferences: OctoPreferences,
    private val gcodeFileRepository: GcodeFileRepository
) : BaseViewModel() {

    private val tag = "GcodePreviewViewModel"

    init {
        Napier.i(tag = tag, message = "New instance")
    }

    private var filePendingToLoad: FileObject.File? = null
    private val gcodeFlow = MutableStateFlow<Flow<GcodeFileDataSource.LoadState>?>(emptyFlow())
    private val contextFactoryFlow = MutableStateFlow<(Message.Current) -> Pair<GcodeRenderContextFactory, Boolean>?> { null }
    private val manualViewStateFlow = MutableStateFlow<ViewState>(ViewState.Loading())
    private val printBedFlow = printerConfigurationRepository.instanceInformationFlow().map { it.printBed }.distinctUntilChanged()

    private val printerProfileFlow = printerConfigurationRepository.instanceInformationFlow().map {
        it?.activeProfile ?: PrinterProfile()
    }

    private val featureEnabledFlow: Flow<Boolean> = BillingManager.isFeatureEnabledFlow(FEATURE_GCODE_PREVIEW).onEach { enabled ->
        if (enabled) {
            filePendingToLoad?.let { file ->
                filePendingToLoad = null
                downloadGcode(file, true)
            }
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private val renderContextFlow: Flow<ViewState> = gcodeFlow.filterNotNull().flatMapLatest { it }
        .combine(printerEngineProvider.passiveCurrentMessageFlow("gcode_preview_1").rateLimit(1000)) { gcodeState, currentMessage ->
            Pair(gcodeState, currentMessage)
        }.combine(contextFactoryFlow) { pair, factory ->
            val (gcodeState, currentMessage) = pair
            val settings = octoPreferences.gcodePreviewSettings

            when (gcodeState) {
                is GcodeFileDataSource.LoadState.Loading -> ViewState.Loading(gcodeState.progress)
                is GcodeFileDataSource.LoadState.FailedLargeFileDownloadRequired -> ViewState.LargeFileDownloadRequired
                is GcodeFileDataSource.LoadState.Failed -> ViewState.Error(gcodeState.exception)
                is GcodeFileDataSource.LoadState.Ready -> {
                    factory(currentMessage)?.let {
                        val (factoryInstance, fromUser) = it
                        ViewState.DataReady(
                            renderContext = factoryInstance.extractMoves(
                                gcode = gcodeState.gcode,
                                includePreviousLayer = settings.showPreviousLayer,
                                includeRemainingCurrentLayer = settings.showCurrentLayer,
                            ),
                            fromUser = fromUser,
                            settings = settings,
                            printBed = GcodeNativeCanvas.Image.PrintBedGeneric,
                        )
                    } ?: ViewState.Loading(1f)
                }
            }
        }.distinctUntilChanged()

    private val internalViewState: Flow<ViewState> =
        combine(featureEnabledFlow, renderContextFlow, printBedFlow, printerProfileFlow) { featureEnabled, s1, printBed, printerProfile ->
            when {
                !featureEnabled -> ViewState.FeatureDisabled(printBed = printBed)
                s1 is ViewState.DataReady -> s1.copy(
                    printBed = printBed,
                    printerProfile = printerProfile,
                    exceedsPrintArea = exceedsPrintArea(printerProfile, s1.renderContext),
                )

                else -> s1
            }
        }.retry(3) {
            Napier.e(tag = tag, message = "Failed", throwable = it)
            delay(500L)
            true
        }.catch { e ->
            emit(ViewState.Error(e))
        }

    val viewState = merge(manualViewStateFlow, internalViewState)
        .asLiveData(viewModelScope.coroutineContext)

    private fun exceedsPrintArea(
        printerProfile: PrinterProfile?,
        context: GcodeRenderContext?,
    ): Boolean {
        context ?: return false
        printerProfile ?: return false
        val bounds = context.gcodeBounds
        bounds ?: return false
        val w = printerProfile.volume.width
        val h = printerProfile.volume.depth

        // Give 5% slack to make sure we don't count homing positions as out of bounds, max 20mm
        val graceDistance = min(minOf(w, h) * 0.05f, 20f)

        val minX = printerProfile.volume.boundingBox.xMin - graceDistance
        val minY = printerProfile.volume.boundingBox.yMin - graceDistance
        val maxX = printerProfile.volume.boundingBox.xMax + graceDistance
        val maxY = printerProfile.volume.boundingBox.yMax + graceDistance

        return bounds.left < minX || bounds.top > maxY || bounds.right > maxX || bounds.bottom < minY
    }

    fun downloadGcode(file: FileObject.File, allowLargeFileDownloads: Boolean) = viewModelScope.launch(coroutineExceptionHandler) {
        Napier.i(tag = tag, message = "Download file: ${file.path}")
        gcodeFlow.value = flowOf(GcodeFileDataSource.LoadState.Loading(0f))

        if (BillingManager.isFeatureEnabled(FEATURE_GCODE_PREVIEW)) {
            gcodeFlow.value = gcodeFileRepository.loadFile(file, allowLargeFileDownloads)
        } else {
            // Feature currently disabled. Store the file to be loaded once the feature got enabled.
            filePendingToLoad = file
        }
    }

    fun useLiveProgress() {
        contextFactoryFlow.value = {
            Pair(
                GcodeRenderContextFactory.ForFileLocation(it.progress?.filepos?.toInt() ?: Int.MAX_VALUE),
                false
            )
        }
    }

    fun useManualProgress(layer: Int, progress: Float) {
        contextFactoryFlow.value = {
            Pair(
                GcodeRenderContextFactory.ForLayerProgress(layerIndex = layer, progress = progress),
                true
            )
        }
    }

    sealed class ViewState {
        data class Loading(val progress: Float = 0f) : ViewState()
        data class FeatureDisabled(val printBed: GcodeNativeCanvas.Image) : ViewState()
        data object LargeFileDownloadRequired : ViewState()
        data class Error(val exception: Throwable) : ViewState()
        data class DataReady(
            val printBed: GcodeNativeCanvas.Image,
            val renderContext: GcodeRenderContext? = null,
            val printerProfile: PrinterProfile? = null,
            val fromUser: Boolean? = null,
            val exceedsPrintArea: Boolean? = null,
            val settings: GcodePreviewSettings,
        ) : ViewState()
    }
}