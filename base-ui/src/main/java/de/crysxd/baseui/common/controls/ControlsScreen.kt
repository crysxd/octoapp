package de.crysxd.baseui.common.controls

import android.graphics.Rect
import android.os.Build
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.BlurEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.compose.ui.zIndex
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoAppInsets
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsPadding
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import de.crysxd.octoapp.sharedcommon.http.ConnectionType.Default
import de.crysxd.octoapp.sharedcommon.http.ConnectionType.DefaultCloud
import de.crysxd.octoapp.sharedcommon.http.ConnectionType.Ngrok
import de.crysxd.octoapp.sharedcommon.http.ConnectionType.Obico
import de.crysxd.octoapp.sharedcommon.http.ConnectionType.OctoEverywhere
import de.crysxd.octoapp.sharedcommon.http.ConnectionType.Tailscale
import de.crysxd.octoapp.viewmodels.OrchestratorViewModelCore
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlin.math.absoluteValue
import kotlin.time.Duration.Companion.seconds

@Composable
fun ControlsScreen(
    modifier: Modifier = Modifier,
    state: ControlsScreenState,
    content: @Composable () -> Unit,
) = ControlCenterContainer(
    modifier = modifier,
    controlCenter = {
        ControlCenter()
    },
    content = {
        ControlsBanner(
            state = state.connectionState
        )

        content()
    }
)

@Composable
fun rememberControlsScreenState(): ControlsScreenState {
    val vm = viewModel(ControlsScreenViewModel::class.java)
    val connection = vm.connectionState.collectAsStateWhileActive(
        key = "controls-connection",
        initial = OrchestratorViewModelCore.ConnectionState(
            eventTime = Clock.System.now(),
            connectionType = Default,
            instanceLabel = LocalOctoPrint.current.label,
            colors = LocalOctoPrint.current.colors,
            showLabelInBanner = false,
        )
    )

    return remember(connection) {
        object : ControlsScreenState {
            override var topInset by mutableIntStateOf(0)
            override val connectionState by connection
        }
    }
}

interface ControlsScreenState {
    var topInset: Int
    val connectionState: OrchestratorViewModelCore.ConnectionState?
}

interface ControlCenter {
    fun dismiss()
    fun show()
}

val LocalControlCenter = compositionLocalOf<ControlCenter?> { null }

@Composable
fun ControlCenterContainer(
    modifier: Modifier = Modifier,
    controlCenter: @Composable () -> Unit,
    content: @Composable () -> Unit,
) {
    val blurSupported = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
    val controlCenterEnabled by remember { BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH) }.collectAsState(false)
    val overlayColors = when {
        !blurSupported -> listOf(OctoAppTheme.colors.windowBackground.copy(0.9f))
        isSystemInDarkTheme() -> listOf(OctoAppTheme.colors.windowBackground.copy(0.66f), Color.White.copy(0.2f))
        else -> listOf(OctoAppTheme.colors.windowBackground.copy(0.66f), Color.Black.copy(0.06f))
    }
    val scope = rememberCoroutineScope()
    val animationSpec = spring<Float>(stiffness = Spring.StiffnessMedium, dampingRatio = Spring.DampingRatioMediumBouncy)
    var initialSwipePosition by rememberSaveable { mutableFloatStateOf(Float.MAX_VALUE) }
    val swipeCompletedDistance = with(LocalDensity.current) { 100.dp.toPx() }
    var swipeStart by remember { mutableFloatStateOf(0f) }
    var size by remember { mutableStateOf(Size.Zero) }
    val animatedOffset = remember { Animatable(initialSwipePosition) }
    val mutex = remember { Mutex() }
    val swipeProgress by remember {
        derivedStateOf {
            if (animatedOffset.value == Float.MAX_VALUE) return@derivedStateOf 0f
            val progress = 1 - (animatedOffset.value.absoluteValue / size.width)
            progress.coerceIn(0f, 1f)
        }
    }
    val controlsScreenScope = remember {
        object : ControlCenter {
            override fun dismiss() {
                scope.launch {
                    mutex.withLock {
                        animatedOffset.animateTo(-size.width)
                        initialSwipePosition = -size.width
                    }
                }
            }

            override fun show() {
                scope.launch {
                    mutex.withLock {
                        animatedOffset.animateTo(targetValue = 0f, animationSpec = animationSpec)
                        initialSwipePosition = 0f
                    }
                }
            }
        }
    }

    Box(
        modifier = modifier
            .fillMaxSize()
            .pointerInput(controlCenterEnabled) {
                if (!controlCenterEnabled) return@pointerInput

                fun endDrag() {
                    val didComplete = (animatedOffset.value - swipeStart).absoluteValue >= swipeCompletedDistance
                    val swipeDir = animatedOffset.value - swipeStart
                    val target = when {
                        didComplete -> when {
                            swipeStart != 0f -> 0f
                            swipeDir > 0 -> size.width
                            else -> -size.width
                        }

                        else -> swipeStart
                    }

                    initialSwipePosition = target
                    scope.launch {
                        animatedOffset.animateTo(
                            targetValue = target,
                            animationSpec = animationSpec
                        )
                    }
                }


                detectHorizontalDragGestures(
                    onDragEnd = ::endDrag,
                    onDragCancel = ::endDrag,
                    onDragStart = { offset ->
                        val start = when {
                            animatedOffset.targetValue == 0f -> 0f
                            offset.x > size.width / 2 -> size.width
                            else -> -size.width
                        }
                        swipeStart = start
                        scope.launch {
                            mutex.withLock {
                                animatedOffset.snapTo(start)
                            }
                        }
                    },
                    onHorizontalDrag = { change, dragAmount ->
                        scope.launch {
                            mutex.withLock {
                                val new = (animatedOffset.targetValue + dragAmount).coerceIn(-size.width, size.width)
                                animatedOffset.snapTo(new)
                            }
                        }
                    },
                )
            }
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .zIndex(Float.MAX_VALUE)
                .fillMaxSize()
                .graphicsLayer { alpha = swipeProgress }
                .offset {
                    IntOffset(x = animatedOffset.value.toInt(), y = 0)
                }
                .onSizeChanged {
                    size = it.toSize()
                }
        ) {
            if (swipeProgress > 0) {
                CompositionLocalProvider(LocalControlCenter provides controlsScreenScope) {
                    controlCenter()
                }
            }
        }

        Column(
            modifier = Modifier
                .drawWithContent {
                    drawContent()
                    overlayColors.forEach {
                        drawRect(it.copy(alpha = it.alpha * swipeProgress))
                    }
                }
                .graphicsLayer {
                    if (blurSupported) {
                        val blur = 15.dp.toPx() * swipeProgress
                        scaleX = 1 - 0.05f * swipeProgress
                        scaleY = scaleX
                        renderEffect = if (blur > 0f) BlurEffect(blur, blur, TileMode.Mirror) else null
                    }
                }
        ) {
            CompositionLocalProvider(LocalControlCenter provides controlsScreenScope) {
                content()
            }
        }
    }
}

@Composable
private fun ControlsBanner(
    modifier: Modifier = Modifier,
    state: OrchestratorViewModelCore.ConnectionState?
) = Column(
    modifier = modifier.heightIn(min = with(LocalDensity.current) { LocalOctoAppInsets.current.top.toDp() }),
) {
    val showLabel = state?.showLabelInBanner == true
    var connectionShown by remember { mutableStateOf(state?.connectionType != Default) }
    val activity = LocalOctoActivity.current

    DisposableEffect(showLabel, connectionShown) {
        activity?.setLightStatusBar(showLabel || connectionShown)
        onDispose { activity?.setLightStatusBar(false) }
    }

    ConnectionState(
        modifier = Modifier.fillMaxWidth(),
        connectionType = state?.connectionType,
        forceShrink = state == null,
        onVisibilityChanged = { connectionShown = it },
    )

    AnimatedVisibility(showLabel) {
        InstanceLabel(
            consumeTopScreenPadding = !connectionShown,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
private fun InstanceLabel(
    modifier: Modifier = Modifier,
    consumeTopScreenPadding: Boolean,
) {
    val topTarget = with(LocalDensity.current) {
        if (consumeTopScreenPadding) LocalOctoAppInsets.current.top.toDp() else 0.dp
    }
    val topPadding by animateDpAsState(topTarget)

    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .background(OctoAppTheme.colors.activeColorScheme)
            .padding(top = topPadding)
            .padding(OctoAppTheme.dimens.margin1)
    ) {
        Text(
            text = LocalOctoPrint.current.label,
            textAlign = TextAlign.Center,
            color = OctoAppTheme.colors.textColoredBackground,
            style = OctoAppTheme.typography.label,
        )
    }
}

@Composable
private fun ConnectionState(
    modifier: Modifier = Modifier,
    connectionType: ConnectionType?,
    forceShrink: Boolean,
    onVisibilityChanged: (Boolean) -> Unit
) {
    var hidden by remember(forceShrink) { mutableStateOf(true) }
    var shrunken by remember(forceShrink) { mutableStateOf(forceShrink) }
    val background by animateColorAsState(connectionType.color)
    var initial by remember { mutableStateOf(true) }

    LaunchedEffect(connectionType) {
        fun performShrinkOrHide() {
            hidden = connectionType == Default
            shrunken = !hidden
            onVisibilityChanged(!hidden)
        }

        // Skip initial "Connected" state
        if (initial && connectionType != null) {
            initial = false
            performShrinkOrHide()
            return@LaunchedEffect
        }

        // Show banner
        onVisibilityChanged(true)
        hidden = false
        shrunken = false

        // Hide after 3s if any connected state
        if (connectionType != null) {
            delay(3.seconds)
            performShrinkOrHide()
        }
    }

    AnimatedContent(
        targetState = Triple(connectionType, hidden, shrunken),
        modifier = modifier
            .drawBehind { drawRect(background) }
    ) { (type, hide, shrink) ->
        if (hide) return@AnimatedContent

        Box(
            modifier = Modifier.octoAppInsetsPadding(top = true)
        ) {
            if (shrink) return@Box

            Row(
                horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01, alignment = Alignment.CenterHorizontally),
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .padding(OctoAppTheme.dimens.margin12)
                    .fillMaxWidth()
            ) {

                type.Decoration()

                Text(
                    text = type.text,
                    color = OctoAppTheme.colors.textColoredBackground,
                    style = OctoAppTheme.typography.label
                )
            }
        }
    }
}

private val ConnectionType?.color
    @Composable get() = when (this) {
        OctoEverywhere -> colorResource(R.color.octoeverywhere)
        Obico -> colorResource(R.color.obico)
        Ngrok -> colorResource(R.color.ngrok)
        Tailscale -> colorResource(R.color.tailscale)
        Default -> colorResource(R.color.green)
        DefaultCloud -> colorResource(R.color.blue)
        null -> colorResource(R.color.yellow)
    }

private val ConnectionType?.text
    @Composable get() = when (this) {
        OctoEverywhere -> stringResource(R.string.main___banner_connected_via_octoeverywhere)
        Obico -> stringResource(R.string.main___banner_connected_via_spaghetti_detective)
        Ngrok -> stringResource(R.string.main___banner_connected_via_ngrok)
        Tailscale -> stringResource(R.string.main___banner_connected_via_tailscale)
        Default -> stringResource(R.string.main___banner_connected_via_local)
        DefaultCloud -> stringResource(R.string.main___banner_connected_via_alternative)
        null -> stringResource(R.string.main___banner_connection_lost_reconnecting)
    }

@Composable
private fun ConnectionType?.Decoration() {
    val resource = when (this) {
        OctoEverywhere -> R.drawable.ic_octoeverywhere_24px
        Obico -> R.drawable.ic_obico_24px
        Ngrok -> R.drawable.ic_ngrok_24px
        Tailscale -> R.drawable.ic_tailscale_24px
        DefaultCloud -> R.drawable.ic_round_cloud_24
        else -> null
    }

    if (resource != null) {
        Icon(
            tint = Color.Unspecified,
            contentDescription = null,
            painter = painterResource(resource),
            modifier = Modifier.size(24.dp)
        )
    } else if (this == null) {
        CircularProgressIndicator(
            color = colorResource(R.color.text_colored_background),
            modifier = Modifier
                .size(24.dp)
                .padding(4.dp),
            strokeWidth = 2.dp
        )
    }
}

@Composable
@Preview(showSystemUi = true)
fun Preview() = OctoAppThemeForPreview {
    var connection by remember { mutableStateOf<ConnectionType?>(null) }
    val printer = LocalOctoPrint.current

    LaunchedEffect(Unit) {
        while (currentCoroutineContext().isActive) {
            delay(3.seconds)
            connection = ConnectionType.entries.random()
            delay(6.seconds)
            connection = null
        }
    }

    CompositionLocalProvider(LocalOctoAppInsets provides Rect(0, 40, 0, 0)) {
        ControlsScreen(
            state = object : ControlsScreenState {
                override var topInset: Int = 0
                override val connectionState
                    get() = OrchestratorViewModelCore.ConnectionState(
                        eventTime = Clock.System.now(),
                        connectionType = connection,
                        instanceLabel = printer.label,
                        colors = printer.colors,
                        showLabelInBanner = true,
                    )

            },
            content = {
                Text("Content", modifier = Modifier.fillMaxSize())
            }
        )
    }
}