//
//  NotificationService.swift
//  Notifications
//
//  Created by Christian on 06/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import UserNotifications
import OctoAppBase
import WidgetKit

private let tag = "NotificationService"

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override init() {
        ensureInitBase(subsystemSuffix: ".notifications")
    }

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        let mutableContent = bestAttemptContent ?? UNMutableNotificationContent()
        guard let instanceId = mutableContent.userInfo["instanceId"] as? String else {
            Napier.e(tag: tag, message: "Did not receive instance id")
            return contentHandler(mutableContent)
        }
        
        WidgetCenter.shared.reloadAllTimelines()
        
        let params = GetWidgetDataUseCase.Params(
            instanceId: instanceId,
            loadData: true,
            loadSnapshot: true,
            loadThumbnail: false,
            skipCache: true
        )
        
        SharedBaseInjector.shared.get().getWidgetDataUseCase().execute(param: params) { result, e in
            if let error = e {
                Napier.e(tag: tag, message: "Failed to fetch data \(error)")
                contentHandler(mutableContent)
            } else {
                guard let url = (result?.snapshotPath.flatMap { URL(fileURLWithPath: $0) }) else {
                    Napier.e(tag: tag, message: "Unable to handle URL: \(result?.snapshotPath ?? "nil") (\(result?.errors ?? "No errors"))")
                    return contentHandler(mutableContent)
                }
                
                do {
                    let image = try UNNotificationAttachment(identifier: "image", url: url)
                    Napier.i(tag: tag, message: "Attached image")
                    mutableContent.attachments = [image]
                    contentHandler(mutableContent)
                } catch {
                    Napier.e(tag: tag, message: "Failed to attach image \(error)")
                    contentHandler(mutableContent)
                }
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
