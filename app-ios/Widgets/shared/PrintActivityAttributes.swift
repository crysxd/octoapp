//
//  PrintActivityAttributes.swift
//  OctoApp
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import ActivityKit

struct PrintActivityAttributes: ActivityAttributes {
    typealias ContentState = Data

    struct Data: Codable, Hashable {
        let fileName: String
        let state: String
        let progress: Int
        let sourceTime: Int
        let timeLeft: Int?
        let printTime: Int?
    }
    
    enum State : Codable, Hashable {
        case printing
        case completed
        case paused
        case pausedGcode
        case cancelled
        case filamentRequired
        case idle
        case expired
    }
    
    let instanceId: String
    let instanceLabel: String
    let instanceColor: ColorsStruct
    let expiresAt: Date
}

extension PrintActivityAttributes.State {
    var string: String {
        switch self {
        case .printing: return "printing"
        case .completed: return "completed"
        case .paused: return "paused"
        case .pausedGcode: return "pausedGcode"
        case .cancelled: return "cancelled"
        case .filamentRequired: return "filamentRequired"
        case .idle: return "idle"
        case .expired: return "expired"
        }
    }
}

extension PrintActivityAttributes.Data {
    var stateEnum: PrintActivityAttributes.State {
        switch state {
        case "printing": return .printing
        case "completed": return .completed
        case "paused": return .paused
        case "pausedGcode": return .pausedGcode
        case "cancelled": return .cancelled
        case "filamentRequired": return .filamentRequired
        case "idle": return .idle
        case "expired": return .expired
        default: return .printing
        }
    }
}
