//
//  PrintActivityThumbnails.swift
//  OctoApp
//
//  Created by Christian on 01/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import SwiftUI


class PrintAcitivityThumbnailHelper {
    
    private let fileManager = Foundation.FileManager.default

    func storeThumbnail(data: Data, activityId: String) {
        if let path = getThumbnailPath(activityId: activityId)?.path() {
            do {
                // Ensure directory created
                if let directoryUrl = getDirectoryUrl() {
                    if !fileManager.fileExists(atPath: directoryUrl.path()) {
                        print("[LA] Creating directory", path)
                        try fileManager.createDirectory(at: directoryUrl, withIntermediateDirectories: true)
                    }
                }
                
                // Scale image down
                let scaledData = UIImage(data: data)?.resizeImage(targetSize: CGSize(width: 256, height: 256))?.pngData() ?? data
                
                // Write file
                print("[LA] Storing thumbnail in file", path)
                fileManager.createFile(atPath: path, contents: scaledData)
            } catch {
                print("[LA] Failed to write file", error)
            }
        }
    }
    
    func isStoredAlready(activityId: String) -> Bool {
        guard let url = getThumbnailPath(activityId: activityId) else {
            return false
        }
                
        return fileManager.fileExists(atPath: url.path)
    }
    
    func clearStorage(runningActivityIds: [String]) {
        guard let directory = getDirectoryUrl() else {
            return
        }
        
        do {
            for file in try fileManager.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil) {
                if (!runningActivityIds.contains { $0 == file.lastPathComponent }) {
                    print("[LA] Removinf outdated file:", file.lastPathComponent)
                    try? fileManager.removeItem(at: file)
                }
            }
        } catch {
            print("[LA] Failed to clean up folder", error)
        }
    }
    
    private func getDirectoryUrl() -> URL? {
        guard let documentDirectory = fileManager.containerURL(forSecurityApplicationGroupIdentifier: "group.de.crysxd.octoapp") else {
            print("[LA] Failed to get document folder")
            return nil
        }
            
        return documentDirectory.appendingPathComponent("activities")
    }
    
    func getThumbnailPath(activityId: String) -> URL? {
        guard var file = getDirectoryUrl() else {
            return nil
        }
        
        file.appendPathComponent(activityId)
        return file
    }
}

private extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage? {
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
