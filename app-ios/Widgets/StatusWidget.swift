//
//  PrintActivity.swift
//  PrintActivity
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import WidgetKit
import SwiftUI
import Intents
import OctoAppBase
import AppIntents

@available(iOSApplicationExtension 17.0, *)
struct StatusWidgetEntry: TimelineEntry {
    var date: Date = Date()
    var thumbnailPath: String? = nil
    var snapshotPath: String? = nil
    var snapshotSuccess: Bool? = nil
    var usePlaceholderThumbnail: Bool = false
    var status: String = "loading"~
    var printing: Bool = false
    var printEta: Date? = nil
    var completion: Float? = nil
    var configuration: ConfigureStatusWidgetIntent
    var label: String? = nil
    var colors: ColorsStruct? = nil
    var printName: String? = nil
    var errors: String? = nil
    var featureEnabled: Bool = true

    static func preview(usePlaceholderThumbnail: Bool = true, configuration: ConfigureStatusWidgetIntent) -> StatusWidgetEntry {
        StatusWidgetEntry(
            usePlaceholderThumbnail: usePlaceholderThumbnail,
            status: "70%",
            printing: true,
            printEta: Date(milliseconds: 823942342),
            completion: 0.7,
            configuration: configuration,
            label: configuration.printer?.label,
            printName: "Benchy.gcode"
        )
    }
}

@available(iOSApplicationExtension 17.0, *)
private struct StatusWidgetTimelineProvider: AppIntentTimelineProvider {
    typealias Entry = StatusWidgetEntry
    typealias Intent = ConfigureStatusWidgetIntent
    
    func placeholder(in context: Context) -> StatusWidgetEntry {
        return  StatusWidgetEntry.preview(configuration: ConfigureStatusWidgetIntent())
    }
    
    func snapshot(for configuration: ConfigureStatusWidgetIntent, in context: Context) async -> StatusWidgetEntry {
        return await loadSnapshot(configuration: configuration, context: context)
    }
    
    func timeline(for configuration: ConfigureStatusWidgetIntent, in context: Context) async -> Timeline<StatusWidgetEntry> {
        let entry = await loadSnapshot(configuration: configuration, context: context)
        
        // Generate the next refresh point, 20 mins if printing, 60 if idle
        let refresh = Calendar.current.date(byAdding: .minute, value: entry.printing ? 20 : 60, to: Date())!
        return  Timeline(entries: [entry], policy: .after(refresh))
    }
  
    private func loadSnapshot(configuration: ConfigureStatusWidgetIntent, context: Context) async -> StatusWidgetEntry {
        guard await BillingManager.shared.isFeatureEnabledAfterStateCheck() || context.isPreview else {
            Napier.i(tag: "StatusWidget", message: "Feature not enabled, skipping update")
            return StatusWidgetEntry(configuration: configuration, featureEnabled: false)
        }
       
        var selectedId: String
        guard let printer = configuration.printer else {
            return StatusWidgetEntry(configuration: configuration)
        }
        
        do {
            selectedId = try printer.resolveId()
        } catch {
            return StatusWidgetEntry(configuration: configuration)
        }
        
        
        Napier.i(tag: "StatusWidget", message: "Loading for \(selectedId)")
        
        let params = GetWidgetDataUseCase.Params(
            instanceId: selectedId,
            loadData: true,
            loadSnapshot: configuration.showSnapshot == true,
            loadThumbnail: configuration.showThumbnail == true && configuration.showSnapshot != true,
            skipCache: false
        )
        
        do {
            guard let result = try await SharedBaseInjector.shared.get().getWidgetDataUseCase().execute(param: params) else {
                return StatusWidgetEntry(configuration: configuration)
            }
            
            return StatusWidgetEntry(
                thumbnailPath: result.thumbnailPath,
                snapshotPath: result.snapshotPath,
                snapshotSuccess: result.snapshotSuccess,
                status: result.statusText,
                printing: result.state?.flags.isPrinting() == true,
                printEta: result.progress?.printTimeLeft.flatMap {
                    Calendar.current.date(byAdding: .second, value: $0.intValue, to: Date())
                },
                completion: result.progress?.completion?.floatValue,
                configuration: configuration,
                label: result.label,
                colors: result.colors?.toStruct(),
                printName: result.job?.file?.name.split(separator: "/").last.flatMap { String($0) },
                errors: result.errors
            )
        } catch {
            Napier.i(tag: "StatusWidget", message: "Failed to get data: \(error)")
            return StatusWidgetEntry(configuration: configuration)
        }
    }
}

@available(iOSApplicationExtension 17.0, *)
private struct StatusWidgetEntryView : View {
    
    @Environment(\.widgetFamily) var widgetFamily
    @Environment(\.colorScheme) var colorScheme
    @Environment(\.redactionReasons) var redactionReasons
    var entry: StatusWidgetTimelineProvider.Entry
    var placeholder: Bool { redactionReasons.contains(.placeholder) }
    var backgroundAccent: Color {
        ((colorScheme == .dark) ? entry.colors?.dark.main : entry.colors?.light.main)?.opacity(0.08) ?? OctoTheme.colors.inputBackground
    }
    var foregroundAccent: Color {
        ((colorScheme == .dark) ? entry.colors?.dark.main : entry.colors?.light.main) ?? OctoTheme.colors.accent
    }
    
    var body: some View {
        HStack(spacing: 0) {
            ZStack(alignment: .bottomTrailing) {
                HStack(alignment: .top, spacing: 0) {
                    colorStrip
                        .opacity(0)
                    
                    VStack(alignment: .leading) {
                        status
                        eta
                        printInfo
                        date
                    }
                    .invalidatableContent()
                    .padding(widgetPadding)
                    .padding(.top, widgetPadding / 2)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
                    
                   
                }
                
                ButtonsBar(
                    errors: entry.errors,
                    showReload: entry.configuration.showReload
                )
                .padding(widgetPadding)
            }
            
            if widgetFamily != .systemSmall,
               entry.configuration.showThumbnail == true {
                thumbnail
                    .frame(maxHeight: .infinity, alignment: .center)
                    .padding([.top, .trailing, .bottom], 96)
                    .background(backgroundAccent)
                    .padding([.top, .trailing, .bottom], -100)
            }
        }
        .containerBackground(for: .widget) {
            ZStack(alignment: .topLeading) {
                Color.clear
                colorStrip
            }.background {
                ZStack(alignment: entry.configuration.showReload == true ? .topLeading : .topTrailing) {
                   let background = OctoTheme.colors.windowBackground
                    background
                    
                    if widgetFamily == .systemSmall && !placeholder {
                        thumbnail
                            .containerRelativeFrame(.vertical, alignment: .bottomLeading) { size, _ in size * 0.8 }
                            .offset(x: entry.thumbnailPath != nil ? 30 : 0, y: 50)
                        
                        Rectangle()
                            .fill(Gradient(colors: [background, background.opacity(0)]))
                            .frame(maxWidth: .infinity, alignment: .top)
                            .containerRelativeFrame(.vertical, alignment: .top) { size, _ in size * 0.66 }
                            .offset(x: 0, y: 30)
                        
                    }
                }
            }
        }
    }
    
    @ViewBuilder
    var thumbnail: some View {
        if entry.configuration.showColor == true {
            if entry.usePlaceholderThumbnail {
                Image("Image")
                    .resizable()
                    .scaledToFit()
            } else if let path = entry.snapshotPath,
                      let snapshot = UIImage(contentsOfFile: path) {
                    Image(uiImage: snapshot)
                        .resizable()
                        .scaledToFill()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .aspectRatio(1, contentMode: .fit)
                        .blur(radius: entry.snapshotSuccess == false ? 10 : 0)
                
            } else if let path = entry.thumbnailPath,
                      let thumbnail = UIImage(contentsOfFile: path) {
                Image(uiImage: thumbnail)
                    .resizable()
                    .scaledToFit()
            } else if widgetFamily != .systemSmall {
                ZStack {
                    if placeholder  {
                        Color.clear
                    } else {
                        Image(systemName: "photo")
                    }
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .aspectRatio(1, contentMode: .fill)
                .foregroundColor(foregroundAccent)
            }
        }
    }
    
    
    @ViewBuilder
    var colorStrip: some View {
        if entry.configuration.showColor == true {
            PrinterColorStrip(colors: entry.colors)
        }
    }
    
    @ViewBuilder
    var status: some View {
        Text(entry.status)
            .typographySubtitle()
    }
    
    @ViewBuilder
    var date: some View {
        HStack(spacing: 0) {
            if entry.configuration.showName == true,
               let label = entry.label {
                Text(label.appending(" - "))
            }
            Text(entry.date, style: .time)
            
        }
        .foregroundColor(OctoTheme.colors.lightText)
        .typographyLabelSmall()
    }
    
    @ViewBuilder
    var eta: some View {
        if let printEta = entry.printEta,
           entry.configuration.showPrintEta == true {
            HStack(spacing: 0) {
                Text("progress_widget___eta"~.appending(": "))
                Text(printEta, style: .time)
            }
            .foregroundColor(OctoTheme.colors.lightText)
            .typographyLabelSmall()
        }
    }
    
    @ViewBuilder
    var printInfo: some View {
        if entry.configuration.showPrintName == true,
           let printName = entry.printName {
            Text(printName)
                .foregroundColor(OctoTheme.colors.lightText)
                .typographyLabelSmall()
                .lineLimit(2)
                .truncationMode(.middle)
                .padding(.bottom, 1)
        }
    }
}

@available(iOSApplicationExtension 17.0, *)
struct StatusWidget: Widget {
    let kind: String = "StatusWidget"
    
    var body: some WidgetConfiguration {
        AppIntentConfiguration(
            kind: kind,
            intent: ConfigureStatusWidgetIntent.self,
            provider: StatusWidgetTimelineProvider()
        ) { entry in
            if entry.featureEnabled {
                StatusWidgetEntryView(entry: entry)
                    .widgetURL(printer: entry.configuration.printer)
            } else {
                WidgetDisabled()
            }
        }
        .contentMarginsDisabled()
        .configurationDisplayName("Status")
        .supportedFamilies([.systemSmall, .systemMedium])
    }
}

@available(iOSApplicationExtension 17.0, *)
struct StatusWidget_Previews: PreviewProvider {
    static var previews: some View {
        StatusWidgetEntryView(
            entry: StatusWidgetEntry(
                usePlaceholderThumbnail: false,
                status: "Ready",
                printing: false,
                completion: nil,
                configuration: ConfigureStatusWidgetIntent(
                    printer: nil,
                    showName: true,
                    showPrintEta: true,
                    showThumbnail: true,
                    showColor: true,
                    showReload: true
                ),
                label: "Ender 3",
                colors: previewColorsStruct,
                printName: nil
            )
        )
        .padding(-widgetPadding)
        .previewContext(WidgetPreviewContext(family: .systemSmall))
        .previewDisplayName("Idle")
        
        StatusWidgetEntryView(
            entry: StatusWidgetEntry(
                usePlaceholderThumbnail: true,
                status: "70%",
                printing: true,
                printEta: Date(milliseconds: 3284298234),
                completion: nil,
                configuration: ConfigureStatusWidgetIntent(
                    printer: nil,
                    showName: true,
                    showPrintEta: true,
                    showThumbnail: true,
                    showColor: true,
                    showReload: true
                ),
                label: "Ender 3",
                colors: previewColorsStruct,
                printName: "Benchy.gcode"
            )
        )
        .padding(-widgetPadding)
        .previewContext(WidgetPreviewContext(family: .systemSmall))
        .previewDisplayName("Printing")
        
        StatusWidgetEntryView(
            entry: StatusWidgetEntry(
                usePlaceholderThumbnail: false,
                status: "Ready",
                printing: true,
                completion: nil,
                configuration: ConfigureStatusWidgetIntent(
                    printer: nil,
                    showName: true,
                    showPrintEta: false,
                    showThumbnail: true,
                    showColor: true,
                    showReload: true
                ),
                label: "Ender 3",
                colors: previewColorsStruct,
                printName: "Benchy.gcode"
            )
        )
        .padding(-widgetPadding)
        .previewContext(WidgetPreviewContext(family: .systemSmall))
        .previewDisplayName("Printing / Failed thumb")
        
        StatusWidgetEntryView(
            entry: StatusWidgetEntry(
                usePlaceholderThumbnail: false,
                status: "Ready",
                printing: true,
                completion: nil,
                configuration: ConfigureStatusWidgetIntent(
                    printer: nil,
                    showName: false,
                    showPrintEta: false,
                    showThumbnail: false,
                    showColor: false,
                    showReload: false
                ),
                label: "Ender 3",
                colors: previewColorsStruct,
                printName: "Benchy.gcode"
            )
        )
        .padding(-widgetPadding)
        .previewContext(WidgetPreviewContext(family: .systemSmall))
        .previewDisplayName("Printing / No extras")
        
        StatusWidgetEntryView(
            entry: StatusWidgetEntry(
                usePlaceholderThumbnail: false,
                status: "Update failed",
                printing: false,
                completion: nil,
                configuration: ConfigureStatusWidgetIntent(
                    printer: nil,
                    showName: false,
                    showPrintEta: false,
                    showThumbnail: false,
                    showColor: false,
                    showReload: false
                ),
                label: "Ender 3",
                colors: previewColorsStruct,
                printName: nil
            )
        )
        .padding(-widgetPadding)
        .previewContext(WidgetPreviewContext(family: .systemSmall))
        .previewDisplayName("Failed")
    }
}
