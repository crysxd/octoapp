//
//  PrintActivityLiveActivity.swift
//  PrintActivity
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import ActivityKit
import WidgetKit
import SwiftUI
import OSLog

struct PrintActivityLiveActivity: Widget {
    
    @Environment(\.colorScheme) var colorScheme
    
    var body: some WidgetConfiguration {
        ActivityConfiguration(for: PrintActivityAttributes.self) { context in
            return Notification(context: context)
                .frame(maxWidth: .infinity)
                .activitySystemActionForegroundColor(.primary)
                .widgetURL(context.widgetUrl)
        } dynamicIsland: { context in
            return DynamicIsland {
                DynamicIslandExpandedRegion(.leading) {
                    MinimalState(
                        state: context.state.stateEnum,
                        asText: true
                    )
                    .padding(.leading, OctoTheme.dimens.margin01)
                }
                DynamicIslandExpandedRegion(.trailing) {
                    MinimalProgress(
                        progress: context.state.progress
                    )
                    .padding(.trailing, OctoTheme.dimens.margin01)
                }
                DynamicIslandExpandedRegion(.bottom) {
                    ExpandedDynamicIsland(context: context)
                }
            } compactLeading: {
                MinimalState(
                    state: context.state.stateEnum
                )
            } compactTrailing: {
                MinimalProgress(
                    progress: context.state.progress
                )
            } minimal: {
                MinimalState(
                    state: context.state.stateEnum,
                    progress: context.state.progress
                )
            }
            .keylineTint(.dark == colorScheme ? context.attributes.instanceColor.dark.main : context.attributes.instanceColor.light.main)
            .widgetURL(context.widgetUrl)
        }
    }
}

private struct Notification: View {
    
    var context: ActivityViewContext<PrintActivityAttributes>
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        VStack(spacing: 0) {
            NotificationLatyout {
                LargePresentation(
                    state: context.state,
                    attributes: context.attributes
                )
                .padding(OctoTheme.dimens.margin01)
                
                LargeThumbnail(
                    activityId: context.activityID,
                    state: context.state,
                    attributes: context.attributes
                )
            }
            .padding(OctoTheme.dimens.margin1)

            LargeProgressBar(
                state: context.state,
                attributes: context.attributes
            )
            .padding([.leading, .trailing, .bottom], OctoTheme.dimens.margin1)
        }
        .background(colorScheme == .light ? .white : .black)

    }
}

private struct NotificationLatyout: Layout {
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        return subviews[0].sizeThatFits(proposal)
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        let margin = OctoTheme.dimens.margin1
        let mainWidth = subviews.count > 1 ? bounds.width - bounds.height - margin : bounds.width
        
        subviews[0].place(
            at: p,
            proposal: .init(width: mainWidth, height: bounds.size.height)
        )
        
        if subviews.count > 1 {
            p.x += mainWidth + margin
            subviews[1].place(
                at: p,
                proposal: .init(width: bounds.size.height, height: bounds.size.height)
            )
        }
    }
}


private struct ExpandedDynamicIsland: View {
    
    var context: ActivityViewContext<PrintActivityAttributes>
    
    var body: some View {
        VStack(spacing: 0) {
            SmallProgressBar(
                state: context.state,
                attributes: context.attributes
            )
            .padding(.bottom, OctoTheme.dimens.margin1)
            
            HStack {
                VStack {
                    LargePresentation(
                        state: context.state,
                        attributes: context.attributes
                    )
                    
                    Spacer()
                }
                
                LargeThumbnail(
                    activityId: context.activityID,
                    state: context.state,
                    attributes: context.attributes
                )
            }
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin01)
    }
}

struct PrintActivityLiveActivity_Previews: PreviewProvider {
    static let attributes = PrintActivityAttributes(
        instanceId: "some",
        instanceLabel: "OctoPi",
        instanceColor: previewColorsStruct,
        expiresAt: Date()
    )
    
    static let contentState = PrintActivityAttributes.ContentState(
        fileName: "CE3PRO_TestCube_10mm_Top_Long.gcode",
        state: "printing",
        progress: 51,
        sourceTime: Int(Date(timeIntervalSinceNow: -103).timeIntervalSince1970 * 1000),
        timeLeft: 5632,
        printTime: 4433
    )
    
    static let contentStateExpired = PrintActivityAttributes.ContentState(
        fileName: "CE3PRO_TestCube_10mm_Top.gcode",
        state: "expired",
        progress: 51,
        sourceTime: Int(Date(timeIntervalSinceNow: -103).timeIntervalSince1970 * 1000),
        timeLeft: 5632,
        printTime: 4433
    )

    static var previews: some View {
        attributes
            .previewContext(contentState, viewKind: .dynamicIsland(.compact))
            .previewDisplayName("Island Compact")
        attributes
            .previewContext(contentState, viewKind: .dynamicIsland(.expanded))
            .previewDisplayName("Island Expanded")
        attributes
            .previewContext(contentState, viewKind: .dynamicIsland(.minimal))
            .previewDisplayName("Minimal")
        attributes
            .previewContext(contentState, viewKind: .content)
            .previewDisplayName("Notification")
        attributes
            .previewContext(contentStateExpired, viewKind: .dynamicIsland(.compact))
            .previewDisplayName("Island Compact (Expired)")
        attributes
            .previewContext(contentStateExpired, viewKind: .dynamicIsland(.expanded))
            .previewDisplayName("Island Expanded (Expired)")
        attributes
            .previewContext(contentStateExpired, viewKind: .dynamicIsland(.minimal))
            .previewDisplayName("Minimal (Expired)")
        attributes
            .previewContext(contentStateExpired, viewKind: .content)
            .previewDisplayName("Notification (Expired)")
    }
}
