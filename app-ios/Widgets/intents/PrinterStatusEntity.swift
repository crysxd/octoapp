//
//  PrinterStatusEntity.swift
//  OctoApp
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents


struct PrinterStatusEntity: AppEntity {
    let id: String
    
    @Property(title: LocalizedStringResource(stringLiteral: "Status text"))
    var statusText: String
    
    @Property(title: LocalizedStringResource(stringLiteral: "Print completion"))
    var completion: Double?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Print time"))
    var printTime: Int?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Print time left"))
    var printTimeLeft: Int?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Print time left origin"))
    var printTimeLeftOrigin: String?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Position in file"))
    var printFilePos: Int?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Printing"))
    var anyPrinting: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Printing -> Paused"))
    var paused: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Printing -> Active"))
    var printing: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Printing -> Cancelling"))
    var cancelling: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Printing -> Finishing"))
    var finishing: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Printing -> Resuming"))
    var resuming: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Printing -> Starting"))
    var starting: Bool?

    @Property(title: LocalizedStringResource(stringLiteral: "Ready"))
    var ready: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Error"))
    var anyError: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Error -> Generic"))
    var error: Bool?
    
    @Property(title: LocalizedStringResource(stringLiteral: "Error -> Maybe closed"))
    var errorOrClosed: Bool?

    
    static var typeDisplayRepresentation: TypeDisplayRepresentation = TypeDisplayRepresentation(stringLiteral: "Printer status")
    static var defaultQuery = PrinterStatusQuery()
            
    init(result: GetWidgetDataUseCase.Result) {
        self.id = result.label
        self.statusText = result.statusText
        self.completion = result.progress?.completion?.doubleValue
        self.printTime = result.progress?.printTime?.intValue
        self.printTimeLeft = result.progress?.printTimeLeft?.intValue
        self.printTimeLeftOrigin = result.progress?.printTimeLeftOrigin
        self.printFilePos = result.progress?.filepos?.intValue
        self.paused = result.state?.flags.paused
        self.printing = result.state?.flags.printing
        self.cancelling = result.state?.flags.cancelling
        self.error = result.state?.flags.error
        self.errorOrClosed = result.state?.flags.closedOrError
        self.ready = result.state?.flags.ready
        self.finishing = result.state?.flags.finishing
        self.resuming = result.state?.flags.resuming
        self.starting = result.state?.flags.starting
        self.anyPrinting = result.state?.flags.isPrinting()
        self.anyError = result.state?.flags.isError()
    }
    
    var displayRepresentation: DisplayRepresentation {
        DisplayRepresentation(title: "\(statusText)")
    }
}

struct PrinterStatusQuery: EntityQuery {
    
    init() {
        ensureInit()
    }
    
    func entities(for identifiers: [PrinterStatusEntity.ID]) async throws -> [PrinterStatusEntity] {
        throw IntentError(errorDescription: "Not supported, use 'Get printer status'")
    }
}

