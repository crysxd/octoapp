//
//  ConfigureWebcamWidgetIntent.swift
//  OctoApp
//
//  Created by Christian on 06/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents

@available(iOS 17.0, *)
@available(iOSApplicationExtension 17.0, *)
struct ConfigureWebcamWidgetIntent: WidgetConfigurationIntent {
    static var title: LocalizedStringResource = "Configure"
    static var description = IntentDescription("Configure")

    @Parameter(title: LocalizedStringResource("app_widget___settings___printer"))
    var printer: PrinterEntity?

    @Parameter(title: LocalizedStringResource("app_widget___settings___fit_image"), default: false)
    var fitSnapshot: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_status"), default: true)
    var showStatus: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_eta"), default: false)
    var showPrintEta: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_name"), default: false)
    var showName: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_color"), default: true)
    var showColor: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_reload"), default: true)
    var showReload: Bool?

    init(
        printer: PrinterEntity,
        fitSnapshot: Bool,
        showName: Bool,
        showReload: Bool
    ) {
        self.printer = printer
        self.fitSnapshot = fitSnapshot
        self.showName = showName
        self.showReload = showReload
    }

    init() {
    }
}
