//
//  PrinterEntity.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents
import OctoAppBase

struct PrinterEntity: AppEntity {
    let id: String
    let label: String
    let webUrl: String?
    
    static var typeDisplayRepresentation: TypeDisplayRepresentation = TypeDisplayRepresentation(name: LocalizedStringResource("app_widget___settings___printer"))
    static var defaultQuery = PrinterQuery()
    static var syncedId = "synced"
            
    var displayRepresentation: DisplayRepresentation {
        DisplayRepresentation(title: "\(label)", subtitle: webUrl.flatMap { "\($0)" })
    }
    
    func resolveId() throws -> String {
        let activeId = SharedBaseInjector.shared.get().printerConfigRepository.getActiveInstanceSnapshot()?.id
        let synced = id == PrinterEntity.syncedId
        guard let selected = (synced ? activeId : id) else {
            Napier.i(tag: "PrinterEntity", message: "Failed to resolve \(id)")
            throw IntentError(errorDescription: "Unable to resolve printer for id \(id)")
        }
        
        return selected
    }
}

struct PrinterQuery: EntityQuery {
    
    init() {
        ensureInit()
    }
    
    func entities(for identifiers: [PrinterEntity.ID]) async throws -> [PrinterEntity] {
        return try await suggestedEntities().filter {
            identifiers.contains($0.id)
        }
    }
    
    func suggestedEntities() async throws -> [PrinterEntity] {
        let all = SharedBaseInjector.shared.get().printerConfigRepository.getAll().map {
            PrinterEntity(
                id: $0.id,
                label: $0.label,
                webUrl: $0.webUrl.description()
            )
        }
        let synced = [
            PrinterEntity(
                id: PrinterEntity.syncedId,
                label: "app_widget___link_widget__option_synced"~,
                webUrl: nil
            )
        ]
        return synced + all
    }
    
    func defaultResult() async -> PrinterEntity? {
        try? await suggestedEntities().first
    }
}
