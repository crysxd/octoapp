//
//  SendGcodeIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct SendGcodeIntent: AppIntent {
    static var title: LocalizedStringResource = "Send Gcode"
    static var description = IntentDescription("Sends the given Gcode")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    @Parameter(title: "Gcode")
    var gcode: String
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        try await BillingManager.shared.throwIfDisabled()
        
        try await SharedBaseInjector.shared.get().executeGcodeCommandUseCase().execute(
            param: ExecuteGcodeCommandUseCase.Param(
                command: GcodeCommand.Single(command: gcode),
                fromUser: true,
                instanceId: try printer.resolveId(),
                recordTimeoutMs: 0,
                recordResponse: false
            )
        )
        return .result()
    }
}
