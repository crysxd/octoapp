//
//  ReloadWidgetIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents
import WidgetKit
import OctoAppBase

struct ReloadWidgetIntent: AppIntent {
    static var title: LocalizedStringResource = "Reload widgets"
    static var description = IntentDescription("Reload all widgets")
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        GetWidgetDataUseCase.companion.invalidateCache()
        WidgetCenter.shared.reloadAllTimelines()
        return .result()
    }
}
