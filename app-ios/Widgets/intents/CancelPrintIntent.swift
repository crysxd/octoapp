//
//  CancelPrintIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct CancelPrintIntent: AppIntent {
    static var title: LocalizedStringResource = "Cancel print"
    static var description = IntentDescription("Cancels the current print. If the printer is not printing does nothing.")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        try await BillingManager.shared.throwIfDisabled()
        
        try await SharedBaseInjector.shared.get().cancelPrintJobUseCase().execute(
            param: CancelPrintJobUseCase.Params(restoreTemperatures: false, instanceId: try printer.resolveId())
        )
        return .result()
    }
}
