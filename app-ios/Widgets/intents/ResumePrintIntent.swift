//
//  ResumePrintIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents
import OctoAppBase

struct ResumePrintIntent: AppIntent {
    static var title: LocalizedStringResource = "Resume print"
    static var description = IntentDescription("If the printer is paused, resumes the print. If the printer is not paused, does nothing.")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        try await BillingManager.shared.throwIfDisabled()
        
        let paused = try await SharedBaseInjector.shared.get().getWidgetDataUseCase().execute(
            param: GetWidgetDataUseCase.Params(
                instanceId: try printer.resolveId(),
                loadData: true,
                loadSnapshot: false,
                loadThumbnail: false,
                skipCache: true
            )
        )?.state?.flags.paused == true
        
        if paused {
            try await SharedBaseInjector.shared.get().togglePausePrintJobUseCase().execute(
                param: TogglePausePrintJobUseCase.Params(instanceId: try printer.resolveId())
            )
        }
        
        return .result()
    }
}
