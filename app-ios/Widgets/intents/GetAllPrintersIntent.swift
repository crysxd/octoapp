//
//  GetAllPrintersIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct GetAllPrintersIntent: AppIntent {
    static var title: LocalizedStringResource = "List all printers"
    static var description = IntentDescription("List all printers")
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some ReturnsValue<[PrinterEntity]> {
        try await BillingManager.shared.throwIfDisabled()
        
        return .result(
            value: SharedBaseInjector.shared.get().printerConfigRepository.getAll().map {
                PrinterEntity(
                    id: $0.id,
                    label: $0.label,
                    webUrl: $0.webUrl.description()
                )
            }
        )
    }
}
