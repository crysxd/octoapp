//
//  BillingManager+Sugar.swift
//  WidgetsExtension
//
//  Created by Christian on 12/07/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase


private var lastStateCheck = Date(timeIntervalSince1970: 0)

extension BillingManager {
    
    func isFeatureEnabledAfterStateCheck() async -> Bool {
        Napier.i(tag: "BillingManagerSugars", message: "Checking if feature is enabled")
        if lastStateCheck.timeIntervalSinceNow < -30 {
            Napier.i(tag: "BillingManagerSugars", message: "No state check since \(lastStateCheck), checking now...")
            try? await BillingManager.shared.onResume().join()
            lastStateCheck = Date()
            Napier.i(tag: "BillingManagerSugars", message: "State check completed")
        }
        
        let enabled = BillingManager.shared.isFeatureEnabled(feature: BillingManagerKt.FEATURE_INFINITE_WIDGETS)
        Napier.i(tag: "BillingManagerSugars", message: "Checking if feature is enabled -> \(enabled)")
        return enabled
    }
    
    func throwIfDisabled() async throws {
        if await isFeatureEnabledAfterStateCheck() == false {
            throw IntentError(errorDescription: String(format: "supporter_perk___description"~, "Shortcuts"))
        }
    }
}
