//
//  ColorSchemeStruct+Sugar.swift
//  OctoApp
//
//  Created by Christian on 28/10/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import SwiftUI

extension ColorSchemeStruct {
    var transparent: Color { main.opacity(0.2) }
}
