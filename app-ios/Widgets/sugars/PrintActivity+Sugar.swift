//
//  File.swift
//  OctoApp
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import ActivityKit
import Foundation
import WidgetKit
import SwiftUI

extension String {
    func loc() -> String {
        return NSLocalizedString(self, comment: "").replacing("%s", with: "%@")
   }
}

extension ActivityViewContext where Attributes == PrintActivityAttributes {
    var widgetUrl: URL? {
        makeWidgetUrl(instanceId: attributes.instanceId)
    }
}

func makeWidgetUrl(instanceId: String? = nil) -> URL? {
    URL(string: "http://app.octoapp.eu/" + (instanceId != nil ? "?activateInstanceId=\(instanceId!)" : ""))
}

extension View {
    func widgetURL(printer: PrinterEntity?) -> some View {
        let id = printer?.id ?? PrinterEntity.syncedId
        let url = id == PrinterEntity.syncedId ? makeWidgetUrl() : makeWidgetUrl(instanceId: id)
        return widgetURL(url)
    }
}

extension PrintActivityAttributes.Data {
    
    var isTerminal: Bool { stateEnum == .cancelled || stateEnum == .completed }

    
    var updatedAtFormatted: String {
        return updatedAt.formatted(date: .omitted, time: .shortened)
    }
    
    var updatedAt: Date {
        Date(timeIntervalSince1970: Double(sourceTime / 1000))
    }
    
    var printingSince: Date? {
        printTime.flatMap {
            Date(timeIntervalSinceNow: -Double($0))
        }
    }
    
    var printedForText: String? {
        guard let since = printingSince else { return "printing since nil" }
        let duration = Calendar.current.dateComponents([.hour, .minute], from: since, to: updatedAt)
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .brief
        guard let formatted = formatter.string(from: duration) else { return "format nil" }
        return String(format: "app_widget___printed_for".loc(), formatted)
    }
   
    var etaText: String {
        if stateEnum == .completed || stateEnum == .cancelled {
            let time = Date(timeIntervalSince1970: Double(sourceTime / 1000))
                .formatted(date: .omitted, time: .shortened)
            
            return String(format: "app_widget___ready_since".loc(), time)
        } else if let e = timeLeft {
            let etaDate = Date(timeIntervalSinceNow: Double(e))
            var etaFormatted = etaDate.formatted(date: .numeric, time: .shortened)
            
            if Calendar.current.isDateInToday(etaDate) {
                etaFormatted = Date(timeIntervalSinceNow: Double(e))
                    .formatted(date: .omitted, time: .shortened)
            }
            
            return String(format: "app_widget___ready_at".loc(), etaFormatted)
        } else {
            return ""
        }
    }
}

extension PrintActivityAttributes.State {
        
    var label: String {
        switch self {
        case .printing: return "app_widget___printing_title".loc()
        case .paused: return "app_widget___paused_title".loc()
        case .pausedGcode: return "app_widget___paused_by_gcode_title".loc()
        case .completed: return "app_widget___completed_title".loc()
        case .filamentRequired: return "app_widget___filament_required_title".loc()
        case .cancelled: return "app_widget___cancelled_title".loc()
        case .expired: return " "
        case .idle: return " "
        }
    }
    
    var iconSystemName: String? {
        switch self {
        case .printing: return "play.fill"
        case .paused, .pausedGcode: return "pause.fill"
        case .filamentRequired: return "exclamationmark.circle.fill"
        case .completed: return "checkmark.circle.fill"
        case .cancelled: return "x.circle.fill"
        case .expired: return "clock.badge.xmark.fill"
        default: return nil
        }
    }
}
