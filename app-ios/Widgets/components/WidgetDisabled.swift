//
//  WidgetDisabled.swift
//  WidgetsExtension
//
//  Created by Christian on 12/07/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

struct WidgetDisabled : View {
    
    var body: some View {
        VStack(spacing: 0) {
            Text(String(format: "supporter_perk___description"~, "Widgets"))
                .typographyBase()
                .multilineTextAlignment(.center)
                .minimumScaleFactor(0.1)
                .padding()
            
            Spacer(minLength: 0)
            
            Link(destination: URL(string: UriLibrary.shared.getPurchaseUri().description())!) {
                Text("support_octoapp"~)
                    .padding(OctoTheme.dimens.margin12)
                    .padding([.leading, .trailing, .bottom], 50)
                    .background(OctoTheme.colors.accent)
                    .padding([.leading, .trailing, .bottom], -50)
                    .foregroundColor(OctoTheme.colors.textColoredBackground)
                    .typographyButton(small: true)
                    .minimumScaleFactor(0.1)
                    .lineLimit(1)
                    .frame(maxWidth: .infinity)
            }
        }
    }
}
