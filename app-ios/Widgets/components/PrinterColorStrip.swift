//
//  PrinterColorStrip.swift
//  OctoApp
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import SwiftUI

struct PrinterColorStrip : View {
    
    var colors: ColorsStruct?
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        ((colorScheme == .dark ? colors?.dark : colors?.light)?.main ?? OctoTheme.colors.lightGrey)
            .frame(maxHeight: .infinity)
            .frame(width: widgetPadding / 2)
    }
}
