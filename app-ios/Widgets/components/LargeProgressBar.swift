//
//  LargeProgressBar.swift
//  WidgetExtension
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import ActivityKit
import SwiftUI

struct LargeProgressBar : View {
    
    @Environment(\.colorScheme) var colorScheme
    var state: PrintActivityAttributes.Data
    var attributes: PrintActivityAttributes
    private let trailingMin: Double = 0.5
    private var progress: Double {
        Double(state.progress) / 100
    }
    private var alignment: Alignment {
        return progress >= trailingMin ? .trailing : .leading
    }
    
    var body: some View {
        let color = .dark == colorScheme ? attributes.instanceColor.dark : attributes.instanceColor.light

        ControlsProgressBarLayout(progress: progress, trailingMin: trailingMin) {
            color.main.clipShape(Capsule())
            
            Text(state.stateEnum == .printing || state.stateEnum == .expired ? "\(Int(state.progress).formatted())%" : state.stateEnum.label)
                .foregroundColor(alignment == .leading ? .primary : OctoTheme.colors.textColoredBackground)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: alignment)
                .padding([.leading, .trailing], OctoTheme.dimens.margin1)
        }
        .frame(maxWidth: .infinity)
        .frame(height: 28)
        .background(color.transparent)
        .clipShape(Capsule())
    }
}

