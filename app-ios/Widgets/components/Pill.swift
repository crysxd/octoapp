//
//  Pill.swift
//  WidgetsExtension
//
//  Created by Christian on 04/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

@available(iOSApplicationExtension 17.0, *)
struct Pill<Content: View>: View {
    
    var colorStroke: Color?
    @ViewBuilder var content: () -> Content
    
    var body: some View {
        HStack(spacing: 0) {
           content()
        }
        .foregroundColor(OctoTheme.colors.white)
        .typographyLabelSmall()
        .frame(alignment: .bottomLeading)
        .padding(.vertical, 4)
        .padding(.horizontal, 8)
        .background(Capsule().stroke(colorStroke ?? Color.white.opacity(0), lineWidth: 2))
        .background(Capsule().fill(.thinMaterial))
        .fixedSize()
        .invalidatableContent()
    }
}
