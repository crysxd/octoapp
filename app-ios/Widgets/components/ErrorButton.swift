//
//  ReloadButton.swift
//  WidgetsExtension
//
//  Created by Christian on 04/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents
import SwiftUI
import OctoAppBase

@available(iOSApplicationExtension 17.0, *)
struct ErrorButton : View {
    
    var tint: Color = OctoTheme.colors.darkText
    var error: String
    
    @Environment(\.openURL) var openURL
    
    var body: some View {
        SimpleButton(
            deeplink: "https://app.octoapp.eu?dialogText=\(error)&dialogTitle=Widget error",
            iconSystemName: "exclamationmark.circle",
            tint: tint
        )
    }
}
