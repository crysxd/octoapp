//
//  MinimalData.swift
//  WidgetExtension
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct MinimalProgress: View {
    
    var progress: Int
    
    var body: some View {
        Text("\(Int(progress).formatted())%")
            .font(.system(size: 16, weight: .medium))
    }
}

struct MinimalState: View {
    
    var state: PrintActivityAttributes.State
    var asText: Bool = false
    var progress: Int? = nil
    
    var body: some View {
        if asText {
            Text(state.label)
                .font(.system(size: 16, weight: .medium))
        } else if let progress = progress {
            ZStack {
                Circle()
                    .stroke(
                        Color.white.opacity(0.3),
                        lineWidth: 4
                    )
                Circle()
                    .trim(from: 0, to: Double(progress) / 100) // 1
                    .stroke(Color.white, lineWidth: 4)
                    .rotationEffect(.degrees(-90))
                
                if let icon = state.iconSystemName {
                    Image(systemName: icon)
                        .font(.system(size: 10))
                }
            }
            .frame(width: 24, height: 24)
          
        } else if let icon = state.iconSystemName {
            Image(systemName: icon)
        }
    }
}

struct Arc: Shape {
    var startAngle: Angle
    var endAngle: Angle
    var clockwise: Bool

    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.addArc(center: CGPoint(x: rect.midX, y: rect.midY), radius: rect.width / 2, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)

        return path
    }
}

//struct MinimalData_Previews: PreviewProvider {
//    static var previews: some View {
//        MinimalData()
//    }
//}
