//
//  LargeThumbnail.swift
//  WidgetExtension
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI


struct LargeThumbnail: View {
    
    var activityId: String
    var state: PrintActivityAttributes.Data
    var attributes: PrintActivityAttributes
    
    private let helper = PrintAcitivityThumbnailHelper()
    @Environment(\.colorScheme) private var colorScheme
    private var backgroundColor: Color {
        colorScheme == .dark ? .white.opacity(0.1) : .black.opacity(0.02)
    }
    
    var body: some View {
        if let url =  helper.getThumbnailPath(activityId: activityId) {
            if let image = UIImage(contentsOfFile: url.path) {
                ZStack {
                    backgroundColor
                        .clipShape(RoundedRectangle(cornerRadius: 15))
                    
                    Image(uiImage: image)
                        .resizable()
                        .padding(OctoTheme.dimens.margin01)
                        .id(activityId)
                }
                .aspectRatio(1, contentMode: .fit)
                .frame(maxHeight: .infinity)
                .fixedSize(horizontal: false, vertical: false)

            }
        }
    }
}

//struct LargeThumbnail_Previews: PreviewProvider {
//    static var previews: some View {
//        LargeThumbnail()
//    }
//}
