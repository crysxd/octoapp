//
//  ReloadButton.swift
//  WidgetsExtension
//
//  Created by Christian on 04/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents
import SwiftUI
import OctoAppBase

@available(iOSApplicationExtension 17.0, *)
struct SimpleButton : View {
    
    var intent: (any AppIntent)? = nil
    var deeplink: String? = nil
    var iconSystemName: String
    var tint: Color = OctoTheme.colors.darkText
    @Environment(\.redactionReasons) var redactionReasons
    
    var body: some View {
        button
            .frame(alignment: .bottomTrailing)
            .background(Circle().fill(.thinMaterial))
            .fixedSize()
            .tint(.white.opacity(0))
    }
    
    @ViewBuilder
    var button: some View {
        if let intent = intent {
            Button(intent: intent) {
                content
            }
        } else {
            Link(destination: URL(string: deeplink ?? "https://app.octoapp.eu")!) {
                content
                    .padding(8)
            }
        }
    }
    
    var content: some View {
        ZStack {
            if redactionReasons.contains(.placeholder)  {
                Color.clear
            } else {
                Image(systemName:  iconSystemName)
            }
        }
        .frame(width: 24, height: 24)
        .foregroundColor(tint)
    }
}
