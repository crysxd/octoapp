//
//  LargePresenration.swift
//  WidgetExtension
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct LargePresentation: View {
    
    var state: PrintActivityAttributes.Data
    var attributes: PrintActivityAttributes
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading, spacing: 0) {
                fileName
                if state.stateEnum == .expired {
                    expiredTag
                } else {
                    status
                }
                Spacer().frame(maxHeight: OctoTheme.dimens.margin12)
                detail1
                detail2
            }
        }
        .foregroundColor(.primary)
        .typographyLabelSmall()
        .frame(maxWidth: .infinity)
    }
    
    var fileName: some View {
        Text(state.fileName)
            .lineLimit(1)
            .frame(maxWidth: .infinity, alignment: .leading)
            .foregroundColor(.primary)
            .typographySectionHeader()
    }
    
    var status: some View {
        HStack(spacing: 0) {
            Text(
                String(
                    format: "app_widget___on_x".loc(),
                    attributes.instanceLabel
                )
            )
            if !state.isTerminal {
                Text(", ")
                Text(state.updatedAt, style: .time)
            }
        }
        .opacity(0.8)
    }
    
    @ViewBuilder
    var detail1: some View {
        if state.stateEnum == .expired {
            Text(String(format: "app_widget___expired_at".loc(), state.updatedAtFormatted))
        } else if state.isTerminal, let text = state.printedForText {
            Text(text)
              
        } else if let since = state.printingSince {
            HStack(spacing: 0) {
                Text("app_widget___printig_for".loc())
                Text(" ")
                Text(since, style: .relative)
            }
        } else {
            Text(" ")
        }
    }
    
    var detail2: some View {
        Text(state.etaText)
    }
    
    var expiredTag: some View {
        Text("app_widget___expired".loc())
            .multilineTextAlignment(.trailing)
            .foregroundColor(.red)
    }
}

//struct LargePresenration_Previews: PreviewProvider {
//    static var previews: some View {
//        LargePresenration()
//    }
//}
