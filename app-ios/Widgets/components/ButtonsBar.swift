//
//  ButtonsBar.swift
//  WidgetsExtension
//
//  Created by Christian on 06/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import SwiftUI

@available(iOSApplicationExtension 17.0, *)
struct ButtonsBar : View {
    
    var errors: String?
    var showReload: Bool?
    
    var body: some View {
        HStack(spacing: 4) {
            if let errors = errors {
                ErrorButton(error: errors)
            }
            
            if showReload == true {
                ReloadButton()
            }
        }
    }
}
