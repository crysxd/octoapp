import SwiftUI
import OctoAppBase
import FirebaseCore
import FirebaseRemoteConfig
import FirebaseCrashlytics
import FirebaseAnalytics
import FirebaseMessaging
import UserNotifications
import Combine
import os.log

class AppDelegate: NSObject, UIApplicationDelegate {
    
    private let tag = "AppDelegate"
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        Napier.i(tag: "AppDelegate", message: "Opening URL: \(url)")
        return true
    }
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil
    ) -> Bool {
        ensureInitBase(subsystemSuffix: "")
        
        // Logging
        CachedAntiLog.shared.setUpDiskCache()

        // Firebase
        FirebaseApp.configure()
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        let preferences = SharedBaseInjector.shared.get().preferences
        let crashlytics = Crashlytics.crashlytics()
        Analytics.setAnalyticsCollectionEnabled(preferences.isAnalyticsEnabled)
#if DEBUG
        crashlytics.setCrashlyticsCollectionEnabled(false)
#else
        crashlytics.setCrashlyticsCollectionEnabled(preferences.isCrashReportingEnabled)
        if preferences.isCrashReportingEnabled {
            crashlytics.checkForUnsentReports { hasReports in
                Napier.i(tag: "Crashlytics", message: "Has unsend reports: \(hasReports)")
                crashlytics.sendUnsentReports()
            }
            
            DarwinAntilog.shared.addLogger  { level, tag, message in
                crashlytics.log("[\(level)] \(tag): \(message)")
            }
        }
#endif
        settings.minimumFetchInterval = debug ? 0 : 86400
        remoteConfig.configSettings = settings
        OctoConfig.shared.link(rc: DarwinRemoteConfigImpl(remoteConfig: remoteConfig))
        
        // Notifications
        UIApplication.shared.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        OctoPushMessaging.shared.getPushTokenAdapter = { callback in
            Messaging.messaging().token { token, error in
                if let e = error {
                    Napier.e(tag: self.tag, message: "Failed to get token: \(e)")
                    _ = callback(nil)
                } else {
                    Napier.e(tag: self.tag, message: "[Push] Retrieved token: \(token ?? "null")")
                    // We prefix the token with `ios:` for the server to distiguish it
                    _ = callback(token.flatMap { "ios:\($0)" })
                }
            }
        }
        
        // Migrations
        AllSharedMigrations().migrateBlocking()
        
        // Clean up old files
        Task {
            try? await SharedBaseInjector.shared.get()
                .clearPublicFilesUseCase()
                .execute(param: KotlinUnit())
        }

        return true
    }
}

extension AppDelegate : FirebaseMessaging.MessagingDelegate {
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            Messaging.messaging().apnsToken = deviceToken;
        }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Napier.i(tag: tag, message: "[Push] Received new token: \(fcmToken ?? "null")")
    }
}


@main
struct iOSApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    @StateObject var viewWModel = WindowGroupViewModel()
    
    var body: some Scene {
        WindowGroup {
            OrchestratorView()
                .background(OctoTheme.colors.windowBackground)
                .environmentObject(viewWModel)
                .onOpenURL { url in viewWModel.consumeDeeplink(link: url.description) }
                .onContinueUserActivity(NSUserActivityTypeBrowsingWeb) { activity in
                    if let url = activity.webpageURL {
                        viewWModel.consumeDeeplink(link: url.description)
                    }
                }
        }
    }
}

class WindowGroupViewModel: ObservableObject {
    private let core = WindowGroupViewModelCore()
    
    func registerForUnconsumedDeeplink(callback: @escaping (Ktor_httpUrl) -> Void) -> String {
        return core.registerForUnconsumedDeeplink(callback: callback)
    }
    
    func unregisterForUnconsumedDeeplink(token: String) {
        core.unregisterForUnconsumedDeeplink(token: token)
    }
    
    fileprivate func consumeDeeplink(link: String) {
        Task {
            _ = try await core.consumeDeeplink(link: link)
        }
    }
}
