//
//  SystemInfoStruct.swift
//  OctoApp
//
//  Created by Christian on 09/09/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase


struct SystemInfoStruct : Equatable {
    var generatedAt: Date = Date()
    var printerFirmware: String? = nil
    var printerFirmwareFamily: FirmwareFamily = .generic
    var safeMode: Bool = false
    var interfaceType: Interface = .octoPrint
    var capabilities: [Capability] = []
}

extension SystemInfo {
    func toStruct() -> SystemInfoStruct {
        SystemInfoStruct(
            generatedAt: generatedAt.toDate(),
            printerFirmware: printerFirmware,
            printerFirmwareFamily: SystemInfoStruct.FirmwareFamily(firmwareFamily: printerFirmwareFamily),
            safeMode: safeMode,
            interfaceType: SystemInfoStruct.Interface(interface: interfaceType),
            capabilities: capabilities.compactMap { SystemInfoStruct.Capability(capability: $0) }
        )
    }
}

extension SystemInfoStruct {
    enum FirmwareFamily {
        case klipper, generic
    }
    
    enum Capability {
        case timelapse, prettyTerminal, plugins, macro, autoNgrok, temperatureOffset, resetZOffset, realtimestats, absolutetoolheadmove
    }
    
    enum Interface {
        case octoPrint, moonrakerFluidd, moonrakerMainsail, moonrakerMixed
    }
    
   
    
    func toClass() -> SystemInfo {
        SystemInfo(
            generatedAt: generatedAt.toInstant(),
            printerFirmware: printerFirmware,
            printerFirmwareFamily: printerFirmwareFamily.toClass(),
            safeMode: safeMode,
            interfaceType: interfaceType.toClass(),
            capabilities: capabilities.map { $0.toClass() }
        )
    }
}

extension SystemInfoStruct.Interface {
    
    init(interface: SystemInfo.Interface) {
        switch interface {
        case .moonrakerfluidd: self = .moonrakerFluidd
        case .moonrakermixed: self = .moonrakerMixed
        case .moonrakermainsail: self = .moonrakerMainsail
        case .octoprint: self = .octoPrint
        default: self = .octoPrint
        }
    }
    
    func toClass() -> SystemInfo.Interface {
        switch self {
        case .moonrakerFluidd: return .moonrakerfluidd
        case .moonrakerMixed: return .moonrakermixed
        case .moonrakerMainsail: return .moonrakermainsail
        case .octoPrint: return .octoprint
        }
    }
    
    var icon: String {
        switch self {
        case .moonrakerFluidd: return "fluidd_menu"
        case .moonrakerMixed: return "klipper_menu"
        case .moonrakerMainsail: return "mainsail_menu"
        case .octoPrint: return "octoprint_menu"
        }
    }
    
    var label: String {
        toClass().label
    }
}

extension SystemInfoStruct.FirmwareFamily {
    
    init(firmwareFamily: SystemInfo.FirmwareFamily) {
        switch firmwareFamily {
        case .klipper: self = .klipper
        case .generic: self = .generic
        default: self = .generic
        }
    }
    
    func toClass() -> SystemInfo.FirmwareFamily {
        switch self {
        case .klipper: return .klipper
        case .generic: return .generic
        }
    }
}

extension SystemInfoStruct.Capability {
    
    init?(capability: SystemInfo.Capability) {
        switch capability {
        case .plugins: self = .plugins
        case .prettyterminal: self = .prettyTerminal
        case .plugins: self = .plugins
        case .timelapse: self = .timelapse
        case .autongrok: self = .autoNgrok
        case .temperatureoffset: self = .temperatureOffset
        case .resetzoffset: self = .resetZOffset
        case .realtimestats: self = .realtimestats
        case .absolutetoolheadmove: self = .absolutetoolheadmove
        default: return nil
        }
    }
    
    func toClass() -> SystemInfo.Capability {
        switch self {
        case .plugins: return .plugins
        case .prettyTerminal: return .prettyterminal
        case .timelapse: return .timelapse
        case .macro: return .macro
        case .autoNgrok: return .autongrok
        case .temperatureOffset: return .temperatureoffset
        case .resetZOffset: return .resetzoffset
        case .realtimestats: return .realtimestats
        case .absolutetoolheadmove: return .absolutetoolheadmove
        }
    }
}
