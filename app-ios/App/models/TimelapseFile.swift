//
//  TimelapseFile.swift
//  OctoApp
//
//  Created by Christian on 11/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct TimelapseFileStruct: Equatable, Identifiable {
    var id: String { downloadPath ?? name }
    let name: String
    let bytes: Int
    let date: Date?
    let downloadPath: String?
    let thumbnail: String?
    let processing: Bool
    let rendering: Bool
    let recording: Bool
    
    func toClass() -> TimelapseFile {
        return TimelapseFile(
            name: name,
            bytes: Int64(bytes),
            date: date.flatMap { Kotlinx_datetimeInstant.companion.fromEpochMilliseconds(epochMilliseconds: $0.millisecondsSince1970) },
            downloadPath: downloadPath,
            thumbnail: thumbnail,
            processing: processing,
            rendering: rendering,
            recording: recording
        )
    }
}

extension TimelapseFile {
    func toStruct() -> TimelapseFileStruct {
        return TimelapseFileStruct(
            name: name,
            bytes: Int(bytes),
            date: date.flatMap { Date(milliseconds: $0.toEpochMilliseconds())},
            downloadPath: downloadPath,
            thumbnail: thumbnail,
            processing: processing,
            rendering: rendering,
            recording: recording
        )
    }
}
