//
//  File.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct FileObjectStruct: Identifiable, Equatable {
    var id: String { return path }
    
    var origin: FileOriginStruct
    var gcodeDimension: FileGcodeDimensionsStruct?
    var filamentUseMm: [FileGcodeFilamentUse]?
    var estimatedPrintTime: TimeInterval?
    var display: String
    var name: String
    var largeThumbnail: String?
    var mediumThumbnail: String?
    var smallThumbnail: String?
    var date: Date?
    var hash: String?
    var prints: PrintHistoryStruct?
    var path: String
    var size: Int64?
    var childFolders: [FileObjectStruct]?
    var children: [FileObjectStruct]?
    var isFolder: Bool
    var isPrintable: Bool
    var metadata: [FileMetadataGroupStruct] = []
}

struct FileMetadataGroupStruct : Identifiable, Equatable {
    var label: String
    var id: String
    var items: [FileMetadataItemStruct]
}

struct FileMetadataItemStruct : Identifiable, Equatable {
    var label: String
    var id: String
    var value: String
}

struct FileGcodeDimensionsStruct: Equatable {
    var depth: Double?
    var height: Double?
    var width: Double?
}

struct FileGcodeFilamentUse: Equatable {
    var name: String
    var length: Double?
    var volume: Double?
}

enum FileOriginStruct: Identifiable, Equatable {
    var id: String {
        switch(self) {
        case .gcode: return "gcode"
        case .other(name: let name): return name
        }
    }
    var label: String {
        switch(self) {
        case .gcode: return "file_manager___file_details___file_location_local"~
        case .other(name: let name): return name
        }
    }
    case gcode, other(name: String)
}

struct PrintHistoryStruct : Equatable {
    var failure: Int32
    var success: Int32
    var lastPrintSuccess: Bool?
    var lastPrintDate: Date?
}


extension FileReference {
    func toStruct() -> FileObjectStruct {
        let file = self as? FileObject.File
        
        return FileObjectStruct(
            origin: self.origin.toStruct(),
            gcodeDimension: file?.gcodeAnalysis?.dimensions?.toStruct(),
            filamentUseMm: file?.gcodeAnalysis?.toFilmentUseStruct(),
            estimatedPrintTime: file?.gcodeAnalysis?.estimatedPrintTime?.toSecondsInterval(),
            display: self.display,
            name: self.name,
            largeThumbnail: file?.largeThumbnail?.path,
            mediumThumbnail: file?.mediumThumbnail?.path,
            smallThumbnail: file?.smallThumbnail?.path,
            date: file?.date.toDate(),
            hash: file?.hash,
            prints: file?.prints?.toStruct(),
            path: self.path,
            size: file?.size?.int64Value,
            childFolders: (self as? FileReferenceFolder)?.children?.filter { $0 is FileReferenceFolder }.map { $0.toStruct() },
            children: (self as? FileReferenceFolder)?.children?.filter { $0 is FileReferenceFile }.map { $0.toStruct() },
            isFolder: self is FileReferenceFolder,
            isPrintable: file?.typePath.last == "gcode",
            metadata: file?.metadata.map { group in
                FileMetadataGroupStruct(
                    label: group.label,
                    id: group.id,
                    items: group.items.map { item in
                        FileMetadataItemStruct(
                            label: item.label,
                            id: item.id,
                            value: item.value
                        )
                    }
                )
            } ?? []
        )
    }
}

private extension KotlinFloat {
    func toSecondsInterval() -> TimeInterval {
        return TimeInterval(self.doubleValue)
    }
}

extension FileObject.PrintHistory {
    func toStruct() -> PrintHistoryStruct {
        return PrintHistoryStruct(
            failure: self.failure,
            success: self.success,
            lastPrintSuccess: self.last?.success,
            lastPrintDate: self.last?.date.toDate()
        )
    }
}

extension FileOrigin {
    func toStruct() -> FileOriginStruct {
        if self is Gcode {
            return .gcode
        } else if let other = self as? Other {
            return .other(name: other.name)
        } else {
            return .other(name: "unknown-origin")
        }
    }
}

extension FileObject.GcodeAnalysisDimensions {
    func toStruct() -> FileGcodeDimensionsStruct {
        return FileGcodeDimensionsStruct(
            depth: self.depth?.doubleValue,
            height: self.height?.doubleValue,
            width: self.width?.doubleValue
        )
    }
}


extension FileObject.GcodeAnalysis {
    func toFilmentUseStruct() -> [FileGcodeFilamentUse] {
        return self.filament.map { x in
            FileGcodeFilamentUse(
                name: x.key,
                length: x.value.length?.doubleValue,
                volume: x.value.volume?.doubleValue
            )
        }
    }
}

extension FileOriginStruct {
    func toClass() -> FileOrigin {
        switch self {
        case .gcode: return FileOrigin.Gcode()
        case .other(name: let name): return FileOrigin.Other(name: name)
        }
    }
}
