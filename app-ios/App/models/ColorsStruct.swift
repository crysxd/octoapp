//
//  Colors.swift
//  OctoApp
//
//  Created by Christian on 28/10/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import SwiftUI

struct ColorsStruct: Equatable, Codable, Hashable {
    var light: ColorSchemeStruct
    var dark: ColorSchemeStruct
}

struct ColorSchemeStruct : Equatable, Codable, Hashable {
    var main: Color
    var accent: Color
}

let previewColorsStruct = ColorsStruct(
    light: ColorSchemeStruct(
        main: Color(hex: "#27AE60"),
        accent: Color(hex: "#D0F3CC")
    ),
    dark: ColorSchemeStruct(
        main: Color(hex: "#106101"),
        accent: Color(hex: "#D0F3CC")
    )
)
