//
//  PurchaseOffer.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct PurchaseOfferStruct: Equatable, Identifiable {
    var id: String { product.productId }
    let product: BillingProductStruct
    let dealFor: BillingProductStruct?
    let label: String?
    let badge: PurchaseBadgeStruct
    let descriptionText: String?
}

extension PurchaseOfferStruct {
    
    init?(productId: String, offer: PurchaseOffers.Offer, products: [BillingProductStruct]) {
        guard let product = products.first(where: { $0.productId == productId }) else {
            return nil
        }
        
        var dealFor: BillingProductStruct? = nil
        if let dealId = offer.dealFor {
            dealFor = products.first(where: { $0.productId == dealId })
        }
        
        self.init(
            product: product,
            dealFor: dealFor,
            label: offer.label,
            badge: PurchaseBadgeStruct(badge: offer.badge),
            descriptionText: offer.description_
        )
    }
}
