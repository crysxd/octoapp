//
//  PrinterProfileStruct.swift
//  OctoApp
//
//  Created by Christian Würthner on 30/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct PrinterProfileStruct : Equatable {
    let id: String
    let current: Bool
    let default_: Bool
    let model: String
    let name: String
    let color: String
    let volume: Volume
    let axes: Axes
    let extruders: [Extruder]
    let heatedChamber: Bool
    let heatedBed: Bool
    let estimatedNozzleDiameter: Float
}

extension PrinterProfileStruct {
    
    init(printerProfile: PrinterProfile) {
        self.init(
            id: printerProfile.id,
            current: printerProfile.current,
            default_: printerProfile.default_,
            model: printerProfile.model,
            name: printerProfile.name,
            color: printerProfile.color,
            volume: Volume(volume: printerProfile.volume),
            axes: Axes(axes: printerProfile.axes),
            extruders: printerProfile.extruders.map { e in Extruder(extruder: e)},
            heatedChamber: printerProfile.heatedChamber,
            heatedBed: printerProfile.heatedBed,
            estimatedNozzleDiameter: printerProfile.estimatedNozzleDiameter
        )
    }
    
    struct Volume : Equatable {
       let depth: Float
       let width: Float
       let height: Float
       let origin: Origin
       let formFactor: FormFactor
    }
    
    struct Axes : Equatable {
        let e: Axis
        let x: Axis
        let y: Axis
        let z: Axis
    }
    
    struct Axis : Equatable {
        let inverted: Bool
        let speed: Float?
    }
    
    struct Extruder : Equatable {
        let nozzleDiameter: Float
        let componentName: String
        let extruderComponents: [String]
    }
    
    enum Origin : Equatable {
        case center, lowerleft
    }
    
    enum FormFactor : Equatable {
        case circular, rectengular
    }
}

extension PrinterProfileStruct.FormFactor {
    init(formFactor: PrinterProfile.FormFactor) {
        switch formFactor {
        case .circular: self = .circular
        case .rectangular: self = .rectengular
        default: self = .rectengular
        }
    }
}

extension PrinterProfileStruct.Origin {
    init(origin: PrinterProfile.Origin) {
        switch origin {
        case .center: self = .center
        case .lowerleft: self = .lowerleft
        default: self = .lowerleft
        }
    }
}

extension PrinterProfileStruct.Extruder {
    init(extruder: PrinterProfile.Extruder) {
        self.init(
            nozzleDiameter: extruder.nozzleDiameter,
            componentName: extruder.componentName,
            extruderComponents: extruder.extruderComponents
        )
    }
}

extension PrinterProfileStruct.Axis {
    init(axis: PrinterProfile.Axis) {
        self.init(
            inverted: axis.inverted,
            speed: axis.speed?.floatValue
        )
    }
}

extension PrinterProfileStruct.Axes {
    init(axes: PrinterProfile.Axes) {
        self.init(
            e: PrinterProfileStruct.Axis(axis: axes.e),
            x: PrinterProfileStruct.Axis(axis: axes.x),
            y: PrinterProfileStruct.Axis(axis: axes.y),
            z: PrinterProfileStruct.Axis(axis: axes.z)
        )
    }
}

extension PrinterProfileStruct.Volume {
    init(volume: PrinterProfile.Volume) {
        self.init(
            depth: volume.depth,
            width: volume.width,
            height: volume.height,
            origin: PrinterProfileStruct.Origin(origin: volume.origin),
            formFactor: PrinterProfileStruct.FormFactor(formFactor: volume.formFactor)
        )
    }
}
