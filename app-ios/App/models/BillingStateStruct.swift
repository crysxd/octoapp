//
//  BillingStateStruct.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct BillingStateStruct {
    var products: [BillingProductStruct]
    var purchases: [BillingPurchaseStruct]
}

extension BillingStateStruct {
    init(state: BillingState) {
        self.init(
            products: state.products.map { BillingProductStruct(product: $0)},
            purchases: state.purchases.map { BillingPurchaseStruct(purchase: $0)}
        )
    }
}
