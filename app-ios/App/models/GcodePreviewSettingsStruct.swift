//
//  GcodePreviewSettingsStruct.swift
//  OctoApp
//
//  Created by Christian Würthner on 30/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct GcodePreviewSettingsStruct : Equatable {
  let showPreviousLayer: Bool
  let showCurrentLayer: Bool
  let layerOffset: Int
  let quality: Quality
}

extension GcodePreviewSettingsStruct {
    
    init(gcodePreviewSettings: GcodePreviewSettings) {
        self.init(
            showPreviousLayer: gcodePreviewSettings.showPreviousLayer,
            showCurrentLayer: gcodePreviewSettings.showCurrentLayer,
            layerOffset: Int(gcodePreviewSettings.layerOffset),
            quality: Quality(quality: gcodePreviewSettings.quality)
        )
    }
    
    enum Quality : Equatable {
        case low, medium, ultra
    }
}

extension GcodePreviewSettingsStruct.Quality {
    init(quality: GcodePreviewSettings.Quality) {
        switch quality {
        case .low: self = .low
        case .medium: self = .medium
        case .ultra: self = .ultra
        default: self = .medium
        }
    }
    
    func toClass() -> GcodePreviewSettings.Quality {
        switch self {
        case .low: return .low
        case .medium: return .medium
        case .ultra: return .ultra
        }
    }
}
