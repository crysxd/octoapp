//
//  ProductStruct.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import OctoAppBase
import Foundation
import StoreKit

struct BillingProductStruct: Equatable, Identifiable {
    var id: String { productId }
    let productId: String
    let productName: String
    let price: String
    let priceOrder: Int
    let storeProduct: Product?
}

extension BillingProductStruct {
    init(product: BillingProduct) {
        self.init(
            productId: product.productId,
            productName: product.productName,
            price: product.price,
            priceOrder: Int(product.priceOrder),
            storeProduct: product.nativeProduct as? Product
        )
    }
}
