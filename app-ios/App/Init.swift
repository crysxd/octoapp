//
//  Init.swift
//  OctoApp
//
//  Created by Christian on 06/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import WidgetKit
import SwiftUI
import OSLog

#if DEBUG
let debug = true
#else
let debug = false
#endif

func ensureInitBase(subsystemSuffix: String) {
    guard SharedBaseInjector.shared.getOrNull() == nil else {
        return
    }
    
    NapierProxyKt.doInitLogging(adapter: nil, isDebug: debug)
    
    // Koin
    SharedCommonInjector.shared.setComponent(
        t: SharedCommonComponent(
            platformModule: PlatformModule(),
            serializationModule: SerializationModule(),
            persistenceModule: PersistenceModule()
        )
    )
    
    SharedBaseInjector.shared.setComponent(
        t: SharedBaseComponent(
            sharedCommonComponent: SharedCommonInjector().get(),
            repositoryModule: RepositoryModule(),
            loggingModule: LoggingModule(),
            networkModule: NetworkModule(
                networkServiceDiscovery: { DarwinNetworkDiscovery() },
                dns: { DarwinDns() }
            ),
            useCaseModule: UseCaseModule(),
            dataSourceModule: DataSourceModule()
    )
    )
    
    // Logging
    DarwinAntilog.shared.addLogger  { level, tag, message in
        let log = Logger(subsystem: "de.crysxd.octoapp\(subsystemSuffix)", category: tag)
        switch level {
        case "V": log.trace("\(message, privacy: .public)")
        case "D": log.debug("\(message, privacy: .public)")
        case "W": log.warning("\(message, privacy: .public)")
        case "E": log.error("\(message, privacy: .public)")
        case "A": log.critical("\(message, privacy: .public)")
        default: log.info("\(message, privacy: .public)")
        }
    }
    
    // Adapters
    MjpegSocketCoreCompanion.shared.factory = { DarwinMjpegSocketCore() }
    ConnectivityHelperCoreCompanion.shared.factory = { DarwinConnectivityHelperCore() }
    BillingManager.shared.adapter = StoreKitBillingAdapter()
    BillingManager.shared.onResume()
}

