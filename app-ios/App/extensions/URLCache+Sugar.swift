//
//  URLCache+Sugar.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation

extension URLCache {
    static let imageCache = URLCache(memoryCapacity: 2*1024*1024, diskCapacity: 5*1024*1024)
}
