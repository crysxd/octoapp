//
//  Number+Sugar.swift
//  OctoApp
//
//  Created by Christian on 29/10/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

extension SomeNumber {
    
    func format(
        minDecimals: Int = 0,
        maxDecimals: Int = 0
    ) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatNumber(
            number: toDouble(),
            minDecimals: Int32(minDecimals),
            maxDecimals: Int32(maxDecimals)
        )
    }
    
    func formatAsPercent(
        minDecimals: Int = 0,
        maxDecimals: Int = 0
    ) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatPercent(
            percent: toDouble(),
            minDecimals: Int32(minDecimals),
            maxDecimals: Int32(maxDecimals)
        )
    }
    
    func formatAsTemperature(
        minDecimals: Int = 0,
        maxDecimals: Int = 1
    ) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatTemperature(
            celcius: toDouble(),
            minDecimals: Int32(minDecimals),
            maxDecimals: Int32(maxDecimals)
        )
    }
    
    func formatAsWeight(
        onlyGrams: Bool = true,
        minDecimals: Int = 0,
        maxDecimals: Int = 2
    ) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatWeight(
            grams: toDouble(),
            onlyGrams: onlyGrams,
            minDecimals: Int32(minDecimals),
            maxDecimals: Int32(maxDecimals)
        )
    }
    
    func formatAsLength(
        onlyMilli: Bool = true,
        minDecimals: Int = 0,
        maxDecimals: Int = 2
    ) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatLength(
            mm: toDouble(),
            onlyMilli: onlyMilli,
            pow: 1,
            minDecimals: Int32(minDecimals),
            maxDecimals: Int32(maxDecimals)
        )
    }
    
    func formatAsLayerHeight() -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatLength(
            mm: toDouble(),
            onlyMilli: true,
            pow: 1,
            minDecimals: 2,
            maxDecimals: 2
        )
    }
    
    func formatAsArea(
        onlyMilli: Bool = true,
        minDecimals: Int = 0,
        maxDecimals: Int = 2
    ) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatArea(
            mm2: toDouble(),
            onlyMilli: onlyMilli,
            minDecimals: Int32(minDecimals),
            maxDecimals: Int32(maxDecimals)
        )
    }
    
    func formatAsVolume(
        onlyMilli: Bool = true,
        minDecimals: Int = 0,
        maxDecimals: Int = 2
    ) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatVolume(
            mm3: toDouble(),
            onlyMilli: onlyMilli,
            minDecimals: Int32(minDecimals),
            maxDecimals: Int32(maxDecimals)
        )
    }
    
    func formatDefault() -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatDuration(seconds: self)
    }
    
    func formatEta(compact: Bool, showLabel: Bool = false, allowRelative: Bool = true) -> String {
        OctoAppBase.DefaultDataFormatter.shared.formatEta(
            seconds: self,
            showLabel: showLabel,
            allowRelative: false,
            useCompactFutureDate: compact
        )
    }
}

protocol SomeNumber {
    func toDouble() -> Double
}

extension Double : SomeNumber {
    func toDouble() -> Double {
        self
    }
}

extension Float : SomeNumber {
    func toDouble() -> Double {
        Double(self)
    }
}

extension Int : SomeNumber {
    func toDouble() -> Double {
        Double(self)
    }
}

extension Int32 : SomeNumber {
    func toDouble() -> Double {
        Double(self)
    }
}

extension KotlinInt : SomeNumber {
    func toDouble() -> Double {
        Double(truncating: self)
    }
}

extension KotlinDouble : SomeNumber {
    func toDouble() -> Double {
        Double(truncating: self)
    }
}

extension KotlinFloat : SomeNumber {
    func toDouble() -> Double {
        Double(truncating: self)
    }
}
