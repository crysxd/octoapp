//
//  BillingManager+Sugar.swift
//  OctoApp Beta
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import Combine

extension BillingManager {
    var currentState: BillingStateStruct {
        BillingStateStruct(state: state.value as! BillingState)
    }
    
    var statePublisher: any Publisher<BillingStateStruct, Never> {
        state.asPublisher().map { (state: BillingState) in BillingStateStruct(state: state)}
    }
}
