//
//  Preview.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

var isSwiftUiPreview: Bool {
    ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] == "1"
}

struct StatefulPreviewWrapper<Value, Content: View>: View {
    @State var value: Value
    var content: (Binding<Value>) -> Content

    var body: some View {
        content($value)
    }

    init(_ value: Value, content: @escaping (Binding<Value>) -> Content) {
        self._value = State(wrappedValue: value)
        self.content = content
    }
}
