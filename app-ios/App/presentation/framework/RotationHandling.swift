//
//  RotationHandling.swift
//  OctoApp
//
//  Created by Christian on 21/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

#if os(iOS)
extension AppDelegate {
    
    static var defaultOrientation = UIInterfaceOrientationMask.portrait
    
//    private static var currentOrientation: UIInterfaceOrientation {
//        switch UIDevice.current.orientation {
//        case .portraitUpsideDown: return .portraitUpsideDown
//        case .landscapeLeft: return .landscapeLeft
//        case .landscapeRight: return .landscapeRight
//        default: return .portrait
//        }
//    }
    
    static var orientationLock = UIInterfaceOrientationMask.portrait {
        didSet {
            // For some reason we need to force the UIDevice rotation as well...no clue. Let's do it...
            // Seems to be no longer needed, keeping it around if needed later
//            UIDevice.current.beginGeneratingDeviceOrientationNotifications()
//            var forcedOrientation = UIInterfaceOrientation.portrait
//            if orientationLock == .all {
//                forcedOrientation = currentOrientation
//            }
//            UIDevice.current.setValue(forcedOrientation.rawValue, forKey: "orientation")

            // Update UI as well
            UIApplication.shared.connectedScenes.forEach { scene in
                if let windowScene = scene as? UIWindowScene {
                    windowScene.requestGeometryUpdate(.iOS(interfaceOrientations: orientationLock))
                }
            }
        }
    }
    
    func application(
        _ application: UIApplication,
        supportedInterfaceOrientationsFor window: UIWindow?
    ) -> UIInterfaceOrientationMask {
        return AppDelegate.orientationLock
    }
}
#endif
