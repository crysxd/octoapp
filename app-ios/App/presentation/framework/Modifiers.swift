//
//  Modifiers.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import SwiftUI

struct WithInstanceId: ViewModifier {
    
    @State var created = false
    @Environment(\.instanceId) private var instanceId
    let cb: (String, String) -> Void
    
    func body(content: Content) -> some View {
        content
            .onAppear {
                if !created {
                    created = true
                    cb(instanceId, instanceId)
                }
            }
            .onChange(of: instanceId, cb)
    }
}

private struct MacOsCompatibleSheetBool<SheetContent: View>: ViewModifier {
    
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    @Binding var isPresented: Bool
    var fullscreen: Bool
    var sheetContent: () -> SheetContent
    
    func body(content: Content) -> some View {
        if fullscreen {
            content.fullScreenCover(isPresented: $isPresented) {
                sheetContent().environmentObject(orchestrator)
            }
        } else {
            content.sheet(isPresented: $isPresented) {
                sheetContent().environmentObject(orchestrator)
            }
        }
    }
}

private struct MacOsCompatibleSheetItem<SheetContent: View, Item: Identifiable>: ViewModifier {
    
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    @Binding var item: Item?
    var fullscreen: Bool
    var sheetContent: (Item) -> SheetContent
    
    func body(content: Content) -> some View {
        if fullscreen {
            content.fullScreenCover(item: $item) { item in
                sheetContent(item).environmentObject(orchestrator)
            }
        } else {
            content.sheet(item: $item) { item in
                sheetContent(item).environmentObject(orchestrator)
            }
        }
    }
}

private struct ToolbarDone: ViewModifier {
    
    var callback: (() -> Void)?
    @Environment(\.dismiss) var dismiss
    
    func body(content: Content) -> some View {
        content .toolbar {
            ToolbarItemGroup(placement: .confirmationAction) {
                Button("done"~) {
                    if let cb = callback {
                        cb()
                    } else {
                        dismiss()
                    }
                }
            }
        }
    }
}

struct DeviceRotationViewModifier: ViewModifier {
    let action: (UIDeviceOrientation) -> Void
    
    func body(content: Content) -> some View {
        content
            .ifPad {
                $0.onAppear().onReceive(NotificationCenter.default.publisher(for: UIDevice.orientationDidChangeNotification)) { _ in
                    let orientation = UIDevice.current.orientation
                    if orientation.isValidInterfaceOrientation {
                        action(orientation)
                    }
                }
            }
    }
}

struct LowOverheadMediatorObserver<T: AnyObject> : ViewModifier {
 
    let mediator: LowOverheadMediator<T>?
    let action: (T?) -> Void
    @State private var sinkId = UUID().uuidString
    
    func body(content: Content) -> some View {
        content
            .onAppear {
                mediator?.pushSink(sinkId: sinkId, sink: action)
            }
            .onChange(of: mediator) { old, new in
                old?.popSink(sinkId: sinkId)
                new?.pushSink(sinkId: sinkId, sink: action)
            }
            .onDisappear {
                mediator?.popSink(sinkId: sinkId)
            }
    }
}


extension View {
    func onRotate(perform action: @escaping (UIDeviceOrientation) -> Void) -> some View {
        self.modifier(DeviceRotationViewModifier(action: action))
    }
    
    func withInstanceId(cb: @escaping (String, String) -> Void) -> some View {
        modifier(WithInstanceId(cb: cb))
    }
    
    func toolbarDoneButton(cb:  (() -> Void)? = nil) -> some View {
        modifier(ToolbarDone(callback: cb))
    }
    
    // @EnvironmentObject is not passed to sheets in macOS, so we need to manually pass them on
    func macOsCompatibleSheet<SheetContent: View>(isPresented: Binding<Bool>, fullscreen: Bool = false, @ViewBuilder sheetContent: @escaping () -> SheetContent ) -> some View {
        modifier(MacOsCompatibleSheetBool(isPresented: isPresented, fullscreen: fullscreen, sheetContent: sheetContent))
    }
    
    // @EnvironmentObject is not passed to sheets in macOS, so we need to manually pass them on
    func macOsCompatibleSheet<SheetContent: View, Item: Identifiable>(item: Binding<Item?>, fullscreen: Bool = false, @ViewBuilder sheetContent: @escaping (Item) -> SheetContent ) -> some View {
        modifier(MacOsCompatibleSheetItem(item: item, fullscreen: fullscreen, sheetContent: sheetContent))
    }
    
    func onReceive<T: AnyObject> (_ mediator: LowOverheadMediator<T>?, action: @escaping (T?) -> Void) -> some View {
        self.modifier(LowOverheadMediatorObserver(mediator: mediator, action: action))
    }
}

func getSafeAreaBottom() -> CGFloat{
    return (findKeyWindow()?.safeAreaInsets.bottom) ?? 0
}


func getSafeAreaTop() -> CGFloat{
    return (findKeyWindow()?.safeAreaInsets.top) ?? 0
}

private func findKeyWindow() -> UIWindow?  {
    return UIApplication.shared.connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows
        .filter({$0.isKeyWindow}).first
}
