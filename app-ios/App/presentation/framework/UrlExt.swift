//
//  UrlExt.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import SwiftUI

extension OpenURLAction {
  
    public func callAsFunction(_ url: Ktor_httpUrl) {
        self(URL(string: url.description())!)
    }

}
