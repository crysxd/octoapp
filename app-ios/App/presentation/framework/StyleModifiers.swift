//
//  StyleModifiers.swift
//  OctoApp
//
//  Created by Christian on 31/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import SwiftUI


struct ColoredFont: ViewModifier {
    
    let font: Font
    let color: Color
    
    init(font: Font) {
        self.font = font
        self.color =  OctoTheme.colors.normalText
    }
    
    init(font: Font, color: Color) {
        self.font = font
        self.color =  color
    }
    
    func body(content: Content) -> some View {
        content
            .font(font)
            .foregroundColor(color)
    }
}

struct Surface: ViewModifier {
    
    let color: SurfaceColor
    let shadow: Bool
    let highlighted: Bool
    
    private var mappedColor: Color {
        switch(color) {
        case .normal: return  OctoTheme.colors.inputBackground
        case .alternative: return  OctoTheme.colors.inputBackgroundAlternative
        case .red: return OctoTheme.colors.redTranslucent
        case .special(let c): return c
        }
    }
    
    private var mappedHightlightColor: Color {
        switch(color) {
        case .normal: return  OctoTheme.colors.accent
        case .alternative: return  OctoTheme.colors.accent
        case .red: return OctoTheme.colors.red
        case .special(let c): return c
        }
    }
    
    private var shape : RoundedRectangle {
        return RoundedRectangle(cornerSize: CGSize(width: OctoTheme.dimens.cornerRadius, height: OctoTheme.dimens.cornerRadius))
    }
    
    func body(content: Content) -> some View {
        content
            .cornerRadius(OctoTheme.dimens.cornerRadius)
            .background(shape.strokeBorder(mappedHightlightColor.opacity(highlighted ? 1 : 0), lineWidth: 0.66))
            .if(shadow) { $0.background(shape.fill(mappedColor).shadow(radius: 3)) }
            .if(!shadow) { $0.background(shape.fill(mappedColor)) }
    }
}


extension View {
    @ViewBuilder func `if`<Content: View>(_ condition: Bool, transform: (Self) -> Content) -> some View {
        if condition {
            transform(self)
        } else {
            self
        }
    }
    
    @ViewBuilder func ifPad<Content: View>(transform: (Self) -> Content) -> some View {
        if UIDevice.current.userInterfaceIdiom == .pad {
            transform(self)
        } else {
            self
        }
    }
    
    @ViewBuilder func ifNotPad<Content: View>(transform: (Self) -> Content) -> some View {
        if UIDevice.current.userInterfaceIdiom != .pad {
            transform(self)
        } else {
            self
        }
    }

    func surface(color: SurfaceColor = .normal, shadow: Bool = false, highlighted: Bool = false) -> some View {
        modifier(Surface(color: color, shadow: shadow, highlighted: highlighted))
    }
    
    func typographyBase() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.base))
    }
    
    func typographyLabel() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.label))
    }
    
    func typographyLabelSmall() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.labelSmall, color: OctoTheme.colors.darkText))
    }
    
    func typographyLabelTiny() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.labelTiny))
    }
    
    func typographyFocus() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.focus))
    }
    
    func typographyInput() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.input))
    }
    
    func typographyData() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.data))
    }
    
    func typographyDataLarge() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.dataLarge))
    }
    
    func typographyTitle() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.title, color: OctoTheme.colors.darkText))
    }
    
    func typographyTerminal() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.terminal))
    }
    
    func typographyTitleBig() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.titleBig, color: OctoTheme.colors.darkText))
    }
    
    func typographyTitleLarge() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.titleLarge, color: OctoTheme.colors.darkText))
    }
    
    func typographySubtitle() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.subtitle, color: OctoTheme.colors.darkText))
    }
    
    func typographyButton(small: Bool = false) -> some View {
        modifier(small ? ColoredFont(font: OctoTheme.typography.buttonSmall) : ColoredFont(font: OctoTheme.typography.button))
    }
 
    func typographyButtonLink() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.sectionHeader, color: OctoTheme.colors.accent))
    }
    
    func typographySectionHeader() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.sectionHeader, color: OctoTheme.colors.darkText))
    }
}

enum SurfaceColor {
    case normal, alternative, red, special(Color)
}

extension View {
    func readSize(onChange: @escaping (CGSize) -> Void) -> some View {
        background(
            GeometryReader { geometryProxy in
                Color.clear
                    .preference(key: SizePreferenceKey.self, value: geometryProxy.size)
            }
        )
        .onPreferenceChange(SizePreferenceKey.self, perform: onChange)
    }
}

private struct SizePreferenceKey: PreferenceKey {
    static var defaultValue: CGSize = .zero
    static func reduce(value _: inout CGSize, nextValue _: () -> CGSize) {}
}
