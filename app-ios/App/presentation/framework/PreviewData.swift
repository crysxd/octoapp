//
//  PreviewData.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation

let PreviewData =  PreviewDataStruct()

struct PreviewDataStruct {
    let printableFile = FileObjectStruct(
        origin: .gcode,
        display: "Some file.gcode",
        name: "Some file.gcode",
        path: "/Some file.gcode",
        isFolder: false,
        isPrintable: true
    )
    
    let folder = FileObjectStruct(
        origin: .gcode,
        display: "Some folder",
        name: "Some folder",
        path: "/Some folder",
        childFolders: [
            FileObjectStruct(
                origin: .gcode,
                display: "Some other folder",
                name: "Some other folder",
                path: "/Some folder/Some other folder",
                isFolder: true,
                isPrintable: false
            ),
            FileObjectStruct(
                origin: .gcode,
                display: "Third folder",
                name: "Third folder",
                path: "/Some folder/Third folder",
                isFolder: true,
                isPrintable: false
            )
        ],
        children: [
            FileObjectStruct(
                origin: .gcode,
                display: "Some file.gcode",
                name: "Some file.gcode",
                path: "/Some file.gcode",
                isFolder: false,
                isPrintable: true
            ),
            FileObjectStruct(
                origin: .gcode,
                display: "Some other file.gcode",
                name: "Some other file.gcode",
                path: "/Some other file.gcode",
                isFolder: false,
                isPrintable: true
            )
        ],
        isFolder: true,
        isPrintable: false
    )
}
