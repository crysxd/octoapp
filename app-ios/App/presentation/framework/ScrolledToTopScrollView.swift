//
//  ScrolledToTopScrollView.swift
//  OctoApp
//
//  Created by Christian on 29/11/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import SwiftUIIntrospect

struct ScrolledToTopScrollView<ScrollContent: View> : View {
    
    @Binding var scrolledToTop: Bool
    var content: () -> ScrollContent
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            content()
        }
        .introspect(.scrollView, on: .iOS(.v13, .v14, .v15, .v16, .v17, .v18)) { scrollView in
            Task {
                while !Task.isCancelled {
                    try await Task.sleep(for: .seconds(0.1))
                    scrolledToTop = scrollView.contentOffset.y < 10
                }
            }
        }
    }
}
