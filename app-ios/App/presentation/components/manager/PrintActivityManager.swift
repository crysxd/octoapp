//
//  PrintActivityManager.swift
//  OctoApp
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import ActivityKit
import OctoAppBase
import SwiftUI

let SharedPrintAcitvyManager = PrintActivityManager()


class PrintActivityManager {
    
    let tag = "[LA] PrintActivityManager"
    private let helper =  PrintAcitivityThumbnailHelper()
    private var resolvingThumbnailFor: String? = nil
    
    var isAvailable: Bool {
        if ProcessInfo.processInfo.isiOSAppOnMac {
            return false
        } else {
            return ActivityAuthorizationInfo().areActivitiesEnabled
        }
    }
    
    func startOrUpdateActivity(
        instanceId: String,
        instanceLabel: String,
        instanceColor: ColorsStruct,
        thumbnailUrl: String?,
        data: PrintActivityAttributes.Data
    ) async throws {
        // Ensure we can run live acitivties
        guard ActivityAuthorizationInfo().areActivitiesEnabled else {
            Napier.d(tag: tag, message: "Live activities are disabled, skipping for instance \(instanceId)")
            return
        }
        
        // Dismiss ended activities
        for activity in (Activity<PrintActivityAttributes>.activities.filter { activity in
            activity.activityState == .ended ||
            activity.content.state.stateEnum == .expired ||
            activity.content.state.stateEnum == .completed
        }) {
            Napier.v(tag: tag, message: "Dismissing ended live activity for instance \(activity.attributes.instanceId), state=\(activity.activityState) stateEnum=\(activity.content.state.stateEnum)")
            await activity.end(nil, dismissalPolicy: .immediate)
        }
        
        // Search active activtiy
        let runningActivity = Activity<PrintActivityAttributes>.activities.filter { activity in
            activity.activityState != .ended && activity.activityState != .dismissed && activity.attributes.instanceId == instanceId
        }.first
        
        // Ensure feature is enabled
        guard BillingManager.shared.isFeatureEnabled(feature: BillingManagerKt.FEATURE_LIVE_ACTIVITY) else {
            Napier.v(tag: tag, message: "Live activities are disbaled by billing, ending for instance \(instanceId)")
            await runningActivity?.end(nil, dismissalPolicy: .immediate)
            return
        }
        
        // Create content state
        let content = ActivityContent<PrintActivityAttributes.ContentState>.init(
            state: data,
            staleDate: nil
        )
        
        // End if idle but running
        if data.stateEnum == .idle, let activity = runningActivity {
            Napier.v(tag: tag, message: "Ending live activity for instance \(instanceId), now idle")
            await activity.end(content, dismissalPolicy: .immediate)
        }
        
        // Udpate if already running
        else if let activity = runningActivity {
            storeThumbnail(thumbnailUrl: thumbnailUrl, instanceId: instanceId, activityId: activity.id) {
                await activity.update(content)
            }
        }
        
        // Start if not running yet
        else if data.stateEnum != .idle {
            // Ensure no other activity shown for this instance
            let outdated = Activity<PrintActivityAttributes>.activities.filter { $0.activityState != .dismissed && $0.attributes.instanceId == instanceId }
            for activity in outdated {
                Napier.i(tag: tag, message: "Stopping outdated activity for \(activity.attributes.instanceId), just started new one")
                await activity.end(nil, dismissalPolicy: .immediate)
            }
            
            // Start
            Napier.i(tag: tag, message: "Starting live activity for instance \(instanceId)")
            let activity = try Activity.request(
                attributes: PrintActivityAttributes(
                    instanceId: instanceId,
                    instanceLabel: instanceLabel,
                    instanceColor: instanceColor,
                    expiresAt: Calendar.current.date(byAdding: DateComponents(hour: 7, minute: 30), to: Date())!
                ),
                content: content,
                pushType: .token
            )
            
            // Store thumbnail
            storeThumbnail(thumbnailUrl: thumbnailUrl, instanceId: instanceId, activityId: activity.id) {
                await activity.update(content)
            }
            
            // Manager push tokens
            Task {
                for await tokenData in activity.pushTokenUpdates {
                    do {
                        let token = tokenData.map {String(format: "%02x", $0)}.joined()
                        Napier.i(tag: tag, message: "[Push] Live activity for instance \(instanceId) received push token \(token), registering with companion plugin")
                        
                        // Build params. Token needs to be prefixed with "activity" and either "sandbox" for debug builds or "release" for release builds
                        let platform = SharedCommonInjector.shared.get().platform
                        let tokenPrefix = platform.debugBuild ? "activity:sandbox:" : "activity:production:"
                        let param = RegisterWithCompanionPluginUseCase.Params(
                            instanceId: instanceId,
                            pushToken: "\(tokenPrefix)\(token)",
                            pushTokenFallback: try? await OctoPushMessaging.shared.getPushToken(),
                            deviceName: await UIDevice.current.name,
                            deviceDescription: "Live Activity for current print",
                            expireInSecs: KotlinLong(longLong: Int64((activity.attributes.expiresAt.timeIntervalSince1970 - Date().timeIntervalSince1970)))
                        )
                                                
                        // Register
                        try await SharedBaseInjector.shared.get().registerWithCompanionPluginUseCase().execute(param: param)
                        Napier.i(tag: tag, message: "Registered live activity for \(instanceId)")
                    } catch {
                        Napier.e(tag: tag, message: "Failed to register, stopping live activity for \(instanceId)")
                        await activity.end(nil, dismissalPolicy: .immediate)
                    }
                }
            }
        }
    }
        
    private func storeThumbnail(thumbnailUrl: String?, instanceId: String, activityId: String, then: @escaping () async -> Void ) {
        // Save thumbnail async
        if !helper.isStoredAlready(activityId: activityId) {
            guard resolvingThumbnailFor != activityId else {
                Napier.i(tag: tag, message: "Skipping thumbnail download, already loading for \(activityId)")
                return
            }
            
            if let checkedUrl = thumbnailUrl {
                Task {
                    // Super simple locking - not perfect but good enough (TM)
                    resolvingThumbnailFor = activityId
                    defer {
                        if resolvingThumbnailFor == activityId {
                            resolvingThumbnailFor = nil
                        }
                    }
                    
                    // Download
                    Napier.i(tag: tag, message: "Saving thumbnail for activity \(activityId) from \(checkedUrl)")
                    let data = try await SimpleNetworkRequestKt.simpleNetworkRequest(
                        instanceId: instanceId,
                        path: checkedUrl,
                        pathEncoded: true
                    ).awaitResponse()
                    
                    // Delete existing files and store, we only keep one for the current activity
                    helper.clearStorage(runningActivityIds: Activity<PrintActivityAttributes>.activities.map { $0.id })
                    helper.storeThumbnail(data: data, activityId: activityId)
                    
                    // Success
                    await then()
                }
            } else {
                Napier.w(tag: tag, message: "Failed to get thumbnail path for \(activityId)")
                Task {
                    await then()
                }
            }
        } else {
            Task {
                await then()
            }
        }
    }
}
