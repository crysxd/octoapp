//
//  PowerControlsRgbView.swift
//  OctoApp
//
//  Created by Christian on 21/10/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine

struct PowerControlsRgbView: View {
    
    @Environment(\.instanceId) private var instanceId
    @StateObject private var viewModel: PowerControlsRgbViewModel
    
    init(type: String) {
        _viewModel = StateObject(wrappedValue: PowerControlsRgbViewModel(type: type))
    }
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            PowerControlsRgbViewContent(colors: viewModel.colors)
                .usingViewModel(viewModel)
        }
    }
}

private class PowerControlsRgbViewModel : BaseViewModel {
    var currentCore: PowerDeviceRgbViewModelCore?
    var bag: Set<AnyCancellable> = []
    @Published var colors: [PowerDeviceRgbwColor] = []
    private var type: String
    
    init(type: String) {
        self.type = type
    }
    
    func createCore(instanceId: String) -> PowerDeviceRgbViewModelCore {
        PowerDeviceRgbViewModelCore(instanceId: instanceId, type: type)
    }
    
    func publish(core: PowerDeviceRgbViewModelCore) {
        core.colors.asPublisher()
            .sink { (state: PowerDeviceRgbViewModelCore.Colors) in
                self.colors = state.colors
            }
            .store(in: &bag)
    }
    
    func clearData() {
        colors = []
    }
}

private struct PowerControlsRgbViewContent: View {
    
    var colors: [PowerDeviceRgbwColor]
    private let size: CGFloat = 16
    
    var body: some View {
        HStack(spacing: 0) {
            ForEach(Array(colors.enumerated()), id: \.offset) { index, color in
                if index > 0 {
                    Rectangle()
                        .fill(OctoTheme.colors.lightGrey)
                        .frame(width: size / 3, height: 2)
                }
                                
                ZStack {
                    Circle()
                        .fill(Color(red: Double(color.r), green: Double(color.g), blue: Double(color.b)))
                        .frame(width: size, height: size)
                    
                    Circle()
                        .fill(Color(red: 1, green: 1, blue: 1).opacity(Double(color.w)))
                        .frame(width: size * 0.4, height: size / 4)
                }
            }
        }
        .padding(OctoTheme.dimens.margin0)
        .background(Capsule().fill(OctoTheme.colors.veryLightGrey))
        .animation(.default, value: colors)
        .frame(height: size)
    }
}


#Preview {
    PowerControlsRgbViewContent(
        colors: [
            PowerDeviceRgbwColor(r: 1, g: 1, b: 1, w: 0),
            PowerDeviceRgbwColor(r: 0, g: 0, b: 0, w: 1),
            PowerDeviceRgbwColor(r: 0, g: 0, b: 0, w: 1),
            PowerDeviceRgbwColor(r: 0, g: 0, b: 0, w: 1)
        ]
    )
}
