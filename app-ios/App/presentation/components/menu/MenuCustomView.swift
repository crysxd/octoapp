//
//  MenuCustomViews.swift
//  OctoApp
//
//  Created by Christian on 04/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct MenuCustomView: View {
    var type: String
    
    var body: some View {
        switch type {
        case MainMenu.companion.MainMenuHeaderView: MainMenuHeader()
        case PrintNotificationsMenu.companion.NotificationStatusView: NotificationMenuHeader()
        case SupporterPerkMenu.companion.SupporterPerkView: SupporterPerkView()
        default: do {
            if type.starts(with: PowerControlsDeviceMenu.companion.PowerControlsDeviceMenuRgbwView) {
                PowerControlsRgbView(type: type)
            } else {
                Text(type)
                    .padding(OctoTheme.dimens.margin3)
                    .background(OctoTheme.colors.veryLightGrey)
            }
        }
        }
    }
}

struct MenuCustomViews_Previews: PreviewProvider {
    static var previews: some View {
        MenuCustomView(type: MainMenu.companion.MainMenuHeaderView)
            .previewDisplayName("Main menu")
        
    }
}
