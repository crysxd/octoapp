//
//  MenuReferences.swift
//  OctoApp
//
//  Created by Christian on 02/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

struct MenuReference : Hashable, Identifiable, Equatable {
    var id: String { return reference.id }
    
    static func == (lhs: MenuReference, rhs: MenuReference) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    var reference: OctoAppBase.Menu
    var menuId: MenuId
}

struct MenuItemReference : Hashable, Identifiable, Equatable {
    var id: String { return reference.itemId }
    
    
    static func == (lhs: MenuItemReference, rhs: MenuItemReference) -> Bool {
        return lhs.id == rhs.id &&
        lhs.pinned == rhs.pinned &&
        lhs.reference.icon == rhs.reference.icon &&
        lhs.reference.title == rhs.reference.title
        
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    var reference: OctoAppBase.MenuItem
    var enabled: Bool = true
    var featureDisabled: Bool = true
    var pinned: Bool = false
    var badgeCount: Int = 0
}

extension MenuItemStyle {
    var foreground: Color {
        switch self {
        case .settings: return OctoTheme.colors.menuStyleSettingsForeground
        case .octoprint: return OctoTheme.colors.menuStyleOctoPrintForeground
        case .printer: return OctoTheme.colors.menuStylePrinterForeground
        case .support: return OctoTheme.colors.menuStyleSupportForeground
        default: return OctoTheme.colors.menuStyleNeutralForeground
        }
    }
    
    var background: Color {
        switch self {
        case .settings: return OctoTheme.colors.menuStyleSettingsBackground
        case .octoprint: return OctoTheme.colors.menuStyleOctoPrintBackground
        case .printer: return OctoTheme.colors.menuStylePrinterBackground
        case .support: return OctoTheme.colors.menuStyleSupportBackground
        default: return OctoTheme.colors.menuStyleNeutralBackground
        }
    }
}
