//
//  MenuView.swift
//  OctoApp
//
//  Created by Christian on 02/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct MenuView: View {
    
    let menu: MenuReference
    let staticList: Bool
    
    @StateObject private var viewModel = MenuViewModel()
    @EnvironmentObject private var menuHost: MenuHostViewModel
    @Environment(\.scenePhase) private var scenePhase
    @Environment(\.instanceSystemInfo) private var systemInfo
    @Namespace private var animation
    @State private var state : MenuState = .initial
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            switch state {
            case .initial: Color.clear.frame(height: 0)
            case .loading: MenuLoading(title: menu.reference.title)
            case .failed: ScreenError().padding(OctoTheme.dimens.margin3)
            case .empty(let state): MenuEmpty(state: state)
            case .loaded(let state): MenuLoaded(state: state, staticList: staticList)
            }
        }
        .withInstanceId { _, id in viewModel.initWithInstanceId(id) }
        .onChange(of: menu) { _, m in viewModel.loadMenu(menu: m, systemInfo: systemInfo) }
        .onAppear {
            if viewModel.state == .initial {
                viewModel.loadMenu(menu: menu, systemInfo: systemInfo)
                menuHost.reloadAction = { menu in
                    try await viewModel.doLoadMenu(menu: menu, systemInfo: systemInfo)
                }
            }
        }
        .onChange(of: scenePhase) { _, newPhase in
            if newPhase == .active {
                viewModel.loadMenu(menu: menu, systemInfo: systemInfo)
            }
        }
        .onChange(of: viewModel.state) { _, x in
            // We need a global animation here to resize the sheet, use withAnimation and copy state
            withAnimation {
                self.state = x
            }
        }
    }
}

@MainActor
private class MenuViewModel : ObservableObject {
    private var bag: Set<AnyCancellable> = []
    private var tasks: Set<Task<Void, any Error>> = []
    private var currentCore: MenuViewModelCore?
    private var loadId: String = ""
    
    @Published var state: MenuState = .initial
    private var loadedSnapshot: MenuLoadedState? {
        switch state {
        case .loaded(let x): return x
        default: return nil
        }
    }
    
    func initWithInstanceId(_ instanceId: String) {
        let core = MenuViewModelCore(instanceId: instanceId)
        currentCore = core
    }
    
    func loadMenu(
        menu: MenuReference,
        systemInfo: SystemInfoStruct
    ) {
        loadId = UUID().uuidString
        tasks.forEach { $0.cancel() }
        tasks.removeAll()
        
        // If we need more than 150ms to build the menu, move to loading
        // We need to loadId as the task sometimes still runs after it was cancelled...
        let deferredLoading = Task {
            let myLoadId = loadId
            try await Task.sleep(for: .milliseconds(150))
            
            // Run atomically on main
            await MainActor.run {
                if myLoadId == loadId {
                    Napier.d(tag: "MenuView", message: "Applying deferred loading state for \(menu.id)")
                    state = .loading(title: menu.reference.title)
                } else {
                    Napier.d(tag: "MenuView", message: "Skipping deferred loading state for \(menu.id), load id changed")
                }
            }
        }
        
        let loadItems = Task {
            try await doLoadMenu(
                menu: menu,
                systemInfo: systemInfo,
                deferredLoading: deferredLoading
            )
        }
        
        tasks.insert(deferredLoading)
        tasks.insert(loadItems)
    }
    
    func doLoadMenu(
        menu: MenuReference,
        systemInfo: SystemInfoStruct,
        deferredLoading: Task<Void, any Error>? = nil
    ) async throws {
        do {
            Napier.d(tag: "MenuView", message: "Loading \(menu.id)")
            guard let items = try await loadMenuItems(menu: menu, systemInfo: systemInfo) else {
                self.state = .failed
                deferredLoading?.cancel()
                return
            }
            
            if items.isEmpty {
                Napier.d(tag: "MenuView", message: "Loaded \(menu.id) with empty content")
                deferredLoading?.cancel()
                self.state = .empty(
                    MenuEmptyState(
                        title: menu.reference.title,
                        subtitle: menu.reference.emptyStateSubtitle,
                        actionText: menu.reference.emptyStateActionText,
                        animation: menu.reference.emptyStateAnimation,
                        action: { mh in menu.reference.emptyStateAction(mh)?.description() }
                    )
                )
            } else {
                Napier.d(tag: "MenuView", message: "Loaded \(menu.id) with \(items.count) items")
                observeTitles(menu: menu.reference, items: items, deferredLoading: deferredLoading)
            }
        } catch {
            Napier.e(tag: "MenuView", message: "Failed to load! \(error)")
            deferredLoading?.cancel()
            loadId = ""
            self.state = .failed
            throw error
        }
    }
    
    private func observeTitles(
        menu: OctoAppBase.Menu,
        items: [[MenuItemReference]],
        deferredLoading: Task<Void, any Error>?
    ) {
        bag.forEach { $0.cancel() }
        bag.removeAll()
        
        let subtitlePublisher : AnyPublisher<NSString, Never> = menu.subtitleFlow?.asPublisher().eraseToAnyPublisher() ?? Just("").eraseToAnyPublisher()
        let bottomTextPublisher :  AnyPublisher<NSString, Never> = menu.bottomTextFlow?.asPublisher().eraseToAnyPublisher() ?? Just("").eraseToAnyPublisher()
        
        subtitlePublisher.combineLatest(bottomTextPublisher) { subtitle, bottomText in
            Napier.d(tag: "MenuView", message: "Received subtitle=\"\(subtitle)\" bottomText=\"\(bottomText)\"")
            
            // We are ready to emit a loaded value, cancel deferred loading
            self.loadId = ""
            deferredLoading?.cancel()
            
            Napier.d(tag: "MenuView", message: "Loaded, \(items.count) groups")
            return .loaded(
                MenuLoadedState(
                    items: items,
                    title: menu.title,
                    subtitle: subtitle.length == 0 ? nil : subtitle as String,
                    bottomText: bottomText.length == 0 ? nil : bottomText as String,
                    headerViewType: menu.customHeaderViewType,
                    viewType: menu.customViewType
                )
            )
        }.sink { state in
            self.state = state
        }.store(in: &bag)
    }
    
    
    private func loadMenuItems(
        menu: MenuReference,
        systemInfo: SystemInfoStruct
    ) async throws -> [[MenuItemReference]]? {
        let x = try await menu.reference.getMenuItems()
        let suppressGrouping = menu.reference.suppressGrouping
        let pinnedItems = currentCore?.getPinnedItems(menuId: menu.menuId)
        var items: [[MenuItemReference]] = suppressGrouping ? [[]] : []
        var lastGroup: String? = nil
        guard let context = try await currentCore?.determineContext() else {
            return nil
        }
        let sorted = x.sorted {
            if $0.order == $1.order {
                return $0.title.lowercased() < $1.title.lowercased()  // Sort alphabetically if order is the same
            }
            return $0.order < $1.order  // Otherwise, sort by order
        }
        let systemInfoClass = systemInfo.toClass()
        
        for item in sorted {
            let visible = (try? await item.isVisible(context: context, systemInfo: systemInfoClass))?.boolValue == true
            
            guard visible  else { continue }
            guard item.supportedTargets.contains(.darwin) else { continue }

            // Check if enabled
            var featureDisabled = false
            let enabled = (try? await item.isEnabled(context: context, systemInfo: systemInfoClass))?.boolValue == true
            if let f = item.billingManagerFeature {
                featureDisabled = !BillingManager.shared.isFeatureEnabled(feature: f)
            }
            
            // Different group id? Start new group
            if item.groupId != lastGroup && !suppressGrouping {
                items.append([])
                lastGroup = item.groupId
            }
            
            let ref = MenuItemReference(
                reference: item,
                enabled: enabled && !featureDisabled,
                featureDisabled: featureDisabled,
                pinned: pinnedItems?.contains(item.itemId) == true,
                badgeCount: Int(item.badgeCount)
            )
            items[items.count - 1].append(ref)
        }
        
        return items
    }
}

 enum MenuState : Equatable {
    case initial
    case loading(title: String)
    case empty(MenuEmptyState)
    case loaded(MenuLoadedState)
    case failed
}

struct MenuLoadedState : Equatable {
    let items: [[MenuItemReference]]
    let title: String
    let subtitle: String?
    let bottomText: String?
    let headerViewType: String?
    let viewType: String?
}

struct MenuEmptyState : Equatable {
    static func == (lhs: MenuEmptyState, rhs: MenuEmptyState) -> Bool {
        lhs.actionText == rhs.actionText && lhs.subtitle == rhs.subtitle && lhs.animation == rhs.animation
    }
    
    let title: String
    let subtitle: String?
    let actionText: String?
    let animation: MenuAnimation?
    let action: (OctoAppBase.MenuHost?) -> Void
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuHost(menu: SettingsMenu(), menuId: .other)
    }
}
