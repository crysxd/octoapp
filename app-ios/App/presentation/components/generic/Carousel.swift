//
//  Carousel.swift
//  OctoApp
//
//  Created by Christian Würthner on 25/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

// Example was taken from here
// https://www.xtabbas.com/implementing-snap-carousel-in-swiftui/
/// FullscreenCarouselCard is a wrapper view with fixed width and VStack
struct FullscreenCarouselCard<Content: View, ItemData: Identifiable>: View {
    private let content: Content
    private let width: CGFloat
    init(
        _ itemData: ItemData,
        width: CGFloat,
        @ViewBuilder content: (_ index: Int, _ itemData: ItemData) -> Content,
        index: Int
    ) {
        self.width = width
        self.content = content(index, itemData)
    }
    var body: some View {
        VStack(spacing: 0) {
            self.content
        }
        .frame(width: width)
    }
}

/// FullscreenCarousel is by the title full screen only, there is no way to swipe more than 1 card
/// the size of the card is the percentage from screen 50-100% cards would be visible by both sides
struct FullscreenCarouselView<Content: View, ItemData: Identifiable>: View {
    /// iterator content property
    private let content: (_ index: Int, _ itemData: ItemData) -> Content
    /// spacing is required to calculate proper offset
    private let spacing: CGFloat
    /// ItemData to pass to iterator content
    let itemsData: [ItemData]
    
    @State private var screenDrag: Float = 0.0
    @State private var activeCardInternal = -1
    @State private var calcOffset: CGFloat
    private var activeCard: Binding<Int>
    
    private let cardWidth: CGFloat
    private let numberOfItems: CGFloat
    // think about passing it from top
    private let screenWidth = UIScreen.main.bounds.width
    private let cardWithSpacing: CGFloat
    /// xOffset to shift HStack emulating scroll
    private let xOffsetToShift: CGFloat
    
    init(
        spacing: CGFloat,
        itemsData: [ItemData],
        cardWidth: CGFloat,
        activeCard: Binding<Int>,
        @ViewBuilder content: @escaping (Int, ItemData) -> Content
    ) {
        self.spacing = spacing
        self.cardWidth = cardWidth
        self.numberOfItems = CGFloat(itemsData.count)
        self.cardWithSpacing = cardWidth + spacing
        self.xOffsetToShift = cardWithSpacing * numberOfItems / 2 - cardWithSpacing / 2
        self._calcOffset = .init(wrappedValue: self.xOffsetToShift)
        self.itemsData = itemsData
        self.content = content
        self.activeCard = activeCard
    }
    
    var body: some View {
        return HStack(spacing: spacing) {
            ForEach(Array(itemsData.enumerated()), id: \.offset) { index, singleItemData in
                FullscreenCarouselCard(
                    singleItemData,
                    width: cardWidth,
                    content: content,
                    index: index
                )
            }
        }
        .offset(x: calcOffset, y: 0)
        .animation(.easeInOut(duration: 0.15), value: calcOffset)
        .gesture(snapDrag)
        .onChange(of: activeCard.wrappedValue) { _, active in
            calculateOffset(0)
            activeCardInternal = active
        }
        .onChange(of: activeCardInternal) { _, active in
            calculateOffset(0)
            activeCard.wrappedValue = active
        }
        .task {
            activeCardInternal = activeCard.wrappedValue
        }
    }
    
    var snapDrag: some Gesture {
        DragGesture()
            .onChanged { currentState in
                self.calculateOffset(Float(currentState.translation.width))
            }
            .onEnded { value in
                self.handleDragEnd(value.translation.width)
            }
    }
    
    /// calculating proper offset for next slide
    func calculateOffset(_ screenDrag: Float) {
        let activeOffset = xOffsetToShift - (cardWithSpacing * CGFloat(activeCard.wrappedValue))
        let nextOffset = xOffsetToShift - (cardWithSpacing * CGFloat(activeCard.wrappedValue + 1))
        calcOffset = activeOffset
        if activeOffset != nextOffset {
            calcOffset = activeOffset + CGFloat(screenDrag)
        }
    }
    
    func handleDragEnd(_ translationWidth: CGFloat) {
        let impactMed = UIImpactFeedbackGenerator(style: .medium)
        if translationWidth < -50 && CGFloat(activeCard.wrappedValue) < numberOfItems - 1 {
            activeCard.wrappedValue += 1
            impactMed.impactOccurred()
        }
        if translationWidth > 50 && activeCard.wrappedValue != 0 {
            activeCard.wrappedValue -= 1
            impactMed.impactOccurred()
        }
        self.calculateOffset(0)
    }
    
}

private struct TestStruct: Identifiable, Hashable {
    let id = UUID()
    let test = "Test"
}

//struct FullscreenCarouselView_Previews: PreviewProvider {
//    static var previews: some View {
//        StatefulPreviewWrapper(0) { active in
//            VStack {
//                FullscreenCarouselView(
//                    spacing: 20,
//                    itemsData: [TestStruct(), TestStruct(), TestStruct(), TestStruct()],
//                    zoomFactor: 0.7,
//                    activeCard: active
//                ) { _, itemData in
//                    // this view is wrapped in VStack with proper width
//                    VStack {
//                        VStack {
//                            Text("some thing \(itemData.test)")
//                                .frame(maxWidth: .infinity)
//                                .aspectRatio(9/16, contentMode: .fit)
//                                .background(Color.red)
//                        }
//                    }
//                }
//
//                Text("Active: \(active.wrappedValue)")
//            }
//        }
//    }
//}
