//
//  PicassoImage.swift
//  OctoApp
//
//  Created by Christian on 21/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

let NetworkImageLoadingMarker: String = "loading"

@MainActor
struct OctoPrintNetworkImage: View {
    var url: String?
    var backupSystemName: String = "photo"
    var backupColor: Color = OctoTheme.colors.accent
    var small = true
    
    @State private var state: ImageState = .loading
    @State var onCancel: () -> Void = {}
    @Environment(\.instanceId) private var instanceId
    
    var body: some View {
        ZStack {
            switch(state){
            case .loading: ProgressView()
                    .if(small) { $0.scaleEffect(0.66) }
                    .transition(.opacity)
                
            case .loaded(let image): Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .transition(.opacity)
                
            case .failed: Image(systemName: backupSystemName)
                    .foregroundColor(backupColor)
                    .font(.system(size: 24))
                    .transition(.opacity)
            }
        }
        .animation(.default, value: state)
        .task(id: url, priority: .userInitiated) {
            await loadImage()
        }
        .onDisappear {
            // Ensure the Kotlin Deferred is cancelled...our task modifier doesn't seem to
            // properly cancel
            onCancel()
        }
    }
    
    private func loadImage() async {
        // Double triple ensure previous cancelled
        onCancel()
        
        // Loading marker?
        guard url != NetworkImageLoadingMarker else {
            state = .loading
            return
        }
        
        // URL valid?
        guard let checkedUrl = url else {
            state = .failed
            return
        }
        
        // Already cached?
        let cacheKey = "\(instanceId)/\(checkedUrl)"
        if let cached = getImage(cacheKey: cacheKey) {
            state = .loaded(cached)
            return
        }
        
        // Load
        state = .loading
        Napier.d(tag: "OctoPrintNetworkImage", message: "Loading \(cacheKey)")
        do {
            let deferredResponse = try await SimpleNetworkRequestKt.simpleNetworkRequest(
                instanceId: instanceId,
                path: checkedUrl,
                pathEncoded: true
            )
            
            onCancel = {
                deferredResponse.cancel()
            }
            
            let image = try await deferredResponse.awaitImageResponse()
            if let image = image {
                insertAndLimit(image: image, cacheKey: cacheKey)
            }

            await MainActor.run {
                if let image = image {
                    Napier.d(tag: "OctoPrintNetworkImage", message: "Loaded \(cacheKey)")
                    state = .loaded(image)
                } else {
                    Napier.d(tag: "OctoPrintNetworkImage", message: "Failed \(cacheKey)")
                    state = .failed
                }
            }
        } catch is CancellationError {
            onCancel()
        } catch {
            state = .failed
        }
    }
}

private var LruCache = Dictionary<String, CacheEntry>()
private let lock = NSLock()

private func getImage(cacheKey: String) -> UIImage? {
    lock.lock()
    let image = LruCache[cacheKey]?.image
    if let image = image {
        LruCache[cacheKey] = CacheEntry(lastUsed: Date(), image: image, cacheKey: cacheKey)
    }
    lock.unlock()
    return image
}

private func insertAndLimit(image: UIImage, cacheKey: String) {
    lock.lock()
    LruCache[cacheKey] = CacheEntry(lastUsed: Date(), image: image, cacheKey: cacheKey)
    
    // Limit cache size to 25 images, delete oldest until limit is adhered to
    while (LruCache.count > 25) {
        let oldest = LruCache.values.sorted(by: { $0.lastUsed < $1.lastUsed })[0]
        LruCache.removeValue(forKey: oldest.cacheKey)
    }
    lock.unlock()
}

private struct CacheEntry {
    var lastUsed: Date
    var image: UIImage
    var cacheKey: String
}

private enum ImageState: Equatable {
    case loading, loaded(UIImage), failed
}

struct PicassoImage_Previews: PreviewProvider {
    static var previews: some View {
        OctoPrintNetworkImage(url: "http://google.com")
            .previewDisplayName("Failed")
        OctoPrintNetworkImage(url: NetworkImageLoadingMarker)
            .previewDisplayName("Loading")
    }
}
