//
//  OctoButton.swift
//  App
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct OctoButton: View {
    
    var icon: String? = nil
    var text: String
    var type: OctoButtonType = .primary
    var small = false
    var loading = false
    var fillWidth = false
    var fillHeight = false
    var enabled = true
    var clickListener: () -> Void
    var longClickListener: () -> Void = {}
    
    @State private var handled = false
    private var active: Bool { return enabled && !loading }
    private var isLink: Bool {
        switch type {
        case .link, .linkColoredBackground, .linkWithCustomColor(_): return true
        default: return false
        }
    }
    private var doFillWidth: Bool {
        (!small || type == .surface) || fillWidth
    }
    
    var body: some View {
        Button(action: {}) {
            ZStack {
                // Just to keep height constant
                Text(" ").padding(OctoTheme.dimens.margin0)
                    .fixedSize(horizontal: false, vertical: true)
                
                
                HStack(spacing: OctoTheme.dimens.margin01) {
                    if let i = icon {
                        Image(systemName: i)
                            .foregroundColor(textColor(type: type, active: active))
                    }
                    
                    if !text.isEmpty {
                        Text(text)
                            .foregroundColor(textColor(type: type, active: active))
                            .shadow(color: .clear, radius: 0)
                            .opacity(loading ? 0 : 1)
                            .transition(.opacity)
                            .lineLimit(1)
                    }
                }
                .if(!isLink) { $0.typographyButton(small: small) }
                .if(isLink) { $0.typographyButtonLink() }
                
                if(loading) {
                    ProgressView()
                        .transition(.opacity)
                        .tint(textColor(type: type, active: active))
                }
            }
        }
        .buttonStyle(OctoButtonStyle(type: type, small: small, fillWidth: doFillWidth, fillHeight: fillHeight, active: active))
        .animation(.default, value: loading)
        .simultaneousGesture(
            LongPressGesture().onEnded { _ in
                if active {
                    longClickListener()
                    
                    let generator = UINotificationFeedbackGenerator()
                    generator.notificationOccurred(.success)
                }
            }
        )
        .highPriorityGesture(
            TapGesture().onEnded { _ in
                if active {
                    clickListener()
                }
            }
        )
    }
}

struct OctoAsyncButton : View {
    
    var icon: String? = nil
    var text: String
    var type: OctoButtonType = .primary
    var small = false
    var fillWidth = false
    var enabled = true
    var loading = false
    var clickListener: () async throws -> Void
    var longClickListener: () -> Void = {}
    
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    @MainActor @State private var internalLoading: Bool = false

    var body: some View {
        OctoButton(
            icon: icon,
            text: text,
            type: type,
            small: small,
            loading: loading || internalLoading,
            fillWidth: fillWidth,
            enabled: enabled,
            clickListener: {
                internalLoading = true
                Task {
                    do {
                        try await clickListener()
                    } catch {
                        orchestrator.postError(error: KotlinThrowable(message: "Error in Swift: \(error)"))
                    }
                    internalLoading = false
                }
            },
            longClickListener: longClickListener
        )
    }
}

struct OctoMenuButton: View {
    
    @State private var pressed = false
    var clickListener: () -> Void
    
    var body: some View {
        Button(action: clickListener) {
            ZStack {
                Text("X")
                    .opacity(0)
                    .typographyButton(small: true)
                
                Image(systemName: "line.3.horizontal")
                    .padding(.trailing, OctoTheme.dimens.margin1)
                    .shadow(color: .clear, radius: 0)
                    .foregroundColor(OctoTheme.colors.accent)
                    .typographyButton(small: true)
            }
        }
        .buttonStyle(OctoButtonStyle(type: .secondary, small: true, fillWidth: false, fillHeight: false, active: true))
    }
}

enum OctoButtonType : Equatable {
    case primary, primaryUnselected, secondary, link, surface, linkColoredBackground
    case linkWithCustomColor(textColor: Color)
    case special(background: Color, foreground: Color, stroke: Color)
}

private func backgroundColor(type: OctoButtonType, active: Bool) -> Color {
    if (!active) {
        switch(type){
        case .linkWithCustomColor(_), .link, .linkColoredBackground: return .clear
        default: return OctoTheme.colors.accent
        }
    }
    
    switch(type){
    case .primary, .surface: return OctoTheme.colors.accent
    case .linkWithCustomColor(_), .link, .linkColoredBackground, .primaryUnselected: return .clear
    case .secondary: return OctoTheme.colors.inputBackground
    case .special(background: let c, foreground: _, stroke: _): return c
    }
}

private func borderColor(type: OctoButtonType) -> Color {
    switch type {
    case .linkWithCustomColor(_), .surface, .linkColoredBackground, .primaryUnselected, .link: return .clear
    case .primary, .secondary: return OctoTheme.colors.accent
    case .special(background: _, foreground: _, stroke: let c): return c
    }
}

private func textColor(type: OctoButtonType, active: Bool) -> Color {
    if !active {
        return OctoTheme.colors.darkGrey.opacity(0.66)
    }
    
    switch type {
    case .linkWithCustomColor(textColor: let textColor): return textColor
    case .linkColoredBackground: return OctoTheme.colors.darkText
    case .primary, .surface: return OctoTheme.colors.textColoredBackground
    case .link: return OctoTheme.colors.accent
    case .secondary, .primaryUnselected: return OctoTheme.colors.accent
    case .special(background: _, foreground: let c, stroke: _): return c
    }
}

private func hasShadow(type: OctoButtonType, active: Bool, small: Bool) -> Bool {
    if !active {
        return false
    }
    
    if small {
        return false
    }
    
    switch(type){
    case .primary, .secondary, .special(_, _ ,_ ): return true
    default: return false
    }
}

private struct OctoButtonStyle: ButtonStyle {
    
    let type: OctoButtonType
    let small: Bool
    let fillWidth: Bool
    let fillHeight: Bool
    let active: Bool
    
    func makeBody(configuration: Self.Configuration) -> some View {
        let activelyPressed = configuration.isPressed && active
        
        configuration.label
            .frame(maxWidth: fillWidth ? .infinity : nil)
            .frame(maxHeight: fillHeight ? .infinity : nil)
            .if(!fillWidth) { $0.padding([.leading, .trailing], OctoTheme.dimens.margin2) }
            .if(!fillHeight) { $0.padding([.top, .bottom], small ? OctoTheme.dimens.margin1 : OctoTheme.dimens.margin2) }
            .if(type == .surface) {
                $0
                    .background(Rectangle().strokeBorder(borderColor(type: type), lineWidth: 1))
                    .background(Rectangle().fill(activelyPressed ? OctoTheme.colors.clickIndicatorOverlay : .clear))
                    .background(Rectangle().fill(backgroundColor(type: type, active: active)))
            }
            .if(type != .surface) {
                $0
                    .background(Capsule().strokeBorder(borderColor(type: type), lineWidth: 1))
                    .background(Capsule().fill(activelyPressed ? OctoTheme.colors.clickIndicatorOverlay : .clear))
                    .background(Capsule().fill(backgroundColor(type: type, active: active)))
            }
            .if(hasShadow(type: type, active: active, small: small)) { $0.background(Capsule().shadow(radius: activelyPressed ? 8 : 3)) }
            .scaleEffect(activelyPressed && !small ? OctoTheme.dimens.buttonActiveScale : 1)
    }
}

struct OctoButton_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 15) {
            OctoButton(text: "Primary", type: .primary) {}
            OctoButton(text: "Primary", type: .primary, enabled: false) {}
            OctoButton(text: "Primary", type: .primary, loading: true) {}
            
            OctoButton(text: "Secondary", type: .secondary) {}
            OctoButton(text: "Link", type: .link) {}
            OctoButton(text: "Surface", type: .surface) {}
            OctoButton(text: "Primary Small", type: .primary, small: true) {}
            OctoButton(icon: "paintbrush.fill", text: "Primary Small", type: .primary, small: true) {}
            OctoButton(icon: "play.fill", text: "", type: .primary, small: true) {}
            OctoMenuButton {}
        }
    }
}

