//
//  TextField.swift
//  App
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct OctoTextField: View {
    // placeolder should not be LocalizedStringKey as this would parse "Markdown", e.g. links
    var placeholder: String
    var labelActiv: LocalizedStringKey
    var alternativeBackgroung = false
    
    var input: Binding<String>
    var autocorrectionDisabled = false
    var multiline = false
    var autoFocus = false
    var showSubmit = false
    
    var onSubmitButton: () async -> Void = {}
    
    @State private  var placeholderShown = true
    @Namespace private var animation
    @FocusState private var focused: Bool
    @State private var text: String = ""
    
    private var placeholderOpacity: CGFloat {
        if !input.wrappedValue.isEmpty {
            return 0.0001
        } else if focused {
            return 1
        } else {
            return 0.66
        }
    }
    
    var body: some View {
        content
        .padding(OctoTheme.dimens.margin12)
        .surface(color: alternativeBackgroung ? .alternative : .normal, highlighted: focused)
        .animation(.spring(), value: focused)
        .onTapGesture { focused = true }
        .if(autocorrectionDisabled) { $0.disableAutocorrection(true) }
        .onAppear {
            if autoFocus {
                focused = true
            }
        }
        .animation(.default, value: input.wrappedValue.isEmpty)
    }
    
    private var content: some View {
        ZStack(alignment: .leading) {
            // Just to maintain hight
            HStack {
                VStack {
                    activeLabel
                    inputWithPlaceholder
                    
                }
                if showSubmit {
                    submit
                }
            }
            .opacity(0)
    

            inputWithPlaceholderAndLabel
        }
    }
    
    private var inputWithPlaceholderAndLabel: some View {
        HStack {
            VStack(alignment: .leading) {
                if !input.wrappedValue.isEmpty || focused {
                    activeLabel
                }
                
                inputWithPlaceholder
            }
            
            if showSubmit {
                submit
            }
        }
    }
    
    @ViewBuilder
    private var submit: some View {
        if !input.wrappedValue.isEmpty {
            OctoAsyncIconButton(icon: "paperplane.fill") {
                await onSubmitButton()
            }
            .transition(.opacity.combined(with: .move(edge: .trailing)))
        } else if focused {
            OctoIconButton(icon: "keyboard.chevron.compact.down.fill") {
                focused = false
            }
            .transition(.opacity.combined(with: .move(edge: .trailing)))
        }
    }
    
    private var activeLabel: some View {
        Text(labelActiv)
            .foregroundColor(focused ? OctoTheme.colors.accent : OctoTheme.colors.lightText)
            .typographyLabel()
            .lineLimit(1)
            .padding([.bottom], -OctoTheme.dimens.margin0)
    }
    
    private var inputWithPlaceholder: some View {
        ZStack(alignment: .leading) {
            placeholderField.opacity(placeholderOpacity)
            inputField
        }
    }
    
    private var placeholderField: some View {
        Text(placeholder)
            .typographyInput()
            .lineLimit(1)
    }
    
    private var inputField: some View {
        TextField(
            "",
            text: input,
            axis: multiline ? .vertical : .horizontal
        )
        .textInputAutocapitalization(autocorrectionDisabled ? .never : .sentences)
        .padding(.bottom, -OctoTheme.dimens.margin12)
        .offset(y: -OctoTheme.dimens.margin12 / 2)
        .focused($focused)
        .lineLimit(...5)
    }
}

struct TextField_Previews: PreviewProvider {
    @State static var input = ""
    
    static var previews: some View {
        StatefulPreviewWrapper(input) { input in
            OctoTextField(placeholder: "Web URL", labelActiv: "Web URL – Copy this from your browser!", alternativeBackgroung: true,  input: $input)
                .previewDisplayName("Alternative")
        }
        
        StatefulPreviewWrapper(input) { input in
            OctoTextField(placeholder: "Web URL", labelActiv: "Web URL – Copy this from your browser!", input: $input)
                .previewDisplayName("Normal")
        }
        
        StatefulPreviewWrapper(input) { input in
            OctoTextField(
                placeholder: "Web URL",
                labelActiv: "Web URL – Copy this from your browser!",
                input: input,
                multiline: true,
                showSubmit: true
            ) {
                input.wrappedValue = ""
            }
            .previewDisplayName("Multiline")
        }
    }
}
