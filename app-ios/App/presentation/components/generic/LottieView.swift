//
//  LottieView.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//


import SwiftUI
import LottieUI
 
struct SimpleLottieView: View {
    let lottieFile: String
    var loop: Bool = false

    var body : some View {
        LottieView(lottieFile)
            .play(true)
            .loopMode(loop ? .loop : .playOnce)
    }
}
