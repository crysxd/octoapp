//
//  FileStartPrintConfirmSettingsView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

@MainActor
private struct StartPrintConfirmations: ViewModifier {
    @State private var showTimelapseConfirmation = false
    @State private var showMaterialConfirmation = false
    @State private var timelapseConfirmed = false
    @State private var materialConfirmed = false
    @Environment(\.instanceId) private var instanceId
    
    var filePath: String
    var result: StartPrintResultStruct
    var startPrinting: (Bool, Bool) async -> Void
    
    func body(content: Content) -> some View {
        content
            .macOsCompatibleSheet(isPresented: $showTimelapseConfirmation) {
                MenuHost(menu: TimelapseMenu(instanceId: instanceId, offerStartPrint: true)) { result in
                    if result == TimelapseMenu.companion.ResultStartPrint {
                        await startPrinting(materialConfirmed, true)
                    }
                }
            }
            .macOsCompatibleSheet(isPresented: $showMaterialConfirmation) {
                MenuHost(menu: MaterialMenu(instanceId: instanceId, startPrintAfterSelectionForPath: filePath)) { result in
                    if result == MaterialMenu.companion.ResultStartPrint {
                        await startPrinting(true, timelapseConfirmed)
                    }
                }
            }
            .onChange(of: result) { _, r in
                Task {
                    // If one is visible, we need to wait before showing the next
                    if (showMaterialConfirmation || showTimelapseConfirmation) && result != .success {
                        showMaterialConfirmation = false
                        showTimelapseConfirmation = false
                        try? await Task.sleep(for: .milliseconds(500))
                    }
                    
                    switch r {
                    case .materialConfirmationRequired(let tc): do {
                        showTimelapseConfirmation = false
                        showMaterialConfirmation = true
                        timelapseConfirmed = tc
                        materialConfirmed = false
                    }
                    case .timelapseConfirmationRequired(let mc): do {
                        showMaterialConfirmation = false
                        showTimelapseConfirmation = true
                        timelapseConfirmed = false
                        materialConfirmed = mc
                    }
                    default: do {
                        showMaterialConfirmation = false
                        showTimelapseConfirmation = false
                        timelapseConfirmed = false
                        materialConfirmed = false
                    }
                    }
                }
            }
    }
}

extension View {
    func startPrintConfirmations(result: StartPrintResultStruct,  filePath: String, startPrinting: @escaping (Bool, Bool) async -> Void) -> some View {
        modifier(StartPrintConfirmations(filePath: filePath, result: result, startPrinting: startPrinting))
    }
}

struct FileStartPrintConfirmSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        Color.clear
            .startPrintConfirmations(result: .success, filePath: "") { _, _ in }
    }
}
