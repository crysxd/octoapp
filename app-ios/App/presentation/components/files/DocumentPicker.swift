//
//  DocumentPicker.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/04/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

import UIKit

@usableFromInline struct DocumentPicker: UIViewControllerRepresentable {
    
    @usableFromInline class Coordinator: NSObject {
        
        var parent: DocumentPicker
        var pickerController: UIDocumentPickerViewController
        var presented = false
        
        init(_ parent: DocumentPicker) {
            self.parent = parent
            self.pickerController = UIDocumentPickerViewController(forOpeningContentTypes: [.data])
            
            // self.pickerController.allowsMultipleSelection = true
            // self.pickerController.directoryURL = URL()
            super.init()
            pickerController.delegate = self
        }
    }
    
    @Binding var isPresented: Bool
    var onCancel: () -> ()
    var onDocumentsPicked: (_: [URL]) -> ()
    
    @usableFromInline init(
        isPresented: Binding<Bool>,
        onCancel: @escaping () -> (),
        onDocumentsPicked: @escaping (_: [URL]) -> ()
    ) {
        self._isPresented = isPresented
        self.onCancel = onCancel
        self.onDocumentsPicked = onDocumentsPicked
    }
    
    @usableFromInline func makeUIViewController(context: Context) -> UIViewController {
        UIViewController()
    }
    
    @usableFromInline func updateUIViewController(_ presentingController: UIViewController, context: Context) {
        let pickerController = context.coordinator.pickerController
        if isPresented && !context.coordinator.presented {
            context.coordinator.presented.toggle()
            presentingController.present(pickerController, animated: true)
        } else if !isPresented && context.coordinator.presented {
            context.coordinator.presented.toggle()
            pickerController.dismiss(animated: true)
        }
    }
    
    @usableFromInline  func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
}

extension DocumentPicker.Coordinator: UIDocumentPickerDelegate {
    @usableFromInline func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        parent.isPresented.toggle()
        parent.onDocumentsPicked(urls)
    }
    
    @usableFromInline func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        parent.isPresented.toggle()
        parent.onCancel()
    }
}

public extension View {
    
    @inlinable func documentPicker(
        isPresented: Binding<Bool>,
        onCancel: @escaping () -> () = { },
        onDocumentsPicked: @escaping (_: [URL]) -> () = { _ in }
    ) -> some View {
        Group {
            self
            DocumentPicker(isPresented: isPresented, onCancel: onCancel, onDocumentsPicked: onDocumentsPicked)
        }
    }
}
