//
//  FileListItem.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct FileListItem: View {
    
    var file: FileObjectStruct
    @State var showMenu = false
    @State var showDeleteConfirmation = false
    @EnvironmentObject private var fileActionsViewModel: FileActionsViewModel
    @Environment(\.instanceId) private var instanceId
    
    private var defaultIcon: String {
        if (file.isFolder) {
            return "folder.fill"
        } else if (file.isPrintable) {
            return "printer.fill"
        } else {
            return "doc.fill"
        }
    }
    private var statusIcon: String {
        switch(file.prints?.lastPrintSuccess) {
        case false: return "x.circle.fill"
        case true: return "checkmark.circle.fill"
        default: return ""
        }
    }
    private var detail: String {
        if (file.isFolder) {
            return ""
        } else {
            var date: String? = nil
            var size: String? = nil
            
            if let d = file.date {
                date = d.formatted()
            }
            
            if let s = file.size {
                size = "(\(s.asStyleFileSize()))"
            }
            
            return [date, size].filter { $0 != nil }.map { $0!! }.joined(separator: " ")
        }
    }
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin12) {
            ZStack(alignment: .bottomTrailing) {
                OctoPrintNetworkImage(
                    url: file.smallThumbnail,
                    backupSystemName: defaultIcon
                )
                .frame(width: 48, height: 48)
                .padding(OctoTheme.dimens.margin01)
                .foregroundColor(OctoTheme.colors.accent)
                .surface()
                
                Image(systemName: statusIcon)
                    .font(.system(size: 16))
                    .offset(x: 2, y: 2)
                    .foregroundColor(OctoTheme.colors.lightText)
            }
            
            VStack(spacing: OctoTheme.dimens.margin01) {
                Text(file.display)
                    .typographySubtitle()
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .lineLimit(2)
                    .truncationMode(.middle)
                if (!file.isFolder) {
                    Text(detail)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .typographyLabelSmall()
                }
            }
        }
        .if(BillingManager.shared.isFeatureEnabled(feature: BillingManagerKt.FEATURE_FILE_MANAGEMENT)) {
            $0.contextMenu { contextMenu }
        }
        .confirmationDialog(
            "",
            isPresented: $showDeleteConfirmation,
            actions: {
                Button("file_manager___file_menu___delete"~, role: .destructive) {
                    fileActionsViewModel.delete(instanceId: instanceId, path: file.path)
                }
            },
            message: {
                Text(String(format:"file_manager___file_menu___delete_confirmation_message"~, file.display))
            }
        )
    }
    
    @ViewBuilder
    var contextMenu: some View {
        Section {
            Button(action: { showDeleteConfirmation = true }) {
                Label("file_manager___file_menu___delete"~, systemImage: "trash.fill")
            }
        }
        Section {
            Button(action: { fileActionsViewModel.prepareRename(instanceId: instanceId, path: file.path, name: file.name) }) {
                Label("file_manager___file_menu___rename"~, systemImage: "pencil")
            }
            Button(action: { fileActionsViewModel.copy(instanceId: instanceId, path: file.path, display: file.display) }) {
                Label("file_manager___file_menu___copy"~, systemImage: "doc.on.doc.fill")
            }
            Button(action: { fileActionsViewModel.cut(instanceId: instanceId, path: file.path, display: file.display) }) {
                Label("file_manager___file_menu___move"~, systemImage: "scissors")
            }
        }
        
        if !file.isFolder, let date = file.date {
            Section {
                Button(
                    action: {
                        fileActionsViewModel.download(
                            instanceId: instanceId,
                            path: file.path,
                            display: file.display,
                            date: date
                        )
                        
                    }
                ) {
                    Label("file_manager___file_menu___download_and_share"~, systemImage: "square.and.arrow.up.fill")
                }
            }
        }
    }
}

struct FileListItem_Previews: PreviewProvider {
    static var previews: some View {
        FileListItem(file: FileObjectStruct(origin: .gcode, display: "Some folder", name: "Some folder", path: "/folder", isFolder: true, isPrintable: false))
            .previewDisplayName("Folder")
        FileListItem(file: FileObjectStruct(origin: .gcode, display: "Some file.gcode", name: "Some file", path: "/folder/some file.gcode", isFolder: false, isPrintable: false))
            .previewDisplayName("File")
        FileListItem(file: FileObjectStruct(origin: .gcode, display: "Some file.gcode", name: "Some file", date: Date(timeIntervalSince1970: 3248234234), path: "/folder/some file.gcode", size: 36248234, isFolder: false, isPrintable: true))
            .previewDisplayName("Gcode")
    }
}
