//
//  DeeplinkHandler.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

private let tag = "OrchestratorDeeplinkHandler"
private let fileInstanceListString = "http://fileInstanceList"

private func handleUrl(externalOpenUrl: OpenURLAction, url: URL, inAppOpenUrl: (String) -> Void) {
    if UriLibrary.shared.isInAppLink(urlString: url.description) {
        Napier.d(tag: tag, message: "[LD] Open in app URL \(url)")
        inAppOpenUrl(url.description)
    } else {
        Napier.d(tag: tag, message: "[LD] Open external URL \(url)")
        externalOpenUrl(url)
    }
}

private struct OrchestratorDeeplinkHandler: ViewModifier {
    
    @State private var urls = [String]()
    @State private var sheetShown = false
//    @State private var dialogText: String? = nil
//    @State private var showDialog = false
    @State private var unconsumedDeeplinkToken: String? = nil
    @Environment(\.openURL) var externalOpenUrl
    @Environment(\.instanceId) var instanceId
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    @EnvironmentObject private var windowGroup: WindowGroupViewModel
    @StateObject private var fileActionsViewModel = FileActionsViewModel()
    
    private var isWebcamFullscreen: Bool {
        guard let url = urls.first else { return false }
        return url == UriLibrary.shared.getWebcamUri().description()
    }
    
    var openUrl: OpenURLAction {
        return OpenURLAction { url in
            if urlToRoute(url.description, activeInstanceId: instanceId) == .undefined {
                // Skip, this link was just to open the app
            } else {
                handleUrl(externalOpenUrl: externalOpenUrl, url: url) { urlString in
                    // If the sheet is not shown, reset URLs
                    if !self.sheetShown {
                        self.urls.removeAll()
                    }
                    
                    self.urls.append(urlString)
                    self.sheetShown = true
                }
            }
            
            // Check if the link tells us to activate a instance
            if let instanceToActivate = UrlExtKt.toUrl(url.description).parameters.get(name: "activateInstanceId") {
                orchestrator.activateInstance(instanceId: instanceToActivate)
            }
            
//            // Check if the link tells us to show a dialog
//            if let text = UrlExtKt.toUrl(url.description).parameters.get(name: "showDialog") {
//                dialogText = text
//                showDialog = true
//            }
            
            return OpenURLAction.Result.handled
        }
    }
    
    func body(content: Content) -> some View {
        content
            .macOsCompatibleSheet(isPresented: $sheetShown, fullscreen: isWebcamFullscreen) {
                if !urls.isEmpty {
                    DeeplinkNavigator(path: $urls, showDoneButton: !isWebcamFullscreen)
                }
            }
            .onAppear {
                unconsumedDeeplinkToken = windowGroup.registerForUnconsumedDeeplink { openUrl($0)}
            }
            .onDisappear {
                if let token = unconsumedDeeplinkToken {
                    windowGroup.unregisterForUnconsumedDeeplink(token: token)
                }
            }
//            .alert(Text(dialogText ?? ""), isPresented: $showDialog) {}
            .environment(\.openURL, openUrl)
            .environment(\.openExternalURL, externalOpenUrl)
            .environment(\.dismissToRoot, { sheetShown = false })
            .environmentObject(fileActionsViewModel)
            .usingViewModel(fileActionsViewModel)
    }
}

private struct DeeplinkDoneButton: ViewModifier {
    @Environment(\.dismissToRoot) private var dismissToRoot
    @Environment(\.dismiss) private var dismissNormal
    var url: String
    
    func body(content: Content) -> some View {
        content.toolbar {
            ToolbarItemGroup(placement: .confirmationAction) {
                Button(textForUrl()) { dismiss() }
            }
        }
    }
    
    func textForUrl() -> String {
        let route = urlToRoute(url, activeInstanceId: "")
        switch route {
        case .contact(_): return "cancel"~
        default: return "done"~
        }
    }
    
    func dismiss() {
        let route = urlToRoute(url, activeInstanceId: "")
        switch route {
        case .contact(_): return dismissNormal()
        default: return dismissToRoot()
        }
    }
}

private struct DeeplinkNavigator : View {
    
    @Binding var path: [String]
    @State private var innerPath: [String] = []
    @State private var root: String = ""
    var showDoneButton: Bool
    @Environment(\.openURL) private var externalOpenUrl
    
    private var openUrl: OpenURLAction {
        return OpenURLAction { url in
            handleUrl(externalOpenUrl: externalOpenUrl, url: url) { urlString in
                path.append(urlString)
            }
            
            return OpenURLAction.Result.handled
        }
    }
    
    var body: some View {
        NavigationStack(path: $innerPath) {
            NavigationDestination(
                url: root
            )
            .if(showDoneButton) { $0.deeplinkDoneButton(root) }
            .navigationDestination(for: String.self) { url in
                NavigationDestination(url: url)
                    .deeplinkDoneButton(url)
            }
        }
        .onAppear {
            innerPath = Array(path.dropFirst(1))
            root = path.isEmpty ? root : path[0]
            
            // Special case if we open files. Always use the file instance list as
            // root if we open the file root
            let rootRoute = urlToRoute(root, activeInstanceId: "")
            switch rootRoute {
            case .file(instanceId: _, path: let filePath, label: _): do {
                if filePath == "/" && BillingManager.shared.isFeatureEnabled(feature: BillingManagerKt.FEATURE_FILE_MANAGEMENT){
                    root = fileInstanceListString
                    innerPath = path
                }
            }
            default: break
            }
        }
        .onChange(of: innerPath) { _, new in
            path = Array(arrayLiteral: root) + innerPath
        }
        .onChange(of: path) { _, new in
            innerPath = Array(path.dropFirst(1))
            root = new.isEmpty ? root : new[0]
        }
        .background(OctoTheme.colors.windowBackground)
        .environment(\.openURL, openUrl)
        .errorDialogDisplay()
    }
}

private struct NavigationDestination : View {
    var url: String
    @Environment(\.instanceId) var instanceId
    
    var body: some View {
        let route = urlToRoute(url, activeInstanceId: instanceId)
        
        switch(route) {
        case .undefined: ZStack {} // Never used, link is skipped
        case .fileInstanceList: FileInstanceListView()
        case .file(instanceId: let i, path: let p, label: let l): FileManager(path: p, label: l)
                .environment(\.instanceId, i)
                .environment(\.instanceLabel, l)
        case .help: HelpLauncherView()
        case .helpItem(id: let id): HelpItemView(id: id)
        case .tutorials: HelpTutorialsView()
        case .contact(bugReport: let bugReport): HelpContactView(bugReport: bugReport)
        case .configureRemote: RemoteAccessView()
        case .timelapseArchive: TimelapseArchive()
        case .pluginLibrary(category: let category): PluginsLibrary(category: category)
        case .terminal: Terminal()
        case .webcam: WebcamFullscreen()
        case .dialogMessage(text: let text, title: let title): DialogMessage(text: text, title: title)
        case .billing(transferId: let transferId): PurchaseView(transferId: transferId)
        case .troubleshooting(instanceId: let instanceId): SignInView(repairForInstanceId: instanceId)
        default: ScreenError(title: "help___content_not_available"~)
                .padding(OctoTheme.dimens.margin12)
        }
    }
}

private struct DialogMessage : View {
    
    var text: String
    var title: String?
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationStack {
            ScrollView {
                Text(text)
                    .padding()
            }
            .navigationTitle(title ?? "")
        }
    }
}

private enum NavigationRoute: Equatable {
    case fileInstanceList
    case file(instanceId: String, path: String, label: String)
    case terminal
    case billing(transferId: String?)
    case help
    case tutorials
    case error
    case helpItem(id: String)
    case contact(bugReport: Bool)
    case configureRemote
    case troubleshooting(instanceId: String)
    case timelapseArchive
    case pluginLibrary(category: String?)
    case webcam
    case dialogMessage(text: String, title: String?)
    case undefined
}

private func urlToRoute(_ urlString: String, activeInstanceId: String) -> NavigationRoute {
    let lib = UriLibrary.shared
    let url = UrlExtKt.toUrl(urlString)
    
    if let dialogText = url.parameters.get(name: "dialogText") {
        return .dialogMessage(text: dialogText, title: url.parameters.get(name: "dialogTitle"))
    }
    
    switch(url.normalized()) {
    case fileInstanceListString: return .fileInstanceList
    case lib.getFileManagerUri(instanceId: nil, path: nil, label: nil, folder: nil).normalized(): return .file(
        instanceId: url.parameters.get(name: "instanceId") ?? activeInstanceId,
        path: lib.secureDecodeParam(url: url, name: "path", defaultValue: "/"),
        label: lib.secureDecodeParam(url: url, name: "label", defaultValue: "")
    )
    case lib.getFaqUri(faqId: "id").normalized(): return .helpItem(
        id: lib.secureDecodeParam(url: url, name: "faqId", defaultValue: url.parameters.get(name: "faqIdPlain") ?? "unknown")
    )
    case lib.getContactUri(bugReport: false).normalized(): return .contact(
        bugReport: url.parameters.get(name: "bugReport") == "true"
    )
    case lib.getFixOctoPrintConnectionUri(baseUrl: UrlExtKt.toUrl(""), instanceId: "").normalized(): return .troubleshooting(
        instanceId: url.parameters.get(name: "instanceId") ?? activeInstanceId
    )
    case lib.getPluginLibraryUri(category: nil).normalized(): return .pluginLibrary(
        category: url.parameters.get(name: "category")
    )
    case lib.getTerminalUri().normalized(): return .terminal
    case lib.getHelpUri().normalized(): return .help
    case lib.getTutorialsUri().normalized(): return .tutorials
    case lib.getWebcamUri().normalized(): return .webcam
    case lib.getTerminalUri().normalized(): return .terminal
    case lib.getConfigureRemoteAccessUri().normalized(): return .configureRemote
    case lib.getTimelapseArchiveUri().normalized(): return .timelapseArchive
    case lib.getPurchaseUri().normalized(): return .billing(
        transferId: url.parameters.get(name: "transferId")
    )
    case lib.getLaunchAppUri().normalized(): return .undefined
        
    default: do {
        Napier.e(tag: tag, message: "Failed to determine route for \(url)")
        return .error
    }
    }
}

private extension Ktor_httpUrl{
    func normalized() -> String {
        return withoutQuery()
            .description()
            .replacingOccurrences(of: "https://", with: "http://")
    }
}


extension View {
    func deeplinkNavigation() -> some View {
        modifier(OrchestratorDeeplinkHandler())
    }
}

private extension View {
    func deeplinkDoneButton(_ url: String) -> some View {
        modifier(DeeplinkDoneButton(url: url))
    }
}

extension OpenURLAction {
    public func callAsFunction(_ url: Ktor_httpUrl) {
        self(URL(string: url.description())!)
    }
    public func callAsFunction(_ url: String) {
        self(URL(string: url)!)
    }
}
