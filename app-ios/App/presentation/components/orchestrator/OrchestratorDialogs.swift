//
//  OrchestratorDialogs.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

private struct ErrorReceiver: ViewModifier {
    
    @EnvironmentObject private var viewModel: OrchestratorViewModel
    
    @State private var showErrorDialog = false
    @State private var showErrorActionDialog = false
    @State private var showErrorDetailsDialog = false
    
    
    func body(content: Content) -> some View {
        ZStack {
            ZStack {}.alert(viewModel.errorDialog?.message ?? "??", isPresented: $showErrorActionDialog) {
                Button("sign_in___continue"~, role: .cancel) {
                    viewModel.errorAction()
                }
            }
            ZStack {}.alert(isPresented: $showErrorDetailsDialog) {
                Alert(
                    title: Text(LocalizedStringKey(viewModel.errorDialog?.message ?? "??")),
                    message: Text(LocalizedStringKey(viewModel.errorDialog?.detailMessage ?? "??")),
                    dismissButton: .default(Text("OK"~))
                )
            }
            ZStack {}.alert(isPresented: $showErrorDialog) {
                Alert(
                    title: Text(LocalizedStringKey(viewModel.errorDialog?.message ?? "??")),
                    primaryButton: .default(Text("show_details"~)) {
                        Task {
                            try? await Task.sleep(for: .milliseconds(100))
                            showErrorDetailsDialog = true
                        }
                    },
                    secondaryButton: .default(Text("OK"~)) { }
                )
            }
            
            content
        }
        .onChange(of: viewModel.errorDialog) { _, newError in
            showErrorDialog = false
            showErrorActionDialog = false
            showErrorDetailsDialog = false
            
            // Ensure error just happend
            guard newError != nil && (newError!.date + TimeInterval(3)) > Date() else {
                return
            }
            
            Task {
                try? await Task.sleep(for: .milliseconds(100))
                let generalError = newError != nil && newError?.hasAction != true
                showErrorDialog = generalError && newError?.showDetailsDirectly == false
                showErrorDetailsDialog = generalError && newError?.showDetailsDirectly == true
                showErrorActionDialog = newError != nil && newError?.hasAction == true
            }
        }
    }
}


extension View {
    func errorDialogDisplay() -> some View {
        modifier(ErrorReceiver())
    }
}

private struct OrchestratorErrorKey: EnvironmentKey {
    static let defaultValue: OrchestratorError? = nil
}

private struct TestKey: EnvironmentKey {
    static let defaultValue: Int  = 0
}

private struct OrchestratorErrorActionKey: EnvironmentKey {
    static let defaultValue: (() -> Void)? = nil
}

private extension EnvironmentValues {
    var orchestratorError: OrchestratorError? {
        get { self[OrchestratorErrorKey.self] }
        set { self[OrchestratorErrorKey.self] = newValue }
    }
    var orchestratorErrorAction: (() -> Void)? {
        get { self[OrchestratorErrorActionKey.self] }
        set { self[OrchestratorErrorActionKey.self] = newValue }
    }
    var test: Int {
        get { self[TestKey.self] }
        set { self[TestKey.self] = newValue }
    }
}


struct OrchestratorError: Equatable {
    let date: Date
    let message: String
    let detailMessage: String?
    let hasAction: Bool
    let showDetailsDirectly: Bool
}
