//
//  SignInNeedHelp.swift
//  App
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct SignInNeedHelp: View {
    @Environment(\.openURL) private var openURL
    
    var body: some View {
        OctoButton(
            text: "sign_in___discovery___need_help"~,
            type: .link,
            small: true,
            clickListener: {
                OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventSignInHelpOpened(), params: [:])
                openURL(UriLibrary.shared.getSignInHelpUri())
            },
            longClickListener: {
                openURL(UriLibrary.shared.getContactUri(bugReport: false))
            }
        )
    }
}



struct SignInNeedHelp_Previews: PreviewProvider {
    static var previews: some View {
        SignInNeedHelp()
    }
}
