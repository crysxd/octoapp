//
//  LoginOption.swift
//  App
//
//  Created by Christian on 09/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI


struct ComponentSignInOption: View {
    
    var title: LocalizedStringKey
    var subtitle: LocalizedStringKey
    var type: OptionType
    var clickListener: () -> Void
    
    var body: some View {
        Button(action: clickListener) {
            HStack {
                VStack(spacing: OctoTheme.dimens.margin0) {
                    Text(title)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .typographySectionHeader()
                        .lineLimit(1)
                        
                    Text(subtitle)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .typographyBase()
                        .lineLimit(1)
                }
                .frame(maxWidth: .infinity)
                .padding([.trailing],  OctoTheme.dimens.margin2)
                
                Image(systemName: "chevron.right")
                    .foregroundColor(foregroundColor())
            }
            .frame(maxWidth: .infinity)
            .padding([.bottom, .top],  OctoTheme.dimens.margin12)
            .padding([.leading, .trailing],  OctoTheme.dimens.margin2)
            .opacity(type == .placeholder ? 0 : 1)
            .background(backgroundColor())
        }
        .buttonStyle(SiginInOptionButtonStyle())
    }
   
    
    func backgroundColor() -> Color {
        switch(type) {
        case .discovered: return OctoTheme.colors.menuStylePrinterBackground
        case .manual: return OctoTheme.colors.menuStyleSettingsBackground
        case .reuse: return OctoTheme.colors.menuStyleOctoPrintBackground
        case .support: return OctoTheme.colors.menuStyleSupportBackground
        case .placeholder: return OctoTheme.colors.lightText.opacity(0.15)
        }
    }
    
    func foregroundColor() -> Color {
        switch(type) {
        case .discovered: return OctoTheme.colors.menuStylePrinterForeground
        case .manual: return OctoTheme.colors.menuStyleSettingsForeground
        case .reuse: return OctoTheme.colors.menuStyleOctoPrintForeground
        case .support: return OctoTheme.colors.menuStyleSupportForeground
        case .placeholder: return OctoTheme.colors.lightText.opacity(0)
        }
    }
    
    enum OptionType {
        case discovered, manual, support, reuse, placeholder
    }
}


private struct SiginInOptionButtonStyle: ButtonStyle {

  func makeBody(configuration: Self.Configuration) -> some View {
    configuration.label
          .frame(maxWidth: .infinity)
          .background(configuration.isPressed ? OctoTheme.colors.clickIndicatorOverlay : .clear)
          .cornerRadius(OctoTheme.dimens.cornerRadius)
          .scaleEffect(configuration.isPressed ? OctoTheme.dimens.buttonActiveScale: 1)
  }
}

struct ComponentSignInOption_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 15) {
            ComponentSignInOption(
                title: "octopi.local",
                subtitle: "192.168.1.43",
                type: .discovered
            ) {
                
            }
            
            ComponentSignInOption(
                title: "Manual login",
                subtitle: "It's easy!",
                type: .manual
            ){
                
            }
            
            ComponentSignInOption(
                title: "Beagle",
                subtitle: "192.168.1.33",
                type: .reuse
            ) {
                
            }
            
            ComponentSignInOption(
                title: "Quick switch not enabled",
                subtitle: "Enable to reconnect",
                type: .support
            ) {
                
            }
            
            ComponentSignInOption(
                title: "Quick switch not enabled",
                subtitle: "Enable to reconnect",
                type: .placeholder
            ) {
                
            }
        }
    }
}
