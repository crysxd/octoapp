//
//  TutorialsListItem.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct TutorialsListItem : View {
    @Environment(\.openURL) var openUrl
    var tutorial: TutorialStruct
    var seenUntil: Date
    
    var body: some View {
        Button(action: { openUrl(tutorial.url) } ) {
            HStack(alignment: .top, spacing: OctoTheme.dimens.margin1) {
                ZStack(alignment: .topTrailing) {
                    ThumbnailImage(url: URL(string: tutorial.thumbnailUrl ?? ""))
                        .ifPad { $0.frame(width: 180) }
                        .ifNotPad{ $0.frame(width: 120) }
                    
                    if ((tutorial.publishedAt ?? Date(timeIntervalSince1970: 0)) > seenUntil) {
                        Text("New"~)
                            .foregroundColor(OctoTheme.colors.textColoredBackground)
                            .padding([.top, .bottom], OctoTheme.dimens.margin0)
                            .padding([.trailing, .leading], OctoTheme.dimens.margin01)
                            .background(Capsule().fill(OctoTheme.colors.accent))
                            .padding(OctoTheme.dimens.margin1)
                    }
                }
                
                VStack(alignment: .leading, spacing: OctoTheme.dimens.margin01) {
                    Text(tutorial.title ?? "???")
                        .typographySubtitle()
                        .lineLimit(2)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    
                    Text(tutorial.description ?? "")
                        .foregroundColor(OctoTheme.colors.lightText)
                        .typographyLabel()
                        .ifPad { $0.lineLimit(6) }
                        .ifNotPad { $0.lineLimit(3) }
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                .frame(maxWidth: .infinity)
                .multilineTextAlignment(.leading)
            }
        }
    }
}

struct TutorialsListItem_Previews: PreviewProvider {
    static var previews: some View {
        TutorialsListItem(
            tutorial: TutorialStruct(
                id: "some",
                url: "",
                title: "How to code in Swift?",
                description: "This is a exmaple tutorial with some lenghty description!",
                thumbnailUrl: "https://i.ytimg.com/vi/71nX2FNGK6Q/hqdefault.jpg",
                publishedAt: Date()
            ),
            seenUntil: Date(timeIntervalSince1970: 0)
        )
    }
}
