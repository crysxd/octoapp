//
//  ControlsMenuBar.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct ControlsBottomBar: View {
    
    var state: ControlsViewModel.State
    var onTogglePause: () async throws  -> Void = {}
    var onCancel: () async throws  -> Void = {}
    var onEmergencyStop: () async throws  -> Void = {}
    var onOpenMenu: () -> Void = {}
    var onShowFiles: () -> Void = {}
    var onShowTemperatures: () -> Void = {}
    @StateObject private var viewModel = ControlsBottomBarViewModel()
    @Environment(\.controlsInEditMode) private var controlsInEditMode
    @Environment(\.leftyMode) private var leftHandMode
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    
    var body: some View {
        VStack(spacing: 0) {
            Color.clear
            
            ZStack {
                normalBottomBar.opacity(controlsInEditMode ? 0 : 1)
                editBottomBar
            }
            .frame(maxWidth: .infinity)
            .animation(.default, value: controlsInEditMode)
            .animation(.default, value: state)
        }
        .frame(maxWidth: .infinity)
        .background(Color.clear)
    }
    
    @ViewBuilder
    var editBottomBar: some View {
        if controlsInEditMode {
            ZStack {
                // Block touch
                OctoTheme.colors.windowBackground.opacity(0.01)
                    .onTapGesture {}
                
                // Edit options
                HStack {
                    Text("controls___customize___ios_hint"~)
                        .typographyLabel()
                    Spacer(minLength: OctoTheme.dimens.margin3)
                    OctoButton(text: "done"~, small: true) { orchestrator.controlsInEditMode = false }
                }
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
            }
            .fixedSize(horizontal: false, vertical: true)
        }
    }
    
    var normalBottomBar: some View {
        HStack {
            switch(state) {
            case .connect: ConnectBottomBar(
                leftHandMode: leftHandMode
            )
            case .prepare: PrepareBottomBar(
                leftHandMode: leftHandMode,
                showEmergencyStop: viewModel.showEmergencyStop,
                onShowFiles: onShowFiles,
                onShowTemperatures: onShowTemperatures,
                onEmergencyStop: onEmergencyStop
            )
            case .print(controls: _, info: let info): PrintBottomBar(
                leftHandMode:leftHandMode,
                info: info,
                showEmergencyStop: viewModel.showEmergencyStop,
                onTogglePause: onTogglePause,
                onCancel: onCancel,
                onOpenMenu: onOpenMenu,
                onEmergencyStop: onEmergencyStop
            )
            default: Spacer()
            }
            
            Spacer()
            
            if case .print(controls: _, info: let printInformation) = state {
                Text(printInformation.description())
                    .typographySubtitle()
                    .transition(.opacity)
                    .scaleEffect(x: leftHandMode ? -1 : 1)
                    .offset(x: OctoTheme.dimens.margin2)
                    .padding([.leading, .trailing], OctoTheme.dimens.margin12)
            }
            
            OctoMenuButton(clickListener: onOpenMenu)
                .offset(x: OctoTheme.dimens.margin2)
                .padding([.top, .bottom], OctoTheme.dimens.margin01)
        }
        .padding(.leading, OctoTheme.dimens.margin2)
        .scaleEffect(x: leftHandMode ? -1 : 1)
    }
}

private class ControlsBottomBarViewModel : ObservableObject {
    
    @Published var showEmergencyStop: Bool
    private static var preferences: OctoPreferences? { SharedBaseInjector.shared.getOrNull()?.preferences }
    
    init() {
        showEmergencyStop = ControlsBottomBarViewModel.preferences?.emergencyStopInBottomBar == true
        ControlsBottomBarViewModel.preferences?.updatedFlow2.asPublisher()
            .map { (prefs: OctoPreferences) in prefs.emergencyStopInBottomBar }
            .assign(to: &$showEmergencyStop)
    }
}

private var actionTransition = AnyTransition.asymmetric(
    insertion: .move(edge: .leading),
    removal: .move(edge: .trailing)
).combined(with: .opacity)

private struct Separator: View {
    
    var body: some View {
        OctoTheme.colors.lightText
            .frame(width: 1, height: OctoTheme.dimens.margin2)
            .transition(.opacity)
        
    }
}

private struct ConnectBottomBar: View {
    
    let leftHandMode: Bool
    @EnvironmentObject private var mediator: ConnectActionMediator
    
    var body: some View {
        HStack {
            if let a = mediator.connectAction {
                OctoAsyncButton(
                    text: a.title,
                    type: a.type,
                    small: true,
                    clickListener: { await mediator.performConnectAction(a.id) }
                )
                .scaleEffect(x: leftHandMode ? -1 : 1)
                .transition(actionTransition)
                .padding(.trailing, OctoTheme.dimens.margin1)
            }
        }
        .animation(.default, value: mediator.connectAction)
    }
}


private struct PrepareBottomBar: View {
    
    let leftHandMode: Bool
    let showEmergencyStop: Bool
    let onShowFiles: () -> Void
    let onShowTemperatures: () -> Void
    let onEmergencyStop: () async throws -> Void
    
    var body: some View {
        HStack {
            
            if showEmergencyStop {
                ConfirmedBottomBarButton(
                    icon: "exclamationmark.octagon.fill",
                    confirmation: "emergency_stop_confirmation_message"~,
                    confirmAction: "emergency_stop_confirmation_action"~,
                    cancelAction: "cancel"~,
                    leftHandMode: leftHandMode,
                    redIcon: true,
                    action: onEmergencyStop
                )
                
                Separator()
                
                OctoIconButton(icon: "folder.fill", clickListener: onShowFiles)
                    .scaleEffect(x: leftHandMode ? -1 : 1)
                    .transition(actionTransition)
                
                Separator()
            } else {
                
                OctoButton(text: "start_printing"~, small: true, clickListener: onShowFiles)
                    .scaleEffect(x: leftHandMode ? -1 : 1)
                    .transition(actionTransition)
                    .padding(.trailing, OctoTheme.dimens.margin1)
                
                Separator()
            }
           
            OctoIconButton(icon: "flame.fill", clickListener: onShowTemperatures)
                .scaleEffect(x: leftHandMode ? -1 : 1)
                .transition(actionTransition)
        }
    }
}

private struct PrintBottomBar: View {
    
    let leftHandMode: Bool
    let info: ControlsViewModel.PrintInformation
    let showEmergencyStop: Bool
    
    let onTogglePause: () async throws  -> Void
    let onCancel: () async throws  -> Void
    let onOpenMenu: () -> Void
    let onEmergencyStop: () async throws -> Void
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            if showEmergencyStop {
                ConfirmedBottomBarButton(
                    icon: "exclamationmark.octagon.fill",
                    confirmation: "emergency_stop_confirmation_message"~,
                    confirmAction: "emergency_stop_confirmation_action"~,
                    cancelAction: "cancel"~,
                    leftHandMode: leftHandMode,
                    redIcon: true,
                    action: onEmergencyStop
                )
               
                if !info.cancelling {
                    Separator()
                }
            }
            
            if !info.cancelling {
                ConfirmedBottomBarButton(
                    icon: "stop.fill",
                    confirmation: "cancel_print_confirmation_message"~,
                    confirmAction: "cancel_print_confirmation_action"~,
                    cancelAction: "resume_print_confirmation_action"~,
                    leftHandMode: leftHandMode,
                    action: onCancel
                )
                
                if !info.pausing {
                    Separator()
                }
            }
            
            if info.paused {
                ConfirmedBottomBarButton(
                    icon: "play.fill",
                    confirmation: "resume_print_confirmation_message"~,
                    confirmAction: "resume_print_confirmation_action"~,
                    cancelAction: "cancel"~,
                    leftHandMode: leftHandMode,
                    action: onTogglePause
                )
            }
            
            if !info.pausing && !info.cancelling && !info.paused {
                ConfirmedBottomBarButton(
                    icon: "pause.fill",
                    confirmation: "pause_print_confirmation_message"~,
                    confirmAction: "pause_print_confirmation_action"~,
                    cancelAction: "resume_print_confirmation_action"~,
                    leftHandMode: leftHandMode,
                    action: onTogglePause
                )
            }
        }
        .animation(.spring(), value: leftHandMode)
    }
}

private struct ConfirmedBottomBarButton : View {
    
    var icon: String
    var confirmation: String
    var confirmAction: String
    var cancelAction: String
    var leftHandMode: Bool
    var redIcon: Bool = false
    var action: () async throws -> Void
    
    @State var loading = false
    @State var showConfirmation = false
    
    var body: some View {
        OctoIconButton(
            icon: icon,
            color: redIcon ? OctoTheme.colors.red : OctoTheme.colors.accent,
            loading: loading,
            clickListener: { showConfirmation = true }
        )
        .scaleEffect(x: leftHandMode ? -1 : 1)
        .transition(actionTransition)
        .confirmationDialog(
            confirmation,
            isPresented: $showConfirmation,
            actions: {
                Button(confirmAction, role: .destructive) {
                    Task {
                        loading = true
                        try? await action()
                        loading = false
                    }
                }
                Button(cancelAction, role: .cancel) {
                    showConfirmation = false
                }
            },
            message: {
                Text(confirmation)
            }
        )
    }
}


private extension ControlsViewModel.PrintInformation {
    func description() -> String {
        if pausing {
            return "pausing"~
        } else if paused {
            return "paused"~
        } else if cancelling {
            return "cancelling"~
        } else {
            return "\(progress)%"
        }
    }
}


struct ControlsMenuBar_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(ControlsViewModel.State.initial) { state in
            ControlsBottomBar(state: state.wrappedValue)
                .background(.black.opacity(0.05))
                .onTapGesture {
                    switch(state.wrappedValue) {
                    case .initial: state.wrappedValue = .connect(controls: [])
                    case .connect: state.wrappedValue = .prepare(controls: [])
                    case .prepare: state.wrappedValue = .print(
                        controls: [],
                        info: ControlsViewModel.PrintInformation(progress: 42, pausing: false, paused: false, cancelling: false)
                    )
                    case .print:  state.wrappedValue = .print(
                        controls: [],
                        info: ControlsViewModel.PrintInformation(progress: 42, pausing: false, paused: true, cancelling: false)
                    )
                    }
                }
        }
        .previewDisplayName("Dynamic")
        
        ControlsBottomBar(state: .connect(controls: []))
            .background(.black.opacity(0.05))
            .previewDisplayName("Connect")
        
        ControlsBottomBar(state: .prepare(controls: []))
            .background(.black.opacity(0.05))
            .previewDisplayName("Prepare")
        
        ControlsBottomBar(state: .print(controls: [], info: ControlsViewModel.PrintInformation(progress: 42, pausing: false, paused: false, cancelling: false)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Printing")
        
        ControlsBottomBar(state: .print(controls: [], info: ControlsViewModel.PrintInformation(progress: 42, pausing: true, paused: false, cancelling: false)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Pausing")
        
        ControlsBottomBar(state: .print(controls: [], info: ControlsViewModel.PrintInformation(progress: 42, pausing: false, paused: true, cancelling: false)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Paused")
        
        ControlsBottomBar(state: .print(controls: [], info: ControlsViewModel.PrintInformation(progress: 42, pausing: false, paused: false, cancelling: true)))
            .background(.black.opacity(0.05))
            .previewDisplayName("Cancelling")
        
        
        ControlsBottomBar(state: .prepare(controls: []))
            .background(.black.opacity(0.05))
            .previewDisplayName("Editing")
            .environment(\.controlsInEditMode, true)
    }
}
