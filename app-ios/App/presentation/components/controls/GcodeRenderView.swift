//
//  GcodeRenderView.swift
//  OctoApp
//
//  Created by Christian Würthner on 30/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import UIKit
import OctoAppBase

struct GcodeRenderView: View {
        
    let mediator: LowOverheadMediator<GcodeRenderContext>
    var plugin: (GcodeNativeCanvas) -> Void = { _ in }
    var onClick: (_ bedMmX: Float, _ bedMmY: Float) -> Void = { _, _ in }
    let printBed: GcodeNativeCanvasImageStruct
    let printBedWidthMm: Float
    let printBedHeightMm: Float
    let extrusionWidthMm: Float
    let originInCenter: Bool
    let quality: GcodePreviewSettingsStruct.Quality
    let paddingHorizontal: CGFloat
    let paddingTop: CGFloat
    let paddingBottom: CGFloat
    
    @State private var sinkId = UUID().uuidString

    var body: some View {
        GcodeRenderViewRepresentable(
            mediator: mediator,
            plugin: plugin,
            onClick: onClick,
            printBed: printBed,
            printBedWidthMm: printBedWidthMm,
            printBedHeightMm: printBedHeightMm,
            extrusionWidthMm: extrusionWidthMm,
            originInCenter: originInCenter,
            quality: quality,
            sinkId: sinkId,
            paddingHorizontal: paddingHorizontal,
            paddingTop: paddingTop,
            paddingBottom: paddingBottom
        )
        .onChange(of: mediator) { old, _ in
            // Mediator changed, remove from old
            old.popSink(sinkId: sinkId)
        }
        .onDisappear {
            // View removed, remove from mediator
            mediator.popSink(sinkId: sinkId)
        }
    }
}

@MainActor
struct GcodeRenderViewRepresentable: UIViewRepresentable {
    
    let mediator: LowOverheadMediator<GcodeRenderContext>
    let plugin: (GcodeNativeCanvas) -> Void
    let onClick: (_ bedMmX: Float, _ bedMmY: Float) -> Void
    let printBed: GcodeNativeCanvasImageStruct
    let printBedWidthMm: Float
    let printBedHeightMm: Float
    let extrusionWidthMm: Float
    let originInCenter: Bool
    let quality: GcodePreviewSettingsStruct.Quality
    let sinkId: String
    let paddingHorizontal: CGFloat
    let paddingTop: CGFloat
    let paddingBottom: CGFloat
    
    func makeUIView(context: Context) -> some UIView {
        let view = CanvasView()
        view.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        view.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        view.clipsToBounds = false
        view.layer.masksToBounds = false
        view.layer.drawsAsynchronously = true
        view.isOpaque = false
        view.enableZoom()
        return view
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        let canvas = uiView as! CanvasView
        canvas.quality = quality
        canvas.paddingHorizontal = paddingHorizontal
        canvas.paddingTop = paddingTop
        canvas.paddingBottom = paddingBottom
        canvas.plugin = plugin
        canvas.onClick = onClick
        canvas.setNeedsDisplay()
        
        // Add this sink to the mediator
        mediator.pushSink(sinkId: sinkId) { context in
            guard let context = context else { return }
            canvas.submitParams(
                params: GcodeRenderer.RenderParams(
                    renderContext: context,
                    printBed: printBed.toClass(),
                    printBedWidthMm: printBedWidthMm,
                    printBedHeightMm: printBedHeightMm,
                    minPrintHeadDiameterPx: Float(3 * UIScreen.main.scale),
                    extrusionWidthMm: extrusionWidthMm,
                    originInCenter: originInCenter,
                    quality: quality.toClass()
                )
            )
            
            DispatchQueue.main.async {
                canvas.setNeedsDisplay()
            }
        }
    }
}

class GestureReceiverView: UIView {
    
    private var lastPosition: CGPoint = .zero
    private var lastScale: CGFloat = 0
    
    func zoomBy(focusX: CGFloat, focusY: CGFloat, scaleFactor: CGFloat) {}
    func moveBy(x: CGFloat, y: CGFloat) {}
    func doubleTap(focusX: CGFloat, focusY: CGFloat) {}
    func singleTap(focusX: CGFloat, focusY: CGFloat) {}

    func enableZoom() {
        isUserInteractionEnabled = true

        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(zoom(_:)))
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pan(_:)))
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTap(_:)))
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(doubleTap(_:)))
        
        singleTapGesture.numberOfTapsRequired = 1
        doubleTapGesture.numberOfTapsRequired = 2
        singleTapGesture.require(toFail: doubleTapGesture)
        
        addGestureRecognizer(pinchGesture)
        addGestureRecognizer(panGesture)
        addGestureRecognizer(singleTapGesture)
        addGestureRecognizer(doubleTapGesture)
    }
   
    @objc private func pan(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .began: lastPosition = sender.location(in: self)
        case .changed: do {
            let position = sender.location(in: self)
            moveBy(x: lastPosition.x - position.x, y: lastPosition.y -  position.y)
            lastPosition = position
        }
        default: break
        }
    }
    
    @objc private func zoom(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
        case .began: lastScale = sender.scale
        case .changed: do {
            let scale = sender.scale
            let focus = sender.location(in: self)
            zoomBy(focusX: focus.x, focusY: focus.y, scaleFactor:  1 + ((scale - lastScale)))
            lastScale = scale
        }
        default: break
        }
    }
    
    @objc private func doubleTap(_ sender: UITapGestureRecognizer) {
        let focus = sender.location(in: self)
        doubleTap(focusX: focus.x, focusY: focus.y)
    }
    
    @objc private func singleTap(_ sender: UITapGestureRecognizer) {
        let focus = sender.location(in: self)
        singleTap(focusX: focus.x, focusY: focus.y)
    }
}

private class CanvasView : GestureReceiverView {
    
    var paddingHorizontal: CGFloat {
        set { canvas.paddingHorizontal = Float(newValue) }
        get { CGFloat(canvas.paddingHorizontal) }
    }
    
    var paddingTop: CGFloat {
        set { canvas.paddingTop = Float(newValue) }
        get { CGFloat( canvas.paddingTop) }
    }
    
    var paddingBottom: CGFloat {
        set { canvas.paddingBottom = Float(newValue) }
        get { CGFloat(canvas.paddingBottom) }
    }
    
    var quality: GcodePreviewSettingsStruct.Quality  {
        set { canvas.quality = newValue }
        get { canvas.quality }
    }
    
    var plugin: (GcodeNativeCanvas) -> Void  {
        set { renderer.plugin = newValue }
        get { renderer.plugin }
    }
    
    var onClick: (_ bedMmX: Float, _ bedMmY: Float) -> Void = { _, _ in }
    
    
    private let canvas: GcodeCanvas
    private let renderer: GcodeRenderer
    
    override init(frame: CGRect) {
        canvas = GcodeCanvas()
        renderer = GcodeRenderer(canvas: canvas)
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        canvas = GcodeCanvas()
        renderer = GcodeRenderer(canvas: canvas)
        super.init(coder: aDecoder)
    }
    
    override func zoomBy(focusX: CGFloat, focusY: CGFloat, scaleFactor: CGFloat) {
        renderer.zoomBy(focusX: Float(focusX), focusY: Float(focusY), scaleFactor: Float(scaleFactor))
    }
    
    override func moveBy(x: CGFloat, y: CGFloat) {
        renderer.moveBy(x: Float(x), y: Float(y))
    }
    
    override func singleTap(focusX: CGFloat, focusY: CGFloat) {
        let bedCoordinates = renderer.getPrintBedCoordinateFromViewPortPosition(
            x: Float(focusX),
            y: Float(focusY)
        )
        assert(bedCoordinates.first != nil && bedCoordinates.second != nil, "X or Y is nil, expected to never be nil")
        onClick(bedCoordinates.first!.floatValue, bedCoordinates.second!.floatValue)
    }
    
    override func doubleTap(focusX: CGFloat, focusY: CGFloat) {
        Task {
            let durationMs = 200
            let frames = durationMs / 60
            let frameTime = durationMs / frames
            let targetZoom: Float = renderer.zoom > 1 ? 1 : 5
            let startZoom = renderer.zoom
            for i in 0...frames {
                let progress = Float(i) / Float(frames)
                let zoom = startZoom + ((targetZoom - startZoom) * progress)
                renderer.zoom(focusX: Float(focusX), focusY: Float(focusY), newZoom: zoom)
                try? await Task.sleep(for: .milliseconds(frameTime))
            }
        }
    }
        
    func submitParams(params: GcodeRenderer.RenderParams) {
        canvas.setNeedsDisplay = {
            DispatchQueue.main.async {
                self.setNeedsDisplay()
            }
        }
        
        renderer.debugLogs = false
        renderer.submitRenderParams(params: params)
    }
    
    override func draw(_ rect: CGRect) {
        let graphics = UIGraphicsGetCurrentContext()
        canvas.lastSize = rect.size
        canvas.setGraphics(graphis: graphics)
        graphics?.clear(rect)
        renderer.draw()
        canvas.setGraphics(graphis: nil)
    }
}

class GcodeCanvas : NSObject, GcodeNativeCanvas {
    var setNeedsDisplay: () -> Void = {}
    var lastSize: CGSize = .zero
    var padding: CGFloat = 0
    var quality: GcodePreviewSettingsStruct.Quality = .medium
    var paddingHorizontal: Float = 0

    var paddingBottom: Float = 0
    var paddingLeft: Float { paddingHorizontal }
    var paddingRight: Float { paddingHorizontal }
    var paddingTop: Float = 0
    var viewPortHeight: Float { Float(lastSize.height) }
    var viewPortWidth: Float { Float(lastSize.width) }
    
    private var currentGraphics: CGContext? = nil
    private var graphis: CGContext {
        assert(currentGraphics != nil, "Graphics not available")
        return currentGraphics!
    }
    
    func setGraphics(graphis: CGContext?) {
        currentGraphics = graphis
        
        // Configure draw quality
        graphis?.setShouldAntialias(quality == .ultra)
        graphis?.setAllowsAntialiasing(quality == .ultra)
        graphis?.setLineCap(quality == .low ? .butt : .round)
        graphis?.setLineJoin(quality == .low ? .miter : .round)
        switch quality {
        case .low: graphis?.setFlatness(3)
        case .medium: graphis?.setFlatness(1)
        case .ultra: graphis?.setFlatness(0.5)
        }
    }
    
    func drawArc(centerX: Float, centerY: Float, radius: Float, startDegrees: Float, sweepAngle: Float, paint: GcodeNativeCanvasPaint) {
        graphis.beginPath()
        let startRad = CGFloat(startDegrees) * CGFloat.pi / 180
        let sweepRad = CGFloat(sweepAngle) * CGFloat.pi / 180
        graphis.addArc(center: CGPoint(x: CGFloat(centerX), y: CGFloat(centerY)), radius: CGFloat(radius), startAngle: startRad + sweepRad, endAngle: startRad, clockwise: sweepRad > 0)
        graphis.drawPath(paint: paint)
    }
    
    func drawLine(xStart: Float, yStart: Float, xEnd: Float, yEnd: Float, paint: GcodeNativeCanvasPaint) {
        graphis.beginPath()
        graphis.move(to: CGPoint(x: CGFloat(xStart), y: CGFloat(yStart)))
        graphis.addLine(to: CGPoint(x: CGFloat(xEnd), y: CGFloat(yEnd)))
        graphis.drawPath(paint: paint)
    }
    
    func drawRect(left: Float, top: Float, right: Float, bottom: Float, paint: GcodeNativeCanvasPaint) {
        graphis.beginPath()
        graphis.addRects([CGRect(x: CGFloat(left), y: CGFloat(top), width: CGFloat(right - left), height: CGFloat(bottom - top))])
        graphis.drawPath(paint: paint)
    }
    
    func drawLines(points: KotlinFloatArray, offset: Int32, count: Int32, paint: GcodeNativeCanvasPaint) {
        graphis.beginPath()
                
        for i in stride(from: offset, to: offset + count, by: 4) {
            let start = CGPoint(x: CGFloat(points.get(index: i + 0)), y: CGFloat(points.get(index: i + 1)))
            let end = CGPoint(x: CGFloat(points.get(index: i + 2)), y: CGFloat(points.get(index: i + 3)))
            
            if graphis.currentPointOfPath != start {
                graphis.move(to: start)
            }
            
            graphis.addLine(to: end)
        }
        
        graphis.drawPath(paint: paint)
    }
    
    func drawImage(image: GcodeNativeCanvasImage, x: Float, y: Float, width: Float, height: Float) {
        image.uiImage.draw(in: CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(width), height: CGFloat(height)))
    }
    
    func intrinsicHeight(image: GcodeNativeCanvasImage) -> Float {
        Float(image.uiImage.size.height)
    }
    
    func intrinsicWidth(image: GcodeNativeCanvasImage) -> Float {
        Float(image.uiImage.size.width)
    }
    
    func invalidate() {
        setNeedsDisplay()
    }
    
    func restoreState() {
        graphis.restoreGState()
    }
    
    func rotate(degrees: Float) {
        graphis.rotate(by: CGFloat(degrees) * CGFloat.pi / 180)
    }
    
    func saveState() {
        graphis.saveGState()
    }
    
    func scale(x: Float, y: Float) {
        graphis.scaleBy(x: CGFloat(x), y: CGFloat(y))
    }
    
    func translate(x: Float, y: Float) {
        graphis.translateBy(x: CGFloat(x), y: CGFloat(y))
    }
}

private var imageCache: [GcodeNativeCanvasImage:UIImage] = [:]
private let clearColor = CGColor(red: 0, green: 0, blue: 0, alpha: 0)

fileprivate extension CGContext {
    func drawPath(paint: GcodeNativeCanvasPaint) {
        // Set color
        let color = paint.color.cgColor
        setFillColor(paint.hasFill ? color : clearColor)
        setStrokeColor(paint.hasStroke ? color : clearColor)
        setAlpha(CGFloat(paint.alpha))
        
        // Emualte Android's handling of scaling
        let correcion = 1 - CGFloat(paint.strokeWidthCorrection)
        let minStroke = CGFloat(paint.strokeWidth) / UIScreen.main.scale
        let maxStroke = CGFloat(paint.strokeWidth)
        let stroke = minStroke + ((maxStroke - minStroke) * correcion)
        setLineWidth(stroke)
        
        // Stroke or fill
        drawPath(using: paint.hasFill ? .fill : .stroke)
    }
}

fileprivate extension GcodeNativeCanvasColor {
    var cgColor: CGColor {
        let fallback = CGColor(red: 1, green: 0, blue: 1, alpha: 1)
        
        switch self {
        case _ as MoveExtrusion: return UIColor(named: "gcodeExtrusion")?.cgColor ?? fallback
        case _ as MovePreviousLayer: return UIColor(named: "gcodePrevious")?.cgColor ?? fallback
        case _ as MoveRemainingLayer: return UIColor(named: "gcodeRemaining")?.cgColor ?? fallback
        case _ as MoveUnsupported: return UIColor(named: "gcodeUnsupported")?.cgColor ?? fallback
        case _ as MoveTravel: return UIColor(named: "gcodeTravel")?.cgColor ?? fallback
        case _ as PrintHead: return UIColor(named: "gcodePrintHead")?.cgColor ?? fallback
        case _ as Accent: return UIColor(named: "blue")?.cgColor ?? fallback
        case _ as Yellow: return UIColor(named: "yellow")?.cgColor ?? fallback
        case _ as LightGrey: return UIColor(named: "lightGrey")?.cgColor ?? fallback
        case _ as Red: return UIColor(named: "red")?.cgColor ?? fallback
        case _ as Green: return UIColor(named: "green")?.cgColor ?? fallback
        case let color as Custom: return CGColor(red: CGFloat(color.red), green: CGFloat(color.green), blue: CGFloat(color.blue), alpha: 1)
        default: return fallback
        }
    }
}

fileprivate extension GcodeNativeCanvasPaint {
    var hasFill: Bool {
        switch mode {
        case .fill: return true
        default: return false
        }
    }
    
    var hasStroke: Bool {
        switch mode {
        case .stroke: return true
        default: return false
        }
    }
}

fileprivate extension GcodeNativeCanvasImage {
    var uiImage: UIImage {
        if let image = imageCache[self] {
            return image
        } else {
            let uiImage = UIImage(named: toStruct().imageName)
            assert(uiImage != nil, "UIImage was expected to be available but is nil: name=\(name)")
            imageCache[self] = uiImage!
            return uiImage!
        }
    }
}
