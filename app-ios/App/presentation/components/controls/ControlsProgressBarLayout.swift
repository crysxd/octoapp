//
//  ControlsProgressBarLayout.swift
//  OctoApp
//
//  Created by Christian on 31/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI


struct ControlsProgressBarLayout: Layout {
    
    var progress: Double
    var trailingMin: Double = 0.5
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        return proposal.replacingUnspecifiedDimensions()
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        let progressWidth = bounds.size.width * progress
        let leftProgress = progress >= trailingMin
        let margin = OctoTheme.dimens.margin12
        let maxWidth = bounds.size.width - margin
        let progressWidthLimited = progressWidth > maxWidth ? maxWidth : progressWidth
        
        subviews[0].place(
            at: p,
            proposal: .init(width: progressWidth, height: bounds.size.height)
        )
        
        if (leftProgress) {
            subviews[1].place(
                at: p,
                proposal: .init(width: progressWidthLimited, height: bounds.size.height)
            )
        } else {
            p.x += progressWidth > OctoTheme.dimens.margin12 ? progressWidth : OctoTheme.dimens.margin12
            subviews[1].place(
                at: p,
                proposal: .init(width: bounds.size.width , height: bounds.size.height)
            )
        }
    }
}
