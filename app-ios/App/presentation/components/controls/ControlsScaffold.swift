//
//  ControlsScaffold.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ControlsScaffold<Control: View>: View {
    
    var title: String?
    var iconSystemName: String? = nil
    var iconSystemName2: String? = nil
    var supportsEdit: Bool = true
    var iconAction: () async throws -> Void = {}
    var iconAction2: () async throws -> Void = {}
    var actionLoading: Bool = false
    var actionLoading2: Bool = false
    var control: () -> Control
    
    @Environment(\.controlsInEditMode) private var controlsInEditMode
    @Environment(\.controlIsVisible) private var controlIsVisible
    @Environment(\.controlToggleVisible) private var controlToggleVisible
    
    var body: some View {
        if !controlsInEditMode || supportsEdit {
            ZStack(alignment: .leading) {
                controlBody
                
                if controlsInEditMode {
                    // Block touch for controls
                    OctoTheme.colors.windowBackground.opacity(0.01).onTapGesture { /* Block click */ }
                    
                    editActions
                }
            }
        }
    }
    
    var editActions: some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            if let icon = iconSystemName {
                OctoAsyncIconButton(
                    icon: icon,
                    clickListener: iconAction
                )
            }

            if let icon = iconSystemName2 {
                OctoAsyncIconButton(
                    icon: icon,
                    clickListener: iconAction2
                )
            }

            
            OctoIconButton(
                icon: controlIsVisible ? "eye.fill" : "eye.slash.fill",
                clickListener: controlToggleVisible
            )
        }
        .padding(OctoTheme.dimens.margin1)
    }
    
    var controlBody: some View {
        VStack(spacing: 0) {
            if (title != nil || iconSystemName != nil) {
                HStack(spacing: 0) {
                    Text(title ?? "")
                        .typographySectionHeader()
                        .padding([.leading], OctoTheme.dimens.margin2)
                        .padding([.top, .bottom], OctoTheme.dimens.margin1)
                    
                    Spacer(minLength: OctoTheme.dimens.margin2)
                    
                    if let icon = iconSystemName2 {
                        OctoAsyncIconButton(
                            icon: icon,
                            loading: actionLoading2,
                            clickListener: iconAction2
                        )
                    }

                    
                    if let icon = iconSystemName {
                        OctoAsyncIconButton(
                            icon: icon,
                            loading: actionLoading,
                            clickListener: iconAction
                        )
                        .padding([.trailing], OctoTheme.dimens.margin01)
                    }
                }
                .frame(maxWidth: .infinity)
            }
            
            control()
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .padding([.top, .bottom], OctoTheme.dimens.margin12)
        .frame(maxWidth: .infinity)
        .opacity(controlIsVisible ? 1: 0.33)
        .if(controlsInEditMode) {
            $0.scaleEffect(0.66)
                .padding([.top, .bottom], -20)
                .padding(.leading, 20)
        }
    }
}

struct ControlsScaffold_Previews: PreviewProvider {
    static var previews: some View {
        ControlsScaffold(title: "Control title", iconSystemName: "slider.horizontal.3") {
            ZStack {
                
            }
            .frame(maxWidth: .infinity)
            .frame(height: 100)
            .background(OctoTheme.colors.inputBackground)
            .cornerRadius(OctoTheme.dimens.cornerRadius)
        }
        .previewDisplayName("Icon")
        
        ControlsScaffold(title: "Control title", iconSystemName: "slider.horizontal.3", iconSystemName2:"puzzlepiece.extension.fill") {
            ZStack {
                
            }
            .frame(maxWidth: .infinity)
            .frame(height: 100)
            .background(OctoTheme.colors.inputBackground)
            .cornerRadius(OctoTheme.dimens.cornerRadius)
        }
        .previewDisplayName("Two Icon")
        
        ControlsScaffold(title: "Control title") {
            ZStack {
                
            }
            .frame(maxWidth: .infinity)
            .frame(height: 100)
            .background(OctoTheme.colors.inputBackground)
            .cornerRadius(OctoTheme.dimens.cornerRadius)
        }
        .previewDisplayName("No icon")
    }
}
