//
//  CancelObjectConfirmation.swift
//  OctoApp
//
//  Created by Christian Würthner on 06/04/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

private struct CancelObjectConfirmation: ViewModifier {
    
    var object: CancelObjectViewModel.PrintObject?
    @Binding var shown: Bool
    var onCancel: () -> Void
    
    private var confirmaitonText: String {
        String(format: "cancel_object___confirmation_message"~, object?.label ?? "")
    }
    
    func body(content: Content) -> some View {
        content.confirmationDialog(
            LocalizedStringKey(confirmaitonText),
            isPresented: $shown,
            actions: {
                Button("cancel_object___confirmation_positive", role: .destructive) {
                    onCancel()
                }
                Button("cancel_object___confirmation_negative"~, role: .cancel) {

                }
            },
            message: { Text(confirmaitonText) }
        )
    }
}


extension View {
    func cancelObjectConfirmation(object: CancelObjectViewModel.PrintObject?, shown: Binding<Bool>, onCancel: @escaping () -> Void) -> some View {
        modifier(CancelObjectConfirmation(object: object, shown: shown, onCancel: onCancel))
    }
}
