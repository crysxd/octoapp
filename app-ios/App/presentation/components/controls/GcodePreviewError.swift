//
//  GcodePreviewError.swift
//  OctoApp
//
//  Created by Christian Würthner on 31/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct GcodePreviewError: View {

    var exception: KotlinThrowable
    var onRetry: () -> Void
    
    var body: some View {
        ScreenError(
            error: exception,
            onRetry: onRetry
        )
        .padding(OctoTheme.dimens.margin12)
    }
}

struct GcodePreviewError_Previews: PreviewProvider {
    static var previews: some View {
        GcodePreviewError(
            exception: KotlinThrowable(message: "sdfsd"),
            onRetry: {}
        )
    }
}
