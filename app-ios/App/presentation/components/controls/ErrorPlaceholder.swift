//
//  ErrorPlaceholder.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct ErrorPlaceholder: View {
    
    var throwable: KotlinThrowable
    var reload: () -> Void
    
    var body: some View {
        ScreenError(
            title: String(describing: throwable.composeErrorMessage()),
            description: "",
            error: throwable,
            canDismiss: false,
            onRetry: reload
        )
        .frame(maxWidth: .infinity)
        .padding(OctoTheme.dimens.margin2)
        .surface()
    }
}

struct ErrorPlaceholder_Previews: PreviewProvider {
    static var previews: some View {
        ErrorPlaceholder(
            throwable: KotlinThrowable(message: "Something ")
        ){}
    }
}
