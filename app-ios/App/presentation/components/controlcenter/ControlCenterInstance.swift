//
//  CotnrolCenterInstance.swift
//  OctoApp
//
//  Created by Christian Würthner on 24/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

@MainActor
struct ControlCenterInstance: View {
        
    var instance: ControlCenterViewModel.ControlCenterInstance
    var onClick: () async -> Void
    
    @Environment(\.colorScheme) var colorScheme
    @State private var loading: Bool = false
    var colors: ColorSchemeStruct {
        .dark == colorScheme ? instance.colors.dark : instance.colors.light
    }
    
    var body: some View {
        Button(
            action: {
                Task {
                    loading = true
                    await onClick()
                    loading = false
                }
            },
            label: {
                HStack(spacing: OctoTheme.dimens.margin1) {
                    card
                    button
                }
            }
        )
        .frame(maxWidth: 500)
    }
    
    @ViewBuilder
   
    var button: some View {
        let base = instance.active ? colors.main : OctoTheme.colors.whiteColorScheme
        let accent = Color.white.opacity(0.25)
        
        ZStack {
            Image(systemName: instance.active ? "checkmark" : "arrow.left.arrow.right")
                .opacity(loading ? 0 : 1)
                .padding(OctoTheme.dimens.margin1)
            
            ProgressView()
                .opacity(loading ? 1 : 0)
        }
        .background(Circle().strokeBorder(accent, lineWidth: 2))
        .background(Circle().fill(base.opacity(0.9)))
        .tint(OctoTheme.colors.textColoredBackground)
        .foregroundColor(OctoTheme.colors.textColoredBackground)
        .font(.system(size: 14))
        .animation(.default, value: colors)
        .animation(.default, value: instance.active)
    }
    
    var card: some View {
        ControlCenterInstanceLayout {
            image
            colorStrip
            info
        }
        .frame(maxWidth: .infinity)
        .surface(color: .special(OctoTheme.colors.windowBackground), shadow: true)
    }
    
    var image: some View {
        Color.black
            .overlay(coroppedImage)
            .clipped()
            .animation(.snappy, value: instance.snapshot)
    }
    
    var coroppedImage: some View {
        ZStack(alignment: .center) {
            if let image = instance.snapshot {
                image
                    .resizable()
                    .scaledToFill()
            }
        }
    }
    
    var colorStrip: some View {
        colors.main
    }
    
    var detailText: String {
        if let eta = instance.eta {
            eta.formatEta(compact: false, showLabel: true)
        } else if instance.advertiseCompanion {
            "control_center___plugin_missing"~
        } else {
            " "
        }
    }
    
    var info: some View {
        VStack(alignment: .leading, spacing: 0) {
            infoTitle
            
            ProgressView(value: instance.progress ?? 0, total: 100)
                .padding([.top, .bottom], OctoTheme.dimens.margin01)
                .animation(.spring, value: instance.progress)
            
            VStack(alignment: .leading, spacing: 0) {
                Text(instance.status)
                Text(detailText)
            }
            .animation(.default, value: instance.status)
            .animation(.default, value: detailText)
        }
        .padding(OctoTheme.dimens.margin1)
        .tint(colors.main)
        .typographyLabelSmall()
        .lineLimit(1)
        .frame(maxWidth: .infinity)
    }
    
    var infoTitle: some View {
        HStack {
            Text(instance.label)
                .typographySubtitle()
            
            Spacer()
            
            ZStack {
                if let progress = instance.progress {
                    Text(progress.formatAsPercent())
                        .typographyLabel()
                }
            }
            .animation(.default, value: instance.progress)
        }
    }
}

private struct ControlCenterInstanceLayout: Layout {
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        if subviews.count != 3 {
            assertionFailure("Expected exactly 3 subviews, got \(subviews.count)")
        }
        
        let size =  proposal.replacingUnspecifiedDimensions()
        return subviews[2].sizeThatFits(
            ProposedViewSize(CGSize(width: size.width, height: size.height))
        )
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        let height = bounds.height
        let imageWidth = bounds.height
        let colorStripWidth = OctoTheme.dimens.margin0
        
        subviews[0].place(
            at: p,
            proposal: ProposedViewSize(width: imageWidth, height: height)
        )
        p.x += imageWidth
        
        subviews[1].place(
            at: p,
            proposal: ProposedViewSize(width: colorStripWidth, height: height)
        )
        p.x += colorStripWidth
        
        subviews[2].place(
            at: p,
            proposal: ProposedViewSize(width: bounds.width - imageWidth - colorStripWidth, height: height)
        )
    }
}


struct CotnrolCenterInstance_Previews: PreviewProvider {
    static var previews: some View {
        ControlCenterInstance(
            instance: ControlCenterViewModel.ControlCenterInstance(
                label: "OctoPi",
                instanceId: "some",
                colors: previewColorsStruct,
                status: "Printing 'some file.gcode'",
                progress: 42,
                advertiseCompanion: false,
                eta: 3443,
                snapshot: Image("WebcamTest"),
                active: false
            ),
            onClick: {}
        )
        .previewDisplayName("Printing")
        .padding()
        .background(.black)
        
        ControlCenterInstance(
            instance: ControlCenterViewModel.ControlCenterInstance(
                label: "OctoPi",
                instanceId: "some",
                colors: previewColorsStruct,
                status: "OctoPi is idle",
                progress: nil,
                advertiseCompanion: false,
                eta: nil,
                snapshot: Image("WebcamTest"),
                active: true
            ),
            onClick: {}
        )
        .previewDisplayName("Idle")
        .padding()
        .background(.black)
        
        ControlCenterInstance(
            instance: ControlCenterViewModel.ControlCenterInstance(
                label: "OctoPi",
                instanceId: "some",
                colors: previewColorsStruct,
                status: "OctoPi is idle",
                progress: nil,
                advertiseCompanion: true,
                eta: nil,
                snapshot: nil,
                active: false
            ),
            onClick: {}
        )
        .previewDisplayName("Advertise Companion")
        .padding()
        .background(.black)
    }
}
