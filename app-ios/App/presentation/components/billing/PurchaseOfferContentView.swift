//
//  OfferContentView.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import StoreKit
import OctoAppBase
import MarkdownUI

struct PurchaseOfferContentView: View {
    
    var offers: [PurchaseOfferStruct]
    var purchases: [BillingPurchaseStruct]
    var title: String
    var height: CGFloat
    var purchaseProduct: (Product, Set<Product.PurchaseOption>) async throws -> Void

    @State private var showTerms = false
    @State private var showSubscriptionOptions = false
    @State private var showOnlyLifetimeTransfer = false
    @State private var showRestoreOptions = false
    @State private var showAndroidRestore = false
    @State private var transferQrCode: QrCode? = nil
    @State private var loading: Bool = false
    
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    
    var body: some View {
        ZStack {
            if offers.isEmpty {
                emptyState
            } else {
                offersList
            }
        }
        .manageSubscriptionsSheet(isPresented: $showSubscriptionOptions)
        .frame(minHeight: height)
        .transition(.scale(scale: 0.3, anchor: .top).combined(with: .opacity))
        .frame(maxWidth: .infinity)
        .macOsCompatibleSheet(isPresented: $showTerms) {
            SafariView(url: "https://octoapp-4e438.web.app/terms")
        }
        .macOsCompatibleSheet(item: $transferQrCode) { qrCode in
            TransferInstructions(qrCode: qrCode)
        }
        .alert(LocalizedStringKey("billing___transfer_to_android_only_lifetime"~), isPresented: $showOnlyLifetimeTransfer) {
            Button(role: .none, action: {}, label: {Text("OK") })
        }
    }
    
    var offersList: some View {
        VStack(spacing: OctoTheme.dimens.margin12) {
            testFlightDisclaimer
            titleView
            offersListView
            buttons
            Spacer()
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .padding(.bottom, OctoTheme.dimens.margin12)
    }
    
    @ViewBuilder
    var testFlightDisclaimer: some View {
        if Bundle.main.appStoreReceiptURL?.path().localizedStandardContains("sandboxReceipt") == true {
            Text("billing___tesflight_disclaimer"~)
                .frame(maxWidth: .infinity)
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
                .padding([.top, .bottom], OctoTheme.dimens.margin1)
                .surface(color: .red)
                .padding(.top, OctoTheme.dimens.margin2)
                .typographyBase()
        }
    }
    
    var titleView: some View {
        Text(LocalizedStringKey(title))
            .typographyTitle()
            .padding([.top], OctoTheme.dimens.margin3)
            .padding([.bottom], OctoTheme.dimens.margin1)
            .multilineTextAlignment(.center)
    }
    
    var emptyState: some View {
        VStack {
            ScreenError(
                title: "billing___error_title"~,
                description: "billing___error_description"~,
                canDismiss: true
            )
            .padding(OctoTheme.dimens.margin12)
            buttons
        }
        .onAppear() {
            OctoAnalytics.shared.logEvent(
                event: OctoAnalytics.EventPurchaseMissingSku.shared,
                params: [:]
            )
        }
    }
    
    var offersListView: some View {
        ForEach(offers) { offer in
            PurchaseOfferView(
                offer: offer,
                screenTitle: title,
                purchases: purchases,
                purchaseProduct: purchaseProduct
            )
        }
    }
    
    @ViewBuilder
    var buttons: some View {
        VStack(spacing: -OctoTheme.dimens.margin01) {
            if !purchases.isEmpty {
                OctoButton(
                    text: "billing___manage_subscriptions"~,
                    type: .link,
                    small: true,
                    clickListener: { showSubscriptionOptions = true }
                )
            }
            
            OctoAsyncButton(
                text: "billing___restore"~,
                type: .link,
                small: true,
                loading: loading,
                clickListener: { showRestoreOptions = true }
            )
            .confirmationDialog("billing___restore"~, isPresented: $showRestoreOptions, titleVisibility: .visible) {
                Button("billing___restore_from_app_store"~) {
                    Task {
                        loading = true
                        defer { loading = false }
                        try? await AppStore.sync()
                    }
                }
                
                Button("billing___restore_from_android"~) {
                    showAndroidRestore = true
                }
            }
            .alert("billing___restore_from_android_explainer"~, isPresented: $showAndroidRestore) {
                Button("Ok"~) { }
            }
            
            if !purchases.isEmpty {
                OctoAsyncButton(
                    text: "billing___transfer_to_android"~,
                    type: .link,
                    small: true,
                    clickListener: {
                        // Ensure we have a receipt
                        await ReceiptRefresh().refreshReceipt()
                        try? await AppStore.sync()
                        
                        // Just to be sure everything is processed
                        try? await Task.sleep(for: .seconds(3))
                        
                        // Let's try....
                        if (BillingManager.shared.canTransfer()) {
                            let result = try await BillingManager.shared.announceTransfer()
                            switch result {
                            case result as BillingProductTransferAnnounceResult.Error: orchestrator.postError(
                                error: (result as! BillingProductTransferAnnounceResult.Error).exception
                            )
                            case result as BillingProductTransferAnnounceResult.Success: do {
                                transferQrCode = QrCode(text: (result as! BillingProductTransferAnnounceResult.Success).url)

                            }
                            default: Napier.e(tag: "PurchaseView", message: "Unexpected case: \(String(describing: result))")
                            }
                        } else {
                            showOnlyLifetimeTransfer = true
                            Napier.e(tag: "PurchaseView", message: "No transfers available")
                        }
                    }
                )
            }
            
            OctoButton(
                text: "billing___terms_and_conditions"~,
                type: .link,
                small: true,
                clickListener: { showTerms = true }
            )
        }
    }
}

private struct TransferInstructions : View {
    
    var qrCode: QrCode
    @Environment(\.dismiss) private var dismiss
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin12) {
            qrCode.image
                .resizable()
                .aspectRatio(contentMode: .fit)
            
            Markdown("billing___transfer_to_android_steps"~)
                .typographyBase()
                .frame(maxWidth: .infinity)
            
            OctoButton(text: "done"~) {
                dismiss()
            }
        }
        .padding(OctoTheme.dimens.margin12)
        .asResponsiveSheet()
    }
}

private struct QrCode : Identifiable {
    var text: String
    var id: String { text }
    
    var image: SwiftUI.Image {
        let filter = CIFilter(name: "CIQRCodeGenerator")
        let data = text.data(using: .ascii, allowLossyConversion: false)
        filter?.setValue(data, forKey: "inputMessage")
        let transform = CGAffineTransform(scaleX: 5, y: 5)
        
        let image = filter?.outputImage?.transformed(by: transform)
        let uiImage = image.flatMap { UIImage(ciImage: $0) }
        
        return uiImage?.pngData().flatMap {
            UIImage(data: $0).flatMap {
                Image(uiImage: $0)
            }
        } ?? SwiftUI.Image(systemName: "exclamationmark.triangle.fill")
    }
}

private class ReceiptRefresh : NSObject, SKRequestDelegate {
    
    private typealias RefreshContinuation = CheckedContinuation<Void, Never> // 1
    private let tag = "BillingReceiptRefresh"
    private var continuation: RefreshContinuation? // 2
    
    func refreshReceipt() async {
        Napier.i(tag: tag, message: "Refershing receipt")
        
        return await withCheckedContinuation({ (continuation: RefreshContinuation) in
            self.continuation = continuation
            
            let request = SKReceiptRefreshRequest(receiptProperties: nil)
            request.delegate = self
            request.start()
        })
    }
    
    func requestDidFinish(_ request: SKRequest) {
        Napier.i(tag: tag, message: "Receipt refresh is done")
        continuation?.resume()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error){
        Napier.i(tag: tag, message: "Error: \(error.localizedDescription)")
        continuation?.resume()
    }
}
