//
//  PurchaseInitialContent.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct PurchaseIntialContentView: View {
    
    var title: String
    var description: String
    var highlightBanner: String?
    var sellingPoints: [SellingPointStruct]
    var hasPurchases: Bool
    
    var width: CGFloat
    var height: CGFloat
    var safeAreaBottom: CGFloat
    
    @State private var activeSellingPoint = 0
    @State private var showDetails = false
    @State private var interactionRecorded = false
    @Environment(\.horizontalSizeClass) private var sizeClass
    
    var body: some View {
        VStack(spacing: 0) {
            if let banner = highlightBanner, !hasPurchases {
                Text(LocalizedStringKey(banner))
                    .foregroundColor(OctoTheme.colors.darkText)
                    .typographyBase()
                    .frame(maxWidth: .infinity)
                    .padding(OctoTheme.dimens.margin12)
                    .background(OctoTheme.colors.redColorSchemeLight)
                    .multilineTextAlignment(.center)
            }
            
            
            Text(LocalizedStringKey(hasPurchases ? "billing___supporter_title"~ : title))
                .fixedSize(horizontal: false, vertical: true)
                .typographyTitle()
                .multilineTextAlignment(.center)
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
                .padding(.top, OctoTheme.dimens.margin3)
            
            Text(LocalizedStringKey(hasPurchases ? "billing___supporter_subtitle"~ : description))
                .fixedSize(horizontal: false, vertical: true)
                .typographyBase()
                .padding(.top, OctoTheme.dimens.margin3)
                .padding(.bottom, OctoTheme.dimens.margin2)
                .multilineTextAlignment(.center)
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
            
            sellingPointsDisplay
        }
        .transition(.move(edge: .bottom).combined(with: .opacity))
        .frame(maxWidth: .infinity)
        .fullScreenCover(isPresented: $showDetails) {
            SellingPointDetailsView(
                sellingPoints: sellingPoints,
                activeSellingPoint: $activeSellingPoint
            )
        }
        .onChange(of: activeSellingPoint) {
            if !interactionRecorded && !hasPurchases {
                interactionRecorded = true
                OctoAnalytics.shared.logEvent(
                    event: OctoAnalytics.EventPurchaseUspInteraction.shared,
                    params: [:]
                )
            }
        }
    }
    
    var sellingPointsDisplay: some View {
        VStack(spacing: 0) {
            if width > 0 {
                carousel
            }
            Spacer()
            sellingPointInfo
        }
        .animation(.default, value: activeSellingPoint)
        .frame(maxWidth: .infinity)
        .frame(height: height + safeAreaBottom)
        .background(OctoTheme.colors.inputBackground)
    }
    
    @ViewBuilder
    var sellingPointInfo: some View {
        sellingPointTitle
        sellingPointDescription
        buttonPlaceholder
    }
    
    var sellingPointTitle: some View {
        Text(sellingPoints[activeSellingPoint].title ?? " ")
            .typographySectionHeader()
            .multilineTextAlignment(.center)
            .padding(.bottom, OctoTheme.dimens.margin0)
            .padding([.leading, .trailing], OctoTheme.dimens.margin12)
    }
    
    var sellingPointDescription: some View {
        Text(
            LocalizedStringKey(
                sellingPoints[activeSellingPoint].description.flatMap {
                    StringResourcesKt.parseHtml($0) as? String
                } ?? " "
            )
        )
        .lineLimit(3, reservesSpace: true)
        .typographyLabel()
        .multilineTextAlignment(.center)
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .padding(.bottom, OctoTheme.dimens.margin2)
    }
    
    @ViewBuilder
    var carousel: some View {
        GeometryReader { geo in
            let aspectRatio: CGFloat = 1179 / 2556
            let imageWidth = geo.size.height * aspectRatio
            
            FullscreenCarouselView(
                spacing: OctoTheme.dimens.margin2,
                itemsData: sellingPoints,
                cardWidth: imageWidth,
                activeCard: $activeSellingPoint
            ) { index, sellingPoint in
                let active = index == activeSellingPoint
                
                SellingPointCarouselItem(
                    sellingPoint: sellingPoint
                )
                .opacity(active ? 1 : 0.25)
                .frame(maxWidth: .infinity,maxHeight: .infinity)
                .aspectRatio(aspectRatio, contentMode: .fill)
                .fixedSize(horizontal: true, vertical: false)
                .surface(color: .alternative, shadow: true)
                .onTapGesture {
                    if active && sellingPoint.imageUrl != "party" {
                        showDetails = true
                    } else {
                        activeSellingPoint = index
                    }
                }
            }
            .frame(width: width)
        }
        .padding(.top, OctoTheme.dimens.margin12)
        .padding(.bottom, OctoTheme.dimens.margin12)
    }
    
    var buttonPlaceholder: some View {
        PurchaseShowOffersButton(
            safeAreaBottom: safeAreaBottom,
            hasPurchases: hasPurchases,
            continueCta: " ",
            clickListener: {}
        )
        .opacity(0)
    }
}
