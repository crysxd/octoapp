//
//  Orchestrator.swift
//  OctoApp
//
//  Created by Christian on 20/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import OctoAppBase

struct OrchestratorView: View {
    
    @Environment(\.scenePhase) private var scenePhase
    @Environment(\.colorScheme) private var colorScheme
    @Environment(\.requestReview) var requestReview
    @StateObject var viewModel = OrchestratorViewModel()
    @StateObject var reviewViewModel = AppReviewViewModel()
    @State private var orchestratorBannerHidden: Bool = false
    @State private var orchestratorBannerShrunken: Bool = false
    
    var body: some View {
        ZStack {
            if let active = viewModel.state as? OrchestratorViewModelCore.NavigationStateActive {
                let colorScheme = .dark == colorScheme ? active.colors.dark.toStruct() : active.colors.light.toStruct()
                
                OrchestratorBanner(
                    bannerHidden: $orchestratorBannerHidden,
                    bannerShrunken: $orchestratorBannerShrunken,
                    colorScheme: colorScheme
                ) {
                    Controls()
                }
                .controlCenter()
                .pluginSupport()
                .manageLiveActivity()
                .deeplinkNavigation()
                .environment(\.controlsInEditMode, viewModel.controlsInEditMode)
                .environment(\.instanceId, active.instanceId)
                .environment(\.instanceSystemInfo, active.systemInfo.toStruct())
                .environment(\.instanceLabel, active.instanceLabel)
                .environment(\.instanceHasCompanion, active.hasCompanion)
                .environment(\.instanceCompanionRunning, active.companionRunning)
                .environment(\.instanceColor, colorScheme)
                .onAppear {
                    viewModel.controlCenterShown = false
                }
            } else if viewModel.state is OrchestratorViewModelCore.NavigationStateLogin {
                SignInView(repairForInstanceId: nil)
                    .transition(.opacity)
            } else if let repair = viewModel.state as? OrchestratorViewModelCore.NavigationStateRepair {
                SignInView(repairForInstanceId: repair.instanceId)
                    .transition(.opacity)
            }
        }
        .onChange(of: reviewViewModel.reviewTrigger) { _, trigger in
            if trigger != 0 {
                OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventReviewFlowLaunched.shared, params: [:])
                requestReview()
            }
        }
        .deeplinkNavigation()
        .errorDialogDisplay()
        .observeUiConfiguration()
        .usingViewModel(reviewViewModel, fixedInstanceId: "")
        .environmentObject(viewModel)
        .environment(\.leftyMode, viewModel.leftyMode)
        .environment(\.compactLayout, viewModel.compactLayout)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .animation(.default, value: viewModel.state)
        .animation(.default, value: orchestratorBannerHidden)
        .animation(.default, value: orchestratorBannerShrunken)
        .onChange(of: scenePhase) { _, newScenePhase in
            switch newScenePhase {
            case .active:
                viewModel.onAppear()
            case .background:
                viewModel.onDisappear()
            default:
                print("Unhandled scene state \(newScenePhase)")
            }
        }
    }
}

@MainActor
class AppReviewViewModel : BaseViewModel {
    typealias Core = AppReviewViewModelCore
    
    var currentCore: AppReviewViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var reviewTrigger = 0
    
    func clearData() {
        // Nothing
    }
    
    func createCore(instanceId: String) -> AppReviewViewModelCore {
        return AppReviewViewModelCore()
    }
    
    func publish(core: AppReviewViewModelCore) {
        core.reviewFlowTrigger
            .asPublisher()
            .sink(receiveValue: { (reviewTrigger: AppReviewViewModelCore.Trigger) in
                self.reviewTrigger = Int(reviewTrigger.id)
            })
        .store(in: &bag)
    }
}

@MainActor
class OrchestratorViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private let core = OrchestratorViewModelCore()
    
    @Published private(set) var state: OrchestratorViewModelCore.NavigationState? = nil
    @Published private(set) var banner: OrchestratorBannerData = OrchestratorBannerData(connectionType: nil, instanceLabel: nil, instanceColors: nil, eventTime: Date())
    @Published private(set) var errorDialog: OrchestratorError? = nil
    @Published private(set) var leftyMode: Bool = false
    @Published private(set) var compactLayout: Bool = false
    @Published var controlCenterShown: Bool = false
    @Published var controlsInEditMode: Bool = false
    var errorAction: () -> Void = {}
    
    init() {
        core.navigationState
            .asPublisher()
            .sink(receiveValue: { (state: OrchestratorViewModelCore.NavigationState) in
                self.state = state
            })
            .store(in: &bag)
        
        core.connectionState
            .asPublisher()
            .sink(receiveValue: { (connection: OrchestratorViewModelCore.ConnectionState) in
                let showBanner = connection.showLabelInBanner
                self.banner = OrchestratorBannerData(
                    connectionType: connection.connectionType,
                    instanceLabel: showBanner ? connection.instanceLabel : nil,
                    instanceColors: showBanner ? connection.colors.toStruct() : nil,
                    eventTime: connection.eventTime.toDate()
                )
            })
            .store(in: &bag)
        
        core.errorEvents
            .asPublisher()
            .sink(receiveValue: { (error: OrchestratorViewModelCore.ErrorEvent) in
                self.errorAction = { error.action?() }
                self.errorDialog = OrchestratorError(
                    date: Date(),
                    message: error.messageString,
                    detailMessage: error.detailsMessageString,
                    hasAction: error.action != nil,
                    showDetailsDirectly: false
                )
            })
            .store(in: &bag)
        
        core.uiSettings
            .asPublisher()
            .sink(receiveValue: { (settings: OrchestratorViewModelCore.UiSettings) in
                self.leftyMode = settings.leftyMode
                self.compactLayout = settings.compactLayout
            })
            .store(in: &bag)
    }
    
    func postError(error: KotlinThrowable, showDetailsDirectly: Bool = false) {
        self.errorAction = { }
        self.errorDialog = OrchestratorError(
           date: Date(),
           message: String(describing: error.composeErrorMessage()),
           detailMessage: error.composeTechnicalErrorMessage() as? String ?? error.message,
           hasAction: false,
           showDetailsDirectly: showDetailsDirectly
       )
    }
    
    func activateInstance(instanceId: String) {
        core.activateInstance(instanceId: instanceId)
    }
    
    func onAppear() {
        Task {
            try? await core.onAppOpen()
        }
    }
    
    func onDisappear() {
        Task {
            try? await core.onAppClose()
        }
    }
}

private enum OrchestratorState {
    case active, inactive
}

private struct InstanceIdKey: EnvironmentKey {
    static let defaultValue = "???"
}

private struct InstanceIdSystemInfoKey: EnvironmentKey {
    static let defaultValue = SystemInfoStruct()
}

private struct InstanceHasCompanionKey: EnvironmentKey {
    static let defaultValue: Bool? = nil
}

private struct InstanceCompanionRunning: EnvironmentKey {
    static let defaultValue: Bool? = nil
}


private struct InstanceColorSchemeKey: EnvironmentKey {
    static let defaultValue = OctoColorSchemesLight.shared.default_.toStruct()
}

private struct DismissToRoot: EnvironmentKey {
    static let defaultValue: () -> Void = {}
}

private struct OpenExternalURL: EnvironmentKey {
    static let defaultValue: OpenURLAction = OpenURLAction(handler: { _ in OpenURLAction.Result.discarded })
}

private struct InstanceLabel: EnvironmentKey {
    static let defaultValue = "???"
}

private struct ControlsInEditModeKey: EnvironmentKey {
    static let defaultValue = false
}

private struct LeftyModeKey: EnvironmentKey {
    static let defaultValue = false
}

private struct CompactLayoutKey: EnvironmentKey {
    static let defaultValue = false
}

extension EnvironmentValues {
    var controlsInEditMode: Bool {
        get { self[ControlsInEditModeKey.self] }
        set { self[ControlsInEditModeKey.self] = newValue }
    }
    var instanceId: String {
        get { self[InstanceIdKey.self] }
        set { self[InstanceIdKey.self] = newValue }
    }
    var leftyMode: Bool {
        get { self[LeftyModeKey.self] }
        set { self[LeftyModeKey.self] = newValue }
    }
    var compactLayout: Bool {
        get { self[CompactLayoutKey.self] }
        set { self[CompactLayoutKey.self] = newValue }
    }
    var instanceSystemInfo: SystemInfoStruct {
        get { self[InstanceIdSystemInfoKey.self] }
        set { self[InstanceIdSystemInfoKey.self] = newValue }
    }
    var instanceColor: ColorSchemeStruct {
        get { self[InstanceColorSchemeKey.self] }
        set { self[InstanceColorSchemeKey.self] = newValue }
    }
    var instanceHasCompanion: Bool? {
        get { self[InstanceHasCompanionKey.self] }
        set { self[InstanceHasCompanionKey.self] = newValue }
    }
    var instanceCompanionRunning: Bool? {
        get { self[InstanceCompanionRunning.self] }
        set { self[InstanceCompanionRunning.self] = newValue }
    }
    var instanceLabel: String {
        get { self[InstanceLabel.self] }
        set { self[InstanceLabel.self] = newValue }
    }
    var dismissToRoot: () -> Void {
        get { self[DismissToRoot.self] }
        set { self[DismissToRoot.self] = newValue }
    }
    var openExternalURL: OpenURLAction {
        get { self[OpenExternalURL.self] }
        set { self[OpenExternalURL.self] = newValue }
    }
}
