//
//  TemperatureControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct TemperatureControls: View {
    
    @StateObject var viewModel: TemperatureControlsViewModel
    @State private var showSettings = false
    @Environment(\.instanceId) private var instanceId
    @Namespace private var animation
    
    var body: some View {
        ControlsScaffold(
            title: "widget_temperature"~,
            iconSystemName: "slider.horizontal.3",
            iconAction:  { showSettings = true }
        ) {
            VStack(spacing: OctoTheme.dimens.margin1) {
                temperatureTable(components: viewModel.controllable)
                temperatureTable(components: viewModel.observable)
            }
        }
        .macOsCompatibleSheet(isPresented: $showSettings) {
            MenuHost(menu: TemperatureControlsSettingsMenu(instanceId: instanceId))
        }
    }
    
    @ViewBuilder
    func temperatureTable(components: [TemperatureControlsComponentData]) -> some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            HStack(spacing: OctoTheme.dimens.margin1) {
                if let x = components[safe: 0] {
                    TemperatureControlsComponent(data: x)
                        .matchedGeometryEffect(id: x.component, in: animation)
                }
                
                if let x = components[safe: 1] {
                    TemperatureControlsComponent(data: x)
                        .matchedGeometryEffect(id: x.component, in: animation)
                } else {
                    Spacer().frame(maxWidth: .infinity)
                }
            }
            
            if components.count > 2 {
                HStack(spacing: OctoTheme.dimens.margin1) {
                    if let x = components[safe: 2] {
                        TemperatureControlsComponent(data: x)
                            .matchedGeometryEffect(id: x.component, in: animation)
                    }
                    
                    if let x = components[safe: 3] {
                        TemperatureControlsComponent(data: x)
                            .matchedGeometryEffect(id: x.component, in: animation)
                    } else {
                        Spacer().frame(maxWidth: .infinity)
                    }
                }
            }
            
            if components.count > 4 {
                HStack(spacing: OctoTheme.dimens.margin1) {
                    if let x = components[safe: 4] {
                        TemperatureControlsComponent(data: x)
                            .matchedGeometryEffect(id: x.component, in: animation)
                    }
                    
                    if let x = components[safe: 5] {
                        TemperatureControlsComponent(data: x)
                            .matchedGeometryEffect(id: x.component, in: animation)
                    } else {
                        Spacer().frame(maxWidth: .infinity)
                    }
                }
            }
            
            if components.count > 6 {
                HStack(spacing: OctoTheme.dimens.margin1) {
                    if let x = components[safe: 6] {
                        TemperatureControlsComponent(data: x)
                            .matchedGeometryEffect(id: x.component, in: animation)
                    }
                    
                    if let x = components[safe: 7] {
                        TemperatureControlsComponent(data: x)
                            .matchedGeometryEffect(id: x.component, in: animation)
                    } else {
                        Spacer().frame(maxWidth: .infinity)
                    }
                }
            }
        }
    }
}

private extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

struct TemperatureControls_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureControls(viewModel: TemperatureControlsViewModel())
    }
}
