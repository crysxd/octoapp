//
//  TemperatureControl.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Charts

struct TemperatureControlsComponent: View {
    var data: TemperatureControlsComponentData
    
    @Environment(\.colorScheme) private var colorScheme
    @State private var showInput = false
    private var canControl : Bool {
        switch data {
        case .placeholder(_, let c): return c
        case .snapshot(let s): return s.canControl
        }
    }
    private var snapshot : TemperatureSnapshotStruct? {
        switch data {
        case .placeholder(_, _): return nil
        case .snapshot(let s): return s
        }
    }
    
    private var detailText : String? {
        if !canControl {
            return nil
        }
        
        guard let target = snapshot?.current.target else {
            return " "
        }
        
        if target <= 0 {
            return "target_off"~
        }
        
        guard let offset = snapshot?.offset else {
            return String(format: "target_x"~, Int(target).formatted())
        }
        
        guard offset != 0 else {
            return String(format: "target_x"~, Int(target).formatted())
        }
        
        
        return String(format: "target_x_offset_y"~, Int(target).formatted(), Int(offset).description)
    }
    private var mainText: String {
        snapshot?.current.actual?.formatAsTemperature(minDecimals: 1, maxDecimals: 1) ?? " "
    }
    private var chartGradient : LinearGradient {
        return LinearGradient(
            gradient: Gradient (
                colors: [
                    colorScheme == .dark ? .white.opacity(0.1) : .black.opacity(0.08),
                    colorScheme == .dark ? .white.opacity(0.02) : .black.opacity(0.01),
                ]
            ),
            startPoint: .top,
            endPoint: .bottom
        )
    }
    
    var body: some View {
        if canControl {
            controllable
        } else {
            observable
        }
    }
    
    var observable: some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            Text(snapshot?.componentLabel ?? " ")
                .lineLimit(2)
                .typographyLabelSmall()
            
            Text(mainText)
                .typographyData()
                .lineLimit(1)
                .frame(maxWidth: .infinity, alignment: .trailing)
                .padding([.top, .bottom], OctoTheme.dimens.margin12)
            
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .background { chart }
        .background(tempColor())
        .surface()
    }
    
    var controllable: some View {
        VStack(alignment: .leading, spacing: 0) {
            VStack(alignment: .leading) {
                Text(snapshot?.componentLabel ?? " ")
                    .typographyLabel()
                
                Text(mainText)
                    .typographyDataLarge()
                    .lineLimit(1)
                
                if let detail = detailText {
                    Text(detail)
                        .typographyLabel()
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding([.trailing, .leading], OctoTheme.dimens.margin12)
            .padding([.top], OctoTheme.dimens.margin12)
            .padding([.bottom], OctoTheme.dimens.margin1)
            .background { chart }
            
            OctoButton(text: "set"~, type: .surface, small: true) {
                if snapshot != nil { showInput = true }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .opacity(snapshot == nil ? 0 : 1)
            .macOsCompatibleSheet(isPresented: $showInput) {
                if let s = snapshot {
                    TemperatureChangeView(
                        component: s.component,
                        componentLabel: s.componentLabel,
                        currentOffset: Int(s.offset ?? 0),
                        currentTarget: Int(s.current.target ?? 0)
                    )
                }
            }
        }
        .background(tempColor())
        .surface()
        .animation(.default, value: mainText)
        .animation(.default, value: detailText)
        .fixedSize(horizontal: false, vertical: true)
    }
    
    @ViewBuilder
    var chart: some View {
        if let history = snapshot?.history {
            Chart {
                ForEach(history) { h in
                    AreaMark(
                        x: .value("Time", h.time),
                        yStart: .value("Temp", 0),
                        yEnd: .value("Temp", h.temperature)
                    )
                }
            }
            .animation(.easeIn, value: history.last == nil)
            .chartXAxis(.hidden)
            .chartYAxis(.hidden)
            .foregroundStyle(chartGradient)
            .chartYScale(domain: 0...(snapshot?.maxTemp ?? 100))
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: .infinity)
        }
    }
    
    func tempColor() -> Color {
        let min = Double(25)
        let max = Double(snapshot?.maxTemp ?? 100)
        let current = Double(snapshot?.current.actual ?? Float(min))
        let percent = ((current - min) / (max - min))
        
        
        return OctoTheme.colors.hot.opacity(percent * 0.4)
    }
}

enum TemperatureControlsComponentData : Equatable {
    case snapshot(TemperatureSnapshotStruct), placeholder(String, Bool)
}

extension TemperatureControlsComponentData {
    var component: String {
        switch self {
        case .snapshot(let s): return s.component
        case .placeholder(let s, _): return s
        }
    }
}

extension UIColor {
    static func blend(color1: UIColor, intensity1: CGFloat = 0.5, color2: UIColor, intensity2: CGFloat = 0.5) -> UIColor {
        let total = intensity1 + intensity2
        let l1 = intensity1/total
        let l2 = intensity2/total
        guard l1 > 0 else { return color2 }
        guard l2 > 0 else { return color1 }
        var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        
        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        return UIColor(red: l1*r1 + l2*r2, green: l1*g1 + l2*g2, blue: l1*b1 + l2*b2, alpha: l1*a1 + l2*a2)
    }
}

struct TemperatureControlsComponent_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureControlsComponent(
            data: .snapshot(
                TemperatureSnapshotStruct(
                    component: "T",
                    componentLabel: "Tool 1",
                    current: ComponentTemperatureStruct(actual: 204.4, target: 205),
                    history: [],
                    canControl: true,
                    offset: nil,
                    isHidden: false,
                    maxTemp: 240
                )
            )
        )
        .previewDisplayName("Hotend ON")
        
        TemperatureControlsComponent(
            data: .snapshot(
                TemperatureSnapshotStruct(
                    component: "T",
                    componentLabel: "Tool 1",
                    current: ComponentTemperatureStruct(actual: 20, target: 21),
                    history: [],
                    canControl: true,
                    offset: nil,
                    isHidden: false,
                    maxTemp: 240
                )
            )
        )
        .previewDisplayName("Hotend OFF")
        
        TemperatureControlsComponent(
            data: .snapshot(
                TemperatureSnapshotStruct(
                    component: "T",
                    componentLabel: "Tool 1",
                    current: ComponentTemperatureStruct(actual: 20, target: 21),
                    history: [],
                    canControl: true,
                    offset: 10,
                    isHidden: false,
                    maxTemp: 240
                )
            )
        )
        .previewDisplayName("Hotend ON + offset")
        
        TemperatureControlsComponent(
            data: .snapshot(
                TemperatureSnapshotStruct(
                    component: "A",
                    componentLabel: "Ambient",
                    current: ComponentTemperatureStruct(actual: 20, target: nil),
                    history: [],
                    canControl: false,
                    offset: 10,
                    isHidden: false,
                    maxTemp: 240
                )
            )
        )
        .previewDisplayName("Ambient")
        
        TemperatureControlsComponent(
            data: .placeholder("H", true)
        )
        .previewDisplayName("Placeholder Control")
        
        TemperatureControlsComponent(
            data: .placeholder("H", false)
        )
        .previewDisplayName("Placeholder no Control")
    }
}
