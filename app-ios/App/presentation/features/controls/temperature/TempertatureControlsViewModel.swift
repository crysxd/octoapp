//
//  TempertatureControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

class TemperatureControlsViewModel : BaseViewModel {
    var currentCore: TemperatureControlsViewModelCore? = nil
    var bag:Set<AnyCancellable> = []
    
    @Published var controllable: [TemperatureControlsComponentData] = []
    @Published var observable: [TemperatureControlsComponentData] = []
    
    func clearData() {
        controllable = []
        observable = []
    }
    
    func createCore(instanceId: String) -> TemperatureControlsViewModelCore {
        return TemperatureControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: Core) {
        controllable = core.initialConfiguration.components.filter { c in c.canControl }.map { c in .placeholder(c.component, c.canControl) }
        observable = core.initialConfiguration.components.filter { c in !c.canControl }.map { c in .placeholder(c.component, c.canControl) }
        
        core.temperatures.asPublisher()
            .sink { (state: TemperatureControlsViewModelCore.State) in
                // Sometimes we get empty right after connect. Keep defaults.
                if state.components.isEmpty {
                    self.controllable = core.initialConfiguration.components.filter { c in c.canControl }.map { c in .placeholder(c.component, c.canControl) }
                    self.observable = core.initialConfiguration.components.filter { c in !c.canControl }.map { c in .placeholder(c.component, c.canControl) }
                } else {
                    self.controllable = state.components.filter { c in c.canControl }.map { .snapshot($0.toStruct()) }
                    self.observable = state.components.filter { c in !c.canControl }.map { .snapshot($0.toStruct()) }
                }
            }
            .store(in: &bag)
    }
}

extension [TemperatureControlsComponentData] {
    public var type: String { map { $0.component }.joined() }
}
