//
//  PrintConfidenceViewModel.swift
//  OctoApp
//
//  Created by Christian Würthner on 23/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

class PrintConfidenceViewModel: BaseViewModel {
    var currentCore: PrintConfidenceViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state = State.unavailable
    
    func createCore(instanceId: String) -> PrintConfidenceViewModelCore {
        return PrintConfidenceViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: PrintConfidenceViewModelCore) {
        core.state.asPublisher()
            .sink { (state: PrintConfidenceViewModelCore.State) in
                if let confidence = state.confidence {
                    self.state = .available(
                        description: confidence.description_,
                        level: Level(level: confidence.level),
                        origin: Origin(origin: confidence.origin)
                    )
                } else {
                    self.state = .unavailable
                }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = State.unavailable
    }
}

extension PrintConfidenceViewModel {
    enum State: Equatable {
        case unavailable
        case available(description: String, level: Level, origin: Origin)
    }
    
    enum Origin {
        case OctoEverywhere, Obico
        
        init(origin: PrintConfidence.Origin) {
            switch origin {
            case .obico: self = .Obico
            case .octoeverywhere: self = .OctoEverywhere
            default: self = .OctoEverywhere
            }
        }
    }
    
    enum Level {
        case High, Medium, Low, Unknown
        
        init(level: PrintConfidence.Level) {
            switch level {
            case .low: self = .Low
            case .medium: self = .Medium
            case .high: self = .High
            default: self = .Unknown
            }
        }
    }
}
