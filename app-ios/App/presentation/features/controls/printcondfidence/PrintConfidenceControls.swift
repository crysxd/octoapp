//
//  PrintConfidenceControls.swift
//  OctoApp
//
//  Created by Christian Würthner on 23/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct PrintConfidenceControls<Content: View>: View {
    
    @StateObject var viewModel: PrintConfidenceViewModel
    @ViewBuilder var content: () -> Content
    
    var body: some View {
        PrintConfidenceContent(
            confidence: viewModel.state,
            content: content
        )
    }
}

private struct PrintConfidenceContent<Content: View>: View {
    
    var confidence: PrintConfidenceViewModel.State
    @ViewBuilder var content: () -> Content
    
    private var backgroundColor: Color {
        switch confidence {
        case .available(_, _, origin: let origin): switch origin {
        case .Obico: return OctoTheme.colors.externalObico
        case .OctoEverywhere: return OctoTheme.colors.externalOctoEverywhere
        }
        default: return Color.clear
        }
    }
    
    var body: some View {
        let shape = RoundedRectangle(cornerRadius: OctoTheme.dimens.cornerRadius)
        
        VStack(spacing: 0) {
            content().clipShape(RoundedRectangle(cornerRadius: OctoTheme.dimens.cornerRadius))
            confidenceDisplay
        }
        .background(shape.strokeBorder(.white.opacity(0.05), lineWidth: 2))
        .background(backgroundColor)
        .clipShape(shape)
    }
    
    @ViewBuilder
    var confidenceDisplay: some View {
        switch confidence {
        case .unavailable: do {}
        case .available(
            description: let description,
            level: let level,
            origin: let origin
        ): PrintConfidenceDisplay(
            description: description,
            level: level,
            origin: origin
        )
        }
    }
}

private struct PrintConfidenceDisplay: View {
    
    var description: String
    var level: PrintConfidenceViewModel.Level
    var origin: PrintConfidenceViewModel.Origin
    
    @Environment(\.openURL) var openUrl
    
    private var avatarImageName: String {
        switch origin {
        case .Obico: return "obicoSmall"
        case .OctoEverywhere: switch level {
        case .High: return "gadgetGreen"
        case .Medium: return "gadgetYellow"
        case .Low: return "gadgetRed"
        case .Unknown: return "gadgetYellow"
        }
        }
    }
    
    private var disclaimerText: String {
        switch origin {
        case .Obico: return "print_confidence_disclaimer_obico"~
        case .OctoEverywhere: return "print_confidence_disclaimer_octoeverywhere"~
        }
    }
    
    private var contentClickUrl: String {
        switch origin {
        case .Obico: return "https://app.obico.io/printers/"
        case .OctoEverywhere: return "https://octoeverywhere.com/gadget"
        }
    }
    
    var body: some View {
        HStack {
            avatar
            bubble
            Spacer()
            disclaimer
        }
        .padding(OctoTheme.dimens.margin1)
        .onTapGesture { openUrl(contentClickUrl) }
    }
    
    var avatar: some View {
        Image(avatarImageName)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(height: 24)
    }
    
    var bubble: some View {
        HStack(spacing: 0) {
            Image("chatBubbleStart")
            Text(description)
                .lineLimit(1)
                .foregroundColor(.black)
                .typographyLabel()
                .truncationMode(.tail)
                .fixedSize(horizontal: false, vertical: false)
                .background(chatBubbleCenter)
            Image("chatBubbleEnd")
        }
        .id(description)
    }
    
    var chatBubbleCenter: some View {
        Image("chatBubbleCenter")
            .resizable(resizingMode: .stretch)
            .frame(maxWidth: .infinity)
            .fixedSize(horizontal: false, vertical: true)
    }
    
    var disclaimer: some View {
        Text(disclaimerText)
            .foregroundColor(OctoTheme.colors.white)
            .opacity(0.7)
            .lineLimit(2)
            .multilineTextAlignment(.trailing)
            .typographyLabelTiny()
        
    }
}

private struct MockWebcam : View {
    var body: some View {
        HStack {
        }
        .frame(maxWidth: .infinity)
        .frame(height: 200)
        .background(.black)
    }
}

struct PrintConfidenceControls_Previews: PreviewProvider {
    static var previews: some View {
        PrintConfidenceContent(confidence: .unavailable) {
            MockWebcam()
        }
        .previewDisplayName("None")
        .padding()
        
        PrintConfidenceContent(
            confidence: .available(
                description: "Some description",
                level: .Low,
                origin: .OctoEverywhere
            )
        ) {
            MockWebcam()
        }
        .previewDisplayName("OE / Low")
        .padding()
        
        PrintConfidenceContent(
            confidence: .available(
                description: "Some description",
                level: .Medium,
                origin: .OctoEverywhere
            )
        ) {
            MockWebcam()
        }
        .previewDisplayName("OE / Medium")
        .padding()
        
        PrintConfidenceContent(
            confidence: .available(
                description: "Some description",
                level: .High,
                origin: .OctoEverywhere
            )
        ) {
            MockWebcam()
        }
        .previewDisplayName("OE / High")
        .padding()
        
        PrintConfidenceContent(
            confidence: .available(
                description: "Some description",
                level: .Unknown,
                origin: .OctoEverywhere
            )
        ) {
            MockWebcam()
        }
        .previewDisplayName("OE / Unknown")
        .padding()
        
        PrintConfidenceContent(
            confidence: .available(
                description: "Some descripg h hg hfy fftftyftyf yto fit here",
                level: .High,
                origin: .Obico
            )
        ) {
            MockWebcam()
        }
        .previewDisplayName("Obico")
        .padding()
        
    }
}
