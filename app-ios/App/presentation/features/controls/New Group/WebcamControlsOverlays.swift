//
//  WebcamControlsOverlays.swift
//  OctoApp
//
//  Created by Christian on 07/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct WebcamOverlayInputs {
    var liveBadgeState: WebcamLiveBadge.State
    var zoomedIn: Bool
    var displayName: String?
    var snapshot: (() async throws -> UIImage)?
    var pictureInPicture: (() async throws -> Void)?
}

struct WebcamTopBar : View {
    var nativeResolution: CGSize
    var liveState: WebcamLiveBadge.State
    var displayName: String?
    
    @StateObject private var viewModel = ViewModel()
    @EnvironmentObject private var webcamViewModel: WebcamControlsViewModel
    private var nextFrameDelay: Double {
        if case .frame(_, frames: let frames, _, _) = webcamViewModel.state {
            Double(frames.frameInterval.components.seconds)
        } else {
            0
        }
    }
    
    var body: some View {
        HStack {
            name
            resolution
            Spacer()
            liveBadge
        }
        .padding(OctoTheme.dimens.margin1)
        .animation(.default, value: viewModel.showName)
        .animation(.default, value: viewModel.showResolution)
        .animation(.default, value: displayName)
    }
    
    @ViewBuilder
    private var resolution: some View {
        if viewModel.showResolution, nativeResolution.width > 0, nativeResolution.height > 0 {
            textTag(text: "\(Int(min(nativeResolution.width, nativeResolution.height)))p")
        }
    }
    
    @ViewBuilder
    private var name: some View {
        if let name = displayName, viewModel.showName {
            textTag(text: name)
        }
    }
    
    @ViewBuilder
    private var liveBadge: some View {
        WebcamLiveBadge(
            state: WebcamLiveBadge.StateWithDelay(
                state: liveState,
                nextFrameDelay: nextFrameDelay
            )
        )
    }
    
    private func textTag(text: String) -> some View {
        TextTag(
            text: text,
            color: .black.opacity(0.2)
        )
    }
}

struct WebcamBottomBar<ExtraContent: View> : View {
    
    @State private var showSettings: Bool = false
    @State private var showShareMenu: Bool = false
    @State private var showShareSheet: Bool = false
    @State private var shareItem: ShareItem? = nil
    @State private var shareLoading: Bool = false
    @Environment(\.openURL) private var openUrl
    @Environment(\.dismiss) private var dismiss
    @Environment(\.instanceLabel) private var instanceLabel
    @Environment(\.instanceId) private var instanceId
    @EnvironmentObject private var webcamViewModel: WebcamControlsViewModel
    
    var isFullscreen: Bool
    var bottomPadding: CGFloat = 0
    var requestSnapshot: (() async throws -> UIImage)?
    var pictureInPicture: (() async throws -> Void)?
    @ViewBuilder var extraContent: () -> ExtraContent
    
    var body: some View {
        VStack {
            extraContentContainer
            HStack(alignment: .bottom) {
                switchWebcamButton
                fullscreenSettingsButton
                pictureInPictureButton
                shareButton
                Spacer()
                toggleFullscreenButton
            }
        }
        .padding(.top, OctoTheme.dimens.margin2)
        .padding([.leading, .trailing], OctoTheme.dimens.margin01)
        .padding(.bottom, bottomPadding)
        .transition(.move(edge: .trailing).combined(with: .opacity))
        .background(background)
        .animation(.default, value: webcamViewModel.state.canFullscreen)
        .animation(.default, value: webcamViewModel.state.canSwitchWebcam)
        .animation(.default, value: requestSnapshot != nil)
        .animation(.default, value: pictureInPicture != nil)
        .macOsCompatibleSheet(isPresented: $showSettings) {
            MenuHost(menu: ProgressControlsSettingsMenu(role: .forwebcamfullscreen))
        }
    }
    
    @ViewBuilder
    private var extraContentContainer: some View {
        extraContent().padding([.leading, .trailing], OctoTheme.dimens.margin1)
    }
    
    @ViewBuilder
    private var switchWebcamButton : some View {
        if webcamViewModel.state.canSwitchWebcam {
            OctoIconButton(
                icon: "arrow.triangle.2.circlepath.camera.fill",
                color: OctoTheme.colors.textColoredBackground,
                clickListener: { webcamViewModel.nextWebcam() }
            )
        }
    }
    
    @ViewBuilder
    private var pictureInPictureButton : some View {
        if let pip = pictureInPicture {
            OctoAsyncIconButton(
                icon: "pip.enter",
                color: OctoTheme.colors.textColoredBackground,
                clickListener: { try await pip() }
            )
        }
    }
    
    @ViewBuilder
    private var shareButton : some View {
        if let rs = requestSnapshot {
            OctoAsyncIconButton(
                icon: "square.and.arrow.up.fill",
                color: OctoTheme.colors.textColoredBackground,
                loading: shareLoading
            ) {
                shareItem = ShareItem(data: try await rs())
                triggerSharing()
            }
            .macOsCompatibleSheet(isPresented: $showShareMenu) {
                MenuHost(menu: WebcamShareMenu(instanceId: instanceId)) { result in
                   handleShareMenuResult(result: result)
                }
            }
            .sheet(isPresented: $showShareSheet) {
                if let data = shareItem?.data {
                    ShareSheet(activityItems: [data])
                } else {
                    ScreenError()
                }
            }
        }
    }
    
    @ViewBuilder
    private var fullscreenSettingsButton : some View {
        if isFullscreen {
            OctoAsyncIconButton(
                icon: "slider.horizontal.3",
                color: OctoTheme.colors.textColoredBackground,
                clickListener: { showSettings = true }
            )
        }
    }
    
    @ViewBuilder
    private var toggleFullscreenButton: some View {
        if webcamViewModel.state.canFullscreen || isFullscreen {
            OctoAsyncIconButton(
                icon: isFullscreen ? "arrow.down.right.and.arrow.up.left" : "arrow.up.backward.and.arrow.down.forward",
                color: OctoTheme.colors.textColoredBackground
            ) {
                if isFullscreen {
                    dismiss()
                } else {
                    openUrl(UriLibrary.shared.getWebcamUri())
                }
            }
        }
    }
    
    private var background: some View {
        VStack(spacing: 0) {
            let color = Color.black.opacity(0.4)
            
            LinearGradient(
                gradient: Gradient(colors: [color.opacity(0), color]),
                startPoint: .top,
                endPoint: .bottom
            )
            .frame(height: OctoTheme.dimens.margin4)
            
            color
        }
        .ignoresSafeArea([.all], edges: .horizontal)
    }
    
    private func handleShareMenuResult(result: String) {
        shareLoading = true
        // shareItem is already initiali
        if let link = (WebcamShareMenu.companion.getSharedLink(result: result).flatMap { URL(string: $0) }) {
            shareItem = ShareItem(data: link)
        } else if WebcamShareMenu.companion.isShareImage(result: result) {
            // shareItem already contains the image -> nothign to do
        }
        
        let hadShareMenu = showShareMenu
        showShareMenu = false
        
        Task {
            if hadShareMenu {
                // Wait a second to ensure smooth transition from one sheet to the other
                try? await Task.sleep(for: .seconds(1))
            }
            showShareSheet = shareItem != nil
            shareLoading = false
        }
    }
    
    private func triggerSharing() {
        if let result = WebcamShareMenu.companion.predetermineCloseMenuResult(instanceId: instanceId) {
            handleShareMenuResult(result: result)
        } else {
            showShareMenu = true
        }
    }
}

private class ViewModel : ObservableObject {
    
    @Published var showResolution = false
    @Published var showName = false
    
    init() {
        SharedBaseInjector.shared.getOrNull()?.preferences.updatedFlow2.asPublisher()
            .map { (prefs: OctoPreferences) in prefs.isShowWebcamResolution }
            .assign(to: &$showResolution)
        SharedBaseInjector.shared.getOrNull()?.preferences.updatedFlow2.asPublisher()
            .map { (prefs: OctoPreferences) in prefs.isShowWebcamName }
            .assign(to: &$showName)
    }
}

private extension WebcamControlsViewModel.State {
    var canFullscreen: Bool {
        if case .error(_, _ ,_ , _, _, _) = self{
            return false
        } else {
            return true
        }
    }
    var canSwitchWebcam: Bool {
        switch self {
        case .hidden: return false
            
        case .loading(
            canSwitchWebcam: let canSwitchWebcam,
            displayName: _
        ): return canSwitchWebcam
            
        case .error(
            canSwitchWebcam: let canSwitchWebcam,
            exception: _,
            description: _,
            hasDetails: _,
            canRetry: _,
            displayName: _
        ): return canSwitchWebcam
            
        case .frame(
            canSwitchWebcam: let canSwitchWebcam,
            frames: _,
            transformation: _,
            displayName: _
        ): return canSwitchWebcam
            
        case .webpage(
            canSwitchWebcam: let canSwitchWebcam,
            content: _,
            transformation: _,
            displayName: _
        ): return canSwitchWebcam
            
        case .notAvailable(
            canSwitchWebcam: let canSwitchWebcam,
            featureName: _,
            displayName: _
        ): return canSwitchWebcam
        }
    }
}


//
//struct WebcamControlsOverlays_Previews: PreviewProvider {
//    static var previews: some View {
//        VStack {
//            WebcamTopBar(
//                nativeResolution: CGSize(width: 1280, height: 720),
//                isLive: true,
//                isLiveFilled: false,
//                nextFrameDelaySecs: 5
//            )
//            Spacer()
//            WebcamBottomBar(
//                canSwitchWebcam: true,
//                canFullscreen: true,
//                switchWebcam: {}
//            )
//        }
//        .surface(color: .special(.green))
//        .frame(maxWidth: .infinity, maxHeight: .infinity)
//        .aspectRatio(16/9, contentMode: .fit)
//        .padding()
//    }
//}
