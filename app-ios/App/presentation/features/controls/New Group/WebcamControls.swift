//
//  WebcamControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct WebcamControls: View {
    
    @StateObject var viewModel : WebcamControlsViewModel
    @StateObject var confidenceViewModel : PrintConfidenceViewModel
    @Binding var nativeImageSize: CGSize
    @State private var showSettings = false
    @Environment(\.instanceId) var instanceId
    
    private var nativeAspectRatio: CGFloat? {
        nativeImageSize != CGSize.zero ? (nativeImageSize.width / nativeImageSize.height) : nil
    }

    var body: some View {
        if viewModel.state != .hidden {
            ControlsScaffold(
                title: "webcam"~,
                iconSystemName: "slider.horizontal.3",
                iconAction:  { showSettings = true }
            ) {
                PrintConfidenceControls(viewModel: confidenceViewModel) {
                    WebcamDisplay(canZoom: true, nativeImageSize: $nativeImageSize) { inputs in
                        overlay(inputs: inputs)
                    }
                    .surface(color: .special(.black))
                    .webcamFrame(
                        nativeAspectRatio: nativeAspectRatio,
                        initialAspectRatio: CGFloat(viewModel.initialAspectRatio),
                        configAspectRatio: CGFloat(viewModel.configAspectRatio),
                        storeAspectRatio: { viewModel.storeAspectRatio(aspectRatio: Float($0)) }
                    )
                    .openFullscreenOnTab()
                    .deeplinkNavigation()
                    .environmentObject(viewModel)
                }
            }
            .macOsCompatibleSheet(isPresented: $showSettings) {
                MenuHost(menu: WebcamSettingsMenu(instanceId: instanceId))
            }
        }
    }
    
    @ViewBuilder
    func overlay(inputs: WebcamOverlayInputs) -> some View {
        VStack {
            WebcamTopBar(
                nativeResolution: nativeImageSize,
                liveState: inputs.liveBadgeState,
                displayName: inputs.displayName
            )
            Spacer()
            WebcamBottomBar(
                isFullscreen: false,
                requestSnapshot: inputs.snapshot,
                pictureInPicture: inputs.pictureInPicture,
                extraContent:  {}
            )
        }
    }
}
private struct OpenFullscreenOnTab: ViewModifier {
    
    @Environment(\.openURL) private var openUrl
    
    func body(content: Content) -> some View {
        content.onTapGesture { openUrl(UriLibrary.shared.getWebcamUri()) }
    }
}


private extension View {
    func openFullscreenOnTab() -> some View {
        modifier(OpenFullscreenOnTab())
    }
}
