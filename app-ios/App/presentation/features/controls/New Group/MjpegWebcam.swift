//
//  MjpegWebcam.swift
//  OctoApp
//
//  Created by Christian on 07/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

@MainActor
struct MjpegWebcam : View {
    var frames: MjpegFrameMediator
    var transformation: WebcamTransformation
    var collectEvenOdd: Bool
    var tinted = false
    var canZoom = true
    var showScale = false
    var fill = false
    var onImageSizeChange: (CGSize) -> Void
    var onSnapshotAvailable: ((() async throws -> UIImage)?) -> Void
    
    @Binding var isLive: Bool
    @Binding var evenOddFrame: Bool
    @Binding var zoomedIn: Bool
    
    @State private var sinkId = UUID().description
    @Environment(\.scenePhase) private var scenePhase
    
    var body: some View {
        ZStack {
            FrameDisplay(
                frames: frames,
                sinkId: sinkId,
                isLive: $isLive,
                evenOddFrame: $evenOddFrame,
                collectEvenOdd: collectEvenOdd
            )
            .onDisappear {
                // Disconnect this surface from the mediator
                frames.mediator.popSink(sinkId: sinkId)
            }
            .onChange(of: frames.size, initial: true) {  _, size in
                onImageSizeChange(size)
                onSnapshotAvailable {
                    guard let image = frames.mediator.currentData?.image else {
                        throw MjpegWebcamError(message: "No image available")
                    }
                    return image
                }
            }
            .id(frames.mediator.id)
            .webcamTransformations(
                transformation: transformation,
                showScale: showScale,
                canZoom: canZoom,
                imageSize: frames.size,
                zoomedIn: $zoomedIn
            )
            .blur(radius: tinted ? 3 : 0)
            
            if tinted {
                Color.black.opacity(0.15)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .animation(.default, value: tinted)
        .animation(.default, value: isLive)
    }
}

@MainActor
private struct FrameDisplay: UIViewRepresentable {
    let frames: MjpegFrameMediator
    let sinkId: String
    @Binding var isLive: Bool
    @Binding var evenOddFrame: Bool
    var collectEvenOdd: Bool
    
    func makeUIView(context: Context) -> some UIView {
        let view = UIImageView(frame: .zero)
        view.contentMode = .scaleAspectFit
        var lastTask: Task<Void, Never>? = nil
        var counter = 0
        var didLogResolution = false
        
        // Add this sink to the mediator
        frames.mediator.pushMainThreadSink(sinkId: sinkId) { frame in
            guard let frame = frame else { return }
            view.image = frame.image
            
            if !didLogResolution {
                didLogResolution = true
                Napier.i(tag: "FrameDisplay", message: "Frame resolution: \(frame.image.size.width)x\(frame.image.size.height)")
            }
            
            lastTask?.cancel()
            lastTask = Task {
                counter += 1
                if !isLive {
                    isLive = true
                }
                
                if collectEvenOdd {
                    evenOddFrame = !evenOddFrame
                }
                
                let delay = Duration.milliseconds(max(1000, frame.frameInterval))
                try? await Task.sleep(for: delay)
                if !Task.isCancelled {
                    isLive = false
                }
            }
        }
        
        return view
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}


struct MjpegFrameMediator: Equatable {
    var mediator: LowOverheadMediator<WebcamControlsViewModelCore.StateMjpegReadyFrame>
    var frameInterval: Duration
    var size: CGSize
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.mediator.isEqual(rhs.mediator)
    }
}

private struct PreviewWebcam : View {
    
    var rotate90 = false
    var flipH = false
    var flipV = false
    var aspect: CGFloat = 16 / 9
    var fill = false
    @State private var isLive = false
    @State private var zoomedIn = false
    @State private var evenOddFrame = false
    @State private var nativeAspect: CGFloat? = nil
    
    var body: some View {
        ZStack{
            MjpegWebcam(
                frames: MjpegFrameMediator(
                    mediator: LowOverheadMediator(
                        initialData: WebcamControlsViewModelCore.StateMjpegReadyFrame(
                            image: UIImage(named: "WebcamTest")!,
                            fps: 0,
                            frameTime: 0,
                            frameInterval: 1000
                        )
                    ),
                    frameInterval: .seconds(1),
                    size: CGSize(width: 1920, height: 1080)
                ),
                transformation: WebcamTransformation(
                    rotate90: rotate90,
                    flipH: flipH,
                    flipV: flipV
                ),
                collectEvenOdd: false,
                showScale: true,
                fill: fill,
                onImageSizeChange: { _ in },
                onSnapshotAvailable: { _ in },
                isLive: $isLive,
                evenOddFrame: $evenOddFrame,
                zoomedIn: $zoomedIn
            )
        }
        .webcamFrame(
            nativeAspectRatio: nativeAspect,
            initialAspectRatio: aspect,
            configAspectRatio: 16 / 9,
            storeAspectRatio: { nativeAspect =  $0 }
        )
        .frame(height: 0)
        .padding()
    }
}

private struct MjpegWebcamError : Error {
    var message: String
}

struct MjpegImage_Previews: PreviewProvider {
    
    static var previews: some View {
        let fill = false
        PreviewWebcam(rotate90: false, flipH: false, flipV: false, aspect: 16/9, fill: fill).previewDisplayName("16:9")
        PreviewWebcam(rotate90: true, flipH: false, flipV: false, aspect: 16/9, fill: fill).previewDisplayName("16:9 + 90°")
        PreviewWebcam(rotate90: false, flipH: false, flipV: true, aspect: 16/9, fill: fill).previewDisplayName("16:9 + V")
        PreviewWebcam(rotate90: false, flipH: true, flipV: false, aspect: 16/9, fill: fill).previewDisplayName("16:9 + H")
        PreviewWebcam(rotate90: true, flipH: true, flipV: false, aspect: 21/9, fill: fill).previewDisplayName("21:9")
        PreviewWebcam(rotate90: false, flipH: true, flipV: false, aspect: 9/21, fill: fill).previewDisplayName("9:21")
        PreviewWebcam(rotate90: true, flipH: true, flipV: false, aspect: 21/9, fill: fill).previewDisplayName("21:9 + 90°")
        PreviewWebcam(rotate90: false, flipH: false, flipV: false, aspect: 4/3, fill: fill).previewDisplayName("4:3")
        PreviewWebcam(rotate90: true, flipH: true, flipV: false, aspect: 4/3, fill: fill).previewDisplayName("4:3 + 90°")
        PreviewWebcam(rotate90: false, flipH: false, flipV: false, aspect: 3/4, fill: fill).previewDisplayName("3:4 + 90°")
        
    }
}
