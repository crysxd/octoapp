//
//  WebcamControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

class WebcamControlsViewModel : BaseViewModel {
    
    var bag:Set<AnyCancellable> = []
    var currentCore: WebcamControlsViewModelCore?
    private let tag = "WebcamControlsViewModel"
    @Published var state: WebcamControlsViewModel.State = .loading()
    @Published var configAspectRatio: Float = 16/9
    @Published var initialAspectRatio: Float = 16/9
    
    func createCore(instanceId: String) -> WebcamControlsViewModelCore {
        Napier.i(tag: tag, message: "Create core for \(instanceId)")
        return WebcamControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: WebcamControlsViewModelCore) {
        Napier.i(tag: tag, message: "Publish")
        initialAspectRatio = core.getInitialAspectRatio()
        doPublish(core: core)
    }

    private func doPublish(core: WebcamControlsViewModelCore) {
        Napier.i(tag: tag, message: "Do publishing")

        core.configAspectRatio.asPublisher()
            .sink { (ratio: WebcamControlsViewModelCore.AspectRatio) in
                self.configAspectRatio = ratio.float_
            }
            .store(in: &bag)
        
        core.state.asPublisher()
            .sink { (result: WebcamControlsViewModelCore.State) in
                Napier.i(tag: self.tag, message: "[Automatic light] State: \(result)")

                if result is WebcamControlsViewModelCore.StateHidden {
                    self.state = .hidden
                } else if let loading = result as? WebcamControlsViewModelCore.StateLoading {
                    Napier.i(tag: self.tag, message: "Moved to loading")
                    self.state = .loading(
                        canSwitchWebcam: loading.webcamCount > 1,
                        displayName: loading.displayName
                    )
                } else if let mjpeg = result as? WebcamControlsViewModelCore.StateMjpegReady {
                    Napier.i(tag: self.tag, message: "Moved to MJPEG")
                    self.state = .frame(
                        canSwitchWebcam: mjpeg.webcamCount > 1,
                        frames: MjpegFrameMediator(
                            mediator: mjpeg.frames,
                            frameInterval: .seconds(Double(mjpeg.frameInterval) / Double(1000)),
                            size: CGSize(width: Double(mjpeg.frameWidth), height: Double(mjpeg.frameHeight))
                        ),
                        transformation: WebcamTransformation(
                            rotate90: mjpeg.rotate90,
                            flipH: mjpeg.flipH,
                            flipV: mjpeg.flipV
                        ),
                        displayName: mjpeg.displayName
                    )
                } else if let error = result as? WebcamControlsViewModelCore.StateError {
                    Napier.i(tag: self.tag, message: "Moved to error")
                    self.state = .error(
                        canSwitchWebcam: error.webcamCount > 1,
                        exception: error.exception,
                        description: error.description_,
                        hasDetails: error.canShowDetails,
                        canRetry: error.canRetry,
                        displayName: error.displayName
                    )
                } else if let notAvailable = result as? WebcamControlsViewModelCore.StateFeatureNotAvailable {
                    Napier.i(tag: self.tag, message: "Moved to not available")
                    self.state = .notAvailable(
                        canSwitchWebcam: notAvailable.webcamCount > 1,
                        featureName: notAvailable.featureName,
                        displayName: notAvailable.displayName
                    )
                } else if let webpage = result as? WebcamControlsViewModelCore.StateWebpageReady {
                    Napier.i(tag: self.tag, message: "Moved to html")
                    self.state = .webpage(
                        canSwitchWebcam: webpage.webcamCount > 1,
                        content: WebContent(
                            html: webpage.content.html,
                            baseUrl: webpage.content.baseUrl
                        ),
                        transformation: WebcamTransformation(
                            rotate90: webpage.rotate90,
                            flipH: webpage.flipH,
                            flipV: webpage.flipV
                        ),
                        displayName: webpage.displayName
                    )
                }
            }
            .store(in: &bag)
    }
    
    func onUiStarted() {
        Napier.i(tag: tag, message: "[Automatic light] UI started")
        bag = []
        if let cc = currentCore {
            doPublish(core: cc)
        }
    }
    
    func onUiStopped() {
        Napier.i(tag: tag, message: "[Automatic light] UI stopped")
        bag = []
    }
    
    func clearData() {
        Napier.i(tag: tag, message: "Clearing data, now loading")
        state = .loading()
        bag = []
    }
    
    func storeAspectRatio(aspectRatio: Float) {
        Napier.i(tag: tag, message: "Storing aspect ratio")
        currentCore?.storeAspectRatio(aspectRatio: aspectRatio)
    }
    
    func nextWebcam() {
        Napier.i(tag: tag, message: "Next webcam")
        currentCore?.nextWebcam()
    }
    
    func retry() {
        Napier.i(tag: tag, message: "Retry")
        currentCore?.retry()
    }
}

extension WebcamControlsViewModel {
    enum State : Equatable {
        case hidden
        
        case loading(
            canSwitchWebcam: Bool = false,
            displayName: String? = nil
        )
        
        case frame(
            canSwitchWebcam: Bool,
            frames: MjpegFrameMediator,
            transformation: WebcamTransformation,
            displayName: String?
        )
        
        case webpage(
            canSwitchWebcam: Bool,
            content: WebcamControlsViewModel.WebContent,
            transformation: WebcamTransformation,
            displayName: String?
        )
        
        case error(
            canSwitchWebcam: Bool,
            exception: KotlinThrowable,
            description: String?,
            hasDetails: Bool,
            canRetry: Bool,
            displayName: String?
        )
        
        case notAvailable(
            canSwitchWebcam: Bool,
            featureName: String,
            displayName: String?
        )
    }

    struct WebContent : Equatable, Hashable {
        let html: String
        let baseUrl: String
    }
}

extension WebcamControlsViewModel.State {
    var type: Int {
        var hasher = Hasher()
        
        switch self {
        case .hidden: -1
        case .loading(_, displayName: let displayName): do {
            hasher.combine(displayName)
            hasher.combine(0)
        }
        case .error(_, exception: let exception, _, _, _, displayName: let displayName): do {
            hasher.combine(displayName)
            hasher.combine(exception)
            hasher.combine(1)
        }
        case .frame(_, frames: let frames, _,  displayName: let displayName): do {
            hasher.combine(displayName)
            hasher.combine(frames.mediator.id)
            hasher.combine(2)
        }
        case .webpage(_, content: let content, _, displayName: let displayName): do {
            hasher.combine(displayName)
            hasher.combine(content)
            hasher.combine(3)
        }
        case .notAvailable(_, _, displayName: let displayName): do {
            hasher.combine(displayName)
            hasher.combine(4)
        }
        }
            
        return hasher.finalize()
    }
}
