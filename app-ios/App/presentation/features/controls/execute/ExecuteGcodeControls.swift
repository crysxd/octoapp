//
//  ExecuteGcodeControls.swift
//  OctoApp
//
//  Created by Christian Würthner on 22/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Flow
import Combine
import OctoAppBase


struct ExecuteGcodeControls: View {
    
    @StateObject var viewModel: ExecuteGcodeCotnrolsViewModel
    @State private var showConfirmation = false
    @State private var showConfirmationItem : ExecuteGcodeCotnrolsViewModel.ItemStruct? = nil
    @State private var showInputsItem : ExecuteGcodeCotnrolsViewModel.ItemStruct? = nil
    @State private var showCustomizationItem: ExecuteGcodeCotnrolsViewModel.ItemStruct? = nil
    @State private var showCustomizationMenu: Bool = false
    @Environment(\.openURL) private var openUrl
    @Environment(\.instanceId) private var instanceId
    
    var body: some View {
        if viewModel.visible {
            control
        }
    }
    
    var control: some View {
        GenericHistoryItemsControl(
            title: "widget_gcode_send"~,
            iconSystemName: "slider.horizontal.3",
            iconAction: { showCustomizationMenu = true },
            otherText: "open_terminal"~,
            options: viewModel.items,
            optionSelected: { item in await  doExecute(item: item.data) },
            optionLongPressed:  { item in showCustomizationItem = item.data },
            otherSelected: { openUrl(UriLibrary.shared.getTerminalUri()) }
        )
        .macOsCompatibleSheet(item: $showCustomizationItem) { item in
            switch item {
            case .history(id: _, command: let command, label: _, isFavorite: _): MenuHost(
                menu: CustomizeGcodeShortcutMenu(
                    gcode: command,
                    offerInsert: false
                )
            )
                
            case .macro(id: _, command: let command, label: _, isFavorite: _): MenuHost(
                menu: CustomizeMacroMenu(
                    instanceId: instanceId,
                    macroId: command
                )
            )
                
            case .other(id: _, command: _, label: _, isFavorite: _): Text("Can't customize other")
                
            }
        }
        .macOsCompatibleSheet(item: $showInputsItem) { item in
            switch item {
            case .history(id: _, command: _, label: _, isFavorite: _): Text("History can't have inputs")
            case .other(id: _, command: _, label: _, isFavorite: _): Text("Other can't have innputs")
            case .macro(id: _, command: let command, label: _, isFavorite: _): MenuHost(
                menu: ExecuteMacroMenu(
                    instanceId: instanceId,
                    macroId: command
                ),
                onResult: { result in
                    if let inputs = ExecuteMacroMenu.companion.unwrapResult(inputs: result) {
                        await doExecute(item: item, confirmed: true, inputs: inputs)
                    }
                }
            )
            }
        }
        .macOsCompatibleSheet(isPresented: $showCustomizationMenu) {
            MenuHost(
                menu: CustomizeMacroMenu(
                    instanceId: instanceId,
                    macroId: nil
                )
            )
        }
        .confirmationDialog(
            "",
            isPresented: $showConfirmation,
            presenting: showConfirmationItem,
            actions: { item in
                Button("cancel"~, role: .cancel) {}
                Button("widget_gcode_send___confirmation_action"~) {
                    Task {
                        _ = await doExecute(item: item, confirmed: true, inputs: [:])
                    }
                }
            },
            message: { item in
                Text(String(format:"widget_gcode_send___confirmation_message"~, item.command))
            }
        )
    }
    
    private func doExecute(item: ExecuteGcodeCotnrolsViewModel.ItemStruct, confirmed: Bool = false, inputs: [String:String] = [:]) async {
        do {
            switch(try await viewModel.execute(id: item.id, inputs: [:], confirmed: false)) {
            case .needsconfirmation: showConfirmationItem = item
            case .needsinputs: showInputsItem = item
            default: do {}
            }
        } catch {
            // Nothing, VM handled it
        }
    }
}

struct ExecuteGcodeControls_Previews: PreviewProvider {
    static var previews: some View {
        ExtrudeControls(viewModel: ExtrudeControlsViewModel())
    }
}
