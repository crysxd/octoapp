//
//  ExecuteGcodeControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

class ExecuteGcodeCotnrolsViewModel: BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: ExecuteGcodeControlsViewModelCore? = nil
    @Published var items: [GenericHistoryItemGroup<ItemStruct>] = []
    @Published var visible = false
    
    func createCore(instanceId: String) -> ExecuteGcodeControlsViewModelCore {
        return ExecuteGcodeControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: ExecuteGcodeControlsViewModelCore) {
        core.items.asPublisher()
            .sink { (state: ExecuteGcodeControlsViewModelCore.State) in
                self.items = state.groups.map { group in
                    GenericHistoryItemGroup(
                        label: group.label,
                        items: group.items.compactMap { item in
                            var raw: ItemStruct? = nil
                            if item.raw is ExecuteGcodeControlsViewModelCore.RawItemHistoryItem {
                                raw = ItemStruct.history(id: item.id, command: item.command, label: item.label, isFavorite: item.isFavorite)
                            } else if item.raw is ExecuteGcodeControlsViewModelCore.RawItemMacroItem {
                                raw = ItemStruct.macro(id: item.id, command: item.command, label: item.label, isFavorite: item.isFavorite)
                            } else {
                                raw = ItemStruct.other(id: item.id, command: item.command, label: item.label, isFavorite: item.isFavorite)
                            }
                            
                            return GenericHistoryItem(
                                title: raw!.label,
                                id: raw!.id,
                                pinned: raw!.isFavorite,
                                data: raw!
                            )
                        }
                    )
                }
            }
            .store(in: &bag)
        
    
        
        core.visible.asPublisher()
            .sink { (visibility: ExecuteGcodeControlsViewModelCore.Visibility) in
                self.visible = visibility.visible
            }
            .store(in: &bag)
    }
    

    func execute(id: String, inputs: [String: String], confirmed: Bool) async throws -> ExecuteGcodeControlsViewModelCore.Result {
        return try await currentCore?.executeGcodeCommand(id: id, inputs: inputs,confirmed: confirmed) ?? .failed
    }
    
    func clearData() {
        items = []
    }
}

extension ExecuteGcodeCotnrolsViewModel {
    enum ItemStruct : Equatable, Identifiable {
        var label: String {
            switch self {
            case .history(id: _, command: _, label: let label, isFavorite: _): label
            case .macro(id: _, command: _, label: let label, isFavorite: _): label
            case .other(id: _, command: _, label: let label, isFavorite: _): label
            }
        }
        var isFavorite: Bool {
            switch self {
            case .history(id: _, command: _, label: _, isFavorite: let isFavorite): isFavorite
            case .macro(id: _, command: _, label: _, isFavorite: let isFavorite): isFavorite
            case .other(id: _, command: _, label: _, isFavorite: let isFavorite): isFavorite
            }
        }
        var command: String {
            switch self {
            case .history(id: _, command: let command, label: _, isFavorite: _): command
            case .macro(id: _, command: let command, label: _, isFavorite: _): command
            case .other(id: _, command: let command, label: _, isFavorite: _): command
            }
        }
        var id: String {
            switch self {
            case .history(id: let id, command: _, label: _, isFavorite: _): id
            case .macro(id: let id, command: _, label: _, isFavorite: _): id
            case .other(id: let id, command: _, label: _, isFavorite: _): id
            }
        }
        
        case history(id: String, command: String, label: String, isFavorite: Bool)
        case macro(id: String, command: String, label: String, isFavorite: Bool)
        case other(id: String, command: String, label: String, isFavorite: Bool)
    }
}
