//
//  MoveControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct MoveControls: View {
    
    @StateObject var viewModel: MoveControlsViewModel
    
    var body: some View {
        if viewModel.state.visible {
            MoveInputs(
                state: viewModel.state,
                moveAxis: { axis, direction, distance in
                    viewModel.moveAxis(direction: direction, axis: axis, distanceMm: distance)
                },
                moveToPosition: {x, y, z in
                    try await viewModel.moveToPosition(x: x, y: y, z: z)
                },
                homeXYAxis: {
                    try await viewModel.homeXYAxis()
                },
                homeZAxis: {
                    try await viewModel.homeZAxis()
                },
                turnMotorsOff: {
                    try await viewModel.turnMotorsOff()
                }
            )
        }
    }
}

private struct MoveInputs: View {
    
    var state: MoveControlsViewModel.State
    var moveAxis: (MoveControlsViewModelCore.Axis, MoveControlsViewModelCore.Direction, Double) -> Void
    var moveToPosition: (Float, Float, Float) async throws -> Void
    var homeXYAxis: () async throws -> Void
    var homeZAxis: () async throws -> Void
    var turnMotorsOff: () async throws -> Void

    @Environment(\.instanceId) private var instanceId
    @State private var selectedDistance: Double = 10
    @State private var showSettings = false

    var body: some View {
        ControlsScaffold(
            title: "widget_move"~,
            iconSystemName: "slider.horizontal.3",
            iconSystemName2: !state.limitedControls && state.steppersEnabled != false ? "powerplug.fill" : nil,
            iconAction: { showSettings = true },
            iconAction2: { try? await turnMotorsOff() }
        ) {
            VStack(spacing: OctoTheme.dimens.margin12) {
                MoveInputsLayout {
                    HorizontalInputs(
                        limitedControls: state.limitedControls,
                        homed: state.xyHomed,
                        moveAxis: { axis, direction in
                            moveAxis(axis, direction, selectedDistance)
                        },
                        homeAxis: homeXYAxis
                    )
                    
                    VerticalInputs(
                        limitedControls: state.limitedControls,
                        homed: state.zHomed,
                        moveAxis: { axis, direction in
                            moveAxis(axis, direction, selectedDistance)
                        },
                        homeAxis: homeZAxis
                    )
                    
                    MoveDistance(
                        selectedDistance: $selectedDistance
                    )
                }
                
                if let position = state.position {
                    PositionDisplay(
                        position: position,
                        moveToPosition: state.canMoveToPosition ? {
                            x, y, z in try await moveToPosition(x, y, z)
                        } : nil
                    )
                }
            }
        }
        .macOsCompatibleSheet(isPresented: $showSettings) {
            MenuHost(menu: MoveControlsSettingsMenu(instanceId: instanceId))
        }
    }
}

private struct PositionDisplay : View {
    
    var position: MoveControlsViewModel.Position
    var moveToPosition: ((Float, Float, Float) async throws -> Void)?
    
    @State private var showInputs = false
    @State private var noValueOpacity: CGFloat = 1
    
    var body: some View {
        DataButton(
            iconSystemName: moveToPosition != nil ? "pencil" : nil,
            clickListener: { showInputs = true }
        ) {
            HStack(spacing: OctoTheme.dimens.margin12) {
                PositionComponent(label: "X", value: position.x, opacity: position.x == nil ? noValueOpacity : 1)
                PositionComponent(label: "Y", value: position.y, opacity: position.y == nil ? noValueOpacity : 1)
                PositionComponent(label: "Z", value: position.z, opacity: position.z == nil ? noValueOpacity : 1)
                Spacer(minLength: 0)
            }
            
            .animation(.default, value: position)
            .animation(.easeInOut(duration: 1), value: noValueOpacity)
            .padding(OctoTheme.dimens.margin12)
        }
        .macOsCompatibleSheet(isPresented: $showInputs) {
            MenuValueInput(
                title: "move_controls___move_to_position"~,
                value: position.x?.format(maxDecimals: 2) ?? "",
                value2: position.y?.format(maxDecimals: 2) ?? "",
                value3: position.z?.format(maxDecimals: 2) ?? "",
                hint: "X",
                hint2: "Y",
                hint3: "Z",
                confirmButton: "move"~,
                keyboardType: .numbers,
                keyboardType2: .numbers,
                keyboardType3: .numbers,
                validator: { $0.parse() == nil ? "error_please_enter_a_value"~ : nil },
                validator2: { $0.parse() == nil ? "error_please_enter_a_value"~ : nil },
                validator3: { $0.parse() == nil ? "error_please_enter_a_value"~ : nil },
                action: { x, y, z in
                    if let x = x.parse(), let y = y.parse(), let z = z.parse() {
                        try await moveToPosition?(x, y, z)
                    }
                }
            )
        }
        .task(id: position.x == nil || position.y == nil || position.z == nil) {
            while !Task.isCancelled, position.x == nil || position.y == nil || position.z == nil {
                noValueOpacity = 0.4
                try? await Task.sleep(for: .seconds(1))
                noValueOpacity = 1
                try? await Task.sleep(for: .seconds(1))
            }
        }
    }
}
                
private extension String {
    func parse() -> Float? {
        Float(replacing(",", with: "."))
    }
}

private struct PositionComponent: View {
    var label: String
    var value: Float?
    var opacity: CGFloat
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin0) {
            Text(label)
                .foregroundColor(OctoTheme.colors.darkText)
                .typographyBase()
            
            Text(value?.format(minDecimals: 2, maxDecimals: 2) ?? "???")
                .typographyLabel()
                .opacity(opacity)
        }
    }
}

private struct HorizontalInputs: View {
    
    var limitedControls: Bool
    var homed: Bool
    var moveAxis: (MoveControlsViewModelCore.Axis, MoveControlsViewModelCore.Direction) -> Void
    var homeAxis: () async throws -> Void
    
    var body: some View {
        ZStack {
            MoveAxis(label: "Y")
            MoveAxis(label: "X", horizontal: true)
            
            HStack {
                VStack {
                    Spacer()
                    OctoIconButton(
                        icon: "chevron.left",
                        enabled: homed,
                        clickListener: {
                            moveAxis(
                                MoveControlsViewModelCore.Axis.x,
                                MoveControlsViewModelCore.Direction.negative
                            )
                        }
                    )
                    Spacer()
                }
                VStack {
                    OctoIconButton(
                        icon: "chevron.up",
                        enabled: homed,
                        clickListener: {
                            moveAxis(
                                MoveControlsViewModelCore.Axis.y,
                                MoveControlsViewModelCore.Direction.positive
                            )
                        }
                    )
                    OctoAsyncIconButton(
                        icon: "house.fill",
                        enabled: !limitedControls,
                        clickListener: homeAxis
                    )
                    OctoIconButton(
                        icon: "chevron.down",
                        enabled: homed,
                        clickListener: {
                            moveAxis(
                                MoveControlsViewModelCore.Axis.y,
                                MoveControlsViewModelCore.Direction.negative
                            )
                        }
                    )
                }
                VStack {
                    Spacer()
                    OctoIconButton(
                        icon: "chevron.right",
                        enabled: homed,
                        clickListener: {
                            moveAxis(
                                MoveControlsViewModelCore.Axis.x,
                                MoveControlsViewModelCore.Direction.positive
                            )
                        }
                    )
                    Spacer()
                }
            }
            .padding(OctoTheme.dimens.margin1)
        }
        .frame(maxHeight: .infinity)
        .surface()
    }
}

private struct VerticalInputs: View {
    
    var limitedControls: Bool
    var homed: Bool
    var moveAxis: (MoveControlsViewModelCore.Axis, MoveControlsViewModelCore.Direction) -> Void
    var homeAxis: () async throws -> Void
    
    var body: some View {
        ZStack {
            MoveAxis(label: "Z")
            
            VStack {
                OctoIconButton(
                    icon: "chevron.up",
                    enabled: homed,
                    clickListener: {
                        moveAxis(
                            MoveControlsViewModelCore.Axis.z,
                            MoveControlsViewModelCore.Direction.positive
                        )
                    }
                )
                OctoAsyncIconButton(
                    icon: "house.fill",
                    enabled: !limitedControls,
                    clickListener: homeAxis
                )
                OctoIconButton(
                    icon: "chevron.down",
                    enabled: homed,
                    clickListener: {
                        moveAxis(
                            MoveControlsViewModelCore.Axis.z,
                            MoveControlsViewModelCore.Direction.negative
                        )
                    }
                )
            }
            .padding(OctoTheme.dimens.margin1)
        }
        .frame(maxHeight: .infinity)
        .surface()
    }
}

private struct MoveDistance: View {
    
    @Binding var selectedDistance: Double
    
    var body: some View {
        VStack(spacing:OctoTheme.dimens.margin01) {
            MoveDistanceOption(selectedDistnace: $selectedDistance, index: 5, distance: 100)
            MoveDistanceOption(selectedDistnace: $selectedDistance, index: 4, distance: 10)
            MoveDistanceOption(selectedDistnace: $selectedDistance, index: 3, distance: 1)
            MoveDistanceOption(selectedDistnace: $selectedDistance, index: 2, distance: 0.1)
            MoveDistanceOption(selectedDistnace: $selectedDistance, index: 1, distance: 0.025)
        }
        .padding([.top, .bottom], OctoTheme.dimens.margin1)
        .padding([.leading, .trailing], OctoTheme.dimens.margin01)
        .animation(.default, value: selectedDistance)
        .frame(maxHeight: .infinity)
        .surface()
    }
}

private struct MoveDistanceOption: View {
    
    @Binding var selectedDistnace: Double
    var index: Int
    var distance: Double
    
    private let baseSize: CGFloat = 5
    private var circleSize: CGFloat { baseSize * Double(index) }
    private var selected: Bool { selectedDistnace == distance }
    
    var body: some View {
        Button(
            action: { selectedDistnace = distance },
            label: {
                ZStack {
                    Text(distance.formatAsLength(maxDecimals: 4))
                        .typographyBase()
                        .opacity(selected ? 1 : 0)
                    
                    OctoTheme.colors.accent
                        .clipShape(Circle())
                        .frame(width: circleSize, height: circleSize)
                        .opacity(selected ? 0 : 1)
                }
            }
        )
        .frame(maxHeight: .infinity)
    }
}

private struct MoveAxis: View {
    
    var label: String
    var horizontal: Bool = false
    
    var body: some View {
        VStack(spacing: 0) {
            OctoTheme.colors.lightText.frame(width: 1, height: 3)
            
            Text(label)
                .foregroundColor(OctoTheme.colors.lightText)
                .font(.system(size: 9))
                .rotationEffect(Angle(degrees: horizontal ?  90: 0))
            
            OctoTheme.colors.lightText.frame(width: 1, height: 3)
            
            Spacer()
            
            OctoTheme.colors.lightText.frame(width: 1, height: 12)
        }
        .rotationEffect(Angle(degrees: horizontal ?  -90: 0))
    }
}

private struct MoveInputsLayout: Layout {
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        return CGSize(
            width: proposal.replacingUnspecifiedDimensions().width,
            height: subviews[0].sizeThatFits(proposal).width
        )
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        let height = bounds.height
        let horizontalWidth = height
        let verticalWidth = subviews[1].sizeThatFits(proposal).width
        let stepWidth = subviews[2].sizeThatFits(proposal).width
        let totalWidth = horizontalWidth + verticalWidth + stepWidth
        let spacing = (bounds.width - totalWidth) / 2
        
        // Horizontal
        subviews[0].place(
            at: p,
            proposal: .init(width: horizontalWidth, height: height)
        )
        
        // Vertical
        p.x += horizontalWidth + spacing
        subviews[1].place(
            at: p,
            proposal: .init(width: verticalWidth, height: height)
        )
        
        // Steps
        p.x += verticalWidth + spacing
        subviews[2].place(
            at: p,
            proposal: .init(width: stepWidth, height: height)
        )
    }
}

struct MoveControls_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            MoveInputs(
                state: MoveControlsViewModel.State(
                    visible: true,
                    limitedControls: true,
                    xyHomed: true,
                    zHomed: true,
                    canMoveToPosition: true,
                    steppersEnabled: true
                ),
                moveAxis: { _, _, _ in },
                moveToPosition: { _, _ , _ in },
                homeXYAxis: {},
                homeZAxis: {},
                turnMotorsOff: {}
            )
            
            MoveInputs(
                state: MoveControlsViewModel.State(
                    visible: true,
                    limitedControls: false,
                    position: MoveControlsViewModel.Position(x: 13.37, y:42.01, z: 12.32),
                    xyHomed: true,
                    zHomed: true,
                    canMoveToPosition: true,
                    steppersEnabled: true
                ),
                moveAxis: { _, _, _ in },
                moveToPosition: { _, _ , _ in },
                homeXYAxis: {},
                homeZAxis: {},
                turnMotorsOff: {}
            )
            Text("Below")
        }
    }
}
