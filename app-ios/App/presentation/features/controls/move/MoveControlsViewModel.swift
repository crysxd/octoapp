//
//  MoveControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

class MoveControlsViewModel : BaseViewModel {
    private static let initialState = State(
        visible: false,
        limitedControls: false,
        position: nil,
        xyHomed: true,
        zHomed: true,
        canMoveToPosition: false,
        steppersEnabled: nil
    )
    
    var currentCore: MoveControlsViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: MoveControlsViewModel.State = initialState
    
    func clearData() {
        state = Self.initialState
    }
    
    func publish(core: MoveControlsViewModelCore) {
        state = State(state: core.getState())
        
        core.state.asPublisher()
            .sink { (event: MoveControlsViewModelCore.State) in
                self.state = State(state: core.getState())
            }
            .store(in: &bag)
    }
    
    func createCore(instanceId: String) -> MoveControlsViewModelCore {
        MoveControlsViewModelCore(instanceId: instanceId)
    }
    
    func moveAxis(
        direction: MoveControlsViewModelCore.Direction,
        axis: MoveControlsViewModelCore.Axis,
        distanceMm: Double
    ) {
        currentCore?.moveAxis(
            axis: axis,
            direction: direction,
            distanceMm: distanceMm
        )
    }
    
    func moveToPosition(
        x: Float,
        y: Float,
        z: Float
    ) async throws{
        try await currentCore?.moveToPosition(
            x: x,
            y: y,
            z: z
        )
    }
    
    func homeXYAxis() async throws {
        try await currentCore?.homeXYAxis()
    }
    
    func homeZAxis() async throws {
        try await currentCore?.homeZAxis()
    }
    
    func turnMotorsOff() async throws {
        try await currentCore?.turnMotorsOff()
    }
}

extension MoveControlsViewModel {
    struct State : Equatable {
        var visible: Bool
        var limitedControls: Bool
        var position: Position?
        var xyHomed: Bool
        var zHomed: Bool
        var canMoveToPosition: Bool
        var steppersEnabled: Bool?
    }
    
    struct Position : Equatable{
        var x: Float?
        var y: Float?
        var z: Float?
    }
}

extension MoveControlsViewModel.State {
    init(state: MoveControlsViewModelCore.State) {
        self.init(
            visible: state.visible,
            limitedControls: state.limitedControls,
            position: state.position.flatMap {
                MoveControlsViewModel.Position(
                    x: $0.x?.floatValue,
                    y: $0.y?.floatValue,
                    z: $0.z?.floatValue
                )
            },
            xyHomed: state.xyHomed,
            zHomed: state.zHomed,
            canMoveToPosition: state.canMoveToPosition,
            steppersEnabled: state.steppersEnabled?.boolValue
        )
    }
}
