//
//  ControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 29/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Combine
import OctoAppBase

class ControlsViewModel : BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: ControlsViewModelCore? = nil
    @Published var state: State = .initial
    private let tag = "ControlsViewModel"
    
    private var pendingState: State? = nil
    private var frozenUntil = Date(timeIntervalSince1970: 0)
    private var frozenState: State? = nil
    
    func createCore(instanceId: String) -> ControlsViewModelCore {
        return  ControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: ControlsViewModelCore) {
        state = frozenState ?? .initial
        core.state.asPublisher()
            .sink { (result: ControlsViewModelCore.State) in
                let controls = result.controls.compactMap { Control(control: $0) }
                var s: State = State.connect(controls: controls)
                
                if result is ControlsViewModelCore.StateConnect {
                    s = .connect(controls: controls)
                } else if result is ControlsViewModelCore.StatePrepare {
                    s = .prepare(controls: controls)
                } else if let state = result as? ControlsViewModelCore.StatePrint {
                    s = .print(
                        controls: controls,
                        info: PrintInformation(
                            progress: state.progress,
                            pausing: state.pausing,
                            paused: state.paused,
                            cancelling: state.cancelling
                        )
                    )
                }
                
                if self.frozenState != nil && self.frozenUntil > Date() {
                    self.pendingState = s
                } else {
                    self.state = s
                }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = .initial
    }
  
    func togglePausePrint() async throws {
        try await currentCore?.togglePause()
    }
    
    func cancelPrint() async throws {
        try await currentCore?.cancelPrint()
    }
    
    func emergencyStop() async throws {
        try await currentCore?.emergencyStop()
    }
    
    
    func updateOrder(order: [ControlType]) {
        currentCore?.updateOrder(
            controls: order.map { $0.toType() }
        )
    }
    
    func toggleVisible(type: ControlType) {
        currentCore?.toggleVisible(type: type.toType())
    }
    
    func freezeStateTemporarily() async -> Duration {
        let seconds = 1
        let duration = Duration.seconds(seconds)
        let components = DateComponents(second: seconds)
        
        await MainActor.run {
            // We can't double freeze
            if frozenState != nil {
                Napier.i(tag: tag, message: "Skipping freeze, already frozen until \(frozenUntil)")
                return
            }
            
            frozenState = state
            pendingState = nil
            frozenUntil = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
            
            Napier.i(tag: tag, message: "Freezing state: \(state) until \(frozenUntil)")
            
            Task {
               try? await Task.sleep(for: duration)
               await MainActor.run {
                   Napier.i(tag: tag, message: "Unfreezing state: \(state)")
                   if let ps = pendingState {
                       state = ps
                       frozenState = nil
                       frozenUntil = Date()
                   }
               }
           }
        }
        
        return duration
    }
}

extension ControlsViewModel {
    enum State: Equatable {
        case initial, connect(controls: [Control]), prepare(controls: [Control]), print(controls: [Control], info: ControlsViewModel.PrintInformation)
    }

    enum ControlType: String {
        case progress = "progress"
        case temperature = "temperature"
        case webcam = "webcam"
        case move = "move"
        case extrude = "extrude"
        case sendGcode = "sendGcode"
        case gcodePreview = "gcodePreview"
        case tune = "tune"
        case quickAccess = "quickAccess"
        case quickPrint = "quickPrint"
        case connect = "connect"
        case multiTool = "multitool"
        case cancelObject = "cancelObject"
        case announcement = "announcement"
        case bedMesh = "bedmesh"
        case saveConfig = "saveConfig"
    }
    
    struct Control: Equatable {
        let type: ControlType
        let visible: Bool
    }
    
    struct PrintInformation: Equatable {
        let progress: Int32
        let pausing: Bool
        let paused: Bool
        let cancelling: Bool
    }

}

extension ControlsViewModel.Control {
    init?(control: ControlsViewModelCore.Control) {
        guard let type = ControlsViewModel.ControlType(type: control.type) else {
            return nil
        }
        
        self.init(
            type: type,
            visible: control.visible
        )
    }
}

extension ControlsViewModel.ControlType {
    init?(type: ControlType) {
        switch type {
        case .announcementwidget: self = .announcement
        case .cancelobject: self = .cancelObject
        case .controltemperaturewidget: self = .temperature
        case .extrudewidget: self = .extrude
        case .gcodepreviewwidget: self = .gcodePreview
        case .movetoolwidget: self = .move
        case .multitool: self = .multiTool
        case .progresswidget: self = .progress
        case .quickaccesswidget: self = .quickAccess
        case .quickprint: self = .quickPrint
        case .tunewidget: self = .tune
        case .webcamwidget: self = .webcam
        case .connect: self = .connect
        case .progresswidget: self = .progress
        case .sendgcodewidget: self = .sendGcode
        case .bedmesh: self = .bedMesh
        case .saveconfig: self = .saveConfig
        default: return nil
        }
    }
    
    func toType() -> ControlType {
        switch self {
        case .progress: return .progresswidget
        case .temperature: return .controltemperaturewidget
        case .webcam: return .webcamwidget
        case .move: return .movetoolwidget
        case .extrude: return .extrudewidget
        case .sendGcode: return .sendgcodewidget
        case .gcodePreview: return .gcodepreviewwidget
        case .tune: return .tunewidget
        case .quickAccess: return .quickaccesswidget
        case .quickPrint: return .quickprint
        case .connect: return .connect
        case .multiTool: return .multitool
        case .cancelObject: return .cancelobject
        case .bedMesh: return .bedmesh
        case .announcement: return .announcementwidget
        case .saveConfig: return .saveconfig
        }
    }
}

extension ControlsViewModel.ControlType: Identifiable {
    var id: String { rawValue }
}
