//
//  BedMeshControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 20/06/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

class BedMeshControlsViewModel : BaseViewModel {
    typealias Core = BedMeshControlsViewModelCore
    var bag: Set<AnyCancellable> = []
    var currentCore: BedMeshControlsViewModelCore? = nil
    @Published var state: BedMeshControlsViewModel.State = .hidden
    
    func createCore(instanceId: String) -> BedMeshControlsViewModelCore {
        BedMeshControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: BedMeshControlsViewModelCore) {
        core.state.asPublisher()
            .sink { (state: BedMeshControlsViewModelCore.State) in
                self.state = State(state: state)
            }
            .store(in: &bag)
    }
    
    func refreshMesh() async {
        try? await currentCore?.refresh()
    }
    
    func clearData() {
        state = .hidden
    }
}

extension BedMeshControlsViewModel {
    
    enum State {
        case hidden
        case notAvailable(printBed: GcodeNativeCanvasImageStruct, settings: GcodePreviewSettingsStruct, profile: PrinterProfileStruct)
        case available(printBed: GcodeNativeCanvasImageStruct, mesh: ParsedMesh, settings: GcodePreviewSettingsStruct, profile: PrinterProfileStruct, hasProfiles: Bool)
    }
    
    struct ParsedMesh : Equatable {
        let sectors: [MeshSector]
        let legend: [MeshLegendItem]
    }
    
    struct MeshSector : Equatable {
        let left: Float
        let top: Float
        let right: Float
        let bottom: Float
        let colorRed: Float
        let colorGreen: Float
        let colorBlue: Float
    }
    
    struct MeshLegendItem : Equatable, Identifiable {
        var id: Float { value }
        let value: Float
        let color: Color
    }
}

extension BedMeshControlsViewModel.State {
    init(state: BedMeshControlsViewModelCore.State) {
        switch state {
        case _ as BedMeshControlsViewModelCore.StateHidden: self = .hidden
        case let state as BedMeshControlsViewModelCore.StateAvailable: self = .available(
            printBed: state.printBed.toStruct(),
            mesh: BedMeshControlsViewModel.ParsedMesh(mesh: state.mesh),
            settings:  GcodePreviewSettingsStruct(gcodePreviewSettings: state.settings),
            profile: PrinterProfileStruct(printerProfile: state.printerProfile),
            hasProfiles: state.hasProfiles
        )
        case _ as BedMeshControlsViewModelCore.StateNotAvailable: self = .notAvailable(
            printBed: state.printBed.toStruct(),
            settings:  GcodePreviewSettingsStruct(gcodePreviewSettings: state.settings),
            profile: PrinterProfileStruct(printerProfile: state.printerProfile)
        )
        default: self = .hidden
        }
    }
}

extension BedMeshControlsViewModel.ParsedMesh {
    init(mesh: ParsedBedMesh) {
        self.init(
            sectors: mesh.sectors.map { BedMeshControlsViewModel.MeshSector(sector: $0) },
            legend: mesh.legend.map { BedMeshControlsViewModel.MeshLegendItem(item: $0) }
        )
    }
    
    func toClass() -> ParsedBedMesh {
       ParsedBedMesh(
        sectors: sectors.map {
            ParsedBedMesh.Sector(
                left: $0.left,
                top: $0.top,
                right: $0.right,
                bottom: $0.bottom,
                color: HexColor(
                    red: $0.colorRed,
                    green: $0.colorGreen,
                    blue: $0.colorBlue,
                    rawValue: nil
                )
            )
        },
        legend: []
       )
    }
}

extension BedMeshControlsViewModel.MeshSector {
    init(sector: ParsedBedMesh.Sector) {
        self.init(
            left: sector.left,
            top: sector.top,
            right: sector.right,
            bottom: sector.bottom,
            colorRed: sector.color.red,
            colorGreen: sector.color.green,
            colorBlue: sector.color.blue
        )
    }
}

extension BedMeshControlsViewModel.MeshLegendItem {
    
    init(item: KotlinPair<KotlinFloat, HexColor>) {
        self.init(
            value: item.first!.floatValue,
            color: Color(
                red: Double(item.second!.red),
                green: Double(item.second!.green),
                blue: Double(item.second!.blue)
            )
        )
    }
}
