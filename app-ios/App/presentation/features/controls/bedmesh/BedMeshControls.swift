//
//  BedMesgControls.swift
//  OctoApp
//
//  Created by Christian on 20/06/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct BedMeshControls: View {
    
    @StateObject var viewModel: BedMeshControlsViewModel
    
    var body: some View {
        switch viewModel.state {
        case .hidden: do {}
        case .available(
            printBed: let printBed,
            mesh: let mesh,
            settings: let settings,
            profile: let profile,
            hasProfiles: let hasProfiles
        ): BedMeshControlsContent(
            printBed: printBed,
            settings: settings,
            printerProfile: profile,
            hasProfiles: hasProfiles,
            mesh: mesh,
            refreshMesh: { await viewModel.refreshMesh() }
        )
        case .notAvailable(
            printBed: let printBed,
            settings: let settings,
            profile: let profile
        ): BedMeshControlsContent(
            printBed: printBed,
            settings: settings,
            printerProfile: profile,
            hasProfiles: false,
            mesh: nil,
            refreshMesh: { await viewModel.refreshMesh() }
        )
        }
    }
}

private struct BedMeshControlsContent : View {
    
    var printBed: GcodeNativeCanvasImageStruct
    var settings: GcodePreviewSettingsStruct
    var printerProfile: PrinterProfileStruct?
    var hasProfiles: Bool
    var mesh: BedMeshControlsViewModel.ParsedMesh?
    var refreshMesh: () async -> Void
    
    private let mediator = LowOverheadMediator(initialData: GcodeRenderContext.companion.Empty)
    private let plugin = BedMeshRenderPlugin()
    @State private var actionLoading = false
    @State private var showRefreshConfirmation = false
    @State private var showMenu = false
    @Environment(\.instanceId) var instanceId
    @Environment(\.compactLayout) var compactLayout
    
    var body: some View {
        ControlsScaffold(
            title: "widget_bed_mesh"~,
            iconSystemName: hasProfiles ? "swatchpalette.fill" : nil,
            iconSystemName2: "arrow.clockwise",
            iconAction: { showMenu = true},
            iconAction2: { showRefreshConfirmation = true },
            actionLoading: false,
            actionLoading2: actionLoading
        ) {
            PrintBedLayout(
                printBed: printBed,
                printerProfile: printerProfile,
                settings: settings,
                paused: false,
                exceedsPrintArea: false,
                unsupportedGcode: false,
                isTrackingPrintProgress: false,
                renderContext: mediator,
                plugin: {
                    if let mesh = mesh {
                        plugin.render(canvas: $0, mesh: mesh.toClass())
                    }
                },
                overlay: { actionButtons },
                legend: { legend }
            )
            .surface()
            .if(showMenu) { $0 }
        }
        .confirmationDialog(
            "",
            isPresented: $showRefreshConfirmation,
            actions: {
                Button("widget_bed_mesh___confirmation_action"~) {
                    Task {
                        actionLoading = true
                        await refreshMesh()
                        actionLoading = false
                    }
                }
            },
            message: {
                Text(String(format:"widget_bed_mesh___confirmation_message"~))
            }
        )
        .sheet(isPresented: $showMenu) {
            MenuHost(menu: BedMeshProfilesMenu(instanceId: instanceId))
        }
    }
    
    @ViewBuilder
    var actionButtons: some View {
        HStack(alignment: .bottom, spacing: -OctoTheme.dimens.margin01) {
//            if compactLayout {
                OctoAsyncIconButton(
                    icon: "arrow.clockwise",
                    clickListener: { showRefreshConfirmation = true }
                )
                .padding(OctoTheme.dimens.margin01)
                .frame(alignment: .bottomTrailing)
                
//                if hasProfiles {
                    OctoAsyncIconButton(
                        icon: "swatchpalette.fill",
                        clickListener: { showMenu = true }
                    )
                    .padding(OctoTheme.dimens.margin01)
                    .frame(alignment: .bottomTrailing)
//                }
//            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottomTrailing)
    }
    
    @ViewBuilder
    var legend: some View {
        if let legend = mesh?.legend {
            VStack {
                ForEach(legend) { item in
                    HStack {
                        Circle()
                            .foregroundColor(item.color)
                            .frame(width: 12, height: 12)
                        
                        Text(item.value.formatAsLength(minDecimals: 3, maxDecimals: 3))
                            .typographyLabel()
                            .frame(maxWidth: .infinity, alignment: .leading)
                    }
                }
            }
            .fixedSize()
        } else {
            Text("no_data_available"~)
                .typographySubtitle()
        }
    }
}

struct BedMesgControls_Previews: PreviewProvider {
    static var previews: some View {
        BedMeshControlsContent(
            printBed: .printbedartillery,
            settings: GcodePreviewSettingsStruct(
                showPreviousLayer: false,
                showCurrentLayer: false,
                layerOffset: 1,
                quality: .medium
            ),
            printerProfile: nil,
            hasProfiles: false,
            mesh: nil,
            refreshMesh: {}
        )
        .previewDisplayName("Not available")
        
        BedMeshControlsContent(
            printBed: .printbedartillery,
            settings: GcodePreviewSettingsStruct(
                showPreviousLayer: false,
                showCurrentLayer: false,
                layerOffset: 1,
                quality: .medium
            ),
            hasProfiles: true,
            mesh: BedMeshControlsViewModel.ParsedMesh(
                sectors: [],
                legend: [
                    BedMeshControlsViewModel.MeshLegendItem(
                        value: -0.022,
                        color: Color.red
                    ),
                    BedMeshControlsViewModel.MeshLegendItem(
                        value: 0,
                        color: Color.gray
                    ),
                    BedMeshControlsViewModel.MeshLegendItem(
                        value: +0.053,
                        color: Color.blue
                    )
                ]
            ),
            refreshMesh: {}
        )
        .previewDisplayName("Available")
    }
}
