//
//  ExtrudeControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

class ExtrudeControlsViewModel: BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: ExtrudeControlsViewModelCore? = nil
    @Published var items: [GenericHistoryItem<Int32>] = []
    @Published var visible = false

    func createCore(instanceId: String) -> ExtrudeControlsViewModelCore {
        return ExtrudeControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: ExtrudeControlsViewModelCore) {
        core.items.asPublisher()
            .sink { (state: ExtrudeControlsViewModelCore.State) in
                self.items = state.items.map { item in
                    GenericHistoryItem(
                        title: item.distanceMm.formatAsLength(),
                        id: item.distanceMm.description,
                        pinned: item.isFavorite,
                        data: item.distanceMm
                    )
                }
            }
            .store(in: &bag)
        
        core.visible.asPublisher()
            .sink { (visibility: ExtrudeControlsViewModelCore.Visibility) in
                self.visible = visibility.visible
            }
            .store(in: &bag)
    }
    
    func togglePinned(distanceMm: Int32) {
        currentCore?.toggleFavourite(distanceMm: distanceMm)
    }
    
    func extrude(distanceMm: Int32, confirmed: Bool) async throws -> ExtrudeControlsViewModelCore.Result {
        return try await currentCore?.extrude(distanceMm: distanceMm, confirmed: confirmed) ?? .failed
    }
    
    func clearData() {
        items = []
    }
}

