//
//  ConnectActionMediator.swift
//  OctoApp
//
//  Created by Christian on 01/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

private struct ConnectActionMediatorModifier: ViewModifier {
    
    @StateObject private var viewModel = ConnectActionMediator()
    
    func body(content: Content) -> some View {
        content.environmentObject(viewModel)
    }
}

extension View {
    func connectActionMediator() -> some View {
        modifier(ConnectActionMediatorModifier())
    }
}

class ConnectActionMediator : ObservableObject {
    @Published var connectAction: ConnectAction? = nil
    private(set) var performConnectAction: (ConnectionActionType) async -> Void = { _ in }
    
    func setAction(connectAction: ConnectAction?, callback: @escaping (ConnectionActionType) async -> Void){
        self.connectAction = connectAction
        self.performConnectAction = callback
    }
}

struct ConnectAction: Identifiable, Equatable {
    let id: ConnectionActionType
    let title: String
    let type: OctoButtonType
}
