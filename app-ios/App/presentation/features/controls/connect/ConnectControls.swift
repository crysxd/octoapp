//
//  ConnectControls.swift
//  OctoApp
//
//  Created by Christian on 01/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct ConnectControls: View {
    
    @StateObject var viewModel: ConnectionControlsViewModel
    
    @EnvironmentObject private var mediator: ConnectActionMediator
    @Environment(\.instanceId) private var instanceId
    
    @State private var showAutoConnectInfo = false
    @State private var showManualConnectConfirmation = false
    @State private var showPowerControls = false
    
    var body: some View {
        StateView(
            state: viewModel.state
        )
        .onChange(of: viewModel.state) { _, s in
            var action: ConnectAction? = nil
            if let actionType = s.display.actionType,
               let title = s.display.action {
                action = ConnectAction(id: actionType, title: title, type: s.display.primaryAction ? .primary : .secondary)
            }
            
            mediator.setAction(connectAction: action) { id in
                switch id {
                case .autoConnectTutorial: showAutoConnectInfo = true
                case .beginConnect: showManualConnectConfirmation = true
                case .configurePsu: showPowerControls = true
                default: await viewModel.performAction(id)
                }
            }
        }
        .onDisappear {
            mediator.setAction(connectAction: nil) { _ in }
        }
        .confirmationDialog(
            "connect_printer___begin_connection_confirmation_message"~,
            isPresented: $showManualConnectConfirmation,
            actions: {
                Button("connect_printer___begin_connection_cofirmation_positive"~, role: .destructive) {
                    viewModel.performActionAsync(.beginConnect)
                }
            },
            message: {
                Text("connect_printer___begin_connection_confirmation_message"~)
            }
        )
        .macOsCompatibleSheet(isPresented: $showAutoConnectInfo) {
            MenuHost(menu: AutoConnectPrinterInfoMenu()) { _ in
                Napier.i(tag: "ConnectControls", message: "AutoConnectPrinterInfoMenu dismissed, connecting")
                viewModel.performActionAsync(.beginConnect)
                showAutoConnectInfo = false
            }
        }
        .macOsCompatibleSheet(isPresented: $showPowerControls) {
            MenuHost(menu: SelectPsuMenu(instanceId: instanceId)) { device in
                Napier.i(tag: "ConnectControls", message: "SelectPsuMenu dismissed, selected \(device). Will turn on now...")
                showPowerControls = false
                await viewModel.performAction(.turnPsuOn)
            }
        }
    }
}

private struct StateView : View {
    
    var state: ConnectionControlsState
    
    @Environment(\.openURL) private var openUrl
    @Environment(\.instanceId) private var instanceId
    
    var body: some View {
        ControlsScaffold(title: nil, supportsEdit: false) {
            VStack(spacing: OctoTheme.dimens.margin01) {
                ZStack(alignment: .center) {
                    Image("Octopus background")
                        .resizable()
                        .aspectRatio(16/9, contentMode: .fit)
                        .padding(-OctoTheme.dimens.margin4)

                    OctoAvatar(action: state.display.avatarAction)
                }
                .ifPad { $0.frame(maxHeight: 300) }
                .padding(.bottom, -OctoTheme.dimens.margin1)


                ZStack(alignment: .top) {
                    // Real content
                    TitleDetail(title: state.display.title, detail: state.display.subtitle)
                    
                    // Placeholder content to ensure height is constant
                    TitleDetail(title: nil, detail: nil).opacity(0)
                }
                
                Spacer().frame(height: OctoTheme.dimens.margin2)
                
                if let exception = state.display.exception {
                    TroubleshootingView(exception: exception)
                        .transition(.move(edge: .bottom).combined(with: .opacity))
                        .frame(maxWidth: OctoTheme.dimens.maxButtonWidth)
                }
                
                
                if state.advertise {
                    RemoteAdvertisementView(state: state)
                        .transition(.move(edge: .bottom).combined(with: .opacity))
                        .frame(maxWidth: OctoTheme.dimens.maxButtonWidth)
                }
                
            }
            .multilineTextAlignment(.center)
            .frame(maxWidth: .infinity)
            .fixedSize(horizontal: false, vertical: true)
            .ifPad { $0.padding(.bottom, OctoTheme.dimens.margin2)}
        }
    }
}

private struct RemoteAdvertisementView : View {
    
    @Environment(\.openURL) private var openUrl
    var state: ConnectionControlsState
    
    var body: some View {
        InformationalButton(
            iconName: state.serviceIcon,
            systemIconName: "cloud.fill",
            text: "connect_printer___action_configure_remote_access"~,
            textColor: state.textColor,
            accentColor: state.accentColor,
            backgroundColor: state.backgroundColor,
            action: { openUrl(UriLibrary.shared.getConfigureRemoteAccessUri()) }
        )
    }
}

private struct TroubleshootingView : View {
    
    var exception: KotlinThrowable
    @Environment(\.instanceId) private var instanceId
    @Environment(\.openURL) private var openUrl
    @EnvironmentObject private var orchestartorViewmodel: OrchestratorViewModel
    
    var body: some View {
        InformationalButton(
            systemIconName: "wifi.exclamationmark",
            text: "connect_printer___action_learn_more"~,
            action: { orchestartorViewmodel.postError(error: exception, showDetailsDirectly: true) }
        )
        InformationalButton(
            systemIconName: "wand.and.stars",
            text: "connect_printer___action_troubleshooting"~,
            action: { openUrl(UriLibrary.shared.getFixOctoPrintConnectionUri(baseUrl: UrlExtKt.toUrl(""), instanceId: instanceId)) }
        )
    }
}

private struct InformationalButton : View {
    
    var iconName: String? = nil
    var systemIconName: String? = nil
    var text: String
    var textColor = OctoTheme.colors.darkText
    var accentColor = OctoTheme.colors.accent
    var backgroundColor = OctoTheme.colors.inputBackground
    var action: () -> Void
    
    var body: some View {
        Button(
            action: action,
            label: {
                HStack(spacing: OctoTheme.dimens.margin12) {
                    if let i = iconName {
                        Image(i)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 28, height: 28)
                    } else if let i = systemIconName {
                        Image(systemName: i)
                            .frame(width: 32, height: 32)
                            .foregroundColor(accentColor)
                            .background(Circle().fill(accentColor.opacity(0.1)))
                    }
                    
                    Text(text)
                        .foregroundColor(textColor)
                        .typographyBase()
                        .multilineTextAlignment(.leading)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    
                    Image(systemName: "chevron.right")
                        .foregroundColor(accentColor)
                        .imageScale(.large)
                }
                .padding([.leading, .trailing],OctoTheme.dimens.margin2)
                .padding([.top, .bottom], OctoTheme.dimens.margin12)
                .surface(color: .special(backgroundColor))
                .accentColor(accentColor)
            }
        )
    }
}

private struct TitleDetail : View {
    var title: String?
    var detail: String?
    
    var body: some View {
        VStack(spacing: 0) {
            Text(title ?? " \n ")
                .typographyTitle()
                .lineLimit(2)
                .padding(.bottom, OctoTheme.dimens.margin01)
            
            Text(detail ?? "  ")
                .typographyLabel()
        }
    }
}

//struct ConnectControls_Previews: PreviewProvider {
//    static var previews: some View {
//         StatefulPreviewWrapper(
//             ConnectionControlsState(display: ConnectionControlsDisplay(uiState: ConnectPrinterUseCase.UiStateCompanion.shared.Initializing))
//         ) { state in
//             StateView(
//                 state: state.wrappedValue
//             ).task {
//                 try? await Task.sleep(for: .milliseconds(1000))
//                 withAnimation {
//                     state.wrappedValue = ConnectionControlsState(
//                         display: ConnectionControlsDisplay(
//                             uiState: ConnectPrinterUseCase.UiState(
//                                 title: "Connecting...",
//                                 detail: nil,
//                                 avatarState: .swim,
//                                 action: nil,
//                                 actionType: nil,
//                                 primaryAction: false,
//                                 exception: nil,
//                                 isNotAvailable: true
//                             )
//                         )
//                     )
//                 }
//                 try? await Task.sleep(for: .milliseconds(2000))
//                 withAnimation {
//                     state.wrappedValue = ConnectionControlsState(
//                         display: ConnectionControlsDisplay(
//                             uiState:  ConnectPrinterUseCase.UiState(
//                                 title: "Connected",
//                                 detail: nil,
//                                 avatarState: .party,
//                                 action: nil,
//                                 actionType: nil,
//                                 primaryAction: false,
//                                 exception: nil,
//                                 isNotAvailable: true
//                             )
//                         )
//                     )
//                 }
//             }
//         }
//         .previewDisplayName("Animated")
//
//         StatefulPreviewWrapper(
//             ConnectionControlsState(display: ConnectionControlsDisplay(uiState: ConnectPrinterUseCase.UiStateCompanion.shared.Initializing))
//         ) { state in
//             StateView(
//                 state: state.wrappedValue
//             ).task {
//                 try? await Task.sleep(for: .milliseconds(2000))
//                 withAnimation {
//                     state.wrappedValue = ConnectionControlsState(
//                         display: ConnectionControlsDisplay(
//                             uiState: ConnectPrinterUseCase.UiState(
//                                 title: "Not available",
//                                 detail: nil,
//                                 avatarState: .idle,
//                                 action: nil,
//                                 actionType: nil,
//                                 primaryAction: false,
//                                 exception: nil,
//                                 isNotAvailable: true
//                             )
//                         ),
//                         advertise: true,
//                         service: OctoPlugins.shared.Obico
//                     )
//                 }
//             }
//         }
//         .previewDisplayName("Not available / Remote")
//         .background(OctoTheme.colors.windowBackground)
//
//         StateView(
//             state: ConnectionControlsState(
//                 display: ConnectionControlsDisplay(
//                     uiState: ConnectPrinterUseCase.UiStateCompanion.shared.Initializing
//                 )
//             )
//         )
//         .previewDisplayName("Initializing")
//
//        StateView(
//            state: ConnectionControlsState(
//                display: ConnectionControlsDisplay(
//                    uiState: ConnectPrinterUseCase.UiStatePrinterOffline(psuSupported: true)
//                )
//            )
//        )
//        .previewDisplayName("Offline / PSU")
//
//        StateView(
//            state: ConnectionControlsState(
//                display: ConnectionControlsDisplay(
//                    uiState: ConnectPrinterUseCase.UiStatePrinterOffline(psuSupported: false)
//                )
//            )
//        )
//        .previewDisplayName("Offline")
//
//        StateView(
//            state: ConnectionControlsState(
//                display: ConnectionControlsDisplay(
//                    uiState: ConnectPrinterUseCase.UiStatePrinterConnecting.shared
//                )
//            )
//        )
//        .previewDisplayName("Connecting")
//
//        StateView(
//            state: ConnectionControlsState(
//                display: ConnectionControlsDisplay(
//                    uiState: ConnectPrinterUseCase.UiStateOctoPrintStarting.shared
//                )
//            )
//        )
//        .previewDisplayName("Starting")
//
//        StateView(
//            state: ConnectionControlsState(
//                display: ConnectionControlsDisplay(
//                    uiState: ConnectPrinterUseCase.UiStateOctoPrintNotAvailable(
//                        exception: KotlinException(message: "sdfio"),
//                        url: UrlExtKt.toUrl("http://test.local")
//                    )
//                )
//            )
//        )
//        .previewDisplayName("Not available")
//
//        StateView(
//            state: ConnectionControlsState(
//                display: ConnectionControlsDisplay(
//                    uiState: ConnectPrinterUseCase.UiStateWaitingForUser(wasAutoConnectInfoShown: false)
//                )
//            )
//        )
//        .previewDisplayName("Waiting for user")
//
//        StateView(
//            state: ConnectionControlsState(
//                display: ConnectionControlsDisplay(
//                    uiState: ConnectPrinterUseCase.UiStatePrinterPsuCycling.shared
//                )
//            )
//        )
//        .previewDisplayName("Cycling")
//    }
//}
