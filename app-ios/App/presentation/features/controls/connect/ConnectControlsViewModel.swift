//
//  ConnectControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Foundation
import OctoAppBase
import Combine

private let initializing = ConnectionControlsState(
    display: ConnectionControlsDisplay(
        uiState: ConnectPrinterUseCase.UiStateCompanion.shared.Initializing
    )
)

class ConnectionControlsViewModel : BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: ConnectionControlsViewModelCore? = nil
    @Published var state: ConnectionControlsState = initializing

    func createCore(instanceId: String) -> ConnectionControlsViewModelCore {
        return ConnectionControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: ConnectionControlsViewModelCore) {
        core.state.asPublisher()
            .sink { (state : ConnectionControlsViewModelCore.State) in
                self.state = ConnectionControlsState(
                    display: ConnectionControlsDisplay(uiState: state.uiState),
                    advertise: state.advertise,
                    service: state.service
                )
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = initializing
    }
    
    func performActionAsync(_ action: ConnectionActionType) {
        Task {
            await performAction(action)
        }
    }
        
    func performAction(_ action: ConnectionActionType) async {
        Napier.i(tag: "ConnectionControlsViewModel", message: "Connect action: \(action)")
        try? await currentCore?.executeAction(actionType: action.toClass())
    }
}

struct ConnectionControlsState : Equatable {
    var display: ConnectionControlsDisplay
    var advertise: Bool = false
    var service: String? = nil
}

struct ConnectionControlsDisplay : Equatable {
    let title: String
    let subtitle: String?
    let avatarAction: OctoAvatarAction
    let action: String?
    let actionType: ConnectionActionType?
    let primaryAction: Bool
    let exception: KotlinThrowable?
}

enum ConnectionActionType: Equatable {
    case beginConnect, turnPsuOn, turnPsuOff, cyclePsu, retry, configurePsu, autoConnectTutorial
}

extension ConnectionControlsDisplay {
    init(uiState: ConnectPrinterUseCase.UiState) {
        self.init(
            title: uiState.title,
            subtitle: uiState.detail,
            avatarAction: OctoAvatarAction(action: uiState.avatarState),
            action: uiState.action,
            actionType: ConnectionActionType(action: uiState.actionType),
            primaryAction: uiState.primaryAction,
            exception: uiState.exception
        )
    }
}

private extension ConnectionActionType {
    
    init?(action: ConnectPrinterUseCase.ActionType?) {
        guard let action = action else { return nil }
        
        switch action {
        case .beginconnect: self = .beginConnect
        case .turnpsuon: self = .turnPsuOn
        case .turnpsuoff: self = .turnPsuOff
        case .cyclepsu: self = .cyclePsu
        case .retry: self = .retry
        case .configurepsu: self = .configurePsu
        case .autoconnecttutorial: self = .autoConnectTutorial
        default: return nil
        }
    }
    
    func toClass() -> ConnectPrinterUseCase.ActionType {
        switch self {
        case .beginConnect: return ConnectPrinterUseCase.ActionType.beginconnect
        case .turnPsuOn: return ConnectPrinterUseCase.ActionType.turnpsuon
        case .turnPsuOff: return ConnectPrinterUseCase.ActionType.turnpsuoff
        case .cyclePsu: return ConnectPrinterUseCase.ActionType.cyclepsu
        case .retry: return ConnectPrinterUseCase.ActionType.retry
        case .configurePsu: return ConnectPrinterUseCase.ActionType.configurepsu
        case .autoConnectTutorial: return ConnectPrinterUseCase.ActionType.autoconnecttutorial
        }
    }
}

private extension OctoAvatarAction {
    init(action: ConnectPrinterUseCase.AvatarState) {
        switch action {
        case .idle: self = .idle
        case .party: self = .party
        case .swim: self = .swim
        default: self = .idle
        }
    }
}

extension ConnectionControlsState {
    var backgroundColor: Color {
        switch service {
        case OctoPlugins.shared.OctoEverywhere: return OctoTheme.colors.externalOctoEverywhere
        case OctoPlugins.shared.Obico: return OctoTheme.colors.externalObico
        case OctoPlugins.shared.Ngrok: return OctoTheme.colors.externalNgrok
        default: return OctoTheme.colors.inputBackground
        }
    }
    
    var serviceIcon: String? {
        switch service {
        case OctoPlugins.shared.OctoEverywhere: return "octoEverywhereSmall"
        case OctoPlugins.shared.Obico: return "obicoSmall"
        case OctoPlugins.shared.Ngrok: return "ngrokSmall"
        default: return nil
        }
    }
    
    var textColor: Color {
        switch service {
        case OctoPlugins.shared.OctoEverywhere: return OctoTheme.colors.textColoredBackground
        case OctoPlugins.shared.Obico: return OctoTheme.colors.textColoredBackground
        case OctoPlugins.shared.Ngrok: return OctoTheme.colors.textColoredBackground
        default: return OctoTheme.colors.darkText
        }
    }
    
    var accentColor: Color {
        switch service {
        case OctoPlugins.shared.OctoEverywhere: return OctoTheme.colors.externalOctoEverywhere3
        case OctoPlugins.shared.Obico: return OctoTheme.colors.externalObico2
        case OctoPlugins.shared.Ngrok: return OctoTheme.colors.externalNgrok2
        default: return OctoTheme.colors.accent
        }
    }
}
