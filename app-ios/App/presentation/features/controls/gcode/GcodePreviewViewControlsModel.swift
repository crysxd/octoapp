//
//  GcodePreviewViewModelCore.swift
//  OctoApp
//
//  Created by Christian Würthner on 30/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

@MainActor
class GcodePreviewControlsViewModel: BaseViewModel {
    var currentCore: GcodePreviewControlsViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: State = .loading(progress: 0)
    @Published var hidden = SharedBaseInjector.shared.getOrNull()?.preferences.gcodeHiddenIos == true
    
    func createCore(instanceId: String) -> GcodePreviewControlsViewModelCore {
        GcodePreviewControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: GcodePreviewControlsViewModelCore) {
        core.retry()
        core.state.asPublisher()
            .sink { (state: GcodePreviewControlsViewModelCore.State) in
                self.state = State(state)
                
                // Double check to unhide
                if !(state is GcodePreviewControlsViewModelCore.StateFeatureDisabled) {
                    self.hidden = false
                }
            }
            .store(in: &bag)
        
        SharedBaseInjector.shared.get().preferences.updatedFlow2.asPublisher()
            .sink { (preferences: OctoPreferences) in
                self.hidden = preferences.gcodeHiddenIos
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = .loading(progress: 0)
    }
    
    func useLive() {
        currentCore?.useLive()
    }
    
    func useManual(
        file: FileObjectStruct?,
        layerIndex: Int,
        layerProgress: Float
    ) {
        if let file = file {
            currentCore?.useManual(
                path: file.path,
                origin: file.origin.toClass(),
                date: file.date?.toInstant() ?? Date(milliseconds: 0).toInstant(),
                size: file.size.flatMap{ KotlinLong(value: $0) },
                name: file.display,
                layerIndex: Int32(layerIndex),
                layerProgress: layerProgress
            )
        } else {
            currentCore?.useManualInLiveFile(
                layerIndex: Int32(layerIndex),
                layerProgress: layerProgress
            )
        }
    }
    
    func retry() {
        currentCore?.retry()
    }
    
    func allowLargeDownloads() {
        currentCore?.allowLargeDownloads()
    }
}

extension GcodePreviewControlsViewModel {
    enum State : Equatable {
        case loading(progress: Float)
        case featureDisabled(printBed: GcodeNativeCanvasImageStruct)
        case largeFileDownloadRequired(downloadSize: Int64)
        case error(exception: KotlinThrowable)
        case printingFromSdCard(file: FileObjectStruct)
        case dataReady(
            printBed: GcodeNativeCanvasImageStruct,
            printerProfile: PrinterProfileStruct?,
            settings: GcodePreviewSettingsStruct,
            exceedsPrintArea: Bool,
            unsupportedGcode: Bool,
            isTrackingPrintProgress: Bool,
            renderContext: LowOverheadMediator<GcodeRenderContext>
        )
    }
}

extension GcodePreviewControlsViewModel.State {
    init(_ state: GcodePreviewControlsViewModelCore.State) {
        if let loading = state as? GcodePreviewControlsViewModelCore.StateLoading {
            self = .loading(progress: loading.progress)
        } else if let featureDisabled = state as? GcodePreviewControlsViewModelCore.StateFeatureDisabled {
            self = .featureDisabled(printBed: featureDisabled.printBed.toStruct())
        } else if let largeFileDownloadRequired = state as? GcodePreviewControlsViewModelCore.StateLargeFileDownloadRequired {
            self = .largeFileDownloadRequired(downloadSize: largeFileDownloadRequired.downloadSize)
        } else if let printingFromSdCard = state as? GcodePreviewControlsViewModelCore.StatePrintingFromSdCard {
            self = .printingFromSdCard(file: printingFromSdCard.file.toStruct())
        } else if let error = state as? GcodePreviewControlsViewModelCore.StateError {
            self = .error(exception: error.exception)
        }  else if let dataReady = state as? GcodePreviewControlsViewModelCore.StateDataReady {
            self = .dataReady(
                printBed: dataReady.printBed.toStruct(),
                printerProfile: dataReady.printerProfile.flatMap { PrinterProfileStruct(printerProfile: $0) },
                settings: GcodePreviewSettingsStruct(gcodePreviewSettings: dataReady.settings),
                exceedsPrintArea: dataReady.exceedsPrintArea,
                unsupportedGcode: dataReady.unsupportedGcode,
                isTrackingPrintProgress: dataReady.isTrackingPrintProgress,
                renderContext: dataReady.renderContext
            )
        } else {
            self = .error(exception: KotlinIllegalStateException(message: "Unknown state: \(state)"))
        }
    }
    
    var type: Int {
        switch self {
        case .loading(progress: _): return 0
        case .featureDisabled(printBed: _): return  1
        case .largeFileDownloadRequired(downloadSize: _): return  2
        case .error(exception: _): return  3
        case .printingFromSdCard(_): return  4
        case .dataReady(printBed: _, printerProfile: _, settings: _, exceedsPrintArea: _, unsupportedGcode: _, isTrackingPrintProgress: _, renderContext: _): return 5
        }
    }
}
