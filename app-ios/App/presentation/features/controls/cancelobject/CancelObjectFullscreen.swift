//
//  CancelObjectFullscreen.swift
//  OctoApp
//
//  Created by Christian Würthner on 06/04/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct CancelObjectFullscreen: View {
    
    var objects: CancelObjectViewModel.ObjectList
    var onCancel: (String) async -> Void
    
    @StateObject private var viewModel = GcodePreviewControlsViewModel()
    
    @State private var emptyMediator = LowOverheadMediator(initialData: GcodeRenderContext.companion.Empty)
    @State private var mediator = LowOverheadMediator(initialData: GcodeRenderContext.companion.Empty)
    @State private var selected: CancelObjectViewModel.PrintObject? = nil
    @State private var showConfirmation = false
    @State private var loading = false
    @State private var quality: GcodePreviewSettingsStruct.Quality = .ultra
        
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            Text(selected?.label ?? "widget_cancel_object"~)
                .typographyTitle()
                .multilineTextAlignment(.center)
            
            GcodeRenderView(
                mediator: mediator,
                plugin: { canvas in objects.renderPlugin.render(canvas: canvas) },
                onClick: { x, y in handleClick(x: x, y: y) },
                printBed: objects.renderParams.printBed,
                printBedWidthMm: objects.renderParams.printBedWidthMm,
                printBedHeightMm: objects.renderParams.printBedHeightMm,
                extrusionWidthMm: 0,
                originInCenter: objects.renderParams.originInCenter,
                quality: quality,
                paddingHorizontal: OctoTheme.dimens.margin12,
                paddingTop: OctoTheme.dimens.margin12,
                paddingBottom: OctoTheme.dimens.margin12
            )
            .aspectRatio(1, contentMode: .fit)
            .surface()
            .onAppear { flushToPlugin(objects: objects) }
            .onChange(of: objects) { _, objects in flushToPlugin(objects: objects) }
            .onAppear { flushToRendered(state: viewModel.state) }
            .onChange(of: viewModel.state) { _, state in flushToRendered(state: state) }

            if let object = selected {
                OctoAsyncButton(
                    text: "widget_cancel_object"~,
                    loading: loading,
                    clickListener: { showConfirmation = true }
                )
                .cancelObjectConfirmation(object: object, shown: $showConfirmation) {
                    loading = true
                    Task {
                        await onCancel(object.id)
                        loading = false
                        await MainActor.run {
                            withAnimation {
                                selected = nil
                            }
                        }
                    }
                }
            }
        }
        .padding(OctoTheme.dimens.margin12)
        .animation(.default, value: selected?.id)
        .usingViewModel(viewModel)
    }
    
    func flushToRendered(state: GcodePreviewControlsViewModel.State) {
        switch state {
        case .loading(_): mediator = emptyMediator
        case .featureDisabled(_): mediator = emptyMediator
        case .largeFileDownloadRequired(_): mediator = emptyMediator
        case .error(_): mediator = emptyMediator
        case .printingFromSdCard(_): mediator = emptyMediator
        case .dataReady(_, _, settings: let settings,_, _, _, renderContext: let renderContext): do {
            mediator = renderContext
            quality = settings.quality
        }
        }
    }
    
    func flushToPlugin(objects: CancelObjectViewModel.ObjectList) {
        // Trigger paint
        Task {
            try? await mediator.republishData()
        }
    }
    
    func handleClick(x: Float, y: Float) {
        let obj = objects.all.filter { obj in
            obj.centerX != nil && obj.centerY != nil
        }.filter { obj in
            !obj.cancelled
        }.first { obj in
            let r = CGFloat(8)
            let cx = CGFloat(obj.centerX ?? 0)
            let cy = CGFloat(obj.centerY ?? 0)
            let clickBox = CGRect(x: cx - r, y: cy - r, width: r * 2, height: r * 2)
            return (clickBox.minX...clickBox.maxX).contains(CGFloat(x)) && (clickBox.minY...clickBox.maxY).contains(CGFloat(y))
        }
        
        // To animate bottom sheet
        withAnimation {
            selected = obj
        }
        
        // Trigger paint
        objects.renderPlugin.selectedId = obj.flatMap { $0.id }
        Task {
            try? await mediator.republishData()
        }
    }
}

struct CancelObjectFullscreen_Previews: PreviewProvider {
    static var previews: some View {
        CancelObjectFullscreen(
            objects: CancelObjectViewModel.ObjectList(
                all: [
                    CancelObjectViewModel.PrintObject(
                        objectId: "0",
                        cancelled: false,
                        active: false,
                        label: "Object A",
                        centerX: 40,
                        centerY: 40
                    ),
                    CancelObjectViewModel.PrintObject(
                        objectId: "1",
                        cancelled: false,
                        active: true,
                        label: "Object B",
                        centerX: 100,
                        centerY: 100
                    ),
                    CancelObjectViewModel.PrintObject(
                        objectId: "2",
                        cancelled: true,
                        active: false,
                        label: "Object C",
                        centerX: 160,
                        centerY: 160
                    ),
                ],
                subset: [],
                renderParams:  CancelObjectViewModel.RenderParams(
                    printBed: .printbedartillery,
                    printBedWidthMm: 250,
                    printBedHeightMm: 250,
                    originInCenter: false
                ),
                renderPlugin:  CancelObjectGcodeRendererPlugin(objects: [])
            )
        ) { objectId in
            
        }
    }
}
