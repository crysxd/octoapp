//
//  CancelObjectViewModel.swift
//  OctoApp
//
//  Created by Christian on 29/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import Combine

class CancelObjectViewModel : BaseViewModel {
    var currentCore: CancelObjectViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: FlowStateStruct<ObjectList> = .loading
    
    func createCore(instanceId: String) -> CancelObjectViewModelCore {
        return CancelObjectViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: CancelObjectViewModelCore) {
        core.state.asPublisher()
            .sink { (state: FlowState<CancelObjectViewModelCore.ObjectList>) in
                self.state = FlowStateStruct(state) { ObjectList($0) }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = .loading
    }
    
    func cancelObject(objectId: String) async throws {
        try await currentCore?.cancelObject(objectId: objectId)
    }
    
    func retry() {
        currentCore?.retry()
    }
    
    func refreshObjects() async {
       try? await currentCore?.triggerInitialMessage()
    }
}

extension CancelObjectViewModel {
    struct ObjectList : Equatable {
        let all: [PrintObject]
        let subset: [PrintObject]
        let renderParams: RenderParams
        let renderPlugin: CancelObjectGcodeRendererPlugin
    }
    
    struct RenderParams : Equatable {
        let printBed: GcodeNativeCanvasImageStruct
        let printBedWidthMm: Float
        let printBedHeightMm: Float
        let originInCenter: Bool
    }
    
    struct PrintObject: Equatable, Identifiable {
        var id: String { objectId }
        let objectId: String
        let cancelled: Bool
        let active: Bool
        let label: String
        let centerX: Float?
        let centerY: Float?
    }
}

fileprivate extension CancelObjectViewModel.ObjectList {
    init(_ other: CancelObjectViewModelCore.ObjectList) {
        self.init(
            all: other.all.map { CancelObjectViewModel.PrintObject($0) },
            subset: other.subset.map { CancelObjectViewModel.PrintObject($0) },
            renderParams: CancelObjectViewModel.RenderParams(other.renderParams),
            renderPlugin: other.renderPlugin
        )
    }
}

fileprivate extension CancelObjectViewModel.RenderParams {
    init(_ other: CancelObjectViewModelCore.RenderParams) {
        self.init(
            printBed: other.printBed.toStruct(),
            printBedWidthMm: other.printBedWidthMm,
            printBedHeightMm: other.printBedHeightMm,
            originInCenter: other.originInCenter
        )
    }
}

 extension CancelObjectViewModel.PrintObject {
     fileprivate init(_ other: CancelObjectViewModelCore.PrintObject) {
        self.init(
            objectId: other.objectId,
            cancelled: other.cancelled,
            active: other.active,
            label: other.label,
            centerX: other.center?.first?.floatValue,
            centerY: other.center?.second?.floatValue
        )
    }
    
    func toClass() -> CancelObjectViewModelCore.PrintObject {
        return CancelObjectViewModelCore.PrintObject(
            objectId: objectId,
            cancelled: cancelled,
            active: active,
            label: label,
            center: KotlinPair(
                first: centerX.flatMap{ KotlinFloat(float: $0) },
                second: centerY.flatMap{ KotlinFloat(float: $0) }
            )
        )
    }
}
