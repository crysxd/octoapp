//
//  QuickAccessControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import Combine

class QuickAccessControlsViewModel: BaseViewModel {
    var currentCore: QuickAccessControlsViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var menu: MenuReference? = nil
    
    func clearData() {
        menu = nil
    }
    
    func createCore(instanceId: String) -> QuickAccessControlsViewModelCore {
        return QuickAccessControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: QuickAccessControlsViewModelCore) {
        core.state.asPublisher()
            .sink { (state: QuickAccessControlsViewModelCore.State) in
                self.menu = MenuReference(
                    reference: QuickAccessMenu(
                        instanceId: core.instanceId,
                        itemIds: state.itemIds
                    ),
                    menuId: state.menuId
                )
            }
            .store(in: &bag)
    }
}

