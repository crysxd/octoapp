//
//  QuickAccess.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine

struct QuickAccessControls: View {
    @StateObject var viewModel: QuickAccessControlsViewModel
    @Environment(\.openURL) private var openURL
    
    var body: some View {
        ControlsScaffold(title: "widget_quick_access"~) {
            VStack {
                AnnouncementController(announcementId: "customization_tutorial_quick_access") { hide in
                    LargeAnnouncement(
                        title: "widget_quick_access___tutorial_title"~,
                        text: "widget_quick_access___tutorial_detail"~,
                        learnMoreButton: "widget_quick_access___tutorial_learn_more"~,
                        onLearnMore: { openURL("https://youtu.be/-gXtKqAfNRM") },
                        onHideAnnouncement: hide
                    )
                    .padding(.bottom, OctoTheme.dimens.margin2)
                }
                
                if let menu = viewModel.menu {
                    EmbeddedMenuHost(menu: menu)
                } else {
                    LoadingPlaceholder()
                }
            }
        }
    }
}


struct QuickAccessControls_Previews: PreviewProvider {
    static var previews: some View {
        QuickAccessControls(viewModel: QuickAccessControlsViewModel())
    }
}
