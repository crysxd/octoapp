//
//  AnnouncementNotificationSheet.swift
//  OctoApp
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct AnnouncementNotificationSheet : View {
    
    var onDismiss: () -> Void
    @State private var showDialog = false
    @Environment(\.openURL) private var openUrl
    private let tag = "AnnouncementNotificationSheet"
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            title
            reasons
            buttonAccept
            buttonDecline
        }
        .padding(OctoTheme.dimens.margin12)
        .asResponsiveSheet()
    }
    
    @ViewBuilder
    var title: some View {
        OctoAvatar(action: .wave)
        Text("notifications_permission___title"~)
            .multilineTextAlignment(.center)
            .typographyTitle()
            .padding(.bottom, OctoTheme.dimens.margin3)
    }
    
    @ViewBuilder
    var reasons: some View {
        NotificationSheetItem(text: "notifications_permission___reason_1_ios"~)
        NotificationSheetItem(text: "notifications_permission___reason_2_ios"~)
        NotificationSheetItem(text: "notifications_permission___reason_3_ios"~)
    }
    
    var buttonAccept: some View {
        OctoButton(text: "notifications_permission___action_enable"~) {
            UNUserNotificationCenter.current().getNotificationSettings { settings in
                if settings.authorizationStatus == .notDetermined {
                    // User not determined yet, we can request directly
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { granted, _ in
                        Napier.i(tag: tag, message: "Permission granted: \(granted)")
                        if granted {
                            onDismiss()
                            updateNotAskBefore()
                        } else {
                            showDialog = true
                        }
                    }
                } else {
                    // User declined before, we need to open app settings
                    Napier.i(tag: tag, message: "Redirecting to settings")
                    openUrl(UIApplication.openSettingsURLString)
                    onDismiss()
                    updateNotAskBefore()
                }
            }
        }
        .padding(.top, OctoTheme.dimens.margin3)
    }
    
    var buttonDecline: some View {
        OctoButton(text: "notifications_permission___action_later"~, type: .link) {
            Napier.i(tag: tag, message: "Permission denied")
            showDialog = true
        }
        .alert("notifications_permission___declined_explainer_ios"~, isPresented: $showDialog) {
            Button("continue"~) {
                onDismiss()
                updateNotAskBefore()
            }
        }
    }
    
    func updateNotAskBefore() {
        let preferences = SharedBaseInjector.shared.get().preferences
        let notBefore = Date(timeIntervalSinceNow: 2419200 * 2 /* 8 weeks */)
        let notBeforeInstant = DateUtilsKt.doNewInstant(millisSince1970: notBefore.millisecondsSince1970)
        preferences.askNotificationPermissionNotBefore = notBeforeInstant
        Napier.i(tag: tag, message: "Not asking for notifications before \(notBeforeInstant.description())")
    }
}


private struct NotificationSheetItem : View {
     
    var text: String
    var icon: String = "checkmark.circle.fill"
    
    var body: some View {
        HStack {
            Image(systemName: icon)
                .foregroundColor(OctoTheme.colors.green)
            
            Text(text)
                .typographyBase()
                .frame(maxWidth: .infinity, alignment: .leading)
        }
    }
    
}

struct NotificationManager_Previews: PreviewProvider {
    static var previews: some View {
        AnnouncementNotificationSheet {
            
        }
    }
}
