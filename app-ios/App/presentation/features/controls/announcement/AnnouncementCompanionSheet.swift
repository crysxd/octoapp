//
//  AnnouncementCompanionSheet.swift
//  OctoApp
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase


struct AnnouncementCompanionSheet: View {
    
    var onDismiss: () -> Void
    
    var body: some View {
        VStack(spacing: 0) {
            CompanionInstructions(onDismiss: onDismiss)
        }
        .padding(OctoTheme.dimens.margin12)
        .asResponsiveSheet()
        .deeplinkNavigation()
    }
}

private struct CompanionInstructions: View {
    
    var onDismiss: () -> Void
    @Environment(\.openURL) private var openUrl
    @Environment(\.instanceSystemInfo) private var systemInfo
    private let tag = "AnnouncementCompanionSheet"

    var body: some View {
        OctoAvatar(action: .wave)
        
        Text("companion_plugin_explainer___notifications_title"~)
            .typographyTitle()
            .padding(.bottom, OctoTheme.dimens.margin3)
        
        Text(
            String(
                format: "companion_plugin_explainer___notifications_description_ios"~,
                systemInfo.interfaceType.label
            )
        )
        .multilineTextAlignment(.center)
        .typographyBase()
        
        
        if systemInfo.interfaceType == .octoPrint {
            Text("companion_plugin_explainer___notifications_update"~)
                .multilineTextAlignment(.center)
                .padding(OctoTheme.dimens.margin12)
                .frame(maxWidth: .infinity)
                .surface(color: .special(OctoTheme.colors.menuStyleSettingsBackground))
                .foregroundColor(OctoTheme.colors.darkText)
                .typographyLabel()
                .padding([.bottom, .top], OctoTheme.dimens.margin3)
            
            OctoButton(text: "companion_plugin_explainer___notifications_action"~) {
                openUrl("https://plugins.octoprint.org/plugins/octoapp/")
            }
        } else {
            OctoButton(text: "companion_plugin_explainer___notifications_action_moonraker"~) {
                openUrl(UriLibrary.shared.getFaqUri(faqId: "octoapp_companion_moonraker").description())
            }
            .padding([.top], OctoTheme.dimens.margin3)
        }
        
        OctoButton(text: "notifications_permission___action_later"~, type: .link) {
            Napier.i(tag: tag, message: "Plugin not installed")
            updateNotAskBefore()
            onDismiss()
        }
    }
    
    func updateNotAskBefore() {
        let preferences = SharedBaseInjector.shared.get().preferences
        let notBefore = Date(timeIntervalSinceNow: 2419200 * 2 /* 8 weeks */)
        let notBeforeInstant = DateUtilsKt.doNewInstant(millisSince1970: notBefore.millisecondsSince1970)
        preferences.askCompanionInstallNotBefore = notBeforeInstant
        Napier.i(tag: tag, message: "Not asking for companion before \(notBeforeInstant.description())")
    }
}


private struct CompanionSheetItem : View {
     
    var text: String
    var position: Int
    var icon: String = "checkmark.circle.fill"
    
    var body: some View {
        VStack {
            Image(systemName: "\(position).circle.fill")
                .font(.system(size: 30))
                .foregroundColor(OctoTheme.colors.lightGrey)
            
            Text(text)
                .frame(maxWidth: .infinity, alignment: .center)
        }
    }
    
}


struct AnnouncementCompanionSheet_Previews: PreviewProvider {
    static var previews: some View {
        AnnouncementCompanionSheet(
            onDismiss: {}
        )
    }
}
