//
//  MultiToolControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 24/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

class MultiToolControlsViewModel: BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: MultiToolControlsViewModelCore? = nil
    @Published var state: MultiToolControlsState = MultiToolControlsState()
    
    func createCore(instanceId: String) -> MultiToolControlsViewModelCore {
        return MultiToolControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: MultiToolControlsViewModelCore) {
        state = MultiToolControlsState(core.initialState)
        core.state.asPublisher()
            .sink { (state: MultiToolControlsViewModelCore.State) in
                self.state = MultiToolControlsState(state)
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = MultiToolControlsState()
    }
    
    func setActiveTool(toolIndex: Int?) {
        currentCore?.setActiveTool(toolIndex: toolIndex.flatMap{ KotlinInt(integerLiteral: $0) }) { _ in }
    }
}

struct MultiToolControlsState : Equatable {
    var activeToolIndex: Int? = nil
    var toolCount: Int = 1
}

private extension MultiToolControlsState {
    init(_ state: MultiToolControlsViewModelCore.State) {
        self.init(
            activeToolIndex: state.activeToolIndex?.intValue,
            toolCount: Int(state.toolCount)
        )
    }
}
