//
//  TuneControlsViewmodel.swift
//  OctoApp
//
//  Created by Christian on 21/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine

class TuneControlsViewModel: BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: TuneControlsViewModelCore?
    @Published var state = TuneControlsState()
    @Published var babyStepIntervals: [Float] = [0.005, 0.01, 0.025, 0.05]
    
    func createCore(instanceId: String) -> TuneControlsViewModelCore {
        return TuneControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: TuneControlsViewModelCore) {
        core.state.asPublisher()
            .sink { (state: TuneState) in
                self.state = TuneControlsState(
                    flowRate: state.print?.flowRate?.intValue,
                    feedRate: state.print?.feedRate?.intValue,
                    fanSpeeds: state.fans.map { fan in
                        TuneControlsState.Fan(
                            speed: fan.speed.flatMap { Int($0.floatValue.rounded()) },
                            label: fan.label,
                            component: fan.component,
                            isPartCoolingFan: fan.isPartCoolingFan
                        )
                    },
                    zOffset: state.offsets?.z?.floatValue,
                    isValuesImprecise: state.isValuesImprecise
                )
            }
            .store(in: &bag)
        
        core.babyStepIntervals.asPublisher()
            .sink { (settings: TuneControlsViewModelCore.Settings) in
                self.babyStepIntervals = settings.babyStepIntervals.map { Float(truncating: $0) }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = TuneControlsState()
    }
    
    func applyChanges(feedRate: Int?, flowRate: Int?, fanSpeeds: [String: Int?]) async throws {
        try await currentCore?.applyChanges(
            feedRate: feedRate.flatMap { KotlinFloat(float: Float($0)) },
            flowRate: flowRate.flatMap { KotlinFloat(float: Float($0)) },
            fanSpeeds: fanSpeeds.mapValues { value in
                value.flatMap { KotlinFloat(float: Float($0)) } as Any
            }
        )
    }
    
    func changeOffset(changeMm: Float) async throws {
        try await currentCore?.changeOffset(changeMm: changeMm)
    }
    
    func resetOffset() async throws {
        try await currentCore?.resetOffset()
    }
    
    func saveOffsetToConfig() async throws {
        try await currentCore?.saveOffsetToConfig()
    }
    
    func setActivePolling(active: Bool) {
        currentCore?.setActivePoll(active: active)
    }
}

struct TuneControlsState: Equatable {
    var flowRate: Int? = nil
    var feedRate: Int? = nil
    var fanSpeeds: [Fan] = []
    var zOffset: Float? = nil
    var isValuesImprecise: Bool = true
}

extension TuneControlsState {
    struct Fan : Equatable, Identifiable, Hashable {
        var id: String { component }
        var speed: Int?
        var label: String
        var component: String
        var isPartCoolingFan: Bool
    }
}
