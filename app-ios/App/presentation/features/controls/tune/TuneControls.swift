//
//  TuneControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct TuneControls: View {
    
    @StateObject var viewModel: TuneControlsViewModel
    @State private var showInputs = false

    var body: some View {
        ControlsScaffold(title: nil) {
            Button(
                action: { showInputs = true },
                label: { TuneControlsContent(state: viewModel.state) }
            )
            .macOsCompatibleSheet(isPresented: $showInputs) {
                TuneControlsInputs(viewModel: viewModel)
            }
        }
    }
}

private struct TuneControlsContent: View {
    
    var state: TuneControlsState
    
    var body: some View {
        content
            .padding(OctoTheme.dimens.margin2)
            .frame(maxWidth: .infinity)
            .background(OctoTheme.colors.inputBackground)
            .cornerRadius(OctoTheme.dimens.cornerRadius)
            .padding(.top, OctoTheme.dimens.margin12)
    }
    
    var content: some View {
        HStack(spacing: 0) {
            Text("tune"~)
                .typographySectionHeader()
            
            Spacer()
            
            // Add id to prevent recomposition unless value changes (disrupts slow spin animation)
            let fanSpeed = state.fanSpeeds.first { $0.isPartCoolingFan }?.speed
            TuneItem(value: fanSpeed, isFan: true, iconSystemName: "fanblades.fill").id("fan/\(fanSpeed ?? 0)")
            TuneItem(value: state.flowRate, isFan: false, iconSystemName: "lineweight")
            TuneItem(value: state.feedRate, isFan: false, iconSystemName: "speedometer")
        
            Image(systemName: "chevron.right")
                .foregroundColor(OctoTheme.colors.accent)
        }
    }
}

private struct TuneItem: View {
 
    var value: Int?
    var isFan: Bool
    var iconSystemName: String
    
    @State private var rotated = false
    
    var body: some View {
        if let v = value {
            Image(systemName: iconSystemName)
                .typographyLabel()
                .if(value ?? 0 > 0) { $0.rotationEffect(Angle(degrees: rotated ? 360 : 0)) }
                .padding(.trailing, OctoTheme.dimens.margin0)
                .task(id: value) {
                    // Spin the fan!
                    guard isFan else { return }
                    let value = Double(value ?? 0)
                    let percent = 1 - (value / 100)
                    let duration = (9 * percent) + 2 // 2...15s depending on value
                    withAnimation(.linear(duration: duration).repeatForever(autoreverses: false)) {
                        rotated.toggle()
                    }
                }
            
            Text(v.formatAsPercent())
                .typographyLabel()
                .padding(.trailing, OctoTheme.dimens.margin1)
        }
    }
    
}

struct TuneControls_Previews: PreviewProvider {
    static var previews: some View {
        TuneControlsContent(state: TuneControlsState())
            .previewDisplayName("Initial")
        
        TuneControlsContent(
            state: TuneControlsState(
                fanSpeeds: [
                    TuneControlsState.Fan(speed: 10, label: "Fan", component: "fan", isPartCoolingFan: false),
                    TuneControlsState.Fan(speed: 80, label: "Fan2", component: "fan2", isPartCoolingFan: true)
                ]
            )
        )
        .previewDisplayName("Some")
        
        TuneControlsContent(
            state: TuneControlsState(
                flowRate: 100,
                feedRate: 105,
                fanSpeeds: [
                    TuneControlsState.Fan(speed: 80, label: "Fan", component: "fan", isPartCoolingFan: false),
                    TuneControlsState.Fan(speed: 10, label: "Fan2", component: "fan2", isPartCoolingFan: true)
                ]
            )
        )
        .previewDisplayName("All")
    }
}
