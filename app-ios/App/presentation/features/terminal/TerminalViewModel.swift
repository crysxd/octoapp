//
//  TerminalViewModel.swift
//  OctoApp
//
//  Created by Christian on 23/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine


private let emptyState = TerminalViewModel.State(
    logs: [],
    inputEnabled: true,
    shortcuts: [],
    useStyledList: false,
    filterCount: 0
)

class TerminalViewModel : BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: TerminalViewModelCore?
    @Published var state = emptyState
    
    func createCore(instanceId: String) -> TerminalViewModelCore {
        TerminalViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: TerminalViewModelCore) {
        state = createEmptyState()
        core.state.asPublisher()
            .sink { (state: TerminalViewModelCore.State) in
                self.state = state.toStruct()
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = createEmptyState()
    }
    
    func executeGcode(_ gcode: String) async {
        try? await currentCore?.executeGcode(gcode: gcode)
    }
    
    func toggleStyledLogs() async {
        try? await currentCore?.toggleStyledLogs()
    }
    
    
    func clear() {
        currentCore?.clear()
    }
    
    private func createEmptyState() -> TerminalViewModel.State {
        TerminalViewModel.State(
            logs: [],
            inputEnabled: true,
            shortcuts: currentCore?.getShortcuts().compactMap { $0.toStruct() } ?? [],
            useStyledList: false,
            filterCount: 0
        )
    }
}

extension TerminalViewModel {
    struct State : Equatable {
        let logs: [TerminalLogLine]
        let inputEnabled: Bool
        let shortcuts: [Shortcut]
        let useStyledList: Bool
        let filterCount: Int
    }
    
    struct Shortcut : Equatable, Identifiable{
        var id: String { command }
        let command: String
        let label: String
        let isFavourite: Bool
    }
    
    struct TerminalLogLine : Equatable, Identifiable {
        let id: Int
        let text: String
        let textStyled: String
        let isStart: Bool
        let isEnd: Bool
        let isMiddle: Bool
    }
}

fileprivate extension GcodeHistoryItem {
    func toStruct() -> TerminalViewModel.Shortcut {
        TerminalViewModel.Shortcut(
            command: command,
            label: name,
            isFavourite: isFavorite
        )
    }
}

fileprivate extension TerminalViewModelCore.State {
    func toStruct() -> TerminalViewModel.State {
        TerminalViewModel.State(
            logs: logs.map { $0.toStruct() },
            inputEnabled: inputEnabled,
            shortcuts: shortcuts.map { $0.toStruct() },
            useStyledList: useStyledList,
            filterCount: Int(filterCount)
        )
    }
}

fileprivate extension TerminalViewModelCore.TerminalLogLine {
    func toStruct() -> TerminalViewModel.TerminalLogLine {
        TerminalViewModel.TerminalLogLine(
            id: Int(id),
            text: text,
            textStyled: textStyled,
            isStart: isStart,
            isEnd: isEnd,
            isMiddle: isMiddle
        )
    }
}
