//
//  PluginSupport.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI


struct PluginSupport: ViewModifier {
    
    func body(content: Content) -> some View {
        ZStack {
            NgrokSupport()
            Mmu2FilamentSelectSupport()
            OctolapseSupport()
            content
        }
    }
}

extension View {
    func pluginSupport() -> some View {
        modifier(PluginSupport())
    }
}
