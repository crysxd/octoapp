//
//  OctolapseSupport.swift
//  OctoApp
//
//  Created by Christian Würthner on 07/04/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

@MainActor
struct OctolapseSupport: View {
    
    @StateObject private var viewModel = OctolapseSupportViewModel()
    @State private var sheetShown = false
    
    var body: some View {
        ZStack {}
            .usingViewModel(viewModel)
            .macOsCompatibleSheet(isPresented: $sheetShown) {
                OctolapseSupportSheet(
                    viewModel: viewModel,
                    state: viewModel.state ?? OctolapseSupportViewModelCore.StateIdle()
                )
                .asResponsiveSheet()
                .interactiveDismissDisabled(true)
            }
            .onChange(of: viewModel.state) { _, new in
                sheetShown = new != nil && !(new is OctolapseSupportViewModelCore.StateIdle)
            }
    }
}

extension OctolapseSupportViewModelCore.State : Identifiable {
    public var id: Int64 { receivedAt }
}

struct OctolapseSupportSheet: View {
    
    @StateObject fileprivate var viewModel: OctolapseSupportViewModel
    @State var state: OctolapseSupportViewModelCore.State
    @Environment(\.dismiss) private var dismiss

    var body: some View {
        OctolapseSupportSheetContent(
            event: state,
            onCancel: {
                if await viewModel.declineSnapshotPlan() {
                    dismiss()
                }
            },
            onAccept: {
                if await viewModel.acceptSnapshotPlan() {
                    dismiss()
                }
            }
        )
        .onChange(of: viewModel.state) { old, new in
            // Skip idle state, sheet will be dismissed
            if !(new is OctolapseSupportViewModelCore.StateIdle) {
                // Use with animation to animate between loading/snapshot states
                withAnimation {
                    new?.handled = true
                    self.state = new ?? old ?? self.state
                }
            }
        }
    }
}

struct OctolapseSupportSheetContent: View {
    var event: OctolapseSupportViewModelCore.State
    var onCancel: () async -> Void
    var onAccept: () async -> Void
    
    private var title: String {
        if event is OctolapseSupportViewModelCore.StateShowMessage {
            return "octolapse___error___title"~
        } else if let event = event as? OctolapseSupportViewModelCore.StateLoading {
            return event.title
        } else if event is OctolapseSupportViewModelCore.StateSnapshotPlanPreview {
            return "octolapse___snapshot_plan___title_ready"~
        } else {
            return "???"
        }
    }
    
    var body: some View {
        VStack(spacing: 0) {
            Text(title)
                .multilineTextAlignment(.center)
                .typographyTitle()
                .padding(.bottom, OctoTheme.dimens.margin3)
            
            if let event = event as? OctolapseSupportViewModelCore.StateShowMessage {
                OctolapseSupportSheetMessage(event: event)
            } else if event is OctolapseSupportViewModelCore.StateLoading {
                OctolapseSupportSheetLoading()
            } else if let event = event as? OctolapseSupportViewModelCore.StateSnapshotPlanPreview {
                OctolapseSupportSheetPreview(
                    event: event,
                    onCancel: onCancel,
                    onAccept: onAccept
                )
            }
        }
        .padding(OctoTheme.dimens.margin12)
        .animation(.default, value: title)
    }
}

struct OctolapseSupportSheetMessage: View {
    var event: OctolapseSupportViewModelCore.StateShowMessage
    
    var body: some View {
        ScreenError(
            title: event.title,
            description: event.message as? String ?? "",
            canDismiss: true
        )
        .padding(.bottom, OctoTheme.dimens.margin3)
    }
}

struct OctolapseSupportSheetLoading: View {
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin4) {
            ProgressView()
        }
        .padding(OctoTheme.dimens.margin3)
        .multilineTextAlignment(.center)
        .typographyTitle()
    }
}

struct OctolapseSupportSheetPreview: View {
    var event: OctolapseSupportViewModelCore.StateSnapshotPlanPreview
    var onCancel: () async -> Void
    var onAccept: () async -> Void
    
    @State private var layer: Float = 0
    @State private var mediator = LowOverheadMediator(initialData: GcodeRenderContext.companion.Empty)
    @State private var emptyMediator = LowOverheadMediator(initialData: GcodeRenderContext.companion.Empty)
    @State private var quality: GcodePreviewSettingsStruct.Quality = .ultra
    @StateObject private var viewModel = GcodePreviewControlsViewModel()

    private var layerText: String {
        let layerNumber = event.layerNumberDisplay(KotlinInt(int: Int32(layer))).intValue
        let layerCount = event.layerCountDisplay(KotlinInt(int: event.layerCount)).intValue
        return String(format: "x_of_y"~, layerNumber.formatted(), layerCount.formatted())
    }
        
    private var positionText: String {
        let layer = Int(layer)
        guard let layers = event.plan.snapshotPlans?.snapshotPlans else { return "" }
        guard layers.count > layer, let position = event.plan.snapshotPlans?.snapshotPlans?[layer].steps?.first else { return "" }
        return String(
            format: "octolapse___snapshot_plan___snapshot_at_x_y"~,
            position.x?.formatAsLength(minDecimals: 1, maxDecimals: 1) ?? "",
            position.y?.formatAsLength(minDecimals: 1, maxDecimals: 1) ?? ""
        )
    }
    
    var body: some View {
        VStack(spacing: 0) {
            preview
            legend
            slider
            options
        }
        .typographyBase()
        .usingViewModel(viewModel)
        .onChange(of: viewModel.state) { _, state in
            flushToRendered(state: state)
        }
        .onChange(of: layer) { _, new in
            event.renderer.layer = Int32(new)
            Task {
              try? await  mediator.republishData()
            }
            viewModel.useManual(file: nil, layerIndex: Int(layer), layerProgress: 1)
        }
        .onAppear {
            viewModel.useManual(file: nil, layerIndex: Int(layer), layerProgress: 1)
        }
    }
    
    var preview: some View {
        GcodeRenderView(
            mediator: mediator,
            plugin: { event.renderer.render(canvas: $0) },
            printBed: event.printBed.toStruct(),
            printBedWidthMm: event.printBedWidthMm,
            printBedHeightMm: event.printBedHeightMm,
            extrusionWidthMm: 0,
            originInCenter: event.originInCenter,
            quality: quality,
            paddingHorizontal: OctoTheme.dimens.margin12,
            paddingTop: OctoTheme.dimens.margin12,
            paddingBottom: OctoTheme.dimens.margin12
        )
        .aspectRatio(1, contentMode: .fit)
        .frame(maxWidth: 400)
        .surface()
    }
    
    var legend: some View {
        HStack {
            OctolapseSupportSheetPreviewLegendItem(color: OctoTheme.colors.red, text: "octolapse___position_start"~)
            OctolapseSupportSheetPreviewLegendItem(color: OctoTheme.colors.accent, text: "octolapse___position_snapshot"~)
            OctolapseSupportSheetPreviewLegendItem(color: OctoTheme.colors.green, text: "octolapse___position_return"~)
        }
        .padding(.top, OctoTheme.dimens.margin1)
    }
    
    var slider: some View {
        VStack(spacing: OctoTheme.dimens.margin01) {
            HStack {
                Text("gcode_preview___layer")
                    .typographySubtitle()
                Text(layerText)
                    .foregroundColor(OctoTheme.colors.lightText)
                    .typographySubtitle()
                Spacer()
                
                Text(positionText)
                    .foregroundColor(OctoTheme.colors.lightText)
                    .typographySubtitle()
            }
            Slider(
                value: $layer,
                in: 0...max(1, Float(event.layerCount - 1)),
                step: 1
            )
        }
        .padding(.top, OctoTheme.dimens.margin3 + OctoTheme.dimens.margin12)
        .padding(.bottom, OctoTheme.dimens.margin12)
    }
    
    var options: some View {
        HStack {
            OctoAsyncButton(
                text: "cancel"~,
                type: .secondary,
                small: true,
                fillWidth: true,
                clickListener: onCancel
            )
            OctoAsyncButton(
                text: "start_printing"~,
                type: .primary,
                small: true,
                fillWidth: true,
                clickListener: onAccept
            )
        }
        .padding(.top, OctoTheme.dimens.margin3)
    }
    
    func flushToRendered(state: GcodePreviewControlsViewModel.State) {
        switch state {
        case .loading(_): mediator = emptyMediator
        case .featureDisabled(_): mediator = emptyMediator
        case .largeFileDownloadRequired(_): mediator = emptyMediator
        case .error(_): mediator = emptyMediator
        case .printingFromSdCard(_): mediator = emptyMediator
        case .dataReady(_, _, settings: let settings,_, _, _, renderContext: let renderContext): do {
            mediator = renderContext
            quality = settings.quality
        }
        }
    }
}

struct OctolapseSupportSheetPreviewLegendItem: View {
    var color: Color
    var text: String
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin0) {
            Circle().fill(color).frame(width: 8, height: 8)
            Text(text).foregroundColor(OctoTheme.colors.lightText).typographyLabelSmall()
        }
    }
}


private class OctolapseSupportViewModel : BaseViewModel {
    var currentCore: OctolapseSupportViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: OctolapseSupportViewModelCore.State? = nil
    
    func createCore(instanceId: String) -> OctolapseSupportViewModelCore {
        OctolapseSupportViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: OctolapseSupportViewModelCore) {
        core.state.asPublisher()
            .sink { (event: OctolapseSupportViewModelCore.State) in
                self.state = event
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = nil
    }
    
    func acceptSnapshotPlan() async -> Bool {
        (try? await currentCore?.acceptSnapshotPlan()) == true
    }
    
    
    func declineSnapshotPlan() async -> Bool {
        (try? await currentCore?.declineSnapshotPlan()) == true
    }
}

struct OctolapseSupport_Previews: PreviewProvider {
    static var previews: some View {
        OctolapseSupportSheetContent(
            event: OctolapseSupportViewModelCore.StateLoading(
                title: "Loading",
                receivedAt: 0
            ),
            onCancel: {},
            onAccept: {}
        )
        .previewDisplayName("Loading")
        
        OctolapseSupportSheetContent(
            event: OctolapseSupportViewModelCore.StateShowMessage(
                message: "This is a test",
                title: "Title",
                receivedAt: 0
            ),
            onCancel: {},
            onAccept: {}
        )
        .previewDisplayName("Message")
        
        let preview = OctolapseSnapshotPlanPreview(
            jobId: nil,
            snapshotPlans: OctolapseSnapshotPlanPreview.SnapshotPlanList(
                snapshotPlans: [
                    OctolapseSnapshotPlanPreview.SnapshotPlan(
                        returnPosition: OctolapseSnapshotPlanPreview.Position(
                            x: 20,
                            y: 20
                        ),
                        initialPosition: OctolapseSnapshotPlanPreview.Position(
                            x: 20,
                            y: 20
                        ),
                        steps: [
                            OctolapseSnapshotPlanPreview.Step(
                                action: "dsf",
                                x: 40,
                                y: 40
                            ),
                            OctolapseSnapshotPlanPreview.Step(
                                action: "dsf",
                                x: 80,
                                y: 80
                            )
                        ]
                    ),
                    OctolapseSnapshotPlanPreview.SnapshotPlan(
                        returnPosition: OctolapseSnapshotPlanPreview.Position(
                            x: 40,
                            y: 40
                        ),
                        initialPosition: OctolapseSnapshotPlanPreview.Position(
                            x: 120,
                            y: 120
                        ),
                        steps: [
                            OctolapseSnapshotPlanPreview.Step(
                                action: "dsf",
                                x: 140,
                                y: 140
                            ),
                            OctolapseSnapshotPlanPreview.Step(
                                action: "dsf",
                                x: 180,
                                y: 180
                            )
                        ]
                    ),
                    OctolapseSnapshotPlanPreview.SnapshotPlan(
                        returnPosition: OctolapseSnapshotPlanPreview.Position(
                            x: 80,
                            y: 20
                        ),
                        initialPosition: OctolapseSnapshotPlanPreview.Position(
                            x: 80,
                            y: 20
                        ),
                        steps: [
                            OctolapseSnapshotPlanPreview.Step(
                                action: "dsf",
                                x: 40,
                                y: 40
                            ),
                            OctolapseSnapshotPlanPreview.Step(
                                action: "dsf",
                                x: 80,
                                y: 80
                            )
                        ]
                    )
                ],
                volume: nil
            )
        )
        
        OctolapseSupportSheetContent(
            event: OctolapseSupportViewModelCore.StateSnapshotPlanPreview(
                plan: preview,
                renderer: OctolapseGcodeRenderPlugin(snapshotPlanPreview: preview),
                printBed: .printbedender,
                printBedWidthMm: 235,
                printBedHeightMm: 235,
                originInCenter: false,
                layerCount: 3,
                layerCountDisplay: { $0 },
                layerNumberDisplay: { $0 },
                receivedAt: 0
            ),
            onCancel: {},
            onAccept: {}
        )
        .previewDisplayName("Preview")
    }
}
