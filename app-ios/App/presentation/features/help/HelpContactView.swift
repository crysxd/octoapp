//
//  HelpContact.swift
//  OctoApp
//
//  Created by Christian on 26/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import MessageUI
import SwiftUIMailView
import FirebaseStorage
import OctoAppBase

private let logTag = "HelpContactView"

@MainActor
struct HelpContactView: View {
    var bugReport: Bool
    
    @StateObject private var viewModel = HelpContactViewModel()
    @Environment(\.dismiss) var dismiss
    @Environment(\.openExternalURL) var openURL
    @EnvironmentObject var orchestrator: OrchestratorViewModel
    
    @State private var input: String = ""
    @State private var purchaseInfo = true
    @State private var phoneInfo = true
    @State private var octoPrintInfo = true
    @State private var appLogs = true
    
    @State private var result: Result<MFMailComposeResult, Error>? = nil
    @State private var showEmptyMessage = false
    @State private var showNoEmail = false
    @State private var showUpdateInfo = false
    @State private var showLongLoadInfo = false
    @State private var showShare = false
    @State private var sharePath: URL? = nil
    @State private var loading = false
    @State private var showMailView = false
    @State private var mailData: ComposeMailData = .empty
    
    var body: some View {
        ScrollView {
            VStack {
                header
                inputField
                toggles
                longLoadingInfo
                button
                explainer
            }
            .padding(OctoTheme.dimens.margin12)
            .alert("contact_form___enter_a_message"~, isPresented: $showEmptyMessage) {
                Button("OK"~, role: .cancel) { showEmptyMessage = false }
            }
            .alert("contact_form___email_not_set_up"~, isPresented: $showNoEmail) {
                Button("OK"~) { showShare = sharePath != nil }
            }
            .alert("contact_form___update_available_info"~, isPresented: $showUpdateInfo) {
                Button("contact_form___update_available_later", role: .cancel) { }
                Button("contact_form___update_available_update_now", role: .destructive) {
                    openURL(URL(string: "itms-apps://itunes.apple.com/app/id1024941703")!)
                }
            }
        }
        .navigationTitle("contact_form___title"~)
        .background(OctoTheme.colors.windowBackground)
        .animation(.default, value: showLongLoadInfo)
        .animation(.default, value: showUpdateInfo)
        .task {
            showUpdateInfo = await viewModel.checkForUpdate()
        }
        .sheet(isPresented: $showShare) {
            if let path = sharePath {
                ShareSheet(activityItems: [path])
            }
        }
        .sheet(isPresented: $showMailView) {
            MailView(data: $mailData) { result in
                switch result {
                case .success(let r): if r == .sent {
                    Napier.i(tag: logTag, message: "Send success")
                    dismiss()
                } else {
                    Napier.i(tag: logTag, message: "Send cancelled or saved")
                }
                case .failure(let e): Napier.e(tag: logTag, message: "Send failed: \(e)")
                }
            }
        }
    }
    
    var header: some View {
        Text("contact_form___instructions"~)
            .typographyBase()
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    var inputField: some View {
        OctoTextField(
            placeholder: "contact_form___message"~,
            labelActiv: LocalizedStringKey("contact_form___message"~),
            input: $input,
            multiline: true
        )
        .submitLabel(.return)
        .padding(.top, OctoTheme.dimens.margin1)
    }
    
    var toggles: some View {
        VStack {
            Toggle("contact_form___information_about_your_purchases"~, isOn: $purchaseInfo)
                .disabled(true)
            Toggle("contact_form___information_about_your_phone"~, isOn: $phoneInfo)
                .disabled(bugReport)
            Toggle("contact_form___information_about_your_octoprint"~, isOn: $octoPrintInfo)
                .disabled(bugReport)
            Toggle("contact_form___app_logs"~, isOn: $appLogs)
        }
        .toggleStyle(SwitchToggleStyle(tint: OctoTheme.colors.accent))
        .typographyBase()
        .padding([.top], OctoTheme.dimens.margin2)
        .padding([.bottom], OctoTheme.dimens.margin3)
    }
    
    @ViewBuilder
    var explainer: some View {
        if (!MFMailComposeViewController.canSendMail()) {
            Text("contact_form___upload_explainer"~)
                .typographyLabel()
                .multilineTextAlignment(.center)
        }
    }
    
    @ViewBuilder
    var button: some View {
        Text(String(format: "help___contact_detail_information"~, viewModel.contactTime, viewModel.contactTimeZone))
            .multilineTextAlignment(.center)
            .typographyLabelSmall()
            .frame(maxWidth: .infinity, alignment: .center)
            .padding(.top, OctoTheme.dimens.margin1)
            .padding(.bottom, OctoTheme.dimens.margin0)
        
        OctoAsyncButton(
            text: "contact_form___open_email"~,
            loading: loading,
            clickListener: { await sendEmail() },
            longClickListener: {
                Task {
                    loading = true
                    await prepareEmail()
                    sharePath = viewModel.saveAttachments(mailData: mailData)
                    showShare = true
                    loading = false
                }
            }
        )
    }
    
    @ViewBuilder
    var longLoadingInfo: some View {
        if showLongLoadInfo {
            Text("contact_form___delay_information"~)
                .typographyBase()
        }
    }
    
    private func sendEmail() async {
        guard !input.isEmpty else {
            showEmptyMessage = true
            return
        }
        
        await prepareEmail()
        
        if MFMailComposeViewController.canSendMail() {
            showMailView = true
        } else {
            try? await openThirdPartyMail(mail: mailData)
        }
    }
    
    private func prepareEmail() async {
        let subtask = Task {
            showLongLoadInfo = false
            try await Task.sleep(for: .seconds(3))
            showLongLoadInfo = true
        }
        
        defer {
            subtask.cancel()
        }
        
        mailData = await viewModel.prepareFeedbackMail(
            sendPhoneInfo: phoneInfo,
            sendOctoPrintInfo: octoPrintInfo,
            sendLogs: appLogs,
            message: input
        )
    }
    
    private func openThirdPartyMail(mail: ComposeMailData) async throws {
        do {
            loading = true
            var downloadUrl: String = ""
            let tag = "HelpContactView"
            
            if let attachments = mail.attachments {
                for a in attachments {
                    Napier.i(tag: tag, message: "Uploading attachment to Storage (\(a.data.count.asStyleFileSize()))")
                    let storage = Storage.storage(url: "gs://octoapp-dropbox/")
                    let id = UUID().uuidString
                    let reference = storage.reference().child("\(id).zip")
                    let metadataIn = StorageMetadata()
                    metadataIn.contentType = a.mimeType
                    let metadataOut = try await reference.putDataAsync(a.data, metadata: metadataIn)
                    Napier.i(tag: tag, message: "Uploaded to storage: \(metadataOut)")
                    downloadUrl += "Debug information id (do not remove): \(id)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                    downloadUrl += "%0D%0A"
                }
            }
            
            let email = mail.recipients?[0] ?? HelpContactViewModel.fallbackEmail
            let subject = mail.subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let inputFormatted = mail.message.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let body = "\(inputFormatted)%0D%0A%0D%0A%0D%0A----%0D%0A\(downloadUrl)"
            
            openURL(URL(string: "mailto:\(email)?subject=\(subject)&body=\(body)")!) { success in
                if success {
                    dismiss()
                } else {
                    showLongLoadInfo = false
                    sharePath = viewModel.saveAttachments(mailData: mail)
                    showNoEmail = true
                }
            }
            
            showLongLoadInfo = false
            loading = false
        } catch let e {
            loading = false
            throw e
        }
    }
}

struct HelpContact_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            HelpContactView(bugReport: true)
        }
    }
}
