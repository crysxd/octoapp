//
//  FileList.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine

struct FileManagerView: View {
    @State var path: String
    var title: String = ""
    
    @Environment(\.dismissToRoot) private var dismissToRoot
    @Environment(\.instanceId) private var instanceId
    @Environment(\.dismiss) private var dismiss
    @EnvironmentObject private var fileActionsViewModel: FileActionsViewModel
    @StateObject private var viewModel = FileManagerViewModel()
    @MainActor @State private var startPrintResult: StartPrintResultStruct = .success
    @State private var isDirectory = true
    @State private var fileSize: Int64? = nil
    @State private var showAddMenu = false
    @State private var showFilterMenu = false
    @State private var showActionsMenu = false
    @State private var sharePath: String? = nil
    
    var body: some View {
        ZStack {
            switch(viewModel.state) {
            case .loading: ProgressView()
            case .error(let e): do {
                ScreenError(
                    title: "file_manager___file_list___error_title"~,
                    description: "file_manager___file_list___error_description"~,
                    error: e,
                    onRetry: {  viewModel.reload() }
                )
            }
            case .loaded(let f): do {
                if (f.isFolder) {
                    FileListView(
                        file: f,
                        informAboutThumbnails: viewModel.informAboutThumbnails,
                        onDismissThumbnailInfo: { viewModel.dismissThumbnailInfo() }
                    )
                } else {
                    FileDetailsView(
                        file: f,
                        canStartPrint: viewModel.canStartPrint,
                        onStartPrint: {
                            startPrintResult = .failed
                            startPrintResult = await viewModel.startPrint(
                                materialConfirmed: false,
                                timelapseConfirmed: false
                            )
                            await checkPrintStarted()
                        }
                    )
                    .onAppear {
                        isDirectory = false
                        fileSize = f.size
                    }
                    .startPrintConfirmations(result: startPrintResult, filePath: f.path) { materialConfirmed, timelapseConfirmed in
                        startPrintResult = await viewModel.startPrint(
                            materialConfirmed: materialConfirmed,
                            timelapseConfirmed: timelapseConfirmed
                        )
                        await checkPrintStarted()
                    }
                }
            }
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .usingViewModel(viewModel)
        .onAppear { viewModel.initWith(path: path, title: title) }
        .onChange(of: path) { _, newPath in viewModel.initWith(path: newPath, title: title) }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .navigationTitle(viewModel.title ?? title)
        .refreshable { await viewModel.reloadAndWait() }
        .onChange(of: viewModel.printStarted) { _ , ps in if ps { dismissToRoot() } }
        .animation(.default, value: viewModel.state)
        .animation(.default, value: viewModel.informAboutThumbnails)
        .environmentObject(viewModel)
        .toolbar {
            if isDirectory {
                // To push the other two items to one side
                ToolbarItemGroup(placement: .bottomBar) {
                    Text("")
                }
                
                ToolbarItemGroup(placement: .bottomBar) {
                    Button(action: { showAddMenu = true }) {
                        Image(systemName: "plus.circle.fill")
                    }
                    Button(action: { showFilterMenu = true }) {
                        Image(systemName: "line.3.horizontal.decrease.circle.fill")
                    }
                }
            }

            ToolbarItemGroup(placement: .primaryAction) {
                Button(action: { showActionsMenu = true }) {
                    Image(systemName: "ellipsis.circle")
                }
            }
        }
        .macOsCompatibleSheet(isPresented: $showAddMenu) {
            MenuHost(menu: AddItemMenu(instanceId: instanceId, path: path))
        }
        .macOsCompatibleSheet(isPresented: $showFilterMenu) {
            MenuHost(menu: FileSortOptionsMenu())
        }
        .macOsCompatibleSheet(isPresented: $showActionsMenu) {
            MenuHost(
                menu: FileActionsMenu(
                    instanceId: instanceId,
                    display: title,
                    size: fileSize.flatMap { KotlinLong(value: $0) },
                    path: path,
                    isDirectory: isDirectory
                )
            ) { result in
                if let sharePath = FileActionsMenu.companion.getSharePath(result: result) {
                    Task {
                        // Wait a second before opening the share sheet, crashes otherwise
                        try await Task.sleep(for: .seconds(1))
                        self.sharePath = sharePath
                    }
                } else if let newPath = FileActionsMenu.companion.getNewPathAfterMove(result: result) {
                    path = newPath
                } else if FileActionsMenu.companion.isCopy(result: result) {
                    fileActionsViewModel.copy(instanceId: instanceId, path: path, display: title)
                } else if FileActionsMenu.companion.isCut(result: result) {
                    fileActionsViewModel.cut(instanceId: instanceId, path: path, display: title)
                } else if FileActionsMenu.companion.isDelete(result: result) {
                    dismiss()
                }
            }
        }
        .sheet(item: $sharePath) { sharePath in
            ShareSheet(activityItems: [URL(filePath: sharePath)]) { _, _ , _, _ in }
        }
    }
    
    func checkPrintStarted() async {
        if startPrintResult == .success {
            // Dismiss if print was started
            // For some reason we need a slight delay for dismiss to work
            try? await Task.sleep(for: .milliseconds(500))
            dismissToRoot()
        }
    }
}


struct FileList_Previews: PreviewProvider {
    static var previews: some View {
        FileManagerView(path: "/", title: "Title")
    }
}
