//
//  FileListView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct FileListView: View {
    
    var file: FileObjectStruct
    var informAboutThumbnails: Bool
    var onDismissThumbnailInfo: () -> Void
    @State private var searchTerm: String = ""
    @State private var showAddMenu: Bool = false
    @State private var showFilterMenu: Bool = false
    @State private var showOptionsMenu: Bool = false
    @State private var fileForSharing: SharableFile? = nil
    @State private var renameFromOriginalName: String? = nil
    @Environment(\.instanceId) private var instanceId
    @EnvironmentObject var copyAndMoveViewModel: FileActionsViewModel

    private var alwaysShowSearch : Bool {
        file.path == "/" && BillingManager.shared.isFeatureEnabled(feature: BillingManagerKt.FEATURE_FILE_MANAGEMENT)
    }
    
    var body: some View {
        VStack(spacing: 0) {
            list
            sharedFileManagerViewModelState
        }
        .animation(.spring(), value: copyAndMoveViewModel.state)
        .searchable(text: $searchTerm, placement: .navigationBarDrawer(displayMode: alwaysShowSearch ? .always : .automatic))
        .macOsCompatibleSheet(isPresented: $showFilterMenu) {
            MenuHost(menu: FileSortOptionsMenu())
        }
        .macOsCompatibleSheet(isPresented: $showAddMenu) {
            MenuHost(menu: AddItemMenu(instanceId: instanceId, path: file.path))
        }
        .macOsCompatibleSheet(isPresented: $showOptionsMenu) {
            MenuHost(menu: AddItemMenu(instanceId: instanceId, path: file.path))
        }
    }
    
    var list: some View {
        FileListOuter(
            file: file,
            searchTerm: $searchTerm,
            informAboutThumbnails: informAboutThumbnails,
            onDismissThumbnailInfo: onDismissThumbnailInfo
        )
    }
    
    @ViewBuilder
    var sharedFileManagerViewModelState: some View {
        ZStack {
            switch copyAndMoveViewModel.state {
            case .idle: do {}
            case .renaming(originalName: _): do {}
            case .readyToShare(display: _, localFilePath: _): do {}
            case .readyToPaste(display: let display): SharedFileManagerViewModelState(
                text: display,
                canPaste: true,
                path: file.path
            )
            case .loading(display: let display): SharedFileManagerViewModelState(
                loading: true,
                text: display,
                path: file.path
            )
            }
        }
        .onChange(of: copyAndMoveViewModel.state) { _, state in
            renameFromOriginalName = nil
            fileForSharing = nil
            
            switch state {
            case .renaming(originalName: let originalName):
                renameFromOriginalName = originalName
            case .readyToShare(display: let display, localFilePath: let path):
                fileForSharing = SharableFile(localFilePath: path, display: display)
            default: break
            }
        }
        .macOsCompatibleSheet(item: $renameFromOriginalName) { name in
            MenuValueInput(
                title: String(format: "file_manager___file_menu___rename_input_title"~, name),
                value: name,
                hint: "file_manager___file_menu___rename_input_hint"~,
                confirmButton: "file_manager___file_menu___rename_action"~,
                keyboardType: .normal,
                validator: { $0.isEmpty ? "error_please_enter_a_value"~ : nil },
                action: { name, _, _ in copyAndMoveViewModel.completeRename(newName: name) },
                cancelled: { copyAndMoveViewModel.cancel() }
            )
        }
        .sheet(
            item: $fileForSharing,
            onDismiss: { copyAndMoveViewModel.cancel() }
        ) { file in
            ShareSheet(activityItems: [URL(filePath: file.localFilePath)]) { _, _ , _, _ in }
        }
    }
}

private struct SharableFile : Identifiable, Equatable {
    var id: String { localFilePath }
    var localFilePath: String
    var display: String
}

private struct SharedFileManagerViewModelState: View {
    
    var loading: Bool = false
    var text: String = ""
    var canPaste: Bool = false
    var path: String
    @Environment(\.instanceId) private var instanceId
    @EnvironmentObject private var copyAndMoveViewModel: FileActionsViewModel

    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            if loading {
                ProgressView()
                
                Text(text)
                    .foregroundColor(OctoTheme.colors.textColoredBackground)
                    .typographySubtitle()
                    .lineLimit(1)
                    .id("progress-text")
            } else {
                Text(text)
                    .foregroundColor(OctoTheme.colors.textColoredBackground)
                    .typographyLabel()
                    .lineLimit(2)
                    .multilineTextAlignment(.leading)
            }
            
            Spacer()
            
            
            HStack(spacing: -OctoTheme.dimens.margin12) {
                if canPaste {
                    OctoButton(
                        text: "file_manager___file_list___paste_here"~,
                        type: .linkWithCustomColor(textColor: OctoTheme.colors.textColoredBackground),
                        small: true,
                        clickListener: { copyAndMoveViewModel.paste(instanceId: instanceId, path: path) }
                    )
                    .fixedSize()
                }
                
                if canPaste || loading {
                    OctoButton(
                        text: "file_manager___file_list___cancel_paste"~,
                        type: .linkWithCustomColor(textColor: OctoTheme.colors.textColoredBackground),
                        small: true,
                        clickListener: { copyAndMoveViewModel.cancel() }
                    )
                    .fixedSize()
                }
            }
            .padding(.trailing, -OctoTheme.dimens.margin1)
        }
        .foregroundColor(OctoTheme.colors.textColoredBackground)
        .tint(OctoTheme.colors.textColoredBackground)
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .padding([.top, .bottom], OctoTheme.dimens.margin1)
        .background(OctoTheme.colors.accent)
        .padding(.bottom, 1)
        .background(.ultraThinMaterial)
        .toolbarBackground(.hidden, for: .bottomBar)
    }
}

private struct FileListOuter: View {
    
    var file: FileObjectStruct
    @Binding var searchTerm: String
    var informAboutThumbnails: Bool
    var onDismissThumbnailInfo: () -> Void
    @Environment(\.isSearching) var isSearching
    
    var body: some View {
        if isSearching {
            FileSearchView(
                searchTerm: searchTerm
            )
        } else {
            FileListInner(
                file: file,
                informAboutThumbnails: informAboutThumbnails,
                onDismissThumbnailInfo: onDismissThumbnailInfo
            )
            .onChange(of: isSearching) { searchTerm = "" }
        }
    }
}


private struct FileListInner: View {
    var file: FileObjectStruct
    var informAboutThumbnails: Bool
    var onDismissThumbnailInfo: () -> Void
    
    var body: some View {
        List {
            FilesThumbnailAnnouncement(
                informAboutThumbnails: informAboutThumbnails,
                path: file.path,
                onDismissThumbnailInfo: onDismissThumbnailInfo
            )
            .transition(.opacity)
            
            if file.childFolders?.isEmpty == true && file.children?.isEmpty == true {
                FileListEmpty()
            } else {
                FileListSection(files: file.childFolders)
                FileListSection(files: file.children)
            }
        }
        .animation(.default, value: informAboutThumbnails)
        .animation(.default, value: file)
    }
}

struct FileListEmpty : View {

    @Environment(\.instanceSystemInfo) var systemInfo
    
    var body: some View {
        VStack(spacing: 0) {
            Text(
                String(
                    format: "file_manager___file_list___no_files_on_octoprint_title"~,
                    systemInfo.interfaceType.label
                )
            )
            .multilineTextAlignment(.center)
            .lineLimit(2)
            .typographySubtitle()
            
            Text(
                LocalizedStringKey(
                    String(
                        format: "file_manager___file_list___no_files_on_octoprint_subtitle"~,
                        systemInfo.interfaceType.label
                    )
                )
            )
            .multilineTextAlignment(.center)
            .typographyLabel()
            .lineLimit(5)
            .padding(.top, OctoTheme.dimens.margin01)
        }
        .padding(OctoTheme.dimens.margin12)
        .padding(.top, OctoTheme.dimens.margin2)
    }
}

struct FileListSection : View {
    @Environment(\.openURL) var openUrl
    @Environment(\.instanceId) var instanceId
    @EnvironmentObject var viewModel: FileManagerViewModel
    var files: [FileObjectStruct]?
    
    var body: some View {
        if (files != nil && files?.isEmpty == false) {
            Section {
                ForEach(files ?? []) { f in
                    NavigationLink(
                        value: UriLibrary.shared.getFileManagerUri(instanceId: instanceId, path: f.path, label: f.display, folder: KotlinBoolean(bool: f.isFolder)).description(),
                        label: { FileListItem(file: f) }
                    )
                }
            }
        }
    }
}

struct FileListView_Previews: PreviewProvider {
        
    static var previews: some View {
        FileListView(file: PreviewData.folder, informAboutThumbnails: false, onDismissThumbnailInfo: {})
            .previewDisplayName("Normal")
        
        FileListView(file: PreviewData.folder, informAboutThumbnails: true, onDismissThumbnailInfo: {})
            .previewDisplayName("Thumbnail info")
    }
}
