//
//  FileCopyAndMoveViewModel.swift
//  OctoApp
//
//  Created by Christian Würthner on 24/04/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

class FileActionsViewModel: BaseViewModel {
    var currentCore: FileActionsViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: State = .idle
    
    func createCore(instanceId: String) -> FileActionsViewModelCore {
        FileActionsViewModelCore()
    }
    
    func publish(core: FileActionsViewModelCore) {
        core.state.asPublisher()
            .sink { (state: FileActionsViewModelCore.State) in
                if let state = state as? FileActionsViewModelCore.StateLoading {
                    self.state = .loading(display: state.display)
                } else if let state = state as? FileActionsViewModelCore.StateReadyToPaste {
                    self.state = .readyToPaste(display: state.display)
                } else if let state = state as? FileActionsViewModelCore.StateRenaming {
                    self.state = .renaming(originalName: state.originalName)
                } else if let state = state as? FileActionsViewModelCore.StateReadyToShare {
                    self.state = .readyToShare(display: state.display, localFilePath: state.localFilePath)
                }else {
                    self.state = .idle
                }
            }
            .store(in: &bag)
    }
    
    func copy(instanceId: String, path: String, display: String) {
        currentCore?.doCopy(instanceId: instanceId, path: path, display: display)
    }
    
    func cut(instanceId: String, path: String, display: String) {
        currentCore?.cut(instanceId: instanceId, path: path, display: display)
    }
    
    func prepareRename(instanceId: String, path: String, name: String) {
        currentCore?.prepareRename(instanceId: instanceId, path: path, name: name)
    }
    
    func completeRename(newName: String) {
        currentCore?.completeRename(newName: newName)
    }
    
    func download(instanceId: String, path: String, display: String, date: Date) {
        currentCore?.download(instanceId: instanceId,  display: display, path: path, date: date.toInstant())
    }
    
    func delete(instanceId: String, path: String) {
        currentCore?.delete(instanceId: instanceId, path: path)
    }
    
    func paste(instanceId: String, path: String) {
        currentCore?.paste(instanceId: instanceId, path: path)
    }
    
    func cancel() {
        currentCore?.cancel()
    }
    
    func clearData() {
        state = .idle
    }
}

extension FileActionsViewModel {
    enum State: Equatable {
        case idle
        case loading(display: String)
        case readyToPaste(display: String)
        case renaming(originalName: String)
        case readyToShare(display: String, localFilePath: String)
    }
}
