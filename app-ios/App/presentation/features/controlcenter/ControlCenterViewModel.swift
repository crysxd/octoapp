//
//  ControlCenterViewModel.swift
//  OctoApp
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
import OctoAppBase

class ControlCenterViewModel : BaseViewModel {
    var currentCore: ControlCenterViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: State = State(enabled: false, instances: [])
    
    func createCore(instanceId: String) -> ControlCenterViewModelCore {
        return ControlCenterViewModelCore()
    }
    
    func publish(core: ControlCenterViewModelCore) {
        state = State(core.stateSnapshot)
        core.state.asPublisher()
            .sink { (state: ControlCenterViewModelCore.State) in
                self.state = State(state)
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = State(enabled: false, instances: [])
    }
    
    func activate(instanceId: String) {
        currentCore?.activate(instanceId: instanceId)
    }
}

extension ControlCenterViewModel {
    
    struct State : Equatable {
        var enabled: Bool
        var instances: [ControlCenterInstance]
        var activeInstanceId: String?
    }
    
    struct ControlCenterInstance : Identifiable, Equatable {
        var id: String { instanceId }
        var label: String
        var instanceId: String
        var colors: ColorsStruct
        var status: String
        var progress: Float?
        var advertiseCompanion: Bool
        var eta: TimeInterval?
        var snapshot: Image?
        var active: Bool
    }
}

fileprivate extension ControlCenterViewModel.State {
    init(_ other: ControlCenterViewModelCore.State) {
        self.init(
            enabled: other.enabled,
            instances: other.instances.map {
                ControlCenterViewModel.ControlCenterInstance($0, activeInstanceId: other.activeInstanceId)
            },
            activeInstanceId: other.activeInstanceId
        )
    }
}

fileprivate extension ControlCenterViewModel.ControlCenterInstance {
    init(_ other: ControlCenterViewModelCore.ControlCenterInstance, activeInstanceId: String?) {
        self.init(
            label: other.label,
            instanceId: other.instanceId,
            colors: other.colors.toStruct(),
            status: other.status,
            progress: other.progress?.floatValue,
            advertiseCompanion: other.advertiseCompanion,
            eta: other.eta?.doubleValue,
            snapshot: other.snapshot.flatMap { Image(uiImage: $0) },
            active: other.instanceId == activeInstanceId
        )
    }
}
