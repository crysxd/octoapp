//
//  ControlCenter.swift
//  OctoApp
//
//  Created by Christian Würthner on 24/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import SwiftUIIntrospect


struct ControlCenterContent: View {
    
    var state: ControlCenterViewModel.State
    var onActivate: (_ instanceId: String) -> Void
    var onClose: () -> Void
    
    @State private var showMenu = false
    @Namespace private var animation
    
    var subtitle: LocalizedStringKey {
        LocalizedStringKey(
            String(
                format: "control_center___subtitle"~,
                UriLibrary.shared.getFaqUri(faqId: "multiprinter")
            )
        )
    }
    
    var body: some View {
        NavigationStack {
            instances
                .onTapGesture { onClose() }
                .toolbar(.visible, for: .bottomBar)
                .toolbar {
                    ToolbarItem(placement: .cancellationAction) {
                        header
                            .padding([.leading, .trailing], -20)
                            .padding([.bottom, .bottom], 20)
                    }
                    
                    ToolbarItem(placement: .bottomBar) {
                        footer
                    }
                }
                .macOsCompatibleSheet(isPresented: $showMenu) {
                    MenuHost(menu: ManageOctoPrintInstancesMenu())
                }
        }
        .introspect(.navigationStack, on: .iOS(.v16, .v17, .v18)) { scrollView in
            DispatchQueue.main.async {
                scrollView.viewControllers.forEach { controller in
                    controller.view.backgroundColor = .clear
                }
            }
        }
        .webcamFullscreenDragToDismiss { onClose() }
        .background(.ultraThinMaterial)
        .animation(.default, value: state.instances.map { $0.id })
    }
    
    var header: some View {
        HStack(spacing: OctoTheme.dimens.margin2) {
            Text("control_center___title"~)
                .fixedSize()
                .multilineTextAlignment(.leading)
                .frame(maxWidth: .infinity)
                .typographyTitle()
                .padding(.leading, OctoTheme.dimens.margin1)
            
            // Stupid stuff to make it fill width?
            ForEach((1...5).reversed(), id: \.self) { _ in
                Text("control_center___title"~)
                    .multilineTextAlignment(.leading)
                    .frame(maxWidth: .infinity)
                    .typographyTitle()
                    .opacity(0)
            }
            
            OctoIconButton(
                icon: "xmark",
                color: OctoTheme.colors.darkText,
                clickListener: onClose
            )
            .background(Circle().fill(.regularMaterial))
        }
        .padding(OctoTheme.dimens.margin12)
        .padding(.top, OctoTheme.dimens.margin3)
        .frame(maxWidth: .infinity)
    }
    
    var instances: some View {
        GeometryReader { geometry in
            ScrollView {
                VStack(
                    alignment: .center,
                    spacing: OctoTheme.dimens.margin1
                ) {
                    ForEach(state.instances) { instance in
                        ControlCenterInstance(
                            instance: instance,
                            onClick: {
                                onActivate(instance.id)
                                try? await Task.sleep(for: .seconds(1))
                                onClose()
                            }
                        )
                        .matchedGeometryEffect(id: instance.id, in: animation)
                    }
                }
                .frame(maxWidth: .infinity, minHeight: geometry.size.height - OctoTheme.dimens.margin12 * 2)
                .padding(OctoTheme.dimens.margin12)
            }
            .scrollBounceBehavior(.basedOnSize, axes: .vertical)
        }
    }
    
    var footer : some View {
        OctoButton(
            text: "control_center___manage_menu___title"~,
            small: true,
            clickListener: { showMenu = true }
        )
        .padding(OctoTheme.dimens.margin12)
    }
}

private extension View {

    func transparentFullScreenCover<Content: View>(isPresented: Binding<Bool>, content: @escaping () -> Content) -> some View {
        fullScreenCover(isPresented: isPresented) {
            ZStack {
                content()
            }
            .background(TransparentBackground())
        }
    }
}

private struct TransparentBackground: UIViewRepresentable {

    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        DispatchQueue.main.async {
            view.superview?.superview?.backgroundColor = .clear
        }
        return view
    }

    func updateUIView(_ uiView: UIView, context: Context) {}
}


private struct ControlCenter: ViewModifier {
    
    @StateObject private var viewModel: ControlCenterViewModel = ControlCenterViewModel()
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    
    func body(content: Content) -> some View {
        content.transparentFullScreenCover(isPresented: $orchestrator.controlCenterShown) {
            controlCenter
        }
    }
    
    @ViewBuilder
    var controlCenter: some View {
        if orchestrator.controlCenterShown {
            ControlCenterContent(
                state: viewModel.state,
                onActivate: { viewModel.activate(instanceId: $0) },
                onClose: { orchestrator.controlCenterShown = false }
            )
            .usingViewModel(viewModel, fixedInstanceId: "")
            .transition(.move(edge: .bottom).combined(with: .opacity))
            .zIndex(10000)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

extension View {
    func controlCenter() -> some View {
        modifier(ControlCenter())
    }
}

struct ControlCenter_Previews: PreviewProvider {
    static var previews: some View {
        ControlCenterContent(
            state: ControlCenterViewModel.State(
                enabled: true,
                instances: [
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Beagle",
                        instanceId: "b",
                        colors: previewColorsStruct,
                        status: "CE3PRO_Ananas.gcode",
                        progress: 42,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: Image("WebcamTest"),
                        active: true
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "c",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    )
                ]
            ),
            onActivate: { _ in },
            onClose: {}
        )
        .previewDisplayName("Few")
        
        ControlCenterContent(
            state: ControlCenterViewModel.State(
                enabled: true,
                instances: [
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Beagle",
                        instanceId: "a",
                        colors: previewColorsStruct,
                        status: "CE3PRO_Ananas.gcode",
                        progress: 42,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: Image("WebcamTest"),
                        active: true
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "b",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "c",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "x",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "d",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "e",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "f",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "g",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "h",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    ),
                    ControlCenterViewModel.ControlCenterInstance(
                        label: "Corgi",
                        instanceId: "i",
                        colors: previewColorsStruct,
                        status: "No data",
                        progress: nil,
                        advertiseCompanion: false,
                        eta: 3246223,
                        snapshot: nil,
                        active: false
                    )
                ]
            ),
            onActivate: { _ in },
            onClose: {}
        )
        .previewDisplayName("Many")
    }
}
