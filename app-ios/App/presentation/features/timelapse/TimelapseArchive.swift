//
//  TimelapseArchive.swift
//  OctoApp
//
//  Created by Christian on 10/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import OctoAppBase
import SwiftUI
import UIKit
import AVKit

struct TimelapseArchive: View {
    
    @StateObject var viewModel = TimelapseArchiveViewModel()
    @State var menuFile: TimelapseFileStruct? = nil
    @State var sharePath: String? = nil
    @State var playerPath: String? = nil
    @Environment(\.instanceId) var instanceId

    var body: some View {
        TimelapseArchiveContent(
            state: viewModel.state,
            action: { file in menuFile = file },
            reload: { await viewModel.reload() }
        )
        .usingViewModel(viewModel)
        .macOsCompatibleSheet(item: $menuFile) { file in
            menu(file: file)
        }
        .macOsCompatibleSheet(item: $sharePath) { path in
            let videoURL = URL(fileURLWithPath: path)
            ActivityViewController(activityItems: [videoURL], subject: "Video")
                .ignoresSafeArea(.all)
        }
        .fullScreenCover(item: $playerPath) { path in
            NavigationStack {
                let url = URL(fileURLWithPath: path)
                let player = AVPlayer(url: url)
                VideoPlayer(player: player)
                    .ignoresSafeArea(.all)
                    .onAppear { player.play() }
                    .toolbarDoneButton()
            }
        }
    }
    
    func menu(file: TimelapseFileStruct) -> some View {
        MenuHost(
            menu: TimelapseArchiveMenu(
                instanceId: instanceId,
                timelapseFile: file.toClass()
            )
        ) { result in
            menuFile = nil
            
            if let path = TimelapseArchiveMenu.companion.getPlayPath(result: result) {
                playerPath = path
                sharePath = nil
            } else if let path = TimelapseArchiveMenu.companion.getSharePath(result: result) {
                sharePath = path
                playerPath = nil
            } else if let path = TimelapseArchiveMenu.companion.getExportPath(result: result) {
                UISaveVideoAtPathToSavedPhotosAlbum(path, nil, nil, nil)
            }
        }
        .onAppear {
            sharePath = nil
            playerPath = nil
        }
    }
}

private struct TimelapseArchiveContent : View {
    
    var state: FlowStateStruct<TimelapseArchiveViewModel.State>
    var action: (TimelapseFileStruct) -> Void
    var reload: @Sendable () async -> Void
    
    var body: some View {
        ZStack {
            switch state {
            case .loading:  ProgressView()
            case .error(let e): ScreenError(
                error: e,
                canDismiss: false,
                onRetry: {
                    Task { await reload() }
                }
            )
            .padding(OctoTheme.dimens.margin12)
            .surface()
            .padding(OctoTheme.dimens.margin12)
            case .ready(let s): TimelapseArchivList(
                unrendered: s.unrendered,
                rendered: s.rendered,
                action: action,
                reload: reload
            )
            }
        }
        .navigationTitle("timelapse_archive___title"~)
    }
}

private struct TimelapseArchivList : View {
 
    var unrendered: [TimelapseFileStruct]
    var rendered: [TimelapseFileStruct]
    var action: (TimelapseFileStruct) -> Void
    var reload: @Sendable () async -> Void

    var body: some View {
        if unrendered.isEmpty && rendered.isEmpty {
            empty
        } else {
            list
        }
    }
    
    var empty: some View {
        ScreenError(
            title: "timelapse_archive___no_timelapse_found"~,
            canDismiss: true
        )
        .padding(OctoTheme.dimens.margin12)
    }
    
    var list: some View {
        List {
            unrenderedSection
            renderedSection
        }
        .refreshable(action: reload)
        .animation(.default, value: unrendered)
        .animation(.default, value: rendered)
    }
    
    var unrenderedSection : some View {
        Section {
            ForEach(unrendered) { file in
                TimelapseItem(file: file) { action(file) }
            }
        }
    }
    
    var renderedSection : some View {
        Section {
            ForEach(rendered) { file in
                TimelapseItem(file: file) { action(file) }
            }
        }
    }
}

struct TimelapseArchive_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            TimelapseArchiveContent(
                state: .loading,
                action: { _ in },
                reload: {}
            )
        }.previewDisplayName("Loading")
        
        NavigationStack {
            TimelapseArchiveContent(
                state: .error(KotlinThrowable(message: "Some")),
                action: { _ in },
                reload: {}
            )
        } .previewDisplayName("Error")
        
        NavigationStack {
            TimelapseArchiveContent(
                state: .ready(
                    TimelapseArchiveViewModel.State(
                        unrendered: [],
                        rendered: []
                    )
                ),
                action: { _ in },
                reload: {}
            )
        }
        .previewDisplayName("Empty")
        
        NavigationStack {
            TimelapseArchiveContent(
                state: .ready(
                    TimelapseArchiveViewModel.State(
                        unrendered: [
                            TimelapseFileStruct(
                                name: "CE3_Body4_20220522102406.mp4",
                                bytes: 3284633,
                                date: Date(milliseconds: 3267234628),
                                downloadPath: nil,
                                thumbnail: nil,
                                processing: false,
                                rendering: false,
                                recording: true
                            )
                        ],
                        rendered: [
                            TimelapseFileStruct(
                                name: "CE3PRO_PlantPot_SquareHoles_drainHoles.gcode_20220522102406.mp4",
                                bytes: 3284633,
                                date: Date(milliseconds: 3267234628),
                                downloadPath: nil,
                                thumbnail: nil,
                                processing: false,
                                rendering: false,
                                recording: false
                            ),
                            TimelapseFileStruct(
                                name: "CE3_Body4_20220522102406.mp4",
                                bytes: 32899633,
                                date: Date(milliseconds: 3267233383),
                                downloadPath: nil,
                                thumbnail: nil,
                                processing: false,
                                rendering: false,
                                recording: false
                            ),
                            TimelapseFileStruct(
                                name: "CE3_Body4_20220522102406.mp4",
                                bytes: 32633,
                                date: Date(milliseconds: 3267223383),
                                downloadPath: nil,
                                thumbnail: nil,
                                processing: false,
                                rendering: false,
                                recording: false
                            )
                        ]
                    )
                ),
                action: { _ in },
                reload: { try? await Task.sleep(for: .seconds(3))}
            )
        }
        .previewDisplayName("Data")
    }
}
