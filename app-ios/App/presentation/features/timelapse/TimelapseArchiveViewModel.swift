//
//  TimelapseArchiveViewModel.swift
//  OctoApp
//
//  Created by Christian on 11/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase


class TimelapseArchiveViewModel : BaseViewModel {
    var currentCore: TimelapseViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: FlowStateStruct<State> = .loading
    
    func createCore(instanceId: String) -> TimelapseViewModelCore {
        return TimelapseViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: TimelapseViewModelCore) {
        core.state.asPublisher()
            .sink { (flowState: FlowState<TimelapseViewModelCore.State>) in
                self.state = FlowStateStruct(flowState) { s in
                    TimelapseArchiveViewModel.State(
                        unrendered: s.unrendered.map { $0.toStruct() },
                        rendered: s.rendered.map { $0.toStruct() }
                    )
                }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = .loading
    }
    
    func reload() async {
        try? await currentCore?.reload()
    }
}

extension TimelapseArchiveViewModel {
    struct State : Equatable{
        var unrendered: [TimelapseFileStruct]
        var rendered: [TimelapseFileStruct]
    }
}
