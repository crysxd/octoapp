//
//  SignInVie.swift
//  App
//
//  Created by Christian on 09/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct SignInView: View {
    
    var repairForInstanceId: String? = nil
    @StateObject var viewModel = SiginInViewModel()
    @Environment(\.dismissToRoot) private var dismiss
    
    var body: some View {
        LargeScreenStage {
            ZStack {
                mainContent
                cancelButton
                successForeground
            }
            .background(successBackground)
            .transition(.opacity)
            .onAppear {
                viewModel.start(repairForInstanceId: repairForInstanceId)
            }
        }
    }
    
    @ViewBuilder
    var mainContent: some View {
        GeometryReader { geometry in
            ScrollView(showsIndicators: false) {
                VStack {
                    SignInHeader(
                        state: viewModel.state
                    )
                    .transition(.slide)
                    .padding([.bottom], OctoTheme.dimens.margin2)
                    
                    if let discover = viewModel.state as? SignInViewModelCore.StateSplash {
                        SignInViewSplash(state: discover)
                            .transition(.opacity)
                    } else if let discover = viewModel.state as? SignInViewModelCore.StateDiscover {
                        SignInViewDiscover(
                            state: discover,
                            onContinueWithUrl: { webUrl, type, confirmed in
                                if let type = type, confirmed {
                                    viewModel.moveToState(
                                        state: SignInViewModelCore.StateAccessRequest(
                                            url: UrlExtKt.toUrl(webUrl),
                                            reusedInstanceId: nil,
                                            loginUrl: nil,
                                            type: type
                                        )
                                    )
                                } else {
                                    viewModel.moveToState(
                                        state: SignInViewModelCore.StateProbe(
                                            urlString: webUrl,
                                            apiKey: nil,
                                            reusedInstanceId: nil,
                                            finding: nil
                                        )
                                    )
                                }
                            },
                            onContinuePrevious: { config in
                                viewModel.activate(config: config)
                            },
                            onDeletePrevious: { configId in
                                viewModel.delete(configId: configId)
                            }
                        ).transition(.opacity)
                    } else if let probe = viewModel.state as? SignInViewModelCore.StateProbe {
                        SignInViewProbe(
                            state: probe,
                            onTryWithUrl: { webUrl in
                                viewModel.moveToState(
                                    state: SignInViewModelCore.StateProbe(
                                        urlString: webUrl,
                                        apiKey: probe.apiKey,
                                        reusedInstanceId: probe.reusedInstanceId,
                                        finding: nil
                                    )
                                )
                            },
                            onReturnToDiscover: {
                                viewModel.moveToState(
                                    state: SignInViewModelCore.StateDiscover(
                                        services: [],
                                        configs: [],
                                        hasQuickSwitch: true
                                    )
                                )
                            }
                        ).transition(.opacity)
                    } else if let accessRquest = viewModel.state as? SignInViewModelCore.StateAccessRequest {
                        SignInViewAccessRequest(
                            state: accessRquest,
                            onReturnToDiscover: {
                                viewModel.moveToState(
                                    state: SignInViewModelCore.StateDiscover(
                                        services: [],
                                        configs: [],
                                        hasQuickSwitch: true
                                    )
                                )
                            },
                            onContinueWithApiKey: { apiKey in
                                viewModel.moveToState(
                                    state: SignInViewModelCore.StateProbe(
                                        urlString: accessRquest.url.description(),
                                        apiKey: apiKey,
                                        reusedInstanceId: accessRquest.reusedInstanceId,
                                        finding: nil
                                    )
                                )
                            }
                        ).transition(.opacity)
                    } else if let accessRquestFailed = viewModel.state as? SignInViewModelCore.StateAccessRequestFailed {
                        SignInViewAccessRequestFailed(
                            state: accessRquestFailed,
                            onRetry: {
                                viewModel.moveToState(
                                    state: SignInViewModelCore.StateAccessRequest(
                                        url: accessRquestFailed.url,
                                        reusedInstanceId: accessRquestFailed.reusedInstanceId,
                                        loginUrl: nil,
                                        type: accessRquestFailed.type
                                    )
                                )
                            }
                        ).transition(.opacity)
                    }
                }
                .padding(OctoTheme.dimens.margin2)
                .frame(width: geometry.size.width)
                .frame(minHeight: geometry.size.height)
                .padding([.bottom], OctoTheme.dimens.margin2)
                .animation(.easeInOut, value: viewModel.state)
            }
        }
    }
    
    @ViewBuilder
    var successBackground: some View {
        if viewModel.state is SignInViewModelCore.StateSuccess {
            SignInViewSuccessBackground()
        }
    }
    
    @ViewBuilder
    var successForeground: some View {
        if let success = viewModel.state as? SignInViewModelCore.StateSuccess {
            SignInViewSuccessForeground {
                viewModel.activate(config: success.config)
                dismiss()
            }
        }
    }
    
    @ViewBuilder
    var cancelButton: some View {
        // Only show cancel button while probing without a finding yet and not if we repair an instance as then we have a "Done" button in the toolbar
        if let state = viewModel.state as? SignInViewModelCore.StateProbe, repairForInstanceId == nil {
            VStack {
                Spacer()
                if state.finding == nil {
                    OctoButton(text: "cancel"~, type: .secondary) {
                        viewModel.moveToState(state: SignInViewModelCore.StateDiscover(services: [], configs: [], hasQuickSwitch: false))
                    }
                    .transition(.move(edge: .bottom).combined(with: .opacity))
                    .padding(OctoTheme.dimens.margin12)
                }
            }
            .animation(.default, value: state.finding == nil)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

class SiginInViewModel : ObservableObject {
    // Disallow network in splash screen so we don't have the local network pop up right away
    private let core = SignInViewModelCore(allowNetworkInSplash: false)
    var bag:Set<AnyCancellable> = []
    
    @Published var state: SignInViewModelCore.State = SignInViewModelCore.StateInitial.shared
    
    init() {
        core.state.asPublisher()
            .sink { result in self.state = result }
            .store(in: &bag)
    }
    
    func moveToState(state: SignInViewModelCore.State) {
        core.moveToState(state: state)
    }
    
    func activate(config: PrinterConfigurationV3) {
        core.activate(config: config)
    }
    
    func delete(configId: String) {
        core.delete(configId: configId)
    }
    
    func start(repairForInstanceId: String?) {
        core.start(repairForInstanceId: repairForInstanceId)
    }
}

struct SignInVie_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
