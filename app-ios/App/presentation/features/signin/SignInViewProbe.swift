//
//  SignInViewProbe.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import MarkdownUI
import OctoAppBase

struct SignInViewProbe: View {
    
    var state: SignInViewModelCore.StateProbe
    var onTryWithUrl: (String) -> Void
    var onReturnToDiscover: () -> Void = {}

    private let library = PrinterFindingDescriptionLibrary()
    @State private var username = ""
    @State private var password = ""
    @State private var notificationsAllowed = false
    @State private var showingManual = false
    
    var body: some View {
        if let finding = state.finding {
            VStack {
                VStack(spacing: OctoTheme.dimens.margin1) {
                    Text(LocalizedStringKey(library.getExplainerForFinding(finding: finding)))
                        .typographyBase()
                        .fixedSize(horizontal: false, vertical: true)
                    
                    if finding is TestFullNetworkStackUseCase.FindingBasicAuthRequired {
                        OctoTextField(
                            placeholder: "sign_in___probe___basic_username"~,
                            labelActiv: "sign_in___probe___basic_username",
                            alternativeBackgroung: true,
                            input: $username,
                            autocorrectionDisabled: true
                        )
                        .padding([.top], OctoTheme.dimens.margin2)
                        .keyboardType(.default)
                        OctoTextField(
                            placeholder: "sign_in___probe___basic_password"~,
                            labelActiv: "sign_in___probe___basic_password",
                            alternativeBackgroung: true,
                            input: $password,
                            autocorrectionDisabled: true
                        )
                        .keyboardType(.default)
                    }
                }
                .padding(OctoTheme.dimens.margin2)
                .onAppear {
                    UNUserNotificationCenter.current().getNotificationSettings { settings in
                        notificationsAllowed = settings.authorizationStatus == .authorized
                    }
                }
                .surface()
                .padding([.top], OctoTheme.dimens.margin2)
                
                if finding is TestFullNetworkStackUseCase.FindingBasicAuthRequired {
                    OctoButton(text: "sign_in___continue"~) {
                        onTryWithUrl(finding.webUrl?.withBasicAuth(user: username, password: password).description() ?? "")
                    }
                    .padding([.top], OctoTheme.dimens.margin3)
                } else {
                    OctoButton(text: "sign_in___try_again"~) {
                        onTryWithUrl(finding.webUrl?.description() ?? "")
                    }
                    .padding([.top], OctoTheme.dimens.margin3)
                }

                if !(finding is TestFullNetworkStackUseCase.FindingBasicAuthRequired) {
                    OctoButton(
                        text: "sign_in___probe___edit_information"~,
                        type: .link,
                        small: true
                    ) {
                        self.showingManual = true
                    }
                    .padding([.top], OctoTheme.dimens.margin12)
                    .macOsCompatibleSheet(isPresented: $showingManual) {
                        SignInViewManual(input: finding.webUrl?.withoutBasicAuth().description() ?? ""){ webUrl in
                            onTryWithUrl(webUrl)
                        }
                    }

                    OctoButton(
                        text: "sign_in___cancel_and_use_other_information_title"~,
                        type: .link,
                        small: true
                    ) {
                        if state.apiKey != nil {
                            SharedBaseInjector.shared.getOrNull()?.printerConfigRepository.clearActive()
                        } else {
                            onReturnToDiscover()
                        }
                    }
                }
            }
        } else {
            ProgressView()
        }
    }
}

private struct SignInViewProbePreview: View {
    
    var state: SignInViewModelCore.StateProbe
    
    var body: some View {
        ScrollView {
            VStack {
                SignInHeader(state: state)
                SignInViewProbe(
                    state: state,
                    onTryWithUrl: { _ in },
                    onReturnToDiscover: {}
                )
            }
        }
    }
}

struct SignInViewProbe_Previews: PreviewProvider {
    static var previews: some View {
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil, reusedInstanceId: nil,finding: nil))
            .previewDisplayName("Probing")
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil,reusedInstanceId: nil, finding: TestFullNetworkStackUseCase.FindingHostNotReachable(webUrl: UrlExtKt.toUrl("http://octopi.local"), host: "octopi.local", ip: "192.168.1.3", timeoutMs: 2000)))
            .previewDisplayName("Not reachable")
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil,reusedInstanceId: nil, finding: TestFullNetworkStackUseCase.FindingDnsFailure(webUrl: UrlExtKt.toUrl("http://octopi.local"), host: "octopi.local")))
            .previewDisplayName("DNS")
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil,reusedInstanceId: nil, finding: TestFullNetworkStackUseCase.FindingPortClosed(webUrl: UrlExtKt.toUrl("http://octopi.local"), host: "octopi.local", port: 80)))
            .previewDisplayName("Port closed")
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil,reusedInstanceId: nil, finding: TestFullNetworkStackUseCase.FindingServerIsNotOctoPrint(webUrl: UrlExtKt.toUrl("http://octopi.local"), host: "octopi.local")))
            .previewDisplayName("Not OctoPrint")
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil,reusedInstanceId: nil, finding: TestFullNetworkStackUseCase.FindingBasicAuthRequired(webUrl: UrlExtKt.toUrl("http://octopi.local"), host: "octopi.local", userRealm: "Please sign in")))
            .previewDisplayName("Basic Auth")
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil,reusedInstanceId: nil, finding: TestFullNetworkStackUseCase.FindingHttpsNotTrusted(webUrl: UrlExtKt.toUrl("http://octopi.local"), host: "octopi.local", certificate: nil, weakHostnameVerificationRequired: false)))
            .previewDisplayName("SSL")
            .previewDisplayName("Klipper")
        SignInViewProbePreview(state: SignInViewModelCore.StateProbe(urlString: "http://octopi.local", apiKey: nil,reusedInstanceId: nil, finding: TestFullNetworkStackUseCase.FindingUnexpectedIssue(webUrl: UrlExtKt.toUrl("http://octopi.local"), exception: KotlinThrowable())))
            .previewDisplayName("Unexpected")
    }
}
