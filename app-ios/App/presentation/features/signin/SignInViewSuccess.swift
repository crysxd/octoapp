//
//  SignInViewSuccess.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct SignInViewSuccessBackground: View {
    
    @State private var animation = ""
    
    var body: some View {
        ZStack {
            if(!animation.isEmpty) {
                SimpleLottieView(lottieFile: animation)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .scaledToFill()
                    .ignoresSafeArea(.all)
                    .blur(radius: 3)
                    .padding(-OctoTheme.dimens.margin3)
                    .opacity(0.8)
            }
        }
        .task {
            try? await Task.sleep(for: .seconds(2.5))
            animation = "confetti"
        }
    }
}

struct SignInViewSuccessForeground: View {
    
    @State private var buttonVisible = false
    var onContinue: () -> Void
    
    var body: some View {
        VStack {
            Spacer()
            OctoButton(text:"sign_in___continue"~) {
                OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventLogin(), params: [:])
                OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventSignInSuccess(), params: [:])
                onContinue()
            }
            .opacity(buttonVisible ? 1 : 0)
            .transition(.opacity)
        }
        .padding(OctoTheme.dimens.margin2)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .zIndex(1000)
        .animation(.default, value: buttonVisible)
        .task {
            try? await Task.sleep(for: .seconds(4))
            buttonVisible = true
        }
        .onDisappear {
            buttonVisible = true
        }
    }
}

struct SignInViewSuccess_Previews: PreviewProvider {
    static var previews: some View {
        
        ZStack {
            SignInViewSuccessBackground()
            VStack {
                SignInHeader(state: SignInViewModelCore.StateInitial())
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding(OctoTheme.dimens.margin2)
            .background(.red.opacity(0.2))
            SignInViewSuccessForeground { }
        }
    }
}
