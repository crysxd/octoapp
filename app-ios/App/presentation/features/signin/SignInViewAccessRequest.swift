//
//  SwiftUIView.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import MarkdownUI
import OctoAppBase
import SafariServices

struct SignInViewAccessRequest: View {
    
    var state: SignInViewModelCore.StateAccessRequest
    var onReturnToDiscover : () -> Void
    var onContinueWithApiKey: (String) -> Void
    
    var body: some View {
        VStack {
            if state.type == BackendType.octoprint {
                SignInViewAccessRequestOctoPrint(
                    state: state
                )
            } else {
                SignInViewAccessRequestMoonraker(
                    state: state,
                    onContinueWithApiKey: onContinueWithApiKey
                )
            }
            
            OctoButton(text: "sign_in___probe___edit_information"~, type: .link) {
                onReturnToDiscover()
            }
            .padding([.top], OctoTheme.dimens.margin01)
        }
    }
}

private struct SignInViewAccessRequestOctoPrint : View {
    
    var state: SignInViewModelCore.StateAccessRequest
    @State private var showingSafari = false
    
    var body: some View {
        VStack {
            VStack {
                Text("sign_in___access___explainer")
                    .padding([.top, .leading,.trailing], OctoTheme.dimens.margin2)
                    .typographyBase()
                    .frame(maxWidth: .infinity)
                    .multilineTextAlignment(.center)
                
                SignInPlayer(videoName: "access_explainer", videoExtension: "mp4")
                    .surface(color: .alternative)
                    .aspectRatio(16/9, contentMode: .fit)
                    .padding(OctoTheme.dimens.margin01)
            }
            .surface()
            
            OctoButton(text: "sign_in___access___open_web"~) {
                showingSafari = true
            }
            .padding([.top], OctoTheme.dimens.margin2)
            .macOsCompatibleSheet(isPresented: $showingSafari) {
                SafariView(url: state.loginUrl ?? state.url)
            }
        }
    }
}

private struct SignInViewAccessRequestMoonraker : View {
    
    var state: SignInViewModelCore.StateAccessRequest
    @State private var apiKey: String = ""
    var onContinueWithApiKey: (String) -> Void
    
    var body: some View {
        VStack {
            VStack {
                Markdown(
                    String(
                        format: "sign_in___probe_finding___explainer_invalid_api_key_moonraker"~,
                        state.urlString ?? ""
                    )
                )
                .typographyBase()
                .frame(maxWidth: .infinity)
                
                OctoTextField(
                    placeholder: "API key",
                    labelActiv: "API key",
                    alternativeBackgroung: true,
                    input: $apiKey
                )
                .submitLabel(.continue)
                .onSubmit {
                    onContinueWithApiKey(apiKey)
                }
            }
            .padding(OctoTheme.dimens.margin2)
            .surface()
            
            OctoButton(text: "sign_in___continue"~) {
                onContinueWithApiKey(apiKey)
            }
            .padding([.top], OctoTheme.dimens.margin2)
        }
    }
}

struct SignInViewAccessRequestPreviews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            let state = SignInViewModelCore.StateAccessRequest(
                url: UrlExtKt.toUrl("http://octopi.local"),
                reusedInstanceId: nil,
                loginUrl: nil,
                type: .moonraker
            )
            VStack {
                SignInHeader(state: state)
                SignInViewAccessRequest(
                    state: state,
                    onReturnToDiscover: {},
                    onContinueWithApiKey: { _ in }
                )
            }
            .padding(OctoTheme.dimens.margin2)
        }
    }
}
