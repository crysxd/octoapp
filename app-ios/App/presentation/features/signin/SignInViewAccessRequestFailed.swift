//
//  SignInViewAccessRequestFailed.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

struct SignInViewAccessRequestFailed: View {
    
    var state: SignInViewModelCore.StateAccessRequestFailed
    var onRetry: () -> Void

    @EnvironmentObject private var orchestator: OrchestratorViewModel
    
    var body: some View {
        OctoButton(text: "retry_general"~, clickListener: onRetry)
            .padding([.top], OctoTheme.dimens.margin4)
        
        OctoButton(text: "show_details"~, type: .link) {
            orchestator.postError(error: state.exception, showDetailsDirectly: true)
        }
    }
}

struct SignInViewAccessRequestFailedPreviews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            let state = SignInViewModelCore.StateAccessRequestFailed(
                url: UrlExtKt.toUrl("http://octopi.local"),
                reusedInstanceId: nil,
                exception: KotlinThrowable(),
                type: .octoprint
            )
            VStack {
                SignInHeader(state: state)
                SignInViewAccessRequestFailed(state: state) {}
            }
        }
    }
}
