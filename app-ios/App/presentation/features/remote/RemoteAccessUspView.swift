//
//  RemoteAccessUsp.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct RemoteAccessUspView: View {
    let foreground: Color
    let background: Color
    let usps : [RemoteAccessUsp]
   
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            Text("configure_remote_acces___external_service_usp_title"~)
                .foregroundColor(foreground)
                .typographySectionHeader()
                .padding([.bottom, .top], OctoTheme.dimens.margin1)
            
            ForEach(usps) { usp in
                HStack(spacing: OctoTheme.dimens.margin1) {
                    Image(systemName: usp.systemImageName)
                        .foregroundColor(foreground.opacity(0.33))
                        .imageScale(.medium)
                        .frame(width: 24)
                    
                    Text(usp.description)
                        .foregroundColor(foreground.opacity(0.8))
                        .typographyBase()
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
            }
        }
        .padding(OctoTheme.dimens.margin12)
        .surface(color: .special(background))
    }
}

struct RemoteAccessUsp : Equatable, Identifiable {
    var id: String { return systemImageName }
    let systemImageName: String
    let description: String
    
}

struct RemoteAccessUsp_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessUspView(
            foreground: OctoTheme.colors.darkText,
            background: OctoTheme.colors.inputBackground,
            usps: [
                RemoteAccessUsp(
                    systemImageName: "checkmark.icloud.fill",
                    description: "configure_remote_acces___octoeverywhere___usp_1"~
                ),
                RemoteAccessUsp(
                    systemImageName: "video.fill",
                    description: "configure_remote_acces___octoeverywhere___usp_2"~
                ),
                RemoteAccessUsp(
                    systemImageName: "star.fill",
                    description: "configure_remote_acces___octoeverywhere___usp_3"~
                ),
                RemoteAccessUsp(
                    systemImageName: "lock.fill",
                    description: "configure_remote_acces___octoeverywhere___usp_4"~
                ),
            ]
        )
    }
}
