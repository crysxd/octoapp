//
//  BecomeSupporterView.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

//
//  SignInViewManual.swift
//  App
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct BecomeSupporterView: View {
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            
            OctoIconButton(icon: "xmark") { dismiss() }
                .padding(OctoTheme.dimens.margin1)
            
            Color.clear
        }
    }
}

struct BecomeSupporterView_Previews: PreviewProvider {
    static var previews: some View {
        BecomeSupporterView()
    }
}
