//
//  DarwinServiceDisacovery.swift
//  App
//
//  Created by Christian on 12/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import Network
import OctoAppBase

class DarwinNetworkDiscovery : NetworkServiceDiscovery {
    
    private var resolveBusy = false
    private var pendingResolve: [(domain: String, type: String, name: String, path: String, endpoint: NWEndpoint)] = []
    
    func discover(callback: @escaping (NetworkService) -> Void) async throws {
        let parameters = NWParameters()
        let  browser = NWBrowser(for: .bonjourWithTXTRecord(type: "_octoprint._tcp", domain: nil), using: parameters)
        browser.stateUpdateHandler = { newState in
            print("browser.stateUpdateHandler \(newState)")
        }
        
        
        browser.browseResultsChangedHandler = { results, changes in
            for result in results {
                var path: String = "/"
                switch result.metadata {
                case .bonjour(let txt): do {
                    path = txt.dictionary["path"] ?? path
                }
                default: break
                }
                
                switch result.endpoint {
                case .service(let name, let type, let domain, _):
                    print("[Bonjour] Discovered: \(name)")
                    self.resolve(domain: domain, type: type, name: name, path: path, endpoint: result.endpoint, callback: callback)
                    
                default: break
                }
            }
        }
        
        browser.start(queue: .main)
    }
    
    private func resolvePending(callback: @escaping (NetworkService) -> Void) {
        if !pendingResolve.isEmpty {
            let next = pendingResolve.removeFirst()
            resolve(domain: next.domain, type: next.type, name: next.name, path: next.path, endpoint: next.endpoint, callback: callback)
        }
    }
    
    private func resolve(domain: String, type: String, name: String, path: String, endpoint: NWEndpoint, callback: @escaping (NetworkService) -> Void) {
        if resolveBusy {
            print("[Bonjour] Resolve busy, adding to pending: \(name)")
            self.pendingResolve.append((domain: domain, type: type, name: name, path: path, endpoint: endpoint))
        } else {
            resolveBusy = true
            let service = NetService(domain: domain, type: type, name: name)
            BonjourResolver.resolve(service: service) { result in
                switch result {
                case .success((let hostName, let port)):
                    // Success! We are resovled and have a hostname like octopi.local
                    print("[Bonjour] Did resolve: \(hostName):\(port)")
                    
                    callback(
                        NetworkService(
                            label: name,
                            detailLabel: "\(hostName.replacing(".local.", with: ".local")):\(port)",
                            webUrl: "http://\(hostName):\(port)\(path)",
                            originQuality: 100,
                            origin: .dnssd,
                            type: .octoprint
                        )
                    )
                    
                    self.resolveBusy = false
                    self.resolvePending(callback: callback)
                    
                    
                case .failure(let error):
                    // Failure...fallback on NWConnection to resolve the IP
                    print("[Bonjour] Did not resolve \(name): \(error)")
                    self.resolve(name: name, path: path, endpoint: endpoint, callback: callback)
                    self.resolveBusy = false
                    self.resolvePending(callback: callback)
                }
            }
        }
    }
    
    
    private func resolve(name: String, path: String, endpoint: NWEndpoint, callback: @escaping (NetworkService) -> Void) {
        let connection = NWConnection(to: endpoint, using: .tcp)
        connection.stateUpdateHandler = { state in
            switch state {
            case .ready:
                if let innerEndpoint = connection.currentPath?.remoteEndpoint,
                   case .hostPort(let host, let port) = innerEndpoint {
                    switch host {
                    case .name(name, _):
                        print("[Bonjour] Backup resolve: \(name)")
                        
                    case .ipv4(let ip):
                        print("[Bonjour] Backup resolve IPv4", ip.debugDescription)
                        
                    case .ipv6(let ip):
                        print("[Bonjour] Backup resolve IPv6", ip.debugDescription)
                        
                    default:
                        print("[Bonjour] Backup resolve unknown")
                    }
                    
                    let h = host.debugDescription.components(separatedBy: "%")[0]
                    callback(
                        NetworkService(
                            label: name,
                            detailLabel: h,
                            webUrl: "http://\(h):\(port)\(path)",
                            originQuality: 100,
                            origin: NetworkService.Origin.dnssd,
                            type: .octoprint
                        )
                    )
                    
                    connection.cancel()
                }
                
            default:
                break
            }
        }
        
        connection.start(queue: .global())
    }
}
