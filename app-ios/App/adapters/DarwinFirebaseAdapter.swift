//
//  DarwinFirebaseAdapter.swift
//  OctoApp
//
//  Created by Christian on 27/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import FirebaseCrashlytics

class DarwinFirebaseAdapter : FirebaseAntiLogAdapter {
    
    func log(line: String) {
        Crashlytics.crashlytics().log(line)
    }
    
    func log(throwableClassName: String, stackTraceAddresses: [KotlinLong], throwable: KotlinThrowable) {
        let ex = ExceptionModel(name: throwableClassName, reason: throwable.message ?? "No message")
        ex.stackTrace = stackTraceAddresses.map { address in
            return StackFrame(address: address.uintValue)
        }
        Crashlytics.crashlytics().record(exceptionModel: ex)
    }
}
