//
//  DarwinMjpegSocket.swift
//  OctoApp
//
//  Created by Christian on 31/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import UIKit

class DarwinMjpegSocketCore: NSObject, URLSessionDataDelegate, MjpegSocketCore {
    
    private let tag = "DarwinMjpegSocketCore"
    private var session: URLSession? = nil
    private var receivedData: NSMutableData? = nil
    private var frameCallback: ((MjpegConnection3.MjpegSnapshotFrame) -> Void)? = nil
    private var completionCallback: ((Any?) -> Void)? = nil
    private var readStart: Date? = nil
    private var dropCount: Int32 = 0
    
    // MjpegSocketCore
    
    func startSession(
        url: String,
        extraHeaders: [String : String],
        onFrame: @escaping (MjpegConnection3.MjpegSnapshotFrame) -> Void,
        onCompletion: @escaping (Any?) -> Void
    ) async throws {
        // Create new session
        self.stopSession()
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        self.receivedData = NSMutableData()
        self.readStart = Date()
        self.dropCount = 0
        self.completionCallback = onCompletion
        self.frameCallback = onFrame
        
        // Parse URL
        guard let url = URL(string: url) else {
            Napier.e(tag: tag, message: "Unable to parse url string to URL")
            onCompletion(nil)
            return
        }
        
        // Request
        var request = URLRequest(url: url, timeoutInterval: 10)
        extraHeaders.forEach {
            request.addValue($0.value, forHTTPHeaderField: $0.key)
        }
        let dataTask = session?.dataTask(with: request)
        dataTask?.resume()
        Napier.d(tag: tag, message: "Resuming data task for \(url)")
        Napier.i(tag: tag, message: "==> GET \(url)")
    }
    
    func stopSession() {
        session?.invalidateAndCancel()
        receivedData = nil
        frameCallback = nil
        completionCallback = nil
    }
    
    // NSURLSessionDataDelegate
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        if let imageData = receivedData , imageData.length > 0,
           let image = UIImage(data: imageData as Data),
           let start = readStart {
            let analytics = MjpegConnection3.Analytics(
                readTime: (Date().millisecondsSince1970 - start.millisecondsSince1970),
                searchTime: 0,
                decodeTime: 0,
                dropCount: dropCount,
                byteCount: Int32(imageData.length),
                fps: 0
            )
            let frame = MjpegConnection3.MjpegSnapshotFrame(frame: image, analytics: analytics)
            frameCallback?(frame)
        } else {
            dropCount += 1
        }
        
        if dropCount > 4 {
            let message = "\(dropCount) consecutive frames invalid, cancelling"
            let error = NSError(
                domain: "MJPEG",
                code: -1,
                userInfo: [NSLocalizedDescriptionKey : message]
            )
            Napier.e(tag: tag, message: message)
            completionCallback?(error)
            session.invalidateAndCancel()
        }
        
        
        receivedData = NSMutableData()
        readStart = Date()
        completionHandler(.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        receivedData?.append(data)
        
        // Just a safe stop.
        if completionCallback == nil, frameCallback == nil {
            Napier.e(tag: tag, message: "Callbacks are gone!")
            session.invalidateAndCancel()
        }
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        Napier.d(tag: tag, message: "Did become invalid with error: \(String(describing: error))")
        completionCallback?(error)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        let url = task.originalRequest?.url?.formatted() ?? "???"
        let errorMessage = error?.localizedDescription ?? ""
        
        if let httpResponse = task.response as? HTTPURLResponse {
            Napier.i(tag: tag, message: "<== \(httpResponse.statusCode) \(url) \(errorMessage)")
            if httpResponse.statusCode != 200 {
                completionCallback?(PrinterApiException(httpUrl: UrlExtKt.toUrl(url), responseCode: Int32(httpResponse.statusCode), body: ""))
            } else {
                completionCallback?(error)
            }
        } else {
            Napier.i(tag: tag, message: "<== \(url) \(errorMessage)")
            completionCallback?(error)
        }
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        completionCallback?(nil)
        Napier.d(tag: tag, message: "Did finish events")
    }
}

fileprivate struct MjpegError: Error {
    var message: String
}
