//
//  DarwinNetworkManager.swift
//  OctoApp
//
//  Created by Christian on 31/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import Network

class DarwinConnectivityHelperCore : ConnectivityHelperCore {
    
    private static var instanceCounter = 0
    private let tag: String
    private let wifiMonitor = NWPathMonitor(requiredInterfaceType: .wifi)
    private let ethernetMonitor = NWPathMonitor(requiredInterfaceType: .wiredEthernet)
    
    private var wifiConnected : Bool? = false
    private var ethernetConnected : Bool? = false
    private var currentType: ConnectivityHelper.ConnectivityType {
        guard let wc = wifiConnected else {
            Napier.w(tag: self.tag, message: "Wifi state is unknown, assuming Unmetered")
            return .unmetered
        }
        guard let ec = ethernetConnected else {
            Napier.w(tag: self.tag, message: "Ethernet state is unknonw, relying on WiFi")
            return wc ? .unmetered : .metered
        }
        
        let state: ConnectivityHelper.ConnectivityType = (wc || ec) ? .unmetered : .metered
        Napier.d(tag: self.tag, message: "WiFi and Ethernet are known, state is now \(state)")
        return state
    }
    
    init() {
        DarwinConnectivityHelperCore.instanceCounter += 1
        tag = "DarwinConnectivityHelper@\(DarwinConnectivityHelperCore.instanceCounter)"
    }
    
    func startObserving(onChange: @escaping (ConnectivityHelper.ConnectivityType) -> Void) {
        Napier.i(tag: tag, message: "Start observing")
        wifiMonitor.start(queue: DispatchQueue.global(qos: .background))
        ethernetMonitor.start(queue: DispatchQueue.global(qos: .background))
        wifiConnected = nil
        ethernetConnected = nil
        wifiMonitor.pathUpdateHandler = { path in
            self.wifiConnected = path.status == .satisfied
            onChange(self.currentType)
        }
        ethernetMonitor.pathUpdateHandler = { path in
            self.ethernetConnected = path.status == .satisfied
            onChange(self.currentType)
        }
    }
    
    func stopObserving() {
        wifiMonitor.cancel()
        wifiMonitor.pathUpdateHandler = { path in }
    }
}
