#!/bin/zsh
set -e

SRCDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ROOTDIR=`dirname "$SRCDIR"`
BASEDIR="$1/OctoAnimations/Lottie - Web"
ANIMDIR="$ROOTDIR/app-ios/App/presentation/framework/assets/animations"

LUT_LIGHT="$SRCDIR/light_mode_lut.json"
LUT_DARK="$SRCDIR/dark_mode_lut.json"



# Swim
SRC="$BASEDIR/Swim/swim.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-swim.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-swim-dark.json"

# Wave
SRC="$BASEDIR/Wave/wave.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-wave.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-wave-dark.json"

# Party
SRC="$BASEDIR/Party/party.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-party.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-party-dark.json"

# Power
SRC="$BASEDIR/Power/power.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-power.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-power-dark.json"

# Material
SRC="$BASEDIR/Materials/materials.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-material.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-material-dark.json"

# Support
SRC="$BASEDIR/Support/support.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-support.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-support-dark.json"

# Blink
SRC="$BASEDIR/Blink/blink.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-blink.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-blink-dark.json"

# Vacation
SRC="$BASEDIR/Vacation/vacation.min.json"
colorize single "$LUT_LIGHT" "$SRC" "$ANIMDIR/octo-vacation.json"
colorize single "$LUT_DARK" "$SRC" "$ANIMDIR/octo-vacation-dark.json"
