import de.crysxd.octoapp.buildscript.octoAppAndroidLibrary

plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinAndroid)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinComposeCompiler)
    alias(libs.plugins.androidxSafeArgs)
}

octoAppAndroidLibrary("pluginsupport")

dependencies {
    implementation(projects.baseUi)
}