package de.crysxd.octoapp.pluginsupport.ngrok

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.models.hasPlugin
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.message.NgrokPluginMessage
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn

class NgrokSupportViewModel(
    printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val updateNgrokTunnelUseCase: UpdateNgrokTunnelUseCase,
) : ViewModel() {

    private val tag = "NgrokSupportViewModel"
    private val instanceId get() = printerConfigurationRepository.getActiveInstanceSnapshot()?.id

    private val credentialsChanges = printerConfigurationRepository.instanceInformationFlow()
        .map { instance ->
            instance?.alternativeWebUrl?.takeIf { it.isNgrokUrl() }?.let {
                Napier.i(tag = tag, message = "Instance with ngrok configuration was updated, updating ngrok as well")
                try {
                    updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.Tunnel(tunnel = it.toString(), instanceId = instance.id))
                } catch (e: Exception) {
                    Napier.e(tag = tag, message = "Failed to update ngrok config", throwable = e)
                }
            }
        }

    private val connectionEvents = printerEngineProvider.passiveConnectionEventFlow("ngrok-support")
        .onEach { event ->
            val id = instanceId ?: return@onEach

            if (event?.connectionType == ConnectionType.Default && printerConfigurationRepository.getActiveInstanceSnapshot().hasPlugin(Settings.Ngrok::class)) {
                Napier.i(tag = tag, message = "Instance with ngrok configuration was connected, updating ngrok")
                try {
                    updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.FetchConfig(id))
                } catch (e: Exception) {
                    Napier.e(tag = tag, message = "Failed to update ngrok config", throwable = e)
                }
            }
        }

    private val messages = printerEngineProvider.passiveCachedMessageFlow("ngrok-support", NgrokPluginMessage::class)
        .filterNotNull()
        .onEach {
            val id = instanceId ?: return@onEach

            Napier.i(tag = tag, message = "Received new ngrok configuration: $it")
            try {
                updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.Tunnel(tunnel = it.tunnel, instanceId = id))
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to update ngrok config", throwable = e)
            }
        }

    @OptIn(ExperimentalCoroutinesApi::class)
    val events = printerConfigurationRepository.instanceInformationFlow(instanceId).flatMapLatest { config ->
        if (config?.hasPlugin(OctoPlugins.Ngrok) == true) {
            combine(credentialsChanges, connectionEvents, messages) { _, _, _ -> }
        } else {
            emptyFlow()
        }
    }.retry {
        delay(1000)
        Napier.e(tag = tag, message = "Failed", throwable = it)
        true
    }.shareIn(viewModelScope, replay = 1, started = SharingStarted.Lazily)
}