package de.crysxd.octoapp.pluginsupport.octolapse

import android.graphics.PointF
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.gcode.GcodeRenderView
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.dto.message.OctolapsePluginMessage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseSnapshotPlanPreview
import de.crysxd.octoapp.pluginsupport.R
import de.crysxd.octoapp.viewmodels.ext.printBed
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class OctolapseBottomSheetViewModel(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : BaseViewModel() {

    private val tag = "OctolapseBottomSheetViewModel"
    private var activeJobId: String? = null

    private val mutableEvents = MutableSharedFlow<OctolapseState>(1)
    val events = mutableEvents.onEach {
        activeJobId = (it as? OctolapseState.SnapshotPlan)?.jobId
    }

    private val flow = printerEngineProvider
        .passiveCachedMessageFlow("octolapse-bottomsheet", OctolapsePluginMessage::class)
        .filterNotNull()
        .combine(printerConfigurationRepository.instanceInformationFlow(null)) { message, instance ->
            Napier.i(tag = tag, message = "Octolapse message: ${message.type}")

            when {
                message.errors?.firstOrNull() != null -> OctolapseState.Close
                message.snapshotPlanPreview != null -> OctolapseState.SnapshotPlan(
                    layers = convertSnapshotPlanToGcode(instance, message.snapshotPlanPreview?.snapshotPlans!!),
                    jobId = message.snapshotPlanPreview?.jobId,
                )

                message.type == OctolapsePluginMessage.Type.SnapshotPlanComplete -> OctolapseState.Close
                message.type == OctolapsePluginMessage.Type.TimelapseStart -> OctolapseState.Close
                message.type == OctolapsePluginMessage.Type.GcodePreProcessingUpdate -> OctolapseState.Loading(R.string.octolapse___snapshot_plan___title_analyzing)
                message.type == OctolapsePluginMessage.Type.GcodePreProcessingStart -> OctolapseState.Loading(R.string.octolapse___snapshot_plan___title_analyzing)
                else -> null
            }
        }
        .filterNotNull()
        .flowOn(Dispatchers.Default)
        .distinctUntilChanged()


    init {
        viewModelScope.launch {
            flow.catch { Napier.e(tag = tag, message = "Failed", throwable = it) }.collect { mutableEvents.tryEmit(it) }
        }
    }

    fun consumePlan(planPreview: OctolapseSnapshotPlanPreview) = viewModelScope.launch(Dispatchers.Default) {
        try {
            val instance = printerConfigurationRepository.get(null)
            val layers = planPreview.snapshotPlans?.let { convertSnapshotPlanToGcode(instance, it) } ?: emptyList()
            mutableEvents.tryEmit(OctolapseState.SnapshotPlan(layers, planPreview.jobId))
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
            mutableEvents.tryEmit(OctolapseState.Close)
        }
    }

    private fun convertSnapshotPlanToGcode(
        instance: PrinterConfigurationV3?,
        snapshotsPlan: OctolapseSnapshotPlanPreview.SnapshotPlanList
    ): List<OctolapseState.Layer>? {
        val width = snapshotsPlan.volume?.width ?: return null
        val height = snapshotsPlan.volume?.height ?: return null
        val origin = snapshotsPlan.volume?.origin ?: return null
        val snapshots = snapshotsPlan.snapshotPlans ?: return null

        return snapshots.mapIndexed { i, snap ->
            val initialPosition = snap.initialPosition.toPointF()
            val snapshotPosition = snap.steps?.takeWhile { it.action != "snapshot" }?.findLast { it.action == "travel" }.toPointF()
            val returnPosition = snap.initialPosition.toPointF()
            val renderContext = GcodeRenderContext(
                previousLayerPaths = null,
                completedLayerPaths = emptyList(),
                remainingLayerPaths = null,
                printHeadPosition = null,
                layerCount = snapshots.size,
                layerNumber = i,
                layerProgress = 0f,
                layerZHeight = 0f,
                gcodeBounds = null,
                layerNumberDisplay = { it },
                layerCountDisplay = { it },
            )
            val params = GcodeRenderView.RenderParams(
                renderContext = renderContext,
                printBedSizeMm = PointF(width, height),
                originInCenter = origin == OctolapseSnapshotPlanPreview.Origin.Center,
                extrusionWidthMm = width * 0.1f,
                quality = GcodePreviewSettings.Quality.Ultra,
                printBed = instance.printBed,
            )
            OctolapseState.Layer(
                renderParams = params,
                initialPosition = initialPosition,
                returnPosition = returnPosition,
                snapshotPosition = snapshotPosition,
                layerNumber = i,
                layerCount = snapshots.size
            )
        }.takeIf { it.isNotEmpty() }
    }

    private fun OctolapseSnapshotPlanPreview.Position?.toPointF(): PointF? {
        val x = this?.x ?: return null
        val y = this.y ?: return null
        return PointF(x, y)
    }

    private fun OctolapseSnapshotPlanPreview.Step?.toPointF(): PointF? {
        val x = this?.x ?: return null
        val y = this.y ?: return null
        return PointF(x, y)
    }

    fun accept() = viewModelScope.launch(coroutineExceptionHandler) {
        // No job? Close right away
        val jobId = requireNotNull(activeJobId) { "No render job ID available" }
        val state = mutableEvents.replayCache.firstOrNull() ?: OctolapseState.Loading(0)
        try {
            mutableEvents.tryEmit(OctolapseState.Loading(0))
            printerEngineProvider.printer().asOctoPrint().octolapseApi.acceptSnapshotPlan(jobId)
            activeJobId = null
            mutableEvents.tryEmit(OctolapseState.Close)
        } catch (e: Exception) {
            // Restore
            mutableEvents.tryEmit(state)
            throw e
        }
    }

    fun onDismiss() = AppScope.launch {
        try {
            // Use app scope, fragment is dismissed
            // No job? Either not done loading or already accepted
            activeJobId?.let {
                postMessage(
                    OctoActivity.Message.SnackbarMessage(
                        text = { it.getString(R.string.cancelling) },
                        type = OctoActivity.Message.SnackbarMessage.Type.Positive
                    )
                )
                printerEngineProvider.printer().asOctoPrint().octolapseApi.cancelPreprocessing(activeJobId)
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
        }
    }
}